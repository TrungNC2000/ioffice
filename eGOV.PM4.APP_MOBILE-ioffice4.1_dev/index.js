/** @format */
/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import { AppRegistry, YellowBox } from 'react-native';
import Setup from './src/Setup';
import { name as appName } from './app.json';
YellowBox.ignoreWarnings(["Require cycle:", "Remote debugger"]);
YellowBox.ignoreWarnings(['Warning: ...']); //Hide warnings
console.disableYellowBox = true;
AppRegistry.registerComponent(appName, () => Setup);
