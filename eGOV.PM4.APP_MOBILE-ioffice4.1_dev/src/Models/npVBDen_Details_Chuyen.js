export default class npVBDen_Details_Chuyen {
    isDuyet: boolean;
    ma_van_ban_den: string;
    ma_xu_ly_den: string;
    ma_ctcb_gui: string;
    ma_yeu_cau: int;
    ma_yeu_cau_real: int;
    trich_yeu: string;
    activeTab: int;
    constructor(
        isDuyet = false,
        ma_van_ban_den = "",
        ma_xu_ly_den = "",
        ma_ctcb_gui = "",
        ma_yeu_cau = 1,
        ma_yeu_cau_real = 1,
        trich_yeu = "",
        activeTab = 0,
    ) {
        this.isDuyet = isDuyet;
        this.ma_van_ban_den = ma_van_ban_den;
        this.ma_xu_ly_den = ma_xu_ly_den;
        this.ma_ctcb_gui = ma_ctcb_gui;
        this.ma_yeu_cau = ma_yeu_cau;
        this.ma_yeu_cau_real = ma_yeu_cau_real;
        this.trich_yeu = trich_yeu;
        this.activeTab = activeTab;
    }
}