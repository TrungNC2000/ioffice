export default class mKySoSmartCA {
    Comment: string;
    DataBase64: string;
    FontColor: string;
    FontName: string;
    FontSize: int
    FontStyle: int
    Image: string
    Signatures: string;
    VisibleType: int;
    access_token: string;
    filePath: string;
    ma_ctcb: int

    constructor(
        Comment = "",
        DataBase64 = "",
        FontColor = "000000",
        FontName = "Time",
        FontSize = 12,
        FontStyle = 3,
        Image = "",
        Signatures = "",
        VisibleType = 3,
        access_token = "",
        filePath = "",
        ma_ctcb = global.ma_ctcb_kc
    ) {
        this.Comment = Comment;
        this.DataBase64 = DataBase64;
        this.FontColor = FontColor;
        this.FontName = FontName;
        this.FontSize = FontSize;
        this.FontStyle = FontStyle;
        this.Image = Image;
        this.Signatures = Signatures;
        this.VisibleType = VisibleType;
        this.access_token = access_token;
        this.filePath = filePath;
        this.ma_ctcb = ma_ctcb;
    }
}