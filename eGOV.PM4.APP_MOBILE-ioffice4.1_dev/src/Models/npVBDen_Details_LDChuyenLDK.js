export default class npVBDen_Details_LDChuyenLDK {
    isDuyet: boolean;
    ma_van_ban_den: string;
    ma_xu_ly_den: string;
    ma_ctcb_gui: string;
    ma_yeu_cau: int;
    trich_yeu: string;
    constructor(
        isDuyet = false,
        ma_van_ban_den = "",
        ma_xu_ly_den = "",
        ma_ctcb_gui = "",
        ma_yeu_cau = 1,
        trich_yeu = "",
    ) {
        this.isDuyet = isDuyet;
        this.ma_van_ban_den = ma_van_ban_den;
        this.ma_xu_ly_den = ma_xu_ly_den;
        this.ma_ctcb_gui = ma_ctcb_gui;
        this.ma_yeu_cau = ma_yeu_cau;
        this.trich_yeu = trich_yeu;
    }
}