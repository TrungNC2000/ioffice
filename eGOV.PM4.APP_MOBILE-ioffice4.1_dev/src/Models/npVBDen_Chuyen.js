export default class npVBDen_Chuyen {
    isDuyet: boolean;
    yKienXuLy: string;
    checkSms: boolean;
    checkEmail: boolean;
    hanXuLy: string;
    ma_trang_thai: int;
    ma_van_ban_den: string;
    ma_xu_ly_den: string;
    ma_ctcb_gui: string;
    ma_yeu_cau: int;
    trich_yeu: string;
    constructor(
        isDuyet = false,
        yKienXuLy = "",
        checkSms = false,
        checkEmail = false,
        hanXuLy = "",
        ma_trang_thai = 1,
        ma_van_ban_den = "",
        ma_xu_ly_den = "",
        ma_ctcb_gui = "",
        ma_yeu_cau = 1, //Xem để biết
        trich_yeu = "",
    ) {
        this.isDuyet = isDuyet;
        this.yKienXuLy = yKienXuLy;
        this.checkSms = checkSms;
        this.checkEmail = checkEmail;
        this.hanXuLy = hanXuLy;
        this.ma_trang_thai = ma_trang_thai;
        this.ma_van_ban_den = ma_van_ban_den;
        this.ma_xu_ly_den = ma_xu_ly_den;
        this.ma_ctcb_gui = ma_ctcb_gui;
        this.ma_yeu_cau = ma_yeu_cau;
        this.trich_yeu = trich_yeu;
    }
}