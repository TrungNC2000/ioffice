export default class npVBDi_ChuyenVT {
    isDuyet: boolean;
    yKienXuLy: string;
    checkSms: boolean;
    checkEmail: boolean;

    ma_van_ban_di: string;
    ma_xu_ly_di: string;
    ma_ctcb_gui: string;
    file_van_ban: string;
    constructor(
        isDuyet = false,
        yKienXuLy = "",
        checkSms = false,
        checkEmail = false,

        ma_van_ban_di = "",
        ma_xu_ly_di = "",
        ma_ctcb_gui = "",
        file_van_ban = "",
    ) {
        this.isDuyet = isDuyet;
        this.yKienXuLy = yKienXuLy;
        this.checkSms = checkSms;
        this.checkEmail = checkEmail;

        this.ma_van_ban_di = ma_van_ban_di;
        this.ma_xu_ly_di = ma_xu_ly_di;
        this.ma_ctcb_gui = ma_ctcb_gui;
        this.file_van_ban = file_van_ban;
    }
}