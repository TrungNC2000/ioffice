export default class mKySoSim {
    uri: string;
    file: string;
    filename: string;
    macongvan: int;
    tenchucdanh: string;
    filehinhanh: string;
    trangky: string;
    domainfile: string;
    domainapi: string;
    ma_ctcb: int;
    madv: int;
    chucnang: string;
    sodienthoai: string;
    msspProvider: string;
    //mode:string;
    //side:string;
    llx: int;
    lly: int;
    urx: int;
    ury: int;
    constructor(
        uri = parseInt(global.web_version) >= 31 ? "/VNPTsignature/rest/signature/byBase64/list" : "http://123.30.60.210/VNPTsignature/rest/signature/byBase64/list",
        file = "",
        filename = "",
        macongvan = 0,
        tenchucdanh = "",
        filehinhanh = "",
        trangky = "",
        domainfile = global.AU_ROOT.replace("api", "file"),
        domainapi = global.AU_ROOT,
        ma_ctcb = global.ma_ctcb_kc,
        madv = global.ma_don_vi,
        chucnang = "vanbandi",
        sodienthoai = global.di_dong_can_bo,
        msspProvider = global.loai_ky_so_sim ? global.loai_ky_so_sim : global.cks_mobile_pki,
        //mode = global.cks_mobile_pki_mode,
        //side = global.cks_mobile_pki_side,
        llx = 0,
        lly = 0,
        urx = 0,
        ury = 0,
    ) {
        this.uri = uri;
        this.file = file;
        this.filename = filename;
        this.macongvan = macongvan;
        this.tenchucdanh = tenchucdanh;
        this.filehinhanh = filehinhanh;
        this.trangky = trangky;
        this.domainfile = domainfile;
        this.domainapi = domainapi;
        this.ma_ctcb = ma_ctcb;
        this.madv = madv;
        this.chucnang = chucnang;
        this.sodienthoai = sodienthoai;
        this.msspProvider = msspProvider;
        this.llx = llx;
        this.lly = lly;
        this.urx = urx;
        this.ury = ury;
    }
}