export default class npVBDi_Details_LDChuyenVT {
    isDuyet: boolean;
    ma_van_ban_di: string;
    ma_xu_ly_di: string;
    ma_ctcb_gui: string;
    ma_ctcb_duyet: string;
    file_van_ban: string;
    trich_yeu: string;
    constructor(
        isDuyet = false,
        ma_van_ban_di = "",
        ma_xu_ly_di = "",
        ma_ctcb_gui = "",
        ma_ctcb_duyet = "",
        file_van_ban = "",
        trich_yeu = "",
    ) {
        this.isDuyet = isDuyet;
        this.ma_van_ban_di = ma_van_ban_di;
        this.ma_xu_ly_di = ma_xu_ly_di;
        this.ma_ctcb_gui = ma_ctcb_gui;
        this.ma_ctcb_duyet = ma_ctcb_duyet;
        this.file_van_ban = file_van_ban;
        this.trich_yeu = trich_yeu;
    }
}