export default class npVBDi_Details_LDChuyenKTTT {
    isDuyet: boolean;
    ma_van_ban_di: string;
    ma_xu_ly_di: string;
    ma_ctcb_gui: string;
    file_van_ban: string;
    trich_yeu: string;
    ma_don_vi_tao: string;
    constructor(
        isDuyet = false,
        ma_van_ban_di = "",
        ma_xu_ly_di = "",
        ma_ctcb_gui = "",
        file_van_ban = "",
        trich_yeu = "",
        ma_don_vi_tao ="",
    ) {
        this.isDuyet = isDuyet;
        this.ma_van_ban_di = ma_van_ban_di;
        this.ma_xu_ly_di = ma_xu_ly_di;
        this.ma_ctcb_gui = ma_ctcb_gui;
        this.file_van_ban = file_van_ban;
        this.trich_yeu = trich_yeu;
        this.ma_don_vi_tao = ma_don_vi_tao;
    }
}