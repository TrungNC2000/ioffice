import { StyleSheet } from 'react-native'
import { AppConfig } from "./AppConfig"

export const AppStyle = StyleSheet.create({
    container: {
        flex: 1
    },
    headerTitleStyle: {
        color: AppConfig.defaultTextColor,
        fontSize: AppConfig.FontSizeLarge
    },
    headerIconStyle: {
        color: AppConfig.defaultTextColor
    },
    footerTabSelectedStyle: {
        color: AppConfig.defaultTextColor,
        opacity: 1,
        fontSize: AppConfig.FontSizeXLarge
    },
    footerTabNoSelectedStyle: {
        color: AppConfig.defaultTextColor,
        opacity: 0.6,
        fontSize: AppConfig.FontSizeXLarge
    },
    drawerIcon: {
        color: AppConfig.blueBackground,
        fontSize: AppConfig.FontSizeLarge
    },
});