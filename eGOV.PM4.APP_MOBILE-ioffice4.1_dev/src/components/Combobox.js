import React, { Component } from 'react'
import { StyleSheet, TouchableOpacity, View } from 'react-native'
import { CardItem, Text, Icon } from "native-base"
import { connect } from "react-redux"
import { AppConfig } from "../AppConfig"

class Combobox extends Component {

    shouldComponentUpdate(nextProps, nextState) {
        if (this.props !== nextProps) {
            return true
        }
        return false
    }

    render() {

        this.styles = StyleSheet.create({
            CardItem: {
                alignItems: 'center'
            },
            CardItemLeft: {
                width: 90
            },
            CardItemRight: {
                flex: 1
            },
            TextTitle: {
                fontSize: this.props.fontSize.FontSizeSmall,
            },
            TextInput: {
                flex: 1,
                fontFamily: AppConfig.fontFamily,
                fontSize: this.props.fontSize.FontSizeNorman,
                color: "#000000",
                borderColor: AppConfig.defaultLineColor,
                borderWidth: AppConfig.defaultLineWidth,
                borderRadius: 5,
                marginRight: 2,
                paddingLeft: 5,
                paddingTop: 7,
                paddingRight: 30,
                minHeight: 40,
            },
        })

        let { Title, textValue, onPress } = this.props
        let padding = this.props.padding ? this.props.padding : 10
        return (
            <CardItem style={[this.styles.CardItem, { paddingTop: padding, paddingBottom: padding }]}>
                <CardItem style={[this.styles.CardItemLeft]}>
                    <Text style={[this.styles.TextTitle]}>{Title}</Text>
                </CardItem>
                <CardItem style={[this.styles.CardItemRight]}>
                    <TouchableOpacity onPress={onPress} style={{ flex: 1, flexDirection: 'row', }}>
                        <Text editable={false} numberOfLines={1} ellipsizeMode="tail" style={this.styles.TextInput}>{textValue}</Text>
                        <View style={{ position: 'absolute', zIndex: 999, right: 0, width: 35, height: 40 }}>
                            <Icon name="arrow-drop-down" type="MaterialIcons" fontSize={this.props.fontSize.FontSizeXXLarge} style={{ color: AppConfig.blueBackground, top: 5, left: 8 }}></Icon>
                        </View>
                    </TouchableOpacity>
                </CardItem>
            </CardItem>
        )
    }
}

function mapStateToProps(state) {
    return {
        fontSize: state.fontSize,
    }
}

export default connect(mapStateToProps)(Combobox)