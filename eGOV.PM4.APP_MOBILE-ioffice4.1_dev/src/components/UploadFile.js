import React, { Component } from "react"
import { View, Modal, Alert, TouchableOpacity, StyleSheet, Platform } from "react-native"
import { Button, Card, CardItem, Icon, Body, Text, Spinner } from "native-base"
import Icon1 from "react-native-vector-icons/MaterialIcons"
import { RNCamera } from "react-native-camera"
import DocumentPicker from "react-native-document-picker"
import API from "../networks"
import ViewFileUpload from "./ViewFileUpload"
import { AppConfig } from "../AppConfig"
import { ToastSuccess } from "../untils/TextUntils"
const DESIRED_RATIO = "16:9"
class TakePicture extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isTaking: false,
            ratio: DESIRED_RATIO,
            type: RNCamera.Constants.Type.back,
            flashMode: RNCamera.Constants.FlashMode.off,
            zoom: 0.1,
        }
    }

    prepareRatio = async () => {
        if (Platform.OS === 'android' && this.camera) {
            const ratios = await this.camera.getSupportedRatiosAsync()
            // See if the current device has your desired ratio, otherwise get the maximum supported one
            // Usually the last element of "ratios" is the maximum supported ratio
            const ratio = ratios.find((ratio) => ratio === DESIRED_RATIO) || ratios[ratios.length - 1]
            this.setState({ ratio })
        }
    }

    switchCamera = () => {
        if (this.state.type === RNCamera.Constants.Type.back) {
            this.setState({ type: RNCamera.Constants.Type.front })
        } else {
            this.setState({ type: RNCamera.Constants.Type.back })
        }
    }

    switchFlashMode = () => {
        if (this.state.flashMode === RNCamera.Constants.FlashMode.off) {
            this.setState({ flashMode: RNCamera.Constants.FlashMode.torch })
        } else if (this.state.flashMode === RNCamera.Constants.FlashMode.torch) {
            this.setState({ flashMode: RNCamera.Constants.FlashMode.auto })
        } else {
            this.setState({ flashMode: RNCamera.Constants.FlashMode.off })
        }
    }

    onZoomIn = () => {
        let zoom = this.state.zoom
        if (zoom > 0.1) {
            this.setState({ zoom: zoom - 0.1 })
        }
    }

    onZoomOut = () => {
        let zoom = this.state.zoom
        if (zoom < 0.9) {
            this.setState({ zoom: zoom + 0.1 })
        }
    }

    takePicture = async () => {
        if (this.camera) {
            const options = { pauseAfterCapture: true, fixOrientation: true, forceUpOrientation: true }
            const data = await this.camera.takePictureAsync(options)
            let res = new Object()
            res.name = data.uri.toString().split("/").pop()
            res.uri = data.uri
            res.type = "image/jpeg"
            this.props.onDoneTakePicture(res, res.name)
        }
    }

    render() {
        let { isShowCamera, togleModalCamera } = this.props
        this.styles = StyleSheet.create({
            container: {
                flex: 1,
                flexDirection: 'column',
                backgroundColor: 'black'
            },
            preview: {
                flex: 1,
                justifyContent: "flex-end",
                alignItems: 'center'
            },
            button: {
                width: 110,
                borderRadius: 5,
                alignSelf: 'center',
                justifyContent: "center",
                alignItems: "center",
                margin: 5,
            },
            buttonText: {
                textAlign: "center",
                fontSize: this.props.fontSize.FontSizeNorman,
                color: "#FFFFFF",
            },
            pendingView: {
                flex: 0,
                backgroundColor: 'lightgreen',
                justifyContent: 'center',
                alignItems: 'center',
            },
            txtWaiting: {
                fontSize: this.props.fontSize.FontSizeNorman,
            },
            buttonEx: {
                width: 50,
                height: 50,
                borderRadius: 5,
                margin: 5,
                alignItems: "center",
                justifyContent: "center",
                borderColor: "gray",
            },
            fileIcon: {
                width: 25,
                height: 25,
                alignSelf: "center",
                color: "gray",
            }
        })

        const PendingView = () => (
            <View style={this.styles.pendingView}>
                <Text style={this.styles.txtWaiting}>Đang thực hiện...</Text>
            </View>
        )

        let iconFlashName = this.state.flashMode === RNCamera.Constants.FlashMode.torch ? "flash-on" : this.state.flashMode === RNCamera.Constants.FlashMode.auto ? "flash-auto" : "flash-off"

        return (
            <Modal
                transparent={true}
                animationType="slide"
                onRequestClose={togleModalCamera}
                visible={isShowCamera}
                supportedOrientations={["landscape", "landscape-left", "landscape-right", "portrait", "portrait-upside-down"]}
            >
                <View style={this.styles.container}>
                    {
                        <RNCamera
                            ref={ref => { this.camera = ref }}
                            style={this.styles.preview}
                            permissionDialogTitle={'Quyền sửa dụng camera'}
                            permissionDialogMessage={'Chúng tôi cần quyền sử dụng camera để chụp ảnh'}
                            onCameraReady={this.prepareRatio} // You can only get the supported ratios when the camera is mounted
                            captureAudio={false}
                            ratio={this.state.ratio}
                            type={this.state.type}
                            flashMode={this.state.flashMode}
                            zoom={this.state.zoom}
                            autoFocus={RNCamera.Constants.AutoFocus.on}
                        >
                            {({ camera, status }) => {
                                if (status !== 'READY') return <PendingView />
                                return (
                                    <View style={{ bottom: 0, width: 250, position: "absolute", flexDirection: 'row', flexWrap: "wrap", justifyContent: 'center', alignItems: "center", alignSelf: "center" }}>
                                        <Button bordered primary onPress={this.switchCamera} style={this.styles.buttonEx}>
                                            <Icon type="MaterialIcons" name="cached" style={this.styles.fileIcon} />
                                        </Button>
                                        <Button bordered primary onPress={this.onZoomOut} style={this.styles.buttonEx}>
                                            <Icon type="MaterialIcons" name="zoom-in" style={this.styles.fileIcon} />
                                        </Button>
                                        <Button bordered primary onPress={this.onZoomIn} style={this.styles.buttonEx}>
                                            <Icon type="MaterialIcons" name="zoom-out" style={this.styles.fileIcon} />
                                        </Button>
                                        <Button bordered primary onPress={this.switchFlashMode} style={this.styles.buttonEx}>
                                            <Icon type="MaterialIcons" name={iconFlashName} style={this.styles.fileIcon} />
                                        </Button>
                                        <Button primary onPress={togleModalCamera} style={this.styles.button}>
                                            <Text style={this.styles.buttonText}>Quay lại</Text>
                                        </Button>
                                        <Button danger onPress={this.takePicture} style={this.styles.button}>
                                            <Text style={this.styles.buttonText}> Chụp </Text>
                                        </Button>

                                    </View>
                                )
                            }}
                        </RNCamera>
                    }
                </View>
            </Modal >
        )
    }
}

export default class UploadFile extends Component {
    constructor(props) {
        super(props)
        this.state = {
            res: {},
            sourceFiles: [],
            name: "Nhấn vào để chọn tập tin",
            showModal: false,
            loading: false,
            isShowCamera: false,
        }
        this.isUpdate = true
        this.mutilFile = true
    }

    componentDidMount() {
        this.mutilFile = this.props.mutilFile !== undefined ? this.props.mutilFile : true
    }

    componentWillReceiveProps(nextProps, nextState) {
        if (nextProps.sourceFiles.length > 0 && this.isUpdate) {
            this.isUpdate = false
            this.mutilFile = this.props.mutilFile ? this.props.mutilFile : true
            this.setState({ sourceFiles: nextProps.sourceFiles })
        }
    }

    openModal = () => { this.setState({ showModal: true }) }

    closeModal = () => {
        this.setState({
            showModal: false,
            res: {},
            name: "Nhấn vào để chọn tập tin",
            loading: false
        })
    }

    togleModalCamera = () => {
        this.setState({ isShowCamera: !this.state.isShowCamera })
    }

    resetForm = () => {
        this.setState({
            res: {},
            name: "Nhấn vào để chọn tập tin",
            loading: false
        })
    }

    onPressTakePicture = () => {
        this.togleModalCamera()
    }

    onDoneTakePicture = (res, name) => {
        this.setState({ res, name })
        this.togleModalCamera()
    }

    onPressChooseFile = async () => {
        if (this.state.loading) { return null }
        try {
            const res = await DocumentPicker.pick({ type: [DocumentPicker.types.allFiles] }).then((res) => {
                if (res) {
                        let name = res.name
                        let tailOfFile = name.substr(name.lastIndexOf(".") + 1, name.length).toLowerCase()
                        if (tailOfFile == "pdf" ||
                            tailOfFile == "doc" ||
                            tailOfFile == "docx" ||
                            tailOfFile == "png" ||
                            tailOfFile == "jpg" ||
                            tailOfFile == "odt" ||
                            tailOfFile == "xlsx" ||
                            tailOfFile == "xls"
                        ) {
                            this.setState({ res, name })
                        } else {
                            this.setState({ res: {}, name: "Nhấn vào để chọn tập tin" }, () => alert("Tập tin không hợp lệ!"))
                        }
                } else {
                    alert('e' + JSON.stringify(error));
                }
            })
            
        } catch (err) {

        }
    }

    onPressUpload = () => {
        console.log("CONSOLE_AU_FILE_CDLF ERROR LLK: " + "onPressUpload")
        let { res, sourceFiles } = this.state
        let { chuc_nang } = this.props
        this.setState({ loading: true })
        if (res.name) {
            API.FILE.AU_FILE_CDLF(res, chuc_nang).then((response) => {
                if (response) {
                    API.FILE.AU_FILE_TL(res, chuc_nang).then((response1) => {
                        if (response1) {
                            if (!this.mutilFile) {
                                sourceFiles = []
                                sourceFiles.push(response1)
                            } else {
                                sourceFiles.push(response1)
                            }
                            this.closeModal()
                            this.setState({ sourceFiles }, () => {
                                ToastSuccess("Tải lên tập tin thành công!")
                            })
                        } else {
                            this.resetForm()
                            Alert.alert("Thông báo", "Tải lên tập tin thất bại!")
                        }
                    })
                } else {
                    this.resetForm()
                    Alert.alert("Thông báo", "Không đủ không gian lưu trữ!")
                }
            })
        } else {
            this.resetForm()
            Alert.alert("Thông báo", "Bạn chưa chọn tập tin")
        }
    }

    getListFileUpload = () => {
        let sourceFiles = this.state.sourceFiles
        if (sourceFiles.length > 0) {
            return sourceFiles.join(":")
        }
        return ""
    }


    clearFileUpload = () => {
        let sourceFiles = []
        this.setState({ sourceFiles })
    }

    render() {

        this.styles = StyleSheet.create({
            container: {
                alignItems: "flex-start"
            },
            viewUpload: {
                flex: 1,
                backgroundColor: "rgba(33,33,33,0.8)",
                alignItems: "center",
                justifyContent: "center"
            },
            card: {
                minWidth: 250,
                maxWidth: 300,
                justifyContent: "flex-start",
                alignItems: "stretch",
                borderRadius: 8,
                paddingBottom: 10,
                paddingLeft: 10,
                paddingRight: 10
            },
            cardItemTitle: {
                alignItems: "center",
                justifyContent: "center",
                borderTopLeftRadius: 8,
                borderTopRightRadius: 8,
                paddingTop: 5,
                paddingBottom: 5
            },
            textChonVBDK: {
                textAlign: "center",
                fontSize: this.props.fontSize.FontSizeLarge,
                fontWeight: "bold"
            },
            body: {
                justifyContent: "center",
                alignItems: "center",
                padding: 10
            },
            viewIcon: {
                alignItems: "center",
                flexDirection: "row"
            },
            iCon: {
                fontSize: 45,
                margin: 5,
                color: AppConfig.blueBackground,
            },
            textFileName: {
                fontSize: this.props.fontSize.FontSizeSmall
            },
            cardButton: {
                justifyContent: "center",
                alignItems: "flex-end",
            },
            button: {
                height: 40,
                borderRadius: 4,
                alignItems: "center",
                justifyContent: "center",
                marginTop: 10
            },
            buttonText: {
                textAlign: "center",
                color: "#FFF",
                fontSize: this.props.fontSize.FontSizeNorman
            },
            btnThemFile: {
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "flex-start"
            },
            fileIcon: {
                color: AppConfig.blueBackground,
                marginRight: 5
            },
            txtUploadFile: {
                color: AppConfig.blueBackground,
                fontSize: this.props.fontSize.FontSizeNorman
            }
        })

        let { showModal, name, loading, sourceFiles } = this.state
        let { navigation, request } = this.props
        return (
            <View style={this.styles.container}>
                <Modal
                    transparent={true}
                    animationType="slide"
                    onRequestClose={this.closeModal}
                    visible={showModal}
                    supportedOrientations={["landscape", "landscape-left", "landscape-right", "portrait", "portrait-upside-down"]}
                >
                    <View style={this.styles.viewUpload}>
                        <TakePicture
                            isShowCamera={this.state.isShowCamera}
                            togleModalCamera={this.togleModalCamera}
                            onDoneTakePicture={this.onDoneTakePicture}
                            fontSize={this.props.fontSize}
                        />
                        <Card style={this.styles.card}>
                            <CardItem style={this.styles.cardItemTitle}>
                                <Text numberOfLines={1} ellipsizeMode="tail" style={this.styles.textChonVBDK}>Chọn tập tin đính kèm</Text>
                            </CardItem>
                            <CardItem>
                                <Body style={this.styles.body}>
                                    <View style={this.styles.viewIcon}>
                                        <Icon onPress={this.onPressChooseFile} type="SimpleLineIcons" name="cloud-upload" style={this.styles.iCon} />
                                        <Icon onPress={this.onPressTakePicture} type="SimpleLineIcons" name="camera" style={this.styles.iCon} />
                                    </View>
                                    <Text numberOfLines={1} ellipsizeMode="tail" style={this.styles.textFileName}>{name}</Text>
                                </Body>
                            </CardItem>
                            {
                                (loading) ? (
                                    <Spinner />
                                ) : (
                                        <CardItem footer style={this.styles.cardButton}>
                                            <Button info style={this.styles.button} onPress={this.closeModal}>
                                                <Text style={this.styles.buttonText}>Đóng</Text>
                                            </Button>
                                            <Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>&nbsp;&nbsp;&nbsp;&nbsp;</Text>
                                            <Button info style={this.styles.button} onPress={() => this.setState({ loading: true }, () => this.onPressUpload())}>
                                                <Text style={this.styles.buttonText}>Xác nhận</Text>
                                            </Button>
                                        </CardItem>
                                    )
                            }
                        </Card>
                    </View>
                </Modal>
                <ViewFileUpload
                    src_file={sourceFiles.join(":")}
                    navigation={navigation}
                    onDeleteFile={(item) => {
                        Alert.alert("Xác nhận", "Bạn muốn xóa tập tin này?",
                            [
                                { text: "Đóng", onPress: () => { console.log("Đóng") }, style: "cancel" },
                                { text: "Đồng ý", onPress: () => { this.setState({ sourceFiles: sourceFiles.filter(el => el !== item) }) }, style: "default" }
                            ], { cancelable: false }
                        )
                    }}
                />
                <TouchableOpacity style={this.styles.btnThemFile} onPress={() => this.openModal()}>
                    <Icon type="MaterialIcons" name="attachment" style={this.styles.fileIcon} />
                    <Text numberOfLines={1} ellipsizeMode="tail" style={this.styles.txtUploadFile}>Chọn tập tin đính kèm {request && <Text style={{ color: 'red', justifyContent: 'center'}}>(*)</Text>}</Text>
                </TouchableOpacity>
            </View>
        )
    }
}