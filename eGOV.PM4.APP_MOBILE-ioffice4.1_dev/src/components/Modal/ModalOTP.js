import React, { Component } from 'react';
import { StyleSheet, Modal, Alert, View, TouchableOpacity, } from 'react-native';
import CodeInput from 'react-native-confirmation-code-input';
import { Button, Icon, Text } from 'native-base';
import { connect } from "react-redux"
import { AppConfig } from "../../AppConfig"
import API from "../../networks"
import Entypo from 'react-native-vector-icons/Entypo'

class ModalOTP extends Component {
    constructor(props) {
        super(props);
        this.state = {
            waitForLogin: false,
            openModalAgain: false,
        }
        this.codeSms = ''
        this.count = 0
        this.fillFull = false
    }
    dangNhap() {
        let codeSms = this.codeSms
        this.setState({ waitForLogin: true, })
        this._onFinishCheckingCode1(codeSms)
    }
    _onFinishCheckingCode1(isValid) {
        if (!isValid) {
            Alert.alert(
                'Thông báo',
                'Vui lòng nhập điền đầy đủ mã',
                [{ text: 'OK', onPress: () => this.setState({ waitForLogin: false, }) }],
                { cancelable: false }
            );
        } else {
            let chuoi1 = isValid.toString()
            let chuoi2 = this.props.stringCode
            this.checkOTP(chuoi1, chuoi2)
        }
    }

    checkOTP = async (value1, value2) => {
        let chuoi1 = value1
        let chuoi2 = value2
        API.Login.AU_CHECKOTPCODE(chuoi1, chuoi2).then((response) => {
            if (response) {
                this.props.parentModal.checkedOTPSuccess()
            } else {
                Alert.alert(
                    'Thông báo',
                    'Mã xác thực chưa đúng!',
                    [{ text: 'OK', onPress: () => console.log("1") }],
                    { cancelable: false }
                );
                this.count += 1
                if (this.count > 2) {
                    this._onPressClose()
                    this.count = 0
                    return;
                }
            }
        })
    }

    _onPressClose() {
        this.setState({
            openModalAgain: this.state.openModalAgain,
        })
        this.props.parentModal.setModalVisible(false)
        this.props.parentModal._return()
    }

    getOTPCodeAgain() {
        this.props.parentModal.getOTPString2()
        this.count = 0
    }

    render() {

        this.styles = StyleSheet.create({
            inputWrapper1: {
                paddingVertical: 30,
                justifyContent: 'flex-end',
                alignItems: 'center',
                flex: 4,
            },
            textNote: {
                fontSize: this.props.fontSize.FontSizeNorman,
            },
            inputLabel1: {
                color: AppConfig.blueBackground,
                fontSize: this.props.fontSize.FontSizeLarge,
            },
            container: {
                flex: 1,
                alignItems: 'center',
                justifyContent: "flex-start"
            },
            button: {
                height: 50,
                width: '40%',
                borderRadius: 10,
                borderWidth: 2,
                borderColor: AppConfig.blueBackground,
                flexDirection: 'row',
                justifyContent: 'space-around',
                alignItems: 'center',
                paddingVertical: 5,
            },
            icon: {
                position: 'absolute',
                top: 'auto',
                left: 12,
            },
            boxText: {
                marginLeft: 20,
                justifyContent: 'center',
                alignItems: 'flex-end',
            },
            buttonText: {
                textAlign: 'center',
                fontSize: this.props.fontSize.FontSizeSmall,
            },
        })

        return (
            <Modal
                transparent={false}
                animationType="slide"
                visible={this.props.modalVisible}
                supportedOrientations={["landscape", "landscape-left", "landscape-right", "portrait", "portrait-upside-down"]}
            >
                <View style={{ flex: 1, padding: 15, backgroundColor: AppConfig.grayBackground }}>

                    <View style={{ flex: 1, backgroundColor: "#FFFFFF", borderRadius: 10, borderWidth: 2, borderColor: AppConfig.blueBackground }}>
                        <View style={{ flexDirection: 'row', flex: 2 }}>

                            <View style={{ position: 'absolute', zIndex: 99, height: 55, left: 0, right: 0, top: 0, justifyContent: "center", padding: 4, alignItems: "center" }}>
                                <Text style={{ fontSize: this.props.fontSize.FontSizeXLarge, fontWeight: 'bold', color: AppConfig.blueBackground }}>Xác thực tài khoản</Text>
                            </View>
                            <View style={{ zIndex: 100, height: 55, alignItems: "flex-end", paddingTop: 5 }}>
                                <Button small danger transparent iconLeft onPress={() => { this._onPressClose() }}><Icon name="close" type="EvilIcons"></Icon></Button>
                            </View>
                        </View>

                        <View style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'center' }}>
                            <Text style={this.styles.textNote}>Mã xác thực đang được gửi đến số</Text>
                            <Text style={[this.styles.textNote, { color: 'red' }]}>{global.di_dong_can_bo}</Text>
                        </View>

                        <View style={this.styles.inputWrapper1}>
                            <Text style={this.styles.inputLabel1}>Nhập mã xác thực trong sms</Text>
                            <CodeInput
                                ref="codeInputRef2"
                                codeLength={4}
                                className={'border-circle'}
                                keyboardType="numeric"
                                activeColor={AppConfig.blueBackground}
                                inactiveColor='#bdc3c7'
                                autoFocus={true}
                                inputPosition='center'
                                size={50}
                                onFulfill={(isValid) => { this.codeSms = isValid; this.fillFull = true }}
                                containerStyle={{ justifyContent: 'center', alignItems: 'center', flex: 9 }}
                                codeInputStyle={{ borderWidth: 1.5, fontSize: this.props.fontSize.FontSizeNorman, }}
                            />
                        </View>
                        <View style={{ flex: 4, zIndex: 99, height: 60, left: 0, right: 0, bottom: 0, flexDirection: "row", justifyContent: "space-evenly", alignItems: "center" }}>
                            <TouchableOpacity style={[this.styles.button]} onPress={() => { this.dangNhap() }}>
                                <Entypo
                                    size={28}
                                    style={this.styles.icon}
                                    color={AppConfig.blueBackground}
                                    name={'check'}
                                />
                                <View style={this.styles.boxText}>
                                    <Text style={[this.styles.buttonText, { color: AppConfig.blueBackground }]}>Đăng nhập</Text>
                                </View>
                            </TouchableOpacity>

                            <TouchableOpacity style={[this.styles.button, { borderColor: '#d35400' }]} onPress={() => { this.getOTPCodeAgain() }}>
                                <Entypo
                                    size={28}
                                    style={this.styles.icon}
                                    color={'#d35400'}
                                    name={'forward'}
                                />
                                <View style={this.styles.boxText}>
                                    <Text style={[this.styles.buttonText, { color: '#d35400' }]}>Gửi lại mã</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </Modal>
        );
    }
}

function mapStateToProps(state) {
    return {
        fontSize: state.fontSize,
    }
}

export default connect(mapStateToProps)(ModalOTP)