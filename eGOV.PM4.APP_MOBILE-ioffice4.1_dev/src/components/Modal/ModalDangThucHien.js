import React, { Component } from 'react';
import { View } from 'react-native';
import { Text, Spinner } from 'native-base';
import { connect } from "react-redux"
import Modal from "react-native-modal"
import { AppConfig } from "../../AppConfig"

class ModalDangThucHien extends Component {
    shouldComponentUpdate(nextProps, nextState) {
        if (this.props !== nextProps) {
            return true
        }
        return false
    }

    render() {
        let isVisible = this.props.isVisible ? this.props.isVisible : false
        let title = this.props.title ? this.props.title : "Đang thực hiện"
        return (
            <Modal
                isVisible={isVisible}
                backdropOpacity={0}
                presentationStyle={"overFullScreen"}
                swipeDirection="left"
            >
                <View style={{ zIndex: 9999, width: 250, height: 125, alignItems: "center", justifyContent: "center", alignSelf: "center", flexDirection: "row", backgroundColor: "rgba(224,224,224,0.9)", borderRadius: 10 }}>
                    <Spinner color={AppConfig.blueBackground} />
                    <Text style={{ fontSize: this.props.fontSize.FontSizeSmall, color: AppConfig.blueBackground }}>  {title}</Text>
                </View>
            </Modal>
        );
    }
}

function mapStateToProps(state) {
    return {
        fontSize: state.fontSize,
    }
}

export default connect(mapStateToProps)(ModalDangThucHien)