import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity } from "react-native"
import { Icon, Text, Card, CardItem } from "native-base"
import { AppConfig } from "../AppConfig"
import { getFileName } from "../untils/TextUntils"
import API from "../networks"

export default class SignInfo extends Component {
    constructor(props) {
        super(props)
        this.state = {
            dataSign: [],
            isShow: false
        }
    }

    componentDidMount() {
        this.getSignInfo(this.props.ma_van_ban, this.props.type)
    }

    componentWillReceiveProps(nextProps) {
        this.setState({ dataSign: [], isShow: false })
        this.getSignInfo(nextProps.ma_van_ban, nextProps.type)
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (this.props !== nextProps ||
            this.state.dataSign !== nextState.dataSign ||
            this.state.isShow !== nextState.isShow) {
            return true
        }
        return false
    }

    getSignInfo = (ma_van_ban, type) => {
        API.KYSO.AU_KYSO_TFP(ma_van_ban, type).then((dataSign) => {
            this.setState({ dataSign })
        })
    }

    onPressShow = () => {
        this.setState({ isShow: !this.state.isShow })
    }

    renderSignInfo = () => {
        let result = []
        let stt = 0
        let length = this.state.dataSign.length
        for (let item of this.state.dataSign) {
            stt += 1
            let noi_dung = getFileName(item.path)
            if (length !== 1) {
                noi_dung = stt + ". " + noi_dung
            }
            result.push(
                <CardItem style={[this.styles.CardItem, { flexDirection: "column", justifyContent: "flex-start", alignItems: "flex-start" }]} key={stt}>
                    <Text style={[this.styles.textRight, { color: AppConfig.blueBackground }]} numberOfLines={1} ellipsizeMode="middle">{noi_dung}</Text>
                    {
                        item.vgca_infos.map((obj, index) => {
                            return (<Text style={this.styles.textRight} key={index}>- {obj}</Text>)
                        })
                    }
                </CardItem>
            )
        }
        return result
    }

    render() {

        //let { ma_van_ban, type, fontSize } = this.props

        this.styles = StyleSheet.create({
            Card: {
                marginLeft: 6,
                marginRight: 6,
                marginTop: 0,
                padding: 10,
                borderRadius: 6,
            },
            CardItem: {
                marginTop: AppConfig.defaultPadding,
                marginBottom: AppConfig.defaultPadding,
            },
            textRight: {
                paddingLeft: 5,
                fontSize: this.props.fontSize.FontSizeNorman,
                flexWrap: "wrap",
                flexShrink: 1,
            },
            iconBottom: {
                fontSize: this.props.fontSize.FontSizeSmall,
                color: "#BDBDBD"
            },
            txtSignInfoFileName: {
                fontSize: this.props.fontSize.FontSizeNorman,
                color: AppConfig.blueBackground
            },
            txtSignInfo: {
                fontSize: this.props.fontSize.FontSizeNorman
            },
            viewTitle: {
                paddingTop: 5,
                paddingBottom: 10,
                borderColor: AppConfig.defaultLineColor,
                borderTopWidth: AppConfig.defaultLineWidth
            }
        })

        if (this.state.dataSign.length > 0) {
            return (
                <Card noShadow style={this.styles.Card}>
                    <CardItem style={this.styles.CardItem}>
                        <TouchableOpacity style={{ flex: 1, flexDirection: "row" }} onPress={this.onPressShow}>
                            <Text style={[this.styles.textRight, { fontSize: this.props.fontSize.FontSizeLarge, fontWeight: "bold", }]}>
                                Thông tin chữ ký số&nbsp;&nbsp;
                                <Icon name="chevron-circle-down" type="FontAwesome" style={this.styles.iconBottom} />
                            </Text>
                        </TouchableOpacity>
                    </CardItem>
                    {
                        (this.state.isShow) && (
                            <View style={this.styles.viewTitle}>
                                {this.renderSignInfo()}
                            </View>
                        )
                    }
                </Card>
            )
        } else {
            return (
                <View></View>
            )
        }
    }
}