import React, { PureComponent } from 'react'
import { StyleSheet, ImageBackground } from 'react-native'
import { AppConfig } from "../AppConfig"

export default class Wallpaper extends PureComponent {
    render() {
        return (
            <ImageBackground style={styles.picture} source={AppConfig.LoginBackground}>
                {this.props.children}
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    picture: {
        flex: 1,
        width: null,
        height: null,
        resizeMode: 'cover'
    },
});