import React, { PureComponent } from 'react';
import { ScrollView, StyleSheet, Image, Alert, TouchableOpacity, Platform, Dimensions } from "react-native"
import { Icon, Text, Toast } from "native-base"
import { AppConfig } from "../AppConfig"
import API from "../networks"
import * as Models from "../Models"
import { getFileChiTietTTDH, getFileName, checkVersionWeb, getURLFile, ToastSuccess, GetNowTime, getSignPDFDomainByVerision, getViewFileDomainByVerision } from "../untils/TextUntils"
import base64 from "../untils/base64"
import ModalWebViewCA from "./ModalWebViewCA"
import { downloadFileToView } from "../untils/DownloadFile"
export default class ViewFile extends PureComponent {
    constructor(props) {
        super(props)
        this.state = {
            isShowWebView: false
        }
        this.urlFile = ""
        this.fileName = ""
        this.pathFile = ""
        this.kyCA = false //false: la view file | true: ky ca
        this.kySoHSM = false
        this.kySoSmartCA = false
        this.parentView = this.props.parentView
    }

    componentDidMount = () => {
        Dimensions.addEventListener("change", this.onChangeSize);
    }

    componentWillUnmount() {
        Dimensions.removeEventListener("change", this.onChangeSize);
    }

    onChangeSize = () => {
        this.setState({ isChangeSize: !this.state.isChangeSize })
    }

    onDeleteFile = (pathFile, styles) => {
        let fileName = getFileName(pathFile)
        Alert.alert(
            "Xác nhận",
            "Bạn chắc chắc muốn xóa tập tin: " + fileName + "?\r\n\nChú ý: không thể phục hồi tập tin đã xóa!",
            [
                { text: "Không", onPress: () => console.log("Logout"), style: "cancel" },
                {
                    text: "Có", onPress: () => {

                        let arrFile = this.props.src_file.split(":")
                        arrFile = arrFile.filter(el => el !== pathFile)

                        let chuoi_xoa = pathFile
                        let chuoi_con_lai = arrFile.join(":")
                        let ma_van_ban_di = this.props.ma_van_ban_di
                        let ma_ctcb = global.ma_ctcb_kc
                        let trich_yeu = this.props.trich_yeu
                        let ten_can_bo_xoa = global.ho_va_ten_can_bo

                        API.FILE.AU_FILE_CNLF(chuoi_xoa, chuoi_con_lai, ma_van_ban_di, ma_ctcb, trich_yeu, ten_can_bo_xoa).then((response) => {
                            if (response) {
                                ToastSuccess("Thực hiện thành công", () => {
                                    this.props.onSuccessMobilePKI()
                                })
                            } else {
                                Toast.show({ text: "Lỗi. Vui lòng thử lại sau!", type: "danger", buttonText: "Đóng", position: "top", duration: 2000 })
                            }
                        })
                    }
                }
            ],
            { cancelable: false }
        )
    }

    renderIconDelete = (pathFile, styles) => {
        if (global.lanh_dao === 1 && this.props.parentView == "VBDiDuyet" && checkVersionWeb(51)) {
            return (
                <Icon name="delete-forever" type="MaterialIcons" style={[styles.iconFile, { color: "red", alignSelf: "center" }]} onPress={() => {
                    this.onDeleteFile(pathFile, styles)
                }}></Icon>
            )
        }
    }

    renderListFile = (styles) => {
        let result = []
        let src_file = this.props.src_file
        if (src_file !== null && src_file !== "" && src_file !== "null") {
            let data = getFileChiTietTTDH(src_file.split(":"))
            for (let item of data) {
                let itemTemp = item.split("|")
                if (itemTemp[0] !== "") {
                    if (Platform.OS === "ios" && (itemTemp[2] === "doc" || itemTemp[2] === "docx")) {
                        result.push(
                            <TouchableOpacity key={item} style={styles.viewFile} >
                                {(data.length > 1) && (this.renderIconDelete(itemTemp[4], styles))}
                                <Image style={styles.iconFile} source={{ uri: itemTemp[3] }} />
                                <Text numberOfLines={1} ellipsizeMode="middle" style={styles.textTenFile} onPress={_ => {
                                    if (this.props.isUpdateViewCV && this.props.updateViewCV) {
                                        this.props.updateViewCV()
                                    }
                                    downloadFileToView(itemTemp[1], itemTemp[4])
                                }}>{itemTemp[1]}</Text>
                                {
                                    ((itemTemp[4].indexOf(".pdf") !== -1 || itemTemp[4].indexOf(".doc") !== -1) && global.cks_vnpt_mobile_pki === "1") && (
                                        <Icon name="edit" type="MaterialIcons" style={{ color: "gray" }} onPress={() => {
                                            this.myWebview.resetStateModal()
                                            this.urlFile = itemTemp[0].replace(getViewFileDomainByVerision(), getSignPDFDomainByVerision())
                                            this.fileName = getFileName(itemTemp[4])
                                            this.pathFile = itemTemp[4]
                                            this.kyCA = true
                                            this.kySoHSM = false
                                            this.kySoSmartCA = false
                                            this.toggleModalWebView()
                                        }}></Icon>
                                    )
                                }
                                {
                                    ((itemTemp[4].indexOf(".pdf") !== -1 || itemTemp[4].indexOf(".doc") !== -1) && global.cks_hien_thi_nut_ky_so_hsm === "1") && (
                                        <Icon name="dns" type="MaterialIcons" style={{ color: "gray" }} onPress={() => {
                                            this.myWebview.resetStateModal()
                                            this.urlFile = itemTemp[0].replace(getViewFileDomainByVerision(), getSignPDFDomainByVerision())
                                            this.fileName = getFileName(itemTemp[4])
                                            this.pathFile = itemTemp[4]
                                            this.kyCA = true
                                            this.kySoHSM = true
                                            this.kySoSmartCA = false
                                            this.toggleModalWebView()
                                        }}></Icon>
                                    )
                                }
                                {
                                    ((itemTemp[4].indexOf(".pdf") !== -1 || itemTemp[4].indexOf(".doc") !== -1) && global.cks_smartca_hien_thi_nut_ky === "1") && (
                                        <Icon name="dns" type="MaterialIcons" style={{ color: "gray" }} onPress={() => {
                                            this.myWebview.resetStateModal()
                                            this.urlFile = itemTemp[0].replace(getViewFileDomainByVerision(),getSignPDFDomainByVerision())
                                            this.fileName = getFileName(itemTemp[4])
                                            this.pathFile = itemTemp[4]
                                            this.kyCA = true
                                            this.kySoHSM = false
                                            this.kySoSmartCA = true
                                            this.toggleModalWebView()
                                        }}></Icon>
                                    )
                                }
                            </TouchableOpacity>
                        )
                    } else {
                        result.push(
                            <TouchableOpacity key={item} style={styles.viewFile} >
                                {(data.length > 1) && (this.renderIconDelete(itemTemp[4], styles))}
                                <Image style={styles.iconFile} source={{ uri: itemTemp[3] }} />
                                <Text numberOfLines={1} ellipsizeMode="middle" style={styles.textTenFile} onPress={_ => {
                                    if (this.props.isUpdateViewCV && this.props.updateViewCV) {
                                        this.props.updateViewCV()
                                    }
                                    if (global.US_Xem_Pdf_iOS === "Offline") {
                                        downloadFileToView(itemTemp[1], itemTemp[4])
                                        return null;
                                    }
                                    this.myWebview.resetStateModal()
                                    this.urlFile = itemTemp[0]
                                    this.fileName = getFileName(itemTemp[4])
                                    this.pathFile = itemTemp[4]
                                    this.kyCA = false
                                    this.kySoHSM = false
                                    this.kySoSmartCA = false
                                    this.toggleModalWebView()
                                }}>{itemTemp[1]}</Text>
                                {
                                    ((itemTemp[4].indexOf(".pdf") !== -1 || itemTemp[4].indexOf(".doc") !== -1) && global.cks_vnpt_mobile_pki === "1") && (
                                        <Icon name="edit" type="MaterialIcons" style={{ color: "gray" }} onPress={() => {
                                            this.myWebview.resetStateModal()
                                            this.urlFile = itemTemp[0].replace(getViewFileDomainByVerision(), getSignPDFDomainByVerision())
                                            this.fileName = getFileName(itemTemp[4])
                                            this.pathFile = itemTemp[4]
                                            this.kyCA = true
                                            this.kySoHSM = false
                                            this.kySoSmartCA = false
                                            this.toggleModalWebView()
                                        }}></Icon>
                                    )
                                }
                                {
                                    ((itemTemp[4].indexOf(".pdf") !== -1 || itemTemp[4].indexOf(".doc") !== -1) && global.cks_hien_thi_nut_ky_so_hsm === "1") && (
                                        <Icon name="dns" type="MaterialIcons" style={{ color: "gray" }} onPress={() => {
                                            this.myWebview.resetStateModal()
                                            this.urlFile = itemTemp[0].replace(getViewFileDomainByVerision(), getSignPDFDomainByVerision())
                                            this.fileName = getFileName(itemTemp[4])
                                            this.pathFile = itemTemp[4]
                                            this.kyCA = true
                                            this.kySoHSM = true
                                            this.kySoSmartCA = false
                                            this.toggleModalWebView()
                                        }}></Icon>
                                    )
                                }
                                {
                                    ((itemTemp[4].indexOf(".pdf") !== -1 || itemTemp[4].indexOf(".doc") !== -1) && global.cks_smartca_hien_thi_nut_ky === "1") && (
                                        <Icon name="dns" type="MaterialIcons" style={{ color: "gray" }} onPress={() => {
                                            this.myWebview.resetStateModal()
                                            this.urlFile = itemTemp[0].replace(getViewFileDomainByVerision(), getSignPDFDomainByVerision())
                                            this.fileName = getFileName(itemTemp[4])
                                            this.pathFile = itemTemp[4]
                                            this.kyCA = true
                                            this.kySoHSM = false
                                            this.kySoSmartCA = true
                                            this.toggleModalWebView()
                                        }}></Icon>
                                    )
                                }
                            </TouchableOpacity>
                        )
                    }
                } else {
                    result.push(
                        <TouchableOpacity key={item} style={styles.viewFile} onPress={_ => {
                            if (this.props.isUpdateViewCV && this.props.updateViewCV) {
                                this.props.updateViewCV()
                            }
                            downloadFileToView(itemTemp[1], itemTemp[4])
                        }}>
                            <Image style={styles.iconFile} source={{ uri: itemTemp[3] }} />
                            <Text numberOfLines={1} ellipsizeMode="middle" style={styles.textTenFile}>{itemTemp[1]}</Text>
                        </TouchableOpacity>
                    )
                }
            }
        }
        return result
    }

    toggleModalWebView = () => {
        this.setState({ isShowWebView: !this.state.isShowWebView })
    }

    onMobilePKI_Auto = (tenchucdanh, filehinhanh) => {
        if (checkVersionWeb(16) && global.US_Mobile_Pki_Side === "TOP") {
            tenchucdanh = global.ho_va_ten_can_bo
        }
        let { ma_van_ban_di } = this.props

        this.myWebview.toggleModalDangKySo()
        let mKySoSim = new Models.mKySoSim
        mKySoSim.file = this.pathFile
        mKySoSim.filename = this.fileName
        mKySoSim.macongvan = ma_van_ban_di
        mKySoSim.tenchucdanh = tenchucdanh
        mKySoSim.filehinhanh = filehinhanh
        console.log("mKySoSim", mKySoSim)
        API.KYSO.SendLog(mKySoSim)
        API.KYSO.AU_KYSO_KSS(mKySoSim).then((fileID) => {
            if (checkVersionWeb(26) && global.kyso_no_signhub == 1) {
                //Ky so moi
                if (fileID !== "-1") {
                    this.myWebview.callbackViewFileOnSuccess(
                        getViewFileDomainByVerision() + getURLFile(fileID),
                        () => { this.updateFile(fileID) }
                    )
                } else { this.myWebview.callbackFailed() }
            } else {
                //Ky so cu
                if (fileID !== "-1") {
                    API.KYSO.AU_KYSO_KSSGFS(fileID).then((filepath) => {
                        if (filepath !== "") {
                            this.myWebview.callbackViewFileOnSuccess(
                                getViewFileDomainByVerision() + getURLFile(filepath),
                                () => { this.updateFile(filepath) }
                            )
                        } else { this.myWebview.callbackFailed() }
                    })
                } else { this.myWebview.callbackFailed() }
            }
        })
    }

    onMobilePKI_Location = (infoCA, filehinhanh) => {
        let { ma_van_ban_di } = this.props
        this.myWebview.toggleModalDangKySo()
        let mKySoSim = new Models.mKySoSim
        mKySoSim.file = this.pathFile
        mKySoSim.filename = this.fileName
        mKySoSim.macongvan = ma_van_ban_di
        mKySoSim.filehinhanh = filehinhanh
        mKySoSim.trangky = infoCA.page
        mKySoSim.llx = infoCA.llx
        mKySoSim.lly = infoCA.lly
        mKySoSim.urx = infoCA.urx
        mKySoSim.ury = infoCA.ury
        console.log("mKySoSim", mKySoSim)
        API.KYSO.SendLog(mKySoSim)
        API.KYSO.AU_KYSO_KSS(mKySoSim).then((fileID) => {
            if (checkVersionWeb(26) && global.kyso_no_signhub == 1) {
                //Ky so moi
                if (fileID !== "-1") {
                    this.myWebview.callbackViewFileOnSuccess(
                        getViewFileDomainByVerision() + getURLFile(fileID),
                        () => { this.updateFile(fileID) }
                    )
                } else { this.myWebview.callbackFailed() }
            } else {
                if (fileID !== "-1") {
                    API.KYSO.AU_KYSO_KSSGFS(fileID).then((filepath) => {
                        if (filepath !== "") {
                            this.myWebview.callbackViewFileOnSuccess(
                                getViewFileDomainByVerision() + getURLFile(filepath),
                                () => { this.updateFile(filepath) }
                            )
                        } else { this.myWebview.callbackFailed() }
                    })
                } else { this.myWebview.callbackFailed() }
            }
        })
    }

    onHSM_Location = (infoCA, filehinhanh) => {
        this.myWebview.toggleModalDangKySo()
        let mKySoHSM = new Models.mKySoHSM

        let newFile = this.pathFile
        if (newFile.toLowerCase().split(".").pop() === "doc" || newFile.toLowerCase().split(".").pop() === "docx") {
            newFile = newFile.replace(".docx", ".pdf")
            newFile = newFile.replace(".doc", ".pdf")
            newFile = newFile.replace(".DOC", ".pdf")
            newFile = newFile.replace(".DOCX", ".pdf")
        }

        API.KYSO.AU_KYSO_GBFP(newFile).then((DataBase64) => {
            if (DataBase64 !== "") {
                API.KYSO.AU_KYSO_GBFP(filehinhanh).then((Image) => {
                    if (Image !== "") {
                        let Signatures = []
                        let obj = new Object()
                        obj.rectangle = Math.round(infoCA.llx) + "," + Math.round(infoCA.lly) + "," + Math.round(infoCA.urx) + "," + Math.round(infoCA.ury)
                        obj.page = infoCA.page
                        Signatures.push(obj)
                        mKySoHSM.DataBase64 = DataBase64;
                        mKySoHSM.Image = Image;
                        mKySoHSM.Signatures = base64.encode(JSON.stringify(Signatures));
                        mKySoHSM.access_token = global.accessTokenHSM.access_token;
                        mKySoHSM.filePath = newFile;
                        if (global.US_Mobile_Pki_Mode === "NAME_AND_DESCRIPTION" || global.US_Mobile_Pki_Mode === "DESCRIPTION") {
                            mKySoHSM.VisibleType = 1
                        }
                        if (global.US_Mobile_Pki_Mode === "GRAPHIC_AND_DESCRIPTION") {
                            mKySoHSM.VisibleType = 2
                        }
                        if (global.US_Mobile_Pki_Mode === "GRAPHIC") {
                            mKySoHSM.VisibleType = 3
                        }

                        API.KYSO.AU_KYSO_KSH(mKySoHSM).then((response) => {
                            if (response !== "-1") {
                                console.log(getURLFile(response))
                                this.myWebview.callbackViewFileOnSuccess(
                                    getViewFileDomainByVerision() + getURLFile(response),
                                    () => { this.updateFile(response) }
                                )
                            } else { this.myWebview.callbackFailed() }
                        })
                    }
                })
            }
        })
    }

    onSmartCA_Location_GetFile = (key, count) => {
        setTimeout(() => {
            if (count > 20) {
                this.myWebview.callbackFailed()
                return null
            } else {
                API.KYSO.AU_KYSO_SCGSF(key).then((response) => {
                    if (response.code == 1) {
                        console.log(getURLFile(response.data))
                        this.myWebview.callbackViewFileOnSuccess(
                            getViewFileDomainByVerision() + getURLFile(response.data),
                            () => { this.updateFile(response.data) }
                        )
                    } else {
                        count += 1
                        this.onSmartCA_Location_GetFile(key, count)
                    }
                })
            }
        }, 1500);
    }

    onSmartCA_Location = (infoCA, filehinhanh) => {
        this.myWebview.toggleModalDangKySo()
        let mKySoSmartCA = new Models.mKySoSmartCA

        let newFile = this.pathFile
        if (newFile.toLowerCase().split(".").pop() === "doc" || newFile.toLowerCase().split(".").pop() === "docx") {
            newFile = newFile.replace(".docx", ".pdf")
            newFile = newFile.replace(".doc", ".pdf")
            newFile = newFile.replace(".DOC", ".pdf")
            newFile = newFile.replace(".DOCX", ".pdf")
        }

        API.KYSO.AU_KYSO_GBFP(newFile).then((DataBase64) => {
            if (DataBase64 !== "") {
                API.KYSO.AU_KYSO_GBFP(filehinhanh).then((Image) => {
                    if (Image !== "") {
                        let Signatures = []
                        let obj = new Object()
                        obj.rectangle = Math.round(infoCA.llx) + "," + Math.round(infoCA.lly) + "," + Math.round(infoCA.urx) + "," + Math.round(infoCA.ury)
                        obj.page = infoCA.page
                        Signatures.push(obj)
                        mKySoSmartCA.DataBase64 = DataBase64;
                        mKySoSmartCA.Image = Image;
                        mKySoSmartCA.Signatures = base64.encode(JSON.stringify(Signatures));
                        mKySoSmartCA.access_token = global.accessTokenSmartCA.access_token;
                        mKySoSmartCA.filePath = newFile;
                        if (global.US_Mobile_Pki_Mode === "NAME_AND_DESCRIPTION" || global.US_Mobile_Pki_Mode === "DESCRIPTION") {
                            mKySoSmartCA.VisibleType = 1
                        }
                        if (global.US_Mobile_Pki_Mode === "GRAPHIC_AND_DESCRIPTION") {
                            mKySoSmartCA.VisibleType = 2
                        }
                        if (global.US_Mobile_Pki_Mode === "GRAPHIC") {
                            mKySoSmartCA.VisibleType = 3
                        }

                        if (checkVersionWeb(60)) {
                            let key = global.ma_ctcb_kc + GetNowTime()
                            console.log("key = " + key)
                            API.KYSO.AU_KYSO_SCSH_IOS(mKySoSmartCA, key).then((response) => {
                                console.log("response = " + JSON.stringify(response))
                                this.onSmartCA_Location_GetFile(key, 0)
                            })
                        } else {
                            API.KYSO.AU_KYSO_SCSH(mKySoSmartCA).then((response) => {
                                console.log("AU_KYSO_SCSH response: " + response)
                                if (response !== "-1") {
                                    console.log(getURLFile(response))
                                    this.myWebview.callbackViewFileOnSuccess(
                                        getViewFileDomainByVerision() + getURLFile(response),
                                        () => { this.updateFile(response) }
                                    )
                                } else { this.myWebview.callbackFailed() }
                            })
                        }
                    }
                })
            }
        })
    }

    updateFile = (filepath) => {
        let { ma_van_ban_di, ma_xu_ly_di, src_file, isPhieuTrinh, isVBNB } = this.props
        let arrFile = src_file.split(":")

        //#region Check tham số cks_xoa_file_sau_khi_ky_so
        if (global.cks_xoa_file_sau_khi_ky_so == 1) {
            arrFile = arrFile.filter(el => el !== this.pathFile)
        }
        if (global.cks_xoa_file_sau_khi_ky_so == 2) {
            let ext = this.pathFile.split('.').pop().toLowerCase()
            if (ext == "pdf") {
                arrFile = arrFile.filter(el => el !== this.pathFile)
            }
        }
        //#endregion

        arrFile.push(filepath)
        let chuoi_file_moi = arrFile.join(":")

        if (isVBNB) {
            //Ký VBNB
            API.KYSO.AU_KYSO_LFKS_VBNB(ma_van_ban_di, ma_xu_ly_di, filepath).then((response) => {
                if (response) {
                    this.myWebview.callbacSuccess()
                } else {
                    this.myWebview.callbackFailed()
                }
            })
        } else if (!isPhieuTrinh) {
            //Ký văn bản
            API.KYSO.AU_KYSO_VTCNFKPH(ma_van_ban_di, chuoi_file_moi).then((response) => {
                if (response) {
                    API.KYSO.AU_KYSO_LFKS(ma_van_ban_di, ma_xu_ly_di, chuoi_file_moi).then(() => {
                        API.KYSO.AU_KYSO_UFSKK(ma_van_ban_di, global.ma_ctcb_kc, filepath).then(() => {
                            this.myWebview.callbacSuccess()
                        })
                    })
                } else { this.myWebview.callbackFailed() }
            })
        } else {
            //Ký phiếu trình
            API.KYSO.AU_KYSO_CNVBDKPTO(ma_van_ban_di, chuoi_file_moi).then((response) => {
                if (response) {
                    API.KYSO.AU_KYSO_LFKS(ma_van_ban_di, ma_xu_ly_di, chuoi_file_moi).then(() => {
                        API.KYSO.AU_KYSO_UFSKK(ma_van_ban_di, global.ma_ctcb_kc, filepath).then(() => {
                            this.myWebview.callbacSuccess()
                        })
                    })
                } else { this.myWebview.callbackFailed() }
            })
        }
    }

    render() {
        const { width } = Dimensions.get("screen")
        const stylesNgang = StyleSheet.create({
            viewFile: {
                maxWidth: 250,
                height: 38,
                flexDirection: "row",
                alignItems: "center",
                margin: 2,
                padding: 2,
                borderRadius: 6,
                borderWidth: 0.5,
                borderColor: AppConfig.defaultLineColor
            },
            iconFile: {
                height: 25,
                width: 25,
                marginLeft: 5,
            },
            textTenFile: {
                paddingLeft: 3,
                maxWidth: 145,
                fontSize: this.props.fontSize.FontSizeNorman
            },
        })

        const stylesDoc = StyleSheet.create({
            viewFile: {
                flexDirection: "row",
                alignItems: "center",
                marginTop: 3,
                marginBottom: 3,
            },
            iconFile: {
                height: 25,
                width: 25,
                marginLeft: 5,
            },
            textTenFile: {
                paddingLeft: 3,
                paddingRight: 3,
                maxWidth: Platform.OS === "ios" && Platform.isPad ? width - 335 : width - 200,
                fontSize: this.props.fontSize.FontSizeNorman
            },
        })

        let loaiHienThiFile = parseInt(global.US_FILE_List_Ngang_Doc) //1:Ngang || 2: Dọc
        let styles = loaiHienThiFile === 1 ? stylesNgang : stylesDoc
        let flag = loaiHienThiFile === 1 ? true : false

        return (
            <ScrollView horizontal={flag} scrollEnabled={flag} >
                <ModalWebViewCA
                    ref={(myRef) => { this.myWebview = myRef }}
                    isShowWebView={this.state.isShowWebView}
                    toggleModalWebView={this.toggleModalWebView}

                    title={this.fileName}
                    url={this.urlFile}
                    pathFile={this.pathFile}
                    kyCA={this.kyCA}
                    kySoHSM={this.kySoHSM}
                    kySoSmartCA={this.kySoSmartCA}
                    onMobilePKI_Auto={this.onMobilePKI_Auto}
                    onMobilePKI_Location={this.onMobilePKI_Location}
                    onHSM_Location={this.onHSM_Location}
                    onSmartCA_Location={this.onSmartCA_Location}
                    onSuccessMobilePKI={this.props.onSuccessMobilePKI}

                    fabActions={this.props.fabActions}
                    fabPressItem={this.props.fabPressItem}
                    fontSize={this.props.fontSize}
                />
                {this.renderListFile(styles)}
            </ScrollView>
        )
    }
}