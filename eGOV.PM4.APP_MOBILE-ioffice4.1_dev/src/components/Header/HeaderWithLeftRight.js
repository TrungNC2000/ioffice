import React, { PureComponent } from "react"
import { StyleSheet } from "react-native"
import { connect } from "react-redux"
import { AppConfig } from "../../AppConfig"
import { Header, Text, Icon } from "native-base"
import { Grid, Col } from "react-native-easy-grid"

class HeaderWithLeftRight extends PureComponent {
    render() {

        this.styles = StyleSheet.create({
            headerTitleStyle: {
                color: AppConfig.defaultTextColor,
                fontSize: this.props.fontSize.FontSizeLarge
            },
            headerIconStyle: {
                color: AppConfig.defaultTextColor
            }
        });

        let { title, onPressLeftButton, onPressRightButton, buttonLeftName, buttonRightName } = this.props
        return (
            <Header hasTabs androidStatusBarColor={AppConfig.statusBarColor} iosBarStyle={"light-content"}>
                <Grid style={{ flex: 1, justifyContent: "center", alignItems: 'center', padding: 10 }}>
                    <Col style={{ width: "10%" }}>
                        <Icon name={buttonLeftName} type="MaterialIcons" onPress={onPressLeftButton} style={this.styles.headerIconStyle} />
                    </Col>
                    <Col style={{ justifyContent: "center", alignItems: "center" }}>
                        <Text style={this.styles.headerTitleStyle}>{title}</Text>
                    </Col>
                    <Col style={{ width: "10%", alignItems: "flex-end" }}>
                        <Icon name={buttonRightName} type="MaterialIcons" onPress={onPressRightButton} style={this.styles.headerIconStyle} />
                    </Col>
                </Grid>
            </Header>
        )
    }
}

function mapStateToProps(state) {
    return {
        fontSize: state.fontSize,
    }
}

export default connect(mapStateToProps)(HeaderWithLeftRight)
