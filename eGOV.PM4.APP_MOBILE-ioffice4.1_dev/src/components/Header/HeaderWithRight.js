import React, { PureComponent } from "react"
import { StyleSheet } from "react-native"
import { AppConfig } from "../../AppConfig"
import { Header, Text, Icon } from "native-base"
import { Grid, Col } from "react-native-easy-grid"

class HeaderWithRight extends PureComponent {
    render() {

        this.styles = StyleSheet.create({
            headerTitleStyle: {
                color: AppConfig.defaultTextColor,
                fontSize: this.props.fontSize.FontSizeLarge
            },
            headerIconStyle: {
                color: AppConfig.defaultTextColor
            }
        });

        let { title, onPressRightButton, buttonName } = this.props
        return (
            <Header hasTabs androidStatusBarColor={AppConfig.statusBarColor} iosBarStyle={"light-content"}>
                <Grid style={{ flex: 1, justifyContent: "center", alignItems: 'center', padding: 10 }}>
                    <Col style={{ width: "10%" }}>
                    </Col>
                    <Col style={{ justifyContent: "center", alignItems: "center" }}>
                        <Text style={this.styles.headerTitleStyle}>{title}</Text>
                    </Col>
                    <Col style={{ width: "10%" }}>
                        <Icon name={buttonName} type="MaterialIcons" onPress={onPressRightButton} style={this.styles.headerIconStyle} />
                    </Col>
                </Grid>
            </Header>
        )
    }
}

function mapStateToProps(state) {
    return {
        fontSize: state.fontSize,
    }
}

export default connect(mapStateToProps)(HeaderWithRight)