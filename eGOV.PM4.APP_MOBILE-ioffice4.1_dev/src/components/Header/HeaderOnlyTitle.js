import React, { PureComponent } from "react"
import { StyleSheet } from "react-native"
import { connect } from "react-redux"
import { AppConfig } from "../../AppConfig"
import { Header, Body, Text } from "native-base"

class HeaderOnlyTitle extends PureComponent {
    render() {

        this.styles = StyleSheet.create({
            headerTitleStyle: {
                color: AppConfig.defaultTextColor,
                fontSize: this.props.fontSize.FontSizeLarge
            },
        });

        return (
            <Header style={{ backgroundColor: "red" }} hasTabs androidStatusBarColor={AppConfig.statusBarColor} iosBarStyle={"light-content"}>
                <Body style={{ alignItems: "center", justifyContent: "center" }}>
                    <Text style={this.styles.headerTitleStyle}>{this.props.title}</Text>
                </Body>
            </Header>
        )
    }
}

function mapStateToProps(state) {
    return {
        fontSize: state.fontSize,
    }
}

export default connect(mapStateToProps)(HeaderOnlyTitle)
