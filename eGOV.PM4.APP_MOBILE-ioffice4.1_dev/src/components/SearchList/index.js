import React, { Component } from "react"
import { View, TouchableOpacity, PixelRatio, StyleSheet } from "react-native"
import { AppConfig } from "../../AppConfig"
import Icon from "react-native-vector-icons/FontAwesome5"
import CustomTextInput from "../CustomTextInput"

export default class SearchList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            keyWord: ""
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (this.state.keyWord !== nextState.keyWord) {
            return true
        }
        return false
    }

    onChangeText = (text) => {
        this.setState({ keyWord: text }, () => {
            setTimeout(() => {
                this.props.onSearchList()
            }, 100);
        })
    }

    setKeyWord = (keyWord, callback) => {
        this.setState({ keyWord }, callback)
    }

    getKeyWord = () => {
        return this.state.keyWord
    }

    render() {

        this.styles = StyleSheet.create({
            ViewSearch: {
                flex: 1,
                top: 0,
                left: 0,
                right: 0,
                height: 40,
                paddingLeft: 10,
                paddingRight: 10,
                flexDirection: "row",
                justifyContent: "center",
                marginBottom: 10,
            },
            ButtonSearch: {
                top: 0,
                left: 18,
                paddingTop: 8,
                position: "absolute",
                width: 40,
                height: 40,
                zIndex: 999
            },
            ButtonClean: {
                top: 0,
                right: 5,
                paddingTop: 8,
                position: "absolute",
                width: 40,
                height: 40,
                zIndex: 999
            },
            TextInputSearch: {
                flex: 1,
                fontFamily: AppConfig.fontFamily,
                fontSize: this.props.fontSize.FontSizeSmall / PixelRatio.getFontScale(),
                color: AppConfig.headerBackgroundColor,
                paddingLeft: 40,
                paddingRight: 40,
                borderColor: AppConfig.headerBackgroundColor,
                borderWidth: 1,
                borderRadius: 5,
            }
        })

        return (
            <View style={this.styles.ViewSearch}>
                <TouchableOpacity onPress={this.onSearchList} style={this.styles.ButtonSearch}>
                    <Icon name="search" size={22} color={AppConfig.headerBackgroundColor}></Icon>
                </TouchableOpacity>
                <CustomTextInput
                    style={this.styles.TextInputSearch}
                    value={this.state.keyWord}
                    onChangeText={(text) => this.onChangeText(text)}
                    placeholder="Nhập từ khóa tìm kiếm ..."
                    underlineColorAndroid={"transparent"}
                    onRef={ref => this.input = ref}
                    placeholderStyle={{ top: 9, left: 40, fontSize: this.props.fontSize.FontSizeSmall }}
                />
                <TouchableOpacity onPress={() => this.onChangeText("")} style={this.styles.ButtonClean}>
                    <Icon name="trash-alt" size={22} color={"red"}></Icon>
                </TouchableOpacity>
            </View>
        )
    }
}