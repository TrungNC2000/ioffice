import React, { Component } from 'react';
import { View, Modal, Platform, Image, Alert, TouchableOpacity, StyleSheet, ScrollView, TextInput, PixelRatio, SafeAreaView } from "react-native"
import { Icon, Header, Button, CardItem, Text, Spinner } from "native-base"
import { Grid, Col } from "react-native-easy-grid"
import { WebView } from 'react-native-webview'
import MyStatusBar from "../components/StatusBar"
import FloatingAction from "../components/Fab"
import API from "../networks"
import { AppConfig } from "../AppConfig"
import { getURLFile, isIphoneX } from "../untils/TextUntils"
import { downloadFile } from "../untils/DownloadFile"
import ModalDangThucHien from "../components/Modal/ModalDangThucHien"
import axios from "axios"
import { getSignPDFDomainByVerision, getViewFileDomainByVerision } from "../untils/TextUntils"
import jwt_decode from "jwt-decode";
import { AsyncStorage } from 'react-native';

export default class ModalWebViewCA extends Component {
    constructor(props) {
        super(props)
        this.state = {
            url: this.props.url,
            urlSuccess: "",
            isShowListMCK: false,
            isDangKySo: false,
            listMauChuKy: [],
            primaryImage: "",
            btnCapNhatClick: null,
            txtTaiKhoanHSM: "",
            txtMatKhauHSM: "",
            accessTokenHSM: null,
            accessTokenSmartCA: null
        }
        this.infoCA = {}
        this.loaiKySo = "auto" //auto | location
        this.SmartCARedirctURL = global.cks_smartca_callback
    }

    componentDidMount() {
        this.checkTokenHSM()
        this.checkTokenSmartCA()
        this.getListMauChuKy()

    }

    shouldComponentUpdate = (nextProps, nextState) => {
        if (this.props !== nextProps ||
            this.state !== nextState) {
            if (this.props.url !== nextProps.url) {
                this.setState({ url: nextProps.url })
            }
            return true
        }
        return false
    };

    resetStateModal = () => {
        this.setState({
            urlSuccess: "",
            isShowListMCK: false,
            isDangKySo: false,
            btnCapNhatClick: null,
            txtTaiKhoanHSM: "",
            txtMatKhauHSM: "",
        })
        this.infoCA = {}
        this.loaiKySo = "auto"
    }

    checkTokenHSM = () => {
        if (global.accessTokenHSM) {
            let today = new Date()
            if (global.accessTokenHSM.expire_time < today.getTime()) {
                this.setState({ accessTokenHSM: global.accessTokenHSM })
            }
        }
    }
    // Kiểm tra accessestoken: nếu còn hạn thì chọn kí, sai thì gọi api refresh lấy asse tk mới
    checkTokenSmartCA = () => {
        let today = new Date()
            AsyncStorage.getItem("accessTokenSmartCA").then(asyncStorageRes => {
                global.accessTokenSmartCA = JSON.parse(asyncStorageRes)
                if (global.accessTokenSmartCA.expire_time > today.getTime()) {
                    console.log("LOINB: hết hạn token")
                    this.setState({ accessTokenSmartCA: global.accessTokenSmartCA })
                }else{
                    console.log("LOINB: Còn hạn token")
                }
            });
    }

    //Region Mẫu chữ ký
    getListMauChuKy = () => {
        API.KYSO.AU_KYSO_DSCKS(global.ma_ctcb_kc).then((listMauChuKy) => {
            let primaryImage = ""
            if (listMauChuKy.length > 0) {
                primaryImage = listMauChuKy[0].link_cks
                for (let item of listMauChuKy) {
                    if (item.trang_thai === 1) {
                        primaryImage = item.link_cks
                    }
                }
            }
            this.setState({ listMauChuKy, primaryImage })
        })
    }

    onPressImageLocation = (link_cks) => {
        if (this.props.kySoHSM) {
            this.props.onHSM_Location(this.infoCA, link_cks)
        } else if (this.props.kySoSmartCA) {
            this.props.onSmartCA_Location(this.infoCA, link_cks)
        } else {
            this.props.onMobilePKI_Location(this.infoCA, link_cks)
        }
    }

    renderListMauChuKy = () => {
        let result = []
        for (let item of this.state.listMauChuKy) {
            let colorText = item.trang_thai === 1 ? "blue" : "black"
            if (this.loaiKySo === "auto") {
                result.push(
                    <CardItem key={item.id_ky_so} style={{ flex: 1, padding: 10, borderBottomWidth: 0.7, borderColor: "lightgray", maxHeight: 80 }}>
                        <TouchableOpacity style={{ flex: 1 }} onPress={() => this.props.onMobilePKI_Auto(item.ten_ky_so, item.link_cks)}>
                            <Text style={{ fontSize: this.props.fontSize.FontSizeNorman, color: colorText }}>{item.ten_ky_so}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ flex: 1, justifyContent: "center", alignItems: 'center' }} onPress={() => this.props.onMobilePKI_Auto(item.ten_ky_so, item.link_cks)}>
                            <Image style={{ width: 150, height: 80 }} resizeMode="contain" source={{ uri: global.AU_ROOT + "/api/file-manage/read-advance?path=" + item.link_cks }} />
                        </TouchableOpacity>
                    </CardItem>
                )
            } else {
                result.push(
                    <CardItem key={item.id_ky_so} style={{ flex: 1, padding: 10, borderBottomWidth: 0.7, borderColor: "lightgray", maxHeight: 80 }}>
                        <TouchableOpacity style={{ flex: 1 }} onPress={() => this.onPressImageLocation(item.link_cks)}>
                            <Text style={{ fontSize: this.props.fontSize.FontSizeNorman, color: colorText }}>{item.ten_ky_so}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ flex: 1, justifyContent: "center", alignItems: 'center' }} onPress={() => this.onPressImageLocation(item.link_cks)}>
                            <Image style={{ width: 150, height: 80 }} resizeMode="contain" source={{ uri: global.AU_ROOT + "/api/file-manage/read-advance?path=" + item.link_cks }} />
                        </TouchableOpacity>
                    </CardItem>
                )
            }
        }
        return result
    }
    //End region Mẫu chữ ký

    //Region Convert File
    convertWordToPdf = () => {
        let { title, url, pathFile } = this.props
        if (title.toLowerCase().split(".").pop() === "doc" || title.toLowerCase().split(".").pop() === "docx") {
            let oldFile = pathFile
            let newFile = oldFile.replace(".docx", ".pdf")
            newFile = newFile.replace(".doc", ".pdf")
            newFile = newFile.replace(".DOC", ".pdf")
            newFile = newFile.replace(".DOCX", ".pdf")
            let url = getURLFile(newFile)

            axios({
                method: "GET",
                url: url,
                timeout: 3000
            }).then(response => {
                if (response.status === 200) {
                    if (this.props.kyCA) {
                        url = getSignPDFDomainByVerision() + url
                    } else {
                        url = getViewFileDomainByVerision() + url
                    }
                    this.setState({ url })
                } else {
                    API.FILE.AU_FILE_CDTTWP(oldFile).then((response) => {
                        if (response) {
                            if (this.props.kyCA) {
                                url = getSignPDFDomainByVerision() + url
                            } else {
                                url = getViewFileDomainByVerision() + url
                            }
                            this.setState({ url })
                        }
                    })
                }
            }).catch(error => { })
        }
    }
    //End region Convert File

    postMessage = (message) => {
        let injectedData = ``;
        switch (message) {
            case "pressAdd":
                injectedData = `window.pressAdd(); true;`;
                break;
            case "pressDelete":
                injectedData = `window.pressDelete(); true;`;
                break;
            case "pressSign":
                injectedData = `window.pressSign(); true;`;
                break;
            case "pressSignHSM":
                injectedData = `window.pressSignHSM(); true;`;
                break;
            default:
                break;
        }
        this.WebView.injectJavaScript(injectedData)
    }

    //Region toggle Modal
    toggleModalMCK = (loaiKySo, infoCA) => {
        if (!this.state.isShowListMCK) {
            if (this.state.listMauChuKy.length > 0) {
                if (this.state.listMauChuKy.length === 1) {
                    let { ten_ky_so, link_cks } = this.state.listMauChuKy[0]
                    if (loaiKySo === "auto") {
                        this.props.onMobilePKI_Auto(ten_ky_so, link_cks)
                    } else {
                        if (this.props.kySoHSM === false && this.props.kySoSmartCA === false) {
                            //Ký số Mobile PKI
                            this.props.onMobilePKI_Location(infoCA, link_cks)
                        } else {
                            //Ký số HSM và SmartCA
                            this.loaiKySo = loaiKySo
                            this.infoCA = infoCA
                            this.setState({ isShowListMCK: true })
                        }
                    }
                } else {
                    this.loaiKySo = loaiKySo
                    this.infoCA = infoCA
                    this.setState({ isShowListMCK: true })
                }
            } else {
                Alert.alert("Thông báo", "Chưa cài đặt mẫu chữ ký", [{ text: "Đóng", style: "cancel" },])
            }
        } else {
            this.setState({ isShowListMCK: false })
        }
    }

    toggleModalDangKySo = (callback) => {
        this.setState({ isDangKySo: !this.state.isDangKySo })
        if (callback) {
            callback()
        }
    }
    //End region toggle Modal

    //Region Callback
    callbackViewFileOnSuccess = (urlSuccess, btnCapNhatClick) => {
        console.log("callbackViewFileOnSuccess")
        this.setState({ isDangKySo: false }, () => {
            setTimeout(() => {
                this.setState({ isShowListMCK: false, urlSuccess, btnCapNhatClick })
            }, 500);
        })
    }

    callbacSuccess = () => {
        this.setState({ isDangKySo: false }, () => {
            setTimeout(() => {
                this.setState({ isShowListMCK: false }, () => {
                    setTimeout(() => {
                        this.props.toggleModalWebView()
                        setTimeout(() => {
                            this.props.onSuccessMobilePKI()
                        }, 500);
                    }, 500);
                })
            }, 500);
        })
    }

    callbackFailed = () => {
        this.setState({ isDangKySo: false }, () => {
            setTimeout(() => {
                this.setState({ isShowListMCK: false }, () => {
                    setTimeout(() => {
                        Alert.alert("Thông báo", "Xảy ra lỗi. Vui lòng thử lại sau!", [{ text: "Đóng", style: "cancel" },], { cancelable: false })
                    }, 500);
                })
            }, 1500);
        })
    }
    //End region Callback

    //region Footer Button
    renderFooterButton = () => {
        if (this.state.accessTokenHSM || this.state.accessTokenSmartCA) {
            if (this.state.isDangKySo && Platform.OS === "ios") {
                return (
                    <View style={{ position: 'absolute', zIndex: 99, height: 60, left: 0, right: 0, bottom: 0, flexDirection: "row", justifyContent: "center", paddingTop: 5, alignItems: "center" }}>
                        <Spinner /><Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>  Đang thực hiện...</Text>
                    </View>
                )
            } else {
                return (
                    <View style={{ position: 'absolute', zIndex: 99, height: 60, left: 0, right: 0, bottom: 0, flexDirection: "row", justifyContent: "space-evenly", paddingTop: 5, alignItems: "center" }}>
                        <Button bordered rounded iconLeft onPress={this.toggleModalMCK}><Icon name="close" type="MaterialIcons"></Icon><Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}> Đóng    </Text></Button>
                    </View>
                )
            }
        } else {
            return (
                <View style={{ position: 'absolute', zIndex: 99, height: 60, left: 0, right: 0, bottom: 0, flexDirection: "row", justifyContent: "space-evenly", paddingTop: 5, alignItems: "center" }}>
                    <Button bordered rounded iconLeft onPress={this.toggleModalMCK}><Icon name="close" type="MaterialIcons"></Icon><Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}> Đóng    </Text></Button>
                </View>
            )
        }
    }
    //End region Footer Button

    //region HSM Button
    onPressDangNhapHSM = () => {
        let state = this.state
        if (state.txtMatKhauHSM && state.txtTaiKhoanHSM) {
            API.KYSO.AU_KYSO_GATH(state.txtTaiKhoanHSM, state.txtMatKhauHSM).then((response) => {
                if (response !== "") {
                    response = JSON.parse(response)
                    let today = new Date()
                    let obj = new Object()
                    obj.access_token = response.access_token
                    obj.expire_time = today.getTime() + response.expire_time
                    global.accessTokenHSM = obj
                    if (this.state.listMauChuKy.length === 1) {
                        this.props.onHSM_Location(this.infoCA, this.state.listMauChuKy[0].link_cks)
                    } else {
                        this.setState({ accessTokenHSM: obj })
                    }
                } else {
                    Alert.alert("Thông báo", "Vui lòng kiểm tra lại thông tin!", [{ text: "Đóng", style: "cancel" },], { cancelable: false })
                }
            })
        } else {
            Alert.alert("Thông báo", "Vui lòng nhập đầy đủ thông tin!", [{ text: "Đóng", style: "cancel" },], { cancelable: false })
        }
    }
    //End region HSM

    //region SmartCA Button
    getSmartCAAuthUrl = () => {
        try {
            let url = global.cks_smartca_url + "/auth/authorize"
            url += "?response_type=code"
            url += "&client_id=" + global.cks_smartca_client_id
            url += "&redirect_uri=" + this.SmartCARedirctURL
            url += "&scope=sign offline_access"
            url += "&state=" + global.ma_ctcb_kc
            console.log(url)
            return url
        } catch (error) {
            return ""
        }
    }

    completeLoginSmartCA = () => {
        setTimeout(() => {
            API.KYSO.AU_KYSO_SMARTCA_GATD().then((response) => {
                if (response !== "") {
                    let today = new Date()
                    let obj = new Object()
                    try {
                        var decoded = jwt_decode(response)
                        if(decoded.exp != null){
                            var localeTime = new Date(decoded.exp * 1000 + new Date().getTimezoneOffset()*60);
                            var timeSmartCa = localeTime.getTime();
                            if(timeSmartCa > today.getTime()){
                                obj.access_token = response
                                obj.expire_time = today.getTime() + 3600000
                                global.accessTokenSmartCA = obj
                                AsyncStorage.setItem('accessTokenSmartCA', JSON.stringify(obj));
                                this.setState({ accessTokenSmartCA: obj })
                            }
                            else{
                                this.completeLoginSmartCA();
                            }
                        } 
                    } catch (error) {
                        Alert.alert("Thông báo", error.message, [{ text: "Đóng", style: "cancel" },], { cancelable: false })
                    }
                    
                    // obj.access_token = response
                    // obj.expire_time = today.getTime() + 3000
                    // global.accessTokenSmartCA = obj
                    // this.setState({ accessTokenSmartCA: obj });
                } else {
                    Alert.alert("Thông báo", "Vui lòng kiểm tra lại thông tin!", [{ text: "Đóng", style: "cancel" },], { cancelable: false })
                }
            })
        }, 1000);
    }
    //End region SmartCA 

    renderSignType = () => {
        if (this.props.kySoHSM) {
            return (
                //Ký số HSM
                <View style={{ flex: 1, backgroundColor: "#FFFFFF", borderRadius: 10 }}>
                    <ScrollView style={{ top: 0, marginBottom: 60 }}>
                        <CardItem style={{ height: 55, justifyContent: "center", padding: 4, alignItems: "center" }}>
                            <Text style={{ fontSize: this.props.fontSize.FontSizeXLarge, textAlign: "center", color: "black" }}>Ký số</Text>
                        </CardItem>
                        {
                            (this.loaiKySo !== "auto" && !this.state.accessTokenHSM) && (
                                <CardItem style={{ flexDirection: "column", marginTop: 20, marginBottom: 20, padding: 4, alignItems: "flex-start" }}>
                                    <Text style={{ fontSize: this.props.fontSize.FontSizeNorman, color: "gray", margin: 5 }}>Tài khoản hệ thống HSM</Text>
                                    <TextInput
                                        ref={(ref) => { this.txtTaiKhoanHSM = ref }}
                                        style={{ width: "100%", height: 45, fontSize: this.props.fontSize.FontSizeNorman / PixelRatio.getFontScale(), borderWidth: 0.8, borderRadius: 22, paddingLeft: 10, paddingRight: 10 }}
                                        value={this.state.txtTaiKhoanHSM}
                                        onChangeText={(text) => { this.setState({ txtTaiKhoanHSM: text }) }}
                                        onSubmitEditing={() => { this.txtMatKhauHSM.focus() }}
                                    />
                                    <Text style={{ fontSize: this.props.fontSize.FontSizeNorman, color: "gray", margin: 5 }}>Mật khẩu hệ thống HSM</Text>
                                    <TextInput
                                        ref={(ref) => { this.txtMatKhauHSM = ref }}
                                        style={{ width: "100%", height: 45, fontSize: this.props.fontSize.FontSizeNorman / PixelRatio.getFontScale(), borderWidth: 0.8, borderRadius: 22, paddingLeft: 10, paddingRight: 10 }}
                                        secureTextEntry={true}
                                        value={this.state.txtMatKhauHSM}
                                        onChangeText={(text) => { this.setState({ txtMatKhauHSM: text }) }}
                                    />
                                    <Button bordered rounded iconLeft onPress={this.onPressDangNhapHSM} style={{ marginTop: 15, marginBottom: 5, alignSelf: "center" }}><Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>Đăng nhập</Text></Button>
                                </CardItem>
                            )
                        }
                        {
                            (this.state.accessTokenHSM) && (
                                <CardItem style={{ justifyContent: "center", padding: 4, alignItems: "center", flexDirection: "column" }}>
                                    <Text style={{ fontSize: this.props.fontSize.FontSizeLarge, textAlign: "center", color: "black" }}>Danh sách mẫu chữ ký</Text>
                                    <Text style={{ fontSize: this.props.fontSize.FontSizeNorman, color: "gray" }}>(Ấn vào mẫu chữ ký để bắt đầu ký)</Text>
                                </CardItem>
                            )
                        }
                        {
                            (this.state.accessTokenHSM) && (this.renderListMauChuKy())
                        }
                    </ScrollView>
                    {this.renderFooterButton()}
                </View>
            )
        } else if (this.props.kySoSmartCA) {
            return (
                //Ký số SmartCA
                <View style={{ flex: 1, backgroundColor: "#FFFFFF", borderRadius: 10 }}>
                    <CardItem style={{ height: 55, justifyContent: "center", padding: 4, alignItems: "center" }}>
                        <Text style={{ fontSize: this.props.fontSize.FontSizeXLarge, textAlign: "center", color: "black" }}>Ký số</Text>
                    </CardItem>
                    {
                        (this.loaiKySo !== "auto" && !this.state.accessTokenSmartCA) && (
                            <CardItem style={{ flex: 1, marginTop: 10, marginBottom: 70, alignItems: "flex-start" }}>
                                <WebView
                                    ref={ref => { this.webViewLoginSmartCA = ref }}
                                    style={{ width: "100%", height: "100%" }}
                                    source={{ uri: this.getSmartCAAuthUrl() }}
                                    javaScriptEnabled={true}
                                    domStorageEnabled={true}
                                    decelerationRate="normal"
                                    mixedContentMode="always"
                                    originWhitelist={["*"]}
                                    startInLoadingState={true}
                                    scalesPageToFit={true}
                                    bounces={false}
                                    onNavigationStateChange={(event) => {
                                        if (event.url) {
                                            if (event.url.startsWith(this.SmartCARedirctURL)) {
                                                setTimeout(() => {
                                                    this.webViewLoginSmartCA.stopLoading()
                                                    this.completeLoginSmartCA()
                                                }, 1000);
                                            }
                                        }
                                        console.log("onNavigationStateChange: " + JSON.stringify(event))
                                    }}
                                />
                            </CardItem>
                        )
                    }
                    {
                        (this.state.accessTokenSmartCA) && (
                            <CardItem style={{ justifyContent: "center", padding: 4, alignItems: "center", flexDirection: "column" }}>
                                <Text style={{ fontSize: this.props.fontSize.FontSizeLarge, textAlign: "center", color: "black" }}>Danh sách mẫu chữ ký</Text>
                                <Text style={{ fontSize: this.props.fontSize.FontSizeNorman, color: "gray" }}>(Ấn vào mẫu chữ ký để bắt đầu ký)</Text>
                            </CardItem>
                        )
                    }
                    {
                        (this.state.accessTokenSmartCA) && (this.renderListMauChuKy())
                    }
                    {this.renderFooterButton()}
                </View>
            )
        } else {
            return (
                //Ký số Mobile PKI
                <View style={{ flex: 1, backgroundColor: "#FFFFFF", borderRadius: 10 }}>
                    <ScrollView style={{ top: 0, marginBottom: 60 }}>
                        {
                            (this.loaiKySo === "auto") ? (
                                <CardItem style={{ height: 55, justifyContent: "center", padding: 4, alignItems: "center" }}>
                                    <Text style={{ fontSize: this.props.fontSize.FontSizeXLarge, textAlign: "center", color: "black" }}>Ký số tự động</Text>
                                </CardItem>
                            ) : (
                                <CardItem style={{ height: 55, justifyContent: "center", padding: 4, alignItems: "center" }}>
                                    <Text style={{ fontSize: this.props.fontSize.FontSizeXLarge, textAlign: "center", color: "black" }}>Ký số vị trí</Text>
                                </CardItem>
                            )
                        }
                        <CardItem style={{ justifyContent: "center", padding: 4, alignItems: "center", flexDirection: "column" }}>
                            <Text style={{ fontSize: this.props.fontSize.FontSizeLarge, textAlign: "center", color: "black" }}>Danh sách mẫu chữ ký</Text>
                            <Text style={{ fontSize: this.props.fontSize.FontSizeNorman, color: "gray" }}>(Ấn vào mẫu chữ ký để bắt đầu ký)</Text>
                        </CardItem>
                        {
                            this.renderListMauChuKy()
                        }
                    </ScrollView>
                    {
                        (this.state.isDangKySo && Platform.OS === "ios") ? (
                            <View style={{ position: 'absolute', zIndex: 99, height: 60, left: 0, right: 0, bottom: 0, flexDirection: "row", justifyContent: "center", paddingTop: 5, alignItems: "center" }}>
                                <Spinner /><Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>  Đang thực hiện...</Text>
                            </View>
                        ) : (
                            <View style={{ position: 'absolute', zIndex: 99, height: 60, left: 0, right: 0, bottom: 0, flexDirection: "row", justifyContent: "space-evenly", paddingTop: 5, alignItems: "center" }}>
                                <Button bordered rounded iconLeft onPress={this.toggleModalMCK}><Icon name="close" type="MaterialIcons"></Icon><Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}> Đóng    </Text></Button>
                            </View>
                        )
                    }
                </View>
            )
        }

    }

    render() {
        let Constants = {
            javascript: {
                injection: `
                    setInterval(function(){ 
                        let data=getPdfData()
                        window.ReactNativeWebView.postMessage(data)
                    }, 500);
                `
            }
        }
        Constants.javascript.injection += "setWidthHeightRec(" + global.US_Mobile_Pki_Width_Rec + "," + global.US_Mobile_Pki_Height_Rec + ");"

        if (this.state.primaryImage !== "") {
            //Constants.javascript.injection += "setUrlImage('" + global.AU_ROOT + "/api/file-manage/read-advance?path=" + this.state.primaryImage + "');"
            Constants.javascript.injection += "setUrlImage('" + getURLFile(this.state.primaryImage) + "');"
        }

        this.styles = StyleSheet.create({
            CardItemButton: {
                //marginTop: isIphoneX() ? 80 : 50,
                backgroundColor: "rgb(50, 54, 57)",
                flexDirection: 'row',
                flexWrap: "wrap",
                justifyContent: "center",
                alignItems: "center",
                height: 40,
            },
            Button: {
                margin: 3,
                justifyContent: "center",
                alignItems: "center"
            },
            ButtonText: {
                fontSize: this.props.fontSize.FontSizeSmall,
                textAlign: "center"
            },
            headerTitleStyle: {
                color: AppConfig.defaultTextColor,
                fontSize: this.props.fontSize.FontSizeLarge
            },
            headerIconStyle: {
                color: AppConfig.defaultTextColor
            },
        })

        let { isShowWebView, toggleModalWebView, title, kyCA, kySoHSM, kySoSmartCA } = this.props
        let { isShowListMCK, isDangKySo, btnCapNhatClick } = this.state
        const paddingTop = Platform.OS === "ios" ? 15 : 0
        const paddingLeft = 10
        const isCA = (title.indexOf(".pdf") !== -1 || title.indexOf(".doc") !== -1) && (kyCA) ? true : false

        if (!isCA) {
            Constants.javascript.injection = ""
        }
        console.log(Constants.javascript.injection)
        if (isShowWebView) {
            return (
                <Modal
                    transparent={false}
                    onRequestClose={() => toggleModalWebView()}
                    onShow={() => this.convertWordToPdf()}
                    visible={isShowWebView}
                    supportedOrientations={["landscape", "landscape-left", "landscape-right", "portrait", "portrait-upside-down"]}
                >

                    <View style={{ flex: 1, backgroundColor: Platform.OS === 'ios' ? "#000000" : AppConfig.statusBarColor }}>
                        <MyStatusBar backgroundColor={Platform.OS === 'ios' ? "#000000" : AppConfig.statusBarColor} skipAndroid={true} barStyle="light-content" />
                        <SafeAreaView style={{ zIndex: 999, backgroundColor: Platform.OS === 'ios' ? "#000000" : AppConfig.statusBarColor, flex: 1 }}>
                            <View style={{ zIndex: 999, backgroundColor: "blue", flex: 1 }}>
                                <ModalDangThucHien
                                    isVisible={isDangKySo}
                                    title={"Đang thực hiện"}
                                />
                                <Modal
                                    transparent={false}
                                    animationType="none"
                                    visible={isShowListMCK}
                                    supportedOrientations={["landscape", "landscape-left", "landscape-right", "portrait", "portrait-upside-down"]}
                                >
                                    <View style={{ flex: 1, padding: 15, backgroundColor: AppConfig.grayBackground, zIndex: 99 }}>
                                        {this.renderSignType()}
                                    </View>
                                </Modal>
                                <Header hasTabs androidStatusBarColor={AppConfig.statusBarColor} iosBarStyle={"light-content"}>
                                    <Grid style={{ flex: 1, justifyContent: "center", alignItems: "center", paddingTop, paddingLeft }}>
                                        <Col style={{ width: "10%" }}>
                                            {
                                                (btnCapNhatClick === null) ? <Icon name="arrow-back" type="MaterialIcons" onPress={() => toggleModalWebView()} style={this.styles.headerIconStyle} /> :
                                                    <Icon name="arrow-back" type="MaterialIcons" onPress={() => { this.setState({ urlSuccess: "", btnCapNhatClick: null }) }} style={this.styles.headerIconStyle} />
                                            }
                                        </Col>
                                        <Col style={{ justifyContent: "center", alignItems: "center" }}>
                                            <Text style={this.styles.headerTitleStyle} numberOfLines={1} ellipsizeMode="middle">{title}</Text>
                                        </Col>
                                        {
                                            isCA ? (
                                                <Col style={{ width: "15%", paddingRight: 10, alignItems: "flex-end", flexDirection: "row" }}>
                                                    {
                                                        (btnCapNhatClick === null) ? (kySoHSM === false && kySoSmartCA === false) && <Icon name="vpn-key" type="MaterialIcons" onPress={() => this.toggleModalMCK("auto", {})} style={this.styles.headerIconStyle} /> :
                                                            <Icon name="done" type="MaterialIcons" onPress={() => { this.state.btnCapNhatClick ? this.state.btnCapNhatClick() : null }} style={this.styles.headerIconStyle} />
                                                    }
                                                </Col>
                                            ) : (
                                                <Col style={{ width: 90, paddingRight: 10, alignItems: "center", justifyContent: "center", flexDirection: "row" }}>
                                                    <TouchableOpacity onPress={() => {
                                                        if (Platform.OS === "ios") {
                                                            toggleModalWebView()
                                                            setTimeout(() => {
                                                                downloadFile(this.props.title, this.props.pathFile)
                                                            }, 300);
                                                        } else {
                                                            downloadFile(this.props.title, this.props.pathFile)
                                                        }
                                                    }}>
                                                        <Icon name={"cloud-download"} type="Ionicons" style={this.styles.headerIconStyle} />
                                                    </TouchableOpacity>
                                                    <Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>&nbsp;&nbsp;&nbsp;</Text>
                                                    <TouchableOpacity onPress={() => {
                                                        if (Platform.OS === "ios") {
                                                            toggleModalWebView()
                                                            setTimeout(() => {
                                                                downloadFile(this.props.title, this.props.pathFile, true)
                                                            }, 300);
                                                        } else {
                                                            downloadFile(this.props.title, this.props.pathFile, true)
                                                        }
                                                    }}>
                                                        <Icon name={"share"} type="MaterialIcons" style={this.styles.headerIconStyle} />
                                                    </TouchableOpacity>
                                                </Col>
                                            )
                                        }
                                    </Grid>
                                </Header>

                                {
                                    (isCA) && (
                                        (btnCapNhatClick === null) ?
                                            <CardItem style={[this.styles.CardItemButton]}>
                                                <Button small success style={this.styles.Button} onPress={() => this.postMessage("pressAdd")}><Text style={this.styles.ButtonText}>Chọn vị trí</Text></Button>
                                                <Button small danger style={this.styles.Button} onPress={() => this.postMessage("pressDelete")}><Text style={this.styles.ButtonText}>Xóa</Text></Button>
                                                {
                                                    (kySoHSM || kySoSmartCA) ? (
                                                        <Button small primary style={this.styles.Button} onPress={() => this.postMessage("pressSignHSM")}><Text style={this.styles.ButtonText}>Ký số</Text></Button>
                                                    ) : (
                                                        <Button small primary style={this.styles.Button} onPress={() => this.postMessage("pressSign")}><Text style={this.styles.ButtonText}>Ký số vị trí</Text></Button>
                                                    )
                                                }
                                            </CardItem> :
                                            <CardItem style={[this.styles.CardItemButton, { height: 0 }]}></CardItem>
                                    )
                                }
                                <View style={{ flex: 1, zIndex: 0 }}>
                                    {
                                        (btnCapNhatClick !== null) ? (
                                            <WebView
                                                ref={WebView => { this.WebView = WebView }}
                                                style={{ flex: 1 }}
                                                source={{ uri: this.state.urlSuccess }}
                                                javaScriptEnabled={true}
                                                domStorageEnabled={true}
                                                decelerationRate="normal"
                                                mixedContentMode="always"
                                                originWhitelist={["*"]}
                                                startInLoadingState={true}
                                                automaticallyAdjustContentInsets={true}
                                                scalesPageToFit={true}
                                                bounces={false}
                                                onError={_ => { console.log("error") }}
                                            />
                                        ) : (
                                            <WebView
                                                ref={WebView => { this.WebView = WebView }}
                                                style={{ flex: 1 }}
                                                source={{ uri: this.state.urlSuccess !== "" ? this.state.urlSuccess : this.state.url }}
                                                javaScriptEnabled={true}
                                                domStorageEnabled={true}
                                                decelerationRate="normal"
                                                mixedContentMode="always"
                                                originWhitelist={["*"]}
                                                startInLoadingState={true}
                                                automaticallyAdjustContentInsets={true}
                                                scalesPageToFit={true}
                                                bounces={false}
                                                onError={_ => { console.log("error") }}
                                                injectedJavaScript={Constants.javascript.injection}
                                                onMessage={event => {
                                                    let arrDataInPut = event.nativeEvent.data
                                                    if (arrDataInPut !== "" && !this.hasData) {
                                                        this.hasData = true
                                                        let objRec = null
                                                        let infoCA = new Object
                                                        let arrData = JSON.parse(arrDataInPut)
                                                        for (i = 0; i < arrData.length; i++) {
                                                            if (arrData[i].objects.length > 0) {
                                                                objRec = arrData[i]
                                                                infoCA.page = i + 1
                                                                break
                                                            }
                                                        }
                                                        if (objRec !== null) {
                                                            let objects = objRec.objects[0]
                                                            let backgroundImage = objRec.backgroundImage
                                                            //x=width, left
                                                            //y=height, top
                                                            infoCA.scale = 1
                                                            infoCA.pageWidth = backgroundImage.width / infoCA.scale
                                                            infoCA.pageHeight = backgroundImage.height / infoCA.scale

                                                            objects.width = (objects.width * objects.scaleX) / infoCA.scale
                                                            objects.height = (objects.height * objects.scaleY) / infoCA.scale

                                                            objects.top = objects.top / infoCA.scale
                                                            objects.left = objects.left / infoCA.scale


                                                            infoCA.llx = objects.left
                                                            infoCA.lly = infoCA.pageHeight - (objects.top + objects.height)

                                                            infoCA.urx = infoCA.llx + objects.width
                                                            infoCA.ury = infoCA.lly + objects.height

                                                            this.toggleModalMCK("location", infoCA)
                                                            this.hasData = false
                                                        } else {
                                                            Alert.alert("Thông báo", "Vui lòng chọn vị trí", [{
                                                                text: "Đóng", style: "cancel", onPress: () => {
                                                                    setTimeout(() => {
                                                                        this.hasData = false
                                                                    }, 500);
                                                                }
                                                            }], { cancelable: false })
                                                        }
                                                    }
                                                }}
                                            />
                                        )
                                    }
                                    {
                                        (this.props.fabActions && !this.props.kyCA) && (
                                            <FloatingAction
                                                fabActions={this.props.fabActions}
                                                fabPressItem={(name) => { this.props.fabPressItem(name) }}
                                            />
                                        )
                                    }
                                </View>
                            </View>
                        </SafeAreaView>
                    </View>
                </Modal>
            )
        } else {
            return null
        }
    }
}

