import React, { Component } from 'react';
import { ScrollView, StyleSheet, Image, TouchableOpacity, Platform, Dimensions } from "react-native"
import { Text, Icon } from "native-base"
import { connect } from "react-redux"
import { AppConfig } from "../AppConfig"
import { getFileChiTietTTDH } from "../untils/TextUntils"
import ModalWebView from "./ModalWebView"
import { downloadFileToView } from "../untils/DownloadFile"

class ViewFile extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isModalVisible: false,
            isChangeSize: false
        }
        this.urlFile = ""
        this.fileName = ""
        this.path = ""
        this.isLoading = false
    }

    componentDidMount = async () => {
        Dimensions.addEventListener("change", this.onChangeSize);
    }

    componentWillUnmount() {
        Dimensions.removeEventListener("change", this.onChangeSize);
    }

    onChangeSize = () => {
        this.setState({ isChangeSize: !this.state.isChangeSize })
    }

    renderListFile = (styles) => {
        let result = []
        let src_file = this.props.src_file
        if (src_file !== null && src_file !== "" && src_file !== "null") {
            let data = getFileChiTietTTDH(src_file.split(":"))
            for (let item of data) {
                let itemTemp = item.split("|")
                if (itemTemp[0] !== "") {
                    if (Platform.OS === "ios" && (itemTemp[2] === "doc" || itemTemp[2] === "docx")) {
                        result.push(
                            <TouchableOpacity key={item} style={styles.viewFile} onPress={_ => {
                                downloadFileToView(itemTemp[1], itemTemp[4])
                            }}>
                                <Image style={styles.iconFile} source={{ uri: itemTemp[3] }} />
                                <Text numberOfLines={1} ellipsizeMode="tail" style={styles.textTenFile}>{itemTemp[1]}</Text>
                                <Icon name="close" type="MaterialIcons" style={{ color: "red", fontSize: this.props.fontSize.FontSizeXXXLarge + 5 }} onPress={() => {
                                    this.props.onDeleteFile(itemTemp[4])
                                }} ></Icon>
                            </TouchableOpacity>
                        )
                    } else {
                        result.push(
                            <TouchableOpacity key={item} style={styles.viewFile} onPress={_ => {
                                if (global.US_Xem_Pdf_iOS === "Offline") {
                                    downloadFileToView(itemTemp[1], itemTemp[4])
                                    return null;
                                }
                                this.urlFile = itemTemp[0]
                                this.path = itemTemp[4]
                                this.fileName = itemTemp[1]
                                this.toggleModal()
                            }}>
                                <Image style={styles.iconFile} source={{ uri: itemTemp[3] }} />
                                <Text numberOfLines={1} ellipsizeMode="tail" style={styles.textTenFile}>{itemTemp[1]}</Text>
                                <Icon name="close" type="MaterialIcons" style={{ color: "red", fontSize: this.props.fontSize.FontSizeXXXLarge + 5 }} onPress={() => {
                                    this.props.onDeleteFile(itemTemp[4])
                                }} ></Icon>
                            </TouchableOpacity>
                        )
                    }
                } else {
                    result.push(
                        <TouchableOpacity key={item} style={styles.viewFile} onPress={_ => {
                            downloadFileToView(itemTemp[1], itemTemp[4])
                        }}>
                            <Image style={styles.iconFile} source={{ uri: itemTemp[3] }} />
                            <Text numberOfLines={1} ellipsizeMode="tail" style={styles.textTenFile}>{itemTemp[1]}</Text>
                            <Icon name="close" type="MaterialIcons" style={{ color: "red", fontSize: this.props.fontSize.FontSizeXXXLarge + 5 }} onPress={() => {
                                this.props.onDeleteFile(itemTemp[4])
                            }} ></Icon>
                        </TouchableOpacity>
                    )
                }
            }
        }
        return result
    }

    toggleModal = () => {
        if (!this.state.isModalVisible) {
            this.setState({ isModalVisible: true })
        } else {
            this.setState({ isModalVisible: false })
        }
    }

    render() {
        const { width } = Dimensions.get("screen")
        const stylesNgang = StyleSheet.create({
            viewFile: {
                maxWidth: 250,
                height: 38,
                flexDirection: "row",
                alignItems: "center",
                margin: 2,
                padding: 2,
                borderRadius: 6,
                borderWidth: 0.5,
                borderColor: AppConfig.defaultLineColor
            },
            iconFile: {
                height: 25,
                width: 25
            },
            textTenFile: {
                paddingLeft: 3,
                maxWidth: 180,
                fontSize: this.props.fontSize.FontSizeNorman
            },
        })

        const stylesDoc = StyleSheet.create({
            viewFile: {
                flexDirection: "row",
                alignItems: "center",
                marginTop: 3,
                marginBottom: 3,
            },
            iconFile: {
                height: 25,
                width: 25
            },
            textTenFile: {
                paddingLeft: 3,
                paddingRight: 3,
                maxWidth: width - 115,
                fontSize: this.props.fontSize.FontSizeNorman
            },
        })

        let loaiHienThiFile = parseInt(global.US_FILE_List_Ngang_Doc) //1:Ngang || 2: Dọc
        let styles = loaiHienThiFile === 1 ? stylesNgang : stylesDoc
        let flag = loaiHienThiFile === 1 ? true : false

        return (
            <ScrollView horizontal={flag} scrollEnabled={flag}>
                {
                    (this.state.isModalVisible) && (
                        <ModalWebView
                            isModalVisible={this.state.isModalVisible}
                            title={this.fileName}
                            url={this.urlFile}
                            path={this.path}
                            isUpdateViewCV={this.props.isUpdateViewCV}
                            updateViewCV={this.props.updateViewCV}
                            toggleModal={() => this.toggleModal()}
                            fabActions={this.props.fabActions}
                            fabPressItem={this.props.fabPressItem}
                        />
                    )
                }
                {this.renderListFile(styles)}
            </ScrollView>
        )
    }
}

function mapStateToProps(state) {
    return {
        fontSize: state.fontSize,
    }
}

export default connect(mapStateToProps)(ViewFile)