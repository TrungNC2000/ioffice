import React, { PureComponent } from "react"
import { Keyboard, StyleSheet } from "react-native"
import { Button, Footer, FooterTab as NBFooterTab } from "native-base"
import { connect } from "react-redux"
import Icon from "react-native-vector-icons/FontAwesome5"
import { AppConfig } from "../../AppConfig"
import { resetStack } from "../../untils/TextUntils"

class FooterTab extends PureComponent {

    constructor(props) {
        super(props)
        this.state = {
            showFooter: true
        }
    }

    componentWillMount() {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this));
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this));
    }

    componentWillUnmount() {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }

    _keyboardDidShow() {
        this.setState({ showFooter: false });
    }

    _keyboardDidHide() {
        this.setState({ showFooter: true });
    }

    resetStack = (screen) => {
        if (screen === "Home") {
            this.props.navigation.dispatch(resetStack(screen))
        } else {
            this.props.navigation.push(screen)
        }
    }

    render() {

        this.styles = StyleSheet.create({
            container: {
                flex: 1
            },
            headerTitleStyle: {
                color: AppConfig.defaultTextColor,
                fontSize: this.props.fontSize.FontSizeLarge
            },
            headerIconStyle: {
                color: AppConfig.defaultTextColor
            },
            footerTabSelectedStyle: {
                color: AppConfig.defaultTextColor,
                opacity: 1,
                fontSize: this.props.fontSize.FontSizeXLarge
            },
            footerTabNoSelectedStyle: {
                color: AppConfig.defaultTextColor,
                opacity: 0.6,
                fontSize: this.props.fontSize.FontSizeXLarge
            },
            drawerIcon: {
                color: AppConfig.blueBackground,
                fontSize: this.props.fontSize.FontSizeLarge
            },
        });

        const tabActive = this.props.tabActive
        let VBDen = global.lanh_dao === 1 ? "VBDenDuyet" : "VBDenXuLy"
        let VBDi = global.lanh_dao === 1 ? "VBDiDuyet" : "VBDiXuLy"
    if (this.state.showFooter) {
      if (global.AU_ROOT.includes("nghean-api")) {
        return (
          <Footer>
            <NBFooterTab>
              <Button
                vertical
                active
                onPress={() => {
                  this.resetStack("Home");
                }}
                style={{
                  backgroundColor:
                    tabActive === "trangchu" ? "transparent" : "none",
                }}
              >
                <Icon
                  name="home"
                  style={
                    tabActive === "trangchu"
                      ? this.styles.footerTabSelectedStyle
                      : this.styles.footerTabNoSelectedStyle
                  }
                />
              </Button>
              <Button
                vertical
                onPress={() => {
                  this.resetStack(VBDen);
                }}
                style={{
                  backgroundColor:
                    tabActive === "vbden" ? "transparent" : "none",
                }}
              >
                <Icon
                  name="arrow-alt-circle-down"
                  style={
                    tabActive === "vbden"
                      ? this.styles.footerTabSelectedStyle
                      : this.styles.footerTabNoSelectedStyle
                  }
                />
              </Button>
              <Button
                vertical
                onPress={() => {
                  this.resetStack(VBDi);
                }}
                style={{
                  backgroundColor:
                    tabActive === "vbdi" ? "transparent" : "none",
                }}
              >
                <Icon
                  name="arrow-alt-circle-up"
                  style={
                    tabActive === "vbdi"
                      ? this.styles.footerTabSelectedStyle
                      : this.styles.footerTabNoSelectedStyle
                  }
                />
              </Button>
              <Button
                vertical
                onPress={() => {
                  this.resetStack("TTDH");
                }}
                style={{
                  backgroundColor:
                    tabActive === "ttdh" ? "transparent" : "none",
                }}
              >
                <Icon
                  name="tasks"
                  style={
                    tabActive === "ttdh"
                      ? this.styles.footerTabSelectedStyle
                      : this.styles.footerTabNoSelectedStyle
                  }
                />
              </Button>
              <Button
                vertical
                onPress={() => {
                  this.resetStack("VBNB");
                }}
                style={{
                  backgroundColor:
                    tabActive === "vbnb" ? "transparent" : "none",
                }}
              >
                <Icon
                  name="exchange-alt"
                  style={
                    tabActive === "vbnb"
                      ? this.styles.footerTabSelectedStyle
                      : this.styles.footerTabNoSelectedStyle
                  }
                />
              </Button>
              <Button
                vertical
                onPress={() => {
                  this.resetStack("XYKTT");
                }}
                style={{
                  backgroundColor:
                    tabActive === "xyktt" ? "transparent" : "none",
                }}
              >
                <Icon
                  name="comments"
                  style={
                    tabActive === "xyktt"
                      ? this.styles.footerTabSelectedStyle
                      : this.styles.footerTabNoSelectedStyle
                  }
                />
              </Button>
              <Button
                vertical
                onPress={() => {
                  this.resetStack("LCT");
                }}
                style={{
                  backgroundColor: tabActive === "lct" ? "transparent" : "none",
                }}
              >
                <Icon
                  name="calendar-alt"
                  style={
                    tabActive === "lct"
                      ? this.styles.footerTabSelectedStyle
                      : this.styles.footerTabNoSelectedStyle
                  }
                />
              </Button>
            </NBFooterTab>
          </Footer>
        );
      } else {
			if (tabActive === "trangchu") {
                return (
                    <Footer>
                        <NBFooterTab>
                            <Button vertical active onPress={() => { this.resetStack("Home") }} style={{ backgroundColor: "transparent" }}>
                                <Icon name="home" style={this.styles.footerTabSelectedStyle} />
                            </Button>
                            <Button vertical onPress={() => { this.resetStack(VBDen) }}>
                                <Icon name="arrow-alt-circle-down" style={this.styles.footerTabNoSelectedStyle} />
                            </Button>
                            <Button vertical onPress={() => { this.resetStack(VBDi) }}>
                                <Icon name="arrow-alt-circle-up" style={this.styles.footerTabNoSelectedStyle} />
                            </Button>
                            <Button vertical onPress={() => { this.resetStack("TTDH") }}>
                                <Icon name="tasks" style={this.styles.footerTabNoSelectedStyle} />
                            </Button>
                            <Button vertical onPress={() => { this.resetStack("VBNB") }}>
                                <Icon name="exchange-alt" style={this.styles.footerTabNoSelectedStyle} />
                            </Button>
                            <Button vertical onPress={() => { this.resetStack("LCT") }}>
                                <Icon name="calendar-alt" style={this.styles.footerTabNoSelectedStyle} />
                            </Button>
                        </NBFooterTab>
                    </Footer>
                )
            } else if (tabActive === "vbden") {
                return (
                    <Footer>
                        <NBFooterTab>
                            <Button vertical onPress={() => { this.resetStack("Home") }}>
                                <Icon name="home" style={this.styles.footerTabNoSelectedStyle} />
                            </Button>
                            <Button vertical active onPress={() => { this.resetStack(VBDen) }} style={{ backgroundColor: "transparent" }}>
                                <Icon name="arrow-alt-circle-down" style={this.styles.footerTabSelectedStyle} />
                            </Button>
                            <Button vertical onPress={() => { this.resetStack(VBDi) }}>
                                <Icon name="arrow-alt-circle-up" style={this.styles.footerTabNoSelectedStyle} />
                            </Button>
                            <Button vertical onPress={() => { this.resetStack("TTDH") }}>
                                <Icon name="tasks" style={this.styles.footerTabNoSelectedStyle} />
                            </Button>
                            <Button vertical onPress={() => { this.resetStack("VBNB") }}>
                                <Icon name="exchange-alt" style={this.styles.footerTabNoSelectedStyle} />
                            </Button>
                            <Button vertical onPress={() => { this.resetStack("LCT") }}>
                                <Icon name="calendar-alt" style={this.styles.footerTabNoSelectedStyle} />
                            </Button>
                        </NBFooterTab>
                    </Footer>
                )
            } else if (tabActive === "vbdi") {
                return (
                    <Footer>
                        <NBFooterTab>
                            <Button vertical onPress={() => { this.resetStack("Home") }}>
                                <Icon name="home" style={this.styles.footerTabNoSelectedStyle} />
                            </Button>
                            <Button vertical onPress={() => { this.resetStack(VBDen) }}>
                                <Icon name="arrow-alt-circle-down" style={this.styles.footerTabNoSelectedStyle} />
                            </Button>
                            <Button vertical active onPress={() => { this.resetStack(VBDi) }} style={{ backgroundColor: "transparent" }}>
                                <Icon name="arrow-alt-circle-up" style={this.styles.footerTabSelectedStyle} />
                            </Button>
                            <Button vertical onPress={() => { this.resetStack("TTDH") }}>
                                <Icon name="tasks" style={this.styles.footerTabNoSelectedStyle} />
                            </Button>
                            <Button vertical onPress={() => { this.resetStack("VBNB") }}>
                                <Icon name="exchange-alt" style={this.styles.footerTabNoSelectedStyle} />
                            </Button>
                            <Button vertical onPress={() => { this.resetStack("LCT") }}>
                                <Icon name="calendar-alt" style={this.styles.footerTabNoSelectedStyle} />
                            </Button>
                        </NBFooterTab>
                    </Footer>
                )
            } else if (tabActive === "ttdh") {
                return (
                    <Footer>
                        <NBFooterTab>
                            <Button vertical onPress={() => { this.resetStack("Home") }}>
                                <Icon name="home" style={this.styles.footerTabNoSelectedStyle} />
                            </Button>
                            <Button vertical onPress={() => { this.resetStack(VBDen) }}>
                                <Icon name="arrow-alt-circle-down" style={this.styles.footerTabNoSelectedStyle} />
                            </Button>
                            <Button vertical onPress={() => { this.resetStack(VBDi) }}>
                                <Icon name="arrow-alt-circle-up" style={this.styles.footerTabNoSelectedStyle} />
                            </Button>
                            <Button vertical active onPress={() => { this.resetStack("TTDH") }} style={{ backgroundColor: "transparent" }}>
                                <Icon name="tasks" style={this.styles.footerTabSelectedStyle} />
                            </Button>
                            <Button vertical onPress={() => { this.resetStack("VBNB") }}>
                                <Icon name="exchange-alt" style={this.styles.footerTabNoSelectedStyle} />
                            </Button>
                            <Button vertical onPress={() => { this.resetStack("LCT") }}>
                                <Icon name="calendar-alt" style={this.styles.footerTabNoSelectedStyle} />
                            </Button>
                        </NBFooterTab>
                    </Footer>
                )
            } else if (tabActive === "vbnb") {
                return (
                    <Footer>
                        <NBFooterTab>
                            <Button vertical onPress={() => { this.resetStack("Home") }}>
                                <Icon name="home" style={this.styles.footerTabNoSelectedStyle} />
                            </Button>
                            <Button vertical onPress={() => { this.resetStack(VBDen) }}>
                                <Icon name="arrow-alt-circle-down" style={this.styles.footerTabNoSelectedStyle} />
                            </Button>
                            <Button vertical onPress={() => { this.resetStack(VBDi) }}>
                                <Icon name="arrow-alt-circle-up" style={this.styles.footerTabNoSelectedStyle} />
                            </Button>
                            <Button vertical onPress={() => { this.resetStack("TTDH") }}>
                                <Icon name="tasks" style={this.styles.footerTabNoSelectedStyle} />
                            </Button>
                            <Button vertical active onPress={() => { this.resetStack("VBNB") }} style={{ backgroundColor: "transparent" }}>
                                <Icon name="exchange-alt" style={this.styles.footerTabSelectedStyle} />
                            </Button>
                            <Button vertical onPress={() => { this.resetStack("LCT") }}>
                                <Icon name="calendar-alt" style={this.styles.footerTabNoSelectedStyle} />
                            </Button>
                        </NBFooterTab>
                    </Footer>
                )
            } else if (tabActive === "lct") {
                return (
                    <Footer>
                        <NBFooterTab>
                            <Button vertical onPress={() => { this.resetStack("Home") }}>
                                <Icon name="home" style={this.styles.footerTabNoSelectedStyle} />
                            </Button>
                            <Button vertical onPress={() => { this.resetStack(VBDen) }}>
                                <Icon name="arrow-alt-circle-down" style={this.styles.footerTabNoSelectedStyle} />
                            </Button>
                            <Button vertical onPress={() => { this.resetStack(VBDi) }}>
                                <Icon name="arrow-alt-circle-up" style={this.styles.footerTabNoSelectedStyle} />
                            </Button>
                            <Button vertical onPress={() => { this.resetStack("TTDH") }}>
                                <Icon name="tasks" style={this.styles.footerTabNoSelectedStyle} />
                            </Button>
                            <Button vertical onPress={() => { this.resetStack("VBNB") }}>
                                <Icon name="exchange-alt" style={this.styles.footerTabNoSelectedStyle} />
                            </Button>
                            <Button vertical active onPress={() => { this.resetStack("LCT") }} style={{ backgroundColor: "transparent" }}>
                                <Icon name="calendar-alt" style={this.styles.footerTabSelectedStyle} />
                            </Button>
                        </NBFooterTab>
                    </Footer>
                )
            }
      }
    } else {
      return null;
    }
  }
}

function mapStateToProps(state) {
    return {
        fontSize: state.fontSize,
    }
}

export default connect(mapStateToProps)(FooterTab)
