import React, { Component } from "react"
import { View, StatusBar } from "react-native"
import { getStatusBarHeight } from "../../untils/StatusBarHeight"
export default class MyStatusBar extends Component {
    render() {
        return (
            <View style={[{ height: getStatusBarHeight(this.props.skipAndroid || false), backgroundColor: this.props.backgroundColor }]}>
                <StatusBar translucent backgroundColor={this.props.backgroundColor} {...this.props} />
            </View>
        )
    }
}