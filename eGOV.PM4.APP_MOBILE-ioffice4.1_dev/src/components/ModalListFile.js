import React, { PureComponent } from 'react';
import { View, Modal, FlatList, Platform } from "react-native"
import { Icon, Item, Left, Text, Button, } from "native-base"
import { connect } from "react-redux"
import { AppConfig } from "../AppConfig"
import ModalWebView from "./ModalWebView"
import { downloadFileToView } from "../untils/DownloadFile"

class ModalListFile extends PureComponent {
    constructor(props) {
        super(props)
        this.state = {
            isModalVisible: false
        }
        this.urlFile = ""
        this.fileName = ""
        this.path = ""
        this.isLoading = false
    }

    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible })
    }

    itemPress = (urlFile, fileName, extFile, path) => {
        if (urlFile !== "") {
            if (Platform.OS === "ios" && (extFile === "doc" || extFile === "docx")) {
                downloadFileToView(fileName, path)
            } else {
                if (global.US_Xem_Pdf_iOS === "Offline") {
                    downloadFileToView(fileName, path)
                    return null;
                }
                this.urlFile = urlFile
                this.path = path
                this.fileName = fileName
                this.toggleModal()
            }
        } else {
            //alert("Tệp tin chưa hỗ trợ xem trực tuyến")
            downloadFileToView(fileName, path)
        }
    }

    render() {
        return (
            <Modal
                transparent={false}
                animationType="slide"
                visible={this.props.isModalVisible}
                supportedOrientations={["landscape", "landscape-left", "landscape-right", "portrait", "portrait-upside-down"]}
            >
                <View style={{ flex: 1, padding: 15, backgroundColor: AppConfig.grayBackground }}>
                    <View style={{ flex: 1, backgroundColor: "#FFFFFF", borderRadius: 10 }}>
                        <View style={{ position: 'absolute', zIndex: 99, height: 55, left: 0, right: 0, top: 0, justifyContent: "center", padding: 4, alignItems: "center" }}>
                            <Text style={{ fontSize: this.props.fontSize.FontSizeLarge }}>Danh sách tệp tin</Text>
                        </View>
                        <FlatList
                            ref={(ref) => { this.flatListRef = ref }}
                            data={this.props.dataFile}
                            keyExtractor={(item, index) => "key" + item + index}
                            style={{ position: 'absolute', left: 0, right: 0, top: 55, bottom: 55 }}
                            initialNumToRender={50}
                            renderItem={({ item, index }) => {
                                item = item.split("|")
                                return (
                                    <Item onPress={() => this.itemPress(item[0], item[1], item[2], item[4])} key={item} style={{ padding: 12, borderBottomWidth: AppConfig.defaultLineWidth, borderColor: AppConfig.defaultLineColor }}>
                                        <Left>
                                            <Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>{item[1]}</Text>
                                        </Left>
                                    </Item>
                                )
                            }}
                            ListEmptyComponent={() => {
                                return (
                                    <View style={{ alignItems: "center", marginTop: 10, }}>
                                        <Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>Không có dữ liệu</Text>
                                    </View>
                                )
                            }}
                            removeClippedSubviews={true}
                        />
                        <View style={{ position: 'absolute', zIndex: 99, height: 55, left: 0, right: 0, bottom: 0, flexDirection: "row", justifyContent: "space-evenly", paddingTop: 10, alignItems: "center" }}>
                            <Button small bordered rounded iconLeft onPress={_ => this.props.toggleModal()}><Icon name="close" type="MaterialIcons"></Icon><Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>Đóng</Text></Button>
                        </View>
                    </View>
                </View>
                <ModalWebView
                    isModalVisible={this.state.isModalVisible}
                    title={this.fileName}
                    url={this.urlFile}
                    path={this.path}
                    toggleModal={() => this.toggleModal()}
                    isUpdateViewCV={this.props.isUpdateViewCV}
                    updateViewCV={this.props.updateViewCV}
                />
            </Modal>

        )
    }
}

function mapStateToProps(state) {
    return {
        fontSize: state.fontSize,
    }
}

export default connect(mapStateToProps)(ModalListFile)