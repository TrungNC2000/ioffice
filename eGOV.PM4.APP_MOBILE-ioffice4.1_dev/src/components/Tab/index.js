import React, { Component } from 'react';
import { View, StyleSheet, ScrollView } from "react-native"
import { Text, Button, Badge } from "native-base"
import { AppConfig } from "../../AppConfig"

export default class MyTab extends Component {
    constructor(props) {
        super(props)
    }

    renderItem = (values) => {
        if (values.length === 0) return
        let { activeTab, badges, onChangeTab } = this.props
        let result = []

        for (let index = 0; index < values.length; index++) {
            let item = values[index]
            let activeTabStyle = index === 0 ? this.styles.activeTabLeftStyle : index === values.length - 1 ? this.styles.activeTabRightStyle : this.styles.activeTabStyle
            let tabStyle = index === 0 ? this.styles.tabLeftStyle : index === values.length - 1 ? this.styles.tabRightStyle : this.styles.tabStyle
            result.push(
                <Button key={index} style={activeTab === index ? activeTabStyle : tabStyle} onPress={() => onChangeTab(index)}>
                    <Text style={[activeTab === index ? this.styles.activeTabTextStyle : this.styles.tabTextStyle]}>{item}</Text>
                    {
                        (badges[index] !== undefined && badges[index] !== 0) && (
                            <Badge style={{ marginRight: 5, alignSelf: 'center', justifyContent: "center", alignItems: "center" }}>
                                <Text style={[this.styles.activeTabTextStyle]}>{badges[index]}</Text>
                            </Badge>
                        )
                    }
                </Button>
            )
        }
        return result
    }

    render() {

        this.styles = StyleSheet.create({
            tabsContainerStyle: {
                maxWidth: 550
            },
            tabLeftStyle: {
                height: 35,
                backgroundColor: "#FFFFFF",
                borderColor: AppConfig.blueBackground,
                borderTopWidth: 0.8,
                borderBottomWidth: 0.8,
                borderLeftWidth: 0.8,
                borderRightWidth: 0.8,
                borderTopLeftRadius: 8,
                borderBottomLeftRadius: 8,
                borderTopRightRadius: 0,
                borderBottomRightRadius: 0,
                marginLeft: 10,
            },
            tabStyle: {
                height: 35,
                backgroundColor: "#FFFFFF",
                borderColor: AppConfig.blueBackground,
                borderTopWidth: 0.8,
                borderBottomWidth: 0.8,
                borderRightWidth: 0.8,
                borderTopLeftRadius: 0,
                borderBottomLeftRadius: 0,
                borderTopRightRadius: 0,
                borderBottomRightRadius: 0,
            },
            tabRightStyle: {
                height: 35,
                backgroundColor: "#FFFFFF",
                borderColor: AppConfig.blueBackground,
                borderTopWidth: 0.8,
                borderBottomWidth: 0.8,
                borderRightWidth: 0.8,
                borderTopLeftRadius: 0,
                borderBottomLeftRadius: 0,
                borderTopRightRadius: 8,
                borderBottomRightRadius: 8,
                marginRight: 10,
            },
            activeTabLeftStyle: {
                height: 35,
                backgroundColor: AppConfig.blueBackground,
                borderColor: AppConfig.blueBackground,
                borderTopWidth: 0.8,
                borderBottomWidth: 0.8,
                borderLeftWidth: 0.8,
                borderRightWidth: 0.8,
                borderTopLeftRadius: 8,
                borderBottomLeftRadius: 8,
                borderTopRightRadius: 0,
                borderBottomRightRadius: 0,
                marginLeft: 10,
            },
            activeTabStyle: {
                height: 35,
                backgroundColor: AppConfig.blueBackground,
                borderColor: AppConfig.blueBackground,
                borderTopWidth: 0.8,
                borderBottomWidth: 0.8,
                borderRightWidth: 0.8,
                borderTopLeftRadius: 0,
                borderBottomLeftRadius: 0,
                borderTopRightRadius: 0,
                borderBottomRightRadius: 0,
            },
            activeTabRightStyle: {
                height: 35,
                backgroundColor: AppConfig.blueBackground,
                borderColor: AppConfig.blueBackground,
                borderTopWidth: 0.8,
                borderBottomWidth: 0.8,
                borderRightWidth: 0.8,
                borderTopLeftRadius: 0,
                borderBottomLeftRadius: 0,
                borderTopRightRadius: 8,
                borderBottomRightRadius: 8,
                marginRight: 10,
            },
            tabTextStyle: {
                fontSize: this.props.fontSize.FontSizeNorman,
                color: AppConfig.blueBackground,
                fontFamily: AppConfig.fontFamily
            },
            activeTabTextStyle: {
                fontSize: this.props.fontSize.FontSizeNorman,
                color: "#FFFFFF",
                textAlign: "center"
            },
        })

        let { tabsContainerStyle, values } = this.props
        return (
            <View style={{ backgroundColor: "#F5F5F5", height: 45, width: "100%", alignItems: 'center', justifyContent: "center" }}>
                <ScrollView
                    ref={(myRef) => { this.myTab = myRef }}
                    horizontal
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={{ ...tabsContainerStyle, alignSelf: 'center', }}
                >
                    {this.renderItem(values)}
                </ScrollView>
            </View>
        );
    }
}