import React, { Component } from 'react'
import { View, TouchableOpacity } from 'react-native'
import { Text } from "native-base"
import PropTypes from 'prop-types'
import { CheckBox, Icon } from 'native-base'
import { AppConfig } from "../../AppConfig"

export default class TouchableItem extends Component {
	static propTypes = {
		showIcon: PropTypes.bool,
		isOpen: PropTypes.bool,
		xemCheck: PropTypes.bool,
		phoiHopCheck: PropTypes.bool,
		xuLyChinhPress: PropTypes.bool,
		//onDropDownPress: PropTypes.func.isRequired,
		onXemPress: PropTypes.func,
		onPhoiHopPress: PropTypes.func,
		onXuLyChinhPress: PropTypes.func,
	}

	static defaultProps = {
		showIcon: false,
		isOpen: false,
		xemCheck: false,
		phoiHopCheck: false,
		xuLyChinhPress: false
	}

	shouldComponentUpdate(nextProps, nextState) {
		if (this.props.xemCheck !== nextProps.xemCheck
			|| this.props.phoiHopCheck !== nextProps.phoiHopCheck
			|| this.props.xuLyChinhPress !== nextProps.xuLyChinhPress
			|| this.props.isOpen !== nextProps.isOpen) {
			return true
		} else {
			return false
		}
	}

	renderTitle(item) {
		let marginLeft = 0
		if (!this.props.showIcon) {
			marginLeft = 15
		}

		if (item.data.ma.indexOf("DV") === -1) {
			return (
				<View>
					<Text style={[this.styles.regular, { marginLeft }]}>{item.data.ten}</Text>
					<Text style={[this.styles.italic, { marginLeft }]}>{item.data.ten_chuc_vu}</Text>
				</View>

			)
		} else {
			return (
				<Text style={[this.styles.regular, { marginLeft }]}>{item.data.ten}</Text>
			)
		}
	}

	renderCheckBox(item) {
		if (this.props.numberSelection === 1) {
			return (
				<View style={{ alignItems: 'flex-end' }}>
					<CheckBox checked={this.props.xemCheck} color={"black"} style={this.styles.chkFirst} onPress={_ => this.props.onXemPress(item.data, item.childs)} />
				</View>
			)
		} else if (this.props.numberSelection === 2) {
			if (this.props.showIcon) {
				if (this.props.primaryCheck === 1) {
					return (
						<View style={{ flexDirection: "row", justifyContent: 'flex-end' }}>
							<View style={this.styles.chk}></View>
							<CheckBox checked={this.props.xemCheck} color={"black"} style={this.styles.chkFirst} onPress={_ => this.props.onXemPress(item.data, item.childs)} />

						</View>
					)
				} else if (this.props.primaryCheck === 2) {
					return (
						<View style={{ flexDirection: "row", justifyContent: 'flex-end' }}>
							<CheckBox checked={this.props.phoiHopCheck} color={AppConfig.blueBackground} style={this.styles.chk} onPress={_ => this.props.onPhoiHopPress(item.data, item.childs)} />
							<View style={this.styles.chkFirst}></View>
						</View>
					)
				} else {
					return null
				}
			} else {
				return (
					<View style={{ flexDirection: "row", justifyContent: 'flex-end' }}>
						<CheckBox checked={this.props.phoiHopCheck} color={AppConfig.blueBackground} style={this.styles.chk} onPress={_ => this.props.onPhoiHopPress(item.data, item.childs)} />
						<CheckBox checked={this.props.xemCheck} color={"black"} style={this.styles.chkFirst} onPress={_ => this.props.onXemPress(item.data, item.childs)} />
					</View>
				)
			}
		} else {
			if (this.props.showIcon) {
				if (this.props.primaryCheck === 1) {
					return (
						<View style={{ flexDirection: "row", justifyContent: 'flex-end' }}>
							<View style={this.styles.chk}></View>
							<View style={this.styles.chk}></View>
							<CheckBox checked={this.props.xemCheck} color={"black"} style={this.styles.chkFirst} onPress={_ => this.props.onXemPress(item.data, item.childs)} />
						</View>
					)
				} else if (this.props.primaryCheck === 2) {
					return (
						<View style={{ flexDirection: "row", justifyContent: 'flex-end' }}>
							<View style={this.styles.chk}></View>
							<CheckBox checked={this.props.phoiHopCheck} color={AppConfig.blueBackground} style={this.styles.chk} onPress={_ => this.props.onPhoiHopPress(item.data, item.childs)} />
							<View style={this.styles.chkFirst}></View>
						</View>
					)
				} else if (this.props.primaryCheck === 3) {
					return (
						<View style={{ flexDirection: "row", justifyContent: 'flex-end' }}>
							<CheckBox checked={this.props.xuLyChinhPress} color={"red"} style={this.styles.chk} onPress={_ => this.props.onXuLyChinhPress(item.data, item.childs)} />
							<View style={this.styles.chk}></View>
							<View style={this.styles.chkFirst}></View>
						</View>
					)
				}
			} else {
				return (
					<View style={{ flexDirection: "row", justifyContent: 'flex-end' }}>
						<CheckBox checked={this.props.xuLyChinhPress} color={"red"} style={this.styles.chk} onPress={_ => this.props.onXuLyChinhPress(item.data, item.childs)} />
						<CheckBox checked={this.props.phoiHopCheck} color={AppConfig.blueBackground} style={this.styles.chk} onPress={_ => this.props.onPhoiHopPress(item.data, item.childs)} />
						<CheckBox checked={this.props.xemCheck} color={"black"} style={this.styles.chkFirst} onPress={_ => this.props.onXemPress(item.data, item.childs)} />
					</View>
				)
			}
		}
	}

	render() {

		this.styles = {
			item: {
				flex: 1,
				paddingTop: 10,
				paddingBottom: 10,
				paddingRight: 10,
				borderBottomColor: '#B3B3B3',
				borderBottomWidth: 0.5,
				marginLeft: 5
			},
			text: {
				flex: 1,
				marginLeft: 5,
			},
			regular: {
				color: '#212121',
				fontSize: this.props.fontSize.FontSizeNorman
			},
			italic: {
				color: '#666666',
				fontSize: this.props.fontSize.FontSizeSmall
			},
			chkFirst: {
				width: 25,
				height: 25,
				borderRadius: 0
			},
			chk: {
				width: 25,
				height: 25,
				borderRadius: 0,
				marginRight: 20
			},
			icon: {
				fontSize: this.props.fontSize.FontSizeLarge,
				marginLeft: -2,
				color: "#9E9E9E",
			}
		}

		const { item, showIcon, onDropDownPress, isOpen } = this.props
		return (
			<TouchableOpacity onPress={onDropDownPress} style={this.styles.item}>
				<View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
					{(showIcon) &&
						<Icon
							type="FontAwesome"
							name={!isOpen ? 'plus-square' : 'minus-square'}
							style={this.styles.icon} />}
					<View style={this.styles.text}>
						{this.renderTitle(item)}
					</View>
					{this.renderCheckBox(item)}
				</View>
			</TouchableOpacity>
		)
	}
}
