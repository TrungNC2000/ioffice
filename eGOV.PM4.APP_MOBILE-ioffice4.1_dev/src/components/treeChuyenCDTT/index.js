import React, { Component } from 'react'
import { View, FlatList } from 'react-native'
import { Text } from "native-base"
import PropTypes from 'prop-types'
import TouchableItem from './touchableItem'

export default class Picker extends Component {
	static propTypes = {
		title: PropTypes.string,
		data: PropTypes.arrayOf(PropTypes.any),
		//onDropDownPress: PropTypes.func.isRequired,
		style: PropTypes.shape({
			text: PropTypes.shape({
				color: PropTypes.string
			}),
			icon: PropTypes.shape({
				color: PropTypes.string
			}),
			underline: PropTypes.shape({
				borderBottomColor: PropTypes.string,
				paddingBottom: PropTypes.number
			})
		}),
		firstChkTitle: PropTypes.string,
		secondChkTitle: PropTypes.string,
		threeChkTitle: PropTypes.string,
		primaryCheck: PropTypes.number,
	}

	static defaultProps = {
		title: null,
		data: [],
		style: {
			text: {
				color: 'white'
			},
			icon: {
				color: 'white'
			},
			underline: {
				borderBottomColor: 'white',
				paddingBottom: 10
			}
		},
		firstChkTitle: null,
		secondChkTitle: null,
		threeChkTitle: null,
		primaryCheck: 1
	}

	constructor(props) {
		super(props)
		this.state = {
			showChildren: {},
			arrXemCheck: [],
			arrPhoiHopCheck: [],
			arrXuLyChinhPress: [],
			arrHoTen: []
		}
	}

	componentDidMount = () => {
		if (this.props.data[0]) {
			this.setState({
				showChildren: {
					...this.state.showChildren,
					[this.props.data[0].data.ma]: true,
				},
			})
		}
	}

	getDataCanBo = () => {
		let arrResult = []
		let arrXemCheck = this.state.arrXemCheck.filter(data => data.ma.indexOf("CB") !== -1)
		for (let item of arrXemCheck) {
			let itemTemp = new Object()
			itemTemp.ma = item.ma
			itemTemp.ten = item.ten
			itemTemp.ten_chuc_vu = item.ten_chuc_vu
			itemTemp.di_dong_can_bo = item.di_dong_can_bo + ""
			itemTemp.ma_yeu_cau = 1
			itemTemp.vai_tro_chuyen = "Xem để biết"
			arrResult.push(itemTemp)
		}
		return arrResult
	}

	getDataCanBo_PH = () => {
		let arrResult = []
		const arrXemCheck = this.state.arrXemCheck.filter(data => data.ma.indexOf("CB") !== -1)
		const arrPhoiHopCheck = this.state.arrPhoiHopCheck.filter(data => data.ma.indexOf("CB") !== -1)
		for (let item of arrPhoiHopCheck) {
			let itemTemp = new Object()
			itemTemp.ma = item.ma
			itemTemp.ten = item.ten
			itemTemp.ten_chuc_vu = item.ten_chuc_vu
			itemTemp.di_dong_can_bo = item.di_dong_can_bo + ""
			itemTemp.ma_yeu_cau = 3
			itemTemp.vai_tro_chuyen = "Phối hợp"
			arrResult.push(itemTemp)
		}
		for (let item of arrXemCheck) {
			let itemTemp = new Object()
			itemTemp.ma = item.ma
			itemTemp.ten = item.ten
			itemTemp.ten_chuc_vu = item.ten_chuc_vu
			itemTemp.di_dong_can_bo = item.di_dong_can_bo + ""
			itemTemp.ma_yeu_cau = 1
			itemTemp.vai_tro_chuyen = "Xem để biết"
			arrResult.push(itemTemp)
		}
		return arrResult
	}

	getDataCanBo_XLC_PH = () => {
		let arrResult = []
		const arrXemCheck = this.state.arrXemCheck.filter(data => data.ma.indexOf("CB") !== -1)
		const arrPhoiHopCheck = this.state.arrPhoiHopCheck.filter(data => data.ma.indexOf("CB") !== -1)
		const arrXuLyChinhPress = this.state.arrXuLyChinhPress.filter(data => data.ma.indexOf("CB") !== -1)
		for (let item of arrXuLyChinhPress) {
			let itemTemp = new Object()
			itemTemp.ma = item.ma
			itemTemp.ten = item.ten
			itemTemp.ten_chuc_vu = item.ten_chuc_vu
			itemTemp.di_dong_can_bo = item.di_dong_can_bo + ""
			itemTemp.ma_yeu_cau = 2
			itemTemp.vai_tro_chuyen = "Xử lý chính"
			arrResult.push(itemTemp)
		}
		for (let item of arrPhoiHopCheck) {
			let itemTemp = new Object()
			itemTemp.ma = item.ma
			itemTemp.ten = item.ten
			itemTemp.ten_chuc_vu = item.ten_chuc_vu
			itemTemp.di_dong_can_bo = item.di_dong_can_bo + ""
			itemTemp.ma_yeu_cau = 3
			itemTemp.vai_tro_chuyen = "Phối hợp"
			arrResult.push(itemTemp)
		}
		for (let item of arrXemCheck) {
			let itemTemp = new Object()
			itemTemp.ma = item.ma
			itemTemp.ten = item.ten
			itemTemp.ten_chuc_vu = item.ten_chuc_vu
			itemTemp.di_dong_can_bo = item.di_dong_can_bo + ""
			itemTemp.ma_yeu_cau = 1
			itemTemp.vai_tro_chuyen = "Xem để biết"
			arrResult.push(itemTemp)
		}
		return arrResult
	}

	onDropDownPress = (ma) => {
		this.setState({
			showChildren: {
				...this.state.showChildren,
				[ma]: !this.state.showChildren[ma]
			},
		})
	}

	checkExistXem = (data) => {
		return this.state.arrXemCheck.includes(data)
	}

	checkExistPhoiHop = (data) => {
		return this.state.arrPhoiHopCheck.includes(data)
	}

	checkExistXuLyChinh = (data) => {
		return this.state.arrXuLyChinhPress.includes(data)
	}

	getAllChild = (childs) => {
		let arrAllChild = []
		for (let node of childs) {
			arrAllChild.push(node.data)
			if (node.childs.length > 0) {
				arrAllChild = arrAllChild.concat(this.getAllChild(node.childs))
			}
		}
		return arrAllChild
	}

	onXemPress = (data, childs) => {
		this.checkExistXem(data) ? this.onXemUnCheck(data, childs) : this.onXemCheck(data, childs)
	}

	onXemCheck = (data, childs) => { // Check chọn
		let { arrXemCheck, arrPhoiHopCheck, arrXuLyChinhPress } = this.state
		const exitTwo = this.checkExistPhoiHop(data)
		const exitThree = this.checkExistXuLyChinh(data)
		if (exitTwo) {
			arrPhoiHopCheck = arrPhoiHopCheck.filter(el => el !== data)
		}
		if (exitThree) {
			arrXuLyChinhPress = arrXuLyChinhPress.filter(el => el !== data)
		}
		if (this.props.primaryCheck === 1 && childs.length > 0) {
			let arrAdd = []
			arrAdd.push(data)
			arrAdd = arrAdd.concat(this.getAllChild(childs))
			let arrNew = []
			arrNew.push(data)
			for (let item of arrAdd) {
				let exitTwo = this.checkExistPhoiHop(item)
				let exitThree = this.checkExistXuLyChinh(item)
				if (!exitTwo && !exitThree) {
					arrNew.push(item)
				}
			}
			this.setState({ arrXemCheck: this.state.arrXemCheck.concat(arrNew), arrPhoiHopCheck, arrXuLyChinhPress })
		} else {
			arrXemCheck = [...this.state.arrXemCheck, data]
			this.setState({ arrXemCheck, arrPhoiHopCheck, arrXuLyChinhPress })
		}
	}

	onXemUnCheck = (data, childs) => { //Bỏ check chọn
		if (this.props.primaryCheck === 1) {
			let arrSub = []
			arrSub.push(data)
			if (childs.length > 0) {
				arrSub = arrSub.concat(this.getAllChild(childs))
			}

			let { arrXemCheck, arrPhoiHopCheck, arrXuLyChinhPress } = this.state
			for (let item of arrSub) {
				arrXemCheck = arrXemCheck.filter(el => el !== item)
				if (this.checkExistPhoiHop(item)) {
					arrPhoiHopCheck = arrPhoiHopCheck.filter(el => el !== item)
				}
				if (this.checkExistXuLyChinh(item)) {
					arrXuLyChinhPress = arrXuLyChinhPress.filter(el => el !== item)
				}
			}
			this.setState({ arrXemCheck, arrPhoiHopCheck, arrXuLyChinhPress })
		} else {
			const arrXemCheck = this.state.arrXemCheck.filter(el => el !== data)
			this.setState({ arrXemCheck })
		}
	}

	onPhoiHopPress = (data, childs) => {
		this.checkExistPhoiHop(data) ? this.onPhoiHopUnCheck(data, childs) : this.onPhoiHopCheck(data, childs)
	}

	onPhoiHopCheck = (data, childs) => { // Check chọn
		let { arrXemCheck, arrPhoiHopCheck, arrXuLyChinhPress } = this.state
		const exitOne = this.checkExistXem(data)
		const exitThree = this.checkExistXuLyChinh(data)
		if (exitOne) {
			arrXemCheck = arrXemCheck.filter(el => el !== data)
		}
		if (exitThree) {
			arrXuLyChinhPress = arrXuLyChinhPress.filter(el => el !== data)
		}
		if (this.props.primaryCheck === 2 && childs.length > 0) {
			let arrAdd = []
			arrAdd.push(data)
			arrAdd = arrAdd.concat(this.getAllChild(childs))
			let arrNew = []
			arrNew.push(data)
			for (let item of arrAdd) {
				let exitOne = this.checkExistXem(item)
				let exitThree = this.checkExistXuLyChinh(item)
				if (!exitOne && !exitThree) {
					arrNew.push(item)
				}
			}
			this.setState({ arrXemCheck, arrPhoiHopCheck: this.state.arrPhoiHopCheck.concat(arrNew), arrXuLyChinhPress })
		} else {
			arrPhoiHopCheck = [...this.state.arrPhoiHopCheck, data]
			this.setState({ arrXemCheck, arrPhoiHopCheck, arrXuLyChinhPress })
		}
	}

	onPhoiHopUnCheck = (data, childs) => { //Bỏ check chọn
		if (this.props.primaryCheck === 2) {
			let arrSub = []
			arrSub.push(data)
			if (childs.length > 0) {
				arrSub = arrSub.concat(this.getAllChild(childs))
			}

			let { arrXemCheck, arrPhoiHopCheck, arrXuLyChinhPress } = this.state
			for (let item of arrSub) {
				arrPhoiHopCheck = arrPhoiHopCheck.filter(el => el !== item)
				if (this.checkExistXem(item)) {
					arrXemCheck = arrXemCheck.filter(el => el !== item)
				}
				if (this.checkExistXuLyChinh(item)) {
					arrXuLyChinhPress = arrXuLyChinhPress.filter(el => el !== item)
				}
			}
			this.setState({ arrXemCheck, arrPhoiHopCheck, arrXuLyChinhPress })
		} else {
			const arrPhoiHopCheck = this.state.arrPhoiHopCheck.filter(el => el !== data)
			this.setState({ arrPhoiHopCheck })
		}
	}

	onXuLyChinhPress = (data, childs) => {
		this.checkExistXuLyChinh(data) ? this.onXuLyChinhUnCheck(data, childs) : this.onXuLyChinhCheck(data, childs)
	}

	onXuLyChinhCheck = (data, childs) => { // Check chọn
		let { arrXemCheck, arrPhoiHopCheck, arrXuLyChinhPress } = this.state
		const exitOne = this.checkExistXem(data)
		const exitTwo = this.checkExistPhoiHop(data)
		if (exitOne) {
			arrXemCheck = arrXemCheck.filter(el => el !== data)
		}
		if (exitTwo) {
			arrPhoiHopCheck = arrPhoiHopCheck.filter(el => el !== data)
		}
		if (global.US_VBDEN_Mot_Can_Bo_XLC === "1") {
			arrXuLyChinhPress = [data]
			this.setState({ arrXemCheck, arrPhoiHopCheck, arrXuLyChinhPress })
		} else {
			if (this.props.primaryCheck === 3 && childs.length > 0) {
				let arrAdd = []
				arrAdd.push(data)
				arrAdd = arrAdd.concat(this.getAllChild(childs))
				let arrNew = []
				arrNew.push(data)
				for (let item of arrAdd) {
					let exitOne = this.checkExistXem(item)
					let exitTwo = this.checkExistPhoiHop(item)
					if (!exitOne && !exitTwo) {
						arrNew.push(item)
					}
				}
				this.setState({ arrXemCheck, arrPhoiHopCheck, arrXuLyChinhPress: this.state.arrXuLyChinhPress.concat(arrNew) })
			} else {
				arrXuLyChinhPress = [...this.state.arrXuLyChinhPress, data]
				this.setState({ arrXemCheck, arrPhoiHopCheck, arrXuLyChinhPress })
			}
		}

	}

	onXuLyChinhUnCheck = (data, childs) => { //Bỏ check chọn
		if (this.props.primaryCheck === 3) {
			let arrSub = []
			arrSub.push(data)
			if (childs.length > 0) {
				arrSub = arrSub.concat(this.getAllChild(childs))
			}

			let { arrXemCheck, arrPhoiHopCheck, arrXuLyChinhPress } = this.state
			for (let item of arrSub) {
				arrXuLyChinhPress = arrXuLyChinhPress.filter(el => el !== item)
				if (this.checkExistXem(item)) {
					arrXemCheck = arrXemCheck.filter(el => el !== item)
				}
				if (this.checkExistPhoiHop(item)) {
					arrPhoiHopCheck = arrPhoiHopCheck.filter(el => el !== item)
				}
			}
			this.setState({ arrXemCheck, arrPhoiHopCheck, arrXuLyChinhPress })
		} else {
			const arrXuLyChinhPress = this.state.arrXuLyChinhPress.filter(el => el !== data)
			this.setState({ arrXuLyChinhPress })
		}
	}

	renderChildren = (childs, id, margin) => childs.map((item) => {
		if (this.state.showChildren[id]) {
			if (item.childs && item.childs.length > 0) {
				return (
					<View key={item.data.ma} style={{ flex: 1, marginLeft: margin }}>
						<TouchableItem
							numberSelection={this.props.numberSelection}
							showIcon={item.childs.length > 0}
							isOpen={this.state.showChildren[item.data.ma]}
							item={item}
							onDropDownPress={() => this.onDropDownPress(item.data.ma)}
							xemCheck={this.state.arrXemCheck.includes(item.data)}
							phoiHopCheck={this.state.arrPhoiHopCheck.includes(item.data)}
							xuLyChinhPress={this.state.arrXuLyChinhPress.includes(item.data)}
							onXemPress={(data, childs) => { this.onXemPress(data, childs) }}
							onPhoiHopPress={(data, childs) => { this.onPhoiHopPress(data, childs) }}
							onXuLyChinhPress={(data, childs) => { this.onXuLyChinhPress(data, childs) }}
							primaryCheck={this.props.primaryCheck}
							fontSize={this.props.fontSize}
						/>
						{this.renderChildren(item.childs, item.data.ma, 20)}
					</View>
				)
			}
			return (
				<View key={item.data.ma} style={{ flex: 1, marginLeft: margin }}>
					<TouchableItem
						numberSelection={this.props.numberSelection}
						item={item}
						onDropDownPress={() => this.onDropDownPress(item.data.ma)}
						xemCheck={this.state.arrXemCheck.includes(item.data)}
						phoiHopCheck={this.state.arrPhoiHopCheck.includes(item.data)}
						xuLyChinhPress={this.state.arrXuLyChinhPress.includes(item.data)}
						onXemPress={(data, childs) => { this.onXemPress(data, childs) }}
						onPhoiHopPress={(data, childs) => { this.onPhoiHopPress(data, childs) }}
						onXuLyChinhPress={(data, childs) => { this.onXuLyChinhPress(data, childs) }}
						primaryCheck={this.props.primaryCheck}
						fontSize={this.props.fontSize}
					/>
				</View>
			)
		}
		return null
	})

	renderFeedbackItems = ({ item }) => {
		if (item.childs.length > 0) {
			return (
				<View key={item.data.ma} style={{ flex: 1 }}>
					<TouchableItem
						numberSelection={this.props.numberSelection}
						showIcon={item.childs.length > 0}
						isOpen={this.state.showChildren[item.data.ma]}
						item={item}
						onDropDownPress={() => this.onDropDownPress(item.data.ma)}
						xemCheck={this.state.arrXemCheck.includes(item.data)}
						phoiHopCheck={this.state.arrPhoiHopCheck.includes(item.data)}
						xuLyChinhPress={this.state.arrXuLyChinhPress.includes(item.data)}
						onXemPress={(data, childs) => { this.onXemPress(data, childs) }}
						onPhoiHopPress={(data, childs) => { this.onPhoiHopPress(data, childs) }}
						onXuLyChinhPress={(data, childs) => { this.onXuLyChinhPress(data, childs) }}
						primaryCheck={this.props.primaryCheck}
						fontSize={this.props.fontSize}
					/>
					{this.renderChildren(item.childs, item.data.ma, 20)}
				</View>
			)
		}
		return (
			<View key={item.data.ma} style={{ flex: 1 }}>
				<TouchableItem
					numberSelection={this.props.numberSelection}
					item={item}
					onDropDownPress={() => this.onDropDownPress(item.data.ma)}
					xemCheck={this.state.arrXemCheck.includes(item.data)}
					phoiHopCheck={this.state.arrPhoiHopCheck.includes(item.data)}
					xuLyChinhPress={this.state.arrXuLyChinhPress.includes(item.data)}
					onXemPress={(data, childs) => { this.onXemPress(data, childs) }}
					onPhoiHopPress={(data, childs) => { this.onPhoiHopPress(data, childs) }}
					onXuLyChinhPress={(data, childs) => { this.onXuLyChinhPress(data, childs) }}
					primaryCheck={this.props.primaryCheck}
					fontSize={this.props.fontSize}
				/>
			</View>
		)
	}

	_keyExtractor = (item, index) => index.toString()

	render() {

		this.styles = {
			row: {
				flexDirection: 'row'
			},
			arrowImage: {
				justifyContent: 'flex-end',
				flexDirection: 'row',
				alignItems: 'center'
			},
			underline: {
				//flex: 1,
				borderBottomColor: 'white',
				borderBottomWidth: 0.5,
				paddingBottom: 10
			},
			absoluteView: {
				position: 'absolute',
				width: '100%',
				height: '100%'
			},
			pickerContainer: {
				flex: 1,
				backgroundColor: "white",
				borderRadius: 5
			},
			title: {
				flexDirection: 'row',
				backgroundColor: "#F5F5F5",
				justifyContent: "center",
				paddingTop: 5,
				paddingBottom: 5,
				paddingLeft: 10,
			},
			titleLeft: {
				width: "70%",
				justifyContent: "center",
				alignItems: "flex-start",
				paddingLeft: 50
			},
			titleRight: {
				width: "30%",
				flexDirection: 'row',
				justifyContent: "flex-end",
				alignItems: "center",
			},
			titleText: {
				color: "#000000",
				fontSize: this.props.fontSize.FontSizeSmall,
				marginLeft: 11,
				marginRight: 9
			}
		}

		return (
			<View style={this.styles.pickerContainer}>
				<View style={this.styles.title}>
					<View style={this.styles.titleLeft}>
						<Text style={this.styles.titleText}>{this.props.title}</Text>
					</View>
					{(this.props.numberSelection === 1) &&
						<View style={this.styles.titleRight}>
							<Text style={this.styles.titleText}>{this.props.firstChkTitle ? this.props.firstChkTitle : "Xem"}</Text>
						</View>
					}
					{(this.props.numberSelection === 2) && (
						<View style={this.styles.titleRight}>
							<Text style={this.styles.titleText}>{this.props.secondChkTitle ? this.props.secondChkTitle : "PH"}</Text>
							<Text style={this.styles.titleText}>{this.props.firstChkTitle ? this.props.firstChkTitle : "Xem"}</Text>
						</View>
					)}
					{(this.props.numberSelection === 3) && (
						<View style={this.styles.titleRight}>
							<Text style={this.styles.titleText}>{this.props.threeChkTitle ? this.props.threeChkTitle : "XLC"}</Text>
							<Text style={this.styles.titleText}>{this.props.secondChkTitle ? this.props.secondChkTitle : "PH"}</Text>
							<Text style={this.styles.titleText}>{this.props.firstChkTitle ? this.props.firstChkTitle : "Xem"}</Text>
						</View>
					)}
				</View>
				<FlatList
					data={this.props.data}
					extraData={this.state}
					keyExtractor={this._keyExtractor}
					renderItem={this.renderFeedbackItems}
					style={[this.props.style, { marginLeft: 10, marginRight: 10 }]}
					removeClippedSubviews={true}
				/>
			</View>
		)
	}
}
