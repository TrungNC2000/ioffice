import React, { Component } from 'react';
import { View, Modal, Dimensions, Alert } from 'react-native';
import DocumentPicker from 'react-native-document-picker';
import { connect } from "react-redux"
import API from '../networks'
import RNFS from "react-native-fs";
import { Button, Card, CardItem, Toast, Icon, Body, Spinner, Text } from "native-base"
import { ToastSuccess } from "../untils/TextUntils"
import { AppConfig } from '../AppConfig';

var { height, width } = Dimensions.get("window");
class UploadFileCMU extends Component {
    constructor(props) {
        super(props);
        this.state = {
            source: null,
            name: "Nhấn vào để chọn tệp tin",
            showModal: false,
            height: height,
            width: width,
            loading: false
        }
    }

    _openModal = () => { this.setState({ showModal: true }) }

    _closeModal = () => {
        this.setState({
            showModal: false,
            source: null,
            name: "Nhấn vào để chọn tệp tin",
            loading: false
        })
    }
    _onPress = async () => {
        // Pick multiple files
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.allFiles],
            });
            let name = res.name;
            let tailOfFile = name.substr(name.lastIndexOf(".") + 1, name.length).toLowerCase();;
            if (tailOfFile == "pdf" ||
                tailOfFile == "doc" ||
                tailOfFile == "docx" ||
                tailOfFile == "png" ||
                tailOfFile == "jpg" ||
                tailOfFile == "odt" ||
                tailOfFile == "xlsx" ||
                tailOfFile == "xls"
            ) {
                RNFS.readFile(res.uri, "base64").then(result => {
                    this.setState({
                        source: result,
                        name: res.name,
                    })
                })

            } else {
                this.setState({
                    source: null,
                    name: "Nhấn vào để chọn tệp tin"
                }, () => alert("Tệp tin không hợp lệ!"))
            }
        } catch (err) {

        }
    }

    notifyUploadFile = () => {
        Toast.show({
            text: 'Tải lên thành công!',
            buttonText: 'Okay',
            position: "top",
            type: 'success'
        });
    }

    _onPressUpload = () => {
        let ma_ctcb = global.ma_ctcb_kc;
        let chuc_nang = "bo_sung_file_vbdi";
        let ten_file = this.state.name;
        let base64 = this.state.source;
        let ma_van_ban_di = this.props.ma_van_ban_di_kc;
        if (base64 == null) {
            this.setState({ loading: false, }, () => Alert.alert(
                'Thông báo',
                'Bạn chưa chọn tệp tin',
            ))
        } else {
            API.FILE.AU_UPLOADFILE_CNTTTLTB64(ma_ctcb, chuc_nang, ten_file, base64, ma_van_ban_di)
                .then((res) => {
                    switch (res) {
                        case (null || "0"): {
                            this.setState({
                                source: null,
                                name: "Nhấn vào để chọn tệp tin",
                                loading: false,
                            }, () => alert("Tải lên thất bại!"))
                            break;
                        }
                        case ("2"): {
                            this.setState({
                                source: null,
                                name: "Nhấn vào để chọn tệp tin",
                                loading: false,
                            }, () => alert("tệp tin quá lớn, chỉ tối đa 20MB!"));
                            break;
                        }
                        case ("3"): {
                            this.setState({
                                source: null,
                                name: "Nhấn vào để chọn tệp tin",
                                loading: false,
                            }, () => alert("Định dạng tệp tin không hợp lệ!"))
                            break;
                        }
                        default: {
                            this.props.parent.getDetails()
                            this._closeModal()
                            this.setState({
                                source: null,
                                name: "Nhấn vào để chọn tệp tin",
                                loading: false,
                            }, () => ToastSuccess("Tải lên tệp tin thành công!"))
                            break;
                        }
                    }
                })
                .catch((err) => {
                    Alert.alert(
                        'Thông báo',
                        'Tải lên lỗi!',
                        [
                            {
                                text: 'OK', onPress: () => this.setState({
                                    source: null,
                                    name: "Nhấn vào để chọn tệp tin",
                                    loading: false
                                })
                            },
                        ]
                    );
                })
        }
    }

    componentDidMount() {
        Dimensions.addEventListener("change", (e) => {
            this.setState(e.window);
        });
    }

    componentWillUnmount() {
        Dimensions.removeEventListener("change")
    }

    render() {
        let modalWidth, modalHeight;
        let buttonWidth, buttonHeight;
        modalWidth = (this.state.width < this.state.height) ? 0.7 : 0.5;
        modalHeight = (this.state.width < this.state.height) ? 0.35 : 0.7;
        buttonWidth = (this.state.width < this.state.height) ? 0.25 : 0.15;
        buttonHeight = (this.state.width < this.state.height) ? 0.05 : 0.1;
        return (
            <View style={{ alignItems: 'center', justifyContent: 'center', position: 'relative' }}>
                <Modal
                    transparent={true}
                    animationType="slide"
                    onRequestClose={this._closeModal}
                    visible={this.state.showModal}
                    supportedOrientations={["landscape", "landscape-left", "landscape-right", "portrait", "portrait-upside-down"]}

                >
                    <View style={{ flex: 1, backgroundColor: "rgba(33,33,33,0.8)", alignItems: 'center', justifyContent: 'center' }}>
                        <Card
                            style={{
                                width: this.state.width * modalWidth,
                                height: this.state.height * modalHeight,
                                justifyContent: 'flex-start',
                                alignItems: 'stretch',
                                borderRadius: 8
                            }}
                        >
                            <CardItem header
                                style={{
                                    flex: 2,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    borderTopLeftRadius: 8,
                                    borderTopRightRadius: 8
                                }}
                            >

                                <Text
                                    numberOfLines={1} ellipsizeMode="tail"
                                    style={{
                                        alignSelf: 'center',
                                        textAlign: 'center',
                                        fontSize: this.props.fontSize.FontSizeNorman,
                                        fontWeight: 'bold'
                                    }}
                                >Chọn văn bản đính kèm</Text>

                            </CardItem>
                            {
                                (this.state.loading) ?
                                    <Spinner />
                                    :
                                    <CardItem
                                        style={{
                                            flex: 5,
                                            justifyContent: 'flex-start',
                                            alignContent: 'flex-start',
                                            alignSelf: 'flex-start',
                                            alignItems: 'flex-start',
                                            marginTop: -20
                                        }}
                                    >
                                        <Body style={{ justifyContent: 'center', alignItems: 'center', }}>
                                            <View style={{ alignItems: 'center', }}>
                                                <Icon
                                                    onPress={() => this._onPress()}
                                                    type="SimpleLineIcons" name="cloud-upload"
                                                    style={{
                                                        fontSize: this.state.width * buttonWidth - 50,
                                                        color: "#1ea3ff",
                                                    }}
                                                />
                                            </View>
                                            <Text numberOfLines={1} ellipsizeMode="tail" style={{ fontSize: this.props.fontSize.FontSizeNorman }}>{this.state.name}</Text>
                                        </Body>
                                    </CardItem>
                            }

                            <CardItem footer
                                style={{
                                    flex: 3,
                                    justifyContent: 'space-evenly',
                                    alignItems: 'flex-end',
                                    borderBottomEndRadius: 8,
                                    borderBottomLeftRadius: 8,
                                }}
                            >
                                <Button
                                    info
                                    style={{ width: this.state.width * buttonWidth, height: this.state.height * buttonHeight, borderRadius: 4, alignItems: 'center', justifyContent: 'center', marginTop: 10 }}
                                    onPress={this._closeModal}
                                >
                                    <Text style={{ textAlign: 'center', color: '#FFF', fontSize: this.props.fontSize.FontSizeNorman }}>Đóng</Text>
                                </Button>
                                <Button
                                    info
                                    style={{ width: this.state.width * buttonWidth, height: this.state.height * buttonHeight, borderRadius: 4, alignItems: 'center', justifyContent: 'center', marginTop: 10 }}
                                    onPress={() => this.setState({ loading: true }, () => this._onPressUpload())}
                                >
                                    <Text style={{ textAlign: 'center', color: '#FFF', fontSize: this.props.fontSize.FontSizeNorman }}>Xác nhận</Text>
                                </Button>
                            </CardItem>
                        </Card>
                    </View>
                </Modal>
                <Button iconLeft transparent style={{ alignItems: 'center' }} onPress={() => this._openModal()}>
                    <Icon type="MaterialIcons" name='attachment' style={{ color: 'blue' }} />
                    <Text numberOfLines={1} ellipsizeMode="tail" style={{ color: 'blue', textAlign: 'left', marginLeft: 0, fontSize: this.props.fontSize.FontSizeNorman }} > Chọn văn bản đính kèm</Text>
                </Button>
            </View>
        );
    }
}

function mapStateToProps(state) {
    return {
        fontSize: state.fontSize,
    }
}

export default connect(mapStateToProps)(UploadFileCMU)