import { StyleSheet } from 'react-native'
import { AppConfig } from '../../../../AppConfig'

export const CenterElement = StyleSheet.create({
	container: {
		//flex: 1,
		justifyContent: 'center',
		alignItems: 'center'
	},
	text: {
		color: '#212121'
	}
})

export const picker = {
	row: {
		flexDirection: 'row'
	},
	arrowImage: {
		justifyContent: 'flex-end',
		flexDirection: 'row',
		alignItems: 'center'
	},
	underline: {
		//flex: 1,
		borderBottomColor: 'white',
		borderBottomWidth: 0.5,
		paddingBottom: 10
	},
	absoluteView: {
		position: 'absolute',
		width: '100%',
		height: '100%'
	},
	pickerContainer: {
		flex: 1,
		backgroundColor: "white",
		borderRadius: 5
	},
	title: {
		flexDirection: 'row',
		backgroundColor: "#F5F5F5",
		justifyContent: "center",
		paddingTop: 5,
		paddingBottom: 5,
		paddingLeft: 10,
	},
	titleLeft: {
		width: "70%",
		justifyContent: "center",
		alignItems: "flex-start",
		paddingLeft: 50
	},
	titleRight: {
		width: "30%",
		flexDirection: 'row',
		justifyContent: "flex-end",
		alignItems: "center",
	},
	titleText: {
		color: "#000000",
		fontSize: AppConfig.FontSizeSmall,
		marginLeft: 11,
		marginRight: 9
	}
}