import React, { Component } from 'react'
import { View, FlatList } from 'react-native'
import { Text } from "native-base"
import PropTypes from 'prop-types'
import TouchableItem from './touchableItem'

export default class Picker extends Component {
	static propTypes = {
		title: PropTypes.string,
		data: PropTypes.arrayOf(PropTypes.any),
		dataMD: PropTypes.arrayOf(PropTypes.any),
		//onDropDownPress: PropTypes.func.isRequired,
		style: PropTypes.shape({
			text: PropTypes.shape({
				color: PropTypes.string
			}),
			icon: PropTypes.shape({
				color: PropTypes.string
			}),
			underline: PropTypes.shape({
				borderBottomColor: PropTypes.string,
				paddingBottom: PropTypes.number
			})
		}),
		firstChkTitle: PropTypes.string,
		secondChkTitle: PropTypes.string,
		threeChkTitle: PropTypes.string,
	}

	static defaultProps = {
		title: null,
		data: [],
		dataMD: [],
		style: {
			text: {
				color: 'white'
			},
			icon: {
				color: 'white'
			},
			underline: {
				borderBottomColor: 'white',
				paddingBottom: 10
			}
		},
		firstChkTitle: null,
		secondChkTitle: null,
		threeChkTitle: null,
	}

	constructor(props) {
		super(props)
		this.state = {
			showChildren: {},
			arrXemCheck: [],
			arrPhoiHopCheck: [],
			arrXuLyChinhPress: [],
			arrHoTen: []
		}
	}

	componentDidMount = () => {
		if(global.nan_nhom_nguoi_nhan_van_ban_den_mac_dinh === '1') {
			this.setState({ arrXuLyChinhPress: this.props.dataMD });
		}
		if (this.props.data[0]) {
			this.setState({
				showChildren: {
					...this.state.showChildren,
					[this.props.data[0].li_attr.data_id]: true,
				},
			})
		}
	}

	componentWillReceiveProps(nextProps, nextState) {
		if (this.props !== nextProps) {
			if (nextProps.data.length > 0) {
				let showChildren = {}
				for (let item of nextProps.data) {
					showChildren = { ...showChildren, [item.li_attr.data_id]: true }
				}
				this.setState({ showChildren });
			}
		}
    }

	getDataCanBo = () => {
		let arrResult = []
		let arrXemCheck = this.state.arrXemCheck.filter(data => data.data_id.indexOf("CB") !== -1)
		for (let item of arrXemCheck) {
			let itemTemp = new Object()
			itemTemp.ma = item.data_id
			itemTemp.ten = item.data_ten_can_bo
			itemTemp.ten_chuc_vu = item.data_chuc_vu
			itemTemp.di_dong_can_bo = item.data_phone + ""
			itemTemp.ma_yeu_cau = 1
			itemTemp.vai_tro_chuyen = "Xem để biết"
			arrResult.push(itemTemp)
		}
		return arrResult
	}

	getDataCanBo_PH = () => {
		let arrResult = []
		const arrXemCheck = this.state.arrXemCheck.filter(data => data.data_id.indexOf("CB") !== -1)
		const arrPhoiHopCheck = this.state.arrPhoiHopCheck.filter(data => data.data_id.indexOf("CB") !== -1)
		for (let item of arrPhoiHopCheck) {
			let itemTemp = new Object()
			itemTemp.ma = item.data_id
			itemTemp.ten = item.data_ten_can_bo
			itemTemp.ten_chuc_vu = item.data_chuc_vu
			itemTemp.di_dong_can_bo = item.data_phone + ""
			itemTemp.ma_yeu_cau = 3
			itemTemp.vai_tro_chuyen = "Phối hợp"
			arrResult.push(itemTemp)
		}
		for (let item of arrXemCheck) {
			let itemTemp = new Object()
			itemTemp.ma = item.data_id
			itemTemp.ten = item.data_ten_can_bo
			itemTemp.ten_chuc_vu = item.data_chuc_vu
			itemTemp.di_dong_can_bo = item.data_phone + ""
			itemTemp.ma_yeu_cau = 1
			itemTemp.vai_tro_chuyen = "Xem để biết"
			arrResult.push(itemTemp)
		}
		return arrResult
	}

	getDataCanBo_XLC_PH = () => {
		let arrResult = []
		const arrXemCheck = this.state.arrXemCheck.filter(data => data.data_id.indexOf("CB") !== -1)
		const arrPhoiHopCheck = this.state.arrPhoiHopCheck.filter(data => data.data_id.indexOf("CB") !== -1)
		const arrXuLyChinhPress = this.state.arrXuLyChinhPress.filter(data => data.data_id.indexOf("CB") !== -1)
		for (let item of arrXuLyChinhPress) {
			let itemTemp = new Object()
			itemTemp.ma = item.data_id
			itemTemp.ten = item.data_ten_can_bo
			itemTemp.ten_chuc_vu = item.data_chuc_vu
			itemTemp.di_dong_can_bo = item.data_phone + ""
			itemTemp.ma_yeu_cau = 2
			itemTemp.vai_tro_chuyen = "Xử lý chính"
			arrResult.push(itemTemp)
		}
		for (let item of arrPhoiHopCheck) {
			let itemTemp = new Object()
			itemTemp.ma = item.data_id
			itemTemp.ten = item.data_ten_can_bo
			itemTemp.ten_chuc_vu = item.data_chuc_vu
			itemTemp.di_dong_can_bo = item.data_phone + ""
			itemTemp.ma_yeu_cau = 3
			itemTemp.vai_tro_chuyen = "Phối hợp"
			arrResult.push(itemTemp)
		}
		for (let item of arrXemCheck) {
			let itemTemp = new Object()
			itemTemp.ma = item.data_id
			itemTemp.ten = item.data_ten_can_bo
			itemTemp.ten_chuc_vu = item.data_chuc_vu
			itemTemp.di_dong_can_bo = item.data_phone + ""
			itemTemp.ma_yeu_cau = 1
			itemTemp.vai_tro_chuyen = "Xem để biết"
			arrResult.push(itemTemp)
		}
		return arrResult
	}

	onDropDownPress = (ma) => {
		this.setState({
			showChildren: {
				...this.state.showChildren,
				[ma]: !this.state.showChildren[ma]
			},
		})
	}

	checkExistXem = (data) => {
		return this.state.arrXemCheck.includes(data)
	}

	checkExistPhoiHop = (data) => {
		return this.state.arrPhoiHopCheck.includes(data)
	}

	checkExistXuLyChinh = (data) => {
		return this.state.arrXuLyChinhPress.includes(data)
	}

	getAllChild = (childs) => {
		let arrAllChild = []
		for (let node of childs) {
			arrAllChild.push(node.li_attr)
			if (node.childs.length > 0) {
				arrAllChild = arrAllChild.concat(this.getAllChild(node.childs))
			}
		}
		return arrAllChild
	}

	onXemPress = (data, childs) => {
		this.checkExistXem(data) ? this.onXemUnCheck(data, childs) : this.onXemCheck(data, childs)
	}

	onXemCheck = (data, childs) => { // Check chọn
		let { arrXemCheck, arrPhoiHopCheck, arrXuLyChinhPress } = this.state
		const exitTwo = this.checkExistPhoiHop(data)
		const exitThree = this.checkExistXuLyChinh(data)
		if (exitTwo) {
			arrPhoiHopCheck = arrPhoiHopCheck.filter(el => el !== data)
		}
		if (exitThree) {
			arrXuLyChinhPress = arrXuLyChinhPress.filter(el => el !== data)
		}
		if (childs.length > 0) {
			let arrAdd = []
			arrAdd.push(data)
			arrAdd = arrAdd.concat(this.getAllChild(childs))
			let arrNew = []
			arrNew.push(data)
			for (let item of arrAdd) {
				let exitTwo = this.checkExistPhoiHop(item)
				let exitThree = this.checkExistXuLyChinh(item)
				if (exitTwo) {
					arrPhoiHopCheck = arrPhoiHopCheck.filter(el => el !== item)
				}
				if (exitThree) {
					arrXuLyChinhPress = arrXuLyChinhPress.filter(el => el !== item)
				}
				arrNew.push(item)
			}
			this.setState({ arrXemCheck: this.state.arrXemCheck.concat(arrNew), arrPhoiHopCheck, arrXuLyChinhPress })
		} else {
			arrXemCheck = [...this.state.arrXemCheck, data]
			this.setState({ arrXemCheck, arrPhoiHopCheck, arrXuLyChinhPress })
		}
	}

	onXemUnCheck = (data, childs) => { //Bỏ check chọn
		let arrSub = []
		arrSub.push(data)
		if (childs.length > 0) {
			arrSub = arrSub.concat(this.getAllChild(childs))
		}

		let { arrXemCheck, arrPhoiHopCheck, arrXuLyChinhPress } = this.state
		for (let item of arrSub) {
			arrXemCheck = arrXemCheck.filter(el => el !== item)
			if (this.checkExistPhoiHop(item)) {
				arrPhoiHopCheck = arrPhoiHopCheck.filter(el => el !== item)
			}
			if (this.checkExistXuLyChinh(item)) {
				arrXuLyChinhPress = arrXuLyChinhPress.filter(el => el !== item)
			}
		}
		this.setState({ arrXemCheck, arrPhoiHopCheck, arrXuLyChinhPress })
	}

	onPhoiHopPress = (data, childs) => {
		this.checkExistPhoiHop(data) ? this.onPhoiHopUnCheck(data, childs) : this.onPhoiHopCheck(data, childs)
	}

	onPhoiHopCheck = (data, childs) => { // Check chọn
		let { arrXemCheck, arrPhoiHopCheck, arrXuLyChinhPress } = this.state
		const exitOne = this.checkExistXem(data)
		const exitThree = this.checkExistXuLyChinh(data)
		if (exitOne) {
			arrXemCheck = arrXemCheck.filter(el => el !== data)
		}
		if (exitThree) {
			arrXuLyChinhPress = arrXuLyChinhPress.filter(el => el !== data)
		}
		if (childs.length > 0) {
			let arrAdd = []
			arrAdd.push(data)
			arrAdd = arrAdd.concat(this.getAllChild(childs))
			let arrNew = []
			arrNew.push(data)
			for (let item of arrAdd) {
				let exitOne = this.checkExistXem(item)
				let exitThree = this.checkExistXuLyChinh(item)
				if (exitOne) {
					arrXemCheck = arrXemCheck.filter(el => el !== item)
				}
				if (exitThree) {
					arrXuLyChinhPress = arrXuLyChinhPress.filter(el => el !== item)
				}
				arrNew.push(item)
			}
			this.setState({ arrXemCheck, arrPhoiHopCheck: this.state.arrPhoiHopCheck.concat(arrNew), arrXuLyChinhPress })
		} else {
			arrPhoiHopCheck = [...this.state.arrPhoiHopCheck, data]
			this.setState({ arrXemCheck, arrPhoiHopCheck, arrXuLyChinhPress })
		}
	}

	onPhoiHopUnCheck = (data, childs) => { //Bỏ check chọn
		let arrSub = []
		arrSub.push(data)
		if (childs.length > 0) {
			arrSub = arrSub.concat(this.getAllChild(childs))
		}

		let { arrXemCheck, arrPhoiHopCheck, arrXuLyChinhPress } = this.state
		for (let item of arrSub) {
			arrPhoiHopCheck = arrPhoiHopCheck.filter(el => el !== item)
			if (this.checkExistXem(item)) {
				arrXemCheck = arrXemCheck.filter(el => el !== item)
			}
			if (this.checkExistXuLyChinh(item)) {
				arrXuLyChinhPress = arrXuLyChinhPress.filter(el => el !== item)
			}
		}
		this.setState({ arrXemCheck, arrPhoiHopCheck, arrXuLyChinhPress })
	}

	onXuLyChinhPress = (data, childs) => {
		this.checkExistXuLyChinh(data) ? this.onXuLyChinhUnCheck(data, childs) : this.onXuLyChinhCheck(data, childs)
	}

	onXuLyChinhCheck = (data, childs) => { // Check chọn
		let { arrXemCheck, arrPhoiHopCheck, arrXuLyChinhPress } = this.state
		const exitOne = this.checkExistXem(data)
		const exitTwo = this.checkExistPhoiHop(data)
		if (exitOne) {
			arrXemCheck = arrXemCheck.filter(el => el !== data)
		}
		if (exitTwo) {
			arrPhoiHopCheck = arrPhoiHopCheck.filter(el => el !== data)
		}
		if (global.US_VBDEN_Mot_Can_Bo_XLC === "1") {
			arrXuLyChinhPress = [data]
			this.setState({ arrXemCheck, arrPhoiHopCheck, arrXuLyChinhPress })
		} else {
			if (childs.length > 0) {
				let arrAdd = []
				arrAdd.push(data)
				arrAdd = arrAdd.concat(this.getAllChild(childs))
				let arrNew = []
				arrNew.push(data)
				for (let item of arrAdd) {
					let exitOne = this.checkExistXem(item)
					let exitTwo = this.checkExistPhoiHop(item)
					if (exitOne) {
						arrXemCheck = arrXemCheck.filter(el => el !== item)
					}
					if (exitTwo) {
						arrPhoiHopCheck = arrPhoiHopCheck.filter(el => el !== item)
					}
					arrNew.push(item)
				}
				this.setState({ arrXemCheck, arrPhoiHopCheck, arrXuLyChinhPress: this.state.arrXuLyChinhPress.concat(arrNew) })
			} else {
				arrXuLyChinhPress = [...this.state.arrXuLyChinhPress, data]
				this.setState({ arrXemCheck, arrPhoiHopCheck, arrXuLyChinhPress })
			}
		}

	}

	onXuLyChinhUnCheck = (data, childs) => { //Bỏ check chọn
		let arrSub = []
		arrSub.push(data)
		if (childs.length > 0) {
			arrSub = arrSub.concat(this.getAllChild(childs))
		}

		let { arrXemCheck, arrPhoiHopCheck, arrXuLyChinhPress } = this.state
		for (let item of arrSub) {
			arrXuLyChinhPress = arrXuLyChinhPress.filter(el => el !== item)
			if (this.checkExistXem(item)) {
				arrXemCheck = arrXemCheck.filter(el => el !== item)
			}
			if (this.checkExistPhoiHop(item)) {
				arrPhoiHopCheck = arrPhoiHopCheck.filter(el => el !== item)
			}
		}
		this.setState({ arrXemCheck, arrPhoiHopCheck, arrXuLyChinhPress })
	}

	renderChildren = (childs, id, margin) => childs.map((item) => {
		if (this.state.showChildren[id]) {
			if (item.childs && item.childs.length > 0) {
				return (
					<View key={item.li_attr.data_id} style={{ flex: 1, marginLeft: margin }}>
						<TouchableItem
							numberSelection={this.props.numberSelection}
							showIcon={item.childs.length > 0}
							isOpen={this.state.showChildren[item.li_attr.data_id]}
							item={item}
							onDropDownPress={() => this.onDropDownPress(item.li_attr.data_id)}
							xemCheck={this.state.arrXemCheck.includes(item.li_attr)}
							phoiHopCheck={this.state.arrPhoiHopCheck.includes(item.li_attr)}
							xuLyChinhPress={this.state.arrXuLyChinhPress.includes(item.li_attr)}
							onXemPress={(data, childs) => { this.onXemPress(data, childs) }}
							onPhoiHopPress={(data, childs) => { this.onPhoiHopPress(data, childs) }}
							onXuLyChinhPress={(data, childs) => { this.onXuLyChinhPress(data, childs) }}
							fontSize={this.props.fontSize}
						/>
						{this.renderChildren(item.childs, item.li_attr.data_id, 20)}
					</View>
				)
			}
			return (
				<View key={item.li_attr.data_id} style={{ flex: 1, marginLeft: margin }}>
					<TouchableItem
						numberSelection={this.props.numberSelection}
						item={item}
						onDropDownPress={() => this.onDropDownPress(item.li_attr.data_id)}
						xemCheck={this.state.arrXemCheck.includes(item.li_attr)}
						phoiHopCheck={this.state.arrPhoiHopCheck.includes(item.li_attr)}
						xuLyChinhPress={
							global.nan_nhom_nguoi_nhan_van_ban_den_mac_dinh === '1' ? 
							this.state.arrXuLyChinhPress.some(x => x.data_id === item.li_attr.data_id)
							 : this.state.arrXuLyChinhPress.includes(item.li_attr)
						}
						onXemPress={(data, childs) => { this.onXemPress(data, childs) }}
						onPhoiHopPress={(data, childs) => { this.onPhoiHopPress(data, childs) }}
						onXuLyChinhPress={(data, childs) => { this.onXuLyChinhPress(data, childs) }}
						fontSize={this.props.fontSize}
					/>
				</View>
			)
		}
		return null
	})

	renderFeedbackItems = ({ item }) => {
		if (item.childs.length > 0) {
			return (
				<View key={item.li_attr.data_id} style={{ flex: 1 }}>
					<TouchableItem
						numberSelection={this.props.numberSelection}
						showIcon={item.childs.length > 0}
						isOpen={this.state.showChildren[item.li_attr.data_id]}
						item={item}
						onDropDownPress={() => this.onDropDownPress(item.li_attr.data_id)}
						xemCheck={this.state.arrXemCheck.includes(item.li_attr)}
						phoiHopCheck={this.state.arrPhoiHopCheck.includes(item.li_attr)}
						xuLyChinhPress={this.state.arrXuLyChinhPress.includes(item.li_attr)}
						onXemPress={(data, childs) => { this.onXemPress(data, childs) }}
						onPhoiHopPress={(data, childs) => { this.onPhoiHopPress(data, childs) }}
						onXuLyChinhPress={(data, childs) => { this.onXuLyChinhPress(data, childs) }}
						fontSize={this.props.fontSize}
					/>
					{this.renderChildren(item.childs, item.li_attr.data_id, 20)}
				</View>
			)
		}
		return (
			<View key={item.li_attr.data_id} style={{ flex: 1 }}>
				<TouchableItem
					numberSelection={this.props.numberSelection}
					item={item}
					onDropDownPress={() => this.onDropDownPress(item.li_attr.data_id)}
					xemCheck={this.state.arrXemCheck.includes(item.li_attr)}
					phoiHopCheck={this.state.arrPhoiHopCheck.includes(item.li_attr)}
					xuLyChinhPress={this.state.arrXuLyChinhPress.includes(item.li_attr)}
					onXemPress={(data, childs) => { this.onXemPress(data, childs) }}
					onPhoiHopPress={(data, childs) => { this.onPhoiHopPress(data, childs) }}
					onXuLyChinhPress={(data, childs) => { this.onXuLyChinhPress(data, childs) }}
					fontSize={this.props.fontSize}
				/>
			</View>
		)
	}

	_keyExtractor = (item, index) => index.toString()

	render() {

		this.styles = {
			pickerContainer: {
				flex: 1,
				backgroundColor: "white",
				borderRadius: 5
			},
			title: {
				flexDirection: 'row',
				backgroundColor: "#F5F5F5",
				justifyContent: "center",
				paddingTop: 5,
				paddingBottom: 5,
				paddingLeft: 10,
			},
			titleLeft: {
				width: "70%",
				justifyContent: "center",
				alignItems: "flex-start",
				paddingLeft: 50
			},
			titleRight: {
				width: "30%",
				flexDirection: 'row',
				justifyContent: "flex-end",
				alignItems: "center",
			},
			titleText: {
				color: "#000000",
				fontSize: this.props.fontSize.FontSizeSmall,
				marginLeft: 11,
				marginRight: 9
			}
		}

		return (
			<View style={this.styles.pickerContainer}>
				<View style={this.styles.title}>
					<View style={this.styles.titleLeft}>
						<Text style={this.styles.titleText}>{this.props.title}</Text>
					</View>
					{(this.props.numberSelection === 1) &&
						<View style={this.styles.titleRight}>
							<Text style={this.styles.titleText}>Xem</Text>
						</View>
					}
					{(this.props.numberSelection === 2) && (
						<View style={this.styles.titleRight}>
							<Text style={this.styles.titleText}>PH</Text>
							<Text style={this.styles.titleText}>Xem</Text>
						</View>
					)}
					{(this.props.numberSelection === 3) && (
						<View style={this.styles.titleRight}>
							<Text style={this.styles.titleText}>XLC</Text>
							<Text style={this.styles.titleText}>PH</Text>
							<Text style={this.styles.titleText}>Xem</Text>
						</View>
					)}
				</View>
				<FlatList
					data={this.props.data}
					extraData={this.state}
					scrollEnabled={!global.AU_ROOT.includes('nghean-api')}
					keyExtractor={this._keyExtractor}
					renderItem={this.renderFeedbackItems}
					style={[this.props.style, { marginLeft: 10, marginRight: 10 }]}
					removeClippedSubviews={true}
				/>
			</View>
		)
	}
}
