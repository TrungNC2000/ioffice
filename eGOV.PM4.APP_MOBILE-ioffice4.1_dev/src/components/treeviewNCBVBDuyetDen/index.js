import React, { Component } from 'react';
import { View, FlatList } from 'react-native';
import { Text } from "native-base"
import PropTypes from 'prop-types';
import TouchableItem from './touchableItem';

export default class Picker extends Component {
	static propTypes = {
		title: PropTypes.string,
		data: PropTypes.arrayOf(PropTypes.any),
		onItemPress: PropTypes.func.isRequired,
		style: PropTypes.shape({
			text: PropTypes.shape({
				color: PropTypes.string
			}),
			icon: PropTypes.shape({
				color: PropTypes.string
			}),
			underline: PropTypes.shape({
				borderBottomColor: PropTypes.string,
				paddingBottom: PropTypes.number
			})
		}),
		firstChkTitle: PropTypes.string,
		secondChkTitle: PropTypes.string,
		threeChkTitle: PropTypes.string,
	}

	static defaultProps = {
		title: null,
		data: [],
		style: {
			text: {
				color: 'white'
			},
			icon: {
				color: 'white'
			},
			underline: {
				borderBottomColor: 'white',
				paddingBottom: 10
			}
		},
		firstChkTitle: null,
		secondChkTitle: null,
		threeChkTitle: null,
	}

	constructor(props) {
		super(props);
		this.state = {
			showChildren: {},
			arrOneCheck: [],
			arrTwoCheck: [],
			arrThreeCheck: [],
			arrHoTen: []
		};
	}

	componentDidMount = () => {
		// if (this.props.data[0]) {
		// 	this.setState({
		// 		showChildren: {
		// 			...this.state.showChildren,
		// 			[this.props.data[0].li_attr.data_id]: true,
		// 		},
		// 	});
		// }
	}

	getDataCanBo = () => {
		let arrResult = []
		let arrOneCheck = this.state.arrOneCheck.filter(li_attr => li_attr.data_chuc_vu !== null)
		for (let item of arrOneCheck) {
			let itemTemp = new Object()
			itemTemp.ma = item.data_id
			itemTemp.ten = item.data_ten_can_bo
			itemTemp.ten_chuc_vu = item.data_chuc_vu
			itemTemp.di_dong_can_bo = item.data_phone + ""
			itemTemp.ma_yeu_cau = 1
			itemTemp.vai_tro_chuyen = "Xem để biết"
			arrResult.push(itemTemp)
		}
		return arrResult
	}

	getDataCanBo_PH = () => {
		let arrResult = []
		const arrOneCheck = this.state.arrOneCheck.filter(li_attr => li_attr.data_chuc_vu !== null)
		const arrTwoCheck = this.state.arrTwoCheck.filter(li_attr => li_attr.data_chuc_vu !== null)
		for (let item of arrTwoCheck) {
			let itemTemp = new Object()
			itemTemp.ma = item.data_id
			itemTemp.ten = item.data_ten_can_bo
			itemTemp.ten_chuc_vu = item.data_chuc_vu
			itemTemp.di_dong_can_bo = item.data_phone + ""
			itemTemp.ma_yeu_cau = 3
			itemTemp.vai_tro_chuyen = "Phối hợp"
			arrResult.push(itemTemp)
		}
		for (let item of arrOneCheck) {
			let itemTemp = new Object()
			itemTemp.ma = item.ma
			itemTemp.ten = item.data_ten_can_bo
			itemTemp.ten_chuc_vu = item.data_chuc_vu
			itemTemp.di_dong_can_bo = item.data_phone + ""
			itemTemp.ma_yeu_cau = 1
			itemTemp.vai_tro_chuyen = "Xem để biết"
			arrResult.push(itemTemp)
		}
		return arrResult
	}

	getDataCanBo_XLC_PH = () => {
		let arrResult = []
		const arrOneCheck = this.state.arrOneCheck.filter(li_attr => li_attr.data_chuc_vu !== null)
		const arrTwoCheck = this.state.arrTwoCheck.filter(li_attr => li_attr.data_chuc_vu !== null)
		const arrThreeCheck = this.state.arrThreeCheck.filter(li_attr => li_attr.data_chuc_vu !== null)
		for (let item of arrThreeCheck) {
			let itemTemp = new Object()
			itemTemp.ma = item.data_id
			itemTemp.ten = item.data_ten_can_bo
			itemTemp.ten_chuc_vu = item.data_chuc_vu
			itemTemp.di_dong_can_bo = item.data_phone + ""
			itemTemp.ma_yeu_cau = 2
			itemTemp.vai_tro_chuyen = "Xử lý chính"
			arrResult.push(itemTemp)
		}
		for (let item of arrTwoCheck) {
			let itemTemp = new Object()
			itemTemp.ma = item.data_id
			itemTemp.ten = item.data_ten_can_bo
			itemTemp.ten_chuc_vu = item.data_chuc_vu
			itemTemp.di_dong_can_bo = item.data_phone + ""
			itemTemp.ma_yeu_cau = 3
			itemTemp.vai_tro_chuyen = "Phối hợp"
			arrResult.push(itemTemp)
		}
		for (let item of arrOneCheck) {
			let itemTemp = new Object()
			itemTemp.ma = item.data_id
			itemTemp.ten = item.data_ten_can_bo
			itemTemp.ten_chuc_vu = item.data_chuc_vu
			itemTemp.di_dong_can_bo = item.data_phone + ""
			itemTemp.ma_yeu_cau = 1
			itemTemp.vai_tro_chuyen = "Xem để biết"
			arrResult.push(itemTemp)
		}
		return arrResult
	}

	onItemPress = (data_id) => {
		this.setState({
			showChildren: {
				...this.state.showChildren,
				[data_id]: !this.state.showChildren[data_id]
			},
		});
	}

	checkExistOne = (li_attr) => {
		return this.state.arrOneCheck.includes(li_attr)
	}

	checkExistTwo = (li_attr) => {
		return this.state.arrTwoCheck.includes(li_attr)
	}

	checkExistThree = (li_attr) => {
		return this.state.arrThreeCheck.includes(li_attr)
	}

	getAllChild = (childs) => {
		let arrAllChild = []
		for (let node of childs) {
			arrAllChild.push(node.li_attr)
			if (node.childs.length > 0) {
				arrAllChild = arrAllChild.concat(this.getAllChild(node.childs))
			}
		}
		return arrAllChild
	}

	onOnePress = (li_attr, childs) => {
		this.checkExistOne(li_attr) ? this.onOneUnCheck(li_attr, childs) : this.onOneCheck(li_attr, childs)
	}

	onOneCheck = (li_attr, childs) => { // Check chọn
		let { arrOneCheck, arrTwoCheck, arrThreeCheck } = this.state
		const exitTwo = this.checkExistTwo(li_attr)
		const exitThree = this.checkExistThree(li_attr)
		if (exitTwo) {
			arrTwoCheck = arrTwoCheck.filter(el => el !== li_attr)
		}
		if (exitThree) {
			arrThreeCheck = arrThreeCheck.filter(el => el !== li_attr)
		}
		if (childs.length > 0) {
			let arrAdd = []
			arrAdd.push(li_attr)
			arrAdd = arrAdd.concat(this.getAllChild(childs))
			let arrNew = []
			arrNew.push(li_attr)
			for (let item of arrAdd) {
				let exitTwo = this.checkExistTwo(item)
				let exitThree = this.checkExistThree(item)
				if (exitTwo) {
					arrTwoCheck = arrTwoCheck.filter(el => el !== item)
				}
				if (exitThree) {
					arrThreeCheck = arrThreeCheck.filter(el => el !== item)
				}
				arrNew.push(item)
			}
			this.setState({ arrOneCheck: this.state.arrOneCheck.concat(arrNew), arrTwoCheck, arrThreeCheck })
		} else {
			arrOneCheck = [...this.state.arrOneCheck, li_attr]
			this.setState({ arrOneCheck, arrTwoCheck, arrThreeCheck })
		}
	}

	onOneUnCheck = (li_attr, childs) => { //Bỏ check chọn
		let arrSub = []
		arrSub.push(li_attr)
		if (childs.length > 0) {
			arrSub = arrSub.concat(this.getAllChild(childs))
		}

		let { arrOneCheck, arrTwoCheck, arrThreeCheck } = this.state
		for (let item of arrSub) {
			arrOneCheck = arrOneCheck.filter(el => el !== item)
			if (this.checkExistTwo(item)) {
				arrTwoCheck = arrTwoCheck.filter(el => el !== item)
			}
			if (this.checkExistThree(item)) {
				arrThreeCheck = arrThreeCheck.filter(el => el !== item)
			}
		}
		this.setState({ arrOneCheck, arrTwoCheck, arrThreeCheck })
	}

	onTwoPress = (li_attr, childs) => {
		this.checkExistTwo(li_attr) ? this.onTwoUnCheck(li_attr, childs) : this.onTwoCheck(li_attr, childs)
	}

	onTwoCheck = (li_attr, childs) => { // Check chọn
		let { arrOneCheck, arrTwoCheck, arrThreeCheck } = this.state
		const exitOne = this.checkExistOne(li_attr)
		const exitThree = this.checkExistThree(li_attr)
		if (exitOne) {
			arrOneCheck = arrOneCheck.filter(el => el !== li_attr)
		}
		if (exitThree) {
			arrThreeCheck = arrThreeCheck.filter(el => el !== li_attr)
		}
		if (childs.length > 0) {
			let arrAdd = []
			arrAdd.push(li_attr)
			arrAdd = arrAdd.concat(this.getAllChild(childs))
			let arrNew = []
			arrNew.push(li_attr)
			for (let item of arrAdd) {
				let exitOne = this.checkExistOne(item)
				let exitThree = this.checkExistThree(item)
				if (exitOne) {
					arrOneCheck = arrOneCheck.filter(el => el !== item)
				}
				if (exitThree) {
					arrThreeCheck = arrThreeCheck.filter(el => el !== item)
				}
				arrNew.push(item)
			}
			this.setState({ arrOneCheck, arrTwoCheck: this.state.arrTwoCheck.concat(arrNew), arrThreeCheck })
		} else {
			arrTwoCheck = [...this.state.arrTwoCheck, li_attr]
			this.setState({ arrOneCheck, arrTwoCheck, arrThreeCheck })
		}
	}

	onTwoUnCheck = (li_attr, childs) => { //Bỏ check chọn
		let arrSub = []
		arrSub.push(li_attr)
		if (childs.length > 0) {
			arrSub = arrSub.concat(this.getAllChild(childs))
		}

		let { arrOneCheck, arrTwoCheck, arrThreeCheck } = this.state
		for (let item of arrSub) {
			arrTwoCheck = arrTwoCheck.filter(el => el !== item)
			if (this.checkExistOne(item)) {
				arrOneCheck = arrOneCheck.filter(el => el !== item)
			}
			if (this.checkExistThree(item)) {
				arrThreeCheck = arrThreeCheck.filter(el => el !== item)
			}
		}
		this.setState({ arrOneCheck, arrTwoCheck, arrThreeCheck })
	}

	onThreePress = (li_attr, childs) => {
		this.checkExistThree(li_attr) ? this.onThreeUnCheck(li_attr, childs) : this.onThreeCheck(li_attr, childs)
	}

	onThreeCheck = (li_attr, childs) => { // Check chọn
		let { arrOneCheck, arrTwoCheck, arrThreeCheck } = this.state
		const exitOne = this.checkExistOne(li_attr)
		const exitTwo = this.checkExistTwo(li_attr)
		if (exitOne) {
			arrOneCheck = arrOneCheck.filter(el => el !== li_attr)
		}
		if (exitTwo) {
			arrTwoCheck = arrTwoCheck.filter(el => el !== li_attr)
		}
		if (childs.length > 0) {
			let arrAdd = []
			arrAdd.push(li_attr)
			arrAdd = arrAdd.concat(this.getAllChild(childs))
			let arrNew = []
			arrNew.push(li_attr)
			for (let item of arrAdd) {
				let exitOne = this.checkExistOne(item)
				let exitTwo = this.checkExistTwo(item)
				if (exitOne) {
					arrOneCheck = arrOneCheck.filter(el => el !== item)
				}
				if (exitTwo) {
					arrTwoCheck = arrTwoCheck.filter(el => el !== item)
				}
				arrNew.push(item)
			}
			this.setState({ arrOneCheck, arrTwoCheck, arrThreeCheck: this.state.arrThreeCheck.concat(arrNew) })
		} else {
			arrThreeCheck = [...this.state.arrThreeCheck, li_attr]
			this.setState({ arrOneCheck, arrTwoCheck, arrThreeCheck })
		}
	}

	onThreeUnCheck = (li_attr, childs) => { //Bỏ check chọn
		let arrSub = []
		arrSub.push(li_attr)
		if (childs.length > 0) {
			arrSub = arrSub.concat(this.getAllChild(childs))
		}

		let { arrOneCheck, arrTwoCheck, arrThreeCheck } = this.state
		for (let item of arrSub) {
			arrThreeCheck = arrThreeCheck.filter(el => el !== item)
			if (this.checkExistOne(item)) {
				arrOneCheck = arrOneCheck.filter(el => el !== item)
			}
			if (this.checkExistTwo(item)) {
				arrTwoCheck = arrTwoCheck.filter(el => el !== item)
			}
		}
		this.setState({ arrOneCheck, arrTwoCheck, arrThreeCheck })
	}

	renderChildren = (childs, id, margin) => childs.map((item) => {
		if (this.state.showChildren[id]) {
			if (item.childs && item.childs.length > 0) {
				return (
					<View key={item.li_attr.data_id} style={{ flex: 1, marginLeft: margin }}>
						<TouchableItem
							numberSelection={this.props.numberSelection}
							showIcon={item.childs.length > 0}
							isOpen={this.state.showChildren[item.li_attr.data_id]}
							item={item}
							onItemPress={() => this.onItemPress(item.li_attr.data_id)}
							oneCheck={this.state.arrOneCheck.includes(item.li_attr)}
							twoCheck={this.state.arrTwoCheck.includes(item.li_attr)}
							threeCheck={this.state.arrThreeCheck.includes(item.li_attr)}
							onOnePress={(li_attr, childs) => { this.onOnePress(li_attr, childs) }}
							onTwoPress={(li_attr, childs) => { this.onTwoPress(li_attr, childs) }}
							onThreePress={(li_attr, childs) => { this.onThreePress(li_attr, childs) }}
							fontSize={this.props.fontSize}
						/>
						{this.renderChildren(item.childs, item.li_attr.data_id, 20)}
					</View>
				);
			}
			return (
				<View key={item.li_attr.data_id} style={{ flex: 1, marginLeft: margin }}>
					<TouchableItem
						numberSelection={this.props.numberSelection}
						item={item}
						onItemPress={() => this.onItemPress(item.li_attr.data_id)}
						oneCheck={this.state.arrOneCheck.includes(item.li_attr)}
						twoCheck={this.state.arrTwoCheck.includes(item.li_attr)}
						threeCheck={this.state.arrThreeCheck.includes(item.li_attr)}
						onOnePress={(li_attr, childs) => { this.onOnePress(li_attr, childs) }}
						onTwoPress={(li_attr, childs) => { this.onTwoPress(li_attr, childs) }}
						onThreePress={(li_attr, childs) => { this.onThreePress(li_attr, childs) }}
						fontSize={this.props.fontSize}
					/>
				</View>
			);
		}
		return null;
	})

	renderFeedbackItems = ({ item }) => {
		if (item.childs.length > 0) {
			return (
				<View key={item.li_attr.data_id} style={{ flex: 1 }}>
					<TouchableItem
						numberSelection={this.props.numberSelection}
						showIcon={item.childs.length > 0}
						isOpen={this.state.showChildren[item.li_attr.data_id]}
						item={item}
						onItemPress={() => this.onItemPress(item.li_attr.data_id)}
						oneCheck={this.state.arrOneCheck.includes(item.li_attr)}
						twoCheck={this.state.arrTwoCheck.includes(item.li_attr)}
						threeCheck={this.state.arrThreeCheck.includes(item.li_attr)}
						onOnePress={(li_attr, childs) => { this.onOnePress(li_attr, childs) }}
						onTwoPress={(li_attr, childs) => { this.onTwoPress(li_attr, childs) }}
						onThreePress={(li_attr, childs) => { this.onThreePress(li_attr, childs) }}
						fontSize={this.props.fontSize}
					/>
					{this.renderChildren(item.childs, item.li_attr.data_id, 20)}
				</View>
			);
		}
		return (
			<View key={item.li_attr.data_id} style={{ flex: 1 }}>
				<TouchableItem
					numberSelection={this.props.numberSelection}
					item={item}
					onItemPress={() => this.onItemPress(item.li_attr.data_id)}
					oneCheck={this.state.arrOneCheck.includes(item.li_attr)}
					twoCheck={this.state.arrTwoCheck.includes(item.li_attr)}
					threeCheck={this.state.arrThreeCheck.includes(item.li_attr)}
					onOnePress={(li_attr, childs) => { this.onOnePress(li_attr, childs) }}
					onTwoPress={(li_attr, childs) => { this.onTwoPress(li_attr, childs) }}
					onThreePress={(li_attr, childs) => { this.onThreePress(li_attr, childs) }}
					primaryCheck={this.props.primaryCheck}
					fontSize={this.props.fontSize}
				/>
			</View>
		);
	}

	_keyExtractor = (item, index) => index.toString();

	render() {

		this.styles = {
			pickerContainer: {
				flex: 1,
				backgroundColor: "white",
				borderRadius: 5
			},
			title: {
				flexDirection: 'row',
				backgroundColor: "#F5F5F5",
				justifyContent: "center",
				paddingTop: 5,
				paddingBottom: 5,
				paddingLeft: 10,
			},
			titleLeft: {
				width: "70%",
				justifyContent: "center",
				alignItems: "flex-start",
				paddingLeft: 50
			},
			titleRight: {
				width: "30%",
				flexDirection: 'row',
				justifyContent: "flex-end",
				alignItems: "center",
			},
			titleText: {
				color: "#000000",
				fontSize: this.props.fontSize.FontSizeSmall,
				marginLeft: 11,
				marginRight: 9
			}
		};

		return (
			<View style={this.styles.pickerContainer}>
				<View style={this.styles.title}>
					<View style={this.styles.titleLeft}>
						<Text style={this.styles.titleText}>{this.props.title}</Text>
					</View>
					{(this.props.numberSelection === 1) &&
						<View style={this.styles.titleRight}>
							<Text style={this.styles.titleText}>{this.props.firstChkTitle}</Text>
						</View>
					}
					{(this.props.numberSelection === 2) && (
						<View style={this.styles.titleRight}>
							<Text style={this.styles.titleText}>{this.props.secondChkTitle}</Text>
							<Text style={this.styles.titleText}>{this.props.firstChkTitle}</Text>
						</View>
					)}
					{(this.props.numberSelection === 3) && (
						<View style={this.styles.titleRight}>
							<Text style={this.styles.titleText}>{this.props.threeChkTitle}</Text>
							<Text style={this.styles.titleText}>{this.props.secondChkTitle}</Text>
							<Text style={this.styles.titleText}>{this.props.firstChkTitle}</Text>
						</View>
					)}
				</View>
				<FlatList
					data={this.props.data}
					extraData={this.state}
					scrollEnabled={!global.AU_ROOT.includes('nghean-api')}
					keyExtractor={this._keyExtractor}
					renderItem={this.renderFeedbackItems}
					style={[this.props.style, { marginLeft: 10, marginRight: 10 }]}
					removeClippedSubviews={true}
				/>
			</View>
		);
	}
}
