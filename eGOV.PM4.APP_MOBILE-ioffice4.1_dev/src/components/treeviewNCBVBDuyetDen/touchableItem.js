import React, { Component } from 'react';
import { View, TouchableOpacity } from 'react-native';
import { Text } from "native-base"
import PropTypes from 'prop-types';
import { CheckBox, Icon } from 'native-base';
import { AppConfig } from "../../AppConfig"

export default class TouchableItem extends Component {
	static propTypes = {
		showIcon: PropTypes.bool,
		isOpen: PropTypes.bool,
		oneCheck: PropTypes.bool,
		twoCheck: PropTypes.bool,
		threeCheck: PropTypes.bool,
		onItemPress: PropTypes.func.isRequired,
		onOnePress: PropTypes.func,
		onTwoPress: PropTypes.func,
		onThreePress: PropTypes.func,
	}

	static defaultProps = {
		showIcon: false,
		isOpen: false,
		oneCheck: false,
		twoCheck: false,
		threeCheck: false
	}

	shouldComponentUpdate(nextProps, nextState) {
		if (this.props.oneCheck !== nextProps.oneCheck
			|| this.props.twoCheck !== nextProps.twoCheck
			|| this.props.threeCheck !== nextProps.threeCheck
			|| this.props.isOpen !== nextProps.isOpen) {
			return true;
		} else {
			return false;
		}
	}

	renderTitle(item) {
		let marginLeft = 0
		if (!this.props.showIcon) {
			marginLeft = 12
		}

		if (item.li_attr.data_id.indexOf("CB") !== -1) {
			return (
				<View>
					<Text style={[this.styles.regular, { marginLeft }]}>{item.li_attr.data_ten_can_bo}</Text>
					<Text style={[this.styles.italic, { marginLeft }]}>{item.li_attr.data_chuc_vu}</Text>
				</View>

			)
		} else {
			return (
				<Text style={[this.styles.regular, { marginLeft }]}>{item.li_attr.data_ten_can_bo}</Text>
			)
		}
	}

	renderCheckBox(item) {
		return (
			<View style={{ flexDirection: "row", justifyContent: 'flex-end' }}>
				<CheckBox checked={this.props.threeCheck} color={"red"} style={this.styles.chk} onPress={_ => this.props.onThreePress(item.li_attr, item.childs)} />
				<CheckBox checked={this.props.twoCheck} color={AppConfig.blueBackground} style={this.styles.chk} onPress={_ => this.props.onTwoPress(item.li_attr, item.childs)} />
				<CheckBox checked={this.props.oneCheck} color={"black"} style={this.styles.chkFirst} onPress={_ => this.props.onOnePress(item.li_attr, item.childs)} />
			</View>
		)
	}

	render() {

		this.styles = {
			item: {
				flex: 1,
				paddingTop: 10,
				paddingBottom: 10,
				paddingRight: 10,
				borderBottomColor: '#B3B3B3',
				borderBottomWidth: 0.5,
				marginLeft: 5
			},
			text: {
				flex: 1,
				marginLeft: 5,
			},
			regular: {
				color: '#212121',
				fontSize: this.props.fontSize.FontSizeNorman
			},
			italic: {
				color: '#666666',
				fontSize: this.props.fontSize.FontSizeSmall
			},
			chkFirst: {
				width: 25,
				height: 25,
				borderRadius: 0
			},
			chk: {
				width: 25,
				height: 25,
				borderRadius: 0,
				marginRight: 20
			},
			icon: {
				fontSize: this.props.fontSize.FontSizeLarge,
				marginLeft: -2,
				color: "#9E9E9E",
			}
		};

		const { item, showIcon, onItemPress, isOpen } = this.props
		return (
			<TouchableOpacity onPress={onItemPress} style={this.styles.item}>
				<View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
					{(showIcon) &&
						<Icon
							type="FontAwesome"
							name={!isOpen ? 'plus-square' : 'minus-square'}
							style={this.styles.icon} />}
					<View style={this.styles.text}>
						{this.renderTitle(item)}
					</View>
					{this.renderCheckBox(item)}
				</View>
			</TouchableOpacity>
		);
	}
}
