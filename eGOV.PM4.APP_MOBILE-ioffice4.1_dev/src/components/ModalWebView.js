import React, { Component } from 'react';
import { View, Modal, Platform, TouchableOpacity, StyleSheet, SafeAreaView } from "react-native"
import { Icon, Header, Text } from "native-base"
import { connect } from "react-redux"
import FloatingAction from "../components/Fab"
import { WebView } from 'react-native-webview';
import MyStatusBar from "../components/StatusBar"
import { Grid, Col } from "react-native-easy-grid"
import API from "../networks"
import { AppConfig } from "../AppConfig"
import { getURLFile, isIphoneX } from "../untils/TextUntils"
import { downloadFile } from "../untils/DownloadFile"
import { getViewFileDomainByVerision } from "../untils/TextUntils"

class ModalWebView extends Component {
    constructor(props) {
        super(props)
        this.state = {
            url: this.props.url
        }
    }

    convertWordToPdf = () => {
        let { title, url, path } = this.props
        if (title.toLowerCase().split(".").pop() === "doc" || title.toLowerCase().split(".").pop() === "docx") {
            let oldFile = path
            API.FILE.AU_FILE_CDTTWP(oldFile).then((response) => {
                if (response) {
                    let newFile = oldFile.replace(".docx", ".pdf")
                    newFile = newFile.replace(".doc", ".pdf")
                    newFile = newFile.replace(".DOC", ".pdf")
                    newFile = newFile.replace(".DOCX", ".pdf")
                    url = getViewFileDomainByVerision() + getURLFile(newFile)
                    this.setState({ url })
                }
            })
        }
    }

    shouldComponentUpdate = (nextProps, nextState) => {
        if (this.props !== nextProps ||
            this.state.url !== nextState.url) {
            if (this.props.url !== nextProps.url) {
                this.setState({ url: nextProps.url })
            }
            return true
        }
        return false
    };

    render() {

        this.styles = StyleSheet.create({
            headerTitleStyle: {
                color: AppConfig.defaultTextColor,
                fontSize: this.props.fontSize.FontSizeLarge
            },
            headerIconStyle: {
                color: AppConfig.defaultTextColor
            }
        })

        const { isModalVisible, title, path, toggleModal } = this.props
        const paddingTop = Platform.OS === "ios" ? 15 : 0
        const paddingLeft = 10

        if (isModalVisible) {
            return (
                <Modal
                    transparent={false}
                    animationType="slide"
                    onRequestClose={() => toggleModal()}
                    onShow={() => this.convertWordToPdf()}
                    visible={isModalVisible}
                    supportedOrientations={["landscape", "landscape-left", "landscape-right", "portrait", "portrait-upside-down"]}
                >
                    <View style={{ flex: 1, backgroundColor: Platform.OS === 'ios' ? "#000000" : AppConfig.statusBarColor }}>
                        <MyStatusBar backgroundColor={Platform.OS === 'ios' ? "#000000" : AppConfig.statusBarColor} skipAndroid={true} barStyle="light-content" />
                        <SafeAreaView style={{ zIndex: 999, backgroundColor: Platform.OS === 'ios' ? "#000000" : AppConfig.statusBarColor, flex: 1 }}>
                            <View style={{ zIndex: 999, backgroundColor: "blue", flex: 1 }}>
                                <Header hasTabs androidStatusBarColor={AppConfig.statusBarColor} iosBarStyle={"light-content"}>
                                    <Grid style={{ flex: 1, justifyContent: "center", alignItems: "center", paddingTop, paddingLeft }}>
                                        <Col style={{ width: "10%" }}>
                                            <Icon name={"arrow-back"} type="MaterialIcons" onPress={() => toggleModal()} style={this.styles.headerIconStyle} />
                                        </Col>
                                        <Col style={{ justifyContent: "center", alignItems: "center" }}>
                                            <Text style={this.styles.headerTitleStyle} numberOfLines={1} ellipsizeMode="middle">{title}</Text>
                                        </Col>
                                        <Col style={{ width: 90, paddingRight: 10, alignItems: "center", justifyContent: "center", flexDirection: "row" }}>
                                            <TouchableOpacity onPress={() => {
                                                if (Platform.OS === "ios") {
                                                    toggleModal()
                                                    setTimeout(() => {
                                                        downloadFile(title, path)
                                                    }, 300);
                                                } else {
                                                    downloadFile(title, path)
                                                }
                                            }}>
                                                <Icon name={"cloud-download"} type="Ionicons" style={this.styles.headerIconStyle} />
                                            </TouchableOpacity>
                                            <Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>&nbsp;&nbsp;&nbsp;</Text>
                                            <TouchableOpacity onPress={() => {
                                                if (Platform.OS === "ios") {
                                                    toggleModal()
                                                    setTimeout(() => {
                                                        downloadFile(title, path, true)
                                                    }, 300);
                                                } else {
                                                    downloadFile(title, path, true)
                                                }
                                            }}>
                                                <Icon name={"share"} type="MaterialIcons" style={this.styles.headerIconStyle} />
                                            </TouchableOpacity>
                                        </Col>
                                    </Grid>
                                </Header>
                                <View style={{ flex: 1 }}>
                                    <WebView
                                        ref={WebView => { this.WebView1 = WebView }}
                                        style={{ flex: 1 }}
                                        //source={{ uri: this.state.url.replace("__http-","__https-") }}
                                        source={{ uri: this.state.url }}
                                        javaScriptEnabled={true}
                                        domStorageEnabled={true}
                                        decelerationRate="normal"
                                        mixedContentMode="always"
                                        originWhitelist={["*"]}
                                        startInLoadingState={true}
                                        scalesPageToFit={true}
                                        injectedJavaScript={Constants.javascript.injection}
                                        bounces={false}
                                        onNavigationStateChange={(event) => {
                                            console.log(event)
                                        }}
                                    />
                                    {
                                        (this.props.fabActions) && (
                                            <FloatingAction
                                                fabActions={this.props.fabActions}
                                                fabPressItem={(name) => { this.props.fabPressItem(name) }}
                                            />
                                        )
                                    }
                                </View>
                            </View>
                        </SafeAreaView>


                        {/* <Header hasTabs style={{ magrinTop: 20 }} androidStatusBarColor={AppConfig.statusBarColor} iosBarStyle={"light-content"}>
                            <Grid style={{ flex: 1, justifyContent: "center", alignItems: "center", paddingTop, paddingLeft }}>
                                <Col style={{ width: "10%" }}>
                                    <Icon name={"arrow-back"} type="MaterialIcons" onPress={() => toggleModal()} style={this.styles.headerIconStyle} />
                                </Col>
                                <Col style={{ justifyContent: "center", alignItems: "center" }}>
                                    <Text style={this.styles.headerTitleStyle} numberOfLines={1} ellipsizeMode="middle">{title}</Text>
                                </Col>
                                <Col style={{ width: 90, paddingRight: 10, alignItems: "center", justifyContent: "center", flexDirection: "row" }}>
                                    <TouchableOpacity onPress={() => {
                                        if (Platform.OS === "ios") {
                                            toggleModal()
                                            setTimeout(() => {
                                                downloadFile(title, path)
                                            }, 300);
                                        } else {
                                            downloadFile(title, path)
                                        }
                                    }}>
                                        <Icon name={"cloud-download"} type="Ionicons" style={this.styles.headerIconStyle} />
                                    </TouchableOpacity>
                                    <Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>&nbsp;&nbsp;&nbsp;</Text>
                                    <TouchableOpacity onPress={() => {
                                        if (Platform.OS === "ios") {
                                            toggleModal()
                                            setTimeout(() => {
                                                downloadFile(title, path, true)
                                            }, 300);
                                        } else {
                                            downloadFile(title, path, true)
                                        }
                                    }}>
                                        <Icon name={"share"} type="MaterialIcons" style={this.styles.headerIconStyle} />
                                    </TouchableOpacity>
                                </Col>
                            </Grid>
                        </Header>
                        <View style={{ flex: 1, marginTop: isIphoneX() ? 80 : 50, zIndex: 0 }}>
                            <WebView
                                ref={WebView => { this.WebView1 = WebView }}
                                style={{ flex: 1 }}
                                //source={{ uri: this.state.url.replace("__http-","__https-") }}
                                source={{ uri: this.state.url }}
                                javaScriptEnabled={true}
                                domStorageEnabled={true}
                                decelerationRate="normal"
                                mixedContentMode="always"
                                originWhitelist={["*"]}
                                startInLoadingState={true}
                                scalesPageToFit={true}
                                injectedJavaScript={Constants.javascript.injection}
                                bounces={false}
                                onNavigationStateChange={(event) => {
                                    console.log(event)
                                }}
                            />
                            {
                                (this.props.fabActions) && (
                                    <FloatingAction
                                        fabActions={this.props.fabActions}
                                        fabPressItem={(name) => { this.props.fabPressItem(name) }}
                                    />
                                )
                            }
                        </View> */}
                    </View>
                </Modal>
            )
        } else {
            return null
        }
    }
}

const Constants = {
    javascript: {
        injection: `
            Array.from(document.getElementsByClassName('ndfHFb-c4YZDc-Wrql6b')).forEach((item) => {
                item.remove();
            })
        `
    }
}

function mapStateToProps(state) {
    return {
        fontSize: state.fontSize,
    }
}

export default connect(mapStateToProps)(ModalWebView)