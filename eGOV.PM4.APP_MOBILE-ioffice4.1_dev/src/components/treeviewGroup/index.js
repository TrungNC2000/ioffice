import React, { Component } from 'react';
import { View, FlatList } from 'react-native';
import { Text } from "native-base"
import PropTypes from 'prop-types';
import TouchableItem from './touchableItem';

export default class Picker extends Component {
	static propTypes = {
		title: PropTypes.string,
		data: PropTypes.arrayOf(PropTypes.any),
		onItemPress: PropTypes.func.isRequired,
		style: PropTypes.shape({
			text: PropTypes.shape({
				color: PropTypes.string
			}),
			icon: PropTypes.shape({
				color: PropTypes.string
			}),
			underline: PropTypes.shape({
				borderBottomColor: PropTypes.string,
				paddingBottom: PropTypes.number
			})
		}),
		firstChkTitle: PropTypes.string,
		secondChkTitle: PropTypes.string,
	}

	static defaultProps = {
		title: null,
		data: [],
		style: {
			text: {
				color: 'white'
			},
			icon: {
				color: 'white'
			},
			underline: {
				borderBottomColor: 'white',
				paddingBottom: 10
			}
		},
		firstChkTitle: null,
		secondChkTitle: null
	}

	constructor(props) {
		super(props);
		this.state = {
			showChildren: {},
			arrOneCheck: [],
			arrTwoCheck: [],
			arrThreeCheck: [],
			arrHoTen: []
		};
	}

	componentDidMount = () => {
		// if (this.props.data[0]) {
		// 	this.setState({
		// 		showChildren: {
		// 			...this.state.showChildren,
		// 			[this.props.data[0].data.id]: true,
		// 		},
		// 	});
		// }
	}

	getDataCanBo = () => {
		let arrResult = []
		let arrOneCheck = this.state.arrOneCheck.filter(data => data.id.indexOf("CB") !== -1)
		for (let item of arrOneCheck) {
			let itemTemp = new Object()
			itemTemp.ma = item.id
			itemTemp.ten = item.text
			itemTemp.ten_chuc_vu = item.ten_chuc_vu
			itemTemp.di_dong_can_bo = item.di_dong_can_bo + ""
			itemTemp.ma_yeu_cau = 1
			itemTemp.vai_tro_chuyen = ""
			arrResult.push(itemTemp)
		}
		return arrResult
	}

	getDataCanBo_PH = () => {
		let arrResult = []
		const arrOneCheck = this.state.arrOneCheck.filter(data => data.id.indexOf("CB") !== -1)
		for (let item of arrOneCheck) {
			if (this.checkExistTwo(item)) {
				let itemTemp = new Object()
				itemTemp.ma = item.id
				itemTemp.ten = item.text
				itemTemp.ten_chuc_vu = item.ten_chuc_vu
				itemTemp.di_dong_can_bo = item.di_dong_can_bo + ""
				itemTemp.vai_tro_chuyen = "Phối hợp"
				itemTemp.ma_yeu_cau = 3
				arrResult.push(itemTemp)
			} else {
				let itemTemp = new Object()
				itemTemp.ma = item.id
				itemTemp.ten = item.text
				itemTemp.ten_chuc_vu = item.ten_chuc_vu
				itemTemp.di_dong_can_bo = item.di_dong_can_bo + ""
				itemTemp.vai_tro_chuyen = "Xem để biết"
				itemTemp.ma_yeu_cau = 1
				arrResult.push(itemTemp)
			}
		}
		return arrResult
	}

	getDataCanBo_XLC_PH = () => {
		let arrResult = []
		const arrOneCheck = this.state.arrOneCheck.filter(data => data.id.indexOf("CB") !== -1)
		for (let item of arrOneCheck) {
			if (this.checkExistTwo(item)) {
				let itemTemp = new Object()
				itemTemp.ma = item.id
				itemTemp.ten = item.text
				itemTemp.ten_chuc_vu = item.ten_chuc_vu
				itemTemp.di_dong_can_bo = item.di_dong_can_bo + ""
				itemTemp.ma_yeu_cau = 2
				itemTemp.vai_tro_chuyen = "Xử lý chính"
				arrResult.push(itemTemp)
			} else if (this.checkExistThree(item)) {
				let itemTemp = new Object()
				itemTemp.ma = item.id
				itemTemp.ten = item.text
				itemTemp.ten_chuc_vu = item.ten_chuc_vu
				itemTemp.di_dong_can_bo = item.di_dong_can_bo + ""
				itemTemp.ma_yeu_cau = 3
				itemTemp.vai_tro_chuyen = "Phối hợp"
				arrResult.push(itemTemp)
			} else {
				let itemTemp = new Object()
				itemTemp.ma = item.id
				itemTemp.ten = item.text
				itemTemp.ten_chuc_vu = item.ten_chuc_vu
				itemTemp.di_dong_can_bo = item.di_dong_can_bo + ""
				itemTemp.ma_yeu_cau = 1
				itemTemp.vai_tro_chuyen = "Xem để biết"
				arrResult.push(itemTemp)
			}
		}
		return arrResult
	}

	onItemPress = (id, isDad, childs) => {
		this.setState({
			showChildren: {
				...this.state.showChildren,
				[id]: !this.state.showChildren[id]
			},
		});
	}


	checkExistOne = (data) => {
		if (this.state.arrOneCheck.includes(data)) {
			return true
		}
		return false
	}

	checkExistTwo = (data) => {
		if (this.state.arrTwoCheck.includes(data)) {
			return true
		}
		return false
	}

	checkExistThree = (data) => {
		if (this.state.arrThreeCheck.includes(data)) {
			return true
		}
		return false
	}

	getAllChild = (childs) => {
		let arrAllChild = []
		for (let node of childs) {
			arrAllChild.push(node.data)
			if (node.childs.length > 0) {
				arrAllChild = arrAllChild.concat(this.getAllChild(node.childs))
			}
		}
		return arrAllChild
	}

	onOnePress = (data, isDad, childs) => {
		this.checkExistOne(data) ? this.onOneUnCheck(data, isDad, childs) : this.onOneCheck(data, isDad, childs)
	}

	onOneCheck = (data, isDad, childs) => { // Check chọn
		let arrAdd = []
		arrAdd.push(data)
		if (isDad) {
			arrAdd = arrAdd.concat(this.getAllChild(childs))
		}
		this.setState({ arrOneCheck: this.state.arrOneCheck.concat(arrAdd) })
	}

	onOneUnCheck = (data, isDad, childs) => { //Bỏ check chọn
		let arrSub = []
		arrSub.push(data)
		if (isDad) {
			arrSub = arrSub.concat(this.getAllChild(childs))
		}

		let { arrOneCheck, arrTwoCheck, arrThreeCheck } = this.state
		for (let item of arrSub) {
			arrOneCheck = arrOneCheck.filter(el => el !== item)
			if (this.checkExistTwo(item)) {
				arrTwoCheck = arrTwoCheck.filter(el => el !== item)
			}
			if (this.checkExistThree(item)) {
				arrThreeCheck = arrThreeCheck.filter(el => el !== item)
			}
		}
		this.setState({ arrOneCheck, arrTwoCheck, arrThreeCheck })
	}

	onTwoPress = (data) => {
		this.checkExistTwo(data) ? this.onTwoUnCheck(data) : this.onTwoCheck(data)
	}

	onTwoCheck = (data) => { // Check chọn
		const arrTwoCheck = [...this.state.arrTwoCheck, data]
		const exitOne = this.checkExistOne(data)
		const exitThree = this.checkExistThree(data)

		if (exitOne && exitThree) { //Nếu check 1, check 3 là check
			const arrThreeCheck = this.state.arrThreeCheck.filter(el => el !== data)
			this.setState({ arrTwoCheck, arrThreeCheck })
		} else if (exitOne && !exitThree) { //Nếu check 1 là check, check 3 là uncheck
			this.setState({ arrTwoCheck })
		} else {
			const arrOneCheck = this.state.arrOneCheck.concat(data)
			this.setState({ arrOneCheck, arrTwoCheck })
		}
	}

	onTwoUnCheck = (data) => { //Bỏ check chọn
		const arrTwoCheck = this.state.arrTwoCheck.filter(el => el !== data)
		this.setState({ arrTwoCheck })
	}

	onThreePress = (data) => {
		this.checkExistThree(data) ? this.onThreeUnCheck(data) : this.onThreeCheck(data)
	}

	onThreeCheck = (data) => { // Check chọn
		const arrThreeCheck = [...this.state.arrThreeCheck, data]
		const exitOne = this.checkExistOne(data)
		const exitTwo = this.checkExistTwo(data)

		if (exitOne && exitTwo) { //Nếu check 1, check 2 là check
			const arrTwoCheck = this.state.arrTwoCheck.filter(el => el !== data)
			this.setState({ arrThreeCheck, arrTwoCheck })
		} else if (exitOne && !exitTwo) { //Nếu check 1 là check, check 2 là uncheck
			this.setState({ arrThreeCheck })
		} else {
			//const arrOneCheck = [data, ...this.state.arrOneCheck]
			const arrOneCheck = this.state.arrOneCheck.concat(data)
			this.setState({ arrOneCheck, arrThreeCheck })
		}
	}

	onThreeUnCheck = (data) => { //Bỏ check chọn
		const arrThreeCheck = this.state.arrThreeCheck.filter(el => el !== data)
		this.setState({ arrThreeCheck })
	}


	renderChildren = (childs, id, margin) => childs.map((item) => {
		if (this.state.showChildren[id]) {
			if (item.childs && item.childs.length > 0) {
				return (
					<View key={item.data.id} style={{ flex: 1, marginLeft: margin }}>
						<TouchableItem
							numberSelection={this.props.numberSelection}
							showIcon={item.childs.length > 0}
							isOpen={this.state.showChildren[item.data.id]}
							item={item}
							onItemPress={() => this.onItemPress(item.data.id, true, item.childs)}
							oneCheck={this.state.arrOneCheck.includes(item.data)}
							twoCheck={this.state.arrTwoCheck.includes(item.data)}
							threeCheck={this.state.arrThreeCheck.includes(item.data)}
							onOnePress={(id, childs) => { this.onOnePress(id, true, childs) }}
							onTwoPress={(id) => { this.onTwoPress(id) }}
							onThreePress={(id) => { this.onThreePress(id) }}
							fontSize={this.props.fontSize}
						/>
						{this.renderChildren(item.childs, item.data.id, 20)}
					</View>
				);
			}
			return (
				<View key={item.data.id} style={{ flex: 1, marginLeft: margin }}>
					<TouchableItem
						numberSelection={this.props.numberSelection}
						item={item}
						onItemPress={() => this.onItemPress(item.data.id, false, item.childs)}
						oneCheck={this.state.arrOneCheck.includes(item.data)}
						twoCheck={this.state.arrTwoCheck.includes(item.data)}
						threeCheck={this.state.arrThreeCheck.includes(item.data)}
						onOnePress={(id, childs) => { this.onOnePress(id, false, childs) }}
						onTwoPress={(id) => { this.onTwoPress(id) }}
						onThreePress={(id) => { this.onThreePress(id) }}
						fontSize={this.props.fontSize}
					/>
				</View>
			);
		}
		return null;
	})

	renderFeedbackItems = ({ item }) => {
		if (item.childs.length > 0) {
			return (
				<View key={item.data.id} style={{ flex: 1 }}>
					<TouchableItem
						numberSelection={this.props.numberSelection}
						showIcon={item.childs.length > 0}
						isOpen={this.state.showChildren[item.data.id]}
						item={item}
						onItemPress={() => this.onItemPress(item.data.id, true, item.childs)}
						oneCheck={this.state.arrOneCheck.includes(item.data)}
						twoCheck={this.state.arrTwoCheck.includes(item.data)}
						threeCheck={this.state.arrThreeCheck.includes(item.data)}
						onOnePress={(id, childs) => { this.onOnePress(id, true, childs) }}
						onTwoPress={(id) => { this.onTwoPress(id) }}
						onThreePress={(id) => { this.onThreePress(id) }}
						fontSize={this.props.fontSize}
					/>
					{this.renderChildren(item.childs, item.data.id, 20)}
				</View>
			);
		}
		return (
			<View key={item.data.id} style={{ flex: 1 }}>
				<TouchableItem
					numberSelection={this.props.numberSelection}
					item={item}
					onItemPress={() => this.onItemPress(item.data.id, false, item.childs)}
					oneCheck={this.state.arrOneCheck.includes(item.data)}
					twoCheck={this.state.arrTwoCheck.includes(item.data)}
					threeCheck={this.state.arrThreeCheck.includes(item.data)}
					onOnePress={(id, childs) => { this.onOnePress(id, false, childs) }}
					onTwoPress={(id) => { this.onTwoPress(id) }}
					onThreePress={(id) => { this.onThreePress(id) }}
					fontSize={this.props.fontSize}
				/>
			</View>
		);
	}

	_keyExtractor = (item, index) => item.data.id.toString();

	render() {

		this.styles = {
			pickerContainer: {
				flex: 1,
				backgroundColor: "#FFFFFF",
				borderRadius: 5
			},
			title: {
				flexDirection: 'row',
				backgroundColor: "#F5F5F5",
				justifyContent: "center",
				paddingTop: 5,
				paddingBottom: 5,
				paddingLeft: 10,
			},
			titleLeft: {
				width: "70%",
				justifyContent: "center",
				alignItems: "flex-start",
				paddingLeft: 50
			},
			titleRight: {
				width: "30%",
				flexDirection: 'row',
				justifyContent: "flex-end",
				alignItems: "center",
			},
			titleText: {
				color: "#000000",
				fontSize: this.props.fontSize.FontSizeNorman,
				marginRight: 16
			}
		};

		return (
			<View style={this.styles.pickerContainer}>
				<View style={this.styles.title}>
					<View style={this.styles.titleLeft}>
						<Text style={this.styles.titleText}>{this.props.title}</Text>
					</View>
					<View style={this.styles.titleRight}>
						{(this.props.numberSelection === 2) &&
							<Text style={this.styles.titleText}>{this.props.secondChkTitle}</Text>}
						{(this.props.numberSelection === 3) && (
							<Text style={this.styles.titleText}>{this.props.firstChkTitle}</Text>
						)}
						{(this.props.numberSelection === 3) && (
							<Text style={this.styles.titleText}>{this.props.secondChkTitle}</Text>
						)}
					</View>
				</View>
				<FlatList
					data={this.props.data}
					extraData={this.state}
					keyExtractor={this._keyExtractor}
					renderItem={this.renderFeedbackItems}
					style={[this.props.style, { marginLeft: 10, marginRight: 10 }]}
					removeClippedSubviews={true}
				/>
			</View>
		);
	}
}
