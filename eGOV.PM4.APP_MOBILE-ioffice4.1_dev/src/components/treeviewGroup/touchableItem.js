import React, { Component } from 'react';
import { View, TouchableOpacity } from 'react-native';
import { Text } from "native-base"
import PropTypes from 'prop-types';
import { CheckBox, Icon } from 'native-base';
import { AppConfig } from "../../AppConfig"

export default class TouchableItem extends Component {
	static propTypes = {
		showIcon: PropTypes.bool,
		isOpen: PropTypes.bool,
		oneCheck: PropTypes.bool,
		twoCheck: PropTypes.bool,
		threeCheck: PropTypes.bool,
		onItemPress: PropTypes.func.isRequired,
		onOnePress: PropTypes.func,
		onTwoPress: PropTypes.func,
		onThreePress: PropTypes.func,
	}

	static defaultProps = {
		showIcon: false,
		isOpen: false,
		oneCheck: false,
		twoCheck: false,
		threeCheck: false
	}

	shouldComponentUpdate(nextProps, nextState) {
		if (this.props.oneCheck !== nextProps.oneCheck
			|| this.props.twoCheck !== nextProps.twoCheck
			|| this.props.threeCheck !== nextProps.threeCheck
			|| this.props.isOpen !== nextProps.isOpen) {
			return true;
		} else {
			return false;
		}
	}

	renderOrderCheck(data) {
		if (this.props.showIcon) {
			return null
		}

		if (this.props.numberSelection === 2) {
			return (
				<View style={{ alignItems: 'flex-end' }}>
					<CheckBox checked={this.props.twoCheck} color={AppConfig.blueBackground} style={{ width: 23, height: 23, borderRadius: 0, marginRight: 15 }} onPress={_ => this.props.onTwoPress(this.props.item.data)} />
				</View>
			)
		} else if (this.props.numberSelection === 3) {
			return (
				<View style={{ flexDirection: "row", justifyContent: 'flex-end' }}>
					<CheckBox checked={this.props.twoCheck} color={AppConfig.blueBackground} style={{ width: 23, height: 23, borderRadius: 0, marginRight: 15 }} onPress={_ => this.props.onTwoPress(this.props.item.data)} />
					<CheckBox checked={this.props.threeCheck} color={AppConfig.blueBackground} style={{ width: 23, height: 23, borderRadius: 0, marginRight: 15 }} onPress={_ => this.props.onThreePress(this.props.item.data)} />
				</View>
			)
		} else {
			return null
		}
	}

	renderTitle(item) {
		return (
			<Text style={this.styles.regular}>{item.data.text}</Text>
		)
	}

	renderFirstCheckBox() {
		if (!this.props.showIcon) {
			return (
				<CheckBox checked={this.props.oneCheck} color="black" style={{ marginLeft: 16, width: 23, height: 23, borderRadius: 0, }} onPress={_ => this.props.onOnePress(this.props.item.data, this.props.item.childs)} />
			)
		} else {
			return (
				<CheckBox checked={this.props.oneCheck} color="black" style={{ width: 23, height: 23, borderRadius: 0, }} onPress={_ => this.props.onOnePress(this.props.item.data, this.props.item.childs)} />
			)
		}
	}

	render() {

		this.styles = {
			item: {
				flex: 1,
				paddingTop: 10,
				paddingBottom: 10,
				paddingRight: 10,
				borderBottomColor: '#B3B3B3',
				borderBottomWidth: 0.5,
				marginLeft: 5
			},
			text: {
				flex: 1,
				marginLeft: 20,
			},
			regular: {
				color: '#212121',
				fontSize: this.props.fontSize.FontSizeNorman
			},
			italic: {
				fontStyle: 'italic',
				color: '#666666',
			},
			icon: {
				fontSize: this.props.fontSize.FontSizeLarge,
				marginLeft: -2,
				color: "#9E9E9E",
			}
		};

		return (
			<TouchableOpacity onPress={this.props.onItemPress} style={this.styles.item}>
				<View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
					{(this.props.showIcon || this.props.item.data.parent === "N") &&
						<Icon
							type="FontAwesome"
							name={!this.props.isOpen ? 'plus-square' : 'minus-square'}
							style={this.styles.icon} />}
					{this.renderFirstCheckBox()}
					<View style={this.styles.text}>
						{this.renderTitle(this.props.item)}
					</View>
					{this.renderOrderCheck(this.props.item)}
				</View>
			</TouchableOpacity>
		);
	}
}
