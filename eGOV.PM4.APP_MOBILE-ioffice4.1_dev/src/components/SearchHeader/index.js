import React, { Component } from "react"
import { View, TouchableOpacity, StyleSheet, PixelRatio } from "react-native"
import { AppConfig } from "../../AppConfig"
import Icon from "react-native-vector-icons/FontAwesome5"
import CustomTextInput from "../CustomTextInput"

export default class SearchHeader extends Component {
    constructor(props) {
        super(props)
        this.state = {
            keyWord: ""
        }
    }

    onSearchData = () => {
        this.props.onSearchData()
    }

    setKeyWord = (keyWord, callback) => {
        this.setState({ keyWord }, callback)
    }

    getKeyWord = () => {
        return this.state.keyWord
    }

    render() {

        this.styles = StyleSheet.create({
            ViewSearch: {
                top: 0,
                left: 0,
                right: 0,
                height: 40,
                paddingLeft: 10,
                paddingRight: 10,
                backgroundColor: "#F5F5F5",
                flexDirection: "row",
                justifyContent: "center",
            },
            ButtonSearch: {
                top: 0,
                left: 18,
                paddingTop: 7,
                position: "absolute",
                width: 40,
                height: 40,
                zIndex: 999
            },
            TextInputSearch: {
                flex: 1,
                fontFamily: AppConfig.fontFamily,
                fontSize: this.props.fontSize.FontSizeSmall / PixelRatio.getFontScale(),
                color: AppConfig.headerBackgroundColor,
                paddingLeft: 40,
                borderColor: AppConfig.headerBackgroundColor,
                borderWidth: 1,
                borderRadius: 5,
            }
        })

        return (
            <View style={this.styles.ViewSearch}>
                <TouchableOpacity onPress={this.onSearchData} style={this.styles.ButtonSearch}>
                    <Icon name="search" size={22} color={AppConfig.headerBackgroundColor}></Icon>
                </TouchableOpacity>
                <CustomTextInput
                    style={this.styles.TextInputSearch}
                    value={this.state.keyWord}
                    onChangeText={(text) => { this.setState({ keyWord: text }) }}
                    placeholder="Nhập từ khóa tìm kiếm ..."
                    underlineColorAndroid={"transparent"}
                    onRef={ref => this.input = ref}
                    placeholderStyle={{ top: 12, left: 40, fontSize: this.props.fontSize.FontSizeSmall }}
                    onSubmitEditing={this.onSearchData}
                />
            </View>
        )
    }
}