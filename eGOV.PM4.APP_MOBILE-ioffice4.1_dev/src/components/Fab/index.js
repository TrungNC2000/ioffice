import React, { Component } from 'react';
import { FloatingAction } from "react-native-floating-action"

export default class Fab extends Component {
    constructor(props) {
        super(props)
        this.state = {
            active: false
        }
        this.fabActions = this.props.fabActions ? this.props.fabActions : []
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (this.state.active !== nextState.active) {
            return true
        }
        return false
    }

    render() {
        return (
            <FloatingAction
                actions={this.fabActions}
                onPressItem={name => this.props.fabPressItem(name)}
            />
        )
    }
}