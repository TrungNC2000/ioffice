import React, { Component } from 'react';
import { StyleSheet, View, Modal, FlatList, Dimensions } from "react-native"
import { Container, Tabs, Tab, Card, Text, Button, Icon, Toast, Item, Left, Spinner } from "native-base"
import { connect } from "react-redux"
import { AppConfig } from "../AppConfig"
import HeaderWithLeftRight from "./Header/HeaderWithLeftRight"
import TreeChuyen from "./treeview"
import TreeChuyenGroup from "./treeviewGroup"
import { mergeArrayDSChuyen } from "../untils/TextUntils"

class ModalNguoiNhan extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        this.styles = this.props.styles
        return (
            <Modal
                transparent={false}
                animationType="slide"
                visible={this.props.isModalVisible}
                supportedOrientations={["landscape", "landscape-left", "landscape-right", "portrait", "portrait-upside-down"]}
            >
                <View style={{ flex: 1, padding: 15, backgroundColor: AppConfig.grayBackground }}>
                    <View style={{ flex: 1, backgroundColor: "#FFFFFF", borderRadius: 10 }}>
                        <View style={{ position: 'absolute', zIndex: 99, height: 55, left: 0, right: 0, top: 0, justifyContent: "center", padding: 4, alignItems: "center" }}>
                            <Text style={{ fontSize: this.props.fontSize.FontSizeLarge }}>Danh sách cán bộ nhận</Text>
                        </View>
                        <FlatList
                            ref={(ref) => { this.flatListRef = ref }}
                            data={this.props.listMCB_DVCB}
                            keyExtractor={(item, index) => "key" + item.ma + index}
                            style={{ position: 'absolute', left: 0, right: 0, top: 55, bottom: 55 }}
                            initialNumToRender={50}
                            renderItem={({ item, index }) => {
                                return (
                                    <Item key={item.ma} style={{ padding: 12, borderBottomWidth: AppConfig.defaultLineWidth, borderColor: AppConfig.defaultLineColor }}>
                                        <Left>
                                            <Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>{item.ten}</Text>
                                            <Text note style={{ fontSize: this.props.fontSize.FontSizeSmall }}>Chức vụ: {item.ten_chuc_vu}</Text>
                                        </Left>
                                    </Item>
                                )
                            }}
                            ListEmptyComponent={() => {
                                return (
                                    <View style={{ alignItems: "center", marginTop: 10, }}>
                                        <Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>Không có dữ liệu</Text>
                                    </View>
                                )
                            }}
                            removeClippedSubviews={true}
                        />
                        {
                            (this.props.isSending) ? (
                                <View style={{ position: 'absolute', zIndex: 99, height: 55, left: 0, right: 0, bottom: 0, flexDirection: "row", justifyContent: "center", paddingTop: 10, alignItems: "center" }}>
                                    <Spinner />
                                    <Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}> Đang thực hiện</Text>
                                </View>
                            ) : (
                                    <View style={{ position: 'absolute', zIndex: 99, height: 55, left: 0, right: 0, bottom: 0, flexDirection: "row", justifyContent: "space-evenly", paddingTop: 10, alignItems: "center" }}>
                                        <Button small bordered rounded iconLeft onPress={_ => this.props.onPressSend()}><Icon name="done" type="MaterialIcons"></Icon><Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>Gửi</Text></Button>
                                        <Button small danger bordered rounded iconLeft onPress={_ => this.props.toggleModal()}><Icon name="close" type="MaterialIcons"></Icon><Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>Chọn lại</Text></Button>
                                    </View>
                                )
                        }
                    </View>
                </View>
            </Modal>
        )
    }
}

class TreeOneCheck extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isModalVisible: false
        }
        this.listMCB_DVCB = []
        this.listMCB_NCB = []
        this.currentTab = 0
        this.hasChangeTab = false
        Dimensions.addEventListener("change", this.onResizeScreen)
    }

    componentWillUnmount() {
        Dimensions.removeEventListener("change", this.onResizeScreen)
    }

    onResizeScreen = () => {
        setTimeout(() => {
            this.hasChangeTab = false
        }, 500);
    }

    toggleModal = () => {
        if (!this.state.isModalVisible) {
            let listTab1 = []
            let listTab2 = []
            try {
                listTab1 = this.treeChuyen.getDataCanBo()
            } catch (error) {
                console.log("Error treeChuyen.getDataCanBo")
            }

            try {
                listTab2 = this.treeChuyen1.getDataCanBo()
            } catch (error) {
                console.log("Error treeChuyen1.getDataCanBo")
            }

            this.listMCB_DVCB = mergeArrayDSChuyen(listTab1, listTab2)
            if (this.listMCB_DVCB.length > 0) {
                this.setState({ isModalVisible: true })
            } else {
                Toast.show({ text: "Vui lòng chọn cán bộ nhận!", type: "warning", buttonText: "Đóng", position: "top", duration: 2000 })
            }
        } else {
            this.setState({ isModalVisible: false })
        }
    }

    onPressSend = () => {
        this.props.onPressSend(this.listMCB_DVCB)
    }

    render() {

        this.styles = StyleSheet.create({
            tabsContainerStyle: {
                width: 300
            },
            tabStyle: {
                flex: 1,
                backgroundColor: AppConfig.blueBackground
            },
            activeTabStyle: {
                backgroundColor: AppConfig.blueBackground
            },
            textStyle: {
                fontSize: this.props.fontSize.FontSizeNorman,
                color: "#FFFFFF",
                opacity: 0.6,
            },
            activeTextStyle: {
                fontSize: this.props.fontSize.FontSizeNorman,
                color: "#FFFFFF",
                opacity: 1,
            }
        })

        let { title, buttonLeftName, buttonRightName, onPressLeftButton, dataTreeDVCB, dataTreeNCB, isLoading } = this.props
        return (
            <Container style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
                <HeaderWithLeftRight
                    title={title}
                    buttonLeftName={buttonLeftName}
                    buttonRightName={buttonRightName}
                    onPressLeftButton={onPressLeftButton}
                    onPressRightButton={this.toggleModal}
                />
                {
                    (this.state.isModalVisible) && (
                        <ModalNguoiNhan
                            listMCB_DVCB={this.listMCB_DVCB}
                            isModalVisible={this.state.isModalVisible}
                            toggleModal={() => this.toggleModal()}
                            onPressSend={this.onPressSend}
                            isSending={this.props.isSending}
                            fontSize={this.props.fontSize}
                            styles={this.styles}
                        />
                    )
                }
                <View style={{ flex: 1 }}>
                    <Tabs onChangeTab={() => { this.hasChangeTab = true }}>
                        <Tab
                            tabStyle={this.styles.tabStyle}
                            activeTabStyle={this.styles.activeTabStyle}
                            textStyle={this.styles.textStyle}
                            activeTextStyle={this.styles.activeTextStyle}
                            heading="Đơn vị/Cán bộ"
                        >
                            <Card noShadow style={{ flex: 1, borderWidth: 1 }}>
                                {
                                    (isLoading) && (
                                        <Spinner />
                                    )
                                }
                                {
                                    (!isLoading) && (
                                        <TreeChuyen
                                            ref={tree => { this.treeChuyen = tree }}
                                            title="Họ tên"
                                            firstChkTitle="XLC"
                                            secondChkTitle="PH"
                                            numberSelection={1}
                                            data={dataTreeDVCB}
                                            onItemPress={(ma, once) => { console.log("press item: " + ma) }}
                                            fontSize={this.props.fontSize}
                                        />
                                    )
                                }
                            </Card>
                        </Tab>
                        <Tab
                            tabStyle={this.styles.tabStyle}
                            activeTabStyle={this.styles.activeTabStyle}
                            textStyle={this.styles.textStyle}
                            activeTextStyle={this.styles.activeTextStyle}
                            heading="Nhóm cán bộ"
                        >
                            <Card noShadow style={{ flex: 1, borderWidth: 1 }}>
                                <TreeChuyenGroup
                                    ref={tree1 => { this.treeChuyen1 = tree1 }}
                                    title="Họ tên"
                                    firstChkTitle="XLC"
                                    secondChkTitle="PH"
                                    numberSelection={1}
                                    data={dataTreeNCB}
                                    onItemPress={(ma, once) => { console.log("press item: " + ma) }}
                                    fontSize={this.props.fontSize}
                                />
                            </Card>
                        </Tab>
                    </Tabs>
                </View>
            </Container>
        );
    }
}

function mapStateToProps(state) {
    return {
        fontSize: state.fontSize,
    }
}

export default connect(mapStateToProps)(TreeOneCheck)
