import React, { Component } from 'react';
import { ScrollView, StyleSheet, Alert, PixelRatio } from "react-native"
import { Container, Text, CardItem, Textarea, CheckBox, Toast } from "native-base"
import { connect } from "react-redux"
import API from "../../networks"
import { AppConfig } from "../../AppConfig"
import { StackActions } from "react-navigation"
import HeaderWithLeftRight from "../../components/Header/HeaderWithLeftRight"
import { resetStack, ToastSuccess } from "../../untils/TextUntils"

class VBDen_NhapYKien extends Component {
    constructor(props) {
        super(props)
        this.state = {
            yKienXuLy: "",
            checkHoanTat: true,
        }
        this.ma_xu_ly_den = 0
    }

    componentDidMount = () => {
        this.ma_xu_ly_den = this.props.navigation.getParam("ma_xu_ly_den")
    }

    oncheckHoanTat = () => {
        this.setState({ checkHoanTat: !this.state.checkHoanTat })
    }

    onPressSend = () => {
        let ma_xu_ly_den = this.ma_xu_ly_den
        let ma_ctcb = global.ma_ctcb_kc
        let { yKienXuLy, checkHoanTat } = this.state
        Alert.alert("Xác nhận", "Hoàn tất thêm ý kiến xử lý?",
            [
                { text: "Không", style: "cancel" },
                {
                    text: "Đồng ý", onPress: () => {
                        API.VanBanDen.AU_VBDEN_CVXVBD(ma_xu_ly_den, ma_ctcb).then((result) => {
                            API.VanBanDen.AU_VBDEN_CVCNYKXL(ma_xu_ly_den, yKienXuLy).then((response) => {
                                if (response) {
                                    if (checkHoanTat) {
                                        API.VanBanDen.AU_VBDEN_CVXLNVBD(ma_xu_ly_den, ma_ctcb).then((response1) => {
                                            if (response1) {
                                                ToastSuccess("Hoàn tất văn bản thành công!", () => {
                                                    API.Login.AU_DSNVCB(global.ma_ctcb_kc, this.props.dispatch).then(() => {
                                                        this.props.navigation.dispatch(resetStack("VBDenXuLy"))
                                                    })
                                                })
                                            } else {
                                                Toast.show({ text: "Xảy ra lỗi. Vui lòng thực hiện lại sau!", type: "danger", buttonText: "Đóng", position: "top", duration: 2000 })
                                            }
                                        })
                                    } else {
                                        this.props.navigation.dispatch(resetStack("VBDenXuLy"))
                                    }
                                } else {
                                    Toast.show({ text: "Xảy ra lỗi. Vui lòng thực hiện lại sau!", type: "danger", buttonText: "Đóng", position: "top", duration: 2000 })
                                }
                            })
                        })
                    }
                }
            ],
            { cancelable: false }
        )
    }

    render() {
        const popAction = StackActions.pop({ n: 1 })

        return (
            <Container style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
                <HeaderWithLeftRight
                    title={"Thêm ý kiến xử lý"}
                    buttonLeftName="arrow-back"
                    buttonRightName="done"
                    onPressLeftButton={() => { this.props.navigation.dispatch(popAction) }}
                    onPressRightButton={this.onPressSend}
                />
                <ScrollView style={styles.scrollContainer}>
                    <Text style={[styles.TextTitle]}>Ý kiến xử lý</Text>
                    <Textarea
                        autoCorrect={false}
                        style={styles.TextInput}
                        autoFocus={true}
                        rowSpan={9}
                        value={this.state.yKienXuLy}
                        onChangeText={(text) => { this.setState({ yKienXuLy: text }) }}
                    />
                    <CardItem style={[styles.CardItemRight, { marginLeft: -10 }]}>
                        <CheckBox onPress={this.oncheckHoanTat} checked={this.state.checkHoanTat} color={AppConfig.blueBackground}></CheckBox>
                        <Text style={[styles.TextTitle, { marginLeft: 20 }]}>Hoàn tất văn bản</Text>
                    </CardItem>
                </ScrollView>
            </Container >
        );
    }
}

const styles = StyleSheet.create({
    scrollContainer: {
        flex: 1,
        marginTop: 10,
        marginLeft: 10,
        marginRight: 10
    },
    CardItem: {
        paddingTop: 10,
        paddingBottom: 10,
        alignItems: 'center'
    },
    CardItemLeft: {
        width: 90
    },
    CardItemRight: {
        flex: 1,
    },
    TextTitle: {
        fontSize: AppConfig.FontSizeSmall,
        margin: 5
    },
    TextInput: {
        flex: 1,
        fontFamily: AppConfig.fontFamily,
        fontSize: AppConfig.FontSizeNorman / PixelRatio.getFontScale(),
        color: "#000000",
        borderColor: AppConfig.defaultLineColor,
        borderWidth: AppConfig.defaultLineWidth,
        borderRadius: 5,
        marginRight: 2,
        paddingLeft: 5,
        paddingTop: 7,
        minHeight: 40,
        margin: 5
    },
})

export default connect()(VBDen_NhapYKien)