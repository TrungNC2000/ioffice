import React, { Component } from 'react';
import { View, ScrollView, StyleSheet, Alert, TouchableOpacity } from "react-native"
import { Container, Icon, Button, Text, Card, CardItem, Toast } from "native-base"
import { StackActions } from "react-navigation"
import { connect } from "react-redux"
import { AppConfig } from "../../AppConfig"
import { RemoveHTMLTag, resetStack, ToastSuccess, setScheduleNotification, checkVersionWeb } from "../../untils/TextUntils"
import HeaderWithLeft from "../../components/Header/HeaderWithLeft"
import API from "../../networks"
import * as Models from "../../Models"
import ViewFile from "../../components/ViewFile"
import SignInfo from "../../components/SignInfo"

class Table_ChiTiet_VBDEN extends Component {
    constructor(props) {
        super(props)
        this.state = {
            strVBLQ: ""
        }
        this.fabActions_DuyetVBDen = [
            {
                text: "Duyệt",
                name: "btnDuyet",
                position: 1
            },
            {
                text: "Chuyển lãnh đạo khác",
                name: "btnDuyetChuyenLDK",
                position: 2
            }
        ];
        this.fabActions_DaDuyetVBDen = [
            {
                text: "Chuyển tiếp",
                name: "btnLDChuyen",
                position: 1
            }
        ];
        this.fabActions_XuLyVBDen = [
            {
                text: "Chuyển",
                name: "btnChuyen",
                position: 1
            },
            {
                text: "Chuyển đang xử lý",
                name: "btnDangXuLy",
                position: 2
            },
            {
                text: "Hoàn thành",
                name: "btnHoanThanh",
                position: 3
            }
        ];
        this.fabActions_DangXuLyVBDen = [
            {
                text: "Chuyển",
                name: "btnChuyen",
                position: 1
            },
            {
                text: "Hoàn thành",
                name: "btnHoanThanh",
                position: 2
            }
        ];
        this.fabActions_DaXuLyVBDen = [
            {
                text: "Chuyển",
                name: "btnChuyen",
                position: 1
            }
        ];
        this.fabActions_XuLyVBDen1 = [
            {
                text: "Hoàn thành",
                name: "btnHoanThanh",
                position: 1
            }
        ];
        this.fabActions_DangXuLyVBDen1 = [
            {
                text: "Hoàn thành",
                name: "btnHoanThanh",
                position: 1
            }
        ];
        this.fabActions_CVTheoDoi = [
            {
                text: "Theo dõi VB",
                name: "btnCVtheodoi",
                position: 1
            }
        ]
    }

    componentWillReceiveProps(nextProps) {
        this.getVBLQ(nextProps)
    };

    getVBLQ = (nextProps) => {
        this.setState({ strVBLQ: "" })
        if (nextProps.dataDetails.length > 0) {
            let ma_van_ban = nextProps.dataDetails[0].ma_van_ban_kc
            API.VanBanDen.AU_VANBAN_DSVBLQDK(ma_van_ban).then((strVBLQ) => {
                if (global.vbdi_an_hien_vblq_ngoai_he_thong == 1 && nextProps.dataDetails[0].vblq_ngoai_he_thong) {
                    this.setState({ strVBLQ: strVBLQ + ':' + nextProps.dataDetails[0].vblq_ngoai_he_thong })
                } else {
                    this.setState({ strVBLQ })
                }
            })
        }
    }

    fabActions = () => {
        let { activeTab, isDuyet, dataDetails } = this.props
        if (dataDetails.length > 0) {
            let ma_yeu_cau = dataDetails[0].ma_yeu_cau
            if (isDuyet) {
                if (activeTab < 2) {
                    return this.fabActions_DuyetVBDen
                }
                if (activeTab === 2) {
                    return this.fabActions_DaDuyetVBDen
                }
            } else {
                if (ma_yeu_cau !== 1) {
                    if (activeTab === 0) {
                        return this.fabActions_XuLyVBDen
                    }
                    if (activeTab === 1) {
                        return this.fabActions_DangXuLyVBDen
                    }
                    if (activeTab === 2) {
                        return this.fabActions_DaXuLyVBDen
                    }
                } else {
                    if (activeTab === 0) {
                        return this.fabActions_XuLyVBDen1
                    }
                    if (activeTab === 1) {
                        return this.fabActions_DangXuLyVBDen1
                    }
                }

            }
            if (global.vbde_cv_theodoi_vb == 1) {
                var btnTheoDoi = {
                    text: "Theo dõi VB",
                    name: "btnCVtheodoi",
                    position: 1
                }
                this.fabActions_DuyetVBDen.push(btnTheoDoi)
                this.fabActions_DaDuyetVBDen.push(btnTheoDoi)
                this.fabActions_XuLyVBDen.push(btnTheoDoi)
                this.fabActions_DangXuLyVBDen.push(btnTheoDoi)
                this.fabActions_DaXuLyVBDen.push(btnTheoDoi)
            }
        }
    }

    render() {
        this.styles = this.props.styles
        if (this.props.dataDetails.length > 0) {
            const { trich_yeu, so_ky_hieu, so_den,
                ngay_den, ngay_ban_hanh, ten_co_quan_ban_hanh, noi_luu_ban_chinh,
                ten_nguoi_duyet, ngay_duyet, han_xu_ly_vn, ten_so_vb_den,
                ten_loai_van_ban, ten_cap_do_khan, ten_cap_do_mat, ma_dinh_danh_vb,
                file_van_ban_bs, ten_yeu_cau, ma_xu_ly_den, ghi_chu } = this.props.dataDetails[0]
            let fabActionsTemp = this.fabActions()
            return (
                <Card noShadow style={this.styles.Card}>
                    {
                        (trich_yeu !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Trích yếu:</Text>
                                <Text style={[this.styles.textRight, { fontWeight: "500" }]}>{RemoveHTMLTag(trich_yeu)}</Text>
                            </CardItem>
                        )
                    }
                    {
                        (so_ky_hieu !== null && so_den !== null) && (
                            <CardItem style={[this.styles.CardItem, { flexWrap: "wrap", flexShrink: 1 }]}>
                                <Text style={this.styles.textLeft}>Số ký hiệu:</Text>
                                <Text style={[this.styles.textRight, { color: "red" }]}>
                                    {so_ky_hieu} &nbsp;&nbsp;
                                </Text>
                                <CardItem style={[this.styles.CardItem, { paddingLeft: -4, paddingBottom: -5 }]}>
                                    <Text style={this.styles.textLeft}>Số đến:</Text>
                                    <Text style={[this.styles.textRight]}>
                                        {so_den}
                                    </Text>
                                </CardItem>
                            </CardItem>
                        )
                    }
                    {
                        (so_ky_hieu === null && so_den !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Số đến:</Text>
                                <Text style={[this.styles.textRight]}>
                                    {so_den}
                                </Text>
                            </CardItem>
                        )
                    }
                    {
                        (ngay_den !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Ngày đến:</Text>
                                <Text style={[this.styles.textRight]}>
                                    {ngay_den}
                                </Text>
                            </CardItem>
                        )
                    }
                    {
                        (ngay_ban_hanh !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Ngày ban hành:</Text>
                                <Text style={[this.styles.textRight, { color: AppConfig.blueBackground }]}>
                                    {ngay_ban_hanh}
                                </Text>
                            </CardItem>
                        )
                    }
                    {
                        (ten_co_quan_ban_hanh !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Cơ quan ban hành:</Text>
                                <Text style={[this.styles.textRight]}>{ten_co_quan_ban_hanh}</Text>
                            </CardItem>
                        )
                    }
                    {
                        (noi_luu_ban_chinh !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Nơi lưu bản chính:</Text>
                                <Text style={[this.styles.textRight]}>{noi_luu_ban_chinh}</Text>
                            </CardItem>
                        )
                    }
                    {
                        (ten_nguoi_duyet !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Lãnh đạo phê duyệt:</Text>
                                <Text style={[this.styles.textRight, { color: AppConfig.blueBackground }]}>
                                    {ten_nguoi_duyet}
                                </Text>
                            </CardItem>
                        )
                    }
                    {
                        (ngay_duyet !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Ngày duyệt:</Text>
                                <Text style={[this.styles.textRight, { color: AppConfig.blueBackground }]}>
                                    {ngay_duyet}
                                </Text>
                            </CardItem>
                        )
                    }
                    {
                        (han_xu_ly_vn !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Hạn xử lý:</Text>
                                <Text style={[this.styles.textRight, { color: "red" }]}>
                                    {han_xu_ly_vn}
                                </Text>
                                <Icon type="MaterialIcons" name="notifications" style={{ fontSize: this.props.fontSize.FontSizeXLarge + 2, color: "#BDBDBD", right: 0, position: 'absolute', }} onPress={async () => {
                                    let id = "NotifyHXLVBDEN" + ma_xu_ly_den
                                    let date = han_xu_ly_vn + " " + "08:00"
                                    let title = "Xử lý văn bản đến ngày " + date
                                    let noi_dung = RemoveHTMLTag(so_ky_hieu + " - " + trich_yeu)
                                    let action = "NotifyHXLVBDEN"
                                    setScheduleNotification(id, title, noi_dung, date, action)
                                }}
                                />
                            </CardItem>
                        )
                    }
                    {
                        (ten_loai_van_ban !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Loại công văn:</Text>
                                <Text style={[this.styles.textRight]}>
                                    {ten_loai_van_ban}
                                </Text>
                            </CardItem>
                        )
                    }
                    {
                        (ten_so_vb_den !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Sổ văn bản:</Text>
                                <Text style={[this.styles.textRight]}>
                                    {ten_so_vb_den}
                                </Text>
                            </CardItem>
                        )
                    }
                    {
                        (ten_cap_do_khan !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Cấp độ khẩn:</Text>
                                <Text style={[this.styles.textRight]}>
                                    {ten_cap_do_khan}
                                </Text>
                            </CardItem>
                        )
                    }
                    {
                        (ten_cap_do_mat !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Cấp độ mật:</Text>
                                <Text style={[this.styles.textRight]}>
                                    {ten_cap_do_mat}
                                </Text>
                            </CardItem>
                        )
                    }
                    {
                        (ma_dinh_danh_vb !== null && ma_dinh_danh_vb !== "Không có thông tin") && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Định danh:</Text>
                                <Text style={[this.styles.textRight]}>{ma_dinh_danh_vb}</Text>
                            </CardItem>
                        )
                    }
                    {
                        (ten_yeu_cau !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Vai trò:</Text>
                                <Text style={[this.styles.textRight]}>
                                    {ten_yeu_cau}
                                </Text>
                            </CardItem>
                        )
                    }
                    {
                        (ghi_chu !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Ghi chú:</Text>
                                <Text style={[this.styles.textRight]}>
                                    {ghi_chu}
                                </Text>
                            </CardItem>
                        )
                    }
                    {
                        (file_van_ban_bs !== null) && (
                            <View>
                                <CardItem style={this.styles.CardItem}>
                                    <Text style={this.styles.textLeft}>Tệp tin đính kèm:</Text>
                                </CardItem>
                                <CardItem style={this.styles.CardItem}>
                                    <ViewFile
                                        ref={ref => { this.ViewFile = ref }}
                                        src_file={file_van_ban_bs}
                                        isUpdateViewCV={this.props.isUpdateViewCV}
                                        updateViewCV={this.props.updateViewCV}
                                        fontSize={this.props.fontSize}
                                        fabActions={fabActionsTemp}
                                        fabPressItem={(name) => {
                                            this.ViewFile.toggleModal()
                                            switch (name) {
                                                case "btnDuyet":
                                                    setTimeout(() => {
                                                        this.props.onApproved()
                                                    }, 200);
                                                    break;
                                                case "btnDuyetChuyenLDK":
                                                    setTimeout(() => {
                                                        this.props.onForwardLDK()
                                                    }, 200);
                                                    break;
                                                case "btnLDChuyen":
                                                    setTimeout(() => {
                                                        this.props.onLDForward()
                                                    }, 200);
                                                    break;
                                                case "btnChuyen":
                                                    setTimeout(() => {
                                                        this.props.onForward()
                                                    }, 200);
                                                    break;
                                                case "btnDangXuLy":
                                                    setTimeout(() => {
                                                        this.props.onInProcess()
                                                    }, 200);
                                                    break;
                                                case "btnHoanThanh":
                                                    setTimeout(() => {
                                                        this.props.onSuccess()
                                                    }, 200);
                                                    break;
                                                default:
                                                    break;
                                            }
                                        }}
                                    />
                                </CardItem>
                            </View>
                        )
                    }
                    {
                        (this.state.strVBLQ !== "") && (
                            <View>
                                <CardItem style={this.styles.CardItem}>
                                    <Text style={this.styles.textLeft}>Văn bản liên quan:</Text>
                                </CardItem>
                                <CardItem style={this.styles.CardItem}>
                                    <ViewFile
                                        src_file={this.state.strVBLQ}
                                        navigation={this.props.navigation}
                                        fontSize={this.props.fontSize}
                                    />
                                </CardItem>
                            </View>
                        )
                    }

                    {this.props.renderButton()}
                </Card>
            )
        } else {
            return <View></View>
        }
    }
}

class Table_ButPhe_VBDEN extends Component {
    getChuoiNguoiNhan = (arr_ten_nguoi_nhan) => {
        let arr = []
        for (let item of arr_ten_nguoi_nhan) {
            let ten_yeu_cau = ""
            switch (item.ma_yeu_cau) {
                case 1:
                    ten_yeu_cau = " (XĐB)"
                    break;
                case 2:
                    ten_yeu_cau = " (XLC)"
                    break;
                case 3:
                    ten_yeu_cau = " (PHXL)"
                    break;
                default:
                    ten_yeu_cau = ""
                    break;
            }
            arr.push(item.ten_don_vi_nhan + "(" + item.ho_va_ten_can_bo_nhan + ten_yeu_cau + ")")
        }
        return arr.join(", ")
    }

    renderDataNote = (dataNotes) => {
        let result = []
        for (let item of dataNotes) {
            result.push(
                <View key={item.ma_xu_ly_den + item.ngay_nhan} style={{ paddingTop: 5, paddingBottom: 10, borderColor: AppConfig.defaultLineColor, borderTopWidth: AppConfig.defaultLineWidth }}>
                    <CardItem style={this.styles.CardItem}>
                        <Text style={[this.styles.textRight, { color: AppConfig.blueBackground }]}>{item.ho_va_ten_can_bo} - {item.ten_don_vi} ({item.ngay_nhan})</Text>
                    </CardItem>
                    {
                        (checkVersionWeb(40) && item.ten_nguoi_nhan.length > 0) ? (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={[this.styles.textRight]}>
                                    Chuyển: {this.getChuoiNguoiNhan(item.ten_nguoi_nhan)}
                                </Text>
                            </CardItem>
                        ) : (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={[this.styles.textRight]}>
                                    Chuyển: {item.ten_nguoi_nhan.replace("  ", " ")}
                                </Text>
                            </CardItem>
                        )
                    }
                    {
                        (item.noi_dung_chuyen && item.noi_dung_chuyen.trim() !== "") && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={[this.styles.textRight, { color: "red" }]}>
                                    Nội dung: {RemoveHTMLTag(item.noi_dung_chuyen)}
                                </Text>
                            </CardItem>
                        )
                    }
                </View>
            )
        }
        return result
    }

    render() {
        this.styles = this.props.styles
        if (this.props.dataNotes.length > 0) {
            return (
                <Card noShadow style={this.styles.Card}>
                    <CardItem style={this.styles.CardItem}>
                        <Text style={[this.styles.textRight, { fontSize: this.props.fontSize.FontSizeLarge, fontWeight: "bold", }]}>
                            Tổng hợp ý kiến xử lý
                        </Text>
                    </CardItem>
                    {this.renderDataNote(this.props.dataNotes)}
                </Card>
            )
        } else {
            return (
                <View></View>
            )
        }
    }
}

class VBDen_Details extends Component {
    constructor(props) {
        super(props)
        this.state = {
            dataDetails: [],
            dataNotes: [],
            index: 0
        }

        this.isDuyet = false
        this.ma_xu_ly_den = 0
        this.ma_van_ban_den = 0
        this.activeTab = 2
        this.isFromNotify = false
        this.dataSource = []
    }

    componentDidMount = () => {
        this.isDuyet = this.props.navigation.getParam("isDuyet", false)
        this.activeTab = this.props.navigation.getParam("activeTab", 2)
        this.isFromNotify = this.props.navigation.getParam("isFromNotify", false)
        this.dataSource = this.props.navigation.getParam("dataSource", [])
        let index = this.props.navigation.getParam("index", 0)
        this.loadData(index)
    }

    loadData = (index) => {
        this.ma_xu_ly_den = this.dataSource[index].ma_xu_ly_den
        this.ma_van_ban_den = this.dataSource[index].ma_van_ban_den_kc
        this.setState({ index, dataDetails: [], dataNotes: [] })
        this.getDetails()
        this.getNotes()
    }

    getDetails = async () => {
        let ma_xu_ly_den = this.ma_xu_ly_den
        let ma_van_ban_den = this.ma_van_ban_den
        let ma_ctcb = global.ma_ctcb_kc
        await API.VanBanDen.AU_VBDEN_VBDCT(ma_xu_ly_den, ma_van_ban_den, ma_ctcb).then((dataDetails) => {
            this.setState({ dataDetails })
        })
    }

    getNotes = async () => {
        let ma_xu_ly_den = this.ma_xu_ly_den
        let ma_van_ban_den = this.ma_van_ban_den
        await API.VanBanDen.AU_VBDEN_BPLDVBD(ma_xu_ly_den, ma_van_ban_den).then((dataNotes) => {
            this.setState({ dataNotes })
        })
    }

    onCVtheoDoiVB = () => {
        let ma_xu_ly_den = this.ma_xu_ly_den
        let ma_van_ban_den = this.ma_van_ban_den
        Alert.alert("Xác nhận", "Xác nhận theo dõi văn bản này?", [
            { text: "Không", style: "cancel" },
            {
                text: "Đồng ý", onPress: () => {
                    API.VanBanDen.AU_VBDEN_CVTHEODOI(ma_van_ban_den, ma_xu_ly_den).then((response) => {
                        if (response[0].result > 0) {
                            ToastSuccess("Theo dõi văn bản thành công!", () => {
                                API.Login.AU_DSNVCB(global.ma_ctcb_kc, this.props.dispatch)

                            })
                        } else {
                            Toast.show({ text: "Văn bản đã được theo dõi, không thể theo dõi thêm !", type: "warning", buttonText: "Đóng", position: "top", duration: 2000 })
                        }
                    })
                }

            }],
            { cancelable: false }
        );
    }

    onSuccess = (ma_xu_ly_den, flagNavigate) => {
        if (this.isFromNotify) {
            this.onSuccessInDetailCV(ma_xu_ly_den)
        } else {
            this.props.navigation.state.params.onSuccess(ma_xu_ly_den, flagNavigate)
        }
    }

    onInProcess = (ma_xu_ly_den, flagNavigate) => {
        if (this.isFromNotify) {
            this.onInProcessInDetailCV(ma_xu_ly_den)
        } else {
            this.props.navigation.state.params.onInProcess(ma_xu_ly_den, flagNavigate)
        }
    }

    onSuccessInDetailCV = (ma_xu_ly_den) => {
        Alert.alert("Xác nhận", "Hoàn thành văn bản này?",
            [
                { text: "Không", style: "cancel" },
                {
                    text: "Đồng ý", onPress: () => {
                        let ma_ctcb = global.ma_ctcb_kc
                        API.VanBanDen.AU_VBDEN_CVXVBD(ma_xu_ly_den, ma_ctcb).then((result) => {
                            API.VanBanDen.AU_VBDEN_CVXLNVBD(ma_xu_ly_den, ma_ctcb).then((response) => {
                                if (response) {
                                    ToastSuccess("Hoàn tất văn bản thành công!", () => {
                                        API.Login.AU_DSNVCB(global.ma_ctcb_kc, this.props.dispatch)
                                        this.props.navigation.dispatch(resetStack("VBDenXuLy"))
                                    })
                                } else {
                                    Toast.show({ text: "Xảy ra lỗi. Vui lòng thực hiện lại sau!", type: "danger", buttonText: "Đóng", position: "top", duration: 2000 })
                                }
                            })
                        })
                    }
                }
            ],
            { cancelable: false }
        )
    }

    onInProcessInDetailCV = (ma_xu_ly_den) => {
        Alert.alert("Xác nhận", "Chuyển đang xử lý?",
            [
                { text: "Không", style: "cancel" },
                {
                    text: "Đồng ý", onPress: () => {
                        let ma_ctcb = global.ma_ctcb_kc
                        let noi_dung_xu_ly = ""
                        let trang_thai_xu_ly = 2 //Đang XL
                        API.VanBanDen.AU_VBDEN_CVXLVBD(ma_ctcb, ma_xu_ly_den, noi_dung_xu_ly, trang_thai_xu_ly).then((response) => {
                            if (response) {
                                ToastSuccess("Chuyển đang xử lý thành công!", () => {
                                    API.Login.AU_DSNVCB(global.ma_ctcb_kc, this.props.dispatch)
                                    this.props.navigation.dispatch(resetStack("VBDenXuLy"))
                                })
                            } else {
                                Toast.show({ text: "Xảy ra lỗi. Vui lòng thực hiện lại sau!", type: "danger", buttonText: "Đóng", position: "top", duration: 2000 })
                            }
                        })
                    }
                }
            ],
            { cancelable: false }
        )
    }

    //Test giao diện hợp nhất
    onApproved = () => {
        if (this.state.dataDetails.length === 0) {
            return null
        }
        let npVBDen_Details_Chuyen = new Models.npVBDen_Details_Chuyen
        npVBDen_Details_Chuyen.isDuyet = true
        npVBDen_Details_Chuyen.ma_van_ban_den = this.state.dataDetails[0].ma_van_ban_den_kc
        npVBDen_Details_Chuyen.ma_xu_ly_den = this.state.dataDetails[0].ma_xu_ly_den
        npVBDen_Details_Chuyen.ma_ctcb_gui = global.ma_ctcb_kc
        npVBDen_Details_Chuyen.ma_yeu_cau = this.state.dataDetails[0].ma_yeu_cau
        npVBDen_Details_Chuyen.trich_yeu = this.state.dataDetails[0].trich_yeu
        npVBDen_Details_Chuyen.activeTab = this.activeTab
        this.props.navigation.navigate("VBDen_Chuyen_Duyet", {
            inVBDEN_Details_Chuyen: npVBDen_Details_Chuyen,
            file_van_ban_bs: global.AU_ROOT.includes('nghean-api') ? this.state.dataDetails[0].file_van_ban_bs : null
        })
    }

    onLDForward = () => {
        if (this.state.dataDetails.length === 0) {
            return null
        }
        let npVBDen_Details_Chuyen = new Models.npVBDen_Details_Chuyen
        npVBDen_Details_Chuyen.isDuyet = true
        npVBDen_Details_Chuyen.ma_van_ban_den = this.state.dataDetails[0].ma_van_ban_den_kc
        npVBDen_Details_Chuyen.ma_xu_ly_den = this.state.dataDetails[0].ma_xu_ly_den
        npVBDen_Details_Chuyen.ma_ctcb_gui = global.ma_ctcb_kc
        npVBDen_Details_Chuyen.ma_yeu_cau = this.state.dataDetails[0].ma_yeu_cau
        npVBDen_Details_Chuyen.trich_yeu = this.state.dataDetails[0].trich_yeu
        npVBDen_Details_Chuyen.activeTab = this.activeTab
        this.props.navigation.navigate("VBDen_ChuyenTiep_LD", {
            inVBDEN_Details_Chuyen: npVBDen_Details_Chuyen,
            file_van_ban_bs: global.AU_ROOT.includes('nghean-api') ? this.state.dataDetails[0].file_van_ban_bs : null
        })
    }

    onForwardLDK = () => {
        if (this.state.dataDetails.length === 0) {
            return null
        }
        let npVBDen_Details_LDChuyenLDK = new Models.npVBDen_Details_LDChuyenLDK
        npVBDen_Details_LDChuyenLDK.isDuyet = true
        npVBDen_Details_LDChuyenLDK.ma_van_ban_den = this.state.dataDetails[0].ma_van_ban_den_kc
        npVBDen_Details_LDChuyenLDK.ma_xu_ly_den = this.state.dataDetails[0].ma_xu_ly_den
        npVBDen_Details_LDChuyenLDK.ma_ctcb_gui = global.ma_ctcb_kc
        npVBDen_Details_LDChuyenLDK.ma_yeu_cau = this.state.dataDetails[0].ma_yeu_cau
        npVBDen_Details_LDChuyenLDK.trich_yeu = this.state.dataDetails[0].trich_yeu
        this.props.navigation.navigate("VBDen_Details_LDChuyenLDK", {
            inVBDen_Details_LDChuyenLDK: npVBDen_Details_LDChuyenLDK
        })
    }

    //Test giao diện hợp nhất
    onForward = () => {
        if (this.state.dataDetails.length === 0) {
            return null
        }
        let npVBDen_Details_Chuyen = new Models.npVBDen_Details_Chuyen
        npVBDen_Details_Chuyen.isDuyet = false
        npVBDen_Details_Chuyen.ma_van_ban_den = this.state.dataDetails[0].ma_van_ban_den_kc
        npVBDen_Details_Chuyen.ma_xu_ly_den = this.state.dataDetails[0].ma_xu_ly_den
        npVBDen_Details_Chuyen.ma_ctcb_gui = global.ma_ctcb_kc
        npVBDen_Details_Chuyen.ma_yeu_cau = this.state.dataDetails[0].ma_yeu_cau === "null" ? 1 : this.state.dataDetails[0].ma_yeu_cau
        npVBDen_Details_Chuyen.ma_yeu_cau_real = this.state.dataDetails[0].ma_yeu_cau === "null" ? 1 : this.state.dataDetails[0].ma_yeu_cau
        npVBDen_Details_Chuyen.activeTab = this.activeTab
        if (!this.isDuyet &&
            this.activeTab === 0 &&
            this.state.dataDetails[0].ma_yeu_cau === 1 &&
            global.vai_tro_xdb_duoc_chuyen_tiep === "1") {
            if (global.vbde_xdb_chuyen_tiep_pto === "1") {
                npVBDen_Details_Chuyen.ma_yeu_cau = 1
            } else {
                npVBDen_Details_Chuyen.ma_yeu_cau = 3
            }
        }
        if (this.state.dataDetails[0].ma_yeu_cau === 5) {
            //Bổ sung trạng thái chuyển "Bị trả lại giống XLC"
            npVBDen_Details_Chuyen.ma_yeu_cau = 2
        }
        npVBDen_Details_Chuyen.trich_yeu = this.state.dataDetails[0].trich_yeu
        this.props.navigation.navigate("VBDen_Chuyen_XuLy", {
            inVBDEN_Details_Chuyen: npVBDen_Details_Chuyen
        })
    }

    onNhapYKien = () => {
        if (this.state.dataDetails.length === 0) {
            return null
        }
        this.props.navigation.navigate("VBDen_NhapYKien", { ma_xu_ly_den: this.state.dataDetails[0].ma_xu_ly_den })
    }

    onLDNhapYKien = () => {
        if (this.state.dataDetails.length === 0) {
            return null
        }
        this.props.navigation.navigate("VBDen_LDNhapYKien", {
            ma_xu_ly_den: this.state.dataDetails[0].ma_xu_ly_den,
            ma_van_ban_den: this.state.dataDetails[0].ma_van_ban_den_kc
        })
    }

    updateViewCV = () => {
        if (this.isFromNotify) {
            this.updateViewCVInDetails()
        } else {
            this.props.navigation.state.params.updateViewCV()
        }
    }

    updateViewCVInDetails = async () => {
        let chuoi_ma_xu_ly_den = this.state.dataDetails[0].ma_xu_ly_den
        let ma_ctcb = global.ma_ctcb_kc
        await API.VanBanDen.AU_VBDEN_CVXVBD(chuoi_ma_xu_ly_den, ma_ctcb)
    }

    isUpdateViewCV = () => {
        if (this.isFromNotify) {
            return true
        } else {
            return this.props.navigation.state.params.isUpdateViewCV
        }
    }

    renderBtnHoanThanh = () => {
        if (global.vbde_xlc_phai_chuyen_ld_pto == 1) {
            //Bắt theo luồng PTO
            if (global.lanh_dao === 1 || this.state.dataDetails[0].ma_yeu_cau === 1) {
                //Nếu là lãnh đạo hoặc vai trò = XĐB thì xử lý bình thường
                return (<Button success style={this.styles.Button} onPress={() => { this.onSuccess(this.ma_xu_ly_den, true) }}><Text style={this.styles.ButtonText}>Hoàn thành</Text></Button>)
            } else {
                return null
            }
        } else {
            //Luồng thông thường
            return (<Button success style={this.styles.Button} onPress={() => { this.onSuccess(this.ma_xu_ly_den, true) }}><Text style={this.styles.ButtonText}>Hoàn thành</Text></Button>)
        }
    }

    renderBtnTheoDoi = () => {
        if (global.vbde_cv_theodoi_vb == 1) {
            return (<Button primary style={this.styles.Button} onPress={() => { this.onCVtheoDoiVB() }}><Text style={this.styles.ButtonText}>Theo dõi VB</Text></Button>)
        } else {
            return null
        }
    }

    renderButton = () => {
        if (this.isDuyet) {
            if (this.activeTab === 0) {
                return (
                    <CardItem style={[this.styles.CardItem, this.styles.CardItemButton]}>
                        <Button primary style={this.styles.Button} onPress={this.onApproved}><Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>Duyệt</Text></Button>
                        <Button primary style={this.styles.Button} onPress={this.onLDNhapYKien}><Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>Hoàn tất</Text></Button>
                        <Button primary style={this.styles.Button} onPress={this.onForwardLDK}><Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>Chuyển LĐK</Text></Button>
                        {this.renderBtnTheoDoi()}
                    </CardItem>
                )
            } else if (this.activeTab == 1) {
                return (
                    <CardItem style={[this.styles.CardItem, this.styles.CardItemButton]}>
                        <Button primary style={this.styles.Button} onPress={this.onApproved}><Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>Duyệt</Text></Button>
                        <Button primary style={this.styles.Button} onPress={this.onLDNhapYKien}><Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>Hoàn tất</Text></Button>
                        <Button primary style={this.styles.Button} onPress={this.onForwardLDK}><Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>Chuyển LĐK</Text></Button>
                        {this.renderBtnTheoDoi()}
                    </CardItem>
                )
            } else {
                return (
                    <CardItem style={[this.styles.CardItem, this.styles.CardItemButton]}>
                        <Button primary style={this.styles.Button} onPress={this.onLDForward}><Text style={this.styles.ButtonText}>Chuyển tiếp</Text></Button>
                        {this.renderBtnTheoDoi()}
                    </CardItem>
                )
            }
        } else {
            if (this.activeTab === 0) {
                if (this.state.dataDetails[0].ma_yeu_cau !== 1) {
                    return (
                        <CardItem style={[this.styles.CardItem, this.styles.CardItemButton]}>
                            {this.renderBtnHoanThanh()}
                            <Button primary style={this.styles.Button} onPress={this.onForward}><Text style={this.styles.ButtonText}>Chuyển</Text></Button>
                            <Button primary style={this.styles.Button} onPress={() => { this.onInProcess(this.ma_xu_ly_den, true) }}><Text style={this.styles.ButtonText}>Đang xử lý</Text></Button>
                            {this.renderBtnTheoDoi()}
                        </CardItem>
                    )
                } else {
                    return (
                        <CardItem style={[this.styles.CardItem, this.styles.CardItemButton]}>
                            {this.renderBtnHoanThanh()}
                            {
                                (global.vai_tro_xdb_duoc_chuyen_tiep === "1") && (
                                    <Button primary style={this.styles.Button} onPress={this.onForward}><Text style={this.styles.ButtonText}>Chuyển</Text></Button>
                                )
                            }
                            <Button primary style={this.styles.Button} onPress={this.onNhapYKien}><Text style={this.styles.ButtonText}>Nhập ý kiến</Text></Button>
                            {this.renderBtnTheoDoi()}
                        </CardItem>
                    )
                }
            } else if (this.activeTab == 1) {
                if (this.state.dataDetails[0].ma_yeu_cau !== 1) {
                    return (
                        <CardItem style={[this.styles.CardItem, this.styles.CardItemButton]}>
                            {this.renderBtnHoanThanh()}
                            <Button primary style={this.styles.Button} onPress={this.onForward}><Text style={this.styles.ButtonText}>Chuyển</Text></Button>
                            {this.renderBtnTheoDoi()}
                        </CardItem>
                    )
                } else {
                    return (
                        <CardItem style={[this.styles.CardItem, this.styles.CardItemButton]}>
                            {this.renderBtnHoanThanh()}
                            <Button primary style={this.styles.Button} onPress={this.onNhapYKien}><Text style={this.styles.ButtonText}>Nhập ý kiến</Text></Button>
                            {this.renderBtnTheoDoi()}
                        </CardItem>
                    )
                }
            } else if (this.activeTab == 2) {
                return (
                    <CardItem style={[this.styles.CardItem, this.styles.CardItemButton]}>
                        <Button primary style={this.styles.Button} onPress={() => { this.onForward() }}><Text style={this.styles.ButtonText}>Chuyển tiếp</Text></Button>
                        {this.renderBtnTheoDoi()}
                    </CardItem>
                )
            } else {
                return null
            }
        }
    }

    renderPre = () => {
        let indexPre = this.state.index - 1
        if (indexPre >= 0) {
            return (
                <View style={this.styles.viewLeftBottomButton}>
                    <TouchableOpacity style={this.styles.buttonBottom} onPress={() => { this.loadData(indexPre) }}>
                        <Icon name="reply" type="FontAwesome" style={this.styles.iconBottom} />
                    </TouchableOpacity>
                </View>
            )
        }
    }

    renderNext = () => {
        let indexNext = this.state.index + 1
        let length = this.dataSource.length
        if (length !== 0 && indexNext < length) {
            return (
                <View style={this.styles.viewRightBottomButton}>
                    <TouchableOpacity style={this.styles.buttonBottom} onPress={() => { this.loadData(indexNext) }}>
                        <Icon name="share" type="FontAwesome" style={this.styles.iconBottom} />
                    </TouchableOpacity>
                </View>
            )
        }
    }

    render() {

        this.styles = StyleSheet.create({
            scrollContainer: {
                flex: 1,
                bottom: 0,
                backgroundColor: AppConfig.grayBackground
            },
            viewTieuDe: {
                backgroundColor: AppConfig.grayBackground,
                justifyContent: "center", alignContent: "center",
                flexWrap: "wrap",
                padding: 4,
            },
            textTieuDe: {
                fontSize: this.props.fontSize.FontSizeXLarge,
                fontWeight: "400",
                textAlign: "center"
            },

            //Table_ChiTiet_VBDEN
            Card: {
                marginLeft: 6,
                marginRight: 6,
                marginTop: 0,
                padding: 10,
                borderRadius: 6,
            },
            CardItem: {
                marginTop: AppConfig.defaultPadding,
                marginBottom: AppConfig.defaultPadding,
            },
            CardItemButton: {
                marginTop: 10,
                paddingTop: 10,
                borderTopColor: AppConfig.defaultLineColor,
                borderTopWidth: AppConfig.defaultLineWidth,
                flexDirection: 'row',
                flexWrap: "wrap",
                justifyContent: "center",
                alignItems: "center"
            },
            Button: {
                margin: 3,
                height: 40,
                justifyContent: "center",
                alignItems: "center",
            },
            ButtonText: {
                fontSize: this.props.fontSize.FontSizeNorman,
                textAlign: "center"
            },
            textLeft: {
                fontSize: this.props.fontSize.FontSizeNorman,
                color: AppConfig.grayText,
                alignSelf: "flex-start"
            },
            textRight: {
                paddingLeft: 5,
                fontSize: this.props.fontSize.FontSizeNorman,
                flexWrap: "wrap",
                flexShrink: 1,
            },
            viewLeftBottomButton: {
                left: 15,
                bottom: 15,
                position: "absolute"
            },
            viewRightBottomButton: {
                right: 15,
                bottom: 15,
                position: "absolute"
            },
            buttonBottom: {
                width: 40,
                height: 40,
                borderRadius: 20,
                backgroundColor: "transparent",
                borderWidth: 0.7,
                borderColor: "#BDBDBD",
                justifyContent: "center",
                alignItems: "center",
            },
            iconBottom: {
                fontSize: this.props.FontSizeSmall,
                color: "#BDBDBD"
            }
        })

        const popAction = StackActions.pop({ n: 1 })
        return (
            <Container style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
                <HeaderWithLeft title="Chi tiết văn bản đến" buttonName="arrow-back" onPressLeftButton={() => {
                    if (this.isFromNotify) {
                        if (this.isDuyet) {
                            this.props.navigation.dispatch(resetStack("VBDenDuyet"))
                        } else {
                            this.props.navigation.dispatch(resetStack("VBDenXuLy"))
                        }
                    } else {
                        this.props.navigation.dispatch(popAction)
                    }
                }} />
                <ScrollView
                    bounces={false}
                    removeClippedSubviews={true}
                    scrollEnabled={true}
                    style={this.styles.scrollContainer}
                >
                    <Table_ChiTiet_VBDEN
                        dataDetails={this.state.dataDetails}
                        renderButton={this.renderButton}
                        isUpdateViewCV={this.isUpdateViewCV}
                        updateViewCV={this.updateViewCV}
                        onApproved={this.onApproved}
                        onForwardLDK={this.onForwardLDK}
                        onSuccess={() => { this.onSuccess(this.ma_xu_ly_den, true) }}
                        onForward={this.onForward}
                        onInProcess={() => { this.onInProcess(this.ma_xu_ly_den, true) }}
                        onLDForward={this.onLDForward}
                        isDuyet={this.isDuyet}
                        activeTab={this.activeTab}
                        fontSize={this.props.fontSize}
                        styles={this.styles}
                    />
                    {(global.cks_hien_thi_thong_tin_app_mobile == 1) && (
                        <SignInfo
                            ma_van_ban={this.ma_van_ban_den}
                            type="den"
                            fontSize={this.props.fontSize}
                        />)
                    }
                    <Table_ButPhe_VBDEN
                        dataNotes={this.state.dataNotes}
                        fontSize={this.props.fontSize}
                        styles={this.styles}
                    />
                </ScrollView>
                {this.renderPre()}
                {this.renderNext()}
            </Container >
        );
    }
}

function mapStateToProps(state) {
    return {
        fontSize: state.fontSize,
    }
}

export default connect(mapStateToProps)(VBDen_Details)