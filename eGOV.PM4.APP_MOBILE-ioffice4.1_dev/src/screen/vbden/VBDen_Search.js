import React, { Component } from 'react';
import { StyleSheet, View, PixelRatio, TouchableOpacity, FlatList } from "react-native"
import { Container, Content, Spinner, Text, CardItem, Toast, Icon } from "native-base"
import { DrawerActions } from "react-navigation"
import HeaderWithLeft from "../../components/Header/HeaderWithLeft"
import FooterTab from "../../components/Footer/FooterTab"
import CustomTextInput from "../../components/CustomTextInput"
import { AppConfig } from "../../AppConfig"
import { CheckSession } from "../../untils/NetInfo"
import { RemoveNoiDungTTDH, ConvertDateTimeDetail } from "../../untils/TextUntils"
import API from "../../networks"

class Search_Item extends Component {
    constructor(props) {
        super(props);
    }

    shouldComponentUpdate = (nextProps, nextState) => {
        if (nextProps !== this.props) {
            return true
        } else {
            return false
        }
    };

    updateViewCV = async (ma_thanh_phan_hop_kc) => {
        await API.LichHop.AU_LH_CNNX(ma_thanh_phan_hop_kc).then((result) => {
            if (result) {
                this.setState({ isBold: false })
            }
        })
    }

    onPressItem = (ma_van_ban, loai_vb, ma_ttdh_kc, ma_vbnb_kc) => {
        if (loai_vb === 'vbde' && !ma_ttdh_kc) {
            this.props.navigation.navigate("VBDen_Details", {
                isDuyet: false,
                dataSource: [{ "ma_xu_ly_den": 0, "ma_van_ban_den_kc": ma_van_ban }],
                activeTab: 99,
                isFromNotify: true
            })
        } else if (loai_vb === 'vbdi' && !ma_ttdh_kc) {
            this.props.navigation.navigate("VBDi_Details", {
                isDuyet: false,
                dataSource: [{ "ma_xu_ly_di": 0, "ma_van_ban_di_kc": ma_van_ban }],
                activeTab: 1,
                isFromNotify: true
            })
        } else if (ma_ttdh_kc) {
            this.props.navigation.navigate("TTDH_Search_Detail", {
                item: this.props.item,
                isFromNotify: true
            })
        } else if (ma_vbnb_kc) {
            this.props.navigation.navigate("VBNB_Search_Detail", {
                item: this.props.item,
                activeTab: (this.props.item.trang_thai_xu_ly) ? "0" : "1",
                isFromNotify: true
            })
        } else {
            this.props.navigation.navigate("Meeting_Search_Detail", {
                ma_thanh_phan_hop_kc: this.props.item.ma_thanh_phan_hop_kc,
                updateViewCV: this.updateViewCV,
                isFromNotify: true
            })
        }
    }


    render() {
        let { item } = this.props
        return (
            <CardItem style={styles.CardItem}>
                <TouchableOpacity style={{ justifyContent: 'center', flex: 1, left: 0 }} onPress={() => this.onPressItem(item.ma_van_ban, item.loai_vb, item.ma_ttdh_kc, item.ma_vbnb_kc, item.ma_thanh_phan_hop_kc)}>

                    <Text numberOfLines={2} ellipsizeMode="tail" style={styles.textNoBold}>{RemoveNoiDungTTDH(item.trich_yeu)}</Text>
                    {
                        (item.so_ky_hieu) && (
                            <Text numberOfLines={1} ellipsizeMode="tail" style={styles.textNoBold}>Số hiệu: {item.so_ky_hieu}</Text>
                        )
                    }
                    {
                        (item.ngay_ban_hanh) && (
                            <Text numberOfLines={1} ellipsizeMode="tail" style={styles.txtNoiDung}>Ngày ban hành: {ConvertDateTimeDetail(item.ngay_ban_hanh)}</Text>
                        )
                    }
                    {
                        (item.ngay_den) && (
                            <Text numberOfLines={1} ellipsizeMode="tail" style={styles.txtNoiDung}>Ngày đến: {ConvertDateTimeDetail(item.ngay_den)}</Text>
                        )
                    }
                    {
                        (item.hinh_thuc) && (
                            <Text numberOfLines={1} ellipsizeMode="tail" style={styles.txtNoiDung}>Hình thức: {item.hinh_thuc}</Text>
                        )
                    }
                    {
                        (item.loai_vb) && (
                            <Text numberOfLines={1} ellipsizeMode="tail" style={styles.txtNoiDung}>Loại: {item.loai_vb === "vbde" ? "Văn bản đến" : "Văn bản đi"}</Text>
                        )
                    }
                    {
                        (item.ten_co_quan_ban_hanh) && (
                            <Text numberOfLines={2} ellipsizeMode="tail" style={styles.txtNoiDung}>Cơ quan ban hành: {item.ten_co_quan_ban_hanh}</Text>
                        )
                    }

                    {/* Show item TTDH */}
                    <View style={{ justifyContent: 'center', flex: 1, left: 0, marginRight: 100 }}>
                        {
                            (item.ten_nguoi_gui) && (
                                <Text numberOfLines={1} ellipsizeMode="tail" style={styles.textNoBold}>{item.ten_nguoi_gui}</Text>
                            )
                        }
                        {
                            (item.tieu_de) && (
                                <Text numberOfLines={1} ellipsizeMode="tail" style={styles.textNoBold}>{item.tieu_de}</Text>
                            )
                        }
                        {
                            (item.ma_ttdh_kc && item.noi_dung) && (
                                <Text numberOfLines={1} ellipsizeMode="tail" style={styles.txtNoiDung}>{RemoveNoiDungTTDH(item.noi_dung)}</Text>
                            )
                        }
                        {
                            (item.ma_ttdh_kc && item.ngay_nhan) ? (
                                <Text numberOfLines={1} ellipsizeMode="tail" style={styles.txtNoiDung}>Loại: Thông tin điều hành nhận</Text>
                            )
                                :
                                (item.ma_ttdh_kc && !item.ngay_nhan) ?
                                    (
                                        <Text numberOfLines={1} ellipsizeMode="tail" style={styles.txtNoiDung}>Loại: Thông tin điều hành gửi</Text>
                                    )
                                    :
                                    null
                        }
                        {
                            (item.ngay_nhan && item.ma_ttdh_kc) ? <Text numberOfLines={1} ellipsizeMode="tail" style={styles.txtNoiDung}>Ngày nhận: {ConvertDateTimeDetail(item.ngay_nhan)}</Text>
                                : (item.ngay_gui && item.ma_ttdh_kc) ?
                                    <Text numberOfLines={1} ellipsizeMode="tail" style={styles.txtNoiDung}>Ngày gửi: {ConvertDateTimeDetail(item.ngay_gui)}</Text>
                                    : null

                        }
                    </View>
                    {/* <View style={{ width: 100, top: 0, right: 0, bottom: 0, position: "absolute", alignItems: 'center', justifyContent: "center" }}>
                    {
                        (item.ngay_nhan) ? <Text numberOfLines={1} ellipsizeMode="tail" style={styles.txtNgayNhan}>{ConvertDateTimeDetail(item.ngay_nhan)}</Text>
                        :
                        <Text numberOfLines={1} ellipsizeMode="tail" style={styles.txtNgayNhan}>{ConvertDateTimeDetail(item.ngay_gui)}</Text>

                    }
                    </View> */}

                    {/* Show item VBNB */}
                    <View style={{ justifyContent: 'center', flex: 1, left: 0, marginRight: 100 }}>
                        {
                            (item.ma_vbnb_kc && item.so_ky_hieu) ? <Text numberOfLines={2} ellipsizeMode="tail" style={styles.textNoBold}>{RemoveNoiDungTTDH(item.trich_yeu)}</Text>
                                :
                                null
                        }

                        {
                            (item.so_hieu && item.ma_vbnb_kc) && (
                                <Text numberOfLines={1} ellipsizeMode="tail" style={styles.textNoBold}>{item.so_hieu}</Text>
                            )
                        }
                        {
                            (item.ten_can_bo_gui && item.ma_vbnb_kc) && (
                                <Text numberOfLines={1} ellipsizeMode="tail" style={styles.txtNoiDung}>Người gủi: {item.ten_can_bo_gui}</Text>
                            )
                        }
                        {
                            (item.ten_don_vi_quan_tri) && (
                                <Text numberOfLines={1} ellipsizeMode="tail" style={styles.txtNoiDung}>Đơn vị gửi: {item.ten_don_vi_quan_tri}</Text>
                            )
                        }

                        {
                            (item.ma_vbnb_kc) && (
                                <Text numberOfLines={1} ellipsizeMode="tail" style={styles.txtNoiDung}>Xuất xứ: {(item.xuat_xu === null) ? "Rỗng" : item.xuat_xu}</Text>
                            )
                        }
                        {
                            (item.ma_vbnb_kc && item.trang_thai_xu_ly) ?
                                (<Text numberOfLines={1} ellipsizeMode="tail" style={styles.txtNoiDung}>Loại: Văn bản nội bộ nhận</Text>)
                                :
                                (item.ma_vbnb_kc && !item.trang_thai_xu_ly) ?
                                    (<Text numberOfLines={1} ellipsizeMode="tail" style={styles.txtNoiDung}>Loại: Văn bản nội bộ gửi</Text>)
                                    :
                                    null
                        }
                        {
                            (item.ma_vbnb_kc && item.ngay_gui && !item.trang_thai_xu_ly) ? (<Text numberOfLines={1} ellipsizeMode="tail" style={styles.txtNoiDung}>Ngày gửi: {ConvertDateTimeDetail(item.ngay_gui)}</Text>)
                                : (item.ma_vbnb_kc && item.ngay_gui && item.trang_thai_xu_ly) ?
                                    (<Text numberOfLines={1} ellipsizeMode="tail" style={styles.txtNoiDung}>Ngày nhận: {ConvertDateTimeDetail(item.ngay_gui)}</Text>)
                                    : null
                        }
                    </View>
                    {/* <View style={{ width: 100, top: 0, right: 0, bottom: 0, position: "absolute", alignItems: 'center', justifyContent: "center" }}>
                    {
                        (item.ngay_gui) && <Text numberOfLines={1} ellipsizeMode="tail" style={styles.txtNgayNhan}>{ConvertDateTimeDetail(item.ngay_gui)}</Text>
                    }
                    </View> */}

                    {/* Show Lịch Họp Cá Nhân */}
                    {
                        (item.ten_cuoc_hop) && <Text numberOfLines={1} ellipsizeMode="tail" style={styles.textNoBold}>{item.ten_cuoc_hop} </Text>
                    }
                    {
                        (item.ten_phong_hop) && <Text numberOfLines={2} ellipsizeMode="tail" style={styles.textNoBold}>Phòng họp: {item.ten_phong_hop ? item.ten_phong_hop : "Rỗng"} </Text>
                    }
                    {
                        (item.ten_vai_tro_hop) && <Text numberOfLines={2} ellipsizeMode="tail" style={styles.textNoBold}>Vai trò họp: {item.ten_vai_tro_hop ? item.ten_vai_tro_hop : "Rỗng"} </Text>
                    }
                    {
                        (item.thoi_gian_bat_dau) && <Text numberOfLines={2} ellipsizeMode="tail" style={[styles.txtNoiDung]}>Thời gian bắt đầu: {item.thoi_gian_bat_dau ? ConvertDateTimeDetail(item.thoi_gian_bat_dau) : "Rỗng"} </Text>
                    }
                    {
                        (item.thoi_gian_ket_thuc) && <Text numberOfLines={2} ellipsizeMode="tail" style={[styles.txtNoiDung]}>Thời gian kết thúc: {item.thoi_gian_ket_thuc ? ConvertDateTimeDetail(item.thoi_gian_ket_thuc) : "Rỗng"} </Text>
                    }
                    {
                        (item.ma_cuoc_hop_kc && item.trang_thai) && <Text numberOfLines={2} ellipsizeMode="tail" style={[styles.txtNoiDung]}>Tình trạng: {(item.trang_thai === 1) ? "chưa hợp" : (item.trang_thai === 2) ? "đang họp" : (item.trang_thai === 3) ? "đã họp" : "đã hủy"}</Text>
                    }

                </TouchableOpacity>
            </CardItem>
        )
    }

}

export default class VBDen_Search extends Component {
    constructor(props) {
        super(props)
        this.state = {
            dataSearchSoKyHieu: [],
            dataSearchTrichYeu: [],
            dataSourceTTDHDN: [],
            dataSourceTTDHDG: [],
            dataSourceVBNBNSoKyHieu: [],
            dataSourceVBNBNTrichYeu: [],
            dataSourceVBNBGSoKyHieu: [],
            dataSourceVBNBGTrichYeu: [],
            dataSourceLHCN: [],
            dataTTDH: [],
            dataVBNB: [],
            fullData: [],
            keyWord: "",
            loading: false,
        }
        this.ma_can_bo = global.ma_can_bo;
        this.sizeTTDHNhan = AppConfig.pageSizeTTDHNhan;
        this.sizeTTDHGui = AppConfig.pageSizeTTDHGui;
        this.dataTTDH = [];

        this.sizeVBNBDN = AppConfig.pageSizeVBNBDN;
        this.sizeVBNBDG = AppConfig.pageSizeVBNBDGDPH;
        this.dataSearchVBNBNSoKyHieu = [];
        this.dataSearchVBNBNTrichYeu = [];
        this.dataSearchVBNBGSoKyHieu = [];
        this.dataSearchVBNBGTrichYeu = [];
        this.dataVBNB = [];
    }

    componentDidMount = () => {
        this.didBlurSubscription = this.props.navigation.addListener('didFocus', payload => { CheckSession(this.props.navigation) })
    };

    componentWillUnmount = () => {
        this.didBlurSubscription.remove()
    };

    searchData = async () => {
        let keyWord = this.state.keyWord.toString()
        if (keyWord === '') {
            Toast.show({ text: "Vui lòng nhập từ khóa tìm kiếm!", type: "warning", buttonText: "Đóng", position: "top", duration: 2000 })
            return
        }

        this.setState({
            fullData: [],
            loading: true
        })

        await this.getDataSearchVB(keyWord);
        await this.getDataSearchTTDH(keyWord);
        await this.getDataSearchVBNB(keyWord);
        await this.getDataLHCN(keyWord);

        // if (this.state.dataSearchSoKyHieu.length > 0 && this.state.dataSearchTrichYeu.length === 0 && this.state.dataTTDH.length === 0 && this.state.dataVBNB.length === 0 && this.state.dataSourceLHCN.length === 0) {
        //     this.setState({ fullData: this.state.dataSearchSoKyHieu, loading: false })
        // } else if (this.state.dataSearchSoKyHieu.length === 0 && this.state.dataSearchTrichYeu.length > 0 && this.state.dataTTDH.length === 0 && this.state.dataVBNB.length === 0 && this.state.dataSourceLHCN.length === 0) {
        //     this.setState({ fullData: this.state.dataSearchTrichYeu, loading: false })
        // }
        // else if (this.state.dataSearchSoKyHieu.length === 0 && this.state.dataSearchTrichYeu.length === 0 && this.state.dataTTDH.length > 0 && this.state.dataVBNB.length === 0 && this.state.dataSourceLHCN.length === 0) {
        //     this.setState({ fullData: this.state.dataTTDH, loading: false })
        // }
        // else if (this.state.dataSearchSoKyHieu.length === 0 && this.state.dataSearchTrichYeu.length === 0 && this.state.dataTTDH.length === 0 && this.state.dataVBNB.length > 0 && this.state.dataSourceLHCN.length === 0) {
        //     this.setState({ fullData: this.state.dataVBNB, loading: false })
        // }
        // else if (this.state.dataSearchSoKyHieu.length === 0 && this.state.dataSearchTrichYeu.length === 0 && this.state.dataTTDH.length === 0 && this.state.dataVBNB.length === 0 && this.state.dataSourceLHCN.length > 0) {
        //     this.setState({ fullData: this.state.dataSourceLHCN, loading: false })
        // } else if (this.state.dataSearchSoKyHieu.length === 0 && this.state.dataSearchTrichYeu.length === 0 && this.state.dataTTDH.length === 0 && this.state.dataVBNB.length === 0 && this.state.dataSourceLHCN.length === 0) {
        //     this.setState({ fullData: [], loading: false })
        //     Toast.show({ text: "Không tìm thấy dữ liệu!", type: "warning", buttonText: "Đóng", position: "top", duration: 2000 })
        // } else {
        //     let dataSearchSoKyHieu = this.state.dataSearchSoKyHieu
        //     let dataSearchTrichYeu = this.state.dataSearchTrichYeu
        //     let ids = new Set(dataSearchSoKyHieu.map(list1 => list1.ma_van_ban));
        //     let data = [...dataSearchSoKyHieu, ...dataSearchTrichYeu.filter(list2 => !ids.has(list2.ma_van_ban)), ...this.state.dataTTDH, ...this.state.dataVBNB, ...this.state.dataSourceLHCN];
        //     this.setState({ fullData: data, loading: false })
        // }
    }

    getDataSearchVB = async (keyword) => {
        await API.VanBanDen.AU_VANBAN_TCVB(keyword, "").then(async (responseSKH) => {
            await API.VanBanDen.AU_VANBAN_TCVB("", keyword).then((responseTY) => {
                let dataSearchSoKyHieu = responseSKH
                let dataSearchTrichYeu = responseTY
                let ids = new Set(dataSearchSoKyHieu.map(list1 => list1.ma_van_ban));
                let data = [...dataSearchSoKyHieu, ...dataSearchTrichYeu.filter(list2 => !ids.has(list2.ma_van_ban)), ...this.state.dataTTDH, ...this.state.dataVBNB, ...this.state.dataSourceLHCN];
                this.setState({ fullData: [...this.state.fullData, ...data], loading: false })
            })
        })
    }

    getPageTTDHDN = async (keyWord) => {
        let page = "";
        let totalPage = 0;
        await API.ThongTinDieuHanh.AU_TTDH_DA_NHAN(this.ma_can_bo, page, this.sizeTTDHNhan, keyWord)
            .then(response => {
                totalPage = response[0].total_row;
            })
        return totalPage;
    }
    getPageTTDHDG = async (keyWord) => {
        let page = "";
        let totalPage = 0;
        await API.ThongTinDieuHanh.AU_GETPAGE_TTDH_DA_GUI(ma_can_bo, page, this.sizeTTDHGui, keyWord, "").then(response => {
            totalPage = response;
        })
        return totalPage;
    }

    getDataSearchTTDH = async (keyWord) => {
        let TempTotalPageTTDHDN = this.getPageTTDHDN(keyWord);
        let totalPageTTDHDN = Math.ceil(TempTotalPageTTDHDN / this.sizeTTDHNhan);
        let pageTTDHDN;
        let dataSourceTTDHDN = []
        for (pageTTDHDN = 1; pageTTDHDN <= totalPageTTDHDN; pageTTDHDN++) {
            API.ThongTinDieuHanh.AU_TTDH_DA_NHAN(this.ma_can_bo, pageTTDHDN, this.sizeTTDHNhan, keyWord).then(response => {
                dataSourceTTDHDN = pageTTDHDN === 1 ? response : [...dataSourceTTDHDN, ...response]
            })
        }

        let totalPageTTDHDG = this.getPageTTDHDG(keyWord);
        let pageTTDHDG;
        let dataSourceTTDHDG = []
        for (pageTTDHDG = 1; pageTTDHDG <= totalPageTTDHDG; pageTTDHDG++) {
            API.ThongTinDieuHanh.AU_TTDH_DA_GUI(this.ma_can_bo, pageTTDHDG, this.sizeTTDHGui, keyWord, "").then(response => {
                dataSourceTTDHDG = pageTTDHDG === 1 ? response : [...dataSourceTTDHDG, ...response]
            })
        }
        let dataSearchTTDHDN = dataSourceTTDHDN
        let dataSearchTTDHDG = dataSourceTTDHDG
        let ids = new Set(dataSearchTTDHDN.map(list1 => list1.ma_ttdh_kc));
        let data = [...dataSearchTTDHDN, ...dataSearchTTDHDG.filter(list2 => !ids.has(list2.ma_ttdh_kc))];
        this.setState({ fullData: [...this.state.fullData, ...data], loading: false })
    }

    // Lấy số trang Văn Bản Nội Bộ Đã Nhận theo Số Ký Hiệu
    getPageVBNBNSoKyHieu = async (keyWord) => {
        if (keyWord === "") return 0;
        let size = AppConfig.pageSizeVBNBDN;
        let totalPage = 0;
        await API.VanBanNoiBo.AU_VBNB_DSVBNBDN_GetPage(keyWord, "", "", size, this.ma_can_bo)
            .then((response) => {
                totalPage = response;
            })
        return totalPage;
    }
    // Lấy số trang Văn Bản Nội Bộ Đã Nhận theo Trích Yếu
    getPageVBNBNTrichYeu = async (keyWord) => {
        if (keyWord === "") return 0;
        let size = AppConfig.pageSizeVBNBDN;
        let totalPage = 0;
        await API.VanBanNoiBo.AU_VBNB_DSVBNBDN_GetPage("", keyWord, "", size, this.ma_can_bo)
            .then((response) => {
                totalPage = response;
            })
        return totalPage;
    }
    // Lấy số trang Văn Bản Nội Bộ Đã Gửi theo Số Ký Hiệu
    getPageVBNBGSoKyHieu = async (keyWord) => {
        if (keyWord === "") return 0;
        let size = AppConfig.pageSizeVBNBDGDPH;
        let totalPage = 0;
        await API.VanBanNoiBo.AU_VBNB_DSVBNBDGDPH_GetPage(keyWord, "", "", size, this.ma_can_bo)
            .then((response) => {
                totalPage = response;
            })
        return totalPage;
    }
    // Lấy số trang Văn Bản Nội Bộ Đã Gửi theo Trích Yếu
    getPageVBNBGTrichYeu = async (keyWord) => {
        if (keyWord === "") return 0;
        let size = AppConfig.pageSizeVBNBDGDPH;
        let totalPage = 0;
        await API.VanBanNoiBo.AU_VBNB_DSVBNBDGDPH_GetPage("", keyWord, "", size, this.ma_can_bo)
            .then((response) => {
                totalPage = response;
            })
        return totalPage;
    }
    // Lấy toàn bộ văn bản nội bộ tìm được theo từ khóa và lọc ra các văn bản nội bộ trùng
    getDataSearchVBNB = async (keyWord) => {
        let totalPageVBNBNSoKyHieu = this.getPageVBNBNSoKyHieu(keyWord);
        let pageVBNBNSoKyHieu;
        let dataSourceVBNBNSoKyHieu = []
        for (pageVBNBNSoKyHieu = 1; pageVBNBNSoKyHieu <= totalPageVBNBNSoKyHieu; pageVBNBNSoKyHieu++) {
            API.VanBanNoiBo.AU_VBNB_DSVBNBDN(keyWord, "", pageVBNBNSoKyHieu, this.sizeVBNBDN, this.ma_can_bo).then(response => {
                dataSourceVBNBNSoKyHieu = pageVBNBNSoKyHieu === 1 ? response : [...dataSourceVBNBNSoKyHieu, ...response]
            })
        }

        let totalPageVBNBNTrichYeu = this.getPageVBNBNTrichYeu(keyWord);
        let pageVBNBNTrichYeu;
        let dataSourceVBNBNTrichYeu = []
        for (pageVBNBNTrichYeu = 1; pageVBNBNTrichYeu <= totalPageVBNBNTrichYeu; pageVBNBNTrichYeu++) {
            API.VanBanNoiBo.AU_VBNB_DSVBNBDN("", keyWord, pageVBNBNSoKyHieu, this.sizeVBNBDN, this.ma_can_bo).then(response => {
                dataSourceVBNBNTrichYeu = pageVBNBNTrichYeu === 1 ? response : [...dataSourceVBNBNTrichYeu, ...response]
            })
        }
        let dataSearchVBNBNSoKyHieu = dataSourceVBNBNSoKyHieu
        let dataSearchVBNBNTrichYeu = dataSourceVBNBNTrichYeu
        let ids = new Set(dataSearchVBNBNSoKyHieu.map(list1 => list1.ma_vbnb_kc));
        let dataFilterSearchVBNBN = [...dataSearchVBNBNSoKyHieu, ...dataSearchVBNBNTrichYeu.filter(list2 => !ids.has(list2.ma_vbnb_kc))];

        let totalPageVBNBGSoKyHieu = this.getPageVBNBGSoKyHieu(keyWord);
        let pageVBNBNGSoKyHieu;
        let dataSourceVBNBGSoKyHieu = []
        for (pageVBNBNGSoKyHieu = 1; pageVBNBNGSoKyHieu <= totalPageVBNBGSoKyHieu; pageVBNBNGSoKyHieu++) {
            API.VanBanNoiBo.AU_VBNB_DSVBNBDGDPH(keyWord, "", pageVBNBNGSoKyHieu, this.sizeVBNBDG, this.ma_can_bo).then(response => {
                dataSourceVBNBGSoKyHieu = pageVBNBNGSoKyHieu === 1 ? response : [...dataSourceVBNBGSoKyHieu, ...response]
            })
        }

        let totalPageVBNBGTrichYeu = this.getPageVBNBGTrichYeu(keyWord);
        let pageVBNBNGTrichYeu;
        let dataSourceVBNBGTrichYeu = []
        for (pageVBNBNGTrichYeu = 1; pageVBNBNGTrichYeu <= totalPageVBNBGTrichYeu; pageVBNBNGTrichYeu++) {
            API.VanBanNoiBo.AU_VBNB_DSVBNBDGDPH("", keyWord, pageVBNBNGTrichYeu, this.sizeVBNBDG, this.ma_can_bo).then(response => {
                dataSourceVBNBGTrichYeu = pageVBNBNGTrichYeu === 1 ? response : [...dataSourceVBNBGTrichYeu, ...response]
            })
        }
        let dataSearchVBNBGSoKyHieu = dataSourceVBNBGSoKyHieu;
        let dataSearchVBNBGTrichYeu = dataSourceVBNBGTrichYeu;
        let ids1 = new Set(dataSearchVBNBGSoKyHieu.map(data1 => data1.ma_vbnb_kc));
        let dataFilterSearchVBNBG = [...dataSearchVBNBGSoKyHieu, ...dataSearchVBNBGTrichYeu.filter(data2 => !ids1.has(data2.ma_vbnb_kc))];

        let ids2 = new Set(dataFilterSearchVBNBN.map(data2 => data2.ma_vbnb_kc));
        this.dataVBNB = [...dataFilterSearchVBNBN, ...dataFilterSearchVBNBG.filter(data3 => !ids2.has(data3.ma_vbnb_kc))];
        this.setState({ fullData: [...this.state.fullData, ...dataVBNB], loading: false })
    }

    // Lấy số trang lịch họp cá nhân theo từ khóa: 
    getPageLHCN = async (keyWord) => {
        let size = AppConfig.pageSizeLH;
        let totalPage = 0;
        await API.LichHop.AU_LH_getPageLHCN(keyWord, size).then(response => {
            totalPage = response;
        })
        return totalPage;
    }
    getDataLHCN = async (keyWord) => {
        let page = await this.getPageLHCN(keyWord);
        let size = AppConfig.pageSizeLH;
        let pageLoadmore;
        let dataSourceLHCN = []
        for (pageLoadmore = 1; pageLoadmore <= page; pageLoadmore++) {
            await API.LichHop.AU_LH_TKLHCN(keyWord, pageLoadmore, size).then(response => {
                dataSourceLHCN = pageLoadmore === 1 ? response : [...dataSourceLHCN, ...response]
            })
        }
        this.setState({ fullData: [...this.state.fullData, ...dataSourceLHCN], loading: false })
    }

    render() {
        return (
            <Container style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
                <HeaderWithLeft
                    title="Tìm kiếm văn bản"
                    buttonName="menu"
                    onPressLeftButton={_ => { this.props.navigation.dispatch(DrawerActions.toggleDrawer()) }}
                />
                <CardItem style={styles.CardItemSearch}>
                    <CardItem style={styles.CardItemBody}>
                        <CustomTextInput
                            style={styles.TextInputSearch}
                            value={this.state.keyWord}
                            onChangeText={(text) => { this.setState({ keyWord: text }) }}
                            placeholder="Nhập từ khóa tìm kiếm ..."
                            underlineColorAndroid={"transparent"}
                            onRef={ref => this.input = ref}
                            placeholderStyle={{ top: 12, left: 6, fontSize: AppConfig.FontSizeNorman }}
                            onSubmitEditing={this.searchData}
                        />
                    </CardItem>
                    <CardItem style={styles.CardItemRight}>
                        <TouchableOpacity onPress={this.searchData} style={{ flex: 1 }}>
                            <Icon name="search" type="Ionicons" style={styles.IconSearch}></Icon>
                        </TouchableOpacity>
                    </CardItem>
                </CardItem>
                <Content>
                    {
                        (this.state.loading) ? (
                            <View><Spinner color={AppConfig.blueBackground} /></View>
                        ) : (
                                <FlatList
                                    data={this.state.fullData}
                                    extraData={this.state}
                                    keyExtractor={(item, index) => "key" + item.ma_van_ban + index}
                                    removeClippedSubviews={true}
                                    initialNumToRender={30}
                                    renderItem={({ item }) => {
                                        return (<Search_Item item={item} navigation={this.props.navigation} />)
                                    }}
                                />
                            )
                    }

                </Content>
                <FooterTab tabActive="trangchu" navigation={this.props.navigation} />
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    CardItem: {
        flex: 1,
        borderTopWidth: 0,
        borderBottomWidth: 0.55,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        borderBottomColor: "lightgray",
        paddingLeft: 8,
        paddingRight: 8,
        paddingTop: 4,
        paddingBottom: 4,
        alignItems: 'center',
        justifyContent: "center",
    },
    textNoBold: {
        color: "#000000",
        fontSize: AppConfig.FontSizeNorman,
    },
    txtNoiDung: {
        color: "#808080",
        fontSize: AppConfig.FontSizeSmall,
    },
    CardItemSearch: {
        borderColor: AppConfig.defaultLineColor,
        borderWidth: AppConfig.defaultLineWidth,
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 20,
        marginRight: 20,
        borderRadius: 5,
        paddingLeft: 5,
        paddingRight: 5
    },
    CardItemBody: {
        flex: 1,
        height: 45
    },
    CardItemRight: {
        backgroundColor: "transparent",
        width: 40,
        height: 40,
        position: 'absolute',
        zIndex: 99,
        right: 0,
        alignItems: 'center',
        justifyContent: "center",
        alignContent: "center",
    },
    TextInputSearch: {
        flex: 1,
        fontFamily: AppConfig.fontFamily,
        fontSize: AppConfig.FontSizeNorman / PixelRatio.getFontScale(),
        color: "#000000",
        minHeight: 40,
        paddingRight: 40,
    },
    IconSearch: {
        marginLeft: 3,
        color: AppConfig.blueBackground,
    },
    txtNgayNhan: {
        width: 100,
        color: "#808080",
        fontSize: AppConfig.FontSizeSmall,
        textAlign: "center",
        paddingBottom: AppConfig.defaultPadding
    },
})