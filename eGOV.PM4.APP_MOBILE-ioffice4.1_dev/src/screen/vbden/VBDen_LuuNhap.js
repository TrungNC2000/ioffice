import React, { Component } from 'react';
import { View, ScrollView, StyleSheet, Alert, TouchableOpacity, TextInput, PixelRatio } from "react-native"
import { Container, Icon, Button, Text, Card, CardItem, Toast, ActionSheet, CheckBox } from "native-base"
import { StackActions } from "react-navigation"
import { connect } from "react-redux"
import { AppConfig } from "../../AppConfig"
import { resetStack, ReturnNumber, ToastSuccess, ConvertDate } from "../../untils/TextUntils"
import HeaderWithLeft from "../../components/Header/HeaderWithLeft"
import API from "../../networks"
import DateTimePicker from 'react-native-modal-datetime-picker'
import ViewFile from "../../components/ViewFile"
import moment from "moment"

const nowYear = new Date().getFullYear();

class Table_ChiTiet_VBDEN extends Component {
    constructor(props) {
        super(props)
        this.state = {
            arrSoVanBan: [],
            arrCapDoKhan: [],
            arrCapDoMat: [],
            arrLoaiVanBan: [],
            arrLinhVucVanBan: [],
            arrLanhDaoDuyet: [],
            nam: nowYear,
            isDatePickerToVisibleNBH: false,
            isDatePickerToVisibleND: false,
            item: {
                but_phe_cb_duyet: "",
                di_dong_nguoi_gui: "",
                di_dong_nguoi_luu: "",
                file_van_ban: "",
                file_van_ban_bs: "",
                file_vb_goc: "",
                hien_thi_vblq: "",
                ho_ten_nguoi_luu: "",
                ho_va_ten_can_bo_gui: "",
                lanh_dao_chuyen_vb: "",
                loai_van_ban_khi_moi_tao: "",
                ma_cap_do_khan: 1,
                ma_cap_do_mat: 1,
                ma_ctcb_duyet: "",
                ma_ctcb_gui: "",
                ma_ctcb_luu: "",
                ma_ctcb_tao: "",
                ma_dinh_danh: "",
                ma_dinh_danh_gui: "",
                ma_dinh_danh_vb: "",
                ma_don_vi: "",
                ma_don_vi_quan_tri: "",
                ma_don_vi_quan_tri_tao: "",
                ma_don_vi_tao: "",
                ma_goc: "",
                ma_ho_so_igate: "",
                ma_linh_vuc_van_ban: "",
                ma_loai_lien_thong: "",
                ma_loai_van_ban: 1,
                ma_quy_trinh: "",
                ma_so_van_ban: "",
                ma_so_vb_den: "",
                ma_so_vb_den_old: "",
                ma_van_ban_den_inout: "",
                ma_van_ban_den_kc: "",
                ma_van_ban_goc: "",
                ma_van_ban_kc: "",
                ma_xu_ly_den: "",
                ma_xu_ly_den_inout: "",
                ngay_ban_hanh: "",
                ngay_den: "",
                ngay_luu: "",
                ngay_nhan: "",
                ngay_tao: "",
                ngay_xem: "",
                nguoi_ky: "",
                noi_luu_ban_chinh: "",
                sms: 0,
                so_ban_phat_hanh: 1,
                so_den: "",
                so_ky_hieu: "",
                ten_cap_do_khan: "",
                ten_cap_do_mat: "",
                ten_co_quan_ban_hanh: "",
                ten_don_vi_gui: "",
                ten_linh_vuc_van_ban: "",
                ten_loai_van_ban: "",
                ten_nguoi_duyet: "",
                ten_nguoi_luu: "",
                ten_so_vb_den: "",
                trang_thai_ra_soat: "",
                trang_thai_van_ban: "",
                trang_thai_van_ban_den: "",
                trang_thai_xu_ly: "",
                trang_thai_xu_ly_den: "",
                trich_yeu: "",
                ghi_chu: "",
            },
        }
    }

    componentWillReceiveProps(nextProps) {
        this.getSoVanBan()
        this.getCapDoKhan()
        this.getCapDoMat()
        this.getLinhVucVanBan()
        this.getLanhDaoDuyet();
        this.getLoaiVanBan()

        let dataDetails = nextProps.dataDetails
        console.log("dataDetails:" + JSON.stringify(dataDetails))
        let item = {
            but_phe_cb_duyet: "",
            di_dong_nguoi_gui: global.di_dong_can_bo,
            di_dong_nguoi_luu: global.di_dong_can_bo,
            file_van_ban: dataDetails.file_van_ban_bs,
            file_van_ban_bs: dataDetails.file_van_ban_bs,
            file_vb_goc: dataDetails.file_vb_goc,
            hien_thi_vblq: dataDetails.hien_thi_vblq || 0,
            ho_ten_nguoi_luu: global.ho_va_ten_can_bo,
            ho_va_ten_can_bo_gui: global.ho_va_ten_can_bo,
            lanh_dao_chuyen_vb: dataDetails.lanh_dao_chuyen_vb,
            loai_van_ban_khi_moi_tao: dataDetails.loai_van_ban_khi_moi_tao,
            ma_cap_do_khan: dataDetails.ma_cap_do_khan,
            ma_cap_do_mat: dataDetails.ma_cap_do_mat,
            ma_ctcb_duyet: dataDetails.ma_ctcb_duyet || "",
            ma_ctcb_gui: global.ma_ctcb_kc,
            ma_ctcb_luu: global.ma_ctcb_kc,
            ma_ctcb_tao: global.ma_ctcb_kc,
            ma_dinh_danh: dataDetails.ma_dinh_danh || "",
            ma_dinh_danh_gui: dataDetails.ma_dinh_danh_gui || "",
            ma_dinh_danh_vb: dataDetails.ma_dinh_danh_vb || "",
            ma_don_vi: global.ma_don_vi,
            ma_don_vi_quan_tri: dataDetails.ma_don_vi_quan_tri,
            ma_don_vi_quan_tri_tao: dataDetails.ma_don_vi_quan_tri_tao,
            ma_don_vi_tao: dataDetails.ma_don_vi_tao,
            ma_goc: dataDetails.ma_goc,
            ma_ho_so_igate: dataDetails.ma_ho_so_igate,
            ma_linh_vuc_van_ban: dataDetails.ma_linh_vuc_van_ban,
            ma_loai_lien_thong: dataDetails.ma_loai_lien_thong,
            ma_loai_van_ban: dataDetails.ma_loai_van_ban,
            ma_quy_trinh: dataDetails.ma_quy_trinh,
            ma_so_van_ban: dataDetails.ma_so_vb_den,
            ma_so_vb_den: dataDetails.ma_so_vb_den,
            ma_so_vb_den_old: dataDetails.ma_so_vb_den,
            ma_van_ban_den_inout: dataDetails.ma_van_ban_den_kc,
            ma_van_ban_den_kc: dataDetails.ma_van_ban_den_kc,
            ma_van_ban_goc: dataDetails.ma_van_ban_goc,
            ma_van_ban_kc: dataDetails.ma_van_ban_kc,
            ma_xu_ly_den: dataDetails.ma_xu_ly_den,
            ma_xu_ly_den_inout: dataDetails.ma_xu_ly_den,
            ngay_ban_hanh: dataDetails.ngay_ban_hanh,
            ngay_den: dataDetails.ngay_den,
            ngay_luu: dataDetails.ngay_luu,
            ngay_nhan: dataDetails.ngay_nhan,
            ngay_tao: dataDetails.ngay_tao,
            ngay_xem: dataDetails.ngay_xem || "",
            nguoi_ky: dataDetails.nguoi_ky || "",
            noi_luu_ban_chinh: dataDetails.noi_luu_ban_chinh || "",
            sms: 0,
            so_ban_phat_hanh: dataDetails.so_ban_phat_hanh || "1",
            so_den: dataDetails.so_den,
            so_ky_hieu: dataDetails.so_ky_hieu,
            ten_cap_do_khan: dataDetails.ten_cap_do_khan,
            ten_cap_do_mat: dataDetails.ten_cap_do_mat,
            ten_co_quan_ban_hanh: dataDetails.ten_co_quan_ban_hanh,
            ten_don_vi_gui: dataDetails.ten_don_vi_gui || "",
            ten_linh_vuc_van_ban: dataDetails.ten_linh_vuc_van_ban,
            ten_loai_van_ban: dataDetails.ten_loai_van_ban,
            ten_nguoi_duyet: dataDetails.ten_nguoi_duyet || "",
            ten_nguoi_luu: global.ho_va_ten_can_bo,
            ten_so_vb_den: dataDetails.ten_so_vb_den,
            trang_thai_ra_soat: dataDetails.trang_thai_ra_soat,
            trang_thai_van_ban: dataDetails.trang_thai_van_ban,
            trang_thai_van_ban_den: dataDetails.trang_thai_van_ban_den,
            trang_thai_xu_ly: dataDetails.trang_thai_xu_ly,
            trang_thai_xu_ly_den: dataDetails.trang_thai_xu_ly_den,
            trich_yeu: dataDetails.trich_yeu,
            ghi_chu: dataDetails.ghi_chu,
        }
        console.log("item:" + JSON.stringify(item))
        this.setState({ item })
    };

    getSoVanBan = async () => {
        await API.VanBanDen.AU_VBDEN_DSDMSVBD(global.ma_don_vi_quan_tri, new Date().getFullYear()).then((arrSoVanBan) => {
            if (arrSoVanBan.length > 0) {
                this.setState({ arrSoVanBan })
            }
        })
    }

    onPressSoVanBan = () => {
        let options = this.state.arrSoVanBan.map(obj => {
            return obj.ten_so_vb_den
        })
        console.log(options)
        options.unshift("Đóng")
        ActionSheet.show(
            {
                options: options,
                title: "Chọn sổ văn bản đến"
            },
            buttonIndex => {
                if (buttonIndex !== undefined && buttonIndex > 0) {
                    let ma_so_vb_den = this.state.arrSoVanBan[buttonIndex - 1].ma_so_vb_den_kc
                    this.setState({ item: { ...this.state.item, ma_so_vb_den } }, () => {
                        this.getSoDen(ma_so_vb_den)
                    })
                }
            }
        )
    }

    getSoDen = async (ma_so_vb_den, nam) => {
        await API.VanBanDen.AU_VBDEN_LSDTSVBD(ma_so_vb_den, nam).then((so_den) => {
            if (so_den) {
                this.setState({ item: { ...this.state.item, so_den } })
            }
        })
    }

    getCapDoKhan = async () => {
        await API.VanBanDen.AU_VBDEN_DSCDK().then((arrCapDoKhan) => {
            this.setState({ arrCapDoKhan })
        })
    }

    onPressCapDoKhan = () => {
        ActionSheet.show(
            {
                options: this.state.arrCapDoKhan.map(obj => {
                    return obj.ten_cap_do_khan
                }),
                title: "Chọn cấp độ khẩn"
            },
            buttonIndex => {
                if (buttonIndex !== undefined) {
                    let ma_cap_do_khan = this.state.arrCapDoKhan[buttonIndex].ma_cap_do_khan_kc
                    this.setState({ item: { ...this.state.item, ma_cap_do_khan } })
                }
            }
        )
    }

    getCapDoMat = async () => {
        await API.VanBanDen.AU_VBDEN_DSCDM().then((arrCapDoMat) => {
            this.setState({ arrCapDoMat })
        })
    }

    onPressCapDoMat = () => {
        ActionSheet.show(
            {
                options: this.state.arrCapDoMat.map(obj => {
                    return obj.ten_cap_do_mat
                }),
                title: "Chọn cấp độ mật"
            },
            buttonIndex => {
                if (buttonIndex !== undefined) {
                    let ma_cap_do_mat = this.state.arrCapDoMat[buttonIndex].ma_cap_do_mat_kc
                    this.setState({ item: { ...this.state.item, ma_cap_do_mat } })
                }
            }
        )
    }

    getLinhVucVanBan = async () => {
        await API.VanBanDen.AU_VBDEN_DSLVVB().then((arrLinhVucVanBan) => {
            this.setState({ arrLinhVucVanBan })
        })
    }

    onPressLinhVucVanBan = () => {
        if (this.props.activeTab !== 0) {
            return null
        }
        ActionSheet.show(
            {
                options: this.state.arrLinhVucVanBan.map(obj => {
                    return obj.ten_linh_vuc_van_ban
                }),
                title: "Chọn lĩnh vực văn bản"
            },
            buttonIndex => {
                if (buttonIndex !== undefined) {
                    let ma_linh_vuc_van_ban = this.state.arrLinhVucVanBan[buttonIndex].ma_linh_vuc_van_ban_kc
                    this.setState({ item: { ...this.state.item, ma_linh_vuc_van_ban } })
                }
            }
        )
    }

    getLoaiVanBan = async () => {
        await API.VanBanDen.AU_VBDEN_DSLVB().then((arrLoaiVanBan) => {
            this.setState({ arrLoaiVanBan })
        })
    }

    onPressLoaiVanBan = () => {
        if (this.props.activeTab !== 0) {
            return null
        }
        ActionSheet.show(
            {
                options: this.state.arrLoaiVanBan.map(obj => {
                    return obj.ten_loai_van_ban
                }),
                title: "Chọn loại văn bản"
            },
            buttonIndex => {
                if (buttonIndex !== undefined) {
                    let ma_loai_van_ban = this.state.arrLoaiVanBan[buttonIndex].ma_loai_van_ban_kc
                    this.setState({ item: { ...this.state.item, ma_loai_van_ban } })
                }
            }
        )
    }

    getLanhDaoDuyet = async () => {
        await API.VanBanDen.AU_VBDEN_DSLDDVBDE().then((arrLanhDaoDuyet) => {
            this.setState({ arrLanhDaoDuyet })
        })
    }

    onPressLanhDaoDuyet = () => {
        let { arrLanhDaoDuyet } = this.state
        ActionSheet.show(
            {
                options: arrLanhDaoDuyet.map(obj => {
                    return obj.ten + " - " + obj.ten_chuc_vu
                }),
                title: "Chọn lãnh đạo duyệt văn bản"
            },
            buttonIndex => {
                if (buttonIndex !== undefined) {
                    let ma_ctcb_duyet = arrLanhDaoDuyet[buttonIndex].ma
                    let ten_nguoi_duyet = arrLanhDaoDuyet[buttonIndex].ten
                    this.setState({ item: { ...this.state.item, ma_ctcb_duyet, ten_nguoi_duyet } })
                }
            }
        )
    }

    onPressNam = () => {
        let options = []
        for (i = -3; i <= 3; i++) {
            options.push((nowYear + i).toString())
        }
        ActionSheet.show(
            {
                options: options,
                title: "Chọn năm"
            },
            buttonIndex => {
                if (buttonIndex !== undefined) {
                    let nam = options[buttonIndex]
                    this.setState({ nam: nam }, () => {
                        this.getSoDen(this.state.arrSoVanBan[0].ma_so_vb_den_kc, nam)
                    })
                }
            }
        )
    }

    _showDatePickerNBH = () => this.setState({ isDatePickerToVisibleNBH: true })

    _hideDatePickerNBH = () => this.setState({ isDatePickerToVisibleNBH: false })

    _handleDatePickedNBH = (date) => { this.setState({ item: { ...this.state.item, ngay_ban_hanh: moment(date).format('DD/MM/YYYY') } }); this._hideDatePickerNBH() }

    _showDatePickerND = () => this.setState({ isDatePickerToVisibleND: true })

    _hideDatePickerND = () => this.setState({ isDatePickerToVisibleND: false })

    _handleDatePickedND = (date) => { this.setState({ item: { ...this.state.item, ngay_den: moment(date).format('DD/MM/YYYY') } }); this._hideDatePickerND() }

    onGuiLanhDao = () => {
        let { item } = this.state
        //Trạng thái = 2 lưu chuyển lãnh đạo duyệt
        item.trang_thai_van_ban_den = 2
        console.log(JSON.stringify(item))

        Alert.alert("Xác nhận", "Chuyển lãnh đạo duyệt?",
            [
                { text: "Không", style: "cancel" },
                {
                    text: "Đồng ý", onPress: () => {
                        API.VanBanDen.AU_VBDEN_TMVBDLN(item).then((response) => {
                            if (response) {
                                ToastSuccess("Chuyển duyệt thành công!", () => {
                                    this.props.navigation.dispatch(resetStack("VBDenVanThu"))
                                })
                            } else {
                                Toast.show({ text: "Xảy ra lỗi. Vui lòng thực hiện lại sau!", type: "danger", buttonText: "Đóng", position: "top", duration: 2000 })
                            }
                        })
                    }
                }
            ],
            { cancelable: false }
        )
    }


    renderButton = () => {
        return (
            <CardItem style={[this.styles.CardItem, this.styles.CardItemButton]}>
                <Button primary style={this.styles.Button} onPress={this.onGuiLanhDao}><Text style={this.styles.ButtonText}>Chuyển Lãnh đạo duyệt</Text></Button>
            </CardItem>
        )
    }

    render() {
        this.styles = this.props.styles

        if (this.props.dataDetails) {
            let { activeTab } = this.props
            const { arrSoVanBan, arrCapDoKhan, arrCapDoMat, arrLinhVucVanBan, arrLoaiVanBan, nam,
                isDatePickerToVisibleNBH, isDatePickerToVisibleND,
                item } = this.state

            return (
                <Card noShadow style={this.styles.Card}>
                    <CardItem style={this.styles.CardItem}>
                        <Text style={this.styles.textLeft}> Số ký hiệu</Text>
                        <TextInput
                            ref={input => { this.txtSoKyHieu = input }}
                            multiline={false}
                            editable={activeTab === 0 ? true : false}
                            style={{
                                width: "100%",
                                height: 45,
                                paddingRight: 18,
                                fontFamily: AppConfig.fontFamily,
                                color: "#000",
                                fontSize: this.props.fontSize.FontSizeNorman / PixelRatio.getFontScale(),
                                borderColor: AppConfig.defaultLineColor,
                                borderBottomWidth: AppConfig.defaultLineWidth,
                            }}
                            autoCapitalize="none"
                            returnKeyType={"done"}
                            value={item.so_ky_hieu}
                            onChangeText={(text) => this.setState({ item: { ...item, so_ky_hieu: text } })}
                        />
                    </CardItem>
                    <CardItem style={this.styles.CardItem}>
                        <Text style={this.styles.textLeft}> Trích yếu</Text>
                        <TextInput
                            ref={input => { this.txtTrichYeu = input }}
                            multiline={true}
                            editable={activeTab === 0 ? true : false}
                            style={{
                                width: "100%",
                                minHeight: 45,
                                maxHeight: 150,
                                fontFamily: AppConfig.fontFamily,
                                color: "#000",
                                fontSize: this.props.fontSize.FontSizeNorman / PixelRatio.getFontScale(),
                                borderColor: AppConfig.defaultLineColor,
                                borderBottomWidth: AppConfig.defaultLineWidth,
                            }}
                            autoCapitalize="none"
                            returnKeyType={"done"}
                            value={item.trich_yeu}
                            onChangeText={(text) => this.setState({ item: { ...item, trich_yeu: text } })}
                        />
                    </CardItem>
                    {(activeTab !== 0 && arrLinhVucVanBan.length > 0) && (
                        <CardItem style={this.styles.CardItem}>
                            <Text style={this.styles.textLeft}> Lĩnh vực văn bản</Text>
                            <TouchableOpacity onPress={this.onPressLinhVucVanBan} style={{ width: "100%", }}>
                                <TextInput
                                    ref={input => { this.txtSoKyHieu = input }}
                                    multiline={true}
                                    editable={false}
                                    style={{
                                        width: "100%",
                                        paddingRight: 18,
                                        fontFamily: AppConfig.fontFamily,
                                        color: "#000",
                                        fontSize: this.props.fontSize.FontSizeNorman / PixelRatio.getFontScale(),
                                        borderColor: AppConfig.defaultLineColor,
                                        borderBottomWidth: AppConfig.defaultLineWidth,
                                    }}
                                    autoCapitalize="none"
                                    returnKeyType={"done"}
                                    value={arrLinhVucVanBan.filter(el => el.ma_linh_vuc_van_ban_kc === item.ma_linh_vuc_van_ban)[0].ten_linh_vuc_van_ban}
                                />
                                <Icon name="caret-down" type="FontAwesome" style={{
                                    fontSize: this.props.fontSize.FontSizeNorman,
                                    top: 16,
                                    right: 0,
                                    position: "absolute",
                                }} />
                            </TouchableOpacity>
                        </CardItem>
                    )}

                    <View style={{ flexDirection: "row" }}>
                        {
                            (arrLoaiVanBan.length > 0) && (
                                <CardItem style={this.styles.CardItem}>
                                    <Text style={this.styles.textLeft}> Loại văn bản</Text>
                                    <TouchableOpacity onPress={this.onPressLoaiVanBan} style={{ width: "100%" }}>
                                        <TextInput
                                            ref={input => { this.txtSoKyHieu = input }}
                                            multiline={true}
                                            editable={false}
                                            style={{
                                                width: "100%",
                                                height: 45,
                                                paddingRight: 18,
                                                fontFamily: AppConfig.fontFamily,
                                                color: "#000",
                                                fontSize: this.props.fontSize.FontSizeNorman / PixelRatio.getFontScale(),
                                                borderColor: AppConfig.defaultLineColor,
                                                borderBottomWidth: AppConfig.defaultLineWidth,

                                            }}
                                            autoCapitalize="none"
                                            returnKeyType={"done"}
                                            value={arrLoaiVanBan.filter(el => el.ma_loai_van_ban_kc === item.ma_loai_van_ban)[0].ten_loai_van_ban}
                                        />
                                        <Icon name="caret-down" type="FontAwesome" style={{
                                            fontSize: this.props.fontSize.FontSizeNorman,
                                            top: 16,
                                            right: 0,
                                            position: "absolute",
                                        }} />
                                    </TouchableOpacity>
                                </CardItem>
                            )
                        }
                        <CardItem style={this.styles.CardItem}>
                            <Text style={this.styles.textLeft}> Số bản phát hành</Text>
                            <TextInput
                                ref={input => { this.txtSoDen = input }}
                                multiline={false}
                                style={{
                                    width: "100%",
                                    height: 45,
                                    fontFamily: AppConfig.fontFamily,
                                    fontSize: this.props.fontSize.FontSizeNorman / PixelRatio.getFontScale(),
                                    borderColor: AppConfig.defaultLineColor,
                                    borderBottomWidth: AppConfig.defaultLineWidth,
                                }}
                                autoCapitalize="none"
                                returnKeyType={"done"}
                                value={item.so_ban_phat_hanh.toString()}
                                onChangeText={(text) => this.setState({ item: { ...item, so_ban_phat_hanh: ReturnNumber(text) } })}
                            />
                        </CardItem>
                    </View>
                    <View style={{ flexDirection: "row" }}>
                        <CardItem style={this.styles.CardItem}>
                            <Text style={this.styles.textLeft}> Ngày ban hành</Text>
                            <TouchableOpacity onPress={this._showDatePickerNBH} style={{ width: "100%", }}>
                                <TextInput
                                    ref={input => { this.txtSoKyHieu = input }}
                                    multiline={false}
                                    editable={false}
                                    style={{
                                        width: "100%",
                                        height: 45,
                                        color: "#000",
                                        fontFamily: AppConfig.fontFamily,
                                        fontSize: this.props.fontSize.FontSizeNorman / PixelRatio.getFontScale(),
                                        borderColor: AppConfig.defaultLineColor,
                                        borderBottomWidth: AppConfig.defaultLineWidth,
                                    }}
                                    autoCapitalize="none"
                                    returnKeyType={"done"}
                                    value={item.ngay_ban_hanh.toString()}
                                />
                            </TouchableOpacity>
                            <DateTimePicker
                                isVisible={isDatePickerToVisibleNBH}
                                onConfirm={this._handleDatePickedNBH}
                                onCancel={this._hideDatePickerNBH}
                                titleIOS="Chọn ngày ban hành"
                                cancelTextIOS="Đóng"
                                confirmTextIOS="Chọn"
                                mode="date"
                                locale="vi"
                                date={new Date(moment(item.ngay_ban_hanh, "DD/MM/YYYY"))}
                            />
                        </CardItem>
                        <CardItem style={this.styles.CardItem}>
                            <Text style={this.styles.textLeft}> Ngày đến</Text>
                            <TouchableOpacity onPress={this._showDatePickerND} style={{ width: "100%", }}>
                                <TextInput
                                    ref={input => { this.txtSoKyHieu = input }}
                                    multiline={false}
                                    editable={false}
                                    style={{
                                        width: "100%",
                                        height: 45,
                                        color: "#000",
                                        fontFamily: AppConfig.fontFamily,
                                        fontSize: this.props.fontSize.FontSizeNorman / PixelRatio.getFontScale(),
                                        borderColor: AppConfig.defaultLineColor,
                                        borderBottomWidth: AppConfig.defaultLineWidth,
                                    }}
                                    autoCapitalize="none"
                                    returnKeyType={"done"}
                                    value={item.ngay_den.toString()}
                                />
                            </TouchableOpacity>
                            <DateTimePicker
                                isVisible={isDatePickerToVisibleND}
                                onConfirm={this._handleDatePickedND}
                                onCancel={this._hideDatePickerND}
                                titleIOS="Chọn ngày đến"
                                cancelTextIOS="Đóng"
                                confirmTextIOS="Chọn"
                                mode="date"
                                locale="vi"
                                date={new Date(moment(item.ngay_den, "DD/MM/YYYY"))}
                            />
                        </CardItem>
                    </View>
                    {
                        (arrSoVanBan.length > 0) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}> Sổ văn bản</Text>
                                <TouchableOpacity onPress={this.onPressSoVanBan} style={{ width: "100%", }}>
                                    <TextInput
                                        ref={input => { this.txtSoKyHieu = input }}
                                        multiline={true}
                                        editable={false}
                                        style={{
                                            width: "100%",
                                            height: 45,
                                            paddingRight: 18,
                                            fontFamily: AppConfig.fontFamily,
                                            color: "#000",
                                            fontSize: this.props.fontSize.FontSizeNorman / PixelRatio.getFontScale(),
                                            borderColor: AppConfig.defaultLineColor,
                                            borderBottomWidth: AppConfig.defaultLineWidth,
                                        }}
                                        autoCapitalize="none"
                                        returnKeyType={"done"}
                                        value={arrSoVanBan.filter(el => el.ma_so_vb_den_kc === item.ma_so_vb_den)[0].ten_so_vb_den}
                                    />
                                    <Icon name="caret-down" type="FontAwesome" style={{
                                        fontSize: this.props.fontSize.FontSizeNorman,
                                        top: 16,
                                        right: 0,
                                        position: "absolute",
                                    }} />
                                </TouchableOpacity>
                            </CardItem>
                        )
                    }
                    <View style={{ flexDirection: "row" }}>
                        <CardItem style={this.styles.CardItem}>
                            <Text style={this.styles.textLeft}> Năm</Text>
                            <TouchableOpacity onPress={this.onPressNam} style={{ width: "100%", }}>
                                <TextInput
                                    ref={input => { this.txtSoKyHieu = input }}
                                    multiline={false}
                                    editable={false}
                                    style={{
                                        width: "100%",
                                        height: 45,
                                        paddingRight: 18,
                                        color: "#000",
                                        fontFamily: AppConfig.fontFamily,
                                        fontSize: this.props.fontSize.FontSizeNorman / PixelRatio.getFontScale(),
                                        borderColor: AppConfig.defaultLineColor,
                                        borderBottomWidth: AppConfig.defaultLineWidth,
                                    }}
                                    autoCapitalize="none"
                                    returnKeyType={"done"}
                                    value={nam.toString()}
                                />
                                <Icon name="caret-down" type="FontAwesome" style={{
                                    fontSize: this.props.fontSize.FontSizeNorman,
                                    top: 16,
                                    right: 0,
                                    position: "absolute",
                                }} />
                            </TouchableOpacity>
                        </CardItem>
                        <CardItem style={this.styles.CardItem}>
                            <Text style={this.styles.textLeft}> Số đến</Text>
                            <TextInput
                                ref={input => { this.txtSoDen = input }}
                                multiline={false}
                                style={{
                                    width: "100%",
                                    height: 45,
                                    fontFamily: AppConfig.fontFamily,
                                    fontSize: this.props.fontSize.FontSizeNorman / PixelRatio.getFontScale(),
                                    borderColor: AppConfig.defaultLineColor,
                                    borderBottomWidth: AppConfig.defaultLineWidth,
                                }}
                                autoCapitalize="none"
                                returnKeyType={"done"}
                                value={item.so_den + ""}
                                onChangeText={(text) => this.setState({ item: { ...item, so_den: ReturnNumber(text) } })}
                            />
                        </CardItem>
                    </View>
                    <CardItem style={this.styles.CardItem}>
                        <Text style={this.styles.textLeft}> Cơ quan ban hành</Text>
                        <TextInput
                            ref={input => { this.txtSoKyHieu = input }}
                            multiline={true}
                            editable={false}
                            style={{
                                width: "100%",
                                fontFamily: AppConfig.fontFamily,
                                color: "#000",
                                fontSize: this.props.fontSize.FontSizeNorman / PixelRatio.getFontScale(),
                            }}
                            autoCapitalize="none"
                            returnKeyType={"done"}
                            value={item.ten_co_quan_ban_hanh}
                        />
                    </CardItem>
                    <CardItem style={this.styles.CardItem}>
                        <Text style={this.styles.textLeft}> Người ký</Text>
                        <TextInput
                            ref={input => { this.txtNguoiKy = input }}
                            multiline={true}
                            style={{
                                width: "100%",
                                minHeight: 45,
                                maxHeight: 150,
                                fontFamily: AppConfig.fontFamily,
                                color: "#000",
                                fontSize: this.props.fontSize.FontSizeNorman / PixelRatio.getFontScale(),
                                borderColor: AppConfig.defaultLineColor,
                                borderBottomWidth: AppConfig.defaultLineWidth,
                            }}
                            autoCapitalize="none"
                            returnKeyType={"done"}
                            value={item.nguoi_ky}
                            onChangeText={(text) => this.setState({ item: { ...item, nguoi_ky: text } })}
                        />
                    </CardItem>
                    <View style={{ flexDirection: "row" }}>
                        {
                            (arrCapDoKhan.length > 0) && (
                                <CardItem style={this.styles.CardItem}>
                                    <Text style={this.styles.textLeft}> Cấp độ khẩn</Text>
                                    <TouchableOpacity onPress={this.onPressCapDoKhan} style={{ width: "100%", }}>
                                        <TextInput
                                            ref={input => { this.txtSoKyHieu = input }}
                                            multiline={true}
                                            editable={false}
                                            style={{
                                                width: "100%",
                                                height: 45,
                                                paddingRight: 18,
                                                fontFamily: AppConfig.fontFamily,
                                                color: "#000",
                                                fontSize: this.props.fontSize.FontSizeNorman / PixelRatio.getFontScale(),
                                                borderColor: AppConfig.defaultLineColor,
                                                borderBottomWidth: AppConfig.defaultLineWidth,
                                            }}
                                            autoCapitalize="none"
                                            returnKeyType={"done"}
                                            value={arrCapDoKhan.filter(el => el.ma_cap_do_khan_kc === item.ma_cap_do_khan)[0].ten_cap_do_khan}
                                        />
                                        <Icon name="caret-down" type="FontAwesome" style={{
                                            fontSize: this.props.fontSize.FontSizeNorman,
                                            top: 16,
                                            right: 0,
                                            position: "absolute",
                                        }} />
                                    </TouchableOpacity>
                                </CardItem>
                            )
                        }
                        {
                            (arrCapDoMat.length > 0) && (
                                <CardItem style={this.styles.CardItem}>
                                    <Text style={this.styles.textLeft}> Cấp độ mật</Text>
                                    <TouchableOpacity onPress={this.onPressCapDoMat} style={{ width: "100%", }}>
                                        <TextInput
                                            ref={input => { this.txtSoKyHieu = input }}
                                            multiline={true}
                                            editable={false}
                                            style={{
                                                width: "100%",
                                                height: 45,
                                                paddingRight: 18,
                                                fontFamily: AppConfig.fontFamily,
                                                color: "#000",
                                                fontSize: this.props.fontSize.FontSizeNorman / PixelRatio.getFontScale(),
                                                borderColor: AppConfig.defaultLineColor,
                                                borderBottomWidth: AppConfig.defaultLineWidth,
                                            }}
                                            autoCapitalize="none"
                                            returnKeyType={"done"}
                                            value={arrCapDoMat.filter(el => el.ma_cap_do_mat_kc === item.ma_cap_do_mat)[0].ten_cap_do_mat}
                                        />
                                        <Icon name="caret-down" type="FontAwesome" style={{
                                            fontSize: this.props.fontSize.FontSizeNorman,
                                            top: 16,
                                            right: 0,
                                            position: "absolute",
                                        }} />
                                    </TouchableOpacity>
                                </CardItem>
                            )
                        }
                    </View>
                    <CardItem style={this.styles.CardItem}>
                        <Text style={this.styles.textLeft}> Lãnh đạo duyệt</Text>
                        <TouchableOpacity onPress={this.onPressLanhDaoDuyet} style={{ width: "100%", }}>
                            <TextInput
                                ref={input => { this.txtSoKyHieu = input }}
                                multiline={true}
                                editable={false}
                                style={{
                                    width: "100%",
                                    paddingRight: 18,
                                    fontFamily: AppConfig.fontFamily,
                                    color: "#000",
                                    fontSize: this.props.fontSize.FontSizeNorman / PixelRatio.getFontScale(),
                                    borderColor: AppConfig.defaultLineColor,
                                    borderBottomWidth: AppConfig.defaultLineWidth,
                                }}
                                autoCapitalize="none"
                                returnKeyType={"done"}
                                value={item.ten_nguoi_duyet}
                            />
                            <Icon name="caret-down" type="FontAwesome" style={{
                                fontSize: this.props.fontSize.FontSizeNorman,
                                top: 16,
                                right: 0,
                                position: "absolute",
                            }} />
                        </TouchableOpacity>
                    </CardItem>
                    {
                        (item.noi_luu_ban_chinh !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}> Nơi lưu bản chính</Text>
                                <TextInput
                                    ref={input => { this.txtSoKyHieu = input }}
                                    multiline={true}
                                    editable={false}
                                    style={{
                                        width: "100%",
                                        fontFamily: AppConfig.fontFamily,
                                        color: "#000",
                                        fontSize: this.props.fontSize.FontSizeNorman / PixelRatio.getFontScale(),
                                        //borderColor: AppConfig.defaultLineColor,
                                        //borderBottomWidth: AppConfig.defaultLineWidth,
                                    }}
                                    autoCapitalize="none"
                                    returnKeyType={"done"}
                                    value={item.noi_luu_ban_chinh}
                                />
                            </CardItem>
                        )
                    }
                    <CardItem style={this.styles.CardItem}>
                        <Text style={this.styles.textLeft}> Ghi chú</Text>
                        <TextInput
                            ref={input => { this.txtGhiChu = input }}
                            multiline={true}
                            style={{
                                width: "100%",
                                minHeight: 40,
                                maxHeight: 80,
                                fontFamily: AppConfig.fontFamily,
                                fontSize: this.props.fontSize.FontSizeNorman / PixelRatio.getFontScale(),
                                borderColor: AppConfig.defaultLineColor,
                                borderBottomWidth: AppConfig.defaultLineWidth,
                            }}
                            autoCapitalize="none"
                            returnKeyType={"done"}
                            value={item.ghi_chu}
                            onChangeText={(text) => this.setState({ item: { ...this.state.item, ghi_chu: text } })}
                        />
                    </CardItem>
                    {
                        (item.file_van_ban_bs !== null) && (
                            <View>
                                <CardItem style={this.styles.CardItem}>
                                    <Text style={this.styles.textLeft}> Tệp tin đính kèm</Text>
                                </CardItem>
                                <CardItem style={this.styles.CardItem}>
                                    <ViewFile
                                        ref={ref => { this.ViewFile = ref }}
                                        src_file={item.file_van_ban_bs}
                                        isUpdateViewCV={this.props.isUpdateViewCV}
                                        updateViewCV={this.props.updateViewCV}
                                        fontSize={this.props.fontSize}
                                        //fabActions={fabActionsTemp}
                                        fabPressItem={(name) => {
                                            this.ViewFile.toggleModal()
                                            switch (name) {
                                                case "btnDuyet":
                                                    setTimeout(() => {
                                                        this.props.onApproved()
                                                    }, 200);
                                                    break;
                                                case "btnDuyetChuyenLDK":
                                                    setTimeout(() => {
                                                        this.props.onForwardLDK()
                                                    }, 200);
                                                    break;
                                                case "btnLDChuyen":
                                                    setTimeout(() => {
                                                        this.props.onLDForward()
                                                    }, 200);
                                                    break;
                                                case "btnChuyen":
                                                    setTimeout(() => {
                                                        this.props.onForward()
                                                    }, 200);
                                                    break;
                                                case "btnDangXuLy":
                                                    setTimeout(() => {
                                                        this.props.onInProcess()
                                                    }, 200);
                                                    break;
                                                case "btnHoanThanh":
                                                    setTimeout(() => {
                                                        this.props.onSuccess()
                                                    }, 200);
                                                    break;
                                                default:
                                                    break;
                                            }
                                        }}
                                    />
                                </CardItem>
                            </View>
                        )
                    }
                    <View style={{ flexDirection: "row" }}>
                        <CardItem style={[this.styles.CardItem, { flexDirection: "row", justifyContent: "flex-start", paddingLeft: -5, marginTop: 5 }]}>
                            <CheckBox onPress={() => { this.setState({ item: { ...item, sms: item.sms === 1 ? 0 : 1 } }) }} checked={item.sms === 1 ? true : false} color={AppConfig.blueBackground}></CheckBox>
                            <Text style={[this.styles.textLeft, { marginLeft: 15 }]}>Sms</Text>
                        </CardItem>
                    </View>
                    {this.renderButton()}
                </Card>
            )
        } else {
            return (
                <View></View>
            )
        }
    }
}

class VBDen_ThemMoi extends Component {
    constructor(props) {
        super(props)
        this.state = {
            dataDetails: null,
            index: 0
        }

        this.isDuyet = false
        this.ma_xu_ly_den = 0
        this.ma_van_ban_den = 0
        this.item = {}
        this.activeTab = 2
        this.isFromNotify = false
        this.dataSource = []
    }

    componentDidMount = () => {
        this.isDuyet = this.props.navigation.getParam("isDuyet", false)
        this.activeTab = this.props.navigation.getParam("activeTab", 2)
        this.isFromNotify = this.props.navigation.getParam("isFromNotify", false)
        this.dataSource = this.props.navigation.getParam("dataSource", [])
        let index = this.props.navigation.getParam("index", 0)
        this.loadData(index)
    }

    loadData = (index) => {
        this.ma_xu_ly_den = this.dataSource[index].ma_xu_ly_den
        this.ma_van_ban_den = this.dataSource[index].ma_van_ban_den_kc
        if (this.activeTab === 0) {
            this.setState({ index, dataDetails: this.dataSource[index] })
        } else {
            this.getDetails(index)
        }
    }

    getDetails = async (index) => {
        let ma_xu_ly_den = this.ma_xu_ly_den
        let ma_van_ban_den = this.ma_van_ban_den
        let ma_ctcb = global.ma_ctcb_kc
        await API.VanBanDen.AU_VBDEN_VBDCT(ma_xu_ly_den, ma_van_ban_den, ma_ctcb).then((dataDetails) => {
            if (dataDetails.length > 0) {
                this.setState({ index, dataDetails: dataDetails[0] })
            } else {
                this.setState({ index })
            }
        })
    }

    renderPre = () => {
        let indexPre = this.state.index - 1
        if (indexPre >= 0) {
            return (
                <View style={this.styles.viewLeftBottomButton}>
                    <TouchableOpacity style={this.styles.buttonBottom} onPress={() => { this.loadData(indexPre) }}>
                        <Icon name="reply" type="FontAwesome" style={this.styles.iconBottom} />
                    </TouchableOpacity>
                </View>
            )
        }
    }

    renderNext = () => {
        let indexNext = this.state.index + 1
        let length = this.dataSource.length
        if (length !== 0 && indexNext < length) {
            return (
                <View style={this.styles.viewRightBottomButton}>
                    <TouchableOpacity style={this.styles.buttonBottom} onPress={() => { this.loadData(indexNext) }}>
                        <Icon name="share" type="FontAwesome" style={this.styles.iconBottom} />
                    </TouchableOpacity>
                </View>
            )
        }
    }

    render() {

        this.styles = StyleSheet.create({
            scrollContainer: {
                flex: 1,
                bottom: 0,
                backgroundColor: AppConfig.grayBackground
            },
            viewTieuDe: {
                backgroundColor: AppConfig.grayBackground,
                justifyContent: "center", alignContent: "center",
                flexWrap: "wrap",
                padding: 4,
            },
            textTieuDe: {
                fontSize: this.props.fontSize.FontSizeXLarge,
                fontWeight: "400",
                textAlign: "center"
            },

            //Table_ChiTiet_VBDEN
            Card: {
                marginLeft: 6,
                marginRight: 6,
                marginTop: 0,
                padding: 10,
                borderRadius: 6,
            },
            CardItem: {
                flex: 1,
                marginTop: AppConfig.defaultPadding,
                marginBottom: AppConfig.defaultPadding,
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "flex-start"
            },
            CardItemButton: {
                marginTop: 10,
                paddingTop: 10,
                borderTopColor: AppConfig.defaultLineColor,
                borderTopWidth: AppConfig.defaultLineWidth,
                flexDirection: 'row',
                flexWrap: "wrap",
                justifyContent: "center",
                alignItems: "center"
            },
            Button: {
                margin: 3,
                height: 40,
                justifyContent: "center",
                alignItems: "center",
            },
            ButtonText: {
                fontSize: this.props.fontSize.FontSizeNorman,
                textAlign: "center"
            },
            textLeft: {
                fontSize: this.props.fontSize.FontSizeNorman,
                color: AppConfig.grayText,
                alignSelf: "flex-start"
            },
            textRight: {
                paddingLeft: 5,
                fontSize: this.props.fontSize.FontSizeNorman,
                flexWrap: "wrap",
                flexShrink: 1,
            },
            viewLeftBottomButton: {
                left: 15,
                bottom: 15,
                position: "absolute"
            },
            viewRightBottomButton: {
                right: 15,
                bottom: 15,
                position: "absolute"
            },
            buttonBottom: {
                width: 40,
                height: 40,
                borderRadius: 20,
                backgroundColor: "transparent",
                borderWidth: 0.7,
                borderColor: "#BDBDBD",
                justifyContent: "center",
                alignItems: "center",
            },
            iconBottom: {
                fontSize: this.props.FontSizeSmall,
                color: "#BDBDBD"
            }
        })

        const popAction = StackActions.pop({ n: 1 })
        return (
            <Container style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
                <HeaderWithLeft title="Thêm mới văn bản đến" buttonName="arrow-back" onPressLeftButton={() => {
                    if (this.isFromNotify) {
                        if (this.isDuyet) {
                            this.props.navigation.dispatch(resetStack("VBDenDuyet"))
                        } else {
                            this.props.navigation.dispatch(resetStack("VBDenXuLy"))
                        }
                    } else {
                        this.props.navigation.dispatch(popAction)
                    }
                }} />
                <ScrollView
                    bounces={false}
                    removeClippedSubviews={true}
                    scrollEnabled={true}
                    style={this.styles.scrollContainer}
                >
                    <Table_ChiTiet_VBDEN
                        navigation={this.props.navigation}
                        dataDetails={this.state.dataDetails}
                        isUpdateViewCV={this.isUpdateViewCV}
                        updateViewCV={this.updateViewCV}
                        onApproved={this.onApproved}
                        onForwardLDK={this.onForwardLDK}
                        onSuccess={() => { this.onSuccess(this.ma_xu_ly_den, true) }}
                        onForward={this.onForward}
                        onInProcess={() => { this.onInProcess(this.ma_xu_ly_den, true) }}
                        onLDForward={this.onLDForward}
                        isDuyet={this.isDuyet}
                        activeTab={this.activeTab}
                        fontSize={this.props.fontSize}
                        styles={this.styles}
                    />
                </ScrollView>
                {this.renderPre()}
                {this.renderNext()}
            </Container >
        );
    }
}

function mapStateToProps(state) {
    return {
        fontSize: state.fontSize,
    }
}

export default connect(mapStateToProps)(VBDen_ThemMoi)