import React, { Component } from 'react'
import { StyleSheet, View, Modal, FlatList, Dimensions, TouchableOpacity, PixelRatio, Alert } from "react-native"
import { Container, Tabs, Tab, Card, Text, Button, Icon, Toast, Item, Left, Spinner, CardItem, Textarea, CheckBox, ActionSheet } from "native-base"
import { AppConfig } from "../../AppConfig"
import { connect } from "react-redux"
import HeaderWithLeftRight from "../../components/Header/HeaderWithLeftRight"
import TreeChuyenCDTT from "../../components/treeChuyenCDTT"
import TreeChuyenNCB from "../../components/treeChuyenNCB"
import { mergeArrayDSChuyen, listToTree, resetStack } from "../../untils/TextUntils"
import DateTimePicker from 'react-native-modal-datetime-picker'
import moment from 'moment'
import API from "../../networks"
import Combobox from "../../components/Combobox"
import { StackActions } from "react-navigation"
import * as Models from "../../Models"

class ModalNguoiNhan extends Component {
    render() {
        this.styles = this.props.styles
        return (
            <Modal
                transparent={false}
                animationType="slide"
                visible={this.props.isModalVisible}
                supportedOrientations={["landscape", "landscape-left", "landscape-right", "portrait", "portrait-upside-down"]}
            >
                <View style={{ flex: 1, padding: 15, backgroundColor: AppConfig.grayBackground }}>
                    <View style={{ flex: 1, backgroundColor: "#FFFFFF", borderRadius: 10 }}>
                        <View style={{ position: 'absolute', zIndex: 99, height: 55, left: 0, right: 0, top: 0, justifyContent: "center", padding: 4, alignItems: "center" }}>
                            <Text style={{ fontSize: this.props.fontSize.FontSizeLarge }}>Danh sách cán bộ nhận</Text>
                        </View>
                        <FlatList
                            bounces={false}
                            ref={(ref) => { this.flatListRef = ref }}
                            data={this.props.listMCB_DVCB}
                            keyExtractor={(item, index) => "key" + item.ma + index}
                            style={{ position: 'absolute', left: 0, right: 0, top: 55, bottom: 55 }}
                            initialNumToRender={50}
                            renderItem={({ item, index }) => {
                                let color = item.vai_tro_chuyen === "Xử lý chính" ? "red" : item.vai_tro_chuyen === "Phối hợp" ? AppConfig.blueBackground : "black"
                                return (
                                    <Item key={item.ma} style={{ padding: 12, borderBottomWidth: AppConfig.defaultLineWidth, borderColor: AppConfig.defaultLineColor }}>
                                        <Left>
                                            <Text style={{ fontSize: this.props.fontSize.FontSizeNorman, color }}>{item.ten}</Text>
                                            <Text note style={{ fontSize: this.props.fontSize.FontSizeSmall, color }}>Chức vụ: {item.ten_chuc_vu}</Text>
                                            {
                                                (item.vai_tro_chuyen !== "") && (
                                                    <Text note style={{ fontSize: this.props.fontSize.FontSizeSmall, color }}>Vai trò: {item.vai_tro_chuyen}</Text>
                                                )
                                            }
                                        </Left>
                                    </Item>
                                )
                            }}
                            ListEmptyComponent={() => {
                                return (
                                    <View style={{ alignItems: "center", marginTop: 10, }}>
                                        <Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>Không có dữ liệu</Text>
                                    </View>
                                )
                            }}
                            removeClippedSubviews={true}
                        />
                        {
                            (this.props.isSending) ? (
                                <View style={{ position: 'absolute', zIndex: 99, height: 55, left: 0, right: 0, bottom: 0, flexDirection: "row", justifyContent: "center", paddingTop: 10, alignItems: "center" }}>
                                    <Spinner />
                                    <Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}> Đang thực hiện</Text>
                                </View>
                            ) : (
                                    <View style={{ position: 'absolute', zIndex: 99, height: 55, left: 0, right: 0, bottom: 0, flexDirection: "row", justifyContent: "space-evenly", paddingTop: 10, alignItems: "center" }}>
                                        <Button small bordered rounded iconLeft onPress={_ => this.props.onPressSend()}><Icon name="done" type="MaterialIcons"></Icon><Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>Gửi</Text></Button>
                                        <Button small danger bordered rounded iconLeft onPress={_ => this.props.toggleModal()}><Icon name="close" type="MaterialIcons"></Icon><Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>Chọn lại</Text></Button>
                                    </View>
                                )
                        }

                    </View>
                </View>
            </Modal>
        )
    }
}

class VBDen_Chuyen_XuLy extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isModalVisible: false,
            yKienXuLy: "",
            checkSms: false,
            hanXuLy: null,
            isDatePickerToVisible: false,
            ma_trang_thai: AppConfig.listTrangThaiVBDenChuyen[1].ma_trang_thai,
            ten_trang_thai: AppConfig.listTrangThaiVBDenChuyen[1].ten_trang_thai,
            dataTreeCDTT: [],
            dataTreeNCB: [],
            isLoading: true,
            isSending: false,
        }
        this.listMCB_DVCB = []
        this.currentTab = 0
        this.hasChangeTab = false
        this.inVBDEN_Details_Chuyen = new Models.npVBDen_Details_Chuyen()
    }

    componentWillMount() {
        Dimensions.addEventListener("change", this.onResizeScreen)
        this.inVBDEN_Details_Chuyen = this.props.navigation.getParam("inVBDEN_Details_Chuyen")
        this.getDSCDTT()
        this.getDSNCB()
    }


    componentWillUnmount() {
        Dimensions.removeEventListener("change", this.onResizeScreen)
    }

    getDSCDTT = () => {
        API.VanBanDen.AU_VBDEN_DSCBTDVTTS(global.ma_don_vi_quan_tri).then((dataTreeCDTT) => {
            this.setState({ dataTreeCDTT, isLoading: false })
        })
    }

    getDSNCB = () => {
        const ma_can_bo = global.ma_can_bo
        API.VanBanDen.AU_VBDEN_DSNCBNVBDLDD(ma_can_bo).then((dataTreeNCB) => {
            let tree = listToTree(dataTreeNCB, {
                idKey: "id",
                parentKey: "parent"
            })
            this.setState({ dataTreeNCB: tree })
        })
    }


    onResizeScreen = () => {
        setTimeout(() => {
            this.hasChangeTab = false
        }, 500)
    }

    _showTrangThai = () => {
        ActionSheet.show(
            {
                options: AppConfig.listTrangThaiVBDenChuyen.map(obj => {
                    return obj.ten_trang_thai
                }),
                title: "Chọn trạng thái"
            },
            buttonIndex => {
                if (buttonIndex !== undefined) {
                    this.setState({
                        ma_trang_thai: AppConfig.listTrangThaiVBDenChuyen[buttonIndex].ma_trang_thai,
                        ten_trang_thai: AppConfig.listTrangThaiVBDenChuyen[buttonIndex].ten_trang_thai,
                    })
                }
            }
        )
    }

    onCheckSms = () => { this.setState({ checkSms: !this.state.checkSms }) }

    onCheckEmail = () => { this.setState({ checkEmail: !this.state.checkEmail }) }

    _showDatePicker = () => this.setState({ isDatePickerToVisible: true })

    _hideDatePicker = () => this.setState({ isDatePickerToVisible: false })

    _handleDatePicked = (date) => {
        let toDay = new Date()
        toDay = moment(toDay, "DD/MM/YYYY").subtract(1, "days")
        let newDay = moment(date, "DD/MM/YYYY")
        if (moment(newDay).isAfter(toDay)) {
            this.setState({ hanXuLy: newDay.format('DD/MM/YYYY') });
            this._hideDatePicker()
        } else {
            Toast.show({ text: "Hạn xử lý không nhỏ hơn ngày hiện tại!", type: "warning", buttonText: "Đóng", position: "top", duration: 3000 })
            this._hideDatePicker()
        }
    }

    toggleModal = () => {
        this.listMCB_DVCB = []
        if (!this.state.isModalVisible) {
            let listTab1 = []
            let listTab2 = []

            try {
                listTab1 = this.treeChuyenCDTT.getDataCanBo_XLC_PH()
            } catch (error) {
                console.log("Error treeChuyenCDTT.getDataCanBo_XLC_PH: " + error)
            }

            try {
                listTab2 = this.treeChuyenNCB.getDataCanBo_XLC_PH()
            } catch (error) {
                console.log("Error treeChuyenNCB.getDataCanBo_XLC_PH: " + error)
            }

            this.listMCB_DVCB = mergeArrayDSChuyen(listTab1, listTab2)
            if (this.listMCB_DVCB.length > 0) {
                this.setState({ isModalVisible: true })
            } else {
                Toast.show({ text: "Vui lòng chọn cán bộ nhận!", type: "warning", buttonText: "Đóng", position: "top", duration: 2000 })
            }
        } else {
            this.setState({ isModalVisible: false })
        }
    }

    onPressSend = () => {
        this.setState({ isSending: true })
        let chuoi_ma_ctcb_nhan = []
        this.listMCB_DVCB.forEach(element => {
            let itemTemp = new Object()
            itemTemp.ma_ctcb_nhan = parseInt(element.ma.replace("CB", ""))
            itemTemp.ma_yeu_cau = parseInt(element.ma_yeu_cau)
            itemTemp.han_xu_ly = this.state.hanXuLy ? this.state.hanXuLy : ""
            itemTemp.sms = parseInt(this.state.checkSms ? 1 : 0)
            itemTemp.email = parseInt(this.state.checkEmail ? 1 : 0)
            chuoi_ma_ctcb_nhan.push(itemTemp)
        });

        if (global.vbde_xlc_phai_chuyen_ld_pto == 1 && global.lanh_dao === 0 && this.inVBDEN_Details_Chuyen.ma_yeu_cau_real !== 1) {
            console.log("Bắt theo luồng PTO")
            //Bắt theo luồng PTO
            API.VanBanDen.AU_VBDEN_CCVCAP(
                0,
                this.inVBDEN_Details_Chuyen.ma_van_ban_den,
                this.inVBDEN_Details_Chuyen.ma_xu_ly_den,
                this.inVBDEN_Details_Chuyen.ma_ctcb_gui,
                chuoi_ma_ctcb_nhan,
                this.state.hanXuLy ? this.state.hanXuLy : "",
                this.inVBDEN_Details_Chuyen.ma_yeu_cau,
                this.state.checkSms ? 1 : 0,
                this.state.yKienXuLy
            ).then((response) => {
                if (response === 1) {
                    API.VanBanDen.AU_VBDEN_CVXLVBD(this.inVBDEN_Details_Chuyen.ma_ctcb_gui, this.inVBDEN_Details_Chuyen.ma_xu_ly_den, this.state.yKienXuLy, this.state.ma_trang_thai).then((response) => {
                        if (response) {
                            API.VanBanDen.AU_VBDEN_CVCTH(
                                0,
                                this.inVBDEN_Details_Chuyen.ma_van_ban_den,
                                this.inVBDEN_Details_Chuyen.ma_xu_ly_den,
                                this.inVBDEN_Details_Chuyen.ma_ctcb_gui,
                                chuoi_ma_ctcb_nhan,
                                this.state.hanXuLy ? this.state.hanXuLy : "",
                                this.inVBDEN_Details_Chuyen.ma_yeu_cau,
                                this.state.checkSms ? 1 : 0,
                                this.state.yKienXuLy
                            ).then((response) => {
                                if (response !== "") {
                                    API.Login.AU_DSNVCB(global.ma_ctcb_kc, this.props.dispatch)
                                    if (this.state.checkSms) {
                                        let arrSDT = []
                                        for (let item of this.listMCB_DVCB) {
                                            if (item.di_dong_can_bo) {
                                                arrSDT.push(item.di_dong_can_bo)
                                            }
                                        }

                                        if (arrSDT.length > 0) {
                                            let noi_dung = this.inVBDEN_Details_Chuyen.trich_yeu
                                            let chuoi_di_dong_nhan = arrSDT.join(",")
                                            API.NhanTinSMS.AU_SMS_GTN(noi_dung, chuoi_di_dong_nhan)
                                        }
                                    }

                                    let arrMCB = this.listMCB_DVCB.map(obj => {
                                        return obj.ma.replace("CB", "")
                                    })
                                    API.Login.AU_PUSH_FCM_APP(arrMCB.join(","), "vanbandenchoxuly", this.inVBDEN_Details_Chuyen.trich_yeu, 0, 0, 0, response)

                                    // Đăng AGG Check tham số cd_chuyen_tiep_cddh_song_song_vbde = 1 => Chuyển CĐĐH song song với VB đến cho cán bộ XLC
                                    if (global.cd_chuyen_tiep_cddh_song_song_vbde == 1 && chuoi_ma_ctcb_nhan.filter(x => x.ma_yeu_cau == 2).length > 0) {
                                        API.ChiDaoDieuHanh.AU_CDDH_PCNVTUVBD(
                                            this.inVBDEN_Details_Chuyen.ma_van_ban_den,
                                            this.inVBDEN_Details_Chuyen.ma_ctcb_gui,
                                            this.state.yKienXuLy,
                                            chuoi_ma_ctcb_nhan
                                        ).then((response) => {
                                        })
                                    }

                                    setTimeout(() => {
                                        this.props.navigation.dispatch(resetStack("VBDenXuLy"))
                                    }, 100);
                                } else {
                                    Alert.alert("Thông báo", "Chuyển xử lý thất bại. Vui lòng thử lại sau!")
                                    this.setState({ isSending: false })
                                }
                            })
                        } else {
                            Alert.alert("Thông báo", "Chuyển xử lý thất bại. Vui lòng thử lại sau!")
                            this.setState({ isSending: false })
                        }
                    })
                } else {
                    Alert.alert("Thông báo", "Bạn không thể chuyển. Vui lòng chọn lại cán bộ!")
                    this.setState({ isSending: false })
                }
            })
        } else {
            //Luồng thông thường
            API.VanBanDen.AU_VBDEN_CVXLVBD(this.inVBDEN_Details_Chuyen.ma_ctcb_gui, this.inVBDEN_Details_Chuyen.ma_xu_ly_den, this.state.yKienXuLy, this.state.ma_trang_thai).then((response) => {
                if (response) {
                    API.VanBanDen.AU_VBDEN_CVCTH(
                        0,
                        this.inVBDEN_Details_Chuyen.ma_van_ban_den,
                        this.inVBDEN_Details_Chuyen.ma_xu_ly_den,
                        this.inVBDEN_Details_Chuyen.ma_ctcb_gui,
                        chuoi_ma_ctcb_nhan,
                        this.state.hanXuLy ? this.state.hanXuLy : "",
                        this.inVBDEN_Details_Chuyen.ma_yeu_cau,
                        this.state.checkSms ? 1 : 0,
                        this.state.yKienXuLy
                    ).then((response) => {
                        if (response !== "") {
                            API.Login.AU_DSNVCB(global.ma_ctcb_kc, this.props.dispatch)
                            if (this.state.checkSms) {
                                let arrSDT = []
                                for (let item of this.listMCB_DVCB) {
                                    if (item.di_dong_can_bo) {
                                        arrSDT.push(item.di_dong_can_bo)
                                    }
                                }

                                if (arrSDT.length > 0) {
                                    let noi_dung = this.inVBDEN_Details_Chuyen.trich_yeu
                                    let chuoi_di_dong_nhan = arrSDT.join(",")
                                    API.NhanTinSMS.AU_SMS_GTN(noi_dung, chuoi_di_dong_nhan)
                                }
                            }

                            let arrMCB = this.listMCB_DVCB.map(obj => {
                                return obj.ma.replace("CB", "")
                            })
                            API.Login.AU_PUSH_FCM_APP(arrMCB.join(","), "vanbandenchoxuly", this.inVBDEN_Details_Chuyen.trich_yeu, 0, 0, 0, response)

                            // Đăng AGG Check tham số cd_chuyen_tiep_cddh_song_song_vbde = 1 => Chuyển CĐĐH song song với VB đến cho cán bộ XLC
                            if (global.cd_chuyen_tiep_cddh_song_song_vbde == 1 && chuoi_ma_ctcb_nhan.filter(x => x.ma_yeu_cau == 2).length > 0) {
                                API.ChiDaoDieuHanh.AU_CDDH_PCNVTUVBD(
                                    this.inVBDEN_Details_Chuyen.ma_van_ban_den,
                                    this.inVBDEN_Details_Chuyen.ma_ctcb_gui,
                                    this.state.yKienXuLy,
                                    chuoi_ma_ctcb_nhan
                                ).then((response) => {
                                })
                            }

                            setTimeout(() => {
                                this.props.navigation.dispatch(resetStack("VBDenXuLy"))
                            }, 100);
                        } else {
                            Alert.alert("Thông báo", "Chuyển xử lý thất bại. Vui lòng thử lại sau!")
                            this.setState({ isSending: false })
                        }
                    })
                } else {
                    Alert.alert("Thông báo", "Chuyển xử lý thất bại. Vui lòng thử lại sau!")
                    this.setState({ isSending: false })
                }
            })
        }


    }

    getPrimaryCheck = () => {
        let ma_yeu_cau = this.inVBDEN_Details_Chuyen.ma_yeu_cau
        if (ma_yeu_cau === 2) {
            return global.US_VBDEN_Tree_Default_Check
        } else if (ma_yeu_cau === 3) {
            if (global.US_VBDEN_Tree_Default_Check === "3") {
                return 2
            }
            return global.US_VBDEN_Tree_Default_Check
        }
        return 1
    }

    getNumberSelection = () => {
        let ma_yeu_cau = this.inVBDEN_Details_Chuyen.ma_yeu_cau
        if (ma_yeu_cau === 2) {
            return 3
        } else if (ma_yeu_cau === 3) {
            return 2
        }
        return 1
    }

    render() {

        this.styles = StyleSheet.create({
            tabsContainerStyle: {
                width: 300
            },
            tabStyle: {
                flex: 1,
                backgroundColor: "#1E88E5"
            },
            activeTabStyle: {
                backgroundColor: AppConfig.blueBackground
            },
            textStyle: {
                fontSize: this.props.fontSize.FontSizeNorman,
                color: "#FFFFFF",
                opacity: 0.6,
            },
            activeTextStyle: {
                fontSize: this.props.fontSize.FontSizeNorman,
                color: "#FFFFFF",
                opacity: 1,
            },
            scrollContainer: {
                flex: 1,
                marginTop: 10,
                marginLeft: 10,
                marginRight: 10
            },
            CardItem: {
                paddingTop: 3,
                paddingBottom: 3,
                alignItems: 'center'
            },
            CardItemLeft: {
                width: 90
            },
            CardItemRight: {
                flex: 1,
            },
            TextTitle: {
                fontSize: this.props.fontSize.FontSizeSmall,
            },
            TextInput: {
                flex: 1,
                fontFamily: AppConfig.fontFamily,
                fontSize: this.props.fontSize.FontSizeNorman / PixelRatio.getFontScale(),
                color: "#000000",
                borderColor: AppConfig.defaultLineColor,
                borderWidth: AppConfig.defaultLineWidth,
                borderRadius: 5,
                marginRight: 2,
                paddingLeft: 5,
                paddingTop: 7,
                minHeight: 40,
            },
            TextHXL: {
                flex: 1,
                fontFamily: AppConfig.fontFamily,
                fontSize: this.props.fontSize.FontSizeNorman,
                color: "#000000",
                borderColor: AppConfig.defaultLineColor,
                borderWidth: AppConfig.defaultLineWidth,
                borderRadius: 5,
                marginRight: 2,
                paddingLeft: 5,
                paddingTop: 7,
                minHeight: 40,
            }
        })

        let { dataTreeDVCB, dataTreeCDTT, dataTreeNCB, isLoading, isSending, isModalVisible, ten_trang_thai, hanXuLy, isDatePickerToVisible, yKienXuLy, checkSms } = this.state
        const popAction = StackActions.pop({ n: 1 })
        let numberSelection = this.getNumberSelection()
        let primaryCheck = parseInt(this.getPrimaryCheck())

        return (
            <Container style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
                <HeaderWithLeftRight
                    title={"Chuyển văn bản đến"}
                    buttonLeftName="arrow-back"
                    buttonRightName="done"
                    onPressLeftButton={() => { this.props.navigation.dispatch(popAction) }}
                    onPressRightButton={this.toggleModal}
                />
                {
                    (isModalVisible) && (
                        <ModalNguoiNhan
                            listMCB_DVCB={this.listMCB_DVCB}
                            isModalVisible={isModalVisible}
                            toggleModal={() => this.toggleModal()}
                            onPressSend={this.onPressSend}
                            isSending={isSending}
                            fontSize={this.props.fontSize}
                            styles={this.styles}
                        />
                    )
                }
                <Combobox Title="Trạng thái" textValue={ten_trang_thai} onPress={this._showTrangThai} padding={3} />
                <CardItem style={this.styles.CardItem}>
                    <CardItem style={[this.styles.CardItemLeft]}>
                        <Text style={[this.styles.TextTitle]}>Hạn xử lý</Text>
                    </CardItem>
                    <CardItem style={[this.styles.CardItemRight]}>
                        <TouchableOpacity onPress={this._showDatePicker} style={{ flex: 1, flexDirection: 'row', }}>
                            <Text editable={false} style={this.styles.TextHXL}>{hanXuLy ? hanXuLy : 'dd/mm/yyyy'}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => { this.setState({ hanXuLy: null }) }} style={{ position: 'absolute', zIndex: 999, right: 0, width: 40, height: 40 }}>
                            <Icon name="delete" type="MaterialIcons" fontSize={this.props.fontSize.FontSizeXXLarge} style={{ color: "red", top: 5, left: 5 }}></Icon>
                        </TouchableOpacity>
                        <DateTimePicker
                            isVisible={isDatePickerToVisible}
                            onConfirm={this._handleDatePicked}
                            onCancel={this._hideDatePicker}
                            titleIOS="Chọn 1 ngày"
                            cancelTextIOS="Đóng"
                            confirmTextIOS="Chọn"
                            mode="date"
                            locale="vi"
                            date={this.state.hanXuLy ? new Date(moment(this.state.hanXuLy, "DD/MM/YYYY")) : new Date()}
                        />
                    </CardItem>
                </CardItem>
                <CardItem style={this.styles.CardItem}>
                    <CardItem style={[this.styles.CardItemLeft]}>
                        <Text style={[this.styles.TextTitle]}>Ý kiến xử lý</Text>
                    </CardItem>
                    <CardItem style={[this.styles.CardItemRight]}>
                        <Textarea
                            autoCorrect={false}
                            style={this.styles.TextInput}
                            rowSpan={3}
                            value={yKienXuLy}
                            onChangeText={(text) => { this.setState({ yKienXuLy: text }) }}
                        />
                    </CardItem>
                </CardItem>
                <CardItem style={this.styles.CardItem}>
                    <CardItem style={[this.styles.CardItemLeft]}></CardItem>
                    <CardItem style={[this.styles.CardItemRight, { marginLeft: -10 }]}>
                        <CheckBox onPress={this.onCheckSms} checked={checkSms} color={AppConfig.blueBackground}></CheckBox>
                        <Text style={[this.styles.TextTitle, { marginLeft: 20 }]}>Sms        </Text>
                    </CardItem>
                </CardItem>
                <View style={{ flex: 1 }}>
                    <Tabs onChangeTab={() => { this.hasChangeTab = true }}>
                        <Tab
                            tabStyle={this.styles.tabStyle}
                            activeTabStyle={this.styles.activeTabStyle}
                            textStyle={this.styles.textStyle}
                            activeTextStyle={this.styles.activeTextStyle}
                            heading="Cán bộ"
                        >
                            <Card noShadow style={{ flex: 1, borderWidth: 1 }}>
                                {
                                    (isLoading) && (
                                        <Spinner />
                                    )
                                }
                                {
                                    (!isLoading) && (
                                        <TreeChuyenCDTT
                                            ref={treeCDTT => { this.treeChuyenCDTT = treeCDTT }}
                                            title="Họ tên"
                                            firstChkTitle="Xem"
                                            secondChkTitle="PH"
                                            threeChkTitle="XLC"
                                            numberSelection={numberSelection}
                                            data={dataTreeCDTT}
                                            primaryCheck={primaryCheck}
                                            onItemPress={(ma, once) => { console.log("press item: " + ma) }}
                                            fontSize={this.props.fontSize}
                                        />
                                    )
                                }
                            </Card>
                        </Tab>
                        <Tab
                            tabStyle={this.styles.tabStyle}
                            activeTabStyle={this.styles.activeTabStyle}
                            textStyle={this.styles.textStyle}
                            activeTextStyle={this.styles.activeTextStyle}
                            heading="Nhóm cán bộ"
                        >
                            <Card noShadow style={{ flex: 1, borderWidth: 1 }}>
                                <TreeChuyenNCB
                                    ref={treeNCB => { this.treeChuyenNCB = treeNCB }}
                                    title="Họ tên"
                                    firstChkTitle="Xem"
                                    secondChkTitle="PH"
                                    threeChkTitle="XLC"
                                    numberSelection={numberSelection}
                                    data={dataTreeNCB}
                                    primaryCheck={primaryCheck}
                                    onItemPress={(ma, once) => { console.log("press item: " + ma) }}
                                    fontSize={this.props.fontSize}
                                />
                            </Card>
                        </Tab>
                    </Tabs>
                </View>
            </Container>
        )
    }
}

function mapStateToProps(state) {
    return {
        fontSize: state.fontSize,
    }
}

export default connect(mapStateToProps)(VBDen_Chuyen_XuLy)
