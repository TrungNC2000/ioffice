import React, { Component, PureComponent } from 'react';
import { View, StyleSheet, FlatList, Alert, TouchableOpacity, Platform } from "react-native"
import { Container, Text, CardItem, Spinner, Toast } from "native-base"
import { DrawerActions } from "react-navigation"
import { connect } from "react-redux"
import HeaderWithLeft from "../../components/Header/HeaderWithLeft"
import FooterTab from "../../components/Footer/FooterTab"
import SearchHeader from "../../components/SearchHeader"
import MyTab from "../../components/Tab"
import API from "../../networks"
import { AppConfig } from "../../AppConfig"
import { CheckSession } from "../../untils/NetInfo"
import { getFileChiTietTTDH, RemoveNoiDungTTDH, ConvertDateTime, ConvertDateTimeDetail, ToastSuccess, checkVersionWeb, getNVNew } from "../../untils/TextUntils"
import VBDen_Details_iPad from "./VBDen_Details_iPad"

const nowDate = new Date()

class CV_XLTab_Item extends PureComponent {
    constructor(props) {
        super(props)
        this.state = {
            isModalVisible: false,
            isBold: this.props.item.ngay_xem === null ? true : false
        }
    }

    onPressItem = () => {
        this.updateViewCV()
        if (this.props.activeTab === 0) {
            let item = this.props.item
            if (item.da_luu_file === 0 && item.chuoi_vb_dien_tu) {
                let chuc_nang = "vanbanden"
                let don_vi = global.ma_don_vi_quan_tri
                let ma_ctcb = global.ma_ctcb_kc
                let partition = global.FILE_PARTITION
                let ten_file = item.chuoi_vb_dien_tu

                API.VanBanDen.AU_VBDEN_UTLT(chuc_nang, don_vi, ma_ctcb, partition, ten_file).then(response => {
                    if (response !== "") {
                        API.VanBanDen.AU_VBDEN_CNCVBDDTLT(item.ma_van_ban_den_kc, response).then(response1 => {
                            if (response1) {
                                let index = this.props.index
                                let dataSource = this.props.dataSource
                                dataSource[index].file_van_ban_bs = response
                                dataSource[index].file_van_ban = response
                                dataSource[index].da_luu_file = 1
                                setTimeout(() => {
                                    this.props.navigation.navigate("VBDen_ThemMoi", {
                                        isDuyet: false,
                                        onSuccess: this.props.onSuccess,
                                        onInProcess: this.props.onInProcess,
                                        isUpdateViewCV: this.state.isBold,
                                        updateViewCV: this.updateViewCV,
                                        activeTab: this.props.activeTab,
                                        index: index,
                                        dataSource: dataSource
                                    })
                                }, 1000);
                            } else {
                                Toast.show({ text: "Vui lòng thực hiện lại sau!", type: "warning", buttonText: "Đóng", position: "top", duration: 2000 })
                            }
                        })
                    } else {
                        Toast.show({ text: "Vui lòng thực hiện lại sau!", type: "warning", buttonText: "Đóng", position: "top", duration: 2000 })
                    }
                })
            } else {
                this.props.navigation.navigate("VBDen_ThemMoi", {
                    isDuyet: false,
                    onSuccess: this.props.onSuccess,
                    onInProcess: this.props.onInProcess,
                    isUpdateViewCV: this.state.isBold,
                    updateViewCV: this.updateViewCV,
                    activeTab: this.props.activeTab,
                    index: this.props.index,
                    dataSource: this.props.dataSource
                })
            }
        } else if (this.props.activeTab === 1) {
            this.props.navigation.navigate("VBDen_LuuNhap", {
                isDuyet: false,
                onSuccess: this.props.onSuccess,
                onInProcess: this.props.onInProcess,
                isUpdateViewCV: this.state.isBold,
                updateViewCV: this.updateViewCV,
                activeTab: this.props.activeTab,
                index: this.props.index,
                dataSource: this.props.dataSource
            })
        } else {
            this.props.navigation.navigate("VBDen_Details", {
                isDuyet: false,
                onSuccess: this.props.onSuccess,
                onInProcess: this.props.onInProcess,
                isUpdateViewCV: this.state.isBold,
                updateViewCV: this.updateViewCV,
                activeTab: 99,
                index: this.props.index,
                dataSource: this.props.dataSource
            })
        }
    }

    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible })
    }

    onViewFile = () => {
        this.toggleModal()
    }

    updateViewCV = async () => {
        let chuoi_ma_xu_ly_den = this.props.item.ma_xu_ly_den
        let ma_ctcb = global.ma_ctcb_kc
        await API.VanBanDen.AU_VBDEN_CVXVBD(chuoi_ma_xu_ly_den, ma_ctcb).then((result) => {
            if (result) {
                this.setState({ isBold: false })
            }
        })
    }

    render() {
        this.styles = this.props.styles
        const { item } = this.props
        let dataFile = item.file_van_ban_bs ? getFileChiTietTTDH(item.file_van_ban_bs.split(":")) : []
        let styleTemp = this.styles.textNoBold
        if (this.state.isBold) {
            styleTemp = this.styles.textBold
        }
        const isVBKhan1 = item.ma_cap_do_khan > 1 ? "red" : "#000000"
        const isVBKhan2 = item.ma_cap_do_khan > 1 ? "red" : "#808080"
        let borderRightWidth = (item.ma_xu_ly_den === this.props.cur_ma_xu_ly_den) ? 2.5 : 0.8
        let borderRightColor = (item.ma_xu_ly_den === this.props.cur_ma_xu_ly_den) ? "red" : "lightgray"

        if (Platform.OS === "ios" && Platform.isPad) {
            return (
                <CardItem style={[this.styles.CardItem, { borderRightWidth, borderRightColor }]} key={item.ma_van_ban_kc}>
                    <TouchableOpacity style={{ justifyContent: 'center', flex: 1, left: 0 }} onPress={this.onPressItem}>
                        <Text numberOfLines={3} ellipsizeMode="tail" style={[styleTemp, { color: isVBKhan1 }]}>{RemoveNoiDungTTDH(this.props.item.trich_yeu)} </Text>
                        <Text numberOfLines={1} ellipsizeMode="tail" style={[this.styles.txtNoiDung, { color: isVBKhan2 }]}>Số: {this.props.item.so_ky_hieu} </Text>
                        <Text numberOfLines={2} ellipsizeMode="tail" style={[this.styles.txtNoiDung, { color: isVBKhan2 }]}>CQBH: {item.ten_co_quan_ban_hanh} </Text>
                        <Text numberOfLines={2} ellipsizeMode="tail" style={[this.styles.txtNoiDung, { color: isVBKhan2 }]}>Ngày: {ConvertDateTimeDetail(item.ngay_nhan)} </Text>
                    </TouchableOpacity>
                </CardItem>
            )
        } else {
            return (
                <CardItem style={this.styles.CardItem} key={item.ma_van_ban_kc}>
                    <TouchableOpacity style={{ justifyContent: 'center', flex: 1, left: 0, marginRight: 100 }} onPress={this.onPressItem}>
                        <Text numberOfLines={3} ellipsizeMode="tail" style={[styleTemp, { color: isVBKhan1 }]}>{RemoveNoiDungTTDH(this.props.item.trich_yeu)} </Text>
                        <Text numberOfLines={1} ellipsizeMode="tail" style={[this.styles.txtNoiDung, { color: isVBKhan2 }]}>Số: {this.props.item.so_ky_hieu} </Text>
                        <Text numberOfLines={2} ellipsizeMode="tail" style={[this.styles.txtNoiDung, { color: isVBKhan2 }]}>CQBH: {item.ten_co_quan_ban_hanh} </Text>
                    </TouchableOpacity>
                    <View style={{ width: 100, top: 0, right: 0, bottom: 0, position: "absolute", alignItems: 'center', justifyContent: "center" }}>
                        <Text style={this.styles.txtNgayNhan}>{ConvertDateTime(item.ngay_nhan)} </Text>
                    </View>
                </CardItem>
            )
        }
    }
}

class VBDenXuLyScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            loading: true,
            refreshing: false,
            dataSource: [],
            ma_xu_ly_den: 0,
            ma_van_ban_den_kc: 0,
        }
        this.page = 1
        this.activeTab = 0
        this.nowYear = nowDate.getFullYear()
        this.stopLoadMore = false
        this.responseLength = 20
    }

    componentDidMount() {
        this.didBlurSubscription = this.props.navigation.addListener('willFocus', payload => { CheckSession(this.props.navigation) })
        this.activeTab = this.props.navigation.getParam("activeTab", 0)
        this.getListByTabIndex()
        API.Login.AU_DSNVCB(global.ma_ctcb_kc, this.props.dispatch)
    }

    componentWillUnmount = () => {
        this.didBlurSubscription.remove()
    };

    shouldComponentUpdate = (nextProps, nextState) => {
        if (this.state.loading !== nextState.loading ||
            this.state.refreshing !== nextState.refreshing ||
            this.state.dataSource !== nextState.dataSource ||
            this.state.ma_xu_ly_den !== nextState.ma_xu_ly_den ||
            this.state.ma_van_ban_den_kc !== nextState.ma_van_ban_den_kc ||
            this.props.cb_dsnvcb !== nextProps.cb_dsnvcb) {
            return true
        }
        return false
    };

    getListByTabIndex = () => {
        let tabIndex = this.activeTab
        let page = this.page
        let nam = this.nowYear
        let keyWord = this.searchHeader.getKeyWord()
        let size = AppConfig.pageSizeVBDENChuaXuLyCV
        let ma_don_vi_quan_tri = global.ma_don_vi_quan_tri
        let trang_thai_xem = -1 //Tất cả (đã xem, chưa xem)
        let trang_thai_xu_ly = tabIndex + 1
        let ma_ctcb_nhan = global.ma_ctcb_kc

        if (keyWord !== "") {
            let data1 = []
            let data2 = []
            nam = 0
            API.VanBanDen.AU_VBDEN_DSVBDTTTCCV(keyWord, "", page, size, ma_don_vi_quan_tri, trang_thai_xem, trang_thai_xu_ly, ma_ctcb_nhan, nam).then(response1 => {
                data1 = response1
                API.VanBanDen.AU_VBDEN_DSVBDTTTCCV("", keyWord, page, size, ma_don_vi_quan_tri, trang_thai_xem, trang_thai_xu_ly, ma_ctcb_nhan, nam).then(response2 => {
                    data2 = response2
                    data1 = data2.filter(bb => !data1.find(aa => bb.ma_van_ban_kc === aa.ma_van_ban_kc)).concat(data1);
                    if (tabIndex === this.activeTab) {
                        let dataSource = this.state.dataSource.filter(bb => !data1.find(aa => bb.ma_van_ban_kc === aa.ma_van_ban_kc)).concat(data1);
                        this.setState({ dataSource: dataSource }, () => {
                            this.setState({ loading: false, refreshing: false })
                        })
                        this.responseLength = data1.length
                        this.stopLoadMore = false
                    }
                })
            })
        } else {
            if (tabIndex === 0) {
                if (global.vbde_gop_chung_vb_dien_tu == 1) {
                    API.VanBanDen.AU_VBDEN_DSVBCLVT("", "", page, size, nam).then(response => {
                        if (tabIndex === this.activeTab) {
                            this.setState({ dataSource: [...this.state.dataSource, ...response] }, () => {
                                this.setState({ loading: false, refreshing: false })
                            })
                            this.responseLength = response.length
                            this.stopLoadMore = false
                            if (this.responseLength === 0) {
                                this.loadMore()
                            }
                        }
                    })
                } else {
                    API.VanBanDen.AU_VBDEN_DSVBDTCVT("", "", page, size, nam).then(response => {
                        if (tabIndex === this.activeTab) {
                            this.setState({ dataSource: [...this.state.dataSource, ...response] }, () => {
                                this.setState({ loading: false, refreshing: false })
                            })
                            this.responseLength = response.length
                            this.stopLoadMore = false
                            if (this.responseLength === 0) {
                                this.loadMore()
                            }
                        }
                    })
                }

            } else if (tabIndex === 1) {
                API.VanBanDen.AU_VBDEN_DSVBDML("", "", page, size, nam).then(response => {
                    if (tabIndex === this.activeTab) {
                        this.setState({ dataSource: [...this.state.dataSource, ...response] }, () => {
                            this.setState({ loading: false, refreshing: false })
                        })
                        this.responseLength = response.length
                        this.stopLoadMore = false
                        if (this.responseLength === 0) {
                            this.loadMore()
                        }
                    }
                })
            } else if (tabIndex === 2) {
                API.VanBanDen.AU_VBDEN_DSVBLDTLCVT("", "", page, size, nam).then(response => {
                    if (tabIndex === this.activeTab) {
                        this.setState({ dataSource: [...this.state.dataSource, ...response] }, () => {
                            this.setState({ loading: false, refreshing: false })
                        })
                        this.responseLength = response.length
                        this.stopLoadMore = false
                        if (this.responseLength === 0) {
                            this.loadMore()
                        }
                    }
                })
            } else if (tabIndex === 3) {
                API.VanBanDen.AU_VBDEN_DSVBDCDCVT("", "", page, size, nam).then(response => {
                    if (tabIndex === this.activeTab) {
                        this.setState({ dataSource: [...this.state.dataSource, ...response] }, () => {
                            this.setState({ loading: false, refreshing: false })
                        })
                        this.responseLength = response.length
                        this.stopLoadMore = false
                        if (this.responseLength === 0) {
                            this.loadMore()
                        }
                    }
                })
            } else if (tabIndex === 4) {
                API.VanBanDen.AU_VBDEN_DSVBDCVTC("", "", page, size, nam).then(response => {
                    if (tabIndex === this.activeTab) {
                        this.setState({ dataSource: [...this.state.dataSource, ...response] }, () => {
                            this.setState({ loading: false, refreshing: false })
                        })
                        this.responseLength = response.length
                        this.stopLoadMore = false
                        if (this.responseLength === 0) {
                            this.loadMore()
                        }
                    }
                })
            } else if (tabIndex === 5) {
                API.VanBanDen.AU_VBDEN_DSVBDVTDC("", "", page, size, nam).then(response => {
                    if (tabIndex === this.activeTab) {
                        this.setState({ dataSource: [...this.state.dataSource, ...response] }, () => {
                            this.setState({ loading: false, refreshing: false })
                        })
                        this.responseLength = response.length
                        this.stopLoadMore = false
                        if (this.responseLength === 0) {
                            this.loadMore()
                        }
                    }
                })
            } else {
                API.VanBanDen.AU_VBDEN_DSVBDHCVT("", "", page, size, nam).then(response => {
                    if (tabIndex === this.activeTab) {
                        this.setState({ dataSource: [...this.state.dataSource, ...response] }, () => {
                            this.setState({ loading: false, refreshing: false })
                        })
                        this.responseLength = response.length
                        this.stopLoadMore = false
                        if (this.responseLength === 0) {
                            this.loadMore()
                        }
                    }
                })
            }

        }
    }

    onChangeTab = (index) => {
        if (!this.state.loading) {
            this.page = 1
            this.activeTab = index
            this.nowYear = nowDate.getFullYear()
            this.stopLoadMore = false
            this.searchHeader.setKeyWord("", () => {
                this.setState({ dataSource: [], loading: true, ma_xu_ly_den: 0 })
                this.getListByTabIndex()
            })
        }
    }

    onRefresh = () => {
        if (!this.state.loading) {
            this.page = 1
            this.nowYear = nowDate.getFullYear()
            this.stopLoadMore = false
            this.setState({ dataSource: [], refreshing: true, loading: true, ma_xu_ly_den: 0 })
            this.getListByTabIndex()
        }
    }

    loadMore = () => {
        if (!this.state.loading && this.stopLoadMore === false) {
            if (this.nowYear > 2000) {
                this.stopLoadMore = true
                this.page = this.page + 1
                if (this.responseLength === 0) {
                    this.page = 1
                    this.nowYear = this.nowYear - 1
                }
                this.getListByTabIndex()
            }
        }
    }

    onSearchData = () => {
        if (!this.state.loading) {
            this.nowYear = nowDate.getFullYear()
            this.page = 1
            this.stopLoadMore = false
            this.setState({ dataSource: [], loading: true, ma_xu_ly_den: 0 })
            this.getListByTabIndex()
        }
    }

    onSuccess = (ma_xu_ly_den, flagNavigate) => {
        Alert.alert("Xác nhận", "Hoàn thành văn bản này?",
            [
                { text: "Không", style: "cancel" },
                {
                    text: "Đồng ý", onPress: () => {
                        let ma_ctcb = global.ma_ctcb_kc
                        API.VanBanDen.AU_VBDEN_CVXVBD(ma_xu_ly_den, ma_ctcb).then((result) => {
                            API.VanBanDen.AU_VBDEN_CVXLNVBD(ma_xu_ly_den, ma_ctcb).then((response) => {
                                if (response) {
                                    let dataSource = this.state.dataSource.filter(item => item.ma_xu_ly_den !== ma_xu_ly_den)
                                    ToastSuccess("Hoàn tất văn bản thành công!", () => {
                                        this.setState({ dataSource, ma_xu_ly_den: 0 })
                                        API.Login.AU_DSNVCB(global.ma_ctcb_kc, this.props.dispatch)
                                        if (flagNavigate) {
                                            this.props.navigation.navigate("VBDenXuLy")
                                        }
                                    })
                                } else {
                                    Toast.show({ text: "Xảy ra lỗi. Vui lòng thực hiện lại sau!", type: "danger", buttonText: "Đóng", position: "top", duration: 2000 })
                                }
                            })
                        })
                    }
                }
            ],
            { cancelable: false }
        )
    }

    onInProcess = (ma_xu_ly_den, flagNavigate) => {
        Alert.alert("Xác nhận", "Chuyển đang xử lý?",
            [
                { text: "Không", style: "cancel" },
                {
                    text: "Đồng ý", onPress: () => {
                        let ma_ctcb = global.ma_ctcb_kc
                        let noi_dung_xu_ly = ""
                        let trang_thai_xu_ly = 2 //Đang XL
                        API.VanBanDen.AU_VBDEN_CVXLVBD(ma_ctcb, ma_xu_ly_den, noi_dung_xu_ly, trang_thai_xu_ly).then((response) => {
                            if (response) {
                                let dataSource = this.state.dataSource.filter(item => item.ma_xu_ly_den !== ma_xu_ly_den)
                                ToastSuccess("Chuyển đang xử lý thành công!", () => {
                                    this.setState({ dataSource, ma_xu_ly_den: 0 })
                                    API.Login.AU_DSNVCB(global.ma_ctcb_kc, this.props.dispatch)
                                    if (flagNavigate) {
                                        this.props.navigation.navigate("VBDenXuLy")
                                    }
                                })
                            } else {
                                Toast.show({ text: "Xảy ra lỗi. Vui lòng thực hiện lại sau!", type: "danger", buttonText: "Đóng", position: "top", duration: 2000 })
                            }
                        })
                    }
                }
            ],
            { cancelable: false }
        )
    }

    renderVBDEN_Detail_iPad = () => {
        let { ma_xu_ly_den, ma_van_ban_den_kc } = this.state
        if (ma_xu_ly_den !== 0) {
            return (
                <VBDen_Details_iPad
                    navigation={this.props.navigation}
                    isDuyet={false}
                    ma_xu_ly_den={ma_xu_ly_den}
                    ma_van_ban_den_kc={ma_van_ban_den_kc}
                    activeTab={this.activeTab}
                    onSuccess={this.onSuccess}
                    onInProcess={this.onInProcess}
                />
            )
        } else {
            return null
        }
    }

    render() {

        this.styles = StyleSheet.create({
            CardItem: {
                flex: 1,
                borderTopWidth: 0,
                borderBottomWidth: 0.55,
                borderLeftWidth: 0,
                borderRightWidth: 0,
                borderBottomColor: "lightgray",
                paddingLeft: 8,
                paddingTop: 4,
                paddingBottom: 4,
                alignItems: 'center',
                justifyContent: "center",
                marginBottom: 1,
            },
            colTieuDe: {
                width: "70%",
                justifyContent: "center"
            },
            textBold: {
                color: "#000000",
                fontSize: this.props.fontSize.FontSizeNorman,
                fontWeight: "bold",
                paddingBottom: AppConfig.defaultPadding
            },
            textNoBold: {
                color: "#000000",
                fontSize: this.props.fontSize.FontSizeNorman,
                paddingBottom: AppConfig.defaultPadding
            },
            txtNgayNhan: {
                width: 100,
                color: "#808080",
                fontSize: this.props.fontSize.FontSizeSmall,
                textAlign: "center",
                paddingBottom: AppConfig.defaultPadding
            },
            txtNoiDung: {
                color: "#808080",
                fontSize: this.props.fontSize.FontSizeSmall,
                paddingBottom: AppConfig.defaultPadding
            },
            colNgayGui: {
                width: "30%",
                justifyContent: "center",
                alignItems: "flex-end"
            },
            menuTrigger: {
                width: 40,
                height: 30,
                justifyContent: "center",
                alignItems: 'center'
            },
            menuTriggerIcon: {
                fontSize: this.props.fontSize.FontSizeXXXLarge,
                color: "#B0BEC5"
            },
            menuOptionsContainer: {
                width: 200
            },
            menuOptions: {
                borderBottomColor: AppConfig.defaultLineColor,
                borderBottomWidth: 0.5
            },
            menuOptionsText: {
                color: AppConfig.headerBackgroundColor,
                fontSize: this.props.fontSize.FontSizeNorman,
                paddingTop: AppConfig.defaultPadding,
                paddingBottom: AppConfig.defaultPadding
            },
            menuOptionsTextDelete: {
                color: "red",
                fontSize: this.props.fontSize.FontSizeNorman,
                paddingTop: AppConfig.defaultPadding,
                paddingBottom: AppConfig.defaultPadding
            }
        })

        let nv_sovbden = 0
        try {
            let cb_dsnvcb = this.props.cb_dsnvcb
            if (checkVersionWeb(21)) {
                nv_sovbden = getNVNew("vbde", "van-ban-den/so-van-ban-den?page=1&t=so_vb_den_dien_tu_cua_vt", cb_dsnvcb)
            } else {
                nv_sovbden = 0
            }
        } catch (error) {
            console.log("nv_vbden_xuly error")
        }

        return (
            <Container style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
                <HeaderWithLeft
                    title="Sổ văn bản đến"
                    buttonName="menu"
                    onPressLeftButton={_ => { this.props.navigation.dispatch(DrawerActions.toggleDrawer()) }}
                />
                <View style={{ flex: 1 }}>
                    <MyTab
                        values={["VB điện tử", "VB lưu tạm", "VB LĐ trả lại", "VB chờ LĐ duyệt", "VB chờ chuyển", "VB đã chuyển", "VB đã hủy"]}
                        badges={[nv_sovbden]}
                        activeTab={this.activeTab}
                        onChangeTab={(index) => { this.onChangeTab(index) }}
                        fontSize={this.props.fontSize}
                    />
                    <SearchHeader
                        ref={(ref) => { this.searchHeader = ref }}
                        onSearchData={this.onSearchData}
                        fontSize={this.props.fontSize}
                    />
                    {
                        (this.state.loading) ? (
                            <View><Spinner color={AppConfig.blueBackground} /></View>
                        ) : (
                                <FlatList
                                    ref={(ref) => { this.flatListRef = ref }}
                                    data={this.state.dataSource}
                                    keyExtractor={(item, index) => "key" + item.ma_van_ban_kc + index}
                                    contentContainerStyle={{ paddingBottom: 0 }}
                                    renderItem={({ item, index }) => {
                                        return (
                                            <CV_XLTab_Item
                                                activeTab={this.activeTab}
                                                item={item}
                                                ma_ctcb_kc={global.ma_ctcb_kc}
                                                navigation={this.props.navigation}
                                                onSuccess={this.onSuccess}
                                                onInProcess={this.onInProcess}
                                                cur_ma_xu_ly_den={this.state.ma_xu_ly_den}
                                                index={index}
                                                dataSource={this.state.dataSource}
                                                fontSize={this.props.fontSize}
                                                styles={this.styles}
                                            />
                                        )
                                    }}
                                    onRefresh={this.onRefresh}
                                    refreshing={this.state.refreshing}
                                    onEndReached={this.loadMore}
                                    onEndReachedThreshold={5}
                                    removeClippedSubviews={true}
                                />
                            )
                    }
                </View>

                <FooterTab tabActive="vbden" navigation={this.props.navigation} />
            </Container>
        )

        // if (Platform.OS === "ios" && Platform.isPad) {
        //     return (
        //         <Container style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
        //             <HeaderWithLeft
        //                 title="Sổ văn bản đến"
        //                 buttonName="menu"
        //                 onPressLeftButton={_ => { this.props.navigation.dispatch(DrawerActions.toggleDrawer()) }}
        //             />
        //             <View style={{ flex: 1 }}>
        //                 <MyTab
        //                     values={["Chưa xử lý", "Đang xử lý", "Đã xử lý"]}
        //                     badges={[nv_chuaxuly, nv_dangxuly, 0]}
        //                     activeTab={this.activeTab}
        //                     onChangeTab={(index) => { this.onChangeTab(index) }}
        //                     fontSize={this.props.fontSize}
        //                 />
        //                 <SearchHeader
        //                     ref={(ref) => { this.searchHeader = ref }}
        //                     onSearchData={this.onSearchData}
        //                     fontSize={this.props.fontSize}
        //                 />
        //                 {
        //                     (this.state.loading) ? (
        //                         <View><Spinner color={AppConfig.blueBackground} /></View>
        //                     ) : (
        //                             <CardItem style={{ flex: 1, flexDirection: "row", paddingLeft: 5, paddingRight: 5 }}>
        //                                 <View style={{ width: 200 }}>
        //                                     <FlatList
        //                                         ref={(ref) => { this.flatListRef = ref }}
        //                                         data={this.state.dataSource}
        //                                         keyExtractor={(item, index) => "key" + item.ma_van_ban_kc + index}
        //                                         contentContainerStyle={{ paddingBottom: 0 }}
        //                                         renderItem={({ item }) => {
        //                                             return (
        //                                                 <CV_XLTab_Item
        //                                                     activeTab={this.activeTab}
        //                                                     item={item}
        //                                                     ma_ctcb_kc={global.ma_ctcb_kc}
        //                                                     navigation={this.props.navigation}
        //                                                     onSuccess={this.onSuccess}
        //                                                     onInProcess={this.onInProcess}
        //                                                     cur_ma_xu_ly_den={this.state.ma_xu_ly_den}
        //                                                     onPress={() => {
        //                                                         this.setState({
        //                                                             ma_xu_ly_den: item.ma_xu_ly_den,
        //                                                             ma_van_ban_den_kc: item.ma_van_ban_den_kc
        //                                                         })
        //                                                     }}
        //                                                     fontSize={this.props.fontSize}
        //                                                     styles={this.styles}
        //                                                 />
        //                                             )
        //                                         }}
        //                                         onRefresh={this.onRefresh}
        //                                         refreshing={this.state.refreshing}
        //                                         onEndReached={this.loadMore}
        //                                         onEndReachedThreshold={5}
        //                                         removeClippedSubviews={true}
        //                                     />
        //                                 </View>
        //                                 <View style={{ flex: 1 }}>
        //                                     {this.renderVBDEN_Detail_iPad()}
        //                                 </View>
        //                             </CardItem>
        //                         )
        //                 }
        //             </View>

        //             <FooterTab tabActive="vbden" navigation={this.props.navigation} />
        //         </Container>
        //     )
        // } else {

        // }
    }
}

function mapStateToProps(state) {
    return {
        cb_dsnvcb: state.cb_dsnvcb.data,
        fontSize: state.fontSize,
    }
}

export default connect(mapStateToProps)(VBDenXuLyScreen)