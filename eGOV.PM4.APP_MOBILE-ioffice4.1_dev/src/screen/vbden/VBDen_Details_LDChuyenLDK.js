import React, { Component } from 'react';
import { ScrollView, StyleSheet, FlatList, Alert, PixelRatio } from "react-native"
import { Container, Text, Card, CardItem, Toast, Textarea, CheckBox } from "native-base"
import { connect } from "react-redux"
import { StackActions } from "react-navigation"
import { AppConfig } from "../../AppConfig"
import HeaderWithLeftRight from "../../components/Header/HeaderWithLeftRight"
import SearchList from "../../components/SearchList"
import API from "../../networks"
import * as Models from "../../Models"
import { resetStack, ToastSuccess } from "../../untils/TextUntils"

class ListLandDaoKhacItem extends Component {
    shouldComponentUpdate(nextProps, nextState) {
        if (this.props.checked !== nextProps.checked) {
            return true
        }
        return false
    }

    render() {
        this.styles = this.props.styles
        let { ma, ten, ten_chuc_vu, ten_don_vi } = this.props.item
        return (
            <CardItem style={{ alignItems: 'center', paddingTop: 4, paddingBottom: 4, borderTopWidth: AppConfig.defaultLineWidth, borderTopColor: AppConfig.defaultLineColor, }} key={ma}>
                <CardItem style={{ width: 90, justifyContent: "center", alignItems: "center" }}>
                    <CheckBox onPress={() => this.props.checkExistOne(ma)} checked={this.props.checked} color={AppConfig.blueBackground} style={{ marginRight: 20 }}></CheckBox>
                </CardItem>
                <Card noShadow style={{ flex: 1, borderTopWidth: 0, borderBottomWidth: 0, borderLeftWidth: 0, borderRightWidth: 0, }}>
                    <CardItem>
                        <Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>{ten}</Text>
                    </CardItem>
                    <CardItem>
                        <Text style={[this.styles.TextTitle, { color: "#808080" }]}>Chức vụ: {ten_chuc_vu}</Text>
                    </CardItem>
                    <CardItem>
                        <Text style={[this.styles.TextTitle, { color: "#808080" }]}>Đơn vị: {ten_don_vi}</Text>
                    </CardItem>
                </Card>
            </CardItem>
        )
    }
}

class VBDen_Details_LDChuyenLDK extends Component {
    constructor(props) {
        super(props)
        this.state = {
            listLDK: [],
            checkedLD: null,
            yKienXuLy: "",
            checkSms: false
        }
        this.npVBDen_Details_LDChuyenLDK = new Models.npVBDen_Details_LDChuyenLDK()
        this.listSearchDefault = []
    }

    componentDidMount = () => {
        this.npVBDen_Details_LDChuyenLDK = this.props.navigation.getParam("inVBDen_Details_LDChuyenLDK")
        this.getListLanhDaoKhac()
    }

    getListLanhDaoKhac = () => {
        API.VanBanDen.AU_VBDEN_DSLDDCD(global.ma_don_vi, global.ma_ctcb_kc).then((listLDK) => {
            if (listLDK.length > 0) {
                listLDK = listLDK.filter(el => el.ma !== global.ma_ctcb_kc)
            }
            this.listSearchDefault = listLDK
            this.setState({ listLDK })
        })
    }

    onCheckSms = () => {
        this.setState({ checkSms: !this.state.checkSms })
    }

    onPressSend = () => {
        let { yKienXuLy, checkSms, checkedLD } = this.state
        let listLDK = this.listSearchDefault

        if (checkedLD === null) {
            Toast.show({ text: "Vui lòng chọn lãnh đạo!", type: "warning", buttonText: "Đóng", position: "top", duration: 2000 })
            return null
        }

        let ma_van_ban_den = this.npVBDen_Details_LDChuyenLDK.ma_van_ban_den
        let ma_ctcb_gui = global.ma_ctcb_kc
        let nguoiNhan = listLDK.filter(el => el.ma === checkedLD)[0]
        let noi_dung_chuyen = yKienXuLy
        let ma_xu_ly_den = this.npVBDen_Details_LDChuyenLDK.ma_xu_ly_den
        let sms = checkSms ? 1 : 0

        Alert.alert(
            "Xác nhận",
            "Bạn muốn chuyển lãnh đạo " + nguoiNhan.ten + " duyệt?",
            [
                { text: "Không", onPress: () => console.log("Logout"), style: "cancel" },
                {
                    text: "Có", onPress: () => {
                        API.VanBanDen.AU_VBDEN_CLDKD(ma_xu_ly_den, ma_van_ban_den, ma_ctcb_gui, nguoiNhan.ma, noi_dung_chuyen, sms, 0).then((response) => {
                            if (response !== "") {
                                API.Login.AU_DSNVCB(global.ma_ctcb_kc, this.props.dispatch)
                                if (sms === 1) {
                                    let noi_dung = this.npVBDen_Details_LDChuyenLDK.trich_yeu
                                    let chuoi_di_dong_nhan = listLDK.filter(el => el.ma === checkedLD)[0].di_dong_can_bo
                                    API.NhanTinSMS.AU_SMS_GTN(noi_dung, chuoi_di_dong_nhan)
                                }

                                API.Login.AU_PUSH_FCM_APP(nguoiNhan.ma, "vanbandenchoduyet", this.npVBDen_Details_LDChuyenLDK.trich_yeu, 0, 0, 0, response)

                                ToastSuccess("Chuyển lãnh đạo khác thành công", () => { this.props.navigation.dispatch(resetStack("VBDenDuyet")) })
                            } else {
                                Toast.show({ text: "Lỗi. Vui lòng thử lại sau!", type: "danger", buttonText: "Đóng", position: "top", duration: 2000 })
                            }
                        })
                    }
                }
            ],
            { cancelable: false }
        )
    }

    checkExistOne = (ma) => {
        if (ma === this.state.checkedLD) {
            this.setState({ checkedLD: null })
        } else {
            this.setState({ checkedLD: ma })
        }
    }

    onSearchList = () => {
        if (this.listSearchDefault.length > 0) {
            let keyWord = this.searchList.getKeyWord()
            let arrResult = this.listSearchDefault.filter(el => el.ten.toLowerCase().indexOf(keyWord.toLowerCase()) !== -1)
            this.setState({ listLDK: arrResult }, () => this.searchList.setKeyWord(keyWord))
        }
    }

    render() {
        this.styles = StyleSheet.create({
            scrollContainer: {
                flex: 1,
                marginLeft: 10,
                marginRight: 10
            },
            CardItem: {
                paddingTop: 10,
                paddingBottom: 10,
                alignItems: 'center'
            },
            CardItemLeft: {
                width: 90
            },
            CardItemRight: {
                flex: 1
            },
            TextTitle: {
                fontSize: this.props.fontSize.FontSizeSmall,
            },
            TextInput: {
                flex: 1,
                fontFamily: AppConfig.fontFamily,
                fontSize: this.props.fontSize.FontSizeNorman / PixelRatio.getFontScale(),
                color: "#000000",
                borderColor: AppConfig.defaultLineColor,
                borderWidth: AppConfig.defaultLineWidth,
                borderRadius: 5,
                marginRight: 2,
                paddingLeft: 5,
                paddingTop: 7,
                minHeight: 40,
            },
        })

        const popAction = StackActions.pop({ n: 1 })
        return (
            <Container style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
                <HeaderWithLeftRight
                    title="Chuyển lãnh đạo khác"
                    buttonLeftName="arrow-back"
                    buttonRightName="done"
                    onPressLeftButton={() => { this.props.navigation.dispatch(popAction) }}
                    onPressRightButton={this.onPressSend}
                />
                <ScrollView style={this.styles.scrollContainer}>
                    <CardItem style={this.styles.CardItem}>
                        <CardItem style={[this.styles.CardItemLeft]}>
                            <Text style={[this.styles.TextTitle]}>Ý kiến chuyển</Text>
                        </CardItem>
                        <CardItem style={[this.styles.CardItemRight]}>
                            <Textarea
                                autoCorrect={false}
                                style={this.styles.TextInput}
                                rowSpan={3}
                                value={this.state.yKienXuLy}
                                onChangeText={(text) => { this.setState({ yKienXuLy: text }) }}
                            />
                        </CardItem>
                    </CardItem>
                    <CardItem style={this.styles.CardItem}>
                        <CardItem style={[this.styles.CardItemLeft]}></CardItem>
                        <CardItem style={[this.styles.CardItemRight, { marginLeft: -10 }]}>
                            <CheckBox onPress={this.onCheckSms} checked={this.state.checkSms} color={AppConfig.blueBackground}></CheckBox>
                            <Text style={[this.styles.TextTitle, { marginLeft: 20 }]}>Sms</Text>
                        </CardItem>
                    </CardItem>
                    <CardItem style={this.styles.CardItem}>
                        <Text style={[this.styles.TextTitle, { fontSize: this.props.fontSize.FontSizeLarge, }]}>Danh sách lãnh đạo:</Text>
                    </CardItem>
                    <CardItem>
                        <SearchList ref={(ref) => { this.searchList = ref }} onSearchList={this.onSearchList} fontSize={this.props.fontSize} />
                    </CardItem>
                    <FlatList
                        data={this.state.listLDK}
                        extraData={this.state}
                        keyExtractor={(item, index) => item.ma.toString()}
                        renderItem={({ item }) => {
                            let flag = item.ma === this.state.checkedLD ? true : false
                            return (
                                <ListLandDaoKhacItem
                                    item={item}
                                    checkExistOne={(ma) => this.checkExistOne(ma)}
                                    checked={flag}
                                    fontSize={this.props.fontSize}
                                    styles={this.styles}
                                />
                            )
                        }}
                        removeClippedSubviews={true}
                    />
                </ScrollView>
            </Container >
        );
    }
}

function mapStateToProps(state) {
    return {
        fontSize: state.fontSize,
    }
}

export default connect(mapStateToProps)(VBDen_Details_LDChuyenLDK)