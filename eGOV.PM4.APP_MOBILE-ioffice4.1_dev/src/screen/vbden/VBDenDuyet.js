import React, { Component, PureComponent } from 'react';
import { View, StyleSheet, FlatList, TouchableOpacity, Platform } from "react-native"
import { Container, Text, CardItem, Spinner } from "native-base"
import { DrawerActions } from "react-navigation"
import { connect } from "react-redux"
import API from "../../networks"
import HeaderWithLeft from "../../components/Header/HeaderWithLeft"
import FooterTab from "../../components/Footer/FooterTab"
import SearchHeader from "../../components/SearchHeader"
import MyTab from "../../components/Tab"
import { AppConfig } from "../../AppConfig"
import { RemoveNoiDungTTDH, ConvertDateTime, ConvertDateTimeDetail, checkVersionWeb, getNVOld, getNVNew } from "../../untils/TextUntils"
import { CheckSession } from "../../untils/NetInfo"
import VBDen_Details_iPad from "./VBDen_Details_iPad"

const nowDate = new Date()

class LD_DuyetTab_Item extends PureComponent {
    constructor(props) {
        super(props)
        this.state = {
            isBold: this.props.item.ngay_xem === null ? true : false
        }
    }

    onPressItem = () => {
        if (Platform.OS === "ios" && Platform.isPad) {
            this.props.onPress()
        } else {
            this.props.navigation.navigate("VBDen_Details", {
                isDuyet: true,
                ma_xu_ly_den: this.props.item.ma_xu_ly_den,
                ma_van_ban_den_kc: this.props.item.ma_van_ban_den_kc,
                isUpdateViewCV: this.state.isBold,
                updateViewCV: this.updateViewCV,
                activeTab: this.props.activeTab,
                index: this.props.index,
                dataSource: this.props.dataSource
            })
        }
    }

    updateViewCV = async () => {
        let chuoi_ma_xu_ly_den = this.props.item.ma_xu_ly_den
        let ma_ctcb = global.ma_ctcb_kc
        await API.VanBanDen.AU_VBDEN_CVXVBD(chuoi_ma_xu_ly_den, ma_ctcb).then((result) => {
            if (result) {
                this.setState({ isBold: false })
            }
        })
    }

    render() {
        this.styles = this.props.styles
        const { item } = this.props
        let styleTemp = this.styles.textNoBold
        if (this.state.isBold) {
            styleTemp = this.styles.textBold
        }
        const isVBKhan1 = item.ma_cap_do_khan > 1 ? "red" : "#000000"
        const isVBKhan2 = item.ma_cap_do_khan > 1 ? "red" : "#808080"
        let borderRightWidth = (item.ma_xu_ly_den === this.props.cur_ma_xu_ly_den) ? 2.5 : 0.8
        let borderRightColor = (item.ma_xu_ly_den === this.props.cur_ma_xu_ly_den) ? "red" : "lightgray"
        if (Platform.OS === "ios" && Platform.isPad) {
            return (
                <CardItem style={[this.styles.CardItem, { borderRightWidth, borderRightColor }]} key={item.ma_van_ban_kc}>
                    <TouchableOpacity style={{ justifyContent: 'center', flex: 1 }} onPress={this.onPressItem}>
                        <Text numberOfLines={3} ellipsizeMode="tail" style={[styleTemp, { color: isVBKhan1 }]}>{RemoveNoiDungTTDH(this.props.item.trich_yeu)}</Text>
                        <Text numberOfLines={2} ellipsizeMode="tail" style={[styleTemp, { color: isVBKhan1 }]}>Số: {this.props.item.so_ky_hieu}</Text>
                        <Text numberOfLines={3} ellipsizeMode="tail" style={[this.styles.txtNoiDung, { color: isVBKhan2 }]}>CQBH: {item.ten_co_quan_ban_hanh}</Text>
                        <Text numberOfLines={2} ellipsizeMode="tail" style={[this.styles.txtNoiDung, { color: isVBKhan2 }]}>Ngày: {ConvertDateTimeDetail(item.ngay_nhan)} </Text>
                    </TouchableOpacity>
                </CardItem>
            )
        } else {
            return (
                <CardItem style={this.styles.CardItem} key={item.ma_van_ban_kc}>
                    <TouchableOpacity style={{ justifyContent: 'center', flex: 1, left: 0, marginRight: 100 }} onPress={this.onPressItem}>
                        <Text numberOfLines={3} ellipsizeMode="tail" style={[styleTemp, { color: isVBKhan1 }]}>{RemoveNoiDungTTDH(this.props.item.trich_yeu)} </Text>
                        <Text numberOfLines={1} ellipsizeMode="tail" style={[styleTemp, { color: isVBKhan1 }]}>Số: {this.props.item.so_ky_hieu} </Text>
                        <Text numberOfLines={2} ellipsizeMode="tail" style={[this.styles.txtNoiDung, { color: isVBKhan2 }]}>CQBH: {item.ten_co_quan_ban_hanh} </Text>
                    </TouchableOpacity>
                    <View style={{ width: 100, top: 0, right: 0, bottom: 0, position: "absolute", alignItems: 'center', justifyContent: "center" }}>
                        <Text style={this.styles.txtNgayNhan}>{ConvertDateTime(item.ngay_nhan)} </Text>
                    </View>
                </CardItem>
            )
        }
    }
}

class VBDenDuyetScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            loading: true,
            refreshing: false,
            dataSource: [],
            ma_xu_ly_den: 0,
            ma_van_ban_den_kc: 0,
        }
        this.page = 1
        this.activeTab = 0
        this.nowYear = nowDate.getFullYear()
        this.stopLoadMore = false
        this.responseLength = 20
    }

    componentDidMount = () => {
        this.didBlurSubscription = this.props.navigation.addListener('willFocus', payload => { CheckSession(this.props.navigation) })
        this.activeTab = this.props.navigation.getParam("activeTab", 0)
        this.getListByTabIndex(this.activeTab)
        API.Login.AU_DSNVCB(global.ma_ctcb_kc, this.props.dispatch)
    }

    componentWillUnmount = () => {
        this.didBlurSubscription.remove()
    }

    shouldComponentUpdate = (nextProps, nextState) => {
        if (this.state.loading !== nextState.loading ||
            this.state.refreshing !== nextState.refreshing ||
            this.state.dataSource !== nextState.dataSource ||
            this.state.ma_xu_ly_den !== nextState.ma_xu_ly_den ||
            this.state.ma_van_ban_den_kc !== nextState.ma_van_ban_den_kc ||
            this.props.cb_dsnvcb !== nextProps.cb_dsnvcb) {
            return true
        }
        return false
    }

    getListByTabIndex = () => {
        let ma_ctcb_duyet = global.ma_ctcb_kc
        let ma_don_vi_quan_tri = global.ma_don_vi_quan_tri
        let tabIndex = this.activeTab
        let page = this.page
        let nam = this.nowYear
        let keyWord = this.searchHeader.getKeyWord()

        if (keyWord !== "") {
            nam = 0
            page = 1
            let size = 9999999
            if (checkVersionWeb(23)) {
                if (tabIndex === 0) {
                    API.VanBanDen.AU_VBDEN_DSVBDCDCLD("", keyWord, ma_ctcb_duyet, ma_don_vi_quan_tri, page, size, nam).then(response => {
                        if (tabIndex === this.activeTab) {
                            this.setState({ dataSource: [...this.state.dataSource, ...response] }, () => {
                                this.setState({ loading: false, refreshing: false })
                            })
                            this.responseLength = response.length
                            this.stopLoadMore = true
                        }
                    })
                } else if (tabIndex === 1) {
                    API.VanBanDen.AU_VBDEN_DSVBDUQDCLD("", keyWord, ma_ctcb_duyet, ma_don_vi_quan_tri, page, size, nam).then(response => {
                        if (tabIndex === this.activeTab) {
                            this.setState({ dataSource: [...this.state.dataSource, ...response] }, () => {
                                this.setState({ loading: false, refreshing: false })
                            })
                            this.responseLength = response.length
                            this.stopLoadMore = true
                        }
                    })
                } else {
                    API.VanBanDen.AU_VBDEN_DSVBDDCTHCLD("", keyWord, ma_ctcb_duyet, ma_don_vi_quan_tri, page, size, nam).then(response => {
                        if (tabIndex === this.activeTab) {
                            this.setState({ dataSource: [...this.state.dataSource, ...response] }, () => {
                                this.setState({ loading: false, refreshing: false })
                            })
                            this.responseLength = response.length
                            this.stopLoadMore = true
                        }
                    })
                }
            } else {
                if (tabIndex === 0) {
                    let data1 = []
                    let data2 = []
                    API.VanBanDen.AU_VBDEN_DSVBDCDCLD(keyWord, "", ma_ctcb_duyet, ma_don_vi_quan_tri, page, size, nam).then(response1 => {
                        data1 = response1
                        API.VanBanDen.AU_VBDEN_DSVBDCDCLD("", keyWord, ma_ctcb_duyet, ma_don_vi_quan_tri, page, size, nam).then(response2 => {
                            data2 = response2
                            data1 = data2.filter(bb => !data1.find(aa => bb.ma_van_ban_kc === aa.ma_van_ban_kc)).concat(data1);
                            if (tabIndex === this.activeTab) {
                                this.setState({ dataSource: [...this.state.dataSource, ...data1] }, () => {
                                    this.setState({ loading: false, refreshing: false })
                                })
                                this.responseLength = data1.length
                                this.stopLoadMore = true
                            }
                        })
                    })
                } else if (tabIndex === 1) {
                    let data1 = []
                    let data2 = []
                    API.VanBanDen.AU_VBDEN_DSVBDUQDCLD(keyWord, "", ma_ctcb_duyet, ma_don_vi_quan_tri, page, size, nam).then(response1 => {
                        data1 = response1
                        API.VanBanDen.AU_VBDEN_DSVBDUQDCLD("", keyWord, ma_ctcb_duyet, ma_don_vi_quan_tri, page, size, nam).then(response2 => {
                            data2 = response2
                            data1 = data2.filter(bb => !data1.find(aa => bb.ma_van_ban_kc === aa.ma_van_ban_kc)).concat(data1);
                            if (tabIndex === this.activeTab) {
                                this.setState({ dataSource: [...this.state.dataSource, ...data1] }, () => {
                                    this.setState({ loading: false, refreshing: false })
                                })
                                this.responseLength = data1.length
                                this.stopLoadMore = true
                            }
                        })
                    })
                } else {
                    let data1 = []
                    let data2 = []
                    API.VanBanDen.AU_VBDEN_DSVBDDCTHCLD(keyWord, "", ma_ctcb_duyet, ma_don_vi_quan_tri, page, size, nam).then(response1 => {
                        data1 = response1
                        API.VanBanDen.AU_VBDEN_DSVBDDCTHCLD("", keyWord, ma_ctcb_duyet, ma_don_vi_quan_tri, page, size, nam).then(response2 => {
                            data2 = response2
                            data1 = data2.filter(bb => !data1.find(aa => bb.ma_van_ban_kc === aa.ma_van_ban_kc)).concat(data1);
                            if (tabIndex === this.activeTab) {
                                this.setState({ dataSource: [...this.state.dataSource, ...data1] }, () => {
                                    this.setState({ loading: false, refreshing: false })
                                })
                                this.responseLength = data1.length
                                this.stopLoadMore = true
                            }
                        })
                    })
                }
            }
        } else {
            if (tabIndex === 0) {
                let size = AppConfig.pageSizeVBDENChoDuyetLD
                if (global.AU_ROOT.includes('nghean-api') && global.ht_chon_tgian_nhac_viec !== "all" && global.ht_chon_tgian_nhac_viec !== "hide") {
                    nam = 0
                    page = 1
                    size = 9999999
                }
                API.VanBanDen.AU_VBDEN_DSVBDCDCLD("", "", ma_ctcb_duyet, ma_don_vi_quan_tri, page, size, nam).then(response => {
                    if (tabIndex === this.activeTab) {
                        this.setState({ dataSource: [...this.state.dataSource, ...response] }, () => {
                            this.setState({ loading: false, refreshing: false })
                        })
                        this.responseLength = response.length
                        this.stopLoadMore = false
                        if (this.responseLength === 0) {
                            this.loadMore()
                        }
                    }
                })
                API.Login.AU_DSNVCB(global.ma_ctcb_kc, this.props.dispatch)
            
            } else if (tabIndex === 1) {
                let size = AppConfig.pageSizeVBDENUyQuyenLD
                if (global.AU_ROOT.includes('nghean-api') && global.ht_chon_tgian_nhac_viec !== "all" && global.ht_chon_tgian_nhac_viec !== "hide") {
                    nam = 0
                    page = 1
                    size = 9999999
                }
                API.VanBanDen.AU_VBDEN_DSVBDUQDCLD("", "", ma_ctcb_duyet, ma_don_vi_quan_tri, page, size, nam).then(response => {
                    if (tabIndex === this.activeTab) {
                        this.setState({ dataSource: [...this.state.dataSource, ...response] }, () => {
                            this.setState({ loading: false, refreshing: false })
                        })
                        this.responseLength = response.length
                        this.stopLoadMore = false
                        if (this.responseLength === 0) {
                            this.loadMore()
                        }
                    }
                })
            } else {
                let size = AppConfig.pageSizeVBDENDaDuyetLD
                if (global.AU_ROOT.includes('nghean-api') && global.ht_chon_tgian_nhac_viec !== "all" && global.ht_chon_tgian_nhac_viec !== "hide") {
                    nam = 0
                    page = 1
                    size = 9999999
                }
                API.VanBanDen.AU_VBDEN_DSVBDDCTHCLD("", "", ma_ctcb_duyet, ma_don_vi_quan_tri, page, size, nam).then(response => {
                    if (tabIndex === this.activeTab) {
                        this.setState({ dataSource: [...this.state.dataSource, ...response] }, () => {
                            this.setState({ loading: false, refreshing: false })
                        })
                        this.responseLength = response.length
                        this.stopLoadMore = false
                        if (this.responseLength === 0 && global.US_Default_Interval === "0") {
                            this.loadMore()
                        }
                    }
                })
            }
        }
    }

    onChangeTab = (index) => {
        if (!this.state.loading) {
            this.page = 1
            this.activeTab = index
            this.nowYear = nowDate.getFullYear()
            this.stopLoadMore = false
            this.searchHeader.setKeyWord("", () => {
                this.setState({ dataSource: [], loading: true, ma_xu_ly_den: 0 })
                this.getListByTabIndex()
            })
        }
    }

    onRefresh = () => {
        if (!this.state.loading) {
            this.page = 1
            this.nowYear = nowDate.getFullYear()
            this.stopLoadMore = false
            this.setState({ dataSource: [], refreshing: true, loading: true, ma_xu_ly_den: 0 })
            this.getListByTabIndex()
        }
    }

    loadMore = () => {
        if (global.AU_ROOT.includes('nghean-api') && global.ht_chon_tgian_nhac_viec !== "all" && global.ht_chon_tgian_nhac_viec !== "hide") {
            this.stopLoadMore = true
        }
        if (!this.state.loading && this.stopLoadMore === false) {
            if (global.khoang_thoi_gian_danh_sach_vb !== "0" && this.activeTab == 2 ) {
                if(this.responseLength !== 0){
                    this.stopLoadMore = true
                    this.page = this.page + 1
                    this.getListByTabIndex()
                }
                else{
                    this.stopLoadMore = true 
                }          
            } else {
                if (this.nowYear > 2000) {
                    this.stopLoadMore = true
                    this.page = this.page + 1
                    if (this.responseLength === 0) {
                        this.page = 1
                        this.nowYear = this.nowYear - 1
                    }
                    this.getListByTabIndex()
                }
            }
        }
    }

    onSearchData = () => {
        if (!this.state.loading) {
            this.nowYear = nowDate.getFullYear()
            this.page = 1
            this.stopLoadMore = false
            this.setState({ dataSource: [], loading: true, ma_xu_ly_den: 0 })
            this.getListByTabIndex()
        }
    }

    renderVBDEN_Detail_iPad = () => {
        let { ma_xu_ly_den, ma_van_ban_den_kc } = this.state
        if (ma_xu_ly_den !== 0) {
            return (
                <VBDen_Details_iPad
                    navigation={this.props.navigation}
                    isDuyet={true}
                    ma_xu_ly_den={ma_xu_ly_den}
                    ma_van_ban_den_kc={ma_van_ban_den_kc}
                    activeTab={this.activeTab}
                />
            )
        } else {
            return null
        }
    }

    render() {

        this.styles = StyleSheet.create({
            CardItem: {
                flex: 1,
                borderTopWidth: 0,
                borderBottomWidth: 0.55,
                borderLeftWidth: 0,
                borderRightWidth: 0,
                borderBottomColor: "lightgray",
                paddingLeft: 8,
                paddingTop: 4,
                paddingBottom: 4,
                alignItems: 'center',
                justifyContent: "center",
                marginBottom: 1,
            },
            colTieuDe: {
                width: "70%",
                justifyContent: "center"
            },
            textBold: {
                color: "#000000",
                fontSize: this.props.fontSize.FontSizeNorman,
                fontWeight: "bold",
                paddingBottom: AppConfig.defaultPadding
            },
            textNoBold: {
                color: "#000000",
                fontSize: this.props.fontSize.FontSizeNorman,
                paddingBottom: AppConfig.defaultPadding
            },
            txtNgayNhan: {
                width: 100,
                color: "#808080",
                fontSize: this.props.fontSize.FontSizeSmall,
                textAlign: "center",
                paddingBottom: AppConfig.defaultPadding
            },
            txtNoiDung: {
                color: "#808080",
                fontSize: this.props.fontSize.FontSizeSmall,
                paddingBottom: AppConfig.defaultPadding
            },
            colNgayGui: {
                width: "30%",
                justifyContent: "center",
                alignItems: "flex-end"
            },
            menuTrigger: {
                width: 40,
                height: 30,
                justifyContent: "center",
                alignItems: 'center'
            },
            menuTriggerIcon: {
                fontSize: this.props.fontSize.FontSizeXXXLarge,
                color: "#B0BEC5"
            },
            menuOptionsContainer: {
                width: 200
            },
            menuOptions: {
                borderBottomColor: AppConfig.defaultLineColor,
                borderBottomWidth: 0.5
            },
            menuOptionsText: {
                color: AppConfig.headerBackgroundColor,
                fontSize: this.props.fontSize.FontSizeNorman,
                paddingTop: AppConfig.defaultPadding,
                paddingBottom: AppConfig.defaultPadding
            },
            menuOptionsTextDelete: {
                color: "red",
                fontSize: this.props.fontSize.FontSizeNorman,
                paddingTop: AppConfig.defaultPadding,
                paddingBottom: AppConfig.defaultPadding
            }
        })

        let nv_choduyet = 0
        let nv_uyquyenduyet = 0
        try {
            let cb_dsnvcb = this.props.cb_dsnvcb
            if (checkVersionWeb(21)) {
                nv_choduyet = getNVNew("vbde", "van-ban-den/duyet-van-ban-den?page=1&t=vb_den_cho_duyet_cua_ld", cb_dsnvcb)
                nv_uyquyenduyet = getNVNew("vbde", "van-ban-den/duyet-van-ban-den?page=1&t=vb_den_uy_quyen_duyet_cua_ld", cb_dsnvcb)
            } else {
                nv_choduyet = getNVOld("duyet-van-ban-den?page?t", "vb_den_cho_duyet_cua_ld", cb_dsnvcb)
                nv_uyquyenduyet = getNVOld("duyet-van-ban-den?page?t", "vb_den_uy_quyen_duyet_cua_ld", cb_dsnvcb)
            }

        } catch (error) {
            console.log("nv_vbden_duyet error")
        }

        if (Platform.OS === "ios" && Platform.isPad) {
            return (
                <Container style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
                    <HeaderWithLeft title="Duyệt văn bản đến" buttonName="menu" onPressLeftButton={_ => { this.props.navigation.dispatch(DrawerActions.toggleDrawer()) }} />
                    <View style={{ flex: 1 }}>
                        <MyTab
                            values={["Chờ duyệt", "Ủy quyền", "Đã duyệt"]}
                            badges={[nv_choduyet, nv_uyquyenduyet, 0]}
                            activeTab={this.activeTab}
                            onChangeTab={(index) => { this.onChangeTab(index) }}
                            fontSize={this.props.fontSize}
                        />
                        <SearchHeader
                            ref={(ref) => { this.searchHeader = ref }}
                            onSearchData={this.onSearchData}
                            fontSize={this.props.fontSize}
                        />
                        {
                            (this.state.loading) ? (
                                <Spinner color={AppConfig.blueBackground} />
                            ) : (
                                <CardItem style={{ flex: 1, flexDirection: "row", paddingLeft: 5, paddingRight: 5 }}>
                                    <View style={{ width: 200 }}>
                                        <FlatList
                                            ref={(ref) => { this.flatListRef = ref }}
                                            data={this.state.dataSource}
                                            keyExtractor={(item, index) => "key" + item.ma_van_ban_kc + index}
                                            contentContainerStyle={{ paddingBottom: 0 }}
                                            renderItem={({ item }) => {
                                                return (
                                                    <LD_DuyetTab_Item
                                                        activeTab={this.activeTab}
                                                        item={item}
                                                        ma_ctcb_kc={global.ma_ctcb_kc}
                                                        navigation={this.props.navigation}
                                                        cur_ma_xu_ly_den={this.state.ma_xu_ly_den}
                                                        onPress={() => {
                                                            this.setState({
                                                                ma_xu_ly_den: item.ma_xu_ly_den,
                                                                ma_van_ban_den_kc: item.ma_van_ban_den_kc
                                                            })
                                                        }}
                                                        fontSize={this.props.fontSize}
                                                        styles={this.styles}
                                                    />
                                                )
                                            }}
                                            onRefresh={this.onRefresh}
                                            refreshing={this.state.refreshing}
                                            onEndReached={this.loadMore}
                                            onEndReachedThreshold={10}
                                            removeClippedSubviews={true}
                                        />
                                    </View>
                                    <View style={{ flex: 1 }}>
                                        {this.renderVBDEN_Detail_iPad()}
                                    </View>
                                </CardItem>
                            )
                        }
                    </View>
                    <FooterTab tabActive="vbden" navigation={this.props.navigation} />
                </Container>
            );
        } else {
            return (
                <Container style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
                    <HeaderWithLeft title="Duyệt văn bản đến" buttonName="menu" onPressLeftButton={_ => { this.props.navigation.dispatch(DrawerActions.toggleDrawer()) }} />
                    <View style={{ flex: 1 }}>
                        <MyTab
                            values={["Chờ duyệt", "Ủy quyền", "Đã duyệt"]}
                            badges={[nv_choduyet, nv_uyquyenduyet, 0]}
                            activeTab={this.activeTab}
                            onChangeTab={(index) => { this.onChangeTab(index) }}
                            fontSize={this.props.fontSize}
                        />
                        <SearchHeader
                            ref={(ref) => { this.searchHeader = ref }}
                            onSearchData={this.onSearchData}
                            fontSize={this.props.fontSize}
                        />
                        {
                            (this.state.loading) ? (
                                <Spinner color={AppConfig.blueBackground} />
                            ) : (
                                <FlatList
                                    ref={(ref) => { this.flatListRef = ref }}
                                    data={this.state.dataSource}
                                    keyExtractor={(item, index) => "key" + item.ma_van_ban_kc + index}
                                    contentContainerStyle={{ paddingBottom: 0 }}
                                    renderItem={({ item, index }) => {
                                        return (
                                            <LD_DuyetTab_Item
                                                activeTab={this.activeTab}
                                                item={item}
                                                ma_ctcb_kc={global.ma_ctcb_kc}
                                                navigation={this.props.navigation}
                                                cur_ma_xu_ly_den={this.state.ma_xu_ly_den}
                                                index={index}
                                                dataSource={this.state.dataSource}
                                                fontSize={this.props.fontSize}
                                                styles={this.styles}
                                            />
                                        )
                                    }}
                                    onRefresh={this.onRefresh}
                                    refreshing={this.state.refreshing}
                                    onEndReached={this.loadMore}
                                    onEndReachedThreshold={5}
                                    removeClippedSubviews={true}
                                />
                            )
                        }
                    </View>
                    <FooterTab tabActive="vbden" navigation={this.props.navigation} />
                </Container>
            );
        }
    }
}

function mapStateToProps(state) {
    return {
        cb_dsnvcb: state.cb_dsnvcb.data,
        fontSize: state.fontSize,
    }
}


export default connect(mapStateToProps)(VBDenDuyetScreen)