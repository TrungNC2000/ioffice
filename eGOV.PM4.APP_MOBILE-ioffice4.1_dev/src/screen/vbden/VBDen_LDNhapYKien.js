import React, { Component } from 'react';
import { ScrollView, StyleSheet, Alert, PixelRatio } from "react-native"
import { Container, Text, CardItem, Textarea, CheckBox, Toast } from "native-base"
import { connect } from "react-redux"
import API from "../../networks"
import { AppConfig } from "../../AppConfig"
import { StackActions } from "react-navigation"
import HeaderWithLeftRight from "../../components/Header/HeaderWithLeftRight"
import { resetStack, ToastSuccess } from "../../untils/TextUntils"

class VBDen_LDNhapYKien extends Component {
    constructor(props) {
        super(props)
        this.state = {
            yKienXuLy: "",
        }
        this.ma_xu_ly_den = 0
        this.ma_van_ban_den = 0
    }

    componentDidMount = () => {
        this.ma_xu_ly_den = this.props.navigation.getParam("ma_xu_ly_den")
        this.ma_van_ban_den = this.props.navigation.getParam("ma_van_ban_den")
    }

    onPressSend = () => {
        let ma_xu_ly_den = this.ma_xu_ly_den
        let ma_van_ban_den = this.ma_van_ban_den
        let ma_ctcb = global.ma_ctcb_kc
        let y_kien_xu_ly_hoan_tat = this.state.yKienXuLy
        API.VanBanDen.AU_VBDEN_LDDHTVBD(ma_ctcb, ma_van_ban_den, ma_xu_ly_den, y_kien_xu_ly_hoan_tat).then((response) => {
            if (response) {
                ToastSuccess("Hoàn tất văn bản thành công!", () => {
                    API.Login.AU_DSNVCB(global.ma_ctcb_kc, this.props.dispatch).then(() => {
                        this.props.navigation.dispatch(resetStack("VBDenDuyet"))
                    })
                })
            } else {
                Toast.show({ text: "Xảy ra lỗi. Vui lòng thực hiện lại sau!", type: "danger", buttonText: "Đóng", position: "top", duration: 2000 })
            }
        })
    }

    render() {
        const popAction = StackActions.pop({ n: 1 })

        return (
            <Container style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
                <HeaderWithLeftRight
                    title={"Thêm ý kiến xử lý"}
                    buttonLeftName="arrow-back"
                    buttonRightName="done"
                    onPressLeftButton={() => { this.props.navigation.dispatch(popAction) }}
                    onPressRightButton={this.onPressSend}
                />
                <ScrollView style={styles.scrollContainer}>
                    <Text style={[styles.TextTitle]}>Ý kiến xử lý</Text>
                    <Textarea
                        autoCorrect={false}
                        style={styles.TextInput}
                        autoFocus={true}
                        rowSpan={10}
                        value={this.state.yKienXuLy}
                        onChangeText={(text) => { this.setState({ yKienXuLy: text }) }}
                    />
                </ScrollView>
            </Container >
        );
    }
}

const styles = StyleSheet.create({
    scrollContainer: {
        flex: 1,
        marginTop: 10,
        marginLeft: 10,
        marginRight: 10
    },
    TextTitle: {
        fontSize: AppConfig.FontSizeSmall,
        margin: 5
    },
    TextInput: {
        flex: 1,
        fontFamily: AppConfig.fontFamily,
        fontSize: AppConfig.FontSizeNorman / PixelRatio.getFontScale(),
        color: "#000000",
        borderColor: AppConfig.defaultLineColor,
        borderWidth: AppConfig.defaultLineWidth,
        borderRadius: 5,
        marginRight: 2,
        paddingLeft: 5,
        paddingTop: 7,
        minHeight: 40,
        margin: 5
    },
})

export default connect()(VBDen_LDNhapYKien)