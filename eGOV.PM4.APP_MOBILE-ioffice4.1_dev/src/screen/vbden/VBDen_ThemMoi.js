import React, { Component } from 'react';
import { View, ScrollView, StyleSheet, Alert, TouchableOpacity, TextInput, PixelRatio } from "react-native"
import { Container, Icon, Button, Text, Card, CardItem, Toast, ActionSheet, CheckBox } from "native-base"
import { StackActions } from "react-navigation"
import { connect } from "react-redux"
import { AppConfig } from "../../AppConfig"
import { resetStack, ReturnNumber, ToastSuccess } from "../../untils/TextUntils"
import HeaderWithLeft from "../../components/Header/HeaderWithLeft"
import ModalDangThucHien from "../../components/Modal/ModalDangThucHien"
import API from "../../networks"
import DateTimePicker from 'react-native-modal-datetime-picker'
import ViewFile from "../../components/ViewFile"
import moment from "moment"

const nowYear = new Date().getFullYear();

class Table_ChiTiet_VBDEN extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isLoading: false,
            arrSoVanBan: [],
            arrCapDoKhan: [],
            arrCapDoMat: [],
            arrLoaiVanBan: [],
            arrLanhDaoDuyet: [],
            arrLinhVucVanBan: [],
            nam: nowYear,
            lanh_dao_duyet: "",
            ma_so_vb_den: 0,
            isDatePickerToVisibleNBH: false,
            isDatePickerToVisibleND: false,
            sms: false,
            van_ban_giay: false,
            item: {
                ma_van_ban_den_kc: "",
                ma_van_ban_kc: "",
                ma_xu_ly_den: "",
                so_den: "",
                trich_yeu: "",
                so_ky_hieu: "",
                ngay_nhan: "",
                ngay_xem: "",
                file_van_ban_bs: "",
                ten_co_quan_ban_hanh: "",
                ten_don_vi_gui: "",
                file_van_ban: "",
                ma_linh_vuc_van_ban: "",
                ma_loai_van_ban: 1,
                ma_cap_do_khan: 1,
                ma_cap_do_mat: 1,
                ngay_den: "",
                han_xu_ly_chung: "",
                ngay_ban_hanh: "",
                so_ioffice: "",
                gui_kem_vb_giay: "",
                so_ban_phat_hanh: 1,
                so_trang_vb: "",
                nguoi_ky: "",
                noi_luu_ban_chinh: "",
                ma_ctcb_duyet: "",
                hien_thi_vblq: "",
                chuoi_vb_dien_tu: "",
                da_luu_file: "",
                r: "",
                ghi_chu: "",
            }
        }
    }

    componentWillReceiveProps(nextProps) {
        this.getSoVanBan()
        this.getCapDoKhan()
        this.getCapDoMat()
        this.getLanhDaoDuyet();
        this.getLoaiVanBan();
        this.getLinhVucVanBan();
        let dataDetails = nextProps.dataDetails
        let item = {
            ma_van_ban_den_kc: dataDetails.ma_van_ban_den_kc,
            ma_van_ban_kc: dataDetails.ma_van_ban_kc || 0,
            ma_xu_ly_den: dataDetails.ma_xu_ly_den || 0,
            so_den: dataDetails.so_den || "",
            trich_yeu: dataDetails.trich_yeu,
            so_ky_hieu: dataDetails.so_ky_hieu,
            ngay_nhan: dataDetails.ngay_nhan,
            ngay_xem: dataDetails.ngay_xem || "",
            file_van_ban_bs: dataDetails.file_van_ban_bs || "",
            ten_co_quan_ban_hanh: dataDetails.ten_co_quan_ban_hanh,
            ten_don_vi_gui: dataDetails.ten_don_vi_gui,
            file_van_ban: dataDetails.file_van_ban,
            ma_linh_vuc_van_ban: dataDetails.ma_linh_vuc_van_ban,
            ma_loai_van_ban: dataDetails.ma_loai_van_ban,
            ma_cap_do_khan: dataDetails.ma_cap_do_khan,
            ma_cap_do_mat: dataDetails.ma_cap_do_mat,
            ngay_den: moment(dataDetails.ngay_den, "DD/MM/YYYY hh:mm").format("DD/MM/YYYY"),
            han_xu_ly_chung: dataDetails.han_xu_ly_chung || "",
            ngay_ban_hanh: moment(dataDetails.ngay_ban_hanh, "DD/MM/YYYY hh:mm").format("DD/MM/YYYY"),
            so_ioffice: dataDetails.so_ioffice,
            gui_kem_vb_giay: dataDetails.gui_kem_vb_giay,
            so_ban_phat_hanh: dataDetails.so_ban_phat_hanh || 1,
            so_trang_vb: dataDetails.so_trang_vb || "",
            nguoi_ky: dataDetails.nguoi_ky,
            noi_luu_ban_chinh: dataDetails.noi_luu_ban_chinh || "",
            ma_ctcb_duyet: dataDetails.ma_ctcb_duyet || "",
            hien_thi_vblq: dataDetails.hien_thi_vblq,
            chuoi_vb_dien_tu: dataDetails.chuoi_vb_dien_tu || "",
            da_luu_file: dataDetails.da_luu_file || 0,
            r: dataDetails.r,
            ghi_chu: "",
        }
        this.setState({ item })
        if (dataDetails.loai_vb_dien_tu == 1 && global.vbde_gop_chung_vb_dien_tu == 1) {
            this.getInfoEdxml(dataDetails.ten_goi_tin_xml);
        }

    };

    getInfoEdxml = async (filexml) => {
        await API.VanBanDen.AU_VBDE_GET_INFOEDXML(filexml).then((arrInforEdxm) => {
            API.VanBanDen.AU_VBDE_COPY_DOCUMENT_EDOC(arrInforEdxm.filevb).then((res) => {
                this.setState({ item: { ...this.state.item, file_van_ban: res } });
                this.setState({ item: { ...this.state.item, ma_van_ban_den_kc: 0 } });
                this.setState({ item: { ...this.state.item, nguoi_ky: arrInforEdxm.nguoiky } });
                this.setState({ item: { ...this.state.item, ten_co_quan_ban_hanh: arrInforEdxm.cqbh } });
                this.setState({ item: { ...this.state.item, ma_linh_vuc_van_ban: this.state.arrLinhVucVanBan[0].ma_linh_vuc_van_ban_kc } });
                this.setState({ item: { ...this.state.item, ma_cap_do_mat: this.state.arrCapDoMat[0].ma_cap_do_mat_kc } });
                this.setState({ item: { ...this.state.item, ma_loai_van_ban: this.state.arrLoaiVanBan[0].ma_loai_van_ban_kc } });
                this.setState({ item: { ...this.state.item, ma_cap_do_khan: this.state.arrCapDoKhan[0].ma_cap_do_khan_kc } });
                this.setState({ item: { ...this.state.item, noi_luu_ban_chinh: arrInforEdxm.noiluu } });
            }
            );
        });
    }
    getSoVanBan = async () => {
        await API.VanBanDen.AU_VBDEN_DSDMSVBD(global.ma_don_vi_quan_tri, new Date().getFullYear()).then((arrSoVanBan) => {
            if (arrSoVanBan.length > 0) {
                let ma_so_vb_den = arrSoVanBan[0].ma_so_vb_den_kc
                this.setState({ arrSoVanBan, ma_so_vb_den }, () => { this.getSoDen(ma_so_vb_den) })
            }
        })
    }

    onPressSoVanBan = () => {
        let options = this.state.arrSoVanBan.map(obj => {
            return obj.ten_so_vb_den
        })
        console.log(options)
        options.unshift("Đóng")
        ActionSheet.show(
            {
                options: options,
                title: "Chọn sổ văn bản đến"
            },
            buttonIndex => {
                if (buttonIndex !== undefined && buttonIndex > 0) {
                    let ma_so_vb_den_kc = this.state.arrSoVanBan[buttonIndex - 1].ma_so_vb_den_kc
                    this.setState({ ma_so_vb_den: ma_so_vb_den_kc }, () => {
                        this.getSoDen(ma_so_vb_den_kc)
                    })
                }
            }
        )
    }

    getSoDen = async (ma_so_vb_den, nam) => {
        await API.VanBanDen.AU_VBDEN_LSDTSVBD(ma_so_vb_den, nam).then((so_den) => {
            if (so_den) {
                this.setState({ item: { ...this.state.item, so_den } })
            }
        })
    }

    getCapDoKhan = async () => {
        await API.VanBanDen.AU_VBDEN_DSCDK().then((arrCapDoKhan) => {
            this.setState({ arrCapDoKhan })
        })
    }

    onPressCapDoKhan = () => {
        ActionSheet.show(
            {
                options: this.state.arrCapDoKhan.map(obj => {
                    return obj.ten_cap_do_khan
                }),
                title: "Chọn cấp độ khẩn"
            },
            buttonIndex => {
                if (buttonIndex !== undefined) {
                    let ma_cap_do_khan = this.state.arrCapDoKhan[buttonIndex].ma_cap_do_khan_kc
                    this.setState({ item: { ...this.state.item, ma_cap_do_khan } })
                }
            }
        )
    }

    getCapDoMat = async () => {
        await API.VanBanDen.AU_VBDEN_DSCDM().then((arrCapDoMat) => {
            this.setState({ arrCapDoMat })
        })
    }

    onPressCapDoMat = () => {
        ActionSheet.show(
            {
                options: this.state.arrCapDoMat.map(obj => {
                    return obj.ten_cap_do_mat
                }),
                title: "Chọn cấp độ mật"
            },
            buttonIndex => {
                if (buttonIndex !== undefined) {
                    let ma_cap_do_mat = this.state.arrCapDoMat[buttonIndex].ma_cap_do_mat_kc
                    this.setState({ item: { ...this.state.item, ma_cap_do_mat } })
                }
            }
        )
    }

    getLoaiVanBan = async () => {
        await API.VanBanDen.AU_VBDEN_DSLVB().then((arrLoaiVanBan) => {
            this.setState({ arrLoaiVanBan })
        })
    }

    onPressLoaiVanBan = () => {
        ActionSheet.show(
            {
                options: this.state.arrLoaiVanBan.map(obj => {
                    return obj.ten_loai_van_ban
                }),
                title: "Chọn loại văn bản"
            },
            buttonIndex => {
                if (buttonIndex !== undefined) {
                    let ma_loai_van_ban = this.state.arrLoaiVanBan[buttonIndex].ma_loai_van_ban_kc
                    this.setState({ item: { ...this.state.item, ma_loai_van_ban } })
                }
            }
        )
    }

    getLinhVucVanBan = async () => {
        await API.VanBanDen.AU_VBDEN_DSLVVB().then((arrLinhVucVanBan) => {
            this.setState({ arrLinhVucVanBan })

        })
    }
    onPressLinhVucVanBan = () => {
        ActionSheet.show({
            options: this.state.arrLinhVucVanBan.map(obj => {
                return obj.ten_linh_vuc_van_ban
            }),
            title: "Chọn lĩnh vực văn bản"
        },
            buttonIndex => {
                if (buttonIndex !== undefined) {
                    let ma_linh_vuc_van_ban = this.state.arrLinhVucVanBan[buttonIndex].ma_linh_vuc_van_ban_kc
                    this.setState({ item: { ...this.state.item, ma_linh_vuc_van_ban } });
                }
            })
    }

    getLanhDaoDuyet = async () => {
        await API.VanBanDen.AU_VBDEN_DSLDDVBDE().then((arrLanhDaoDuyet) => {
            let ma_ctcb_duyet = null
            let lanh_dao_duyet = ""
            if (arrLanhDaoDuyet.length > 0) {
                ma_ctcb_duyet = arrLanhDaoDuyet[0].ma
                lanh_dao_duyet = arrLanhDaoDuyet[0].ten + " - " + arrLanhDaoDuyet[0].ten_chuc_vu
            }
            this.setState({ arrLanhDaoDuyet, lanh_dao_duyet, item: { ...this.state.item, ma_ctcb_duyet } })
        })
    }

    onPressLanhDaoDuyet = () => {
        let { arrLanhDaoDuyet } = this.state
        ActionSheet.show(
            {
                options: arrLanhDaoDuyet.map(obj => {
                    return obj.ten + " - " + obj.ten_chuc_vu
                }),
                title: "Chọn loại văn bản"
            },
            buttonIndex => {
                if (buttonIndex !== undefined) {
                    let ma_ctcb_duyet = arrLanhDaoDuyet[buttonIndex].ma
                    let lanh_dao_duyet = arrLanhDaoDuyet[buttonIndex].ten + " - " + arrLanhDaoDuyet[buttonIndex].ten_chuc_vu
                    this.setState({ lanh_dao_duyet, item: { ...this.state.item, ma_ctcb_duyet } })
                }
            }
        )
    }

    onPressNam = () => {
        let options = []
        for (i = -3; i <= 3; i++) {
            options.push((nowYear + i).toString())
        }
        ActionSheet.show(
            {
                options: options,
                title: "Chọn năm"
            },
            buttonIndex => {
                if (buttonIndex !== undefined) {
                    let nam = options[buttonIndex]
                    this.setState({ nam: nam }, () => {
                        this.getSoDen(this.state.arrSoVanBan[0].ma_so_vb_den_kc, nam)
                    })
                }
            }
        )
    }

    _showDatePickerNBH = () => this.setState({ isDatePickerToVisibleNBH: true })

    _hideDatePickerNBH = () => this.setState({ isDatePickerToVisibleNBH: false })

    _handleDatePickedNBH = (date) => { this.setState({ item: { ...this.state.item, ngay_ban_hanh: moment(date).format('DD/MM/YYYY') } }); this._hideDatePickerNBH() }

    _showDatePickerND = () => this.setState({ isDatePickerToVisibleND: true })

    _hideDatePickerND = () => this.setState({ isDatePickerToVisibleND: false })

    _handleDatePickedND = (date) => { this.setState({ item: { ...this.state.item, ngay_den: moment(date).format('DD/MM/YYYY') } }); this._hideDatePickerND() }

    onGuiLanhDao = () => {
        let { item, ma_so_vb_den, sms, van_ban_giay } = this.state
        item.ma_so_vb_den = ma_so_vb_den
        item.sms = sms ? 1 : 0
        item.gui_kem_van_ban_giay = van_ban_giay ? 1 : 0
        item.trang_thai_van_ban_den = 2
        //Trạng thái = 2 lưu chuyển lãnh đạo duyệt
        Alert.alert("Xác nhận", "Chuyển lãnh đạo duyệt?",
            [
                { text: "Không", style: "cancel" },
                {
                    text: "Đồng ý", onPress: () => {
                        this.setState({ isLoading: true })
                        API.VanBanDen.AU_VBDEN_TMVBD(item).then((response) => {
                            if (response) {
                                API.VanBanDen.AU_VBDEN_CNKTMVBD(item.ma_van_ban_den_kc)
                                if (this.props.dataDetails.loai_vb_dien_tu == 1 && global.vbde_gop_chung_vb_dien_tu == 1) {
                                    console.log("update trang thái!");
                                    this.onUpdateTrangThaiVBTempEdoc(this.props.dataDetails.ma_van_ban_den_kc, 1);
                                }
                                ToastSuccess("Chuyển duyệt thành công!", () => {
                                    this.setState({ isLoading: false })
                                    this.props.navigation.dispatch(resetStack("VBDenVanThu"))
                                })
                            } else {
                                this.setState({ isLoading: false })
                                Toast.show({ text: "Xảy ra lỗi. Vui lòng thực hiện lại sau!", type: "danger", buttonText: "Đóng", position: "top", duration: 2000 })
                            }
                        })
                    }
                }
            ],
            { cancelable: false }
        )
    }

    onLuuTam = () => {
        let { item, ma_so_vb_den, sms, van_ban_giay } = this.state
        item.ma_so_vb_den = ma_so_vb_den
        item.sms = sms ? 1 : 0
        item.gui_kem_van_ban_giay = van_ban_giay ? 1 : 0
        item.trang_thai_van_ban_den = 1

        //Trạng thái = 1 lưu tạm văn bản
        Alert.alert("Xác nhận", "Lưu tạm văn bản?",
            [
                { text: "Không", style: "cancel" },
                {
                    text: "Đồng ý", onPress: () => {
                        this.setState({ isLoading: true })
                        API.VanBanDen.AU_VBDEN_TMVBD(item).then((response) => {
                            if (response) {
                                if (this.props.dataDetails.loai_vb_dien_tu == 1 && global.vbde_gop_chung_vb_dien_tu == 1) {
                                    this.onUpdateTrangThaiVBTempEdoc(this.props.dataDetails.ma_van_ban_den_kc, 1);
                                }
                                API.VanBanDen.AU_VBDEN_CNKTMVBD(item.ma_van_ban_den_kc)
                                ToastSuccess("Lưu tạm văn bản thành công!", () => {
                                    this.setState({ isLoading: false })
                                    this.props.navigation.dispatch(resetStack("VBDenVanThu"))
                                })
                            } else {
                                this.setState({ isLoading: false })
                                Toast.show({ text: "Xảy ra lỗi. Vui lòng thực hiện lại sau!", type: "danger", buttonText: "Đóng", position: "top", duration: 2000 })
                            }
                        })
                    }
                }
            ],
            { cancelable: false }
        )
    }

    onHuyVanBan = () => {
        let { item, ma_so_vb_den, sms, van_ban_giay } = this.state
        if (this.props.dataDetails.loai_vb_dien_tu == 1 && global.vbde_gop_chung_vb_dien_tu == 1) {
            item.ma_so_vb_den = ma_so_vb_den
            item.sms = sms ? 1 : 0
            item.gui_kem_van_ban_giay = van_ban_giay ? 1 : 0
            item.trang_thai_van_ban_den = 0

            API.VanBanDen.AU_VBDEN_THEM_DE_HUY_EDOC(item).then((response) => {
                if (response) {
                    Alert.alert("Xác nhận", "Hủy văn bản đến?",
                        [
                            { text: "Không", style: "cancel" },
                            {
                                text: "Đồng ý", onPress: () => {
                                    this.setState({ isLoading: true })
                                    this.onUpdateTrangThaiVBTempEdoc(this.props.dataDetails.ma_van_ban_den_kc, -2);
                                    this.onUpdateStatusFileEdxml(this.props.dataDetails.ten_goi_tin_xml);
                                    this.opUpdateStatusTrenTrucEdoc(this.props.dataDetails.ten_goi_tin_xml)
                                    if (response) {
                                        ToastSuccess("Hủy văn bản thành công!", () => {
                                            this.setState({ isLoading: false })
                                            this.props.navigation.dispatch(resetStack("VBDenVanThu"))
                                        })
                                    } else {
                                        this.setState({ isLoading: false })
                                        Toast.show({ text: "Xảy ra lỗi. Vui lòng thực hiện lại sau!", type: "danger", buttonText: "Đóng", position: "top", duration: 2000 })
                                    }
                                }
                            }
                        ],
                        { cancelable: false }
                    )
                }
            })
        } else {
            Alert.alert("Xác nhận", "Hủy văn bản đến?",
                [
                    { text: "Không", style: "cancel" },
                    {
                        text: "Đồng ý", onPress: () => {
                            this.setState({ isLoading: true })
                            API.VanBanDen.AU_VBDEN_VTHVBDDT(item.ma_xu_ly_den).then((response) => {
                                if (response) {
                                    ToastSuccess("Hủy văn bản thành công!", () => {
                                        this.setState({ isLoading: false })
                                        this.props.navigation.dispatch(resetStack("VBDenVanThu"))
                                    })
                                } else {
                                    this.setState({ isLoading: false })
                                    Toast.show({ text: "Xảy ra lỗi. Vui lòng thực hiện lại sau!", type: "danger", buttonText: "Đóng", position: "top", duration: 2000 })
                                }
                            })
                        }
                    }
                ],
                { cancelable: false }
            )
        }

    }

    onUpdateTrangThaiVBTempEdoc = async (ma_van_ban_edoc_temp, status) => {
        await API.VanBanDen.AU_UPDATE_STATUS_VBTEMP_EDOC(ma_van_ban_edoc_temp, status);
    }

    onUpdateStatusFileEdxml = async (filexml) => {
        await API.VanBanDen.AU_VBDE_UPDATE_STATUS_EDXML(filexml);
    }

    opUpdateStatusTrenTrucEdoc = async (filexml) => {
        await API.VanBanDen.AU_VBDE_UPDATE_STATUS_TRUC_EDOC(filexml);
    }

    renderButton = () => {
        return (
            <CardItem style={[this.styles.CardItem, this.styles.CardItemButton]}>
                <Button success style={this.styles.Button} onPress={this.onLuuTam}><Text style={this.styles.ButtonText}>Lưu chờ chuyển</Text></Button>
                <Button primary style={this.styles.Button} onPress={this.onGuiLanhDao}><Text style={this.styles.ButtonText}>Chuyển LĐ duyệt</Text></Button>
                <Button danger style={this.styles.Button} onPress={this.onHuyVanBan}><Text style={this.styles.ButtonText}>Hủy</Text></Button>
            </CardItem>
        )
    }

    render() {
        this.styles = this.props.styles

        if (this.props.dataDetails) {
            const { arrSoVanBan, arrCapDoKhan, arrCapDoMat, arrLoaiVanBan, nam,
                lanh_dao_duyet, ma_so_vb_den, isDatePickerToVisibleNBH, isDatePickerToVisibleND,
                item, arrLinhVucVanBan, isLoading } = this.state
            return (
                <Card noShadow style={this.styles.Card}>
                    <ModalDangThucHien
                        isVisible={isLoading}
                        title={"Đang thực hiện"}
                    />
                    <CardItem style={this.styles.CardItem}>
                        <Text style={this.styles.textLeft}> Số ký hiệu</Text>
                        <TextInput
                            ref={input => { this.txtSoKyHieu = input }}
                            multiline={false}
                            style={{
                                width: "100%",
                                height: 45,
                                paddingRight: 18,
                                fontFamily: AppConfig.fontFamily,
                                fontSize: this.props.fontSize.FontSizeNorman / PixelRatio.getFontScale(),
                                borderColor: AppConfig.defaultLineColor,
                                borderBottomWidth: AppConfig.defaultLineWidth,
                            }}
                            autoCapitalize="none"
                            returnKeyType={"done"}
                            value={item.so_ky_hieu}
                            onChangeText={(text) => this.setState({ item: { ...this.state.item, so_ky_hieu: text } })}
                        />
                    </CardItem>
                    <CardItem style={this.styles.CardItem}>
                        <Text style={this.styles.textLeft}> Trích yếu</Text>
                        <TextInput
                            ref={input => { this.txtTrichYeu = input }}
                            multiline={true}
                            style={{
                                width: "100%",
                                minHeight: 45,
                                maxHeight: 150,
                                fontFamily: AppConfig.fontFamily,
                                fontSize: this.props.fontSize.FontSizeNorman / PixelRatio.getFontScale(),
                                borderColor: AppConfig.defaultLineColor,
                                borderBottomWidth: AppConfig.defaultLineWidth,
                            }}
                            autoCapitalize="none"
                            returnKeyType={"done"}
                            value={item.trich_yeu}
                            onChangeText={(text) => this.setState({ item: { ...this.state.item, trich_yeu: text } })}
                        />
                    </CardItem>
                    <View style={{ flexDirection: "row" }}>
                        {
                            (arrLoaiVanBan.length > 0) && (
                                <CardItem style={this.styles.CardItem}>
                                    <Text style={this.styles.textLeft}> Loại văn bản</Text>
                                    <TouchableOpacity onPress={this.onPressLoaiVanBan} style={{ width: "100%" }}>
                                        <TextInput
                                            ref={input => { this.txtSoKyHieu = input }}
                                            multiline={true}
                                            editable={false}
                                            style={{
                                                width: "100%",
                                                height: 45,
                                                paddingRight: 18,
                                                fontFamily: AppConfig.fontFamily,
                                                color: "#000",
                                                fontSize: this.props.fontSize.FontSizeNorman / PixelRatio.getFontScale(),
                                                borderColor: AppConfig.defaultLineColor,
                                                borderBottomWidth: AppConfig.defaultLineWidth,

                                            }}
                                            autoCapitalize="none"
                                            returnKeyType={"done"}
                                            value={item.ma_loai_van_ban ? arrLoaiVanBan.filter(el => el.ma_loai_van_ban_kc === item.ma_loai_van_ban)[0].ten_loai_van_ban : arrLoaiVanBan[0].ten_loai_van_ban}
                                        />
                                        <Icon name="caret-down" type="FontAwesome" style={{
                                            fontSize: this.props.fontSize.FontSizeNorman,
                                            top: 16,
                                            right: 0,
                                            position: "absolute",
                                        }} />
                                    </TouchableOpacity>
                                </CardItem>
                            )
                        }
                        <CardItem style={this.styles.CardItem}>
                            <Text style={this.styles.textLeft}> Số bản phát hành</Text>
                            <TextInput
                                ref={input => { this.txtSoDen = input }}
                                multiline={false}
                                style={{
                                    width: "100%",
                                    height: 45,
                                    fontFamily: AppConfig.fontFamily,
                                    fontSize: this.props.fontSize.FontSizeNorman / PixelRatio.getFontScale(),
                                    borderColor: AppConfig.defaultLineColor,
                                    borderBottomWidth: AppConfig.defaultLineWidth,
                                }}
                                autoCapitalize="none"
                                returnKeyType={"done"}
                                value={item.so_ban_phat_hanh.toString()}
                                onChangeText={(text) => this.setState({ item: { ...this.state.item, so_ban_phat_hanh: ReturnNumber(text) } })}
                            />
                        </CardItem>
                    </View>
                    {

                        (this.props.dataDetails.loai_vb_dien_tu == 1 && global.vbde_gop_chung_vb_dien_tu == 1 && arrLinhVucVanBan.length > 0) && (
                            <View style={{ flexDirection: "row" }}>
                                <CardItem style={this.styles.CardItem}>
                                    <Text style={this.styles.textLeft}> Lĩnh vực văn bản</Text>
                                    <TouchableOpacity onPress={this.onPressLinhVucVanBan} style={{ width: "100%" }}>
                                        <TextInput
                                            ref={input => { this.txtSoKyHieu = input }}
                                            multiline={true}
                                            editable={false}
                                            style={{
                                                width: "100%",
                                                height: 45,
                                                paddingRight: 18,
                                                fontFamily: AppConfig.fontFamily,
                                                color: "#000",
                                                fontSize: this.props.fontSize.FontSizeNorman / PixelRatio.getFontScale(),
                                                borderColor: AppConfig.defaultLineColor,
                                                borderBottomWidth: AppConfig.defaultLineWidth,

                                            }}
                                            autoCapitalize="none"
                                            returnKeyType={"done"}
                                            value={item.ma_linh_vuc_van_ban ? arrLinhVucVanBan.filter(el => el.ma_linh_vuc_van_ban_kc === item.ma_linh_vuc_van_ban)[0].ten_linh_vuc_van_ban : arrLinhVucVanBan[0].ten_linh_vuc_van_ban}
                                        />
                                        <Icon name="caret-down" type="FontAwesome" style={{
                                            fontSize: this.props.fontSize.FontSizeNorman,
                                            top: 16,
                                            right: 0,
                                            position: "absolute",
                                        }} />
                                    </TouchableOpacity>
                                </CardItem>
                            </View>
                        )
                    }
                    <View style={{ flexDirection: "row" }}>
                        <CardItem style={this.styles.CardItem}>
                            <Text style={this.styles.textLeft}> Ngày ban hành</Text>
                            <TouchableOpacity onPress={this._showDatePickerNBH} style={{ width: "100%", }}>
                                <TextInput
                                    ref={input => { this.txtSoKyHieu = input }}
                                    multiline={false}
                                    editable={false}
                                    style={{
                                        width: "100%",
                                        height: 45,
                                        color: "#000",
                                        fontFamily: AppConfig.fontFamily,
                                        fontSize: this.props.fontSize.FontSizeNorman / PixelRatio.getFontScale(),
                                        borderColor: AppConfig.defaultLineColor,
                                        borderBottomWidth: AppConfig.defaultLineWidth,
                                    }}
                                    autoCapitalize="none"
                                    returnKeyType={"done"}
                                    value={item.ngay_ban_hanh.toString()}
                                />
                            </TouchableOpacity>
                            <DateTimePicker
                                isVisible={isDatePickerToVisibleNBH}
                                onConfirm={this._handleDatePickedNBH}
                                onCancel={this._hideDatePickerNBH}
                                titleIOS="Chọn ngày ban hành"
                                cancelTextIOS="Đóng"
                                confirmTextIOS="Chọn"
                                mode="date"
                                locale="vi"
                                date={new Date(moment(item.ngay_ban_hanh, "DD/MM/YYYY"))}
                            />
                        </CardItem>
                        <CardItem style={this.styles.CardItem}>
                            <Text style={this.styles.textLeft}> Ngày đến</Text>
                            <TouchableOpacity onPress={this._showDatePickerND} style={{ width: "100%", }}>
                                <TextInput
                                    ref={input => { this.txtSoKyHieu = input }}
                                    multiline={false}
                                    editable={false}
                                    style={{
                                        width: "100%",
                                        height: 45,
                                        color: "#000",
                                        fontFamily: AppConfig.fontFamily,
                                        fontSize: this.props.fontSize.FontSizeNorman / PixelRatio.getFontScale(),
                                        borderColor: AppConfig.defaultLineColor,
                                        borderBottomWidth: AppConfig.defaultLineWidth,
                                    }}
                                    autoCapitalize="none"
                                    returnKeyType={"done"}
                                    value={item.ngay_den.toString()}
                                />
                            </TouchableOpacity>
                            <DateTimePicker
                                isVisible={isDatePickerToVisibleND}
                                onConfirm={this._handleDatePickedND}
                                onCancel={this._hideDatePickerND}
                                titleIOS="Chọn ngày đến"
                                cancelTextIOS="Đóng"
                                confirmTextIOS="Chọn"
                                mode="date"
                                locale="vi"
                                date={new Date(moment(item.ngay_den, "DD/MM/YYYY"))}
                            />
                        </CardItem>
                    </View>
                    {
                        (arrSoVanBan.length > 0) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}> Sổ văn bản</Text>
                                <TouchableOpacity onPress={this.onPressSoVanBan} style={{ width: "100%", }}>
                                    <TextInput
                                        ref={input => { this.txtSoKyHieu = input }}
                                        multiline={true}
                                        editable={false}
                                        style={{
                                            width: "100%",
                                            height: 45,
                                            paddingRight: 18,
                                            fontFamily: AppConfig.fontFamily,
                                            color: "#000",
                                            fontSize: this.props.fontSize.FontSizeNorman / PixelRatio.getFontScale(),
                                            borderColor: AppConfig.defaultLineColor,
                                            borderBottomWidth: AppConfig.defaultLineWidth,
                                        }}
                                        autoCapitalize="none"
                                        returnKeyType={"done"}
                                        value={arrSoVanBan.filter(el => el.ma_so_vb_den_kc === ma_so_vb_den)[0].ten_so_vb_den}
                                    />
                                    <Icon name="caret-down" type="FontAwesome" style={{
                                        fontSize: this.props.fontSize.FontSizeNorman,
                                        top: 16,
                                        right: 0,
                                        position: "absolute",
                                    }} />
                                </TouchableOpacity>
                            </CardItem>
                        )
                    }
                    <View style={{ flexDirection: "row" }}>
                        <CardItem style={this.styles.CardItem}>
                            <Text style={this.styles.textLeft}> Năm</Text>
                            <TouchableOpacity onPress={this.onPressNam} style={{ width: "100%", }}>
                                <TextInput
                                    ref={input => { this.txtSoKyHieu = input }}
                                    multiline={false}
                                    editable={false}
                                    style={{
                                        width: "100%",
                                        height: 45,
                                        paddingRight: 18,
                                        color: "#000",
                                        fontFamily: AppConfig.fontFamily,
                                        fontSize: this.props.fontSize.FontSizeNorman / PixelRatio.getFontScale(),
                                        borderColor: AppConfig.defaultLineColor,
                                        borderBottomWidth: AppConfig.defaultLineWidth,
                                    }}
                                    autoCapitalize="none"
                                    returnKeyType={"done"}
                                    value={nam.toString()}
                                />
                                <Icon name="caret-down" type="FontAwesome" style={{
                                    fontSize: this.props.fontSize.FontSizeNorman,
                                    top: 16,
                                    right: 0,
                                    position: "absolute",
                                }} />
                            </TouchableOpacity>
                        </CardItem>
                        <CardItem style={this.styles.CardItem}>
                            <Text style={this.styles.textLeft}> Số đến</Text>
                            <TextInput
                                ref={input => { this.txtSoDen = input }}
                                multiline={false}
                                style={{
                                    width: "100%",
                                    height: 45,
                                    fontFamily: AppConfig.fontFamily,
                                    fontSize: this.props.fontSize.FontSizeNorman / PixelRatio.getFontScale(),
                                    borderColor: AppConfig.defaultLineColor,
                                    borderBottomWidth: AppConfig.defaultLineWidth,
                                }}
                                autoCapitalize="none"
                                returnKeyType={"done"}
                                value={item.so_den + ""}
                                onChangeText={(text) => this.setState({ item: { ...this.state.item, so_den: ReturnNumber(text) } })}
                            />
                        </CardItem>
                    </View>
                    <CardItem style={this.styles.CardItem}>
                        <Text style={this.styles.textLeft}> Cơ quan ban hành</Text>
                        <TextInput
                            ref={input => { this.txtSoKyHieu = input }}
                            multiline={true}
                            editable={false}
                            style={{
                                width: "100%",
                                fontFamily: AppConfig.fontFamily,
                                color: "#000",
                                fontSize: this.props.fontSize.FontSizeNorman / PixelRatio.getFontScale(),
                            }}
                            autoCapitalize="none"
                            returnKeyType={"done"}
                            value={item.ten_co_quan_ban_hanh}
                        />
                    </CardItem>
                    <CardItem style={this.styles.CardItem}>
                        <Text style={this.styles.textLeft}> Người ký</Text>
                        <TextInput
                            ref={input => { this.txtSoKyHieu = input }}
                            multiline={true}
                            editable={false}
                            style={{
                                width: "100%",
                                fontFamily: AppConfig.fontFamily,
                                color: "#000",
                                fontSize: this.props.fontSize.FontSizeNorman / PixelRatio.getFontScale(),
                            }}
                            autoCapitalize="none"
                            returnKeyType={"done"}
                            value={item.nguoi_ky}
                        />
                    </CardItem>
                    <View style={{ flexDirection: "row" }}>
                        {
                            (arrCapDoKhan.length > 0) && (
                                <CardItem style={this.styles.CardItem}>
                                    <Text style={this.styles.textLeft}> Cấp độ khẩn</Text>
                                    <TouchableOpacity onPress={this.onPressCapDoKhan} style={{ width: "100%", }}>
                                        <TextInput
                                            ref={input => { this.txtSoKyHieu = input }}
                                            multiline={true}
                                            editable={false}
                                            style={{
                                                width: "100%",
                                                height: 45,
                                                paddingRight: 18,
                                                fontFamily: AppConfig.fontFamily,
                                                color: "#000",
                                                fontSize: this.props.fontSize.FontSizeNorman / PixelRatio.getFontScale(),
                                                borderColor: AppConfig.defaultLineColor,
                                                borderBottomWidth: AppConfig.defaultLineWidth,
                                            }}
                                            autoCapitalize="none"
                                            returnKeyType={"done"}
                                            value={item.ma_cap_do_khan ? arrCapDoKhan.filter(el => el.ma_cap_do_khan_kc === item.ma_cap_do_khan)[0].ten_cap_do_khan : arrCapDoKhan[0].ten_cap_do_khan}
                                        />
                                        <Icon name="caret-down" type="FontAwesome" style={{
                                            fontSize: this.props.fontSize.FontSizeNorman,
                                            top: 16,
                                            right: 0,
                                            position: "absolute",
                                        }} />
                                    </TouchableOpacity>
                                </CardItem>
                            )
                        }
                        {
                            (arrCapDoMat.length > 0) && (
                                <CardItem style={this.styles.CardItem}>
                                    <Text style={this.styles.textLeft}> Cấp độ mật</Text>
                                    <TouchableOpacity onPress={this.onPressCapDoMat} style={{ width: "100%", }}>
                                        <TextInput
                                            ref={input => { this.txtSoKyHieu = input }}
                                            multiline={true}
                                            editable={false}
                                            style={{
                                                width: "100%",
                                                height: 45,
                                                paddingRight: 18,
                                                fontFamily: AppConfig.fontFamily,
                                                color: "#000",
                                                fontSize: this.props.fontSize.FontSizeNorman / PixelRatio.getFontScale(),
                                                borderColor: AppConfig.defaultLineColor,
                                                borderBottomWidth: AppConfig.defaultLineWidth,
                                            }}
                                            autoCapitalize="none"
                                            returnKeyType={"done"}
                                            value={item.ma_cap_do_mat ? arrCapDoMat.filter(el => el.ma_cap_do_mat_kc === item.ma_cap_do_mat)[0].ten_cap_do_mat : arrCapDoMat[0].ten_cap_do_mat}
                                        />
                                        <Icon name="caret-down" type="FontAwesome" style={{
                                            fontSize: this.props.fontSize.FontSizeNorman,
                                            top: 16,
                                            right: 0,
                                            position: "absolute",
                                        }} />
                                    </TouchableOpacity>
                                </CardItem>
                            )
                        }
                    </View>
                    <CardItem style={this.styles.CardItem}>
                        <Text style={this.styles.textLeft}> Lãnh đạo duyệt</Text>
                        <TouchableOpacity onPress={this.onPressLanhDaoDuyet} style={{ width: "100%", }}>
                            <TextInput
                                ref={input => { this.txtSoKyHieu = input }}
                                multiline={true}
                                editable={false}
                                style={{
                                    width: "100%",
                                    paddingRight: 18,
                                    fontFamily: AppConfig.fontFamily,
                                    color: "#000",
                                    fontSize: this.props.fontSize.FontSizeNorman / PixelRatio.getFontScale(),
                                    borderColor: AppConfig.defaultLineColor,
                                    borderBottomWidth: AppConfig.defaultLineWidth,
                                }}
                                autoCapitalize="none"
                                returnKeyType={"done"}
                                value={lanh_dao_duyet}
                            />
                            <Icon name="caret-down" type="FontAwesome" style={{
                                fontSize: this.props.fontSize.FontSizeNorman,
                                top: 16,
                                right: 0,
                                position: "absolute",
                            }} />
                        </TouchableOpacity>
                    </CardItem>
                    {
                        (item.noi_luu_ban_chinh !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}> Nơi lưu bản chính</Text>
                                <TextInput
                                    ref={input => { this.txtSoKyHieu = input }}
                                    multiline={true}
                                    editable={false}
                                    style={{
                                        width: "100%",
                                        fontFamily: AppConfig.fontFamily,
                                        color: "#000",
                                        fontSize: this.props.fontSize.FontSizeNorman / PixelRatio.getFontScale(),
                                        //borderColor: AppConfig.defaultLineColor,
                                        //borderBottomWidth: AppConfig.defaultLineWidth,
                                    }}
                                    autoCapitalize="none"
                                    returnKeyType={"done"}
                                    value={item.noi_luu_ban_chinh}
                                />
                            </CardItem>
                        )
                    }
                    <CardItem style={this.styles.CardItem}>
                        <Text style={this.styles.textLeft}> Ghi chú</Text>
                        <TextInput
                            ref={input => { this.txtGhiChu = input }}
                            multiline={true}
                            style={{
                                width: "100%",
                                minHeight: 40,
                                maxHeight: 80,
                                fontFamily: AppConfig.fontFamily,
                                fontSize: this.props.fontSize.FontSizeNorman / PixelRatio.getFontScale(),
                                borderColor: AppConfig.defaultLineColor,
                                borderBottomWidth: AppConfig.defaultLineWidth,
                            }}
                            autoCapitalize="none"
                            returnKeyType={"done"}
                            value={item.ghi_chu}
                            onChangeText={(text) => this.setState({ item: { ...this.state.item, ghi_chu: text } })}
                        />
                    </CardItem>
                    {
                        (item.file_van_ban_bs !== null) && (
                            <View>
                                <CardItem style={this.styles.CardItem}>
                                    <Text style={this.styles.textLeft}>Tệp tin đính kèm</Text>
                                </CardItem>
                                <CardItem style={this.styles.CardItem}>
                                    <ViewFile
                                        ref={ref => { this.ViewFile = ref }}
                                        src_file={item.file_van_ban_bs}
                                        isUpdateViewCV={this.props.isUpdateViewCV}
                                        updateViewCV={this.props.updateViewCV}
                                        fontSize={this.props.fontSize}
                                        //fabActions={fabActionsTemp}
                                        fabPressItem={(name) => {
                                            this.ViewFile.toggleModal()
                                            switch (name) {
                                                case "btnDuyet":
                                                    setTimeout(() => {
                                                        this.props.onApproved()
                                                    }, 200);
                                                    break;
                                                case "btnDuyetChuyenLDK":
                                                    setTimeout(() => {
                                                        this.props.onForwardLDK()
                                                    }, 200);
                                                    break;
                                                case "btnLDChuyen":
                                                    setTimeout(() => {
                                                        this.props.onLDForward()
                                                    }, 200);
                                                    break;
                                                case "btnChuyen":
                                                    setTimeout(() => {
                                                        this.props.onForward()
                                                    }, 200);
                                                    break;
                                                case "btnDangXuLy":
                                                    setTimeout(() => {
                                                        this.props.onInProcess()
                                                    }, 200);
                                                    break;
                                                case "btnHoanThanh":
                                                    setTimeout(() => {
                                                        this.props.onSuccess()
                                                    }, 200);
                                                    break;
                                                default:
                                                    break;
                                            }
                                        }}
                                    />
                                </CardItem>
                            </View>
                        )
                    }
                    <View style={{ flexDirection: "row" }}>
                        <CardItem style={[this.styles.CardItem, { flexDirection: "row", justifyContent: "flex-start", paddingLeft: -5, marginTop: 5 }]}>
                            <CheckBox onPress={() => { this.setState({ sms: !this.state.sms }) }} checked={this.state.sms} color={AppConfig.blueBackground}></CheckBox>
                            <Text style={[this.styles.textLeft, { marginLeft: 15 }]}>Sms</Text>
                        </CardItem>
                        <CardItem style={[this.styles.CardItem, { flexDirection: "row", justifyContent: "flex-start", paddingLeft: -5, marginTop: 5 }]}>
                            <CheckBox onPress={() => { this.setState({ van_ban_giay: !this.state.van_ban_giay }) }} checked={this.state.van_ban_giay} color={AppConfig.blueBackground}></CheckBox>
                            <Text style={[this.styles.textLeft, { marginLeft: 15 }]}>Kèm văn bản giấy</Text>
                        </CardItem>
                    </View>
                    {this.renderButton()}
                </Card>
            )
        } else {
            return (
                <View></View>
            )
        }
    }
}

class VBDen_Details extends Component {
    constructor(props) {
        super(props)
        this.state = {
            dataDetails: null,
            index: 0
        }

        this.isDuyet = false
        this.ma_xu_ly_den = 0
        this.ma_van_ban_den = 0
        this.item = {}
        this.activeTab = 2
        this.isFromNotify = false
        this.dataSource = []
    }

    componentDidMount = () => {
        this.isDuyet = this.props.navigation.getParam("isDuyet", false)
        this.activeTab = this.props.navigation.getParam("activeTab", 2)
        this.isFromNotify = this.props.navigation.getParam("isFromNotify", false)
        this.dataSource = this.props.navigation.getParam("dataSource", [])
        let index = this.props.navigation.getParam("index", 0)
        this.loadData(index)
    }

    loadData = (index) => {
        this.ma_xu_ly_den = this.dataSource[index].ma_xu_ly_den
        this.ma_van_ban_den = this.dataSource[index].ma_van_ban_den_kc
        this.setState({ index, dataDetails: this.dataSource[index] })
    }

    renderPre = () => {
        let indexPre = this.state.index - 1
        if (indexPre >= 0) {
            return (
                <View style={this.styles.viewLeftBottomButton}>
                    <TouchableOpacity style={this.styles.buttonBottom} onPress={() => { this.loadData(indexPre) }}>
                        <Icon name="reply" type="FontAwesome" style={this.styles.iconBottom} />
                    </TouchableOpacity>
                </View>
            )
        }
    }

    renderNext = () => {
        let indexNext = this.state.index + 1
        let length = this.dataSource.length
        if (length !== 0 && indexNext < length) {
            return (
                <View style={this.styles.viewRightBottomButton}>
                    <TouchableOpacity style={this.styles.buttonBottom} onPress={() => { this.loadData(indexNext) }}>
                        <Icon name="share" type="FontAwesome" style={this.styles.iconBottom} />
                    </TouchableOpacity>
                </View>
            )
        }
    }

    render() {

        this.styles = StyleSheet.create({
            scrollContainer: {
                flex: 1,
                bottom: 0,
                backgroundColor: AppConfig.grayBackground
            },
            viewTieuDe: {
                backgroundColor: AppConfig.grayBackground,
                justifyContent: "center", alignContent: "center",
                flexWrap: "wrap",
                padding: 4,
            },
            textTieuDe: {
                fontSize: this.props.fontSize.FontSizeXLarge,
                fontWeight: "400",
                textAlign: "center"
            },

            //Table_ChiTiet_VBDEN
            Card: {
                marginLeft: 6,
                marginRight: 6,
                marginTop: 0,
                padding: 10,
                borderRadius: 6,
            },
            CardItem: {
                flex: 1,
                marginTop: AppConfig.defaultPadding,
                marginBottom: AppConfig.defaultPadding,
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "flex-start"
            },
            CardItemButton: {
                marginTop: 10,
                paddingTop: 10,
                borderTopColor: AppConfig.defaultLineColor,
                borderTopWidth: AppConfig.defaultLineWidth,
                flexDirection: 'row',
                flexWrap: "wrap",
                justifyContent: "center",
                alignItems: "center"
            },
            Button: {
                margin: 3,
                height: 40,
                justifyContent: "center",
                alignItems: "center",
            },
            ButtonText: {
                fontSize: this.props.fontSize.FontSizeNorman,
                textAlign: "center"
            },
            textLeft: {
                fontSize: this.props.fontSize.FontSizeNorman,
                color: AppConfig.grayText,
                alignSelf: "flex-start"
            },
            textRight: {
                paddingLeft: 5,
                fontSize: this.props.fontSize.FontSizeNorman,
                flexWrap: "wrap",
                flexShrink: 1,
            },
            viewLeftBottomButton: {
                left: 15,
                bottom: 15,
                position: "absolute"
            },
            viewRightBottomButton: {
                right: 15,
                bottom: 15,
                position: "absolute"
            },
            buttonBottom: {
                width: 40,
                height: 40,
                borderRadius: 20,
                backgroundColor: "transparent",
                borderWidth: 0.7,
                borderColor: "#BDBDBD",
                justifyContent: "center",
                alignItems: "center",
            },
            iconBottom: {
                fontSize: this.props.FontSizeSmall,
                color: "#BDBDBD"
            }
        })

        const popAction = StackActions.pop({ n: 1 })
        return (
            <Container style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
                <HeaderWithLeft title="Thêm mới văn bản đến" buttonName="arrow-back" onPressLeftButton={() => {
                    if (this.isFromNotify) {
                        if (this.isDuyet) {
                            this.props.navigation.dispatch(resetStack("VBDenDuyet"))
                        } else {
                            this.props.navigation.dispatch(resetStack("VBDenXuLy"))
                        }
                    } else {
                        this.props.navigation.dispatch(popAction)
                    }
                }} />
                <ScrollView
                    bounces={false}
                    removeClippedSubviews={true}
                    scrollEnabled={true}
                    style={this.styles.scrollContainer}
                >
                    <Table_ChiTiet_VBDEN
                        navigation={this.props.navigation}
                        dataDetails={this.state.dataDetails}
                        isUpdateViewCV={this.isUpdateViewCV}
                        updateViewCV={this.updateViewCV}
                        onApproved={this.onApproved}
                        onForwardLDK={this.onForwardLDK}
                        onSuccess={() => { this.onSuccess(this.ma_xu_ly_den, true) }}
                        onForward={this.onForward}
                        onInProcess={() => { this.onInProcess(this.ma_xu_ly_den, true) }}
                        onLDForward={this.onLDForward}
                        isDuyet={this.isDuyet}
                        activeTab={this.activeTab}
                        fontSize={this.props.fontSize}
                        styles={this.styles}
                    />
                </ScrollView>
                {this.renderPre()}
                {this.renderNext()}
            </Container >
        );
    }
}

function mapStateToProps(state) {
    return {
        fontSize: state.fontSize,
    }
}

export default connect(mapStateToProps)(VBDen_Details)
