import React, { Component, PureComponent } from 'react';
import { View, ScrollView, StyleSheet, FlatList, Image, Modal, TouchableOpacity } from "react-native"
import { connect } from "react-redux"
import { Container, Text, Card, Button, Icon, Item, Left } from "native-base"
import { AppConfig } from "../../AppConfig"
import { RemoveNoiDungTTDH, getFileChiTietTTDH } from "../../untils/TextUntils"
import HeaderWithLeft from "../../components/Header/HeaderWithLeft"
import API from "../../networks"
import ViewFile from "../../components/ViewFile"

class ModalNguoiNhan extends PureComponent {
    render() {
        this.styles = this.props.styles
        return (
            <Modal
                transparent={false}
                animationType="slide"
                visible={this.props.isModalVisible}
                supportedOrientations={["landscape", "landscape-left", "landscape-right", "portrait", "portrait-upside-down"]}
            >
                <View style={{ flex: 1, padding: 15, backgroundColor: AppConfig.grayBackground }}>
                    <View style={{ flex: 1, backgroundColor: "#FFFFFF", borderRadius: 10 }}>
                        <View style={{ position: 'absolute', zIndex: 99, height: 55, left: 0, right: 0, top: 0, justifyContent: "center", padding: 4, alignItems: "center" }}>
                            <Text style={{ fontSize: this.props.fontSize.FontSizeLarge }}>Danh sách cán bộ nhận</Text>
                        </View>
                        <FlatList
                            ref={(ref) => { this.flatListRef = ref }}
                            data={this.props.listMCB_DVCB}
                            keyExtractor={(item, index) => "key" + item.ma_ctcb_kc + index}
                            style={{ position: 'absolute', left: 0, right: 0, top: 55, bottom: 55 }}
                            initialNumToRender={50}
                            renderItem={({ item, index }) => {
                                let color = item.trang_thai_ttdh_gui === 0 ? "red" : "#000000"
                                return (
                                    <Item key={item.ma_ctcb_kc} style={{ padding: 12, borderBottomWidth: AppConfig.defaultLineWidth, borderColor: AppConfig.defaultLineColor }}>
                                        <Left>
                                            <Text style={{ fontSize: this.props.fontSize.FontSizeNorman, color }}>{item.ho_va_ten_can_bo}</Text>
                                        </Left>
                                    </Item>
                                )
                            }}
                            ListEmptyComponent={() => {
                                return (
                                    <View style={{ alignItems: "center", marginTop: 10, }}>
                                        <Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>Không có dữ liệu</Text>
                                    </View>
                                )
                            }}
                            removeClippedSubviews={true}
                        />
                        <View style={{ position: 'absolute', zIndex: 99, height: 55, left: 0, right: 0, bottom: 0, flexDirection: "row", justifyContent: "space-evenly", paddingTop: 10, alignItems: "center" }}>
                            <Button small bordered rounded iconLeft onPress={_ => this.props.toggleModal()}><Icon name="close" type="MaterialIcons"></Icon><Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>Đóng</Text></Button>
                        </View>
                    </View>
                </View>
            </Modal>
        )
    }
}

class TTDH_Details_Item extends PureComponent {
    constructor(props) {
        super(props)
        this.state = {
            showFullNguoiNhan: true,
            listNguoiNhan: [],
        }
        this.item = this.props.item
        this.cb_dsctcb = this.props.cb_dsctcb
        this.loaiGuiNhan = this.props.loaiGuiNhan
    }

    componentDidMount = () => {
        this.getNguoiNhan(this.item.ma_ttdh_kc)
    }

    getNguoiNhan = (ma_ttdh_kc) => {
        API.ThongTinDieuHanh.AU_TTDH_DSCBN(ma_ttdh_kc)
            .then((listNguoiNhan) => {
                this.setState({ listNguoiNhan })
            })
    }

    renderNguoiNhan = () => {
        let { listNguoiNhan } = this.state
        const dataCount = listNguoiNhan.length
        const numberShow = 20
        let reSult = []
        if (dataCount > 0) {
            let numberRender = dataCount > numberShow ? numberShow : dataCount
            for (i = 0; i < numberRender; i += 1) {
                if (i === numberRender - 1) {
                    if (listNguoiNhan[i].trang_thai_ttdh_gui === 0) {
                        reSult.push(<Text key={listNguoiNhan[i].ma_ctcb_kc} style={this.styles.textNguoiNhanChuaXem}>{listNguoiNhan[i].ho_va_ten_can_bo}</Text>)
                    } else {
                        reSult.push(<Text key={listNguoiNhan[i].ma_ctcb_kc} style={this.styles.textNguoiNhanDaXem}>{listNguoiNhan[i].ho_va_ten_can_bo}</Text>)
                    }
                } else {
                    if (listNguoiNhan[i].trang_thai_ttdh_gui === 0) {
                        reSult.push(<Text key={listNguoiNhan[i].ma_ctcb_kc} style={this.styles.textNguoiNhanChuaXem}>{listNguoiNhan[i].ho_va_ten_can_bo},</Text>)
                    } else {
                        reSult.push(<Text key={listNguoiNhan[i].ma_ctcb_kc} style={this.styles.textNguoiNhanDaXem}>{listNguoiNhan[i].ho_va_ten_can_bo},</Text>)
                    }
                }
            }
        }

        if (dataCount > numberShow) {
            return (
                <TouchableOpacity style={{ flexDirection: "row", flexWrap: "wrap" }} onPress={() => { this.setState({ isModalVisible: true }) }} >
                    <Text style={{ fontSize: this.props.fontSize.FontSizeSmall, color: "#808080", margin: 3 }}>Người nhận:</Text>
                    {reSult}
                    <Text style={{ fontSize: this.props.fontSize.FontSizeSmall, color: "#808080", margin: 3 }}>+{dataCount - numberShow}</Text>
                </TouchableOpacity>
            )
        } else {
            return (
                <View style={{ flexDirection: "row", flexWrap: "wrap" }} >
                    <Text style={{ fontSize: this.props.fontSize.FontSizeSmall, color: "#808080", margin: 3 }}>Người nhận:</Text>
                    {reSult}
                </View>
            )
        }
    }

    renderListFile = () => {
        let result = []
        if (this.item.src_file_ttdh !== null) {
            let data = this.item.src_file_ttdh.split(":")
            data = getFileChiTietTTDH(data)
            for (let item of data) {
                let itemTemp = item.split("|")
                result.push(
                    <View key={item} style={this.styles.viewFile}>
                        <Image style={this.styles.iconFile} source={{ uri: itemTemp[3] }} />
                        <Text numberOfLines={1} ellipsizeMode="tail" style={this.styles.textTenFile}>{itemTemp[1]}</Text>
                    </View>
                )
            }
        }
        return result
    }

    onPressReply = () => {
        const { ma_ttdh_kc, ma_ttdh_goc, tieu_de, ma_loai_ttdh, ten_loai_ttdh, src_file_ttdh, ma_ctcb_tao, ten_nguoi_gui, di_dong_can_bo } = this.props.item
        let item = new Object()
        item.ma_ctcb_kc = ma_ctcb_tao
        item.ho_va_ten_can_bo = ten_nguoi_gui
        item.di_dong_can_bo = di_dong_can_bo
        this.props.navigation.navigate("TTDH_Gui", {
            isReply: true,
            titleHeader: "Phản hồi",
            tieuDe: tieu_de,
            ma_ttdh_kc: ma_ttdh_kc,
            ma_ttdh_goc: ma_ttdh_goc,
            ma_loai_ttdh_kc: ma_loai_ttdh,
            ten_loai_ttdh: ten_loai_ttdh,
            src_file_ttdh: src_file_ttdh,
            listNguoiNhan: [item]
        })
    }

    onPressReplyAll = () => {
        const { ma_ttdh_kc, ma_ttdh_goc, tieu_de, ma_loai_ttdh, ten_loai_ttdh, src_file_ttdh, ma_ctcb_tao, ten_nguoi_gui, di_dong_can_bo } = this.props.item
        let listNguoiNhan = []
        let flag = true
        for (let item of this.state.listNguoiNhan) {
            let obj1 = new Object()
            obj1.ma_ctcb_kc = item.ma_ctcb_kc
            obj1.ho_va_ten_can_bo = item.ho_va_ten_can_bo
            obj1.di_dong_can_bo = item.di_dong_can_bo
            listNguoiNhan.push(obj1)
            if (item.ma_ctcb_kc === ma_ctcb_tao) {
                flag = false
            }
        }

        if (flag) {
            let obj = new Object()
            obj.ma_ctcb_kc = ma_ctcb_tao
            obj.ho_va_ten_can_bo = ten_nguoi_gui
            obj.di_dong_can_bo = di_dong_can_bo
            listNguoiNhan.push(obj)
        }
        listNguoiNhan = listNguoiNhan.filter(el => el.ma_ctcb_kc !== global.ma_ctcb_kc)

        this.props.navigation.navigate("TTDH_Gui", {
            isReply: true,
            titleHeader: "Phản hồi",
            tieuDe: tieu_de,
            ma_ttdh_kc: ma_ttdh_kc,
            ma_ttdh_goc: ma_ttdh_goc,
            ma_loai_ttdh_kc: ma_loai_ttdh,
            ten_loai_ttdh: ten_loai_ttdh,
            src_file_ttdh: src_file_ttdh,
            listNguoiNhan: listNguoiNhan
        })
    }

    onPressForward = () => {
        const { ma_ttdh_kc, ma_ttdh_goc, tieu_de, noi_dung, ma_loai_ttdh, ten_loai_ttdh, src_file_ttdh } = this.props.item
        this.props.navigation.navigate("TTDH_Gui", {
            isForward: true,
            titleHeader: "Chuyển tiếp",
            tieuDe: tieu_de,
            noiDungChuyen: noi_dung,
            ma_ttdh_kc: ma_ttdh_kc,
            ma_ttdh_goc: ma_ttdh_goc,
            ma_loai_ttdh_kc: ma_loai_ttdh,
            ten_loai_ttdh: ten_loai_ttdh,
            src_file_ttdh: src_file_ttdh
        })
    }

    render() {
        this.styles = this.props.styles
        const isSend = this.props.cb_dsctcb.ma_can_bo === this.item.ma_can_bo_gui ? true : false
        const isRecieve = !isSend
        return (
            <Card noShadow={true}>
                <View style={this.styles.viewNguoiGui_NguoiNhan_File}>
                    <View style={{ flex: 1, justifyContent: "center", alignItems: 'center', flexDirection: "row" }}>
                        <View style={this.styles.viewNguoiGui}>
                            <Text style={this.styles.textNguoiGui}>{this.item.ten_nguoi_gui}</Text>
                            <Text style={this.styles.textLoaiTTDH}>Loại: {this.item.ten_loai_ttdh}</Text>
                        </View>
                        <View style={this.styles.viewNgayGui}>
                            <Text style={this.styles.textNgayGui}>{this.item.ngay_gui_vn}</Text>
                        </View>
                    </View>
                    <View style={this.styles.viewNguoiNhan}>
                        {this.renderNguoiNhan()}
                    </View>
                    {
                        (this.state.isModalVisible) && (
                            <ModalNguoiNhan
                                listMCB_DVCB={this.state.listNguoiNhan}
                                isModalVisible={this.state.isModalVisible}
                                toggleModal={() => { this.setState({ isModalVisible: false }) }}
                                fontSize={this.props.fontSize}
                                styles={this.styles}
                            />
                        )
                    }
                    <View style={{ justifyContent: "flex-start" }}>
                        <ViewFile src_file={this.item.src_file_ttdh} fontSize={this.props.fontSize}/>
                    </View>
                </View>
                <View style={this.styles.viewNoiDung}>
                    <Text style={this.styles.textNoiDung}>{RemoveNoiDungTTDH(this.item.noi_dung)}</Text>
                </View>
                {
                    (isRecieve) && (
                        <View style={this.styles.viewPhanHoi_ChuyenTiep}>
                            <Button bordered rounded style={this.styles.btnPhanHoi} iconLeft onPress={_ => { this.onPressReply() }}><Icon name="reply" type="FontAwesome" ></Icon><Text style={this.styles.textPhanHoi}>Phản hồi</Text></Button>
                            <Button bordered rounded style={this.styles.btnPhanHoi} iconLeft onPress={_ => { this.onPressReplyAll() }}><Icon name="reply-all" type="FontAwesome" ></Icon><Text style={this.styles.textPhanHoi}>Phản hồi tất cả</Text></Button>
                            <Button bordered rounded style={this.styles.btnPhanHoi} iconLeft onPress={_ => { this.onPressForward() }}><Icon name="share" type="FontAwesome" ></Icon><Text style={this.styles.textPhanHoi}>Chuyển tiếp</Text></Button>
                        </View>
                    )
                }
                {
                    (isSend) && (
                        <View style={this.styles.viewPhanHoi_ChuyenTiep}>
                            <Button bordered rounded style={this.styles.btnPhanHoi} iconLeft onPress={_ => { this.onPressForward() }}><Icon name="share" type="FontAwesome" ></Icon><Text style={this.styles.textPhanHoi}>Chuyển tiếp</Text></Button>
                        </View>
                    )
                }
            </Card>
        )
    }
}

class TTDH_Details extends Component {
    constructor(props) {
        super(props)
        this.state = {
            danhSachChiTiet: [],
            listNguoiNhan: [],
        }
        this.loaiGuiNhan = ""
        this.ma_ttdh_kc = 0
        this.ma_ttdh_gui_kc = 0
    }

    componentDidMount = () => {
        let ma_ttdh_kc = this.props.navigation.getParam("ma_ttdh_kc", 0)
        let ma_ttdh_gui_kc = this.props.navigation.getParam("ma_ttdh_gui_kc", 0)
        this.loaiGuiNhan = this.props.navigation.getParam("loaiGuiNhan", "")
        if (this.loaiGuiNhan === "Nhận") {
            this.getDSChiTietTTDHNhan(ma_ttdh_gui_kc)
        } else {
            this.getNguoiNhan(ma_ttdh_kc)
        }

    }

    getNguoiNhan = (ma_ttdh_kc) => {
        API.ThongTinDieuHanh.AU_TTDH_DSCBN(ma_ttdh_kc)
            .then((listNguoiNhan) => {
                if (listNguoiNhan.length > 0) {
                    this.getDSChiTietTTDHGui(ma_ttdh_kc, listNguoiNhan[0].ma_ctcb_kc, listNguoiNhan[0].ma_ctcb_gui)
                }
            })
    }

    getDSChiTietTTDHGui = (ma_ttdh_kc, ma_ctcb_nhan, ma_ctcb_gui) => {
        API.ThongTinDieuHanh.AU_TTDH_CTTTDHG(ma_ttdh_kc, ma_ctcb_nhan, ma_ctcb_gui).then((danhSachChiTiet) => {
            this.setState({ danhSachChiTiet })
        })
    }

    getDSChiTietTTDHNhan = (ma_ttdh_gui_kc) => {
        const ma_can_bo = this.props.cb_dsctcb.ma_can_bo
        API.ThongTinDieuHanh.AU_TTDH_CTTTDHN(ma_ttdh_gui_kc, ma_can_bo).then((danhSachChiTiet) => {
            this.setState({ danhSachChiTiet })
        })
    }

    renderDSChiTiet = () => {
        let result = []
        for (let item of this.state.danhSachChiTiet) {
            result.push(
                <TTDH_Details_Item
                    loaiGuiNhan={this.loaiGuiNhan}
                    navigation={this.props.navigation}
                    key={item.ma_ttdh_kc}
                    item={item}
                    cb_dsctcb={this.props.cb_dsctcb}
                    fontSize={this.props.fontSize}
                    styles={this.styles}
                />
            )
        }
        return result
    }

    render() {

        this.styles = StyleSheet.create({
            scrollContainer: {
                flex: 1,
                bottom: 0,
                backgroundColor: AppConfig.grayBackground
            },
            viewTieuDe: {
                backgroundColor: AppConfig.grayBackground,
                justifyContent: "center", alignContent: "center",
                flexWrap: "wrap",
                padding: 4,
            },
            textTieuDe: {
                fontSize: this.props.fontSize.FontSizeXLarge,
                fontWeight: "400",
                textAlign: "center"
            },
            viewNguoiGui_NguoiNhan_File: {
                marginLeft: 12,
                marginRight: 12,
            },
            viewNguoiGui: {
                width: "70%",
                justifyContent: "center",
            },
            viewNgayGui: {
                width: "30%",
                justifyContent: "center",
                alignItems: "flex-end",
            },
            viewNguoiNhan: {
                justifyContent: "center",
                flexWrap: "wrap",
            },
            textNguoiGui: {
                fontSize: this.props.fontSize.FontSizeNorman,
                margin: 3,
            },
            textLoaiTTDH: {
                fontSize: this.props.fontSize.FontSizeSmall,
                color: "#808080",
                margin: 3,
            },
            textNguoiNhanChuaXem: {
                fontSize: this.props.fontSize.FontSizeSmall,
                color: "red",
                margin: 3,
            },
            textNguoiNhanDaXem: {
                fontSize: this.props.fontSize.FontSizeSmall,
                color: "#808080",
                margin: 3,
            },
            textNgayGui: {
                fontSize: this.props.fontSize.FontSizeSmall,
                color: "#808080",
                textAlign: "center",
                margin: 3,
            },
            textNoiDung: {
                fontSize: this.props.fontSize.FontSizeNorman,
            },
            viewFile: {
                maxWidth: 250,
                height: 38,
                flexDirection: "row",
                alignItems: "center",
                margin: 2,
                padding: 2,
                borderRadius: 6,
                borderWidth: 0.5,
                borderColor: AppConfig.defaultLineColor
            },
            iconFile: {
                height: 25,
                width: 25
            },
            textTenFile: {
                maxWidth: 180,
                fontSize: this.props.fontSize.FontSizeNorman
            },
            viewNoiDung: {
                justifyContent: "center",
                alignContent: "center",
                flexWrap: "wrap",
                padding: 12,
            },
            viewPhanHoi_ChuyenTiep: {
                flexDirection: "row",
                justifyContent: "space-evenly",
                flexWrap: "wrap",
                alignItems: "center",
                marginLeft: 12,
                marginRight: 12,
                paddingTop: 6,
                paddingBottom: 6,
                borderTopWidth: AppConfig.defaultLineWidth,
                borderTopColor: AppConfig.defaultLineColor
            },
            btnPhanHoi: {
                marginTop: 3,
                marginBottom: 3,
                height: 35,
            },
            textPhanHoi: {
                color: "#000000",
                fontSize: this.props.fontSize.FontSizeSmall,
                fontWeight: "100",
            }
        })

        let title = this.state.danhSachChiTiet.length > 0 ? this.state.danhSachChiTiet[0].tieu_de : ""
        return (
            <Container style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
                <HeaderWithLeft title="Chi tiết thông tin điều hành" buttonName="arrow-back" onPressLeftButton={() => { this.props.navigation.navigate("TTDH") }} />
                <View style={this.styles.viewTieuDe}>
                    <Text style={this.styles.textTieuDe}>{RemoveNoiDungTTDH(title)}</Text>
                </View>
                <View style={{ flex: 1 }}>
                    <ScrollView
                        bounces={false}
                        removeClippedSubviews={true}
                        scrollEnabled={true}
                        style={this.styles.scrollContainer}
                    >
                        {this.renderDSChiTiet()}
                    </ScrollView>
                </View>
            </Container >
        );
    }
}

function mapStateToProps(state) {
    return {
        cb_dsctcb: state.cb_dsctcb.data,
        fontSize: state.fontSize,
    }
}

export default connect(mapStateToProps)(TTDH_Details)