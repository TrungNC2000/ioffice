import React, { Component } from 'react';
import { View, ScrollView, StyleSheet, TouchableOpacity, KeyboardAvoidingView, Platform, TextInput, Alert, Modal, FlatList, PixelRatio } from "react-native"
import { connect } from "react-redux"
import { Container, ActionSheet, CheckBox, Toast, Item, Left, Text, Button, Icon, Spinner } from "native-base"
import { StackActions } from "react-navigation"
import { AppConfig } from "../../AppConfig"
import HeaderWithLeftRight from "../../components/Header/HeaderWithLeftRight"
import API from "../../networks"
import { GetNowTime, ReplaceAll, HtmlEncode, RemoveHTMLTag, resetStack } from "../../untils/TextUntils"
import UploadFile from "../../components/UploadFile"

//Qui trình:
// Nếu là gửi: cho chọn loại, nhập tiêu đề, nhập nội dung, check sms, email.
// Nếu là chuyển tiếp: lấy thông tin loại, tiêu để, nội dung. Cho phép sửa loại, tiêu đề, nội dung; không sửa nội dung chuyển tiếp
// Nếu là phản hồi: lấy thông tin loại, tiêu để. Cho phép sửa loại, tiêu đề, nội dung
//Tham số đầu vào:
//tieuDe
//titleHeader
//noiDungChuyen
//ma_ttdh_kc
//ma_ttdh_goc
//ma_loai_ttdh_kc
//ten_loai_ttdh

class ModalNguoiNhan extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        this.styles = this.props.styles
        let { isModalVisible, listMCB_DVCB, onPressSend, toggleModal } = this.props
        return (
            <Modal
                transparent={false}
                animationType="slide"
                visible={isModalVisible}
                supportedOrientations={["landscape", "landscape-left", "landscape-right", "portrait", "portrait-upside-down"]}
            >
                <View style={{ flex: 1, padding: 15, backgroundColor: AppConfig.grayBackground }}>
                    <View style={{ flex: 1, backgroundColor: "#FFFFFF", borderRadius: 10 }}>
                        <View style={{ position: 'absolute', zIndex: 99, height: 55, left: 0, right: 0, top: 0, justifyContent: "center", padding: 4, alignItems: "center" }}>
                            <Text style={{ fontSize: this.props.fontSize.FontSizeLarge }}>Danh sách cán bộ nhận</Text>
                        </View>
                        <FlatList
                            ref={(ref) => { this.flatListRef = ref }}
                            data={listMCB_DVCB}
                            keyExtractor={(item, index) => "key" + index}
                            style={{ position: 'absolute', left: 0, right: 0, top: 55, bottom: 55 }}
                            initialNumToRender={50}
                            renderItem={({ item, index }) => {
                                return (
                                    <Item key={item.ma_ctcb_kc} style={{ padding: 12, borderBottomWidth: AppConfig.defaultLineWidth, borderColor: AppConfig.defaultLineColor }}>
                                        <Left>
                                            <Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>{item.ho_va_ten_can_bo}</Text>
                                        </Left>
                                    </Item>
                                )
                            }}
                            ListEmptyComponent={() => {
                                return (
                                    <View style={{ alignItems: "center", marginTop: 10, }}>
                                        <Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>Không có dữ liệu</Text>
                                    </View>
                                )
                            }}
                            removeClippedSubviews={true}
                        />
                        {
                            (this.props.isSending) ? (
                                <View style={{ position: 'absolute', zIndex: 99, height: 55, left: 0, right: 0, bottom: 0, flexDirection: "row", justifyContent: "center", paddingTop: 10, alignItems: "center" }}>
                                    <Spinner />
                                    <Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}> Đang thực hiện</Text>
                                </View>
                            ) : (
                                    <View style={{ position: 'absolute', zIndex: 99, height: 55, left: 0, right: 0, bottom: 0, flexDirection: "row", justifyContent: "space-evenly", paddingTop: 10, alignItems: "center" }}>
                                        <Button small bordered rounded iconLeft onPress={onPressSend}><Icon name="done" type="MaterialIcons"></Icon><Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>Gửi</Text></Button>
                                        <Button small danger bordered rounded iconLeft onPress={toggleModal}><Icon name="close" type="MaterialIcons"></Icon><Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>Đóng</Text></Button>
                                    </View>
                                )
                        }
                    </View>
                </View>
            </Modal>
        )
    }
}

class TTDH_Gui extends Component {
    constructor(props) {
        super(props)
        this.state = {
            listLoaiThongDiep: [],
            ten_loai_ttdh: "Chọn loại thông tin điều hành",
            newValue: "",
            height: 40,
            isModalVisible: false,

            ma_loai_ttdh_kc: -1,
            tieuDe: "",
            noiDung: "",
            dataFile: [],
            checkSms: false,
            checkEmail: false,
            isSending: false
        }
        this.titleHeader = "Gửi thông tin điều hành"
        this.noiDungChuyen = ""

        //Tham số mặc định gửi ttdh
        this.ma_ttdh_kc = 0
        this.trang_thai_ttdh = 1
        this.tra_loi_cho_ttdh = 0
        this.chuyen_tiep_tu_ttdh = 0
        this.ma_ttdh_goc = 0
        this.src_file_ttdh = ""
        this.listNguoiNhan = []

        this.isForward = false
        this.isReply = false
    }

    componentDidMount = () => {
        const isForward = this.props.navigation.getParam("isForward", false)
        const isReply = this.props.navigation.getParam("isReply", false)

        if (isForward || isReply) {
            this.isForward = isForward
            this.isReply = isReply
            const tieuDe = this.props.navigation.getParam("tieuDe", "")
            this.titleHeader = this.props.navigation.getParam("titleHeader", "Gửi thông tin điều hành")
            this.noiDungChuyen = this.props.navigation.getParam("noiDungChuyen", "")
            this.src_file_ttdh = this.props.navigation.getParam("src_file_ttdh", "")
            if (isForward) {
                this.ma_ttdh_kc = 0
                this.trang_thai_ttdh = 1
                this.tra_loi_cho_ttdh = 0
                this.chuyen_tiep_tu_ttdh = this.props.navigation.getParam("ma_ttdh_kc", 0)
                this.ma_ttdh_goc = this.props.navigation.getParam("ma_ttdh_goc", 0)
            } else if (isReply) {
                this.ma_ttdh_kc = 0
                this.trang_thai_ttdh = 1
                this.tra_loi_cho_ttdh = this.props.navigation.getParam("ma_ttdh_kc", 0)
                this.chuyen_tiep_tu_ttdh = 0
                this.ma_ttdh_goc = this.props.navigation.getParam("ma_ttdh_goc", 0)
                this.listNguoiNhan = this.props.navigation.getParam("listNguoiNhan", [])
            }

            this.setState({ tieuDe })
            setTimeout(() => {
                this.txtNoiDung.focus()
            }, 800);
        } else {
            setTimeout(() => {
                this.txtTieuDe.focus()
            }, 800);
        }
        this.getLoadThongDiep()
    }

    updateSize = (height) => {
        this.setState({ height: height })
        setTimeout(() => {
            this.ScrollView.scrollToEnd()
        }, 10);
    }

    getLoadThongDiep = () => {
        API.ThongTinDieuHanh.AU_TTDH_LDSLTD().then((listLoaiThongDiep) => {
            if (listLoaiThongDiep.length > 0) {
                listLoaiThongDiep = listLoaiThongDiep.filter(el => el.trang_thai_loai_ttdh == 1)
                const ma_loai_ttdh_kc = this.props.navigation.getParam("ma_loai_ttdh_kc", -1)
                const ten_loai_ttdh = this.props.navigation.getParam("ten_loai_ttdh", "Chọn loại thông tin điều hành")
                if (ma_loai_ttdh_kc !== -1) {
                    this.setState({
                        listLoaiThongDiep,
                        ten_loai_ttdh: ten_loai_ttdh,
                        ma_loai_ttdh_kc: ma_loai_ttdh_kc
                    })
                } else {
                    this.setState({
                        listLoaiThongDiep,
                        ten_loai_ttdh: listLoaiThongDiep[0].ten_loai_ttdh,
                        ma_loai_ttdh_kc: listLoaiThongDiep[0].ma_loai_ttdh_kc
                    })
                }
            }
        })
    }

    showLoaiThongDiep = () => {
        let listTenLoaiThongDiep = []
        listTenLoaiThongDiep.push("Đóng")
        for (let item of this.state.listLoaiThongDiep) {
            listTenLoaiThongDiep.push(item.ten_loai_ttdh)
        }
        ActionSheet.show(
            {
                options: listTenLoaiThongDiep,
                cancelButtonIndex: 0,
                title: "Chọn loại thông tin điều hành"
            },
            buttonIndex => {
                if (buttonIndex != 0) {
                    this.setState({
                        ten_loai_ttdh: this.state.listLoaiThongDiep[buttonIndex - 1].ten_loai_ttdh,
                        ma_loai_ttdh_kc: this.state.listLoaiThongDiep[buttonIndex - 1].ma_loai_ttdh_kc
                    })
                }
            }
        )
    }

    setFocus = () => {
        this.txtNoiDung.focus()
    }

    onCheckSms = () => {
        this.setState({ checkSms: !this.state.checkSms })
    }

    onCheckEmail = () => {
        this.setState({ checkEmail: !this.state.checkEmail })
    }

    onPressBack = () => {
        const popAction = StackActions.pop({ n: 1 })
        this.props.navigation.dispatch(popAction)
        return false

        if (this.state.ma_loai_ttdh_kc !== -1 && this.state.tieuDe !== "" && this.state.noiDung !== "") {
            Alert.alert(
                "Thông báo",
                "Lưu nháp thông tin điều hành ?",
                [

                    { text: "Đóng", style: "cancel" },
                    { text: "Có", onPress: () => { this.onSaveDraft() } },
                    {
                        text: "Không", onPress: () => {
                            const popAction = StackActions.pop({ n: 1 })
                            this.props.navigation.dispatch(popAction)
                        }, style: "destructive"
                    },
                ],
                { cancelable: false }
            )
        } else {
            const popAction = StackActions.pop({ n: 1 })
            this.props.navigation.dispatch(popAction)
        }
    }

    onPressSend = () => {
        if (this.state.ma_loai_ttdh_kc !== -1 && this.state.tieuDe !== "" && this.state.noiDung !== "") {
            if (this.isReply) {
                this.toggleModal()
            } else {
                this.props.navigation.navigate("TTDH_Gui1",
                    {
                        isForward: this.isForward,
                        ma_ttdh_kc: this.ma_ttdh_kc,
                        trang_thai_ttdh: this.trang_thai_ttdh,
                        tra_loi_cho_ttdh: this.tra_loi_cho_ttdh,
                        chuyen_tiep_tu_ttdh: this.chuyen_tiep_tu_ttdh,
                        ma_ttdh_goc: this.ma_ttdh_goc,
                        ma_loai_ttdh: this.state.ma_loai_ttdh_kc,
                        tieu_de: this.state.tieuDe,
                        noi_dung: this.state.noiDung,
                        src_file_ttdh: this.UploadFile.getListFileUpload(),
                        noi_dung_chuyen: this.noiDungChuyen,
                        sms: this.state.checkSms,
                        email: this.state.checkEmail
                    }
                )
            }
        } else {
            Toast.show({ text: "Vui lòng nhập đầy đủ thông tin!", type: "warning", buttonText: "Đóng", position: "top", duration: 2000 })
        }
    }

    toggleModal = () => {
        if (!this.state.isModalVisible) {
            if (this.listNguoiNhan.length > 0) {
                this.setState({ isModalVisible: true })
            } else {
                Toast.show({ text: "Lấy thông tin cán bộ nhận thất bại!", type: "warning", buttonText: "Đóng", position: "top", duration: 2000 })
            }
        } else {
            this.setState({ isModalVisible: false })
        }
    }

    onPressSendReply = () => {
        const listMCB_DVCB = this.listNguoiNhan.map(obj => {
            return obj.ma_ctcb_kc
        })
        const ma_ttdh_kc = this.ma_ttdh_kc
        const ma_loai_ttdh = this.state.ma_loai_ttdh_kc
        const ma_ctcb_tao = this.props.cb_dsctcb.ma_ctcb_kc
        const tieu_de = this.state.tieuDe
        let noi_dung = this.state.noiDung
        noi_dung = HtmlEncode(noi_dung)
        const ngay_gui = GetNowTime()
        const src_file_ttdh = this.UploadFile.getListFileUpload()
        const trang_thai_ttdh = this.trang_thai_ttdh
        const tra_loi_cho_ttdh = this.tra_loi_cho_ttdh
        const chuyen_tiep_tu_ttdh = this.chuyen_tiep_tu_ttdh
        const chuoi_ma_ctcb_nhan = ReplaceAll(listMCB_DVCB.join(";"), "CB", "")
        const sms = this.state.checkSms ? 1 : 0
        const ma_ttdh_goc = this.ma_ttdh_goc
        this.setState({ isSending: true })
        API.ThongTinDieuHanh.AU_TTDH_GTD(ma_ttdh_kc, ma_loai_ttdh, ma_ctcb_tao, tieu_de, noi_dung, ngay_gui, src_file_ttdh, trang_thai_ttdh, tra_loi_cho_ttdh, chuyen_tiep_tu_ttdh, chuoi_ma_ctcb_nhan, sms, ma_ttdh_goc).then(response => {
            if (response !== "") {
                if (sms === 1) {
                    let arrSDT = []
                    for (let item of this.listNguoiNhan) {
                        if (item.di_dong_can_bo) {
                            arrSDT.push(item.di_dong_can_bo)
                        }
                    }
                    if (arrSDT.length > 0) {
                        let noi_dung = this.state.tieuDe
                        let chuoi_di_dong_nhan = arrSDT.join(",")
                        API.NhanTinSMS.AU_SMS_GTN(noi_dung, chuoi_di_dong_nhan)
                    }
                }
                API.Login.AU_PUSH_FCM_APP(listMCB_DVCB.join(","), "thongtindieuhanhdanhan", this.state.tieuDe, response, 0, 0, 0)
                setTimeout(() => {
                    this.props.navigation.dispatch(resetStack("TTDH"))
                }, 100);
            } else {
                Alert.alert("Thông báo", "Xảy ra lỗi trong quá trình gửi!")
                this.setState({ isSending: false })
            }
        })
    }

    render() {

        this.styles = StyleSheet.create({
            content: {
                flex: 1,
                marginTop: 20,
                //paddingBottom: 20,
                paddingLeft: 20,
                paddingRight: 20
            },
            viewItem: {
                paddingTop: 10,
                paddingBottom: 10
            },
            viewItem1: {
                borderColor: AppConfig.defaultLineColor,
                borderBottomWidth: AppConfig.defaultLineWidth,
            },
            labelTieuDe: {
                fontFamily: AppConfig.fontFamily,
                color: AppConfig.blueBackground,
                fontSize: this.props.fontSize.FontSizeSmall
            },
            txtLoaiThongDiep: {
                fontFamily: AppConfig.fontFamily,
                color: "#000000",
                fontSize: this.props.fontSize.FontSizeNorman,
                borderColor: AppConfig.defaultLineColor,
                borderBottomWidth: AppConfig.defaultLineWidth,
                paddingTop: 10,
                paddingBottom: 10
            },
            txtTieuDe: {
                height: 40,
                fontFamily: AppConfig.fontFamily,
                color: "#000000",
                fontSize: this.props.fontSize.FontSizeNorman / PixelRatio.getFontScale(),
                borderColor: AppConfig.defaultLineColor,
                borderBottomWidth: AppConfig.defaultLineWidth,
            },
            txtNoiDung: {
                height: 40,
                fontFamily: AppConfig.fontFamily,
                color: "#000000",
                fontSize: this.props.fontSize.FontSizeNorman,
                borderColor: AppConfig.defaultLineColor,
                borderBottomWidth: AppConfig.defaultLineWidth,
            }
        })

        return (
            <Container style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
                <HeaderWithLeftRight
                    title={this.titleHeader}
                    buttonLeftName="arrow-back"
                    buttonRightName="send"
                    onPressLeftButton={_ => this.onPressBack()}
                    onPressRightButton={_ => this.onPressSend()}
                />
                <View style={this.styles.content}>
                    {
                        (this.state.isModalVisible) && (
                            <ModalNguoiNhan
                                listMCB_DVCB={this.listNguoiNhan}
                                isModalVisible={this.state.isModalVisible}
                                toggleModal={this.toggleModal}
                                onPressSend={this.onPressSendReply}
                                isSending={this.state.isSending}
                                fontSize={this.props.fontSize}
                                styles={this.styles}
                            />
                        )
                    }
                    <KeyboardAvoidingView enabled behavior={Platform.OS === "ios" ? "padding" : null} keyboardVerticalOffset={104}>
                        <ScrollView ref={ref => { this.ScrollView = ref }} bounces={false}>
                            <View style={this.styles.viewItem1}>
                                <TouchableOpacity onPress={() => this.showLoaiThongDiep()}>
                                    <Text style={this.styles.labelTieuDe}>Loại thông tin</Text>
                                    <Text editable={false} style={this.styles.txtLoaiThongDiep}>{this.state.ten_loai_ttdh}</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={this.styles.viewItem}>
                                <Text style={this.styles.labelTieuDe}>Tiêu đề</Text>
                                <TextInput
                                    ref={input => { this.txtTieuDe = input }}
                                    multiline={false}
                                    style={this.styles.txtTieuDe}
                                    autoCapitalize="none"
                                    returnKeyType={"done"}
                                    value={this.state.tieuDe}
                                    onChangeText={(text) => this.setState({ tieuDe: text })}
                                    onSubmitEditing={() => { this.txtNoiDung.focus() }}
                                />
                            </View>
                            {
                                (this.isForward) && (
                                    <View style={this.styles.viewItem}>
                                        <Text style={this.styles.labelTieuDe}>Nội dung {this.titleHeader.toLocaleLowerCase()}:</Text>
                                        <Text style={this.styles.txtNoiDung}>{RemoveHTMLTag(this.noiDungChuyen)}</Text>
                                    </View>
                                )
                            }
                            <View style={this.styles.viewItem}>
                                <Text style={this.styles.labelTieuDe}>Nội dung</Text>
                                <TextInput
                                    ref={input => { this.txtNoiDung = input }}
                                    autoCapitalize="none"
                                    style={this.styles.txtTieuDe}
                                    value={this.state.noiDung}
                                    onChangeText={(text) => this.setState({ noiDung: text })}
                                    multiline={true}
                                    onContentSizeChange={(e) => this.updateSize(e.nativeEvent.contentSize.height)}
                                />
                            </View>
                            <View style={this.styles.viewItem}>
                                <UploadFile
                                    ref={(upload) => { this.UploadFile = upload }}
                                    chuc_nang="thongtindieuhanh"
                                    sourceFiles={this.src_file_ttdh && !this.isReply ? this.src_file_ttdh.split(":") : []}
                                    navigation={this.props.navigation}
                                    fontSize={this.props.fontSize}
                                />
                            </View>
                            <View style={[this.styles.viewItem, { flexDirection: "row", }]}>
                                <View style={[{ flexDirection: "row", alignItems: 'center', justifyContent: "flex-start" }]}>
                                    <CheckBox onPress={this.onCheckSms} checked={this.state.checkSms} color={AppConfig.blueBackground}></CheckBox>
                                    <Text style={this.styles.labelTieuDe}>    Sms      </Text>
                                </View>
                            </View>
                        </ScrollView>
                    </KeyboardAvoidingView>
                </View>
            </Container>
        );
    }
}

function mapStateToProps(state) {
    return {
        cb_dsctcb: state.cb_dsctcb.data,
        fontSize: state.fontSize,
    }
}

export default connect(mapStateToProps)(TTDH_Gui)