import React, { Component } from 'react';
import { Alert, View, StyleSheet, Dimensions, Modal, FlatList } from "react-native"
import { Container, Text, Card, Tab, Tabs, Toast, Left, Spinner, Item, Button, Icon, } from "native-base"
import { connect } from "react-redux"
import { StackActions } from "react-navigation"
import { AppConfig } from "../../AppConfig"
import HeaderWithLeftRight from "../../components/Header/HeaderWithLeftRight"
import TreeChuyenCDTT from "../../components/treeChuyenCDTT"
import TreeChuyenNCB from "../../components/treeChuyenNCB"
import API from "../../networks"
import { mergeArrayDSChuyen, GetNowTime, HtmlEncode, resetStack, listToTree } from "../../untils/TextUntils"

class ModalNguoiNhan extends Component {
    render() {
        this.styles = this.props.styles
        return (
            <Modal
                transparent={false}
                animationType="slide"
                visible={this.props.isModalVisible}
                supportedOrientations={["landscape", "landscape-left", "landscape-right", "portrait", "portrait-upside-down"]}
            >
                <View style={{ flex: 1, padding: 15, backgroundColor: AppConfig.grayBackground }}>
                    <View style={{ flex: 1, backgroundColor: "#FFFFFF", borderRadius: 10 }}>
                        <View style={{ position: 'absolute', zIndex: 99, height: 55, left: 0, right: 0, top: 0, justifyContent: "center", padding: 4, alignItems: "center" }}>
                            <Text style={{ fontSize: this.props.fontSize.FontSizeLarge }}>Danh sách cán bộ nhận</Text>
                        </View>
                        <FlatList
                            ref={(ref) => { this.flatListRef = ref }}
                            data={this.props.listMCB_DVCB}
                            keyExtractor={(item, index) => "key" + item.ma + index}
                            style={{ position: 'absolute', left: 0, right: 0, top: 55, bottom: 55 }}
                            initialNumToRender={50}
                            renderItem={({ item, index }) => {
                                return (
                                    <Item key={item.ma} style={{ padding: 12, borderBottomWidth: AppConfig.defaultLineWidth, borderColor: AppConfig.defaultLineColor }}>
                                        <Left>
                                            <Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>{item.ten}</Text>
                                            <Text note style={{ fontSize: this.props.fontSize.FontSizeSmall }}>Chức vụ: {item.ten_chuc_vu}</Text>
                                        </Left>
                                    </Item>
                                )
                            }}
                            ListEmptyComponent={() => {
                                return (
                                    <View style={{ alignItems: "center", marginTop: 10, }}>
                                        <Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>Không có dữ liệu</Text>
                                    </View>
                                )
                            }}
                            removeClippedSubviews={true}
                        />
                        {
                            (this.props.isSending) ? (
                                <View style={{ position: 'absolute', zIndex: 99, height: 55, left: 0, right: 0, bottom: 0, flexDirection: "row", justifyContent: "center", paddingTop: 10, alignItems: "center" }}>
                                    <Spinner />
                                    <Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}> Đang thực hiện</Text>
                                </View>
                            ) : (
                                    <View style={{ position: 'absolute', zIndex: 99, height: 55, left: 0, right: 0, bottom: 0, flexDirection: "row", justifyContent: "space-evenly", paddingTop: 10, alignItems: "center" }}>
                                        <Button small bordered rounded iconLeft onPress={_ => this.props.onPressSend()}><Icon name="done" type="MaterialIcons"></Icon><Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>Gửi</Text></Button>
                                        <Button small danger bordered rounded iconLeft onPress={_ => this.props.toggleModal()}><Icon name="close" type="MaterialIcons"></Icon><Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>Chọn lại</Text></Button>
                                    </View>
                                )
                        }
                    </View>
                </View>
            </Modal>
        )
    }
}

class TTDH_Gui1 extends Component {
    constructor(props) {
        super(props)
        this.state = {
            activeTab: 0,
            isModalVisible: false,
            dataTreeDVCB: [],
            dataTreeNCB: [],
            selectedItem: [],
            isLoading: true,
            isSending: false,
        }
        this.ma_loai_ttdh = 1
        this.tieu_de = ""
        this.noi_dung = ""
        this.noi_dung_chuyen = ""
        this.sms = ""
        this.email = ""
        this.isForward = false

        //Tham số mặc định gửi ttdh
        this.ma_ttdh_kc = 0
        this.trang_thai_ttdh = 1
        this.tra_loi_cho_ttdh = 0
        this.chuyen_tiep_tu_ttdh = 0
        this.ma_ttdh_goc = 0
        this.src_file_ttdh = ""
        Dimensions.addEventListener("change", this.onResizeScreen)
        this.ngay_gui = ""
        this.chuoi_ma_ctcb_nhan = ""
        this.listMCB_DVCB = []
    }

    componentDidMount() {
        this.ma_loai_ttdh = this.props.navigation.getParam("ma_loai_ttdh", 1)
        this.tieu_de = this.props.navigation.getParam("tieu_de", "")
        this.noi_dung = this.props.navigation.getParam("noi_dung", "")
        this.noi_dung_chuyen = this.props.navigation.getParam("noi_dung_chuyen", "")
        this.sms = this.props.navigation.getParam("sms", false)
        this.email = this.props.navigation.getParam("email", false)
        this.isForward = this.props.navigation.getParam("isForward", false)
        this.ma_ttdh_kc = this.props.navigation.getParam("ma_ttdh_kc", 0)
        this.trang_thai_ttdh = this.props.navigation.getParam("trang_thai_ttdh", 1)
        this.tra_loi_cho_ttdh = this.props.navigation.getParam("tra_loi_cho_ttdh", 0)
        this.chuyen_tiep_tu_ttdh = this.props.navigation.getParam("chuyen_tiep_tu_ttdh", 0)
        this.ma_ttdh_goc = this.props.navigation.getParam("ma_ttdh_goc", 0)
        this.src_file_ttdh = this.props.navigation.getParam("src_file_ttdh", "")

        const ma_don_vi = global.ma_don_vi
        const ma_can_bo = global.ma_ctcb_kc
        this.getDSDVCB(ma_don_vi, ma_can_bo)
        this.getDSNCB()
    }

    getDSDVCB = (ma_don_vi, ma_can_bo) => {
        API.ThongTinDieuHanh.AU_TTDH_DSCBNTTDH(ma_don_vi, ma_can_bo).then((dataTreeDVCB) => {
            this.setState({ dataTreeDVCB, isLoading: false })
        })
    }

    getDSNCB = async () => {
        const ma_can_bo = global.ma_can_bo
        await API.ThongTinDieuHanh.AU_TTDH_DSNCBNTTDH(ma_can_bo).then((dataTreeNCB) => {
            let tree = listToTree(dataTreeNCB, {
                idKey: "id",
                parentKey: "parent"
            })
            console.log("tree 0: " + tree)
            this.setState({ dataTreeNCB: [...this.state.dataTreeNCB, ...tree] })
        })

        await API.ThongTinDieuHanh.AU_TTDH_DSNCBNTTDHLT(ma_can_bo).then((dataTreeNCB) => {
            let tree = listToTree(dataTreeNCB, {
                idKey: "id",
                parentKey: "parent"
            })
            console.log("tree 1: " + tree)
            this.setState({ dataTreeNCB: [...this.state.dataTreeNCB, ...tree] })
        })
    }

    componentWillUnmount() {
        Dimensions.removeEventListener("change", this.onResizeScreen)
    }

    onResizeScreen = () => {
        setTimeout(() => {
            this.hasChangeTab = false
        }, 500);
    }

    toggleModal = () => {
        this.listMCB_DVCB = []
        if (!this.state.isModalVisible) {
            let listTab1 = []
            let listTab2 = []

            try {
                listTab1 = this.treeChuyenCDTT.getDataCanBo_XLC_PH()
                listTab1 = listTab1.map((element) => {
                    element.ma = element.ma.replace("CB", "")
                    return element
                });
            } catch (error) {
                console.log("Error treeChuyenCDTT.getDataCanBo_XLC_PH: " + error)
            }

            try {
                listTab2 = this.treeChuyenNCB.getDataCanBo_XLC_PH()
                listTab2 = listTab2.map((element) => {
                    element.ma = element.ma.replace("CB", "")
                    return element
                });
            } catch (error) {
                console.log("Error treeChuyenNCB.getDataCanBo_XLC_PH: " + error)
            }

            this.listMCB_DVCB = mergeArrayDSChuyen(listTab1, listTab2)
            if (this.listMCB_DVCB.length > 0) {
                this.setState({ isModalVisible: true })
            } else {
                Toast.show({ text: "Vui lòng chọn cán bộ nhận!", type: "warning", buttonText: "Đóng", position: "top", duration: 2000 })
            }
        } else {
            this.setState({ isModalVisible: false })
        }
    }

    onPressSend = () => {
        let listMCB_DVCB = this.listMCB_DVCB.map((element) => {
            return element.ma
        });

        const ma_ttdh_kc = this.ma_ttdh_kc
        const ma_loai_ttdh = this.ma_loai_ttdh
        const ma_ctcb_tao = global.ma_ctcb_kc
        const tieu_de = this.tieu_de
        let noi_dung = !this.isForward ? this.noi_dung : this.noi_dung + "<br><br>- Chuyển tiếp từ:<br>" + this.noi_dung_chuyen
        noi_dung = HtmlEncode(noi_dung)
        const ngay_gui = GetNowTime()
        const src_file_ttdh = this.src_file_ttdh
        const trang_thai_ttdh = this.trang_thai_ttdh
        const tra_loi_cho_ttdh = this.tra_loi_cho_ttdh
        const chuyen_tiep_tu_ttdh = this.chuyen_tiep_tu_ttdh
        const chuoi_ma_ctcb_nhan = listMCB_DVCB.join(";").toString()
        const sms = this.sms ? 1 : 0
        const ma_ttdh_goc = this.ma_ttdh_goc
        this.setState({ isSending: true })
        API.ThongTinDieuHanh.AU_TTDH_GTD(ma_ttdh_kc, ma_loai_ttdh, ma_ctcb_tao, tieu_de, noi_dung, ngay_gui, src_file_ttdh, trang_thai_ttdh, tra_loi_cho_ttdh, chuyen_tiep_tu_ttdh, chuoi_ma_ctcb_nhan, sms, ma_ttdh_goc).then(response => {
            if (response !== "") {
                API.Login.AU_DSNVCB(global.ma_ctcb_kc, this.props.dispatch)
                if (sms === 1) {
                    let arrSDT = []
                    for (let item of this.listMCB_DVCB) {
                        if (item.di_dong_can_bo) {
                            arrSDT.push(item.di_dong_can_bo)
                        }
                    }
                    if (arrSDT.length > 0) {
                        let noi_dung = this.tieu_de
                        let chuoi_di_dong_nhan = arrSDT.join(",")
                        API.NhanTinSMS.AU_SMS_GTN(noi_dung, chuoi_di_dong_nhan)
                    }
                }
                API.Login.AU_PUSH_FCM_APP(listMCB_DVCB.join(","), "thongtindieuhanhdanhan", this.tieu_de, response, 0, 0, 0)
                setTimeout(() => {
                    this.props.navigation.dispatch(resetStack("TTDH"))
                }, 100);
            } else {
                Alert.alert("Thông báo", "Xảy ra lỗi trong quá trình gửi!")
                this.setState({ isSending: false })
            }
        })
    }

    render() {

        this.styles = StyleSheet.create({
            tabStyle: {
                flex: 1,
                backgroundColor: AppConfig.blueBackground
            },
            activeTabStyle: {
                backgroundColor: AppConfig.blueBackground
            },
            textStyle: {
                fontSize: this.props.fontSize.FontSizeNorman,
                color: "#FFFFFF",
                opacity: 0.6,
            },
            activeTextStyle: {
                fontSize: this.props.fontSize.FontSizeNorman,
                color: "#FFFFFF",
                opacity: 1,
            }
        })

        const popAction = StackActions.pop({ n: 1 })
        return (
            <Container style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
                <HeaderWithLeftRight
                    title="Gửi thông tin điều hành"
                    buttonLeftName="arrow-back"
                    buttonRightName="send"
                    onPressLeftButton={() => { this.props.navigation.dispatch(popAction) }}
                    onPressRightButton={this.toggleModal}
                />
                {
                    (this.state.isModalVisible) && (
                        <ModalNguoiNhan
                            listMCB_DVCB={this.listMCB_DVCB}
                            isModalVisible={this.state.isModalVisible}
                            toggleModal={this.toggleModal}
                            onPressSend={this.onPressSend}
                            isSending={this.state.isSending}
                            fontSize={this.props.fontSize}
                            styles={this.styles}
                        />
                    )
                }
                <Tabs onChangeTab={() => { this.hasChangeTab = true }}>
                    <Tab
                        tabStyle={this.styles.tabStyle}
                        activeTabStyle={this.styles.activeTabStyle}
                        textStyle={this.styles.textStyle}
                        activeTextStyle={this.styles.activeTextStyle}
                        heading="Đơn vị/Cán bộ"
                    >
                        <Card noShadow style={{ flex: 1, borderWidth: 1 }}>
                            {
                                (this.state.isLoading) && (
                                    <Spinner />
                                )
                            }
                            {
                                (!this.state.isLoading) && (
                                    <TreeChuyenCDTT
                                        ref={tree => { this.treeChuyenCDTT = tree }}
                                        title="Họ tên"
                                        firstChkTitle="Chọn"
                                        secondChkTitle="PH"
                                        threeChkTitle="XLC"
                                        numberSelection={1}
                                        data={this.state.dataTreeDVCB}
                                        primaryCheck={1}
                                        onItemPress={(ma, once) => { console.log("press item: " + ma) }}
                                        fontSize={this.props.fontSize}
                                    />
                                )
                            }
                        </Card>
                    </Tab>
                    <Tab
                        tabStyle={this.styles.tabStyle}
                        activeTabStyle={this.styles.activeTabStyle}
                        textStyle={this.styles.textStyle}
                        activeTextStyle={this.styles.activeTextStyle}
                        heading="Nhóm cán bộ"
                    >
                        <Card noShadow style={{ flex: 1, borderWidth: 1 }}>
                            <TreeChuyenNCB
                                ref={treeNCB => { this.treeChuyenNCB = treeNCB }}
                                title="Họ tên"
                                firstChkTitle="Chọn"
                                secondChkTitle="PH"
                                threeChkTitle="XLC"
                                numberSelection={1}
                                data={this.state.dataTreeNCB}
                                primaryCheck={1}
                                onItemPress={(ma, once) => { console.log("press item: " + ma) }}
                                fontSize={this.props.fontSize}
                            />
                        </Card>
                    </Tab>
                </Tabs>
            </Container>
        );
    }
}

function mapStateToProps(state) {
    return {
        fontSize: state.fontSize,
    }
}

export default connect(mapStateToProps)(TTDH_Gui1)