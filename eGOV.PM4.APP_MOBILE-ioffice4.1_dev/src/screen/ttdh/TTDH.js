import React, { Component, PureComponent } from 'react';
import { View, StyleSheet, FlatList, Alert, TouchableOpacity, Platform } from "react-native"
import { Container, Text, CardItem, Spinner, Toast } from "native-base"
import { connect } from "react-redux"
import { DrawerActions } from "react-navigation"
import { Menu, MenuOptions, MenuOption, MenuTrigger } from 'react-native-popup-menu'
import Icon from "react-native-vector-icons/FontAwesome5"
import HeaderWithLeftRight from "../../components/Header/HeaderWithLeftRight"
import FooterTab from "../../components/Footer/FooterTab"
import SearchHeader from "../../components/SearchHeader"
import MyTab from "../../components/Tab"
import API from "../../networks"
import ModalListFile from "../../components/ModalListFile"
import ModalWebView from "../../components/ModalWebView"
import { AppConfig } from "../../AppConfig"
import { CheckSession } from "../../untils/NetInfo"
import { getFileChiTietTTDH, RemoveNoiDungTTDH, ConvertDateTime, ConvertDateTimeDetail, ToastSuccess, checkVersionWeb, getNVOld, getNVNew } from "../../untils/TextUntils"
import { downloadFileToView } from "../../untils/DownloadFile"
import TTDH_Details_iPad from "./TTDH_Details_iPad"
const nowDate = new Date()
class TTDHTab_Item extends PureComponent {
    constructor(props) {
        super(props)
        this.state = {
            isModalVisible: false,
            isBold: this.props.item.trang_thai_ttdh_gui === 0 ? true : false
        }
    }

    onPressItem = () => {
        if (Platform.OS === "ios" && Platform.isPad) {
            if (this.state.isBold) {
                this.setState({ isBold: false })
            }
            this.props.onPress()
        } else {
            if (this.state.isBold) {
                this.setState({ isBold: false })
            }
            this.props.navigation.navigate("TTDH_Details", {
                ma_ttdh_kc: this.props.item.ma_ttdh_kc,
                ma_ttdh_gui_kc: this.props.item.ma_ttdh_gui_kc,
                tieu_de: this.props.item.tieu_de,
                loaiGuiNhan: this.props.activeTab === 0 ? "Nhận" : "gửi"
            })
        }
    }

    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible })
    }

    onViewFile = (data) => {
        let item = data.split("|")
        let urlFile = item[0]
        let fileName = item[1]
        let extFile = item[2]
        let path = item[4]
        if (urlFile !== "") {
            if (Platform.OS === "ios" && (extFile === "doc" || extFile === "docx")) {
                downloadFileToView(fileName, path)
            } else {
                if (global.US_Xem_Pdf_iOS === "Offline") {
                    downloadFileToView(fileName, path)
                    return null;
                }
                this.urlFile = urlFile
                this.path = path
                this.fileName = fileName
                this.toggleModal()
            }
        } else {
            downloadFileToView(fileName, path)
        }
    }

    updateViewCV = async () => {

    }

    render() {
        this.styles = this.props.styles
        const { item, activeTab } = this.props
        let dataFile = item.src_file_ttdh !== null && item.src_file_ttdh !== "null" ? getFileChiTietTTDH(item.src_file_ttdh.split(":")) : []
        let styleTemp = this.styles.textNoBold
        if (this.state.isBold) {
            styleTemp = this.styles.textBold
        }
        const ngay = activeTab === 0 ? item.ngay_nhan : item.ngay_gui
        let noiDung = RemoveNoiDungTTDH(item.noi_dung)
        noiDung = noiDung ? noiDung.replace("\n", " ") : "Không có nội dung"
        const minHeight = (activeTab === 0 || dataFile.length > 0) ? 85 : 70

        let borderRightWidth = (item.ma_ttdh_kc === this.props.cur_ma_ttdh_kc) ? 2.5 : 0.8
        let borderRightColor = (item.ma_ttdh_kc === this.props.cur_ma_ttdh_kc) ? "red" : "lightgray"

        if (Platform.OS === "ios" && Platform.isPad) {
            return (
                <CardItem style={[this.styles.CardItem, { borderRightWidth, borderRightColor, paddingTop: 6, paddingBottom: 6, }]} key={item.ma_ttdh_kc}>
                    <TouchableOpacity style={{ justifyContent: 'center', flex: 1 }} onPress={this.onPressItem}>
                        {
                            (activeTab === 0) && (
                                <Text numberOfLines={2} ellipsizeMode="tail" style={[styleTemp]}>{item.ten_nguoi_gui} </Text>
                            )
                        }
                        <Text numberOfLines={2} ellipsizeMode="tail" style={[styleTemp]}>Tiêu đề: {item.tieu_de.trim()} </Text>
                        <Text numberOfLines={1} ellipsizeMode="tail" style={[styleTemp, { color: "#808080" }]}>Nội dung: {noiDung} </Text>
                        <Text numberOfLines={1} ellipsizeMode="tail" style={[styleTemp]}>Ngày: {ConvertDateTimeDetail(ngay)}  </Text>
                    </TouchableOpacity>
                </CardItem>
            )
        } else {
            return (
                <CardItem key={item.ma_ttdh_kc} style={[this.styles.CardItem]}>
                    <TouchableOpacity style={{ justifyContent: 'center', flex: 1, left: 0, marginRight: 100, minHeight }} onPress={this.onPressItem}>
                        {
                            (activeTab === 0) && (
                                <Text numberOfLines={1} ellipsizeMode="tail" style={[styleTemp]}>{item.ten_nguoi_gui} </Text>
                            )
                        }
                        <Text numberOfLines={1} ellipsizeMode="tail" style={[styleTemp]}>{item.tieu_de.trim()} </Text>
                        <Text numberOfLines={1} ellipsizeMode="tail" style={[styleTemp, { color: "#808080" }]}>{noiDung} </Text>
                    </TouchableOpacity>
                    <View style={{ width: 100, top: 0, right: 0, bottom: 0, position: "absolute", alignItems: 'center', justifyContent: "center" }}>
                        <Text style={this.styles.txtNgayNhan}>{ConvertDateTime(ngay)} </Text>
                        {
                            (dataFile.length > 0) && (
                                <Icon name="paperclip" style={[this.styles.menuTriggerIcon, { fontSize: this.props.fontSize.FontSizeNorman, }]}></Icon>
                            )
                        }
                        <Menu>
                            <MenuTrigger style={this.styles.menuTrigger}>
                                <Icon name="ellipsis-v" style={this.styles.menuTriggerIcon}></Icon>
                            </MenuTrigger>
                            <MenuOptions optionsContainerStyle={this.styles.menuOptionsContainer}>
                                {
                                    (dataFile.length > 1 || (dataFile.length === 1 && dataFile[0] !== "")) && (
                                        <MenuOption onSelect={() => this.onViewFile(dataFile[0])} >
                                            <Text style={this.styles.menuOptionsText}>Xem tệp tin</Text>
                                        </MenuOption>
                                    )
                                }
                                <MenuOption onSelect={() => { onDelete(item.ma_ttdh_kc, activeTab, false) }} >
                                    <Text style={this.styles.menuOptionsTextDelete}>Xoá</Text>
                                </MenuOption>
                            </MenuOptions>
                        </Menu>
                    </View>
                    {
                        (dataFile.length === 1) && (
                            <ModalWebView
                                isModalVisible={this.state.isModalVisible}
                                title={dataFile[0].split("|")[1]}
                                url={dataFile[0].split("|")[0]}
                                path={dataFile[0].split("|")[4]}
                                toggleModal={() => this.toggleModal()}
                                isUpdateViewCV={this.state.isBold}
                                updateViewCV={() => this.updateViewCV()}
                            />
                        )
                    }
                    {
                        (dataFile.length > 1) && (
                            <ModalListFile
                                isModalVisible={this.state.isModalVisible}
                                dataFile={dataFile}
                                toggleModal={() => this.toggleModal()}
                                isUpdateViewCV={this.state.isBold}
                                updateViewCV={() => this.updateViewCV()}
                            />
                        )
                    }
                </CardItem>
            )
        }

    }
}

class TTDHScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            loading: true,
            refreshing: false,
            dataSource: [],
            dataSearch: [],
            keyWord: "",
            ma_ttdh_kc: 0,
            cur_item: {}
        }
        this.page = 1
        this.activeTab = 0
        this.nowYear = nowDate.getFullYear()
        this.stopLoadMore = false
        this.responseLength = 20
    }

    componentDidMount() {
        this.didBlurSubscription = this.props.navigation.addListener('willFocus', payload => { CheckSession(this.props.navigation) })
        this.activeTab = this.props.navigation.getParam("activeTab", 0)
        this.getListByTabIndex(this.activeTab)
        API.Login.AU_DSNVCB(global.ma_ctcb_kc, this.props.dispatch)
    }

    componentWillUnmount = () => {
        this.didBlurSubscription.remove()
    };

    shouldComponentUpdate = (nextProps, nextState) => {
        if (this.state.loading !== nextState.loading ||
            this.state.refreshing !== nextState.refreshing ||
            this.state.dataSource !== nextState.dataSource ||
            this.state.ma_ttdh_kc !== nextState.ma_ttdh_kc ||
            this.state.cur_item !== nextState.cur_item ||
            this.state.keyWord !== nextState.keyWord ||
            this.props.cb_dsnvcb !== nextProps.cb_dsnvcb) {
            return true
        }
        return false
    };

    getListByTabIndex = () => {
        let ma_can_bo = global.ma_can_bo
        let tabIndex = this.activeTab
        let page = this.page
        let size = AppConfig.pageSizeTTDHNhan
        let nam = this.nowYear
        let keyWord = this.searchHeader.getKeyWord()

        if (tabIndex === 0) {
            if (checkVersionWeb(23)) {
                API.ThongTinDieuHanh.AU_TTDH_DA_NHANV23(ma_can_bo, page, size, nam, keyWord).then(response => {
                    if (tabIndex === this.activeTab) {
                        this.setState({ dataSource: [...this.state.dataSource, ...response] }, () => {
                            this.setState({ loading: false, refreshing: false })
                        })
                        this.responseLength = response.length
                        this.stopLoadMore = false
                        if (this.responseLength === 0) {
                            this.loadMore()
                        }
                    }
                })
            } else {
                API.ThongTinDieuHanh.AU_TTDH_DA_NHAN(ma_can_bo, page, size, keyWord).then(response => {
                    if (tabIndex === this.activeTab) {
                        this.setState({ dataSource: page === 1 ? response : [...this.state.dataSource, ...response] }, () => {
                            this.setState({ loading: false, refreshing: false })
                        })
                    }
                })
            }
        } else {
            if (checkVersionWeb(23)) {
                API.ThongTinDieuHanh.AU_TTDH_DA_GUIV23(ma_can_bo, page, size, nam, keyWord, "").then(response => {
                    if (tabIndex === this.activeTab) {
                        this.setState({ dataSource: [...this.state.dataSource, ...response] }, () => {
                            this.setState({ loading: false, refreshing: false })
                        })
                        this.responseLength = response.length
                        this.stopLoadMore = false
                        if (this.responseLength === 0) {
                            this.loadMore()
                        }
                    }
                })
            } else {
                API.ThongTinDieuHanh.AU_TTDH_DA_GUI(ma_can_bo, page, size, keyWord, "").then(response => {
                    if (tabIndex === this.activeTab) {
                        this.setState({ dataSource: page === 1 ? response : [...this.state.dataSource, ...response] }, () => {
                            this.setState({ loading: false, refreshing: false })
                        })
                    }
                })
            }
        }
    }

    onChangeTab = (index) => {
        if (checkVersionWeb(23)) {
            if (!this.state.loading) {
                this.page = 1
                this.activeTab = index
                this.nowYear = nowDate.getFullYear()
                this.stopLoadMore = false
                this.searchHeader.setKeyWord("", () => {
                    this.setState({ dataSource: [], loading: true, ma_xu_ly_den: 0 })
                    this.getListByTabIndex()
                })
            }
        } else {
            if (!this.state.loading) {
                this.page = 1
                this.activeTab = index
                this.searchHeader.setKeyWord("", () => {
                    this.setState({ loading: true })
                    this.setState({ dataSource: [], loading: true, ma_ttdh_kc: 0 })
                    this.getListByTabIndex()
                })
            }
        }
    }

    onRefresh = () => {
        if (checkVersionWeb(23)) {
            if (!this.state.loading) {
                this.page = 1
                this.nowYear = nowDate.getFullYear()
                this.stopLoadMore = false
                this.setState({ dataSource: [], refreshing: true, loading: true, ma_ttdh_kc: 0 })
                this.getListByTabIndex()
            }
        } else {
            if (!this.state.loading) {
                this.page = 1
                this.setState({ dataSource: [], refreshing: true, loading: true, ma_ttdh_kc: 0 })
                this.getListByTabIndex()
            }
        }
    }

    loadMore = () => {
        if (checkVersionWeb(23)) {
            if (!this.state.loading && this.stopLoadMore === false) {
                if (this.nowYear > 2000) {
                    this.stopLoadMore = true
                    this.page = this.page + 1
                    if (this.responseLength === 0) {
                        this.page = 1
                        this.nowYear = this.nowYear - 1
                    }
                    this.getListByTabIndex()
                }
            }
        } else {
            if (!this.state.loading) {
                this.page = this.page + 1
                this.getListByTabIndex()
            }
        }
    }

    onSearchData = () => {
        if (checkVersionWeb(23)) {
            if (!this.state.loading) {
                this.nowYear = nowDate.getFullYear()
                this.page = 1
                this.stopLoadMore = false
                this.setState({ dataSource: [], loading: true, ma_ttdh_kc: 0 })
                this.getListByTabIndex()
            }
        } else {
            if (!this.state.loading) {
                this.page = 1
                this.setState({ dataSource: [], loading: true, ma_ttdh_kc: 0 })
                this.getListByTabIndex()
            }
        }
    }

    onDelete = (ma_ttdh_kc, activeTab, flagNavigate) => {
        Alert.alert("Xác nhận", "Xóa thông tin điều hành này?",
            [
                { text: "Không", style: "cancel" },
                {
                    text: "Đồng ý", onPress: () => {
                        if (activeTab === 0) {
                            API.ThongTinDieuHanh.AU_TTDH_ATTDHN(ma_ttdh_kc || "", global.ma_ctcb_kc).then((response) => {
                                if (response !== "") {
                                    ToastSuccess("Xóa thành công!", () => {
                                        let dataSource = this.state.dataSource.filter(el => el.ma_ttdh_kc !== ma_ttdh_kc)
                                        this.setState({ dataSource, ma_ttdh_kc: 0 })
                                        API.Login.AU_DSNVCB(global.ma_ctcb_kc, this.props.dispatch)
                                        if (flagNavigate) {
                                            this.props.navigation.navigate("TTDH")
                                        }
                                    })
                                } else {
                                    Toast.show({ text: "Xảy ra lỗi. Vui lòng thực hiện lại sau!", type: "danger", buttonText: "Đóng", position: "top", duration: 2000 })
                                }
                            })
                        } else {
                            API.ThongTinDieuHanh.AU_TTDH_CNTTDHDG(ma_ttdh_kc || "", 2).then((response) => {
                                if (response !== "") {
                                    ToastSuccess("Xóa thành công!", () => {
                                        let dataSource = this.state.dataSource.filter(el => el.ma_ttdh_kc !== ma_ttdh_kc)
                                        this.setState({ dataSource, ma_ttdh_kc: 0 })
                                        API.Login.AU_DSNVCB(global.ma_ctcb_kc, this.props.dispatch)
                                        if (flagNavigate) {
                                            this.props.navigation.navigate("TTDH")
                                        }
                                    })
                                } else {
                                    Toast.show({ text: "Xảy ra lỗi. Vui lòng thực hiện lại sau!", type: "danger", buttonText: "Đóng", position: "top", duration: 2000 })
                                }
                            })
                        }

                    }
                }
            ],
            { cancelable: false }
        )
    }

    renderTTDH_Details_iPad = () => {
        let { ma_ttdh_kc } = this.state
        if (ma_ttdh_kc !== 0) {
            return (
                <TTDH_Details_iPad
                    navigation={this.props.navigation}
                    ma_ttdh_kc={ma_ttdh_kc}
                    item={this.state.cur_item}
                    loaiGuiNhan={this.activeTab === 0 ? "Nhận" : "gửi"}
                />
            )
        } else {
            return null
        }
    }

    render() {

        this.styles = StyleSheet.create({
            CardItem: {
                flex: 1,
                borderTopWidth: 0,
                borderBottomWidth: 0.55,
                borderLeftWidth: 0,
                borderRightWidth: 0,
                borderBottomColor: "lightgray",
                paddingLeft: 8,
                alignItems: 'center',
                justifyContent: "center",
                marginBottom: 1,
            },
            colTieuDe: {
                width: "70%",
                justifyContent: "center"
            },
            textBold: {
                color: "#000000",
                fontSize: this.props.fontSize.FontSizeNorman,
                fontWeight: "bold",
                paddingBottom: AppConfig.defaultPadding
            },
            textNoBold: {
                color: "#000000",
                fontSize: this.props.fontSize.FontSizeNorman,
                paddingBottom: AppConfig.defaultPadding
            },
            txtNgayNhan: {
                width: 100,
                color: "#808080",
                fontSize: this.props.fontSize.FontSizeSmall,
                textAlign: "center",
                paddingBottom: AppConfig.defaultPadding
            },
            txtNoiDung: {
                color: "#808080",
                fontSize: this.props.fontSize.FontSizeSmall,
            },
            colNgayGui: {
                width: "30%",
                justifyContent: "center",
                alignItems: "flex-end"
            },
            menuTrigger: {
                width: 40,
                height: 25,
                justifyContent: "center",
                alignItems: 'center'
            },
            menuTriggerIcon: {
                fontSize: this.props.fontSize.FontSizeXXXLarge,
                color: "#B0BEC5"
            },
            menuOptionsContainer: {
                width: 200
            },
            menuOptions: {
                borderBottomColor: AppConfig.defaultLineColor,
                borderBottomWidth: 0.5
            },
            menuOptionsText: {
                color: AppConfig.headerBackgroundColor,
                fontSize: this.props.fontSize.FontSizeNorman,
                paddingTop: AppConfig.defaultPadding,
                paddingBottom: AppConfig.defaultPadding
            },
            menuOptionsTextDelete: {
                color: "red",
                fontSize: this.props.fontSize.FontSizeNorman,
                paddingTop: AppConfig.defaultPadding,
                paddingBottom: AppConfig.defaultPadding
            }
        })

        let nv_nhan = 0
        try {
            let cb_dsnvcb = this.props.cb_dsnvcb
            if (checkVersionWeb(21)) {
                nv_nhan = getNVNew("khac", "thong-diep/thong-tin-dieu-hanh-chua-xem?page=1", cb_dsnvcb)
            } else {
                nv_nhan = getNVOld("thong-tin-dieu-hanh-da-nhan?page", "", cb_dsnvcb)
            }
        } catch (error) {
            console.log("nv_ttdh_danhan error")
        }

        if (Platform.OS === "ios" && Platform.isPad) {
            return (
                <Container style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
                    <HeaderWithLeftRight
                        title="Thông tin điều hành"
                        buttonLeftName="menu"
                        onPressLeftButton={() => { this.props.navigation.dispatch(DrawerActions.toggleDrawer()) }}
                        buttonRightName="note-add"
                        onPressRightButton={() => { this.props.navigation.navigate("TTDH_Gui") }}
                    />
                    <View style={{ flex: 1 }}>
                        <MyTab
                            values={["Đã nhận", "Đã gửi"]}
                            badges={[nv_nhan, 0]}
                            activeTab={this.activeTab}
                            onChangeTab={(index) => { this.onChangeTab(index) }}
                            fontSize={this.props.fontSize}
                        />
                        <SearchHeader
                            ref={(ref) => { this.searchHeader = ref }}
                            onSearchData={this.onSearchData}
                            fontSize={this.props.fontSize}
                        />
                        {
                            (this.state.loading) ? (
                                <Spinner color={AppConfig.blueBackground} />
                            ) : (
                                    <CardItem style={{ flex: 1, flexDirection: "row", paddingLeft: 5, paddingRight: 5 }}>
                                        <View style={{ width: 200 }}>
                                            <FlatList
                                                ref={(ref) => { this.flatListRef = ref }}
                                                data={this.state.dataSource}
                                                keyExtractor={(item, index) => "key" + item.ma_ttdh_kc + index}
                                                renderItem={({ item }) => {
                                                    return (
                                                        <TTDHTab_Item
                                                            activeTab={this.activeTab}
                                                            item={item}
                                                            ma_ctcb_kc={global.ma_ctcb_kc}
                                                            navigation={this.props.navigation}
                                                            onDelete={this.onDelete}
                                                            cur_ma_ttdh_kc={this.state.ma_ttdh_kc}
                                                            onPress={() => {
                                                                this.setState({
                                                                    ma_ttdh_kc: item.ma_ttdh_kc,
                                                                    cur_item: item
                                                                })
                                                            }}
                                                            fontSize={this.props.fontSize}
                                                            styles={this.styles}
                                                        />
                                                    )
                                                }}
                                                onRefresh={this.onRefresh}
                                                refreshing={this.state.refreshing}
                                                onEndReached={this.loadMore}
                                                onEndReachedThreshold={10}
                                                removeClippedSubviews={true}
                                            />
                                        </View>
                                        <View style={{ flex: 1 }}>
                                            {this.renderTTDH_Details_iPad()}
                                        </View>
                                    </CardItem>
                                )
                        }
                    </View>
                    <FooterTab tabActive="ttdh" navigation={this.props.navigation} />
                </Container>
            );
        } else {
            return (
                <Container style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
                    <HeaderWithLeftRight
                        title="Thông tin điều hành"
                        buttonLeftName="menu"
                        onPressLeftButton={() => { this.props.navigation.dispatch(DrawerActions.toggleDrawer()) }}
                        buttonRightName="note-add"
                        onPressRightButton={() => { this.props.navigation.navigate("TTDH_Gui") }}
                    />
                    <View style={{ flex: 1 }}>
                        <MyTab
                            values={["Đã nhận", "Đã gửi"]}
                            badges={[nv_nhan, 0]}
                            activeTab={this.activeTab}
                            onChangeTab={(index) => { this.onChangeTab(index) }}
                            fontSize={this.props.fontSize}
                        />
                        <SearchHeader
                            ref={(ref) => { this.searchHeader = ref }}
                            onSearchData={this.onSearchData}
                            fontSize={this.props.fontSize}
                        />
                        {
                            (this.state.loading) ? (
                                <View><Spinner color={AppConfig.blueBackground} /></View>
                            ) : (
                                    <FlatList
                                        ref={(ref) => { this.flatListRef = ref }}
                                        data={this.state.dataSource}
                                        keyExtractor={(item, index) => "key" + item.ma_ttdh_kc + index}
                                        renderItem={({ item }) => {
                                            return (
                                                <TTDHTab_Item
                                                    activeTab={this.activeTab}
                                                    item={item}
                                                    ma_ctcb_kc={global.ma_ctcb_kc}
                                                    navigation={this.props.navigation}
                                                    onDelete={this.onDelete}
                                                    fontSize={this.props.fontSize}
                                                    styles={this.styles}
                                                />
                                            )
                                        }}
                                        onRefresh={this.onRefresh}
                                        refreshing={this.state.refreshing}
                                        onEndReached={this.loadMore}
                                        onEndReachedThreshold={15}
                                        removeClippedSubviews={true}
                                    />
                                )
                        }
                    </View>
                    <FooterTab tabActive="ttdh" navigation={this.props.navigation} />
                </Container>
            );
        }

    }
}

function mapStateToProps(state) {
    return {
        cb_dsnvcb: state.cb_dsnvcb.data,
        fontSize: state.fontSize,
    }
}

export default connect(mapStateToProps)(TTDHScreen)