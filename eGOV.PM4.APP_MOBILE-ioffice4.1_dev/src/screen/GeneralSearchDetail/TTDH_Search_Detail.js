import React, { Component } from 'react';
import { View, ScrollView, StyleSheet } from "react-native"
import { connect } from "react-redux"
import { Container, Text } from "native-base"
import { AppConfig } from "../../AppConfig"
import { RemoveNoiDungTTDH } from "../../untils/TextUntils"
import HeaderWithLeft from "../../components/Header/HeaderWithLeft"
import API from "../../networks"
import TTDH_Search_Details_Item from "../../screen/GeneralSearchDetail/TTDH_Search_Detail_Item";

class TTDH_Search_Details extends Component {
    constructor(props) {
        super(props)
        this.state = {
            danhSachChiTiet: [],
            listNguoiNhan: [],
        }
        this.item = []
        this.loaiGuiNhan = ""
    }

    componentDidMount = () => {
        this.item = this.props.navigation.getParam("item", [])
        this.loaiGuiNhan = this.props.navigation.getParam("loaiGuiNhan", "")
        if (this.loaiGuiNhan === "Nhận") {
            this.getDSChiTietTTDHNhan(this.item.ma_ttdh_gui_kc)
        } else {
            this.getNguoiNhan(this.item.ma_ttdh_kc)
        }

    }

    getNguoiNhan = (ma_ttdh_kc) => {
        API.ThongTinDieuHanh.AU_TTDH_DSCBN(ma_ttdh_kc).then((listNguoiNhan) => {
            if (listNguoiNhan.length > 0) {
                this.getDSChiTietTTDHGui(ma_ttdh_kc, listNguoiNhan[0].ma_ctcb_kc, listNguoiNhan[0].ma_ctcb_gui)
            }
        })
    }

    getDSChiTietTTDHGui = (ma_ttdh_kc, ma_ctcb_nhan, ma_ctcb_gui) => {
        API.ThongTinDieuHanh.AU_TTDH_CTTTDHG(ma_ttdh_kc, ma_ctcb_nhan, ma_ctcb_gui).then((danhSachChiTiet) => {
            this.setState({ danhSachChiTiet })
        })
    }

    getDSChiTietTTDHNhan = (ma_ttdh_gui_kc) => {
        const ma_can_bo = global.ma_can_bo
        API.ThongTinDieuHanh.AU_TTDH_CTTTDHN(ma_ttdh_gui_kc, ma_can_bo).then((danhSachChiTiet) => {
            this.setState({ danhSachChiTiet })
        })
    }

    renderDSChiTiet = () => {
        let result = []
        for (let item of this.state.danhSachChiTiet) {
            result.push(
                <TTDH_Search_Details_Item
                    loaiGuiNhan={this.loaiGuiNhan}
                    navigation={this.props.navigation}
                    key={item.ma_ttdh_kc}
                    item={item}
                />
            )
        }
        return result
    }

    render() {

        this.styles = StyleSheet.create({
            scrollContainer: {
                flex: 1,
                bottom: 0,
                backgroundColor: AppConfig.grayBackground
            },
            viewTieuDe: {
                backgroundColor: AppConfig.grayBackground,
                justifyContent: "center", alignContent: "center",
                flexWrap: "wrap",
                padding: 4,
            },
            textTieuDe: {
                fontSize: this.props.fontSize.FontSizeXLarge,
                fontWeight: "400",
                textAlign: "center"
            }
        })

        
        return (
            <Container style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
                <HeaderWithLeft title="Chi tiết thông tin điều hành" buttonName="arrow-back" onPressLeftButton={() => { this.props.navigation.navigate("VBDen_Search") }} />
                <View style={this.styles.viewTieuDe}>
                    <Text style={this.styles.textTieuDe}>{RemoveNoiDungTTDH(this.item.tieu_de)}</Text>
                </View>
                <View style={{ flex: 1 }}>
                    <ScrollView
                        removeClippedSubviews={true}
                        scrollEnabled={true}
                        style={this.styles.scrollContainer}
                    >
                        {this.renderDSChiTiet()}
                    </ScrollView>
                </View>
            </Container>
        );
    }
}

function mapStateToProps(state) {
    return {
        fontSize: state.fontSize,
    }
}

export default connect(mapStateToProps)(TTDH_Search_Details)