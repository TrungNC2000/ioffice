import React, { PureComponent } from 'react';
import { View, FlatList, StyleSheet, Image, Modal } from "react-native"
import { connect } from "react-redux"
import { Text, Card, Button, Icon, Item, Left } from "native-base"
import { AppConfig } from "../../AppConfig"
import { getFileChiTietTTDH, RemoveNoiDungTTDH } from "../../untils/TextUntils"
import API from "../../networks"
import ViewFile from "../../components/ViewFile"

class ModalNguoiNhan extends PureComponent {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <Modal
                transparent={false}
                animationType="slide"
                visible={this.props.isModalVisible}
                supportedOrientations={["landscape", "landscape-left", "landscape-right", "portrait", "portrait-upside-down"]}
            >
                <View style={{ flex: 1, padding: 15, backgroundColor: AppConfig.grayBackground }}>
                    <View style={{ flex: 1, backgroundColor: "#FFFFFF", borderRadius: 10 }}>
                        <View style={{ position: 'absolute', zIndex: 99, height: 55, left: 0, right: 0, top: 0, justifyContent: "center", padding: 4, alignItems: "center" }}>
                            <Text style={{ fontSize: this.props.fontSize.FontSizeLarge }}>Danh sách cán bộ nhận</Text>
                        </View>
                        <FlatList
                            ref={(ref) => { this.flatListRef = ref }}
                            data={this.props.listMCB_DVCB}
                            keyExtractor={(item, index) => "key" + item.ma_ctcb_kc + index}
                            style={{ position: 'absolute', left: 0, right: 0, top: 55, bottom: 55 }}
                            initialNumToRender={50}
                            renderItem={({ item, index }) => {
                                let color = item.trang_thai_ttdh_gui === 0 ? "red" : "#000000"
                                return (
                                    <Item key={item.ma_ctcb_kc} style={{ padding: 12, borderBottomWidth: AppConfig.defaultLineWidth, borderColor: AppConfig.defaultLineColor }}>
                                        <Left>
                                            <Text style={{ fontSize: this.props.fontSize.FontSizeNorman, color }}>{item.ho_va_ten_can_bo}</Text>
                                        </Left>
                                    </Item>
                                )
                            }}
                            ListEmptyComponent={() => {
                                return (
                                    <View style={{ alignItems: "center", marginTop: 10, }}>
                                        <Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>Không có dữ liệu</Text>
                                    </View>
                                )
                            }}
                            removeClippedSubviews={true}
                        />
                        <View style={{ position: 'absolute', zIndex: 99, height: 55, left: 0, right: 0, bottom: 0, flexDirection: "row", justifyContent: "space-evenly", paddingTop: 10, alignItems: "center" }}>
                            <Button small bordered rounded iconLeft onPress={_ => this.props.toggleModal()}><Icon name="close" type="MaterialIcons"></Icon><Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>Đóng</Text></Button>
                        </View>
                    </View>
                </View>
            </Modal>

        )
    }
}

class TTDH_Search_Details_Item extends PureComponent {
    constructor(props) {
        super(props)
        this.state = {
            showFullNguoiNhan: true,
            listNguoiNhan: [],
        }
        this.item = this.props.item
        this.cb_dsctcb = this.props.cb_dsctcb
        this.loaiGuiNhan = this.props.loaiGuiNhan
    }

    componentDidMount = () => {
        this.getNguoiNhan(this.item.ma_ttdh_kc)
    }

    getNguoiNhan = (ma_ttdh_kc) => {
        API.ThongTinDieuHanh.AU_TTDH_DSCBN(ma_ttdh_kc)
            .then((listNguoiNhan) => {
                this.setState({ listNguoiNhan })
            })
    }

    renderNguoiNhan = () => {
        let { listNguoiNhan } = this.state
        const dataCount = listNguoiNhan.length
        const numberShow = 20
        let reSult = []
        if (dataCount > 0) {
            let numberRender = dataCount > numberShow ? numberShow : dataCount
            for (i = 0; i < numberRender; i += 1) {
                if (i === numberRender - 1) {
                    if (listNguoiNhan[i].trang_thai_ttdh_gui === 0) {
                        reSult.push(<Text key={listNguoiNhan[i].ma_ctcb_kc} style={this.styles.textNguoiNhanChuaXem}>{listNguoiNhan[i].ho_va_ten_can_bo}</Text>)
                    } else {
                        reSult.push(<Text key={listNguoiNhan[i].ma_ctcb_kc} style={this.styles.textNguoiNhanDaXem}>{listNguoiNhan[i].ho_va_ten_can_bo}</Text>)
                    }
                } else {
                    if (listNguoiNhan[i].trang_thai_ttdh_gui === 0) {
                        reSult.push(<Text key={listNguoiNhan[i].ma_ctcb_kc} style={this.styles.textNguoiNhanChuaXem}>{listNguoiNhan[i].ho_va_ten_can_bo},</Text>)
                    } else {
                        reSult.push(<Text key={listNguoiNhan[i].ma_ctcb_kc} style={this.styles.textNguoiNhanDaXem}>{listNguoiNhan[i].ho_va_ten_can_bo},</Text>)
                    }
                }
            }
        }

        if (dataCount > numberShow) {
            return (<Text onPress={() => { this.setState({ isModalVisible: true }) }} style={{ fontSize: this.props.fontSize.FontSizeSmall, color: "#808080", margin: 3 }}>Người nhận: {reSult} + {dataCount - numberShow}</Text>)
        } else {
            return (<Text style={{ fontSize: this.props.fontSize.FontSizeSmall, color: "#808080", margin: 3 }}>Người nhận: {reSult}</Text>)
        }
    }

    renderListFile = () => {
        let result = []
        if (this.item.src_file_ttdh !== null) {
            let data = this.item.src_file_ttdh.split(":")
            data = getFileChiTietTTDH(data)
            for (let item of data) {
                let itemTemp = item.split("|")
                result.push(
                    <View key={item} style={this.styles.viewFile}>
                        <Image style={this.styles.iconFile} source={{ uri: itemTemp[3] }} />
                        <Text numberOfLines={1} ellipsizeMode="tail" style={this.styles.textTenFile}>{itemTemp[1]}</Text>
                    </View>
                )
            }
        }
        return result
    }


    render() {

        this.styles = StyleSheet.create({
            viewNguoiGui_NguoiNhan_File: {
                marginLeft: 12,
                marginRight: 12
            },
            viewNguoiGui: {
                width: "70%",
                justifyContent: "center",
            },
            viewNgayGui: {
                width: "30%",
                justifyContent: "center",
                alignItems: "flex-end",
            },
            viewNguoiNhan: {
                justifyContent: "center",
                flexWrap: "wrap",
            },
            textNguoiGui: {
                fontSize: this.props.fontSize.FontSizeNorman,
                margin: 3,
            },
            textLoaiTTDH: {
                fontSize: this.props.fontSize.FontSizeSmall,
                color: "#808080",
                margin: 3,
            },
            textNguoiNhanChuaXem: {
                fontSize: this.props.fontSize.FontSizeSmall,
                color: "red",
                margin: 3,
            },
            textNguoiNhanDaXem: {
                fontSize: this.props.fontSize.FontSizeSmall,
                color: "#808080",
                margin: 3,
            },
            textNgayGui: {
                fontSize: this.props.fontSize.FontSizeSmall,
                color: "#808080",
                textAlign: "center",
                margin: 3,
            },
            textNoiDung: {
                fontSize: this.props.fontSize.FontSizeNorman,
            },
            viewFile: {
                maxWidth: 250,
                height: 38,
                flexDirection: "row",
                alignItems: "center",
                margin: 2,
                padding: 2,
                borderRadius: 6,
                borderWidth: 0.5,
                borderColor: AppConfig.defaultLineColor
            },
            iconFile: {
                height: 25,
                width: 25
            },
            textTenFile: {
                maxWidth: 180,
                fontSize: this.props.fontSize.FontSizeNorman
            },
            viewNoiDung: {
                justifyContent: "center",
                alignContent: "center",
                flexWrap: "wrap",
                padding: 12,
            },
            viewPhanHoi_ChuyenTiep: {
                flexDirection: "row",
                justifyContent: "space-evenly",
                alignItems: "center",
                marginLeft: 12,
                marginRight: 12,
                paddingTop: 6,
                paddingBottom: 6,
                borderTopWidth: AppConfig.defaultLineWidth,
                borderTopColor: AppConfig.defaultLineColor
            },
            btnPhanHoi: {
                height: 35,
            },
            textPhanHoi: {
                color: "#000000",
                fontSize: this.props.fontSize.FontSizeSmall,
                fontWeight: "100",
            }
        })

        return (
            <Card noShadow={true}>
                <View style={this.styles.viewNguoiGui_NguoiNhan_File}>
                    <View style={{ flex: 1, justifyContent: "center", alignItems: 'center', flexDirection: "row" }}>
                        <View style={this.styles.viewNguoiGui}>
                            <Text style={this.styles.textNguoiGui}>{this.item.ten_nguoi_gui}</Text>
                            <Text style={this.styles.textLoaiTTDH}>Loại: {this.item.ten_loai_ttdh}</Text>
                        </View>
                        <View style={this.styles.viewNgayGui}>
                            <Text style={this.styles.textNgayGui}>{this.item.ngay_gui_vn}</Text>
                        </View>
                    </View>
                    <View style={this.styles.viewNguoiNhan}>
                        {this.renderNguoiNhan()}
                    </View>
                    <View style={{ justifyContent: "flex-start" }}>
                        <ViewFile src_file={this.item.src_file_ttdh} fontSize={this.props.fontSize} />
                    </View>
                </View>
                <View style={this.styles.viewNoiDung}>
                    <Text style={this.styles.textNoiDung}>{RemoveNoiDungTTDH(this.item.noi_dung)}</Text>
                </View>
            </Card>
        )
    }
}

function mapStateToProps(state) {
    return {
        fontSize: state.fontSize,
    }
}

export default connect(mapStateToProps)(TTDH_Search_Details_Item)
