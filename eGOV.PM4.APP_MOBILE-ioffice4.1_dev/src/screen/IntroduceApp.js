import React, { Component } from 'react';
import { View, Image, ScrollView, StyleSheet, Platform } from "react-native"
import { StackActions } from "react-navigation"
import { Container, Text, Card } from "native-base"
import DeviceInfo from "react-native-device-info"
import { connect } from "react-redux"
import HeaderWithLeft from "../components/Header/HeaderWithLeft"
import { AppConfig } from '../AppConfig'
const logoImage = require('../assets/images/loginLogo.png')
import * as LogChange from "../../logChange.json"

class IntroduceApp extends Component {
    render() {

        this.styles = StyleSheet.create({
            container: {
                flex: 1
            },
            scrollView: {
                backgroundColor: "#BDBDBD",
            },
            viewAll: {
                alignItems: 'center',
                backgroundColor: "white",
                borderWidth: 0.8,
                borderColor: "gray",
                borderRadius: 10,
                marginTop: 10,
                marginBottom: 10,
                marginLeft: 10,
                marginRight: 10,
            },
            image: {
                // minWidth: 200,
                // minHeight: 80,
                // maxWidth: 340,
                // maxHeight: 130,
                width: 80,
                height: 80
            },
            text: {
                fontSize: this.props.fontSize.FontSizeLarge,
                padding: 15,
                paddingTop: 0,
            }
        })

        const popAction = StackActions.pop({ n: 1 })
        let introduceApp = AppConfig.introduceApp
        introduceApp += "\r\n\r\nThông tin phiên bản:"
        introduceApp += "\r\n      + API Version: 4.0." + global.web_version
        introduceApp += "\r\n      + App Version: " + DeviceInfo.getVersionSync()

        let noiDungCapNhat = LogChange[Platform.OS][DeviceInfo.getVersionSync()]
        if (noiDungCapNhat) {
            introduceApp += "\r\n\r\nNội dung cập nhật:\r\n" + noiDungCapNhat
        }
        return (
            <Container style={this.styles.container}>
                <HeaderWithLeft title="Giới thiệu ứng dụng" buttonName="arrow-back" onPressLeftButton={() => {
                    this.props.navigation.dispatch(popAction);
                }} />
                <ScrollView style={this.styles.scrollView}>
                    <Card style={this.styles.viewAll}>
                        <View style={{ padding: 10, backgroundColor: "#1967d2", borderRadius: 25, margin: 10 }}>
                            <Image style={this.styles.image} source={logoImage} />
                        </View>
                        <Text style={this.styles.text}>
                            {introduceApp}
                        </Text>
                    </Card>
                </ScrollView>
            </Container>
        )
    }
}

function mapStateToProps(state) {
    return {
        fontSize: state.fontSize,
    }
}

export default connect(mapStateToProps)(IntroduceApp)