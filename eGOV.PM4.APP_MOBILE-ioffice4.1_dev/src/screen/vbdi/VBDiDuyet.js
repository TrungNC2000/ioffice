import React, { Component } from 'react';
import { View, StyleSheet, FlatList, Alert, TouchableOpacity, Platform } from "react-native"
import { Container, Text, CardItem, Spinner, Toast } from "native-base"
import { DrawerActions } from "react-navigation"
import { connect } from "react-redux"
import { Menu, MenuOptions, MenuOption, MenuTrigger } from 'react-native-popup-menu'
import Icon from "react-native-vector-icons/FontAwesome5"
import API from "../../networks"
import HeaderWithLeft from "../../components/Header/HeaderWithLeft"
import FooterTab from "../../components/Footer/FooterTab"
import SearchHeader from "../../components/SearchHeader"
import MyTab from "../../components/Tab"
import ModalListFile from "../../components/ModalListFile"
import ModalWebView from "../../components/ModalWebView"
import { AppConfig } from "../../AppConfig"
import { RemoveNoiDungTTDH, ConvertDateTime, ConvertDateTimeDetail, getFileChiTietTTDH, ToastSuccess, checkVersionWeb, getNVOld, getNVNew } from "../../untils/TextUntils"
import { CheckSession } from "../../untils/NetInfo"
import VBDi_Details_iPad from "./VBDi_Details_iPad"

const nowDate = new Date()

class LD_DuyetTab_Item extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isModalVisible: false,
            isBold: this.props.item.xem === 0 ? true : false
        }
    }

    onPressItem = () => {
        if (Platform.OS === "ios" && Platform.isPad) {
            this.props.onPress()
        } else {
            this.props.navigation.navigate("VBDi_Details", {
                isDuyet: true,
                ma_xu_ly_di: this.props.item.ma_xu_ly_di,
                ma_van_ban_di_kc: this.props.item.ma_van_ban_di_kc,
                onSuccess: this.props.onSuccess,
                isUpdateViewCV: this.state.isBold,
                updateViewCV: this.updateViewCV,
                activeTab: this.props.activeTab,
                index: this.props.index,
                dataSource: this.props.dataSource
            })
        }
    }

    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible })
    }

    onViewFile = () => {
        this.toggleModal()
    }

    updateViewCV = async () => {
        let ma_van_ban_di_kc = this.props.item.ma_van_ban_di_kc
        let ma_ctcb = global.ma_ctcb_kc
        await API.VanBanDi.AU_VBDI_CNDX(ma_ctcb, ma_van_ban_di_kc).then((result) => {
            if (result) {
                this.setState({ isBold: false })
            }
        })
    }

    render() {
        this.styles = this.props.styles
        const { item } = this.props
        let dataFile = item.file_van_ban ? getFileChiTietTTDH(item.file_van_ban.split(":")) : []
        let styleTemp = this.styles.textNoBold
        if (this.state.isBold) {
            styleTemp = this.styles.textBold
        }
        const isVBKhan1 = item.ma_cap_do_khan > 1 ? "red" : "#000000"
        const isVBKhan2 = item.ma_cap_do_khan > 1 ? "red" : "#808080"
        let borderRightWidth = (item.ma_xu_ly_di === this.props.cur_ma_xu_ly_di) ? 2.5 : 0.8
        let borderRightColor = (item.ma_xu_ly_di === this.props.cur_ma_xu_ly_di) ? "red" : "lightgray"

        if (Platform.OS === "ios" && Platform.isPad) {
            return (
                <CardItem style={[this.styles.CardItem, { borderRightWidth, borderRightColor }]} key={item.ma_van_ban_kc}>
                    <TouchableOpacity style={{ justifyContent: 'center', flex: 1 }} onPress={this.onPressItem}>
                        <Text numberOfLines={3} ellipsizeMode="tail" style={[styleTemp, { color: isVBKhan1 }]}>{RemoveNoiDungTTDH(this.props.item.trich_yeu)} </Text>
                        {
                            (this.props.item.so_ky_hieu !== null) && (
                                <Text numberOfLines={1} ellipsizeMode="tail" style={[styleTemp, { color: isVBKhan1 }]}>Số: {this.props.item.so_ky_hieu} </Text>
                            )
                        }
                        <Text numberOfLines={2} ellipsizeMode="tail" style={[this.styles.txtNoiDung, { color: isVBKhan2 }]}>CQBH: {item.ten_co_quan_ban_hanh} </Text>
                        <Text numberOfLines={1} ellipsizeMode="tail" style={[this.styles.txtNoiDung, { color: isVBKhan2 }]}>Độ khẩn: {item.ten_cap_do_khan} </Text>
                        <Text numberOfLines={1} ellipsizeMode="tail" style={[this.styles.txtNoiDung, { color: isVBKhan2 }]}>Độ mật: {item.ten_cap_do_mat} </Text>
                        <Text numberOfLines={2} ellipsizeMode="tail" style={[this.styles.txtNoiDung, { color: isVBKhan2 }]}>Ngày: {ConvertDateTimeDetail(this.props.activeTab === 2 ? item.ngay_duyet : this.props.activeTab === 3 ? item.ngay_di : item.ngay_nhan)} </Text>
                    </TouchableOpacity>
                </CardItem>
            )
        } else {
            return (
                <CardItem style={this.styles.CardItem} key={item.ma_van_ban_kc}>
                    <TouchableOpacity style={{ justifyContent: 'center', flex: 1, left: 0, marginRight: 100 }} onPress={this.onPressItem}>
                        <Text numberOfLines={3} ellipsizeMode="tail" style={[styleTemp, { color: isVBKhan1 }]}>{RemoveNoiDungTTDH(this.props.item.trich_yeu)} </Text>
                        {
                            (this.props.item.so_ky_hieu !== null) && (
                                <Text numberOfLines={1} ellipsizeMode="tail" style={[styleTemp, { color: isVBKhan1 }]}>Số: {this.props.item.so_ky_hieu} </Text>
                            )
                        }
                        <Text numberOfLines={2} ellipsizeMode="tail" style={[this.styles.txtNoiDung, { color: isVBKhan2 }]}>CQBH: {item.ten_co_quan_ban_hanh} </Text>
                        <Text numberOfLines={1} ellipsizeMode="tail" style={[this.styles.txtNoiDung, { color: isVBKhan2 }]}>Độ khẩn: {item.ten_cap_do_khan} </Text>
                        <Text numberOfLines={1} ellipsizeMode="tail" style={[this.styles.txtNoiDung, { color: isVBKhan2 }]}>Độ mật: {item.ten_cap_do_mat} </Text>
                    </TouchableOpacity>
                    <View style={{ width: 100, top: 0, right: 0, bottom: 0, position: "absolute", alignItems: 'center', justifyContent: "center" }}>
                        <Text style={this.styles.txtNgayNhan}>{ConvertDateTime(this.props.activeTab === 2 ? item.ngay_duyet : this.props.activeTab === 3 ? item.ngay_di : item.ngay_nhan)} </Text>
                        <Menu>
                            <MenuTrigger style={this.styles.menuTrigger}>
                                <Icon name="ellipsis-v" style={this.styles.menuTriggerIcon}></Icon>
                            </MenuTrigger>
                            <MenuOptions optionsContainerStyle={this.styles.menuOptionsContainer}>
                                {
                                    (dataFile.length > 1 || (dataFile.length === 1 && dataFile[0] !== "")) && (
                                        <MenuOption onSelect={() => this.onViewFile()} >
                                            <Text style={this.styles.menuOptionsText}>Xem tệp tin</Text>
                                        </MenuOption>
                                    )
                                }
                                {
                                    (this.props.activeTab === 0 || this.props.activeTab === 1) && (
                                        <MenuOption onSelect={() => { this.props.onSuccess(item.ma_van_ban_di_kc, item.ma_xu_ly_di, 7, item.file_van_ban, false) }} >
                                            <Text style={this.styles.menuOptionsText}>Duyệt nhanh</Text>
                                        </MenuOption>
                                    )
                                }
                            </MenuOptions>
                        </Menu>
                    </View>
                    {
                        (dataFile.length === 1) && (
                            <ModalWebView
                                isModalVisible={this.state.isModalVisible}
                                title={dataFile[0].split("|")[1]}
                                url={dataFile[0].split("|")[0]}
                                path={dataFile[0].split("|")[4]}
                                toggleModal={() => this.toggleModal()}
                                isUpdateViewCV={this.state.isBold}
                                updateViewCV={() => this.updateViewCV()}
                            />
                        )
                    }
                    {
                        (dataFile.length > 1) && (
                            <ModalListFile
                                isModalVisible={this.state.isModalVisible}
                                dataFile={dataFile}
                                toggleModal={() => this.toggleModal()}
                                isUpdateViewCV={this.state.isBold}
                                updateViewCV={() => this.updateViewCV()}
                            />
                        )
                    }
                </CardItem>
            )
        }
    }
}

class VBDiDuyetScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            loading: true,
            refreshing: false,
            dataSource: [],
            ma_xu_ly_di: 0,
            ma_van_ban_di_kc: 0,
        }
        this.page = 1
        this.activeTab = 0
        this.nowYear = nowDate.getFullYear()
        this.stopLoadMore = false
        this.responseLength = 20
    }

    componentDidMount = () => {
        this.didBlurSubscription = this.props.navigation.addListener('willFocus', payload => { CheckSession(this.props.navigation) })
        this.activeTab = this.props.navigation.getParam("activeTab", 0)
        this.getListByTabIndex(this.activeTab)
        API.Login.AU_DSNVCB(global.ma_ctcb_kc, this.props.dispatch)
    };

    componentWillUnmount = () => {
        this.didBlurSubscription.remove()
    };

    shouldComponentUpdate = (nextProps, nextState) => {
        if (this.state.loading !== nextState.loading ||
            this.state.refreshing !== nextState.refreshing ||
            this.state.dataSource !== nextState.dataSource ||
            this.state.ma_xu_ly_di !== nextState.ma_xu_ly_di ||
            this.state.ma_van_ban_di_kc !== nextState.ma_van_ban_di_kc ||
            this.props.cb_dsnvcb !== nextProps.cb_dsnvcb) {
            return true
        }
        return false
    };

    getListByTabIndex = () => {
        let ma_ctcb_duyet = global.ma_ctcb_kc
        let ma_don_vi_quan_tri = global.ma_don_vi_quan_tri
        let page = this.page
        let nam = this.nowYear
        let tabIndex = this.activeTab
        let keyWord = this.searchHeader.getKeyWord()

        if (keyWord !== "") {
            nam = 0
            page = 1
            let size = 9999999
            if (checkVersionWeb(23)) {
                if (tabIndex === 0) {
                    API.VanBanDi.AU_VBDI_VBDCDCLD("", keyWord, page, size, ma_ctcb_duyet, ma_don_vi_quan_tri, nam).then(response => {
                        if (tabIndex === this.activeTab) {
                            this.setState({ dataSource: [...this.state.dataSource, ...response] }, () => {
                                this.setState({ loading: false, refreshing: false })
                            })
                            this.responseLength = response.length
                            this.stopLoadMore = true
                        }
                    })
                } else if (tabIndex === 1) {
                    API.VanBanDi.AU_VBDI_VBDDUQCLD("", keyWord, page, size, ma_ctcb_duyet, ma_don_vi_quan_tri, nam).then(response => {
                        if (tabIndex === this.activeTab) {
                            this.setState({ dataSource: [...this.state.dataSource, ...response] }, () => {
                                this.setState({ loading: false, refreshing: false })
                            })
                            this.responseLength = response.length
                            this.stopLoadMore = true
                        }
                    })
                } else if (tabIndex === 2) {
                    API.VanBanDi.AU_VBDI_VBDCPHCLD("", keyWord, page, size, ma_ctcb_duyet, ma_don_vi_quan_tri, nam).then(response => {
                        if (tabIndex === this.activeTab) {
                            this.setState({ dataSource: [...this.state.dataSource, ...response] }, () => {
                                this.setState({ loading: false, refreshing: false })
                            })
                            this.responseLength = response.length
                            this.stopLoadMore = true
                        }
                    })
                } else {
                    API.VanBanDi.AU_VBDI_VBDDPHCLD("", keyWord, page, size, ma_ctcb_duyet, ma_don_vi_quan_tri, nam).then(response => {
                        if (tabIndex === this.activeTab) {
                            this.setState({ dataSource: [...this.state.dataSource, ...response] }, () => {
                                this.setState({ loading: false, refreshing: false })
                            })
                            this.responseLength = response.length
                            this.stopLoadMore = true
                        }
                    })
                }
            } else {
                if (tabIndex === 0) {
                    let data1 = []
                    let data2 = []
                    API.VanBanDi.AU_VBDI_VBDCDCLD(keyWord, "", page, size, ma_ctcb_duyet, ma_don_vi_quan_tri, nam).then(response1 => {
                        data1 = response1
                        API.VanBanDi.AU_VBDI_VBDCDCLD("", keyWord, page, size, ma_ctcb_duyet, ma_don_vi_quan_tri, nam).then(response2 => {
                            data2 = response2
                            data1 = data2.filter(bb => !data1.find(aa => bb.ma_van_ban_di_kc === aa.ma_van_ban_di_kc)).concat(data1);
                            if (tabIndex === this.activeTab) {
                                let dataSource = this.state.dataSource.filter(bb => !data1.find(aa => bb.ma_van_ban_di_kc === aa.ma_van_ban_di_kc)).concat(data1);
                                this.setState({ dataSource: dataSource }, () => {
                                    this.setState({ loading: false, refreshing: false })
                                })
                                this.responseLength = data1.length
                                this.stopLoadMore = true
                            }
                        })
                    })
                } else if (tabIndex === 1) {
                    let data1 = []
                    let data2 = []
                    API.VanBanDi.AU_VBDI_VBDDUQCLD(keyWord, "", page, size, ma_ctcb_duyet, ma_don_vi_quan_tri, nam).then(response1 => {
                        data1 = response1
                        API.VanBanDi.AU_VBDI_VBDDUQCLD("", keyWord, page, size, ma_ctcb_duyet, ma_don_vi_quan_tri, nam).then(response2 => {
                            data2 = response2
                            data1 = data2.filter(bb => !data1.find(aa => bb.ma_van_ban_di_kc === aa.ma_van_ban_di_kc)).concat(data1);
                            if (tabIndex === this.activeTab) {
                                let dataSource = this.state.dataSource.filter(bb => !data1.find(aa => bb.ma_van_ban_di_kc === aa.ma_van_ban_di_kc)).concat(data1);
                                this.setState({ dataSource: dataSource }, () => {
                                    this.setState({ loading: false, refreshing: false })
                                })
                                this.responseLength = data1.length
                                this.stopLoadMore = true
                            }
                        })
                    })
                } else if (tabIndex === 2) {
                    let data1 = []
                    let data2 = []
                    API.VanBanDi.AU_VBDI_VBDCPHCLD(keyWord, "", page, size, ma_ctcb_duyet, ma_don_vi_quan_tri, nam).then(response1 => {
                        data1 = response1
                        API.VanBanDi.AU_VBDI_VBDCPHCLD("", keyWord, page, size, ma_ctcb_duyet, ma_don_vi_quan_tri, nam).then(response2 => {
                            data2 = response2
                            data1 = data2.filter(bb => !data1.find(aa => bb.ma_van_ban_di_kc === aa.ma_van_ban_di_kc)).concat(data1);
                            if (tabIndex === this.activeTab) {
                                let dataSource = this.state.dataSource.filter(bb => !data1.find(aa => bb.ma_van_ban_di_kc === aa.ma_van_ban_di_kc)).concat(data1);
                                this.setState({ dataSource: dataSource }, () => {
                                    this.setState({ loading: false, refreshing: false })
                                })
                                this.responseLength = data1.length
                                this.stopLoadMore = true
                            }
                        })
                    })
                } else {
                    let data1 = []
                    let data2 = []
                    API.VanBanDi.AU_VBDI_VBDDPHCLD(keyWord, "", page, size, ma_ctcb_duyet, ma_don_vi_quan_tri, nam).then(response1 => {
                        data1 = response1
                        API.VanBanDi.AU_VBDI_VBDDPHCLD("", keyWord, page, size, ma_ctcb_duyet, ma_don_vi_quan_tri, nam).then(response2 => {
                            data2 = response2
                            data1 = data2.filter(bb => !data1.find(aa => bb.ma_van_ban_di_kc === aa.ma_van_ban_di_kc)).concat(data1);
                            if (tabIndex === this.activeTab) {
                                let dataSource = this.state.dataSource.filter(bb => !data1.find(aa => bb.ma_van_ban_di_kc === aa.ma_van_ban_di_kc)).concat(data1);
                                this.setState({ dataSource: dataSource }, () => {
                                    this.setState({ loading: false, refreshing: false })
                                })
                                this.responseLength = data1.length
                                this.stopLoadMore = true
                            }
                        })
                    })
                }
            }
        } else {
            if (tabIndex === 0) {
                let size = AppConfig.pageSizeVBDIChoDuyetLD
                if (global.AU_ROOT.includes('nghean-api') && global.ht_chon_tgian_nhac_viec !== "all" && global.ht_chon_tgian_nhac_viec !== "hide") {
                    nam = 0
                    page = 1
                    size = 9999999
                }
                API.VanBanDi.AU_VBDI_VBDCDCLD("", "", page, size, ma_ctcb_duyet, ma_don_vi_quan_tri, nam).then(response => {
                    if (tabIndex === this.activeTab) {
                        this.setState({ dataSource: [...this.state.dataSource, ...response] }, () => {
                            this.setState({ loading: false, refreshing: false })
                        })
                        this.responseLength = response.length
                        this.stopLoadMore = false
                        if (this.responseLength === 0) {
                            this.loadMore()
                        }
                    }
                })
            } else if (tabIndex === 1) {
                let size = AppConfig.pageSizeVBDIUyQuyenLD
                if (global.AU_ROOT.includes('nghean-api') && global.ht_chon_tgian_nhac_viec !== "all" && global.ht_chon_tgian_nhac_viec !== "hide") {
                    nam = 0
                    page = 1
                    size = 9999999
                }
                API.VanBanDi.AU_VBDI_VBDDUQCLD("", "", page, size, ma_ctcb_duyet, ma_don_vi_quan_tri, nam).then(response => {
                    if (tabIndex === this.activeTab) {
                        this.setState({ dataSource: [...this.state.dataSource, ...response] }, () => {
                            this.setState({ loading: false, refreshing: false })
                        })
                        this.responseLength = response.length
                        this.stopLoadMore = false
                        if (this.responseLength === 0) {
                            this.loadMore()
                        }
                    }
                })
            } else if (tabIndex === 2) {
                let size = AppConfig.pageSizeVBDIDaChuyenVT
                if (global.AU_ROOT.includes('nghean-api') && global.ht_chon_tgian_nhac_viec !== "all" && global.ht_chon_tgian_nhac_viec !== "hide") {
                    nam = 0
                    page = 1
                    size = 9999999
                }
                API.VanBanDi.AU_VBDI_VBDCPHCLD("", "", page, size, ma_ctcb_duyet, ma_don_vi_quan_tri, nam).then(response => {
                    if (tabIndex === this.activeTab) {
                        this.setState({ dataSource: [...this.state.dataSource, ...response] }, () => {
                            this.setState({ loading: false, refreshing: false })
                        })
                        this.responseLength = response.length
                        this.stopLoadMore = false
                        if (this.responseLength === 0) {
                            this.loadMore()
                        }
                    }
                })
            } else {
                let size = AppConfig.pageSizeVBDIDaPhatHanhLD
                if (global.AU_ROOT.includes('nghean-api') && global.ht_chon_tgian_nhac_viec !== "all" && global.ht_chon_tgian_nhac_viec !== "hide") {
                    nam = 0
                    page = 1
                    size = 9999999
                }
                API.VanBanDi.AU_VBDI_VBDDPHCLD("", "", page, size, ma_ctcb_duyet, ma_don_vi_quan_tri, nam).then(response => {
                    if (tabIndex === this.activeTab) {
                        this.setState({ dataSource: [...this.state.dataSource, ...response] }, () => {
                            this.setState({ loading: false, refreshing: false })
                        })
                        this.responseLength = response.length
                        this.stopLoadMore = false
                        if (this.responseLength === 0) {
                            this.loadMore()
                        }
                    }
                })
            }
        }
    }

    onChangeTab = (index) => {
        if (!this.state.loading) {
            this.page = 1
            this.activeTab = index
            this.nowYear = nowDate.getFullYear()
            this.stopLoadMore = false
            this.searchHeader.setKeyWord("", () => {
                this.setState({ dataSource: [], loading: true, ma_xu_ly_di: 0 })
                this.getListByTabIndex()
            })
        }
    }

    onRefresh = () => {
        if (!this.state.loading) {
            this.page = 1
            this.nowYear = nowDate.getFullYear()
            this.stopLoadMore = false
            this.setState({ dataSource: [], refreshing: true, loading: true, ma_xu_ly_di: 0 })
            this.getListByTabIndex()
        }
    }

    loadMore = () => {
        if (global.AU_ROOT.includes('nghean-api') && global.ht_chon_tgian_nhac_viec !== "all" && global.ht_chon_tgian_nhac_viec !== "hide") {
            this.stopLoadMore = true
        }
        if (!this.state.loading && this.stopLoadMore === false) {
            if (this.nowYear > 2000) {
                this.stopLoadMore = true
                this.page = this.page + 1
                if (this.responseLength === 0) {
                    this.page = 1
                    this.nowYear = this.nowYear - 1
                }
                this.getListByTabIndex()
            }
        }
    }

    onSearchData = () => {
        if (!this.state.loading) {
            this.nowYear = nowDate.getFullYear()
            this.page = 1
            this.stopLoadMore = false
            this.setState({ dataSource: [], loading: true, ma_xu_ly_di: 0 })
            this.getListByTabIndex()
        }
    }

    onSuccess = (ma_van_ban_di_kc, ma_xu_ly_di, loai_xu_ly, file_van_ban, flagNavigate) => {
        Alert.alert("Xác nhận", "Duyệt nhanh văn bản này?",
            [
                { text: "Không", style: "cancel" },
                {
                    text: "Đồng ý", onPress: () => {
                        let ma_ctcb_kc = global.ma_ctcb_kc
                        let ma_don_vi_quan_tri = global.ma_don_vi_quan_tri
                        API.VanBanDi.AU_VBDI_DSVTTDVQT(ma_don_vi_quan_tri).then((listVT) => {
                            if (listVT.length > 0) {
                                API.KYSO.AU_KYSO_GFDK(ma_van_ban_di_kc, global.ma_ctcb_kc).then((file_moi) => {
                                    API.VanBanDi.AU_VBDI_LDCVT(ma_van_ban_di_kc, ma_ctcb_kc, listVT[0].ma_ctcb_kc, "", file_van_ban, file_moi, loai_xu_ly, ma_xu_ly_di, 0).then((response) => {
                                        if (response) {
                                            API.VanBanDi.AU_VBDI_LDDVBD(ma_ctcb_kc, ma_van_ban_di_kc).then((response) => {
                                                if (response) {
                                                    let dataSource = this.state.dataSource.filter(item => item.ma_xu_ly_di !== ma_xu_ly_di)
                                                    ToastSuccess("Duyệt nhanh văn bản thành công!", () => {
                                                        this.setState({ dataSource, ma_xu_ly_di: 0 })
                                                        API.Login.AU_DSNVCB(ma_ctcb_kc, this.props.dispatch)
                                                        if (flagNavigate) {
                                                            this.props.navigation.navigate("VBDiDuyet")
                                                        }
                                                    })
                                                } else {
                                                    Toast.show({ text: "Xảy ra lỗi. Vui lòng thực hiện lại sau!", type: "danger", buttonText: "Đóng", position: "top", duration: 2000 })
                                                }
                                            })
                                        } else {
                                            Toast.show({ text: "Xảy ra lỗi. Vui lòng thực hiện lại sau!", type: "danger", buttonText: "Đóng", position: "top", duration: 2000 })
                                        }
                                    })
                                })
                            } else {
                                Toast.show({ text: "Xảy ra lỗi. Vui lòng thực hiện lại sau!", type: "danger", buttonText: "Đóng", position: "top", duration: 2000 })
                            }
                        })
                    }
                }
            ],
            { cancelable: false }
        )
    }

    renderVBDi_Detail_iPad = () => {
        let { ma_xu_ly_di, ma_van_ban_di_kc } = this.state
        if (ma_xu_ly_di !== 0) {
            return (
                <VBDi_Details_iPad
                    navigation={this.props.navigation}
                    isDuyet={true}
                    ma_xu_ly_di={ma_xu_ly_di}
                    ma_van_ban_di_kc={ma_van_ban_di_kc}
                    onSuccess={this.onSuccess}
                    activeTab={this.activeTab}
                />
            )
        } else {
            return null
        }
    }

    render() {

        this.styles = StyleSheet.create({
            CardItem: {
                flex: 1,
                borderTopWidth: 0,
                borderBottomWidth: 0.55,
                borderLeftWidth: 0,
                borderRightWidth: 0,
                borderBottomColor: "lightgray",
                paddingLeft: 8,
                paddingTop: 4,
                paddingBottom: 4,
                alignItems: 'center',
                justifyContent: "center",
                marginBottom: 1,
            },
            colTieuDe: {
                width: "70%",
                justifyContent: "center"
            },
            textBold: {
                color: "#000000",
                fontSize: this.props.fontSize.FontSizeNorman,
                fontWeight: "bold",
                paddingBottom: AppConfig.defaultPadding
            },
            textNoBold: {
                color: "#000000",
                fontSize: this.props.fontSize.FontSizeNorman,
                paddingBottom: AppConfig.defaultPadding
            },
            txtNgayNhan: {
                width: 100,
                color: "#808080",
                fontSize: this.props.fontSize.FontSizeSmall,
                textAlign: "center",
                paddingBottom: AppConfig.defaultPadding
            },
            txtNoiDung: {
                color: "#808080",
                fontSize: this.props.fontSize.FontSizeSmall,
                paddingBottom: AppConfig.defaultPadding
            },
            colNgayGui: {
                width: "30%",
                justifyContent: "center",
                alignItems: "flex-end"
            },
            menuTrigger: {
                width: 40,
                height: 30,
                justifyContent: "center",
                alignItems: 'center'
            },
            menuTriggerIcon: {
                fontSize: this.props.fontSize.FontSizeXXXLarge,
                color: "#B0BEC5"
            },
            menuOptionsContainer: {
                width: 200
            },
            menuOptions: {
                borderBottomColor: AppConfig.defaultLineColor,
                borderBottomWidth: 0.5
            },
            menuOptionsText: {
                color: AppConfig.headerBackgroundColor,
                fontSize: this.props.fontSize.FontSizeNorman,
                paddingTop: AppConfig.defaultPadding,
                paddingBottom: AppConfig.defaultPadding
            },
            menuOptionsTextDelete: {
                color: "red",
                fontSize: this.props.fontSize.FontSizeNorman,
                paddingTop: AppConfig.defaultPadding,
                paddingBottom: AppConfig.defaultPadding
            }
        })

        let nv_choduyet = 0
        let nv_uyquyenduyet = 0
        try {
            let cb_dsnvcb = this.props.cb_dsnvcb
            if (checkVersionWeb(21)) {
                nv_choduyet = getNVNew("vbdi", "van-ban-di/duyet-van-ban-di?page=1&t=vb_di_cho_duyet_cua_ld", cb_dsnvcb)
                nv_uyquyenduyet = getNVNew("vbdi", "van-ban-di/duyet-van-ban-di?page=1&t=vb_di_duoc_uy_quyen_cua_ld", cb_dsnvcb)
            } else {
                nv_choduyet = getNVOld("duyet-van-ban-di?page?t", "vb_di_cho_duyet_cua_ld", cb_dsnvcb)
                nv_uyquyenduyet = getNVOld("duyet-van-ban-di?page?t", "vb_di_duoc_uy_quyen_cua_ld", cb_dsnvcb)
            }
        } catch (error) {
            console.log("nv_vbdi_duyet error")
        }

        if (Platform.OS === "ios" && Platform.isPad) {
            return (
                <Container style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
                    <HeaderWithLeft
                        title="Duyệt văn bản đi"
                        buttonName="menu"
                        onPressLeftButton={_ => { this.props.navigation.dispatch(DrawerActions.toggleDrawer()) }}
                    />
                    <View style={{ flex: 1 }}>
                        <MyTab
                            values={["Chờ duyệt", "Ủy quyền", "Đã chuyển văn thư", "Đã phát hành"]}
                            badges={[nv_choduyet, nv_uyquyenduyet, 0, 0]}
                            activeTab={this.activeTab}
                            onChangeTab={(index) => { this.onChangeTab(index) }}
                            fontSize={this.props.fontSize}
                        />
                        <SearchHeader
                            ref={(ref) => { this.searchHeader = ref }}
                            onSearchData={this.onSearchData}
                            fontSize={this.props.fontSize}
                        />
                        {
                            (this.state.loading) ? (
                                <Spinner color={AppConfig.blueBackground} />
                            ) : (
                                    <CardItem style={{ flex: 1, flexDirection: "row", paddingLeft: 5, paddingRight: 5 }}>
                                        <View style={{ width: 200 }}>
                                            <FlatList
                                                ref={(ref) => { this.flatListRef = ref }}
                                                data={this.state.dataSource}
                                                keyExtractor={(item, index) => "key" + item.ma_van_ban_kc + index}
                                                contentContainerStyle={{ paddingBottom: 0 }}
                                                renderItem={({ item, index }) => {
                                                    return (
                                                        <LD_DuyetTab_Item
                                                            activeTab={this.activeTab}
                                                            item={item}
                                                            ma_ctcb_kc={global.ma_ctcb_kc}
                                                            navigation={this.props.navigation}
                                                            onSuccess={this.onSuccess}
                                                            cur_ma_xu_ly_di={this.state.ma_xu_ly_di}
                                                            onPress={() => {
                                                                this.setState({
                                                                    ma_xu_ly_di: item.ma_xu_ly_di,
                                                                    ma_van_ban_di_kc: item.ma_van_ban_di_kc
                                                                })
                                                            }}
                                                            fontSize={this.props.fontSize}
                                                            styles={this.styles}
                                                        />
                                                    )
                                                }}
                                                onRefresh={this.onRefresh}
                                                refreshing={this.state.refreshing}
                                                onEndReached={this.loadMore}
                                                onEndReachedThreshold={5}
                                                removeClippedSubviews={true}
                                            />
                                        </View>
                                        <View style={{ flex: 1 }}>
                                            {this.renderVBDi_Detail_iPad()}
                                        </View>
                                    </CardItem>
                                )
                        }
                    </View>
                    <FooterTab tabActive="vbdi" navigation={this.props.navigation} />
                </Container>
            );
        } else {
            return (
                <Container style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
                    <HeaderWithLeft
                        title="Duyệt văn bản đi"
                        buttonName="menu"
                        onPressLeftButton={_ => { this.props.navigation.dispatch(DrawerActions.toggleDrawer()) }}
                    />
                    <View style={{ flex: 1 }}>
                        <MyTab
                            values={["Chờ duyệt", "Ủy quyền", "Đã chuyển văn thư", "Đã phát hành"]}
                            badges={[nv_choduyet, nv_uyquyenduyet, 0, 0]}
                            activeTab={this.activeTab}
                            onChangeTab={(index) => { this.onChangeTab(index) }}
                            fontSize={this.props.fontSize}
                        />
                        <SearchHeader
                            ref={(ref) => { this.searchHeader = ref }}
                            onSearchData={this.onSearchData}
                            fontSize={this.props.fontSize}
                        />
                        {
                            (this.state.loading) ? (
                                <View><Spinner color={AppConfig.blueBackground} /></View>
                            ) : (
                                    <FlatList
                                        ref={(ref) => { this.flatListRef = ref }}
                                        data={this.state.dataSource}
                                        keyExtractor={(item, index) => "key" + item.ma_van_ban_kc + index}
                                        contentContainerStyle={{ paddingBottom: 0 }}
                                        renderItem={({ item, index }) => {
                                            return (
                                                <LD_DuyetTab_Item
                                                    activeTab={this.activeTab}
                                                    item={item}
                                                    ma_ctcb_kc={global.ma_ctcb_kc}
                                                    navigation={this.props.navigation}
                                                    onSuccess={this.onSuccess}
                                                    cur_ma_xu_ly_di={this.state.ma_xu_ly_di}
                                                    index={index}
                                                    dataSource={this.state.dataSource}
                                                    fontSize={this.props.fontSize}
                                                    styles={this.styles}
                                                />
                                            )
                                        }}
                                        onRefresh={this.onRefresh}
                                        refreshing={this.state.refreshing}
                                        onEndReached={this.loadMore}
                                        onEndReachedThreshold={5}
                                        removeClippedSubviews={true}
                                    />
                                )
                        }
                    </View>
                    <FooterTab tabActive="vbdi" navigation={this.props.navigation} />
                </Container>
            );
        }


    }
}

function mapStateToProps(state) {
    return {
        cb_dsnvcb: state.cb_dsnvcb.data,
        fontSize: state.fontSize,
    }
}

export default connect(mapStateToProps)(VBDiDuyetScreen)