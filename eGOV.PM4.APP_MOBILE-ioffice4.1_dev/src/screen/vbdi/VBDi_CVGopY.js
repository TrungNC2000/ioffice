import React, { Component } from 'react';
import { ScrollView, StyleSheet, PixelRatio } from "react-native"
import { Container, Text, Textarea, Toast } from "native-base"
import { connect } from "react-redux"
import API from "../../networks"
import { AppConfig } from "../../AppConfig"
import { StackActions } from "react-navigation"
import HeaderWithLeftRight from "../../components/Header/HeaderWithLeftRight"
import { resetStack, ToastSuccess } from "../../untils/TextUntils"

class VBDi_CVGopY extends Component {
    constructor(props) {
        super(props)
        this.state = {
            yKienXuLy: "",
        }
        this.ma_xu_ly_di = 0
        this.ma_van_ban_di = 0
        this.file_dinh_kem = ""
    }

    componentDidMount = () => {
        this.ma_xu_ly_di = this.props.navigation.getParam("ma_xu_ly_di")
        this.ma_van_ban_di = this.props.navigation.getParam("ma_van_ban_di")
        this.file_dinh_kem = this.props.navigation.getParam("file_dinh_kem")
    }

    onPressSend = () => {
        let ma_xu_ly_di = this.ma_xu_ly_di
        let ma_van_ban_di = this.ma_van_ban_di
        let file_dinh_kem = this.file_dinh_kem
        let ma_ctcb_kc = global.ma_ctcb_kc
        let y_kien_xu_ly = this.state.yKienXuLy

        API.VanBanDi.AU_VBDI_CVGY(file_dinh_kem, ma_ctcb_kc, ma_van_ban_di, ma_xu_ly_di, y_kien_xu_ly).then((response) =>{
            if (response) {
                ToastSuccess("Góp ý văn bản thành công!", () => {
                    API.Login.AU_DSNVCB(global.ma_ctcb_kc, this.props.dispatch).then(() => {
                        this.props.navigation.dispatch(resetStack("VBDiXuLy"))
                    })
                })
            } else {
                Toast.show({ text: "Xảy ra lỗi. Vui lòng thực hiện lại sau!", type: "danger", buttonText: "Đóng", position: "top", duration: 2000 })
            }
        })
    }

    render() {
        const popAction = StackActions.pop({ n: 1 })

        return (
            <Container style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
                <HeaderWithLeftRight
                    title={"Thêm góp ý"}
                    buttonLeftName="arrow-back"
                    buttonRightName="done"
                    onPressLeftButton={() => { this.props.navigation.dispatch(popAction) }}
                    onPressRightButton={this.onPressSend}
                />
                <ScrollView style={styles.scrollContainer}>
                    <Text style={[styles.TextTitle]}>Nội dung góp ý</Text>
                    <Textarea
                        autoCorrect={false}
                        style={styles.TextInput}
                        autoFocus={true}
                        rowSpan={12}
                        value={this.state.yKienXuLy}
                        onChangeText={(text) => { this.setState({ yKienXuLy: text }) }}
                    />
                </ScrollView>
            </Container >
        );
    }
}

const styles = StyleSheet.create({
    scrollContainer: {
        flex: 1,
        marginTop: 10,
        marginLeft: 10,
        marginRight: 10
    },
    TextTitle: {
        fontSize: AppConfig.FontSizeSmall,
        margin: 5
    },
    TextInput: {
        flex: 1,
        fontFamily: AppConfig.fontFamily,
        fontSize: AppConfig.FontSizeNorman / PixelRatio.getFontScale(),
        color: "#000000",
        borderColor: AppConfig.defaultLineColor,
        borderWidth: AppConfig.defaultLineWidth,
        borderRadius: 5,
        marginRight: 2,
        paddingLeft: 5,
        paddingTop: 7,
        minHeight: 40,
        margin: 5
    },
})

export default connect()(VBDi_CVGopY)