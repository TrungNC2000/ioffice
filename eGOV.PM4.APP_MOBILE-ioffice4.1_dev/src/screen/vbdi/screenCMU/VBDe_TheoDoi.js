import React, { Component, PureComponent } from 'react';
import { View, StyleSheet, FlatList, Alert, TouchableOpacity, Platform } from "react-native"
import { Container, Text, CardItem, Spinner, Toast } from "native-base"
import { connect } from "react-redux"
import { DrawerActions } from "react-navigation"
import { Menu, MenuOptions, MenuOption, MenuTrigger } from 'react-native-popup-menu'
import Icon from "react-native-vector-icons/FontAwesome5"
import HeaderWithLeft from "../../../components/Header/HeaderWithLeft"
import FooterTab from "../../../components/Footer/FooterTab"
import SearchHeader from "../../../components/SearchHeader"
import MyTab from "../../../components/Tab"
import API from "../../../networks"
import ModalListFile from "../../../components/ModalListFile"
import ModalWebView from "../../../components/ModalWebView"
import { AppConfig } from "../../../AppConfig"
import { CheckSession } from "../../../untils/NetInfo"
import { getFileChiTietTTDH, RemoveNoiDungTTDH, ConvertDateTimeDetail, ToastSuccess, checkVersionWeb, getNVOld, getNVNew } from "../../../untils/TextUntils"
import VBDi_Details_iPad from "../VBDi_Details_iPad"
import moment from "moment"

const nowDate = new Date()

class VBDE_Details_TD extends PureComponent {
    constructor(props) {
        super(props)
        this.state = {
            isModalVisible: false,
            isBold: this.props.item.xem === 0 ? true : false
        }
    }

    onPressItem = () => {
        if (Platform.OS === "ios" && Platform.isPad) {
            this.props.onPress()
        } else {
            this.props.navigation.navigate("VBDen_Details_Cmu", {
                ma_xu_ly_den: this.props.item.ma_xu_ly_den,
                ma_van_ban_den_kc: this.props.item.ma_van_ban_den_kc,
                isUpdateViewCV: this.state.isBold,
                updateViewCV: this.updateViewCV,
                dataSource: this.props.dataSource,
                index: this.props.index
            })
        }
    }

    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible })
    }

    onViewFile = () => {
        this.toggleModal()
    }

    updateViewCV = async () => {
        let ma_van_ban_di_kc = this.props.item.ma_van_ban_di_kc
        let ma_ctcb = global.ma_ctcb_kc
        await API.VanBanDi.AU_VBDI_CNDX(ma_ctcb, ma_van_ban_di_kc).then((result) => {
            if (result) {
                this.setState({ isBold: false })
            }
        })
    }


    render() {
        this.styles = this.props.styles
        const { item, activeTab } = this.props
        let dataFile = item.file_van_ban ? getFileChiTietTTDH(item.file_van_ban.split(":")) : []
        let styleTemp = this.styles.textNoBold
        if (this.state.isBold) {
            styleTemp = this.styles.textBold
        }
        const isVBKhan1 = item.ma_cap_do_khan > 1 ? "red" : "#000000"
        const isVBKhan2 = item.ma_cap_do_khan > 1 ? "red" : "#808080"
        let borderRightWidth = (item.ma_xu_ly_di === this.props.cur_ma_xu_ly_di) ? 2.5 : 0.8
        let borderRightColor = (item.ma_xu_ly_di === this.props.cur_ma_xu_ly_di) ? "red" : "lightgray"

        if (Platform.OS === "ios" && Platform.isPad) {
            return (
                <CardItem style={[this.styles.CardItem, { borderRightWidth, borderRightColor }]} key={item.ma_van_ban_kc}>
                    <TouchableOpacity style={{ justifyContent: 'center', flex: 1 }} onPress={this.onPressItem}>
                        <Text numberOfLines={3} ellipsizeMode="tail" style={[styleTemp, { color: isVBKhan1 }]}>{RemoveNoiDungTTDH(item.trich_yeu)} </Text>
                        {
                            (item.so_ky_hieu !== null) && (
                                <Text numberOfLines={1} ellipsizeMode="tail" style={[styleTemp, { color: isVBKhan1 }]}>Số: {item.so_ky_hieu} </Text>
                            )
                        }
                        <Text numberOfLines={2} ellipsizeMode="tail" style={[this.styles.txtNoiDung, { color: isVBKhan2 }]}>Người ký: {item.nguoi_ky} </Text>
                        <Text numberOfLines={1} ellipsizeMode="tail" style={[this.styles.txtNoiDung, { color: isVBKhan2 }]}>Số đi: {item.so_di} </Text>
                        <Text numberOfLines={1} ellipsizeMode="tail" style={[this.styles.txtNoiDung, { color: isVBKhan2 }]}>Loại văn bản: {item.ten_loai_van_ban} </Text>
                        <Text numberOfLines={1} ellipsizeMode="tail" style={[this.styles.txtNoiDung, { color: isVBKhan2 }]}>Ngày: {item.ngay_nhan}</Text>
                    </TouchableOpacity>
                </CardItem>
            )
        } else {
            return (
                <CardItem style={this.styles.CardItem} key={item.ma_van_ban_kc}>
                    <TouchableOpacity style={{ justifyContent: 'center', flex: 1, left: 0, marginRight: 100 }} onPress={this.onPressItem}>
                        <Text numberOfLines={3} ellipsizeMode="tail" style={[styleTemp, { color: isVBKhan1 }]}>{RemoveNoiDungTTDH(item.trich_yeu)} </Text>
                        {
                            (item.so_ky_hieu !== null) && (
                                <Text numberOfLines={1} ellipsizeMode="tail" style={[styleTemp, { color: isVBKhan1 }]}>Số: {item.so_ky_hieu} </Text>
                            )
                        }
                        <Text numberOfLines={2} ellipsizeMode="tail" style={[this.styles.txtNoiDung, { color: isVBKhan2 }]}>Người ký: {item.nguoi_ky} </Text>
                        {
                            (item.nguoi_xu_ly_chinh) && (
                                <Text numberOfLines={1} ellipsizeMode="tail" style={[this.styles.txtNoiDung, { color: isVBKhan2 }]}>CB xử lý: {item.nguoi_xu_ly_chinh} </Text>
                            )
                        }
                        {
                            (item.han_xu_ly) && (
                                <Text numberOfLines={1} ellipsizeMode="tail" style={[this.styles.txtNoiDung, { color: isVBKhan2 }]}>Hạn xử lý: {item.han_xu_ly} </Text>
                            )
                        }
                        {
                            (item.ngay_xu_ly) && (
                                <Text numberOfLines={1} ellipsizeMode="tail" style={[this.styles.txtNoiDung, { color: isVBKhan2 }]}>Ngày xử lý: {item.ngay_xu_ly} </Text>
                            )
                        }
                        {
                            (item.so_ngay_con_lai) && (
                                <Text numberOfLines={1} ellipsizeMode="tail" style={[this.styles.txtNoiDung, { color: isVBKhan2 }]}>số ngày còn lại: {item.so_ngay_con_lai} </Text>
                            )
                        }
                    </TouchableOpacity>
                    <View style={{ width: 100, top: 0, right: 0, bottom: 0, position: "absolute", alignItems: 'center', justifyContent: "center" }}>
                        <Text style={this.styles.txtNgayNhan}>{item.ngay_nhan} </Text>
                    </View>
                    {
                        (dataFile.length === 1) && (
                            <ModalWebView
                                isModalVisible={this.state.isModalVisible}
                                title={dataFile[0].split("|")[1]}
                                url={dataFile[0].split("|")[0]}
                                path={dataFile[0].split("|")[4]}
                                toggleModal={() => this.toggleModal()}
                                isUpdateViewCV={this.state.isBold}
                                updateViewCV={() => this.updateViewCV()}
                            />
                        )
                    }
                    {
                        (dataFile.length > 1) && (
                            <ModalListFile
                                isModalVisible={this.state.isModalVisible}
                                dataFile={dataFile}
                                toggleModal={() => this.toggleModal()}
                                isUpdateViewCV={this.state.isBold}
                                updateViewCV={() => this.updateViewCV()}
                            />
                        )
                    }
                </CardItem>
            )
        }
    }
}

class VBDe_TheoDoi extends Component {
    constructor(props) {
        super(props)
        this.state = {
            loading: true,
            refreshing: false,
            dataSource: [],
            ma_xu_ly_di: 0,
            ma_van_ban_di_kc: 0,
            action: ''
        }
        this.page = 1
        this.size = 20
        this.nowYear = nowDate.getFullYear()
        this.stopLoadMore = false
    }

    onLoadData = () => {
        let searchValue = this.searchHeader.getKeyWord()
        API.VanBanDen.AU_VBDEN_PCTHEODOI(
            global.ma_can_bo,
            global.ma_ctcb_kc,
            global.ma_don_vi_quan_tri,
            this.page,
            this.size,
            searchValue
        ).then(response => {
            this.setState({ dataSource: [...this.state.dataSource, ...response.data] }, () => {
                this.setState({ loading: false, refreshing: false })
            })
            if (this.page == response.total_page) {
                this.stopLoadMore = true
            }

        })
    }

    componentDidMount() {
        this.didBlurSubscription = this.props.navigation.addListener('willFocus', payload => { CheckSession(this.props.navigation) })
        API.Login.AU_DSNVCB(global.ma_ctcb_kc, this.props.dispatch)
        this.onLoadData();
    }



    componentWillUnmount = () => {
        this.didBlurSubscription.remove()
    };

    shouldComponentUpdate = (nextProps, nextState) => {
        if (this.state.loading !== nextState.loading ||
            this.state.refreshing !== nextState.refreshing ||
            this.state.dataSource !== nextState.dataSource ||
            this.state.ma_xu_ly_di !== nextState.ma_xu_ly_di ||
            this.state.ma_van_ban_di_kc !== nextState.ma_van_ban_di_kc ||
            this.props.cb_dsnvcb !== nextProps.cb_dsnvcb) {
            return true
        }
        return false
    };



    onRefresh = () => {
        if (!this.state.loading) {
            this.page = 1
            this.stopLoadMore = false
            this.setState({ dataSource: [], refreshing: true, loading: true, ma_xu_ly_di: 0 })
            this.onLoadData();
        }
    }

    loadMore = () => {
        if (!this.state.loading && this.stopLoadMore === false) {
            this.page = this.page + 1
            this.onLoadData();
        }
    }

    onSearchData = () => {
        if (!this.state.loading) {
            this.nowYear = nowDate.getFullYear()
            this.page = 1
            this.stopLoadMore = false
            this.setState({ dataSource: [], loading: true, ma_xu_ly_di: 0 })
            this.onLoadData();
        }
    }


    renderVBDi_Detail_iPad = () => {
        let { ma_xu_ly_di, ma_van_ban_di_kc } = this.state
        if (ma_xu_ly_di !== 0) {
            return (
                <VBDi_Details_iPad
                    navigation={this.props.navigation}
                    isDuyet={false}
                    ma_xu_ly_di={ma_xu_ly_di}
                    ma_van_ban_di_kc={ma_van_ban_di_kc}
                    activeTab={this.activeTab}
                />
            )
        } else {
            return null
        }
    }

    render() {

        this.styles = StyleSheet.create({
            CardItem: {
                flex: 1,
                borderTopWidth: 0,
                borderBottomWidth: 0.55,
                borderLeftWidth: 0,
                borderRightWidth: 0,
                borderBottomColor: "lightgray",
                paddingLeft: 8,
                paddingTop: 4,
                paddingBottom: 4,
                alignItems: 'center',
                justifyContent: "center",
                marginBottom: 1,
            },
            colTieuDe: {
                width: "70%",
                justifyContent: "center"

            },
            textBold: {
                color: "#000000",
                fontSize: this.props.fontSize.FontSizeNorman,
                fontWeight: "bold",
                paddingBottom: AppConfig.defaultPadding
            },
            textNoBold: {
                color: "#000000",
                fontSize: this.props.fontSize.FontSizeNorman,
                paddingBottom: AppConfig.defaultPadding
            },
            txtNgayNhan: {
                width: 100,
                color: "#808080",
                fontSize: this.props.fontSize.FontSizeSmall,
                textAlign: "center",
                paddingBottom: AppConfig.defaultPadding
            },
            txtNoiDung: {
                color: "#808080",
                fontSize: this.props.fontSize.FontSizeSmall,
                paddingBottom: AppConfig.defaultPadding
            },
            colNgayGui: {
                width: "30%",
                justifyContent: "center",
                alignItems: "flex-end"
            },
            menuTrigger: {
                width: 40,
                height: 30,
                justifyContent: "center",
                alignItems: 'center'
            },
            menuTriggerIcon: {
                fontSize: this.props.fontSize.FontSizeXXXLarge,
                color: "#B0BEC5"
            },
            menuOptionsContainer: {
                width: 200
            },
            menuOptions: {
                borderBottomColor: AppConfig.defaultLineColor,
                borderBottomWidth: 0.5
            },
            menuOptionsText: {
                color: AppConfig.headerBackgroundColor,
                fontSize: this.props.fontSize.FontSizeNorman,
                paddingTop: AppConfig.defaultPadding,
                paddingBottom: AppConfig.defaultPadding
            },
            menuOptionsTextDelete: {
                color: "red",
                fontSize: this.props.fontSize.FontSizeNorman,
                paddingTop: AppConfig.defaultPadding,
                paddingBottom: AppConfig.defaultPadding
            }
        })

        if (Platform.OS === "ios" && Platform.isPad) {
            return (
                <Container style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
                    <HeaderWithLeft
                        title="Theo dõi xử lý"
                        buttonName="menu"
                        onPressLeftButton={_ => { this.props.navigation.dispatch(DrawerActions.toggleDrawer()) }}
                    />
                    <View style={{ flex: 1 }}>
                        <SearchHeader
                            ref={(ref) => { this.searchHeader = ref }}
                            onSearchData={this.onSearchData}
                            fontSize={this.props.fontSize}
                        />
                        {
                            (this.state.loading) ? (
                                <Spinner color={AppConfig.blueBackground} />
                            ) : (
                                <CardItem style={{ flex: 1, flexDirection: "row", paddingLeft: 5, paddingRight: 5 }}>
                                    <View style={{ width: 200 }}>
                                        <FlatList
                                            ref={(ref) => { this.flatListRef = ref }}
                                            data={this.state.dataSource}
                                            keyExtractor={(item, index) => "key" + item.ma_van_ban_kc}
                                            contentContainerStyle={{ paddingBottom: 0 }}
                                            renderItem={({ item }) => {
                                                return (
                                                    <VBDE_Details_TD
                                                        item={item}
                                                        ma_ctcb_kc={global.ma_ctcb_kc}
                                                        navigation={this.props.navigation}
                                                        cur_ma_xu_ly_den={this.state.ma_xu_ly_den}
                                                        onPress={() => {
                                                            this.setState({
                                                                ma_xu_ly_den: item.ma_xu_ly_den,
                                                                ma_van_ban_den_kc: item.ma_van_ban_den_kc
                                                            })
                                                        }}
                                                        index={index}
                                                        dataSource={this.state.dataSource}
                                                        fontSize={this.props.fontSize}
                                                        styles={this.styles}
                                                    />
                                                )
                                            }}
                                            onRefresh={this.onRefresh}
                                            refreshing={this.state.refreshing}
                                            onEndReached={this.loadMore}
                                            onEndReachedThreshold={5}
                                            removeClippedSubviews={true}
                                        />
                                    </View>
                                    <View style={{ flex: 1 }}>
                                        {this.renderVBDi_Detail_iPad()}
                                    </View>
                                </CardItem>
                            )
                        }
                    </View>
                    <FooterTab tabActive="vbdi" navigation={this.props.navigation} />
                </Container>
            );
        } else {
            return (
                <Container style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
                    <HeaderWithLeft
                        title="Theo dõi xử lý"
                        buttonName="menu"
                        onPressLeftButton={_ => { this.props.navigation.dispatch(DrawerActions.toggleDrawer()) }}
                    />
                    <View style={{ flex: 1, paddingTop: 5 }}>
                        <SearchHeader
                            ref={(ref) => { this.searchHeader = ref }}
                            onSearchData={this.onSearchData}
                            fontSize={this.props.fontSize}
                        />
                        {
                            (this.state.loading) ? (
                                <View><Spinner color={AppConfig.blueBackground} /></View>
                            ) : (
                                <FlatList
                                    ref={(ref) => { this.flatListRef = ref }}
                                    data={this.state.dataSource}
                                    keyExtractor={(item, index) => "key" + item.ma_van_ban_kc}
                                    contentContainerStyle={{ paddingBottom: 0 }}
                                    renderItem={({ item, index }) => {
                                        return (
                                            <VBDE_Details_TD
                                                item={item}
                                                ma_ctcb_kc={global.ma_ctcb_kc}
                                                navigation={this.props.navigation}
                                                cur_ma_xu_ly_den={this.state.ma_xu_ly_den}
                                                onPress={() => {
                                                    this.setState({
                                                        ma_xu_ly_den: item.ma_xu_ly_den,
                                                        ma_van_ban_den_kc: item.ma_van_ban_den_kc
                                                    })
                                                }}
                                                dataSource={this.state.dataSource}
                                                index={index}
                                                fontSize={this.props.fontSize}
                                                styles={this.styles}
                                            />
                                        )
                                    }}
                                    onRefresh={this.onRefresh}
                                    refreshing={this.state.refreshing}
                                    onEndReached={this.loadMore}
                                    onEndReachedThreshold={5}
                                    removeClippedSubviews={true}
                                />
                            )
                        }
                    </View>
                    <FooterTab tabActive="vbden" navigation={this.props.navigation} />
                </Container>
            );
        }

    }
}

function mapStateToProps(state) {
    return {
        cb_dsnvcb: state.cb_dsnvcb.data,
        fontSize: state.fontSize,
    }
}

export default connect(mapStateToProps)(VBDe_TheoDoi)