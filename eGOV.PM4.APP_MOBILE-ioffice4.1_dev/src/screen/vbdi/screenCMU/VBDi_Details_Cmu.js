import React, { Component } from 'react';
import { View, ScrollView, StyleSheet, Alert, TouchableOpacity } from "react-native"
import { connect } from "react-redux"
import { Container, Icon, Button, Text, Card, CardItem, Toast } from "native-base"
import { StackActions } from "react-navigation"
import { AppConfig } from "../../../AppConfig"
import { RemoveHTMLTag, ConvertDateTimeDetail, checkVersionWeb, resetStack, ToastSuccess } from "../../../untils/TextUntils"
import HeaderWithLeft from "../../../components/Header/HeaderWithLeft"
import API from "../../../networks"
import ViewFileCA from "../../../components/ViewFileCA"
import ViewFile from "../../../components/ViewFile"
import SignInfo from "../../../components/SignInfo"

class Table_ChiTiet_VBDi extends Component {
    constructor(props) {
        super(props)
        this.state = {
            strVBLQ: "",
            strPhieuTrinhHGG: "",
        }
    }

    componentWillReceiveProps(nextProps) {
        this.getVBLQ(nextProps)
    };

    getVBLQ = (nextProps) => {
        this.setState({ strVBLQ: "" })
        if (nextProps.dataDetails.length > 0) {
            let ma_van_ban = nextProps.dataDetails[0].ma_van_ban_kc
            API.VanBanDen.AU_VANBAN_DSVBLQDK(ma_van_ban).then((strVBLQ) => {
                this.setState({ strVBLQ })
            })
        }
    }

    render() {
        this.styles = this.props.styles
        if (this.props.dataDetails.length > 0) {
            const { trich_yeu, so_ky_hieu, ngay_ban_hanh, nguoi_ky,
                ten_co_quan_ban_hanh, phong_ban_soan, so_ban_phat_hanh, so_di,
                ten_loai_van_ban, ngay_luu, ma_dinh_danh_vb, ten_so_vb_di,
                file_dinh_kem, file_van_ban_bs, ma_yeu_cau } = this.props.dataDetails[0]
            let isShowFileCA = false
            let isShowFile = (file_van_ban_bs !== null) ? true : false
            return (
                <Card noShadow style={this.styles.Card}>
                    {
                        (trich_yeu !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Trích yếu:</Text>
                                <Text style={[this.styles.textRight, { fontWeight: "500" }]}>{RemoveHTMLTag(trich_yeu)}</Text>
                            </CardItem>
                        )
                    }
                    {
                        (so_ky_hieu !== null && so_di !== null) && (
                            <CardItem style={[this.styles.CardItem, { flexWrap: "wrap", flexShrink: 1 }]}>
                                <Text style={this.styles.textLeft}>Số ký hiệu:</Text>
                                <Text style={[this.styles.textRight, { color: "red" }]}>
                                    {so_ky_hieu} &nbsp;&nbsp;
                                </Text>
                                <CardItem style={[this.styles.CardItem, { paddingLeft: -4, paddingBottom: -5 }]}>
                                    <Text style={this.styles.textLeft}>Số đi:</Text>
                                    <Text style={[this.styles.textRight]}>
                                        {so_di}
                                    </Text>
                                </CardItem>
                            </CardItem>
                        )
                    }
                    {
                        (so_ky_hieu === null && so_di !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Số đi:</Text>
                                <Text style={[this.styles.textRight]}>
                                    {so_di}
                                </Text>
                            </CardItem>
                        )
                    }
                    {
                        (nguoi_ky !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Người ký văn bản:</Text>
                                <Text style={[this.styles.textRight]}>
                                    {nguoi_ky}
                                </Text>
                            </CardItem>
                        )
                    }
                    {
                        (ten_loai_van_ban !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Loại văn bản:</Text>
                                <Text style={[this.styles.textRight]}>
                                    {ten_loai_van_ban}
                                </Text>
                            </CardItem>
                        )
                    }
                    {
                        (phong_ban_soan !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Phòng ban soạn:</Text>
                                <Text style={[this.styles.textRight, { color: "red" }]}>{phong_ban_soan}</Text>
                            </CardItem>
                        )
                    }
                    {
                        (so_ban_phat_hanh !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Số bản phát hành:</Text>
                                <Text style={[this.styles.textRight]}>
                                    {so_ban_phat_hanh}
                                </Text>
                            </CardItem>
                        )
                    }
                    {
                        (ten_co_quan_ban_hanh !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>CQBH:</Text>
                                <Text style={[this.styles.textRight]}>{ten_co_quan_ban_hanh}</Text>
                            </CardItem>
                        )

                    }
                    {
                        (ngay_ban_hanh !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Ngày ban hành:</Text>
                                <Text style={[this.styles.textRight]}>
                                    {ngay_ban_hanh}
                                </Text>
                            </CardItem>
                        )
                    }
                    {
                        (ngay_luu !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Ngày lưu:</Text>
                                <Text style={[this.styles.textRight]}>
                                    {ConvertDateTimeDetail(ngay_luu)}
                                </Text>
                            </CardItem>
                        )
                    }
                    {
                        (ma_dinh_danh_vb !== null && ma_dinh_danh_vb !== "Không có thông tin") && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Định danh:</Text>
                                <Text style={[this.styles.textRight]}>{ma_dinh_danh_vb}</Text>
                            </CardItem>
                        )
                    }
                    {
                        (ma_yeu_cau !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Vai trò:</Text>
                                <Text style={[this.styles.textRight]}>
                                    {ma_yeu_cau === 2 ? "Xử lý chính" : ma_yeu_cau === 3 ? "Phối hợp" : "Xem để biết"}
                                </Text>
                            </CardItem>
                        )
                    }
                    {
                        (ten_so_vb_di !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Sổ văn bản đi:</Text>
                                <Text style={[this.styles.textRight]}>
                                    {ten_so_vb_di}
                                </Text>
                            </CardItem>
                        )
                    }
                    {
                        (isShowFileCA === true && isShowFile === true) && (
                            <View>
                                <CardItem style={this.styles.CardItem}>
                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                        <Text
                                            style={this.styles.textLeft}
                                            numberOfLines={1} ellipsizeMode="tail"
                                        >Tệp tin đính kèm: </Text>

                                    </View>
                                </CardItem>
                                <CardItem style={this.styles.CardItem}>
                                    <ViewFileCA
                                        ref={ref => { this.ViewFile = ref }}
                                        parentView="VBDiDuyet"
                                        trich_yeu={trich_yeu}
                                        src_file={file_van_ban_bs}
                                        isPhieuTrinh={false}
                                        isUpdateViewCV={this.props.isUpdateViewCV}
                                        updateViewCV={this.props.updateViewCV}
                                        navigation={this.props.navigation}
                                        fontSize={this.props.fontSize}
                                        ma_van_ban_di={this.props.ma_van_ban_di}
                                        ma_xu_ly_di={this.props.ma_xu_ly_di}
                                    />
                                </CardItem>
                            </View>
                        )
                    }
                    {
                        (isShowFileCA === false && isShowFile === true) && (
                            <View>
                                <CardItem style={this.styles.CardItem}>
                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                        <Text style={this.styles.textLeft} numberOfLines={1} ellipsizeMode="tail">Tệp tin đính kèm:</Text>
                                    </View>
                                </CardItem>
                                <CardItem style={this.styles.CardItem}>
                                    <ViewFile
                                        ref={ref1 => { this.ViewFile1 = ref1 }}
                                        src_file={file_van_ban_bs}
                                        isUpdateViewCV={this.props.isUpdateViewCV}
                                        updateViewCV={this.props.updateViewCV}
                                        navigation={this.props.navigation}
                                        fontSize={this.props.fontSize}
                                        ma_van_ban_di={this.props.ma_van_ban_di}
                                    />
                                </CardItem>
                            </View>
                        )
                    }
                    {
                        (file_dinh_kem !== null && file_dinh_kem !== "null") && (
                            <View>
                                <CardItem style={this.styles.CardItem}>
                                    <Text style={this.styles.textLeft}>Văn bản phiếu trình:</Text>
                                </CardItem>
                                <CardItem style={this.styles.CardItem}>
                                    {
                                        (isShowFileCA) ? (
                                            <ViewFileCA
                                                ref={ref => { this.ViewFile = ref }}
                                                src_file={file_dinh_kem}
                                                isPhieuTrinh={true}
                                                navigation={this.props.navigation}
                                                fontSize={this.props.fontSize}
                                                ma_van_ban_di={this.props.ma_van_ban_di}
                                                ma_xu_ly_di={this.props.ma_xu_ly_di}
                                               
                                            />
                                        ) : (
                                                <ViewFile
                                                    src_file={file_dinh_kem}
                                                    navigation={this.props.navigation}
                                                    fontSize={this.props.fontSize}
                                                />
                                            )
                                    }
                                </CardItem>
                            </View>
                        )
                    }
                    {
                        (this.state.strVBLQ !== "") && (
                            <View>
                                <CardItem style={this.styles.CardItem}>
                                    <Text style={this.styles.textLeft}>Văn bản liên quan:</Text>
                                </CardItem>
                                <CardItem style={this.styles.CardItem}>
                                    <ViewFile
                                        src_file={this.state.strVBLQ}
                                        navigation={this.props.navigation}
                                        fontSize={this.props.fontSize}
                                    />
                                </CardItem>
                            </View>
                        )
                    } 
                </Card>
            )
        } else {
            return <View></View>
        }
    }
}

class Table_ButPhe_VBDi extends Component {
    renderDataNotes = (dataNotes) => {
        let result = []
        for (let item of dataNotes) {
            result.push(
                <View key={item.ma_xu_ly_di_kc} style={this.styles.viewTitle}>
                    <CardItem style={this.styles.CardItem}>
                        <Text style={[this.styles.textRight, { color: AppConfig.blueBackground }]}>
                            Người gửi: {item.can_bo_gui}
                        </Text>
                    </CardItem>
                    <CardItem style={this.styles.CardItem}>
                        <Text style={[this.styles.textRight]}>
                            Người nhận: {item.can_bo_nhan}
                        </Text>
                    </CardItem>
                    {
                        (item.ngay_nhan !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={[this.styles.textRight]}>
                                    Ngày nhận: {ConvertDateTimeDetail(item.ngay_nhan)}
                                </Text>
                            </CardItem>
                        )
                    }
                    {
                        (item.ngay_xu_ly !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={[this.styles.textRight]}>
                                    Ngày xử lý: {ConvertDateTimeDetail(item.ngay_xu_ly)}
                                </Text>
                            </CardItem>
                        )
                    }
                    {
                        (item.noi_dung_chuyen_ctcb_khac !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={[this.styles.textRight]}>
                                    Nội dung yêu cầu: {RemoveHTMLTag(item.noi_dung_chuyen_ctcb_khac)}
                                </Text>
                            </CardItem>
                        )
                    }
                </View>
            )
        }
        return result
    }

    render() {
        this.styles = this.props.styles
        if (this.props.dataNotes.length > 0) {
            return (
                <Card noShadow style={this.styles.Card}>
                    <CardItem style={this.styles.CardItem}>
                        <Text style={[this.styles.textRight, { fontSize: this.props.fontSize.FontSizeLarge, fontWeight: "bold", }]}>
                            Tổng hợp ý kiến xử lý
                        </Text>
                    </CardItem>
                    {this.renderDataNotes(this.props.dataNotes)}
                </Card>
            )
        } else {
            return (
                <View></View>
            )
        }
    }
}

class VBDi_Details_Cmu extends Component {
    constructor(props) {
        super(props)
        this.state = {
            dataDetails: [],
            dataNotes: [],
            index: 0
        }
        this.ma_xu_ly_di = -1
        this.ma_van_ban_di = -1
        this.dataSource = []
    }

    componentDidMount = () => {
        this.dataSource = this.props.navigation.getParam("dataSource", [])
        let index = this.props.navigation.getParam("index", 0)
        this.loadData(index)
    }

    loadData = (index) => {
        this.ma_xu_ly_di = this.dataSource[index].ma_xu_ly_di
        this.ma_van_ban_di = this.dataSource[index].ma_van_ban_di_kc
        this.setState({ index, dataDetails: [], dataNotes: [] })
        this.getDetails()
        this.getNotes()
    }

    getDetails = async () => {
        let ma_van_ban_di = this.ma_van_ban_di
        let ma_ctcb = global.ma_ctcb_kc
        await API.VanBanDi.AU_VBDI_CTVBD(ma_van_ban_di, ma_ctcb).then((dataDetails) => {
            this.setState({ dataDetails })
        })
    }
    getNotes = async () => {
        let ma_van_ban_di = this.ma_van_ban_di
        let ma_don_vi_quan_tri = global.ma_don_vi_quan_tri
        await API.VanBanDi.AU_VBDI_QTXLVBD(ma_van_ban_di, ma_don_vi_quan_tri).then((dataNotes) => {
            this.setState({ dataNotes })
        })
    }
    renderPre = () => {
        let indexPre = this.state.index - 1
        if (indexPre >= 0) {
            return (
                <View style={this.styles.viewLeftBottomButton}>
                    <TouchableOpacity style={this.styles.buttonBottom} onPress={() => { this.loadData(indexPre) }}>
                        <Icon name="reply" type="FontAwesome" style={this.styles.iconBottom} />
                    </TouchableOpacity>
                </View>
            )
        }
    }

    renderNext = () => {
        let indexNext = this.state.index + 1
        let length = this.dataSource.length
        if (length !== 0 && indexNext < length) {
            return (
                <View style={this.styles.viewRightBottomButton}>
                    <TouchableOpacity style={this.styles.buttonBottom} onPress={() => { this.loadData(indexNext) }}>
                        <Icon name="share" type="FontAwesome" style={this.styles.iconBottom} />
                    </TouchableOpacity>
                </View>
            )
        }
    }

    render() {

        this.styles = StyleSheet.create({
            scrollContainer: {
                flex: 1,
                bottom: 0,
                backgroundColor: AppConfig.grayBackground
            },
            viewTieuDe: {
                backgroundColor: AppConfig.grayBackground,
                justifyContent: "center", alignContent: "center",
                flexWrap: "wrap",
                padding: 4,
            },
            textTieuDe: {
                fontSize: this.props.fontSize.FontSizeXLarge,
                fontWeight: "400",
                textAlign: "center"
            },

            //Table_ChiTiet_VBDEN
            Card: {
                marginLeft: 6,
                marginRight: 6,
                marginTop: 0,
                padding: 10,
                borderRadius: 6,
            },
            CardItem: {
                marginTop: AppConfig.defaultPadding,
                marginBottom: AppConfig.defaultPadding,
            },
            CardItemButton: {
                marginTop: 10,
                paddingTop: 10,
                borderTopColor: AppConfig.defaultLineColor,
                borderTopWidth: AppConfig.defaultLineWidth,
                flexDirection: 'row',
                flexWrap: "wrap",
                justifyContent: "center",
                alignItems: "center"
            },
            Button: {
                margin: 3,
                height: 40,
                justifyContent: "center",
                alignItems: "center"
            },
            ButtonText: {
                fontSize: this.props.fontSize.FontSizeNorman,
                textAlign: "center"
            },
            textLeft: {
                fontSize: this.props.fontSize.FontSizeNorman,
                color: AppConfig.grayText,
                alignSelf: "flex-start"
            },
            textRight: {
                paddingLeft: 5,
                fontSize: this.props.fontSize.FontSizeNorman,
                flexWrap: "wrap",
                flexShrink: 1,
            },
            viewLeftBottomButton: {
                left: 15,
                bottom: 15,
                position: "absolute"
            },
            viewRightBottomButton: {
                right: 15,
                bottom: 15,
                position: "absolute"
            },
            buttonBottom: {
                width: 40,
                height: 40,
                borderRadius: 20,
                backgroundColor: "transparent",
                borderWidth: 0.7,
                borderColor: "#BDBDBD",
                justifyContent: "center",
                alignItems: "center",
            },
            iconBottom: {
                fontSize: this.props.fontSize.FontSizeSmall,
                color: "#BDBDBD"
            },
            viewTitle: {
                paddingTop: 5,
                paddingBottom: 10,
                borderColor: AppConfig.defaultLineColor,
                borderTopWidth: AppConfig.defaultLineWidth
            }
        })

        const popAction = StackActions.pop({ n: 1 })
        return (
            <Container style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
                <HeaderWithLeft title="Chi tiết văn bản đi" buttonName="arrow-back" onPressLeftButton={() => {
                    if (this.isFromNotify) {
                        if (this.isDuyet) {
                            this.props.navigation.dispatch(resetStack("VBDiDuyet"))
                        } else {
                            this.props.navigation.dispatch(resetStack("VBDiXuLy"))
                        }
                    } else {
                        this.props.navigation.dispatch(popAction)
                    }
                }} />
                <ScrollView
                    bounces={false}
                    removeClippedSubviews={true}
                    scrollEnabled={true}
                    style={this.styles.scrollContainer}
                >
                    <Table_ChiTiet_VBDi
                        dataDetails={this.state.dataDetails}
                        renderButton={this.renderButton}
                        navigation={this.props.navigation}
                        ma_van_ban_di={this.ma_van_ban_di}
                        ma_xu_ly_di={this.ma_xu_ly_di}
                        parent={this}
                        fontSize={this.props.fontSize}
                        styles={this.styles}
                    />
                    <SignInfo
                        ma_van_ban={this.ma_van_ban_di}
                        type="di"
                        fontSize={this.props.fontSize}
                    />
                    <Table_ButPhe_VBDi
                        dataNotes={this.state.dataNotes}
                        fontSize={this.props.fontSize}
                        styles={this.styles}
                    />
                </ScrollView>
                {this.renderPre()}
                {this.renderNext()}
            </Container>
        );
    }
}

function mapStateToProps(state) {
    return {
        fontSize: state.fontSize,
    }
}

export default connect(mapStateToProps)(VBDi_Details_Cmu)