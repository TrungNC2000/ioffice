import React, { Component } from 'react';
import { View, StyleSheet, FlatList, Alert, PixelRatio } from "react-native"
import { Container, Text, Card, CardItem, Toast, Textarea, CheckBox } from "native-base"
import { connect } from "react-redux"
import { StackActions } from "react-navigation"
import { AppConfig } from "../../AppConfig"
import HeaderWithLeftRight from "../../components/Header/HeaderWithLeftRight"
import SearchList from "../../components/SearchList"
import API from "../../networks"
import * as Models from "../../Models"
import { resetStack, ToastSuccess } from "../../untils/TextUntils"

class ListChuyenVienItem extends Component {
    shouldComponentUpdate(nextProps, nextState) {
        if (this.props.checked !== nextProps.checked) {
            return true
        }
        console.log('nextProps'+ nextProps.checked)
        return false
    }

    render() {
        this.styles = this.props.styles
        let { ma_ctcb_kc, ho_va_ten_can_bo, ten_chuc_vu, ten_don_vi } = this.props.item
        return (
            <CardItem style={{ alignItems: 'center', paddingTop: 4, paddingBottom: 4, borderTopWidth: AppConfig.defaultLineWidth, borderTopColor: AppConfig.defaultLineColor, }} key={ma_ctcb_kc}>
                <CardItem style={{ width: 90, justifyContent: "center", alignItems: "center" }}>
                    <CheckBox onPress={() => this.props.checkExistOne(ma_ctcb_kc)} checked={this.props.checked} color={AppConfig.blueBackground} style={{ marginRight: 20 }}></CheckBox>
                </CardItem>
                <Card noShadow style={{ flex: 1, borderTopWidth: 0, borderBottomWidth: 0, borderLeftWidth: 0, borderRightWidth: 0, }}>
                    <CardItem>
                        <Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>{ho_va_ten_can_bo}</Text>
                    </CardItem>
                    <CardItem>
                        <Text style={[this.styles.TextTitle, { color: "#808080" }]}>Chức vụ: {ten_chuc_vu}</Text>
                    </CardItem>
                    <CardItem>
                        <Text style={[this.styles.TextTitle, { color: "#808080" }]}>Đơn vị: {ten_don_vi}</Text>
                    </CardItem>
                </Card>
            </CardItem>
        )
    }
}

class VBDi_Details_LDChuyenKTTT extends Component {
    constructor(props) {
        super(props)
        this.state = {
            listCV: [],
            checkedCV: null,
            yKienXuLy: "",
            checkSms: false,
            checkEmail: false
        }
        this.npVBDi_Details_LDChuyenKTTT= new Models.npVBDi_Details_LDChuyenKTTT()
        this.listSearchDefault = []
        this.isSending = false
    }

    componentDidMount = () => {
        this.npVBDi_Details_LDChuyenKTTT = this.props.navigation.getParam("npVBDi_Details_LDChuyenKTTT")
        this.getListChuyenVien(this.npVBDi_Details_LDChuyenKTTT.ma_don_vi_tao)    
    }

    getListChuyenVien = (ma_don_vi_tao) => {
        API.VanBanDi.AU_VBDI_DSCVPCUT(ma_don_vi_tao).then((listCV) => {
            this.listSearchDefault = listCV
            this.setState({ listCV })
            this.setState({checkedCV: listCV[0].ma_ctcb_kc});
        })
    }

    onCheckSms = () => {
        this.setState({ checkSms: !this.state.checkSms })
    }

    onCheckEmail = () => {
        this.setState({ checkEmail: !this.state.checkEmail })
    }

    onPressSend = () => {
        let { yKienXuLy, checkSms, checkedCV } = this.state
        let listCV = this.listSearchDefault

        if (checkedCV === null) {
            Toast.show({ text: "Vui lòng chọn chuyên viên!", type: "warning", buttonText: "Đóng", position: "top", duration: 2000 })
            return null
        }

        if (this.isSending) {
            Toast.show({ text: "Vui lòng chờ trong giây lát!", type: "warning", buttonText: "Đóng", position: "top", duration: 2000 })
            return null
        }

        let ma_van_ban_di = this.npVBDi_Details_LDChuyenKTTT.ma_van_ban_di
        let ma_ctcb_gui = global.ma_ctcb_kc
        let nguoiNhan = listCV.filter(el => el.ma_ctcb_kc === checkedCV)[0]
        let noi_dung_chuyen = yKienXuLy
        let file_dinh_kem = this.npVBDi_Details_LDChuyenKTTT.file_van_ban
        let ma_xu_ly_di_cha = this.npVBDi_Details_LDChuyenKTTT.ma_xu_ly_di
        let sms = checkSms ? 1 : 0
        let chuoi_chi_tiet_temp = []
        let chi_tiet_temp = new Object()
        chi_tiet_temp.ma_ctcb_nhan = nguoiNhan.ma_ctcb_kc
        chi_tiet_temp.ma_yeu_cau = 2
        chi_tiet_temp.han_xu_ly = ""
        chi_tiet_temp.sms = checkSms ? 1 : 0
        chi_tiet_temp.ma_don_vi_nhan_temp = 0
        chuoi_chi_tiet_temp.push(chi_tiet_temp)
        
        Alert.alert(
            "Xác nhận",
            "Bạn muốn chuyển cho " + nguoiNhan.ho_va_ten_can_bo + "?",
            [
                { text: "Không", onPress: () => console.log("Logout"), style: "cancel" },
                {
                    text: "Có", onPress: () => {
                        this.isSending = true
                        API.KYSO.AU_KYSO_GFDK(ma_van_ban_di, global.ma_ctcb_kc).then((file_moi) => { 
                            API.VanBanDi.AU_VBDI_CHUYENPC(nguoiNhan, chuoi_chi_tiet_temp, file_dinh_kem, file_dinh_kem, ma_ctcb_gui,ma_van_ban_di,ma_xu_ly_di_cha,noi_dung_chuyen ,sms, file_moi, this.npVBDi_Details_LDChuyenKTTT.isDuyet).then((response) => {
                                if (response !== "") {
                                    API.Login.AU_DSNVCB(global.ma_ctcb_kc, this.props.dispatch)
                                    if (sms === 1) {
                                        let noi_dung = this.npVBDi_Details_LDChuyenKTTT.trich_yeu
                                        let chuoi_di_dong_nhan = listCV.filter(el => el.ma_ctcb_kc === checkedCV)[0].di_dong_can_bo
                                        API.NhanTinSMS.AU_SMS_GTN(noi_dung, chuoi_di_dong_nhan)
                                    }
                                    this.isSending = false
                                    API.Login.AU_PUSH_FCM_APP(nguoiNhan.ma_ctcb_kc, "vanbandichoxuly", this.npVBDi_Details_LDChuyenKTTT.trich_yeu, 0, 0, response, 0)
                                    if(this.npVBDi_Details_LDChuyenKTTT.isDuyet){
                                        ToastSuccess("Chuyển thành công", () => { this.props.navigation.dispatch(resetStack("VBDiDuyet")) })
                                    }else{
                                        ToastSuccess("Chuyển thành công", () => { this.props.navigation.dispatch(resetStack("VBDiXuLy")) })
                                    }
                                   
                                } else {
                                    this.isSending = false
                                    Toast.show({ text: "Lỗi. Vui lòng thử lại sau!", type: "danger", buttonText: "Đóng", position: "top", duration: 2000 })
                                }
                            })
                        })
                    }
                }
            ],
            { cancelable: false }
        )
    }

    checkExistOne = (ma_ctcb_kc) => {
        if (ma_ctcb_kc === this.state.checkedCV) {
            this.setState({ checkedCV: null })
        } else {
            this.setState({ checkedCV: ma_ctcb_kc })
        }
    }

    onSearchList = () => {
        if (this.listSearchDefault.length > 0) {
            let keyWord = this.searchList.getKeyWord()
            let arrResult = this.listSearchDefault.filter(el => el.ho_va_ten_can_bo.toLowerCase().indexOf(keyWord.toLowerCase()) !== -1)
            this.setState({ listCV: arrResult }, () => this.searchList.setKeyWord(keyWord))
        }
    }

    render() {

        this.styles = StyleSheet.create({
            scrollContainer: {
                flex: 1,
                marginLeft: 10,
                marginRight: 10
            },
            CardItem: {
                paddingTop: 10,
                paddingBottom: 10,
                alignItems: 'center'
            },
            CardItemLeft: {
                width: 90
            },
            CardItemRight: {
                flex: 1
            },
            TextTitle: {
                fontSize: this.props.fontSize.FontSizeSmall,
            },
            TextInput: {
                flex: 1,
                fontFamily: AppConfig.fontFamily,
                fontSize: this.props.fontSize.FontSizeNorman / PixelRatio.getFontScale(),
                color: "#000000",
                borderColor: AppConfig.defaultLineColor,
                borderWidth: AppConfig.defaultLineWidth,
                borderRadius: 5,
                marginRight: 2,
                paddingLeft: 5,
                paddingTop: 7,
                minHeight: 40,
            },
        })

        const popAction = StackActions.pop({
            n: 1
        })

        return (
            <Container style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
                <HeaderWithLeftRight
                    title="Chuyển chuyên viên pháp chế"
                    buttonLeftName="arrow-back"
                    buttonRightName="done"
                    onPressLeftButton={() => { this.props.navigation.dispatch(popAction) }}
                    onPressRightButton={this.onPressSend}
                />
                <View style={this.styles.scrollContainer}>
                    <CardItem style={this.styles.CardItem}>
                        <CardItem style={[this.styles.CardItemLeft]}>
                            <Text style={[this.styles.TextTitle]}>Ý kiến xử lý</Text>
                        </CardItem>
                        <CardItem style={[this.styles.CardItemRight]}>
                            <Textarea
                                autoCorrect={false}
                                style={this.styles.TextInput}
                                rowSpan={3}
                                value={this.state.yKienXuLy}
                                onChangeText={(text) => { this.setState({ yKienXuLy: text }) }}
                            />
                        </CardItem>
                    </CardItem>
                    <CardItem style={this.styles.CardItem}>
                        <CardItem style={[this.styles.CardItemLeft]}></CardItem>
                        <CardItem style={[this.styles.CardItemRight, { marginLeft: -10 }]}>
                            <CheckBox onPress={this.onCheckSms} checked={this.state.checkSms} color={AppConfig.blueBackground}></CheckBox>
                            <Text style={[this.styles.TextTitle, { marginLeft: 20 }]}>Sms        </Text>
                        </CardItem>
                    </CardItem>
                    <CardItem style={this.styles.CardItem}>
                        <Text style={[this.styles.TextTitle, { fontSize: this.props.fontSize.FontSizeLarge, }]}>Danh sách chuyên viên:</Text>
                    </CardItem>
                    <CardItem>
                        <SearchList ref={(ref) => { this.searchList = ref }} onSearchList={this.onSearchList} fontSize={this.props.fontSize} />
                    </CardItem>
                    <FlatList
                        data={this.state.listCV}
                        extraData={this.state}
                        keyExtractor={(item, index) => item.ma_ctcb_kc.toString()}
                        renderItem={({ item }) => {
                            let flag = false;
                            if(this.state.checkedCV == null){
                                flag = item.ma_ctcb_kc === this.state.listCV[0].ma_ctcb_kc? true : false    
                            }else{
                                flag = item.ma_ctcb_kc === this.state.checkedCV? true : false  
                            }
                                                                              
                            return (
                                <ListChuyenVienItem
                                    item={item}
                                    checkExistOne={(ma_ctcb_kc) => this.checkExistOne(ma_ctcb_kc)}
                                    checked={flag}
                                    fontSize={this.props.fontSize}
                                    styles={this.styles}
                                />
                            )
                        }}
                        removeClippedSubviews={true}
                    />
                </View>
            </Container >
        );
    }
}

function mapStateToProps(state) {
    return {
        fontSize: state.fontSize,
    }
}

export default connect(mapStateToProps)(VBDi_Details_LDChuyenKTTT)