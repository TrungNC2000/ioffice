import React, { Component } from 'react';
import { View, ScrollView, StyleSheet } from "react-native"
import { connect } from "react-redux"
import { Container, Button, Text, Card, CardItem, Toast } from "native-base"
import { AppConfig } from "../../AppConfig"
import { RemoveHTMLTag, ConvertDateTimeDetail, checkVersionWeb } from "../../untils/TextUntils"
import API from "../../networks"
import * as Models from "../../Models"
import ViewFileCA from "../../components/ViewFileCA"
import ViewFile from "../../components/ViewFile"
import UploadFileCMU from "../../components/UploadFileCMU"
import SignInfo from "../../components/SignInfo"

class Table_ChiTiet_VBDi extends Component {
    constructor(props) {
        super(props)
        this.state = {
            strVBLQ: "",
            strPhieuTrinhHGG: "",
        }
        this.fabActions_DuyetVBDi = [
            {
                text: "Duyệt",
                name: "btnDuyet",
                position: 1,
                textStyle: {
                    alignItems: 'center',
                    alignItems: 'center',
                    justifyContent: "center",
                    alignSelf: 'center',
                }
            },
            {
                text: "Duyệt nhanh",
                name: "btnHoanThanh",
                position: 2
            },
            {
                text: "Chuyển lãnh đạo khác",
                name: "btnLDChuyenLDK",
                position: 3
            },
            {
                text: "Chuyển chuyên viên",
                name: "btnLDChuyenCV",
                position: 4
            },
            ,
            {
                text: "Chuyển KTTT",
                name: "btnChuyenKTTT",
                position: 5
            }
        ];
        this.fabActions_XuLyVBDi = [
            {
                text: "Chuyển chuyên viên",
                name: "btnCVChuyenCV",
                position: 1
            },
            {
                text: "Gửi lãnh đạo",
                name: "btnCVGuiLD",
                position: 2
            },
            {
                text: "Hoàn thành",
                name: "btnHoanThanh",
                position: 3
            },
            {
                text: "Chuyển KTTT",
                name: "btnChuyenKTTT",
                position: 5
            }
        ];
        this.fabActions_DaXuLyVBDi = [
            {
                text: "Chuyển",
                name: "btnCVSendCVNew",
                position: 1
            }
        ];
    }

    componentWillReceiveProps(nextProps) {
        this.getVBLQ(nextProps)
        this.getPhieuTrinhHGG(nextProps)
    };

    getVBLQ = (nextProps) => {
        this.setState({ strVBLQ: "" })
        if (nextProps.dataDetails.length > 0) {
            let ma_van_ban = nextProps.dataDetails[0].ma_van_ban_kc
            API.VanBanDen.AU_VANBAN_DSVBLQDK(ma_van_ban).then((strVBLQ) => {
                if (global.vbdi_an_hien_vblq_ngoai_he_thong == 1 && nextProps.dataDetails[0].vblq_ngoai_he_thong) {
                    this.setState({ strVBLQ: strVBLQ + ':' + nextProps.dataDetails[0].vblq_ngoai_he_thong })
                } else {
                    this.setState({ strVBLQ })
                }
            })
        }
    }

    getPhieuTrinhHGG = (nextProps) => {
        this.setState({ strPhieuTrinhHGG: "" })
        if (checkVersionWeb(36) && nextProps.dataDetails.length > 0) {
            let ma_van_ban_di_kc = nextProps.dataDetails[0].ma_van_ban_di_kc
            let ma_don_vi_quan_tri = global.ma_don_vi_quan_tri
            API.VanBanDi.AU_VBDI_DSBPVBDICHGG(ma_van_ban_di_kc, ma_don_vi_quan_tri).then((strPhieuTrinhHGG) => {
                console.log(JSON.stringify(strPhieuTrinhHGG))
                this.setState({ strPhieuTrinhHGG })
            })
        }
    }

    fabActions = () => {
        let { activeTab, isDuyet } = this.props
        if (global.phap_che > 0) {
            this.fabActions_XuLyVBDi.push({
                text: "Chuyển thường trực ký",
                name: "btnCVchuyenThuongTruc",
                position: 4
            });
        }
        if (activeTab < 2 && isDuyet) {
            return this.fabActions_DuyetVBDi
        }
        if (activeTab === 0 && !isDuyet) {
            return this.fabActions_XuLyVBDi
        }
        if (activeTab === 3 && !isDuyet) {
            return this.fabActions_DaXuLyVBDi
        }
    }

    render() {
        this.styles = this.props.styles
        if (this.props.dataDetails.length > 0) {
            const { trich_yeu, so_ky_hieu, ngay_ban_hanh, nguoi_ky,
                ten_co_quan_ban_hanh, phong_ban_soan, so_ban_phat_hanh, so_di,
                ten_loai_van_ban, ngay_luu, ma_dinh_danh_vb, ten_so_vb_di,
                file_dinh_kem, file_van_ban_bs, ma_yeu_cau, vblq_ngoai_he_thong } = this.props.dataDetails[0]
            let activeTab = this.props.activeTab;
            let isShowFileCA = false
            if (global.cks_vnpt_mobile_pki === "1" || global.cks_hien_thi_nut_ky_so_hsm == "1" || global.cks_smartca_hien_thi_nut_ky == "1") {
                if (this.props.isDuyet && (this.props.activeTab === 0 || this.props.activeTab === 1)) {
                    isShowFileCA = true
                }
                if (!this.props.isDuyet && this.props.activeTab === 0) {
                    isShowFileCA = true
                }
            }
            let isShowFile = (file_van_ban_bs !== null) ? true : false
            let fabActionsTemp = this.fabActions()

            return (
                <Card noShadow style={this.styles.Card}>
                    {
                        (trich_yeu !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Trích yếu:</Text>
                                <Text style={[this.styles.textRight, { fontWeight: "500" }]}>{RemoveHTMLTag(trich_yeu)}</Text>
                            </CardItem>
                        )
                    }
                    {
                        (so_ky_hieu !== null && so_di !== null) && (
                            <CardItem style={[this.styles.CardItem, { flexWrap: "wrap", flexShrink: 1 }]}>
                                <Text style={this.styles.textLeft}>Số ký hiệu:</Text>
                                <Text style={[this.styles.textRight, { color: "red" }]}>
                                    {so_ky_hieu} &nbsp;&nbsp;
                                </Text>
                                <CardItem style={[this.styles.CardItem, { paddingLeft: -4, paddingBottom: -5 }]}>
                                    <Text style={this.styles.textLeft}>Số đi:</Text>
                                    <Text style={[this.styles.textRight]}>
                                        {so_di}
                                    </Text>
                                </CardItem>
                            </CardItem>
                        )
                    }
                    {
                        (so_ky_hieu === null && so_di !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Số đi:</Text>
                                <Text style={[this.styles.textRight]}>
                                    {so_di}
                                </Text>
                            </CardItem>
                        )
                    }
                    {
                        (nguoi_ky !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Người ký văn bản:</Text>
                                <Text style={[this.styles.textRight]}>
                                    {nguoi_ky}
                                </Text>
                            </CardItem>
                        )
                    }
                    {
                        (ten_loai_van_ban !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Loại văn bản:</Text>
                                <Text style={[this.styles.textRight]}>
                                    {ten_loai_van_ban}
                                </Text>
                            </CardItem>
                        )
                    }
                    {
                        (phong_ban_soan !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Phòng ban soạn:</Text>
                                <Text style={[this.styles.textRight, { color: "red" }]}>{phong_ban_soan}</Text>
                            </CardItem>
                        )
                    }
                    {
                        (so_ban_phat_hanh !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Số bản phát hành:</Text>
                                <Text style={[this.styles.textRight]}>
                                    {so_ban_phat_hanh}
                                </Text>
                            </CardItem>
                        )
                    }
                    {
                        (ten_co_quan_ban_hanh !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Cơ quan ban hành:</Text>
                                <Text style={[this.styles.textRight]}>{ten_co_quan_ban_hanh}</Text>
                            </CardItem>
                        )

                    }
                    {
                        (ngay_ban_hanh !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Ngày ban hành:</Text>
                                <Text style={[this.styles.textRight]}>
                                    {ngay_ban_hanh}
                                </Text>
                            </CardItem>
                        )
                    }
                    {
                        (ngay_luu !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Ngày lưu:</Text>
                                <Text style={[this.styles.textRight]}>
                                    {ConvertDateTimeDetail(ngay_luu)}
                                </Text>
                            </CardItem>
                        )
                    }
                    {
                        (ma_dinh_danh_vb !== null && ma_dinh_danh_vb !== "Không có thông tin") && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Mã định danh văn bản:</Text>
                                <Text style={[this.styles.textRight]}>{ma_dinh_danh_vb}</Text>
                            </CardItem>
                        )
                    }
                    {
                        (ma_yeu_cau !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Vai trò:</Text>
                                <Text style={[this.styles.textRight]}>
                                    {ma_yeu_cau === 2 ? "Xử lý chính" : ma_yeu_cau === 3 ? "Phối hợp" : "Xem để biết"}
                                </Text>
                            </CardItem>
                        )
                    }
                    {
                        (ten_so_vb_di !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Sổ văn bản đi:</Text>
                                <Text style={[this.styles.textRight]}>
                                    {ten_so_vb_di}
                                </Text>
                            </CardItem>
                        )
                    }
                    {
                        (this.state.strPhieuTrinhHGG !== "") && (
                            <View>
                                <CardItem style={this.styles.CardItem}>
                                    <Text style={this.styles.textLeft}>Phiếu trình:</Text>
                                </CardItem>
                                <CardItem style={this.styles.CardItem}>
                                    <ViewFile
                                        src_file={this.state.strPhieuTrinhHGG}
                                        navigation={this.props.navigation}
                                        fontSize={this.props.fontSize}
                                    />
                                </CardItem>
                            </View>
                        )
                    }
                    {
                        (isShowFileCA === true && isShowFile === true) && (
                            <View>
                                <CardItem style={this.styles.CardItem}>
                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                        <Text
                                            style={this.styles.textLeft}
                                            numberOfLines={1} ellipsizeMode="tail"
                                        >Tệp tin đính kèm: </Text>

                                    </View>
                                    <View style={{ flex: 1, alignItems: 'flex-start', paddingLeft: 0, paddingRight: 25 }}>
                                        {
                                            (activeTab == 0 && checkVersionWeb(15)) &&
                                            <UploadFileCMU
                                                ma_van_ban_di_kc={this.props.dataDetails[0].ma_van_ban_di_kc}
                                                parent={this.props.parent}
                                            />
                                        }
                                    </View>
                                </CardItem>
                                <CardItem style={this.styles.CardItem}>
                                    <ViewFileCA
                                        ref={ref => { this.ViewFile = ref }}
                                        parentView="VBDiDuyet"
                                        trich_yeu={trich_yeu}
                                        src_file={file_van_ban_bs}
                                        isPhieuTrinh={false}
                                        isUpdateViewCV={this.props.isUpdateViewCV}
                                        updateViewCV={this.props.updateViewCV}
                                        navigation={this.props.navigation}
                                        fontSize={this.props.fontSize}
                                        ma_van_ban_di={this.props.ma_van_ban_di}
                                        ma_xu_ly_di={this.props.ma_xu_ly_di}
                                        onSuccessMobilePKI={this.props.onSuccessMobilePKI}
                                        fabActions={fabActionsTemp}
                                        fabPressItem={(name) => {
                                            this.ViewFile.toggleModalWebView()
                                            switch (name) {
                                                case "btnDuyet":
                                                    setTimeout(() => {
                                                        this.props.onLDSendVT()
                                                    }, 200);
                                                    break;
                                                case "btnHoanThanh":
                                                    setTimeout(() => {
                                                        this.props.onSuccess()
                                                    }, 200);
                                                    break;
                                                case "btnLDChuyenLDK":
                                                    setTimeout(() => {
                                                        this.props.onLDSendLDK()
                                                    }, 200);
                                                    break;
                                                case "btnLDChuyenCV":
                                                    setTimeout(() => {
                                                        this.props.onLDSendCV()
                                                    }, 200);
                                                    break;
                                                case "btnCVChuyenCV":
                                                    setTimeout(() => {
                                                        this.props.onCVSendCV()
                                                    }, 200);
                                                    break;
                                                case "btnCVGuiLD":
                                                    setTimeout(() => {
                                                        this.props.onCVSendLD()
                                                    }, 200);
                                                    break;
                                                case "btnCVSendCVNew":
                                                    setTimeout(() => {
                                                        this.props.onCVSendCVNew()
                                                    }, 200);
                                                    break;
                                                case "btnChuyenKTTT":
                                                    setTimeout(() => {
                                                        this.props.onLDchuyenKTTT()
                                                    }, 200);
                                                    break;
                                                case "":
                                                    setTimeout(()=>{
                                                        this.props.on
                                                    })
                                                default:
                                                    break;
                                            }
                                        }}
                                    />
                                </CardItem>
                            </View>
                        )
                    }
                    {
                        (isShowFileCA === false && isShowFile === true) && (
                            <View>
                                <CardItem style={this.styles.CardItem}>
                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                        <Text style={this.styles.textLeft} numberOfLines={1} ellipsizeMode="tail">Tệp tin đính kèm:</Text>
                                    </View>
                                    <View style={{ flex: 1, alignItems: 'flex-start', paddingLeft: 0, paddingRight: 25 }}>
                                        {
                                            (activeTab == 0 && checkVersionWeb(15)) &&
                                            <UploadFileCMU
                                                ma_van_ban_di_kc={this.props.dataDetails[0].ma_van_ban_di_kc}
                                                parent={this.props.parent}
                                            />
                                        }
                                    </View>
                                </CardItem>
                                <CardItem style={this.styles.CardItem}>
                                    <ViewFile
                                        ref={ref1 => { this.ViewFile1 = ref1 }}
                                        src_file={file_van_ban_bs}
                                        isUpdateViewCV={this.props.isUpdateViewCV}
                                        updateViewCV={this.props.updateViewCV}
                                        navigation={this.props.navigation}
                                        fontSize={this.props.fontSize}
                                        ma_van_ban_di={this.props.ma_van_ban_di}
                                        fabActions={fabActionsTemp}
                                        fabPressItem={(name) => {
                                            this.ViewFile1.toggleModal()
                                            switch (name) {
                                                case "btnDuyet":
                                                    setTimeout(() => {
                                                        this.props.onLDSendVT()
                                                    }, 200);
                                                    break;
                                                case "btnHoanThanh":
                                                    setTimeout(() => {
                                                        this.props.onSuccess()
                                                    }, 200);
                                                    break;
                                                case "btnLDChuyenLDK":
                                                    setTimeout(() => {
                                                        this.props.onLDSendLDK()
                                                    }, 200);
                                                    break;
                                                case "btnLDChuyenCV":
                                                    setTimeout(() => {
                                                        this.props.onLDSendCV()
                                                    }, 200);
                                                    break;
                                                case "btnCVChuyenCV":
                                                    setTimeout(() => {
                                                        this.props.onCVSendCV()
                                                    }, 200);
                                                    break;
                                                case "btnCVGuiLD":
                                                    setTimeout(() => {
                                                        this.props.onCVSendLD()
                                                    }, 200);
                                                    break;
                                                case "btnCVSendCVNew":
                                                    setTimeout(() => {
                                                        this.props.onCVSendCVNew()
                                                    }, 200);
                                                    break;
                                                case "btnChuyenKTTT":
                                                    setTimeout(() => {
                                                        this.props.onLDchuyenKTTT()
                                                    }, 200);
                                                    break;
                                                default:
                                                    break;
                                            }
                                        }}
                                    />
                                </CardItem>
                            </View>
                        )
                    }
                    {
                        (file_dinh_kem != null) && (
                            <View>
                                <CardItem style={this.styles.CardItem}>
                                    <Text style={this.styles.textLeft}>Văn bản phiếu trình:</Text>
                                </CardItem>
                                <CardItem style={this.styles.CardItem}>
                                    {
                                        (isShowFileCA) ? (
                                            <ViewFileCA
                                                ref={ref => { this.ViewFile = ref }}
                                                src_file={file_dinh_kem}
                                                isPhieuTrinh={true}
                                                navigation={this.props.navigation}
                                                fontSize={this.props.fontSize}
                                                ma_van_ban_di={this.props.ma_van_ban_di}
                                                ma_xu_ly_di={this.props.ma_xu_ly_di}
                                                onSuccessMobilePKI={this.props.onSuccessMobilePKI}
                                            />
                                        ) : (
                                                <ViewFile
                                                    src_file={file_dinh_kem}
                                                    navigation={this.props.navigation}
                                                    fontSize={this.props.fontSize}
                                                />
                                            )
                                    }
                                </CardItem>
                            </View>
                        )
                    }
                    {
                        (this.state.strVBLQ !== "") && (
                            <View>
                                <CardItem style={this.styles.CardItem}>
                                    <Text style={this.styles.textLeft}>Văn bản liên quan:</Text>
                                </CardItem>
                                <CardItem style={this.styles.CardItem}>
                                    <ViewFile
                                        src_file={this.state.strVBLQ}
                                        navigation={this.props.navigation}
                                        fontSize={this.props.fontSize}
                                    />
                                </CardItem>
                            </View>
                        )
                    }
                    {this.props.renderButton()}
                </Card>
            )
        } else {
            return <View></View>
        }
    }
}

class Table_ButPhe_VBDi extends Component {
    renderDataNotes = (dataNotes) => {
        let result = []
        for (let item of dataNotes) {
            result.push(
                <View key={item.ma_xu_ly_di_kc} style={{ paddingTop: 5, paddingBottom: 10, borderColor: AppConfig.defaultLineColor, borderTopWidth: AppConfig.defaultLineWidth }}>
                    <CardItem style={this.styles.CardItem}>
                        <Text style={[this.styles.textRight, { color: AppConfig.blueBackground }]}>
                            Người gửi: {item.can_bo_gui}
                        </Text>
                    </CardItem>
                    <CardItem style={this.styles.CardItem}>
                        <Text style={[this.styles.textRight]}>
                            Người nhận: {item.can_bo_nhan}
                        </Text>
                    </CardItem>
                    {
                        (item.ngay_nhan !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={[this.styles.textRight]}>
                                    Ngày nhận: {ConvertDateTimeDetail(item.ngay_nhan)}
                                </Text>
                            </CardItem>
                        )
                    }
                    {
                        (item.ngay_xu_ly !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={[this.styles.textRight]}>
                                    Ngày xử lý: {ConvertDateTimeDetail(item.ngay_xu_ly)}
                                </Text>
                            </CardItem>
                        )
                    }
                    {
                        (item.noi_dung_chuyen_ctcb_khac !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={[this.styles.textRight]}>
                                    Nội dung yêu cầu: {RemoveHTMLTag(item.noi_dung_chuyen_ctcb_khac)}
                                </Text>
                            </CardItem>
                        )
                    }
                </View>
            )
        }
        return result
    }

    render() {
        this.styles = this.props.styles
        if (this.props.dataNotes.length > 0) {
            return (
                <Card noShadow style={this.styles.Card}>
                    <CardItem style={this.styles.CardItem}>
                        <Text style={[this.styles.textRight, { fontSize: this.props.fontSize.FontSizeLarge, fontWeight: "bold", }]}>
                            Tổng hợp ý kiến xử lý
                        </Text>
                    </CardItem>
                    {this.renderDataNotes(this.props.dataNotes)}
                </Card>
            )
        } else {
            return (
                <View></View>
            )
        }
    }
}

class VBDi_Details extends Component {
    constructor(props) {
        super(props)
        this.state = {
            dataDetails: [],
            dataNotes: []
        }

        this.isDuyet = false
        this.ma_xu_ly_di = -1
        this.ma_van_ban_di = -1
        this.activeTab = 2
    }

    componentDidMount = () => {
        this.isDuyet = this.props.isDuyet
        this.ma_xu_ly_di = this.props.ma_xu_ly_di
        this.ma_van_ban_di = this.props.ma_van_ban_di_kc
        this.activeTab = this.props.activeTab
        this.getDetails()
        this.getNotes()
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.ma_xu_ly_di !== this.props.ma_xu_ly_di) {
            this.isDuyet = nextProps.isDuyet
            this.ma_xu_ly_di = nextProps.ma_xu_ly_di
            this.ma_van_ban_di = nextProps.ma_van_ban_di_kc
            this.activeTab = nextProps.activeTab
            this.setState({ dataDetails: [], dataNotes: [] })
            this.getDetails()
            this.getNotes()
        }
    }

    getDetails = async () => {
        let ma_van_ban_di = this.ma_van_ban_di
        let ma_ctcb = global.ma_ctcb_kc
        await API.VanBanDi.AU_VBDI_CTVBD(ma_van_ban_di, ma_ctcb).then((dataDetails) => {
            this.setState({ dataDetails })
        })
    }

    getNotes = async () => {
        let ma_van_ban_di = this.ma_van_ban_di
        let ma_don_vi_quan_tri = global.ma_don_vi_quan_tri
        await API.VanBanDi.AU_VBDI_QTXLVBD(ma_van_ban_di, ma_don_vi_quan_tri).then((dataNotes) => {
            this.setState({ dataNotes })
        })
    }

    onSuccess = () => {
        if (this.isDuyet) {
            this.props.onSuccess(this.ma_van_ban_di, this.ma_xu_ly_di, 7, this.state.dataDetails[0].file_van_ban, true)
        } else {
            this.props.onSuccess(this.ma_van_ban_di, this.ma_xu_ly_di, true)
        }
    }

    onLDSendVT = () => {
        if (this.state.dataDetails.length === 0) {
            return null
        }
        let npVBDi_Details_LDChuyenVT = new Models.npVBDi_Details_LDChuyenVT
        npVBDi_Details_LDChuyenVT.isDuyet = false
        npVBDi_Details_LDChuyenVT.ma_van_ban_di = this.state.dataDetails[0].ma_van_ban_di_kc
        npVBDi_Details_LDChuyenVT.ma_xu_ly_di = this.ma_xu_ly_di
        npVBDi_Details_LDChuyenVT.ma_ctcb_gui = global.ma_ctcb_kc
        npVBDi_Details_LDChuyenVT.file_van_ban = this.state.dataDetails[0].file_van_ban
        npVBDi_Details_LDChuyenVT.trich_yeu = this.state.dataDetails[0].trich_yeu
        this.props.navigation.navigate("VBDi_Details_LDChuyenVT", {
            npVBDi_Details_LDChuyenVT: npVBDi_Details_LDChuyenVT
        })
    }

    onLDSendCV = () => {
        if (this.state.dataDetails.length === 0) {
            return null
        }
        let npVBDi_Details_LDChuyenCV = new Models.npVBDi_Details_LDChuyenCV
        npVBDi_Details_LDChuyenCV.ma_van_ban_di = this.state.dataDetails[0].ma_van_ban_di_kc
        npVBDi_Details_LDChuyenCV.ma_xu_ly_di = this.ma_xu_ly_di
        npVBDi_Details_LDChuyenCV.ma_ctcb_gui = global.ma_ctcb_kc
        npVBDi_Details_LDChuyenCV.file_van_ban = this.state.dataDetails[0].file_van_ban
        npVBDi_Details_LDChuyenCV.trich_yeu = this.state.dataDetails[0].trich_yeu
        this.props.navigation.navigate("VBDi_Details_LDChuyenCV", {
            npVBDi_Details_LDChuyenCV: npVBDi_Details_LDChuyenCV
        })
    }

    onLDSendLDK = () => {
        if (this.state.dataDetails.length === 0) {
            return null
        }
        let npVBDi_Details_LDChuyenLDK = new Models.npVBDi_Details_LDChuyenLDK
        npVBDi_Details_LDChuyenLDK.ma_van_ban_di = this.state.dataDetails[0].ma_van_ban_di_kc
        npVBDi_Details_LDChuyenLDK.ma_xu_ly_di = this.ma_xu_ly_di
        npVBDi_Details_LDChuyenLDK.ma_ctcb_gui = global.ma_ctcb_kc
        npVBDi_Details_LDChuyenLDK.file_van_ban = this.state.dataDetails[0].file_van_ban
        npVBDi_Details_LDChuyenLDK.trich_yeu = this.state.dataDetails[0].trich_yeu
        this.props.navigation.navigate("VBDi_Details_LDChuyenLDK", {
            npVBDi_Details_LDChuyenLDK: npVBDi_Details_LDChuyenLDK
        })
    }

    onCVSendLD = () => {
        if (this.state.dataDetails.length === 0) {
            return null
        }
        let npVBDi_Details_CVChuyenLD = new Models.npVBDi_Details_CVChuyenLD
        npVBDi_Details_CVChuyenLD.ma_van_ban_di = this.state.dataDetails[0].ma_van_ban_di_kc
        npVBDi_Details_CVChuyenLD.ma_xu_ly_di = this.ma_xu_ly_di
        npVBDi_Details_CVChuyenLD.ma_ctcb_gui = global.ma_ctcb_kc
        npVBDi_Details_CVChuyenLD.file_van_ban = this.state.dataDetails[0].file_van_ban
        npVBDi_Details_CVChuyenLD.trich_yeu = this.state.dataDetails[0].trich_yeu
        this.props.navigation.navigate("VBDi_Details_CVChuyenLD", {
            npVBDi_Details_CVChuyenLD: npVBDi_Details_CVChuyenLD
        })
    }

    onCVSendCV = () => {
        if (this.state.dataDetails.length === 0) {
            return null
        }
        let npVBDi_Details_CVChuyenCV = new Models.npVBDi_Details_CVChuyenCV
        npVBDi_Details_CVChuyenCV.ma_van_ban_di = this.state.dataDetails[0].ma_van_ban_di_kc
        npVBDi_Details_CVChuyenCV.ma_xu_ly_di = this.ma_xu_ly_di
        npVBDi_Details_CVChuyenCV.ma_ctcb_gui = global.ma_ctcb_kc
        npVBDi_Details_CVChuyenCV.file_van_ban = this.state.dataDetails[0].file_van_ban
        npVBDi_Details_CVChuyenCV.trich_yeu = this.state.dataDetails[0].trich_yeu
        this.props.navigation.navigate("VBDi_Details_CVChuyenCV", {
            npVBDi_Details_CVChuyenCV: npVBDi_Details_CVChuyenCV
        })
    }

    onCVSendVT = () => {
        if (this.state.dataDetails.length === 0) {
            return null
        }
        let npVBDi_Details_CVChuyenVT = new Models.npVBDi_Details_CVChuyenVT
        npVBDi_Details_CVChuyenVT.ma_van_ban_di = this.state.dataDetails[0].ma_van_ban_di_kc
        npVBDi_Details_CVChuyenVT.ma_xu_ly_di = this.ma_xu_ly_di
        npVBDi_Details_CVChuyenVT.ma_ctcb_gui = global.ma_ctcb_kc
        npVBDi_Details_CVChuyenVT.ma_ctcb_duyet = this.state.dataDetails[0].ma_ctcb_duyet
        npVBDi_Details_CVChuyenVT.file_van_ban = this.state.dataDetails[0].file_van_ban
        npVBDi_Details_CVChuyenVT.trich_yeu = this.state.dataDetails[0].trich_yeu
        this.props.navigation.navigate("VBDi_Details_CVChuyenVT", {
            npVBDi_Details_CVChuyenVT: npVBDi_Details_CVChuyenVT
        })
    }

    onCVSendCVNew = () => {
        if (this.state.dataDetails.length === 0) {
            return null
        }
        let npVBDi_Details_CVChuyenCV = new Models.npVBDi_Details_CVChuyenCV
        npVBDi_Details_CVChuyenCV.ma_van_ban_di = this.state.dataDetails[0].ma_van_ban_di_kc
        npVBDi_Details_CVChuyenCV.ma_xu_ly_di = this.ma_xu_ly_di
        npVBDi_Details_CVChuyenCV.ma_ctcb_gui = global.ma_ctcb_kc
        npVBDi_Details_CVChuyenCV.file_van_ban = this.state.dataDetails[0].file_van_ban
        npVBDi_Details_CVChuyenCV.trich_yeu = this.state.dataDetails[0].trich_yeu
        this.props.navigation.navigate("VBDi_CVChuyenCV", {
            npVBDi_Details_CVChuyenCV: npVBDi_Details_CVChuyenCV
        })
    }

    onCVGopY = () => {
        if (this.state.dataDetails.length === 0) {
            return null
        }
        this.props.navigation.navigate("VBDi_CVGopY", {
            ma_xu_ly_di: this.state.dataDetails[0].ma_xu_ly_di,
            ma_van_ban_di: this.state.dataDetails[0].ma_van_ban_di_kc,
            file_dinh_kem: this.state.dataDetails[0].file_van_ban
        })
    }

    onCVchuyenThuongTruc = () => {
        if (this.state.dataDetails.length === 0) {
            return null
        }
        let npVBDi_Details_LDChuyenVT = new Models.npVBDi_Details_LDChuyenVT
        npVBDi_Details_LDChuyenVT.isDuyet = this.isDuyet;
        npVBDi_Details_LDChuyenVT.ma_van_ban_di = this.state.dataDetails[0].ma_van_ban_di_kc
        npVBDi_Details_LDChuyenVT.ma_xu_ly_di = this.ma_xu_ly_di
        npVBDi_Details_LDChuyenVT.ma_ctcb_gui = global.ma_ctcb_kc
        npVBDi_Details_LDChuyenVT.file_van_ban = this.state.dataDetails[0].file_van_ban
        npVBDi_Details_LDChuyenVT.trich_yeu = this.state.dataDetails[0].trich_yeu
        npVBDi_Details_LDChuyenVT.loai_xu_ly = 12
        this.props.navigation.navigate("VBDi_Details_LDChuyenVT", {
            npVBDi_Details_LDChuyenVT: npVBDi_Details_LDChuyenVT
        })
    }

    onLDchuyenKTTT = () => {
        if (this.state.dataDetails.length === 0) {
            return null;
        }
        let npVBDI_Details_LDChuyenKTT = new Models.npVBDi_Details_LDChuyenKTTT
        npVBDI_Details_LDChuyenKTT.isDuyet = this.isDuyet;
        npVBDI_Details_LDChuyenKTT.ma_van_ban_di = this.state.dataDetails[0].ma_van_ban_di_kc
        npVBDI_Details_LDChuyenKTT.ma_xu_ly_di = this.ma_xu_ly_di
        npVBDI_Details_LDChuyenKTT.ma_ctcb_gui = global.ma_ctcb_kc
        npVBDI_Details_LDChuyenKTT.file_van_ban = this.state.dataDetails[0].file_van_ban
        npVBDI_Details_LDChuyenKTT.trich_yeu = this.state.dataDetails[0].trich_yeu
        npVBDI_Details_LDChuyenKTT.ma_don_vi_tao = this.state.dataDetails[0].ma_don_vi_tao
        this.props.navigation.navigate("VBDi_Details_LDChuyenKTTT", {
            npVBDi_Details_LDChuyenKTTT: npVBDI_Details_LDChuyenKTT
        })
    }

    onSuccessMobilePKI = () => {
        this.componentDidMount()
    }

    updateViewCV = async () => {
        let ma_van_ban_di_kc = this.state.dataDetails[0].ma_van_ban_di_kc
        let ma_ctcb = global.ma_ctcb_kc
        await API.VanBanDi.AU_VBDI_CNDX(ma_ctcb, ma_van_ban_di_kc)
    }

    renderButton = () => {
        if (this.isDuyet) {
            if (this.activeTab === 0 || this.activeTab === 1) {
                var ArrayButtonDuyet = [
                    <Button success style={this.styles.Button} onPress={this.onSuccess}><Text style={this.styles.ButtonText}>Duyệt nhanh</Text></Button>,
                    <Button danger style={this.styles.Button} onPress={this.onLDSendVT}><Text style={this.styles.ButtonText}>Duyệt</Text></Button>,
                    <Button primary style={this.styles.Button} onPress={this.onLDSendLDK}><Text style={this.styles.ButtonText}>Chuyển LDK</Text></Button>,
                    <Button primary style={this.styles.Button} onPress={this.onLDSendCV}><Text style={this.styles.ButtonText}>Chuyển CV</Text></Button>
                ];
                if (global.phap_che > 0) {
                    ArrayButtonDuyet.push(
                        <Button primary style={this.styles.Button} onPress={this.onCVchuyenThuongTruc}><Text style={this.styles.ButtonText}>Chuyển Thường trực ký</Text></Button>
                    )
                }
                if (global.vbdi_duyet_vb_chuyen_phap_che > 0) {
                    ArrayButtonDuyet.push(
                        <Button primary style={this.styles.Button} onPress={this.onLDchuyenKTTT}><Text style={this.styles.ButtonText}>Chuyển KTTT</Text></Button>
                    )
                }
                return (
                    <CardItem style={[this.styles.CardItem, this.styles.CardItemButton]}>
                        {ArrayButtonDuyet}
                    </CardItem>
                )
            } else {
                return null
            }
        } else {
            if (this.activeTab === 0) {
                if (this.state.dataDetails[0].ma_yeu_cau === 2) {
                    var ArrayButtonCVXuLy = [
                        <Button success style={this.styles.Button} onPress={this.onSuccess}><Text style={this.styles.ButtonText}>Hoàn tất</Text></Button>,
                        <Button primary style={this.styles.Button} onPress={this.onCVSendLD}><Text style={this.styles.ButtonText}>Gửi LĐ</Text></Button>,
                        <Button primary style={this.styles.Button} onPress={this.onCVSendCV}><Text style={this.styles.ButtonText}>Gửi CV</Text></Button>,
                        <Button primary style={this.styles.Button} onPress={this.onCVSendVT}><Text style={this.styles.ButtonText}>Gửi VT</Text></Button>
                    ]
                    if (global.phap_che > 0) {
                        ArrayButtonCVXuLy.push(
                            <Button primary style={this.styles.Button} onPress={this.onCVchuyenThuongTruc}><Text style={this.styles.ButtonText}>Chuyển Thường trực ký</Text></Button>
                        )
                    }
                    if (global.vbdi_cho_xl_chuyen_phap_che > 0) {
                        ArrayButtonCVXuLy.push(
                            <Button primary style={this.styles.Button} onPress={this.onLDchuyenKTTT}><Text style={this.styles.ButtonText}>Chuyển KTTT</Text></Button>
                        )
                    }
                    return (
                        <CardItem style={[this.styles.CardItem, this.styles.CardItemButton]}>
                            {ArrayButtonCVXuLy}
                        </CardItem>
                    )
                } else {
                    return (
                        <CardItem style={[this.styles.CardItem, this.styles.CardItemButton]}>
                            <Button primary style={this.styles.Button} onPress={this.onCVGopY}><Text style={this.styles.ButtonText}>Góp ý</Text></Button>
                        </CardItem>
                    )
                }
            }
            if (global.vbdi_chuyen_thuong_truc_ky == 1) {
                if (this.activeTab === 4) {
                    return (
                        <CardItem style={[this.styles.CardItem, this.styles.CardItemButton]}>
                            <Button primary style={this.styles.Button} onPress={this.onCVSendCVNew}><Text style={this.styles.ButtonText}>Chuyển tiếp</Text></Button>
                        </CardItem>
                    )
                } else {
                    return null
                }
            } else {
                if (this.activeTab === 3) {
                    return (
                        <CardItem style={[this.styles.CardItem, this.styles.CardItemButton]}>
                            <Button primary style={this.styles.Button} onPress={this.onCVSendCVNew}><Text style={this.styles.ButtonText}>Chuyển tiếp</Text></Button>
                        </CardItem>
                    )
                } else {
                    return null
                }
            }


        }
    }

    render() {

        this.styles = StyleSheet.create({
            scrollContainer: {
                flex: 1,
                bottom: 0,
                backgroundColor: AppConfig.grayBackground
            },
            viewTieuDe: {
                backgroundColor: AppConfig.grayBackground,
                justifyContent: "center", alignContent: "center",
                flexWrap: "wrap",
                padding: 4,
            },
            textTieuDe: {
                fontSize: this.props.fontSize.FontSizeXLarge,
                fontWeight: "400",
                textAlign: "center"
            },

            //Table_ChiTiet_VBDEN
            Card: {
                marginLeft: 6,
                marginRight: 6,
                marginTop: 0,
                padding: 10,
                borderRadius: 6,
            },
            CardItem: {
                marginTop: AppConfig.defaultPadding,
                marginBottom: AppConfig.defaultPadding,
            },
            CardItemButton: {
                marginTop: 10,
                paddingTop: 10,
                borderTopColor: AppConfig.defaultLineColor,
                borderTopWidth: AppConfig.defaultLineWidth,
                flexDirection: 'row',
                flexWrap: "wrap",
                justifyContent: "center",
                alignItems: "center"
            },
            Button: {
                margin: 3,
                height: 40,
                justifyContent: "center",
                alignItems: "center"
            },
            ButtonText: {
                fontSize: this.props.fontSize.FontSizeNorman,
                textAlign: "center"
            },
            textLeft: {
                fontSize: this.props.fontSize.FontSizeNorman,
                color: AppConfig.grayText,
                alignSelf: "flex-start"
            },
            textRight: {
                paddingLeft: 5,
                fontSize: this.props.fontSize.FontSizeNorman,
                flexWrap: "wrap",
                flexShrink: 1,
            }
        })

        return (
            <Container style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
                <ScrollView
                    bounces={false}
                    removeClippedSubviews={true}
                    scrollEnabled={true}
                    style={this.styles.scrollContainer}
                >
                    <Table_ChiTiet_VBDi
                        dataDetails={this.state.dataDetails}
                        renderButton={() => this.renderButton()}
                        navigation={this.props.navigation}
                        ma_van_ban_di={this.ma_van_ban_di}
                        ma_xu_ly_di={this.ma_xu_ly_di}
                        activeTab={this.activeTab}
                        isDuyet={this.isDuyet}
                        parent={this}
                        isUpdateViewCV={true}
                        updateViewCV={this.updateViewCV}
                        onSuccessMobilePKI={this.onSuccessMobilePKI}
                        onSuccess={this.onSuccess}
                        onLDSendVT={this.onLDSendVT}
                        onLDSendLDK={this.onLDSendLDK}
                        onLDSendCV={this.onLDSendCV}
                        onCVSendLD={this.onCVSendLD}
                        onCVSendCV={this.onCVSendCV}
                        onCVSendCVNew={this.onCVSendCVNew}
                        onLDchuyenKTTT={this.onLDchuyenKTTT}
                        fontSize={this.props.fontSize}
                        styles={this.styles}
                    />
                    {(global.cks_hien_thi_thong_tin_app_mobile == 1) && (
                        <SignInfo
                            ma_van_ban={this.ma_van_ban_di}
                            type="di"
                            fontSize={this.props.fontSize}
                        />)
                    }
                    <Table_ButPhe_VBDi
                        dataNotes={this.state.dataNotes}
                        fontSize={this.props.fontSize}
                        styles={this.styles}
                    />
                </ScrollView>
            </Container>
        );
    }
}

function mapStateToProps(state) {
    return {
        fontSize: state.fontSize,
    }
}

export default connect(mapStateToProps)(VBDi_Details)
