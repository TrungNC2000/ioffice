import React, { Component } from 'react';
import { ScrollView, View, StyleSheet, FlatList, Alert, PixelRatio, Dimensions } from "react-native"
import { Container, Text, Card, CardItem, Toast, Textarea, CheckBox, ActionSheet } from "native-base"
import { connect } from "react-redux"
import { StackActions } from "react-navigation"
import moment from 'moment';
import { AppConfig } from "../../AppConfig"
import HeaderWithLeftRight from "../../components/Header/HeaderWithLeftRight"
import SearchList from "../../components/SearchList"
import API from "../../networks"
import * as Models from "../../Models"
import { resetStack, ToastSuccess } from "../../untils/TextUntils"

class ListLanhDaoItem extends Component {
    shouldComponentUpdate(nextProps, nextState) {
        if (this.props.checked !== nextProps.checked) {
            return true
        }
        return false
    }

    render() {
        this.styles = this.props.styles
        let { ma, ten, ten_chuc_vu } = this.props.item
        return (
            <CardItem style={{ alignItems: 'center', paddingTop: 4, paddingBottom: 4, borderTopWidth: AppConfig.defaultLineWidth, borderTopColor: AppConfig.defaultLineColor, }} key={ma}>
                <CardItem style={{ width: 90, justifyContent: "center", alignItems: "center" }}>
                    <CheckBox onPress={() => this.props.checkExistOne(ma)} checked={this.props.checked} color={AppConfig.blueBackground} style={{ marginRight: 20 }}></CheckBox>
                </CardItem>
                <Card noShadow style={{ flex: 1, borderTopWidth: 0, borderBottomWidth: 0, borderLeftWidth: 0, borderRightWidth: 0, }}>
                    <CardItem>
                        <Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>{ten}</Text>
                    </CardItem>
                    <CardItem>
                        <Text style={[this.styles.TextTitle, { color: "#808080" }]}>Chức vụ: {ten_chuc_vu}</Text>
                    </CardItem>
                </Card>
            </CardItem>
        )
    }
}

class ListVanThuItem extends Component {
    shouldComponentUpdate(nextProps, nextState) {
        if (this.props.checked !== nextProps.checked) {
            return true
        }
        return false
    }

    render() {
        this.styles = this.props.styles
        let { ma_ctcb_kc, ho_va_ten_can_bo, ten_chuc_vu, ten_don_vi } = this.props.item
        return (
            <CardItem style={{ alignItems: 'center', paddingTop: 4, paddingBottom: 4, borderTopWidth: AppConfig.defaultLineWidth, borderTopColor: AppConfig.defaultLineColor, }} key={ma_ctcb_kc}>
                <CardItem style={{ width: 90, justifyContent: "center", alignItems: "center" }}>
                    <CheckBox onPress={() => this.props.checkExistOne(ma_ctcb_kc)} checked={this.props.checked} color={AppConfig.blueBackground} style={{ marginRight: 20 }}></CheckBox>
                </CardItem>
                <Card noShadow style={{ flex: 1, borderTopWidth: 0, borderBottomWidth: 0, borderLeftWidth: 0, borderRightWidth: 0, }}>
                    <CardItem>
                        <Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>{ho_va_ten_can_bo}</Text>
                    </CardItem>
                    <CardItem>
                        <Text style={[this.styles.TextTitle, { color: "#808080" }]}>Chức vụ: {ten_chuc_vu}</Text>
                    </CardItem>
                    {/* <CardItem>
                        <Text style={[this.styles.TextTitle, { color: "#808080" }]}>Đơn vị: {ten_don_vi}</Text>
                    </CardItem> */}
                </Card>
            </CardItem>
        )
    }
}

class VBDi_Details_CVChuyenVT extends Component {
    constructor(props) {
        super(props)
        this.state = {
            listLD: [],
            listCheckLD: [],
            listVT: [],
            listCheckVT: [],
            yKienXuLy: "",
            checkSms: false,
            checkEmail: false
        }
        this.npVBDi_Details_CVChuyenVT = new Models.npVBDi_Details_CVChuyenVT()
        this.listSearchDefaultLD = []
        this.listSearchDefault = []
        this.isSending = false
    }

    componentDidMount = () => {
        this.npVBDi_Details_CVChuyenVT = this.props.navigation.getParam("npVBDi_Details_CVChuyenVT")
        this.getListLanhDao()
        this.getListVanThu()
    }

    getListLanhDao = () => {
        API.VanBanDi.AU_VBDI_DSLDCDK(global.ma_don_vi).then((listLD) => {
            this.listSearchDefaultLD = listLD
            this.setState({ listLD }, () => {
                if (listLD.length > 0) {
                    let item = listLD.filter(el => el.ma === this.npVBDi_Details_CVChuyenVT.ma_ctcb_duyet)
                    if (item.length > 0) {
                        setTimeout(() => {
                            this.checkExistOneLD(item[0].ma)
                        }, 200);
                    }
                }
            })
        })
    }

    getListVanThu = () => {
        API.VanBanDi.AU_VBDI_DSVTTDVQT(global.ma_don_vi_quan_tri).then((listVT) => {
            this.listSearchDefault = listVT
            this.setState({ listVT }, () => {
                if (listVT.length > 0) {
                    setTimeout(() => {
                        this.checkExistOne(listVT[0].ma_ctcb_kc)
                    }, 200);
                }
            })
        })
    }

    onCheckSms = () => {
        this.setState({ checkSms: !this.state.checkSms })
    }

    onPressSend = () => {
        let { yKienXuLy, checkSms, listCheckLD, listCheckVT } = this.state
        let listVT = this.listSearchDefault

        if (listCheckLD.length === 0) {
            Toast.show({ text: "Vui lòng chọn một cán bộ duyệt!", type: "warning", buttonText: "Đóng", position: "top", duration: 2000 })
            return null
        }

        if (listCheckVT.length === 0) {
            Toast.show({ text: "Vui lòng chọn ít nhất một văn thư!", type: "warning", buttonText: "Đóng", position: "top", duration: 2000 })
            return null
        }

        if (this.isSending) {
            Toast.show({ text: "Vui lòng chờ trong giây lát!", type: "warning", buttonText: "Đóng", position: "top", duration: 2000 })
            return null
        }

        let ma_van_ban_di_kc = this.npVBDi_Details_CVChuyenVT.ma_van_ban_di
        let ma_ctcb_gui = global.ma_ctcb_kc
        let chuoi_ma_ctcb_nhan = listCheckVT.join(";")
        let noi_dung_chuyen = yKienXuLy
        let file_dinh_kem = this.npVBDi_Details_CVChuyenVT.file_van_ban
        let ma_xu_ly_di_cha = this.npVBDi_Details_CVChuyenVT.ma_xu_ly_di
        let sms = checkSms ? 1 : 0
        let ma_ctcb_duyet = listCheckVT[0]

        Alert.alert(
            "Xác nhận",
            "Bạn muốn chuyển văn thư?",
            [
                { text: "Không", onPress: () => console.log("Logout"), style: "cancel" },
                {
                    text: "Có", onPress: () => {
                        this.isSending = true
                        API.KYSO.AU_KYSO_GFDK(ma_van_ban_di_kc, global.ma_ctcb_kc).then((file_moi) => {
                            API.VanBanDi.AU_VBDI_CVCVT(ma_van_ban_di_kc, ma_ctcb_gui, chuoi_ma_ctcb_nhan, noi_dung_chuyen,
                                file_moi, file_dinh_kem, ma_xu_ly_di_cha, sms, ma_ctcb_duyet).then((response) => {
                                    if (response) {
                                        API.Login.AU_DSNVCB(global.ma_ctcb_kc, this.props.dispatch)
                                        if (sms === 1) {
                                            let arrSDT = []
                                            listCheckVT = listCheckVT.split(";")
                                            for (let item of listCheckVT) {
                                                let sdt = listVT.filter(el => el.ma_ctcb_kc == item)[0].di_dong_can_bo
                                                arrSDT.push(sdt)
                                            }

                                            if (arrSDT.length > 0) {
                                                let noi_dung = this.npVBDi_Details_CVChuyenVT.trich_yeu
                                                let chuoi_di_dong_nhan = arrSDT.join(",")
                                                API.NhanTinSMS.AU_SMS_GTN(noi_dung, chuoi_di_dong_nhan)
                                            }
                                        }
                                        this.isSending = false
                                        ToastSuccess("Chuyển văn thư thành công", () => { this.props.navigation.dispatch(resetStack("VBDiXuLy")) })
                                    } else {
                                        this.isSending = false
                                        Toast.show({ text: "Lỗi. Vui lòng thử lại sau!", type: "danger", buttonText: "Đóng", position: "top", duration: 2000 })
                                    }
                                })
                        })
                    }
                }
            ],
            { cancelable: false }
        )
    }

    getCheckedLD = () => {
        return this.state.listCheckLD
    }

    getChecked = () => {
        return this.state.listCheckVT
    }

    checkExistOneLD = (ma) => {
        let { listCheckLD } = this.state
        if (!listCheckLD.includes(ma)) {
            listCheckLD = []
            listCheckLD.push(ma)
        } else {
            listCheckLD = listCheckLD.filter(el => el !== ma)
        }
        this.setState({ listCheckLD })
    }

    checkExistOne = (ma_ctcb_kc) => {
        let { listCheckVT } = this.state
        if (!listCheckVT.includes(ma_ctcb_kc)) {
            listCheckVT.push(ma_ctcb_kc)
        } else {
            listCheckVT = listCheckVT.filter(el => el !== ma_ctcb_kc)
        }
        this.setState({ listCheckVT })
    }

    onSearchListLD = () => {
        if (this.listSearchDefaultLD.length > 0) {
            let keyWord = this.searchListLD.getKeyWord()
            let arrResult = this.listSearchDefaultLD.filter(el => el.ten.toLowerCase().indexOf(keyWord.toLowerCase()) !== -1)
            this.setState({ listLD: arrResult }, () => this.searchListLD.setKeyWord(keyWord))
        }
    }

    onSearchList = () => {
        if (this.listSearchDefault.length > 0) {
            let keyWord = this.searchList.getKeyWord()
            let arrResult = this.listSearchDefault.filter(el => el.ho_va_ten_can_bo.toLowerCase().indexOf(keyWord.toLowerCase()) !== -1)
            this.setState({ listVT: arrResult }, () => this.searchList.setKeyWord(keyWord))
        }
    }

    render() {

        this.styles = StyleSheet.create({
            scrollContainer: {
                flex: 1,
                marginLeft: 10,
                marginRight: 10
            },
            CardItem: {
                paddingTop: 10,
                paddingBottom: 10,
                alignItems: 'center'
            },
            CardItemLeft: {
                width: 90
            },
            CardItemRight: {
                flex: 1
            },
            TextTitle: {
                fontSize: this.props.fontSize.FontSizeSmall,
            },
            TextInput: {
                flex: 1,
                fontFamily: AppConfig.fontFamily,
                fontSize: this.props.fontSize.FontSizeNorman / PixelRatio.getFontScale(),
                color: "#000000",
                borderColor: AppConfig.defaultLineColor,
                borderWidth: AppConfig.defaultLineWidth,
                borderRadius: 5,
                marginRight: 2,
                paddingLeft: 5,
                paddingTop: 7,
                minHeight: 40,
            },
        })

        const popAction = StackActions.pop({
            n: 1
        })
        return (
            <Container style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
                <HeaderWithLeftRight
                    title="Chuyển văn thư"
                    buttonLeftName="arrow-back"
                    buttonRightName="done"
                    onPressLeftButton={() => { this.props.navigation.dispatch(popAction) }}
                    onPressRightButton={this.onPressSend}
                />
                {/* <ScrollView style={this.styles.scrollContainer}>
                    <CardItem style={this.styles.CardItem}>
                        <CardItem style={[this.styles.CardItemLeft]}>
                            <Text style={[this.styles.TextTitle]}>Ý kiến xử lý</Text>
                        </CardItem>
                        <CardItem style={[this.styles.CardItemRight]}>
                            <Textarea
                                autoCorrect={false}
                                style={this.styles.TextInput}
                                rowSpan={3}
                                value={this.state.yKienXuLy}
                                onChangeText={(text) => { this.setState({ yKienXuLy: text }) }}
                            />
                        </CardItem>
                    </CardItem>
                    <CardItem style={this.styles.CardItem}>
                        <CardItem style={[this.styles.CardItemLeft]}></CardItem>
                        <CardItem style={[this.styles.CardItemRight, { marginLeft: -10 }]}>
                            <CheckBox onPress={this.onCheckSms} checked={this.state.checkSms} color={AppConfig.blueBackground}></CheckBox>
                            <Text style={[this.styles.TextTitle, { marginLeft: 20 }]}>Sms        </Text>
                        </CardItem>
                    </CardItem>
                    <View style={{ flex: 1 }}>

                        <View style={{ flex: 1, minHeight: 200, maxHeight: 400 }}>
                            <CardItem style={this.styles.CardItem}>
                                <Text style={[this.styles.TextTitle, { fontSize: this.props.fontSize.FontSizeLarge, }]}>Danh sách cán bộ duyệt:</Text>
                            </CardItem>
                            <CardItem>
                                <SearchList ref={(ref) => { this.searchList = ref }} onSearchList={this.onSearchList} fontSize={this.props.fontSize} />
                            </CardItem>
                            <CardItem>

                                <FlatList
                                    data={this.state.listVT}
                                    extraData={this.state}
                                    keyExtractor={(item, index) => item.ma_ctcb_kc.toString()}
                                    renderItem={({ item }) => {
                                        let flag = this.state.listCheckVT.includes(item.ma_ctcb_kc)
                                        return (
                                            <ListVanThuItem
                                                item={item}
                                                checkExistOne={(ma_ctcb_kc) => this.checkExistOne(ma_ctcb_kc)}
                                                checked={flag}
                                                fontSize={this.props.fontSize}
                                                styles={this.styles}
                                            />)
                                    }}
                                    removeClippedSubviews={true}
                                />
                            </CardItem>
                        </View>
                        <View style={{ flex: 1, minHeight: 200, maxHeight: 400 }}>
                            <CardItem style={this.styles.CardItem}>
                                <Text style={[this.styles.TextTitle, { fontSize: this.props.fontSize.FontSizeLarge, }]}>Danh sách văn thư nhận:</Text>
                            </CardItem>
                            <CardItem>
                                <SearchList ref={(ref) => { this.searchList = ref }} onSearchList={this.onSearchList} fontSize={this.props.fontSize} />
                            </CardItem>
                            <CardItem>
                                <FlatList
                                scrollEnabled=
                                    data={this.state.listVT}
                                    extraData={this.state}
                                    keyExtractor={(item, index) => item.ma_ctcb_kc.toString()}
                                    renderItem={({ item }) => {
                                        let flag = this.state.listCheckVT.includes(item.ma_ctcb_kc)
                                        return (
                                            <ListVanThuItem
                                                item={item}
                                                checkExistOne={(ma_ctcb_kc) => this.checkExistOne(ma_ctcb_kc)}
                                                checked={flag}
                                                fontSize={this.props.fontSize}
                                                styles={this.styles}
                                            />)
                                    }}
                                    removeClippedSubviews={true}
                                />

                            </CardItem>
                        </View>
                    </View>

                </ScrollView> */}

                <ScrollView style={this.styles.scrollContainer}>
                    <CardItem style={this.styles.CardItem}>
                        <CardItem style={[this.styles.CardItemLeft]}>
                            <Text style={[this.styles.TextTitle]}>Ý kiến xử lý</Text>
                        </CardItem>
                        <CardItem style={[this.styles.CardItemRight]}>
                            <Textarea
                                autoCorrect={false}
                                style={this.styles.TextInput}
                                rowSpan={3}
                                value={this.state.yKienXuLy}
                                onChangeText={(text) => { this.setState({ yKienXuLy: text }) }}
                            />
                        </CardItem>
                    </CardItem>
                    <CardItem style={this.styles.CardItem}>
                        <CardItem style={[this.styles.CardItemLeft]}></CardItem>
                        <CardItem style={[this.styles.CardItemRight, { marginLeft: -10 }]}>
                            <CheckBox onPress={this.onCheckSms} checked={this.state.checkSms} color={AppConfig.blueBackground}></CheckBox>
                            <Text style={[this.styles.TextTitle, { marginLeft: 20 }]}>Sms        </Text>
                        </CardItem>
                    </CardItem>
                    <CardItem style={this.styles.CardItem}>
                        <Text style={[this.styles.TextTitle, { fontSize: this.props.fontSize.FontSizeLarge, }]}>Danh sách cán bộ duyệt:</Text>
                    </CardItem>
                    <CardItem>
                        <SearchList ref={(ref) => { this.searchListLD = ref }} onSearchList={this.onSearchListLD} fontSize={this.props.fontSize} />
                    </CardItem>
                    <FlatList
                        data={this.state.listLD}
                        extraData={this.state}
                        keyExtractor={(item, index) => item.ma.toString()}
                        renderItem={({ item }) => {
                            let flag = this.state.listCheckLD.includes(item.ma)
                            return (
                                <ListLanhDaoItem
                                    item={item}
                                    checkExistOne={(ma) => this.checkExistOneLD(ma)}
                                    checked={flag}
                                    fontSize={this.props.fontSize}
                                    styles={this.styles}
                                />)
                        }}
                        removeClippedSubviews={true}
                    />
                    <CardItem style={this.styles.CardItem}>
                        <Text style={[this.styles.TextTitle, { fontSize: this.props.fontSize.FontSizeLarge, }]}>Danh sách văn thư nhận:</Text>
                    </CardItem>
                    <CardItem>
                        <SearchList ref={(ref) => { this.searchList = ref }} onSearchList={this.onSearchList} fontSize={this.props.fontSize} />
                    </CardItem>
                    <FlatList
                        data={this.state.listVT}
                        extraData={this.state}
                        keyExtractor={(item, index) => item.ma_ctcb_kc.toString()}
                        renderItem={({ item }) => {
                            let flag = this.state.listCheckVT.includes(item.ma_ctcb_kc)
                            return (
                                <ListVanThuItem
                                    item={item}
                                    checkExistOne={(ma_ctcb_kc) => this.checkExistOne(ma_ctcb_kc)}
                                    checked={flag}
                                    fontSize={this.props.fontSize}
                                    styles={this.styles}
                                />)
                        }}
                        removeClippedSubviews={true}
                    />
                </ScrollView>
            </Container >
        );
    }
}

function mapStateToProps(state) {
    return {
        fontSize: state.fontSize,
    }
}

export default connect(mapStateToProps)(VBDi_Details_CVChuyenVT)