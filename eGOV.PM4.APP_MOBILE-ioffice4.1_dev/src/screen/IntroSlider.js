import React, { Component } from 'react';
import { StyleSheet, Image, View, AsyncStorage, } from 'react-native';
import { connect } from "react-redux"
import { Text } from "native-base"
import AppIntroSlider from 'react-native-app-intro-slider';
import Ionicons from 'react-native-vector-icons/Ionicons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { AppConfig } from "../AppConfig"

const slides = [
    {
        key: 'hieunang',
        title: 'HIỆU NĂNG',
        text: 'Ứng dụng xử lý nhanh các thao tác vuốt chạm cho bạn cảm giác thoải mái khi sử dụng.',
        image: require("../assets/images/performance.png"),
        backgroundColor: '#59b2ab',
    },
    {
        key: 'giaodien',
        title: 'GIAO DIỆN',
        text: 'Giao diện và màu sắc thân thiện, dễ sử dụng mang đến trải nghiệm tuyệt vời hơn!',
        image: require("../assets/images/interface.png"),
        backgroundColor: '#59b2ab',
    },
    {
        key: 'xulyvanbanden',
        title: 'XỬ LÝ VĂN BẢN ĐẾN',
        text: 'Xử lý các văn bản đến một cách dễ dàng chỉ với vài thao tác vuốt chạm!',
        image: require("../assets/images/vbden.png"),
        backgroundColor: '#59b2ab',
    },
    {
        key: 'xulyvanbandi',
        title: 'XỬ LÝ VĂN BẢN ĐI',
        text: 'Các văn bản sẽ được xử lý và xét duyệt trước khi được gửi tới người nhận.',
        image: require("../assets/images/vbdi.png"),
        backgroundColor: '#59b2ab',
    },
    // {
    //     key: 'timkiemchung',
    //     title: 'TÌM KIẾM CHUNG',
    //     text: 'Người dùng chỉ cần nhâp từ khóa tìm kiếm tất cả văn bản chứa từ khóa sẽ được liệt kê.',
    //     image: require("../assets/images/search.png"),
    //     backgroundColor: '#59b2ab',
    // },
    {
        key: 'thongtindieuhanh',
        title: 'THÔNG TIN ĐIỀU HÀNH',
        text: 'Các văn bản điều hành, chỉ đạo sẽ được phân thành một chức năng để dễ quản lý.',
        image: require("../assets/images/ttdhitr.png"),
        backgroundColor: '#59b2ab',
    },
    {
        key: 'xulyvanbannoibo',
        title: 'XỬ LÝ VĂN BẢN NỘI BỘ',
        text: 'Văn bản nội bộ là một chức năng giúp quản lý và lưu hành văn bản trong phạm vi giới hạn.',
        image: require("../assets/images/vbnbitr.png"),
        backgroundColor: '#59b2ab',
    },
    {
        key: 'lichcongtac',
        title: 'LỊCH CÔNG TÁC',
        text: 'Với cách bố trí giao diện tinh gọn, đẹp mắt sẽ mang lại cảm giác xem dễ hiểu và thích thú!',
        image: require("../assets/images/LCT2.png"),
        backgroundColor: '#febe29',
    },
    // {
    //     key: 'lichhop',
    //     title: 'LỊCH HỌP',
    //     text: 'Việc tích hợp chức năng xem cuộc họp sẽ giúp bạn không bị bỏ lỡ cuộc họp nào!',
    //     image: require("../assets/images/LH.png"),
    //     backgroundColor: '#22bcb5',
    // },
    // {
    //     key: 'sms',
    //     title: 'TIN NHẮN SMS',
    //     text: 'Chức năng này có thể gửi và nhận tin nhắn một cách nhanh chóng và bảo mật!',
    //     image: require("../assets/images/sms1.png"),
    //     backgroundColor: '#22bcb5',
    // },
    // {
    //     key: 'tracuudanhba',
    //     title: 'TRA CỨU DANH BẠ',
    //     text: 'Người dùng có thể tra cứu danh bạ ngay trong app mà không cần phải tìm kiếm nơi nào khác!',
    //     image: require("../assets/images/address-book.png"),
    //     backgroundColor: '#22bcb5',
    // },
];

class IntroSlider extends Component {
    _renderItem = ({ item }) => {
        return (
            <View style={this.styles.slide}>
                <View style={{ alignItems: 'center' }}>
                    <Text style={this.styles.title}>{item.title}</Text>
                    <Image source={item.image} style={this.styles.image} />
                </View>
                <View style={this.styles.viewNoiDung}><Text style={this.styles.text}>{item.text}</Text></View>
                <View style={{ marginTop: 30 }}></View>
            </View>
        );
    }

    _renderNextButton = () => {
        return (
            <View style={this.styles.buttonCircle}>
                <Ionicons name="md-arrow-round-forward" color="rgba(255, 255, 255, .9)" size={24} style={this.styles.icon} />
            </View>
        );
    };

    _renderDoneButton = () => {
        return (
            <View style={this.styles.buttonCircle}>
                <Ionicons name="md-checkmark" color="rgba(255, 255, 255, .9)" size={24} style={this.styles.icon} />
            </View>
        );
    };

    _renderSkipButton = () => {
        return (
            <View style={this.styles.buttonCircle}>
                <AntDesign name="close" color="rgba(255, 255, 255, .9)" size={24} style={this.styles.icon} />
            </View>
        );
    };

    _onDone = async () => {
        await AsyncStorage.setItem("firstTimesInstall", "false")
        this.props.navigation.navigate("Login")
    }
    _onSkip = async () => {
        await AsyncStorage.setItem("firstTimesInstall", "false")
        this.props.navigation.navigate("Login")
    }
    render() {

        this.styles = StyleSheet.create({
            slide: {
                flex: 1,
                alignItems: 'center',
                justifyContent: 'space-around',
                backgroundColor: '#299cec',
                alignItems: 'center'
            },
            text: {
                color: 'rgba(255, 255, 255, 0.8)',
                backgroundColor: 'transparent',
                textAlign: 'center',
                paddingHorizontal: 18,
            },
            title: {
                fontSize: this.props.fontSize.FontSizeXLarge,
                color: '#FFF',
                backgroundColor: 'transparent',
                textAlign: 'center',
                marginBottom: 16,
            },
            buttonCircle: {
                width: 40,
                height: 40,
                backgroundColor: 'rgba(0, 0, 0, .2)',
                borderRadius: 20,
                justifyContent: 'center',
                alignItems: 'center',
            },
            icon: {
                backgroundColor: 'transparent'
            },
            image: {
                width: 200,
                height: 200,
            },
            viewNoiDung: {
                width: '85%',
                alignItems: 'center'
            },
        });

        return (
            <AppIntroSlider
                slides={slides}
                renderItem={this._renderItem}
                renderNextButton={this._renderNextButton}
                renderDoneButton={this._renderDoneButton}
                renderSkipButton={this._renderSkipButton}
                showSkipButton={true}
                onDone={this._onDone}
                onSkip={this._onSkip}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        fontSize: state.fontSize,
    }
}

export default connect(mapStateToProps)(IntroSlider)