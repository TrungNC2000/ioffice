import React, { Component } from 'react';
import { StyleSheet, Alert, AsyncStorage, View, Dimensions, Platform, TextInput, TouchableOpacity, Image, PixelRatio } from "react-native"
import { Container, Content, Text, Card, CardItem, Button, Toast, Icon } from "native-base"
import { connect } from "react-redux"
import Modal from "react-native-modal"
import Orientation from 'react-native-orientation'
import { DrawerActions } from "react-navigation"
import { AppConfig } from "../AppConfig"
import API from "../networks"
import HeaderWithLeft from "../components/Header/HeaderWithLeft"
import { getUrlAvatar, ToastSuccess } from "../untils/TextUntils"
import { CheckSession } from "../untils/NetInfo"
import axios from "axios"
import { Switch } from 'react-native-gesture-handler';

class ModalDoiMatKhau extends Component {
    constructor(props) {
        super(props)
        this.state = {
            onChangeSize: "PORTRAIT",
            matkhaucu: "",
            matkhaumoi: "",
            xacnhan: "",
            visibleMKC: true,
            visibleMKM: true,
            visibleXN: true,
        }
    }

    componentDidMount = async () => {
        Orientation.getOrientation((err, orientation) => {
            this.setState({ onChangeSize: orientation })
        });
        Dimensions.addEventListener("change", this.onChangeSize);
    }

    componentWillUnmount() {
        Dimensions.removeEventListener("change", this.onChangeSize);
    }

    onChangeSize = () => {
        Orientation.getOrientation((err, orientation) => {
            this.setState({ onChangeSize: orientation })
        });
    }

    onVisibleMKC = () => {
        this.setState({ visibleMKC: !this.state.visibleMKC })
    }

    onVisibleMKM = () => {
        this.setState({ visibleMKM: !this.state.visibleMKM })
    }

    onVisibleXN = () => {
        this.setState({ visibleXN: !this.state.visibleXN })
    }

    render() {
        const deviceWidth = Dimensions.get("window").width;
        let deviceHeight = Dimensions.get("window").height
        this.styles = this.props.styles
        if (this.state.onChangeSize === "PORTRAIT") {
            deviceHeight = Platform.OS === "ios"
                ? Dimensions.get("window").height
                : require("react-native-extra-dimensions-android").get("REAL_WINDOW_HEIGHT");
        } else {
            deviceHeight = Platform.OS === "ios"
                ? Dimensions.get("window").height
                : require("react-native-extra-dimensions-android").get("REAL_WINDOW_WIDTH");
        }

        return (
            <Modal
                deviceWidth={deviceWidth}
                deviceHeight={deviceHeight}
                avoidKeyboard={true}
                isVisible={this.props.isModalVisible}
                supportedOrientations={["landscape", "landscape-left", "landscape-right", "portrait", "portrait-upside-down"]}
                style={{ alignItems: 'center', justifyContent: "center" }}
                hideModalContentWhileAnimating={true}
            >
                <View style={{ width: 320, height: this.state.onChangeSize === "PORTRAIT" ? deviceHeight / 2 : deviceHeight - 100, backgroundColor: "#FFFFFF", paddingLeft: 10, paddingRight: 10 }}>
                    <View style={{ position: 'absolute', zIndex: 99, height: 55, left: 0, right: 0, top: 0, justifyContent: "center", padding: 4, alignItems: "center", backgroundColor: AppConfig.blueBackground }}>
                        <Text style={{ fontSize: this.props.fontSize.FontSizeLarge, color: "#FFFFFF" }}>Thay đổi mật khẩu</Text>
                    </View>
                    <Card transparent noShadow style={[this.styles.CardItem, { marginTop: 55, }]}>
                        <CardItem style={this.styles.CardItem}>
                            <CardItem style={[this.styles.CardItemLeft]}>
                                <Text style={[this.styles.TextTitle]}>Mật khẩu cũ</Text>
                            </CardItem>
                            <CardItem style={[this.styles.CardItemBody]}>
                                <TextInput
                                    ref={input => { this.txtMatKhauCu = input }}
                                    secureTextEntry={this.state.visibleMKC}
                                    style={this.styles.TextInput}
                                    editable={true}
                                    value={this.props.matkhaucu}
                                    onChangeText={(matkhaucu) => { this.props.onChangeTextMKC(matkhaucu) }}
                                />
                            </CardItem>
                            <CardItem style={[this.styles.CardItemRight]}>
                                <TouchableOpacity onPressIn={this.onVisibleMKC} onPressOut={this.onVisibleMKC} style={{ flex: 1 }}>
                                    <Icon name="remove-red-eye" type="MaterialIcons" style={this.styles.Icon}></Icon>
                                </TouchableOpacity>
                            </CardItem>
                        </CardItem>
                        <CardItem style={this.styles.CardItem}>
                            <CardItem style={[this.styles.CardItemLeft]}>
                                <Text style={[this.styles.TextTitle]}>Mật khẩu mới</Text>
                            </CardItem>
                            <CardItem style={[this.styles.CardItemBody]}>
                                <TextInput
                                    secureTextEntry={this.state.visibleMKM}
                                    style={this.styles.TextInput}
                                    editable={true}
                                    value={this.props.matkhaumoi}
                                    onChangeText={(matkhaumoi) => { this.props.onChangeTextMKM(matkhaumoi) }}
                                />
                            </CardItem>
                            <CardItem style={[this.styles.CardItemRight]}>
                                <TouchableOpacity onPressIn={this.onVisibleMKM} onPressOut={this.onVisibleMKM} style={{ flex: 1 }}>
                                    <Icon name="remove-red-eye" type="MaterialIcons" style={this.styles.Icon}></Icon>
                                </TouchableOpacity>
                            </CardItem>
                        </CardItem>
                        <CardItem style={this.styles.CardItem}>
                            <CardItem style={[this.styles.CardItemLeft]}>
                                <Text style={[this.styles.TextTitle]}>Nhập lại mật khẩu mới</Text>
                            </CardItem>
                            <CardItem style={[this.styles.CardItemBody]}>
                                <TextInput
                                    secureTextEntry={this.state.visibleXN}
                                    style={this.styles.TextInput}
                                    editable={true}
                                    value={this.props.xacnhan}
                                    onChangeText={(xacnhan) => { this.props.onChangeTextXN(xacnhan) }}
                                />
                            </CardItem>
                            <CardItem style={[this.styles.CardItemRight]}>
                                <TouchableOpacity onPressIn={this.onVisibleXN} onPressOut={this.onVisibleXN} style={{ flex: 1 }}>
                                    <Icon name="remove-red-eye" type="MaterialIcons" style={this.styles.Icon}></Icon>
                                </TouchableOpacity>
                            </CardItem>
                        </CardItem>
                    </Card>
                    <View style={{ position: 'absolute', zIndex: 99, height: 60, left: 0, right: 0, bottom: 0, flexDirection: "row", justifyContent: "space-evenly", paddingTop: 10, alignItems: "center" }}>
                        <Button bordered rounded iconLeft onPress={_ => this.props.onChangePass()}><Icon name="done" type="MaterialIcons"></Icon><Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>Thay đổi</Text></Button>
                        <Button danger bordered rounded iconLeft onPress={_ => this.props.toggleModal()}><Icon name="close" type="MaterialIcons"></Icon><Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>Đóng</Text></Button>
                    </View>
                </View>
            </Modal>
        )
    }
}

class UserInfo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userInfo: {},
            matkhaucu: "",
            matkhaumoi: "",
            xacnhan: "",
            isModalVisible: false,
            avatar: require("../assets/images/iAvatarUser.png"),
            switchValue: global.ngay_dang_ky_otp ? true : false,
            defaultSwitchValue: global.ngay_dang_ky_otp ? true : false
        }
    }

    componentWillMount = () => {
        this.getInfoUser()
    };


    componentDidMount = () => {
        this.didBlurSubscription = this.props.navigation.addListener('didFocus', payload => { CheckSession(this.props.navigation) })

    };

    componentWillUnmount = () => {
        this.didBlurSubscription.remove()
    };

    setAvatar = (avatar) => {
        if (avatar !== null) {
            let url = getUrlAvatar(this.props.cb_dsctcb.avatar)
            axios({
                method: "GET",
                url: url,
                timeout: AppConfig.TIME_OUT_SHORT
            }).then(response => {
                if (response.status === 200) {
                    this.setState({
                        avatar: { uri: url }
                    })
                }
            }).catch(error => {
            })
        }
    }

    getInfoUser = () => {
        API.Login.AU_DSCBBID(this.props.cb_dsctcb.refresh_token).then((userInfo) => {
            let flagOTP = userInfo.ngay_dang_ky_otp ? true : false
            this.setState({ userInfo, switchValue: flagOTP, defaultSwitchValue: flagOTP })
            this.setAvatar(userInfo.avatar)
        })
    }

    onLogout = () => {
        Alert.alert(
            "Xác nhận",
            "Bạn muốn đăng xuất tài khoản?",
            [
                { text: "Không", onPress: () => console.log("Logout"), style: "cancel" },
                {
                    text: "Có", onPress: () => {
                        API.Login.AU_DEL_FCM()
                        AsyncStorage.setItem("chkSaveLogin", "false")
                        AsyncStorage.setItem("chkDeepLinkSso", "false")
                        AsyncStorage.setItem("chkSaveDeepLinkSso", "false") 
                        AsyncStorage.setItem("Url_Api_SaveLogin", "")
                        AsyncStorage.setItem("US_First_Load_Page", "Home")
                        this.props.dispatch({ type: "NORMAN_FONT" })
                        global.isLogin = false
                        this.props.navigation.navigate("Login", { loaiDangNhap: "dangXuat" })
                    }
                }
            ],
            { cancelable: false }
        )
    }

    toggleModal = () => {
        if (!this.state.isModalVisible === true) {
            this.setState({
                matkhaucu: "",
                matkhaumoi: "",
                xacnhan: "",
            })
        }
        this.setState({ isModalVisible: !this.state.isModalVisible })
    }

    onChangePass = () => {
        let { matkhaucu, matkhaumoi, xacnhan } = this.state
        let macanbo = global.ma_can_bo
        if (matkhaumoi !== xacnhan) {
            alert("Mật khẩu mới và mật khẩu xác nhận không trùng khớp!")
        } else {
            API.Login.AU_KTTMKC(macanbo, matkhaucu).then((response) => {
                if (response.trung_ma_mat_khau === 1) {
                    API.Login.AU_CNMK(macanbo, matkhaucu, matkhaumoi).then((response) => {
                        if (response.message === "Thực thi thành công") {
                            Alert.alert(
                                "Thông báo",
                                "Thành công. Đăng nhập lại để tiếp tục sử dụng",
                                [{
                                    text: "Đồng ý", onPress: () => {
                                        this.toggleModal()
                                        setTimeout(() => {
                                            API.Login.AU_DEL_FCM()
                                            AsyncStorage.setItem("chkSaveLogin", "false")
                                            AsyncStorage.setItem("Url_Api_SaveLogin", "")
                                            this.props.navigation.navigate("Login")
                                        }, 300);
                                    }, style: "cancel"
                                }],
                                { cancelable: false }
                            )
                        } else {
                            Alert.alert(
                                "Thông báo",
                                "Lỗi. Vui lòng thử lại sau!",
                                [{ text: "Đóng", onPress: () => { console.log("close alert") }, style: "cancel" }],
                                { cancelable: false }
                            )
                        }
                    })
                } else {
                    Alert.alert(
                        "Thông báo",
                        "Mật khẩu cũ không đúng. Vui lòng thử lại!",
                        [{ text: "Đóng", onPress: () => { console.log("close alert") }, style: "cancel" }],
                        { cancelable: false }
                    )
                }
            })
        }
    }

    // Quan Vo creates this function
    toggleSwitch = (value) => {
        this.setState({ switchValue: value })
    }

    // Quan Vo creates this function
    _onPressSave = () => {
        let dang_ky_otp = (this.state.switchValue) ? 1 : 0;
        let di_dong_can_bo = global.di_dong_can_bo;
        let ma_can_bo_kc = global.ma_can_bo;
        API.Login.AU_DKOTPNVSMS(dang_ky_otp, di_dong_can_bo, ma_can_bo_kc).then((response) => {
            if (response) {
                if (dang_ky_otp === 1) {
                    ToastSuccess("Đăng ký OTP thành công!")
                } else {
                    ToastSuccess("Hủy dăng ký OTP thành công!")
                }
                this.setState({ defaultSwitchValue: !this.state.defaultSwitchValue })
            } else {
                Toast.show({ text: "Thao tác thất bại!", type: "warning", buttonText: "Đóng", position: "top", duration: 2000 })
            }
        })
    }

    render() {

        this.styles = StyleSheet.create({
            container: {
                flex: 1,
                backgroundColor: "#FFFFFF"
            },
            CardItem: {
                paddingTop: 5,
                paddingBottom: 5,
                borderBottomWidth: 0,
                //justifyContent: "center"
            },
            textTieuDe: {
                color: AppConfig.grayText,
                fontSize: this.props.fontSize.FontSizeNorman,
                width: 100
            },
            textNoiDung: {
                fontSize: this.props.fontSize.FontSizeNorman,
                width: 200
            },
            CardItemLeft: {
                width: 100
            },
            CardItemBody: {
                flex: 1
            },
            CardItemRight: {
                backgroundColor: "transparent",
                width: 30,
                position: 'absolute',
                zIndex: 99,
                height: 30,
                right: 20,
                bottom: 0,
                top: 14,
                alignItems: 'center',
                justifyContent: "center",
            },
            TextTitle: {
                fontSize: this.props.fontSize.FontSizeSmall,
            },
            TextInput: {
                flex: 1,
                fontFamily: AppConfig.fontFamily,
                fontSize: this.props.fontSize.FontSizeNorman / PixelRatio.getFontScale(),
                color: "#000000",
                borderColor: AppConfig.defaultLineColor,
                borderWidth: AppConfig.defaultLineWidth,
                borderRadius: 5,
                marginRight: 2,
                paddingRight: 30,
                minHeight: 40,
            },
            Icon: {
                flex: 1,
                color: AppConfig.blueBackground,
                fontSize: this.props.fontSize.FontSizeXLarge,
                padding: 5,
            },
            CardItem: {
                paddingTop: 5,
                paddingBottom: 5,
                borderBottomWidth: 0,
            },
            CardItemLeft: {
                width: 100
            },
            CardItemBody: {
                flex: 1
            },
            CardItemRight: {
                backgroundColor: "transparent",
                width: 30,
                position: 'absolute',
                zIndex: 99,
                height: 30,
                right: 20,
                bottom: 0,
                top: 14,
                alignItems: 'center',
                justifyContent: "center",
            },
            TextTitle: {
                fontSize: this.props.fontSize.FontSizeSmall,
            },
            TextInput: {
                flex: 1,
                fontFamily: AppConfig.fontFamily,
                fontSize: this.props.fontSize.FontSizeNorman / PixelRatio.getFontScale(),
                color: "#000000",
                borderColor: AppConfig.defaultLineColor,
                borderWidth: AppConfig.defaultLineWidth,
                borderRadius: 5,
                marginRight: 2,
                paddingRight: 30,
                minHeight: 40,
            },
            Icon: {
                flex: 1,
                color: AppConfig.blueBackground,
                fontSize: this.props.fontSize.FontSizeXLarge,
                padding: 5,
            },
            Button: {
                margin: 3
            }
        })

        return (
            <Container style={this.styles.container}>
                <HeaderWithLeft
                    title="Thông tin cá nhân"
                    buttonName="menu"
                    onPressLeftButton={_ => { this.props.navigation.dispatch(DrawerActions.toggleDrawer()) }}
                />
                <Content>
                    <ModalDoiMatKhau
                        isModalVisible={this.state.isModalVisible}
                        toggleModal={this.toggleModal}
                        onChangePass={this.onChangePass}
                        matkhaucu={this.state.matkhaucu}
                        matkhaumoi={this.state.matkhaumoi}
                        xacnhan={this.state.xacnhan}
                        onChangeTextMKC={(matkhaucu) => { this.setState({ matkhaucu }) }}
                        onChangeTextMKM={(matkhaumoi) => { this.setState({ matkhaumoi }) }}
                        onChangeTextXN={(xacnhan) => { this.setState({ xacnhan }) }}
                        fontSize={this.props.fontSize}
                        styles={this.styles}
                    />
                    <Card transparent noShadow style={[this.styles.CardItem, { alignItems: 'center', }]}>
                        <Card transparent noShadow style={[{ width: 300 }]}>
                            <CardItem style={[this.styles.CardItem, { justifyContent: "center" }]}>
                                <Image
                                    style={{ height: 200, width: 200 }}
                                    source={this.state.avatar}
                                />
                            </CardItem>
                            <CardItem style={[this.styles.CardItem]}>
                                <Text style={this.styles.textTieuDe}>Họ tên:</Text>
                                <Text style={this.styles.textNoiDung}>{this.state.userInfo.ho_va_ten_can_bo ? this.state.userInfo.ho_va_ten_can_bo : ""}</Text>
                            </CardItem>
                            <CardItem style={[this.styles.CardItem]}>
                                <Text style={this.styles.textTieuDe}>Điện thoại:</Text>
                                <Text style={this.styles.textNoiDung}>{this.state.userInfo.di_dong_can_bo ? this.state.userInfo.di_dong_can_bo : ""}</Text>
                            </CardItem>
                            <CardItem style={[this.styles.CardItem]}>
                                <Text style={this.styles.textTieuDe}>Ngày sinh:</Text>
                                <Text style={this.styles.textNoiDung}>{this.state.userInfo.ngay_sinh_vn ? this.state.userInfo.ngay_sinh_vn : ""}</Text>
                            </CardItem>
                            <CardItem style={[this.styles.CardItem]}>
                                <Text style={this.styles.textTieuDe}>Giới tính:</Text>
                                <Text style={this.styles.textNoiDung}>{this.state.userInfo.gioi_tinh ? this.state.userInfo.gioi_tinh === 1 ? "Nam" : "Nữ" : ""}</Text>
                            </CardItem>
                            <CardItem style={[this.styles.CardItem]}>
                                <Text style={this.styles.textTieuDe}>Chức vụ:</Text>
                                <Text style={this.styles.textNoiDung}>{this.state.userInfo.ten_chuc_vu ? this.state.userInfo.ten_chuc_vu : ""}</Text>
                            </CardItem>
                            <CardItem style={[this.styles.CardItem]}>
                                <Text style={this.styles.textTieuDe}>Đơn vị:</Text>
                                <Text style={this.styles.textNoiDung}>{this.props.cb_dsctcb.ten_don_vi}</Text>
                            </CardItem>
                            <CardItem style={[this.styles.CardItem, { flexDirection: 'row', justifyContent: "space-between" }]}>
                                <Text style={this.styles.textTieuDe}>Xác thực OTP:</Text>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: 200, paddingRight: 40 }}>
                                    <View>
                                        <Switch onValueChange={this.toggleSwitch} value={this.state.switchValue} />
                                    </View>
                                    {
                                        (this.state.defaultSwitchValue !== this.state.switchValue) && (
                                            <Button small onPress={this._onPressSave}><Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>Lưu</Text></Button>
                                        )
                                    }
                                </View>
                            </CardItem>
                            <CardItem style={[this.styles.CardItem, { justifyContent: "space-around", flexWrap: "wrap" }]}>
                                <Button onPress={this.toggleModal} style={this.styles.Button}>
                                    <Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>Đổi mật khẩu</Text>
                                </Button>
                                {/* <Button small onPress={this.toggleModal} style={this.styles.Button}>
                                    <Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>Cập nhật SĐT</Text>
                                </Button> */}
                                <Button onPress={this.onLogout} style={this.styles.Button}>
                                    <Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>Đăng xuất</Text>
                                </Button>
                            </CardItem>
                        </Card>
                    </Card>
                </Content>
            </Container>
        );
    }
}

function mapStateToProps(state) {
    return {
        cb_dsctcb: state.cb_dsctcb.data,
        fontSize: state.fontSize,
    }
}


export default connect(mapStateToProps)(UserInfo)