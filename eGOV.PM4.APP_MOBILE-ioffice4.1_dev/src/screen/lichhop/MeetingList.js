import React, { Component, PureComponent } from 'react';
import { Container, Text, CardItem, Spinner, View } from "native-base"
import { StyleSheet, TouchableWithoutFeedback, Keyboard, FlatList, TouchableOpacity, } from 'react-native'
import { connect } from "react-redux"
import { DrawerActions } from "react-navigation"
import HeaderWithLeft from "../../components/Header/HeaderWithLeft"
import { CheckSession } from "../../untils/NetInfo"
import SearchHeader from "../../components/SearchHeader"
import API from "../../networks"
import { AppConfig } from "../../AppConfig"
import { ConvertDateTimeDetail } from "../../untils/TextUntils"


class LHTab_Item extends PureComponent {
    constructor(props) {
        super(props)
        this.state = {
            isBold: this.props.item.ngay_xem === null ? true : false
        }
    }

    updateViewCV = async () => {
        let ma_thanh_phan_hop_kc = this.props.item.ma_thanh_phan_hop_kc
        await API.LichHop.AU_LH_CNNX(ma_thanh_phan_hop_kc).then((result) => {
            if (result) {
                this.setState({ isBold: false })
            }
        })
    }

    onPressItem = () => {
        this.props.navigation.navigate("Meeting_Details", {
            ma_thanh_phan_hop_kc: this.props.item.ma_thanh_phan_hop_kc,
            updateViewCV: this.updateViewCV,
        })
        this.setState({ isBold: false })
    }

    render() {
        this.styles = this.props.styles
        const { item } = this.props
        let styleTemp = this.styles.textNoBold
        if (this.state.isBold) {
            styleTemp = this.styles.textBold
        }
        return (
            <CardItem noShadow style={this.styles.CardItem} key={item.ma_cuoc_hop_kc}>
                <TouchableOpacity style={{ justifyContent: 'center', flex: 1, left: 0 }} onPress={this.onPressItem}>
                    <Text numberOfLines={1} ellipsizeMode="tail" style={styleTemp}>{item.ten_cuoc_hop} </Text>
                    <Text numberOfLines={2} ellipsizeMode="tail" style={styleTemp}>Phòng họp: {item.ten_phong_hop ? item.ten_phong_hop : "Rỗng"} </Text>
                    <Text numberOfLines={2} ellipsizeMode="tail" style={styleTemp}>Vai trò họp: {item.ten_vai_tro_hop ? item.ten_vai_tro_hop : "Rỗng"} </Text>
                    <Text numberOfLines={2} ellipsizeMode="tail" style={[this.styles.txtNoiDung]}>Thời gian bắt đầu: {item.thoi_gian_bat_dau ? ConvertDateTimeDetail(item.thoi_gian_bat_dau) : "Rỗng"} </Text>
                    <Text numberOfLines={2} ellipsizeMode="tail" style={[this.styles.txtNoiDung]}>Thời gian kết thúc: {item.thoi_gian_ket_thuc ? ConvertDateTimeDetail(item.thoi_gian_ket_thuc) : "Rỗng"} </Text>
                    <Text numberOfLines={2} ellipsizeMode="tail" style={[this.styles.txtNoiDung]}>Tình trạng: {(item.trang_thai === 1) ? "chưa hợp" : (item.trang_thai === 2) ? "đang họp" : (item.trang_thai === 3) ? "đã họp" : "đã hủy"}</Text>
                </TouchableOpacity>
            </CardItem>
        )
    }
}


class MeetingList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            refreshing: false,
            dataSource: [],
        }
        this.page = 1
    }

    componentDidMount = () => {
        this.didBlurSubscription = this.props.navigation.addListener('didFocus', payload => { CheckSession(this.props.navigation) })
        this.getMeetingList()
    };

    componentWillUnmount = () => {
        this.didBlurSubscription.remove()
    };

    onRefresh = () => {
        if (!this.state.loading) {
            this.page = 1
            this.setState({ refreshing: true, loading: true })
            this.getMeetingList()
        }
    }

    getMeetingList = () => {
        let page = this.page
        let keyWord = this.searchHeader.getKeyWord()
        let size = AppConfig.pageSizeLH
        API.LichHop.AU_LH_TKLHCN(keyWord, page, size).then(response => {
            this.setState({ dataSource: page === 1 ? response : [...this.state.dataSource, ...response] }, () => {
                this.setState({ loading: false, refreshing: false })
            })
        })
    }

    loadMore = () => {
        if (!this.state.loading) {
            this.page = this.page + 1
            this.getMeetingList()
        }
    }

    onSearchData = () => {
        if (!this.state.loading) {
            this.page = 1
            this.setState({ loading: true })
            this.getMeetingList()
        }
    }
    render() {

        this.styles = StyleSheet.create({
            searchBox: {
                height: 'auto',
                width: '100%',
                paddingTop: 10,
                // borderWidth: 2,
                // borderColor: 'red',
                backgroundColor: '#F5F5F5',
            },
            CardItem: {
                flex: 1,
                borderTopWidth: 0,
                borderBottomWidth: 0.55,
                borderLeftWidth: 0,
                borderRightWidth: 0,
                borderBottomColor: "lightgray",
                paddingLeft: 8,
                paddingRight: 8,
                paddingTop: 4,
                paddingBottom: 4,
                alignItems: 'center',
                justifyContent: "center",
            },
            colTieuDe: {
                width: "70%",
                justifyContent: "center"
            },
            textBold: {
                color: "#000000",
                fontSize: this.props.fontSize.FontSizeNorman,
                fontWeight: "bold",
                paddingBottom: AppConfig.defaultPadding
            },
            textNoBold: {
                color: "#000000",
                fontSize: this.props.fontSize.FontSizeNorman,
                paddingBottom: AppConfig.defaultPadding
            },
            txtNgayNhan: {
                width: 100,
                color: "#808080",
                fontSize: this.props.fontSize.FontSizeSmall,
                textAlign: "center",
                paddingBottom: AppConfig.defaultPadding
            },
            txtNoiDung: {
                color: "#808080",
                fontSize: this.props.fontSize.FontSizeSmall,
                paddingBottom: AppConfig.defaultPadding
            },
            colNgayGui: {
                width: "30%",
                justifyContent: "center",
                alignItems: "flex-end"
            },
            menuTrigger: {
                width: 40,
                height: 30,
                justifyContent: "center",
                alignItems: 'center'
            },
            menuTriggerIcon: {
                fontSize: this.props.fontSize.FontSizeXXXLarge,
                color: "#B0BEC5"
            },
            menuOptionsContainer: {
                width: 200
            },
            menuOptions: {
                borderBottomColor: AppConfig.defaultLineColor,
                borderBottomWidth: 0.5
            },
            menuOptionsText: {
                color: AppConfig.headerBackgroundColor,
                fontSize: this.props.fontSize.FontSizeNorman,
                paddingTop: AppConfig.defaultPadding,
                paddingBottom: AppConfig.defaultPadding
            },
            menuOptionsTextDelete: {
                color: "red",
                fontSize: this.props.fontSize.FontSizeNorman,
                paddingTop: AppConfig.defaultPadding,
                paddingBottom: AppConfig.defaultPadding
            }
        });

        return (
            <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
                <Container style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
                    <HeaderWithLeft
                        title="Lịch họp"
                        buttonName="menu"
                        onPressLeftButton={_ => { this.props.navigation.dispatch(DrawerActions.toggleDrawer()) }}
                    />
                    <View style={this.styles.searchBox}>
                        <SearchHeader
                            ref={(ref) => { this.searchHeader = ref }}
                            onSearchData={this.onSearchData}
                            fontSize={this.props.fontSize}
                        />
                    </View>
                    <View style={{ flex: 1 }}>
                        {
                            (this.state.loading) ? (
                                <View><Spinner color={AppConfig.blueBackground} /></View>
                            ) : (
                                    <FlatList
                                        ref={(ref) => { this.flatListRef = ref }}
                                        data={this.state.dataSource}
                                        keyExtractor={(item, index) => "key" + item.ma_cuoc_hop_kc + index}
                                        contentContainerStyle={{ paddingBottom: 0 }}
                                        renderItem={({ item }) => {
                                            return (
                                                <LHTab_Item
                                                    item={item}
                                                    navigation={this.props.navigation}
                                                    fontSize={this.props.fontSize}
                                                    styles={this.styles}
                                                />
                                            )
                                        }}
                                        onRefresh={this.onRefresh}
                                        refreshing={this.state.refreshing}
                                        onEndReached={this.loadMore}
                                        onEndReachedThreshold={2}
                                        removeClippedSubviews={true}
                                    />
                                )
                        }
                    </View>
                </Container>
            </TouchableWithoutFeedback>
        );
    }
}

function mapStateToProps(state) {
    return {
        fontSize: state.fontSize,
    }
}
export default connect(mapStateToProps)(MeetingList)