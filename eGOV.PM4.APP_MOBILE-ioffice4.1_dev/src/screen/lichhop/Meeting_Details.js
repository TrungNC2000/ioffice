import React, { Component } from 'react';
import { View, ScrollView, StyleSheet } from "react-native"
import { connect } from "react-redux"
import { Container, Text, Card, CardItem } from "native-base"
import { StackActions } from "react-navigation"
import { AppConfig } from "../../AppConfig"
import { ConvertDateTimeDetail } from "../../untils/TextUntils"
import HeaderWithLeft from "../../components/Header/HeaderWithLeft"
import API from "../../networks"
import ViewFile from "../../components/ViewFile"

class Table_ChiTiet_LH extends Component {
    shouldComponentUpdate(nextProps, nextState) {
        if (this.props !== nextProps) {
            return true
        }
        return false
    }

    render() {
        this.styles = this.props.styles
        if (this.props.dataDetails.length > 0) {
            const { ten_cuoc_hop, ten_phong_hop, ten_linh_vuc_hop, noi_dung, ten_don_vi,
                src_tai_lieu_dinh_kem, src_tai_lieu_ca_nhan, src_bien_ban_cuoc_hop, thoi_gian_bat_dau,
                thoi_gian_ket_thuc, ten_hinh_thuc_hop, ten_loai_hinh_hop, don_vi_chu_tri, ten_can_bo_moi,
                ghi_chu } = this.props.dataDetails[0]
            return (
                <Card noShadow style={this.styles.Card}>
                    {
                        (ten_cuoc_hop !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Tên cuộc họp: </Text>
                                <Text style={[this.styles.textRight, { fontWeight: "500" }]}>
                                    {ten_cuoc_hop}
                                </Text>
                            </CardItem>
                        )
                    }
                    {
                        (noi_dung !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Nội dung: </Text>
                                <Text style={[this.styles.textRight]}>
                                    {noi_dung}
                                </Text>
                            </CardItem>
                        )
                    }
                    {
                        (ten_phong_hop !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Địa điểm họp: </Text>
                                <Text style={[this.styles.textRight, { color: "red" }]}>
                                    {ten_phong_hop}
                                </Text>
                            </CardItem>
                        )
                    }
                    {
                        (thoi_gian_bat_dau !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Thời gian bắt đầu: </Text>
                                <Text style={[this.styles.textRight, { color: "red" }]}>
                                    {ConvertDateTimeDetail(thoi_gian_bat_dau)}
                                </Text>
                            </CardItem>
                        )
                    }
                    {
                        (thoi_gian_ket_thuc !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Thời gian kết thúc: </Text>
                                <Text style={[this.styles.textRight]}>
                                    {ConvertDateTimeDetail(thoi_gian_ket_thuc)}
                                </Text>
                            </CardItem>
                        )
                    }
                    {
                        (don_vi_chu_tri !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Đơn vị chủ trì: </Text>
                                <Text style={[this.styles.textRight, { color: AppConfig.blueBackground }]}>
                                    {don_vi_chu_tri}
                                </Text>
                            </CardItem>
                        )
                    }
                    {
                        (ten_don_vi !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Đơn vị diễn ra:</Text>
                                <Text style={[this.styles.textRight]}>
                                    {ten_don_vi}
                                </Text>
                            </CardItem>
                        )
                    }
                    {
                        (ten_can_bo_moi !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Cán bộ mời: </Text>
                                <Text style={[this.styles.textRight]}>
                                    {ten_can_bo_moi}
                                </Text>
                            </CardItem>
                        )
                    }
                    {
                        (ten_linh_vuc_hop !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Lĩnh vực: </Text>
                                <Text style={[this.styles.textRight]}>
                                    {ten_linh_vuc_hop}
                                </Text>
                            </CardItem>
                        )
                    }
                    {
                        (ten_hinh_thuc_hop !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Hình thức: </Text>
                                <Text style={[this.styles.textRight]}>
                                    {ten_hinh_thuc_hop}
                                </Text>
                            </CardItem>
                        )
                    }
                    {
                        (ten_loai_hinh_hop !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Loại hình: </Text>
                                <Text style={[this.styles.textRight]}>
                                    {ten_loai_hinh_hop}
                                </Text>
                            </CardItem>
                        )
                    }
                    {
                        (ghi_chu !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Ghi chú: </Text>
                                <Text style={[this.styles.textRight]}>
                                    {ghi_chu}
                                </Text>
                            </CardItem>
                        )
                    }
                    {
                        (src_tai_lieu_dinh_kem !== null) && (
                            <View>
                                <CardItem style={this.styles.CardItem}>
                                    <Text style={this.styles.textLeft}>Tài liệu họp: </Text>
                                </CardItem>
                                <CardItem style={this.styles.CardItem}>
                                    <ViewFile
                                        src_file={src_tai_lieu_dinh_kem}
                                        isUpdateViewCV={this.props.isUpdateViewCV}
                                        updateViewCV={this.props.updateViewCV}
                                        fontSize={this.props.fontSize}
                                    />
                                </CardItem>
                            </View>
                        )
                    }
                    {
                        (src_tai_lieu_ca_nhan !== null) && (
                            <View>
                                <CardItem style={this.styles.CardItem}>
                                    <Text style={this.styles.textLeft}>Tài liệu cá nhân: </Text>
                                </CardItem>
                                <CardItem style={this.styles.CardItem}>
                                    <ViewFile
                                        src_file={src_tai_lieu_ca_nhan}
                                        isUpdateViewCV={this.props.isUpdateViewCV}
                                        updateViewCV={this.props.updateViewCV}
                                        fontSize={this.props.fontSize}
                                    />
                                </CardItem>
                            </View>
                        )
                    }
                </Card>
            )
        } else {
            return <View></View>
        }
    }
}

class Table_ButPhe_LH extends Component {
    shouldComponentUpdate(nextProps, nextState) {
        if (this.props !== nextProps) {
            return true
        }
        return false
    }

    renderDataNotes = (dataNotes) => {
        let result = []
        for (let item of dataNotes) {
            result.push(
                <View key={item.ma_cuoc_hop_kc + item.ma_ctcb_hop} style={{ paddingTop: 5, paddingBottom: 10, borderColor: AppConfig.defaultLineColor, borderTopWidth: AppConfig.defaultLineWidth }}>
                    <CardItem style={this.styles.CardItem}>
                        <Text style={[this.styles.textRight]}>
                            Cán bộ tham dự: {item.can_bo_tham_du}
                        </Text>
                    </CardItem>
                    <CardItem style={this.styles.CardItem}>
                        <Text style={[this.styles.textRight]}>
                            Vai trò: {item.ten_vai_tro_hop}
                        </Text>
                    </CardItem>
                    {
                        (item.trang_thai !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={[this.styles.textRight]}>
                                    Trạng thái: {(item.trang_thai === 0) ? 'Vắng mặt' : (item.trang_thai === 1) ? 'Có mặt' : 'Đi thay'}
                                </Text>
                            </CardItem>
                        )
                    }
                    {
                        <CardItem style={this.styles.CardItem}>
                            <Text style={[this.styles.textRight]}>
                                Trạng thái xem: {(item.ngay_xem === null) ? 'Chưa xem' : 'Đã xem'}
                            </Text>
                        </CardItem>
                    }
                    {
                        (item.src_tai_lieu_ca_nhan !== null) && (
                            <View>
                                <CardItem style={this.styles.CardItem}>
                                    <Text style={this.styles.textLeft}>Tệp tin đính kèm:</Text>
                                </CardItem>
                                <CardItem style={this.styles.CardItem}>
                                    <ViewFile
                                        src_file={item.src_tai_lieu_ca_nhan}
                                        isUpdateViewCV={this.props.isUpdateViewCV}
                                        updateViewCV={this.props.updateViewCV}
                                        fontSize={this.props.fontSize}
                                    />
                                </CardItem>
                            </View>
                        )
                    }
                </View>
            )
        }
        return result
    }

    render() {
        this.styles = this.props.styles
        if (this.props.dataNotes.length > 0) {
            let tong_can_bo = this.props.dataNotes.length;
            let data = this.props.dataNotes;
            let cb_co_mat = 0;
            let cb_vang_mat = 0;
            let cb_di_thay = 0;
            data.map(cb => {
                if (cb.trang_thai == 1) {
                    cb_co_mat += 1
                } else if (cb.trang_thai == 0) {
                    cb_vang_mat += 1
                } else {
                    cb_di_thay += 1
                }
            })

            return (
                <Card noShadow style={this.styles.Card}>
                    <CardItem style={this.styles.CardItem}>
                        <Text style={[this.styles.textRight, { fontSize: this.props.fontSize.FontSizeLarge, fontWeight: "bold", }]}>
                            Danh sách cán bộ tham gia
                        </Text>
                    </CardItem>
                    <CardItem style={this.styles.CardItem}>
                        <Text style={this.styles.textRight}>
                            Tổng số cán bộ tham gia: {tong_can_bo}
                        </Text>
                    </CardItem>
                    <CardItem style={this.styles.CardItem}>
                        <Text style={this.styles.textRight}>
                            ( Có mặt: {cb_co_mat} - Đi thay: {cb_di_thay} - Vắng mặt: {cb_vang_mat} )
                        </Text>
                    </CardItem>
                    {this.renderDataNotes(this.props.dataNotes)}
                </Card>
            )
        } else {
            return (
                <View></View>
            )
        }
    }
}

class Meeting_Details extends Component {
    constructor(props) {
        super(props)
        this.state = {
            dataDetails: [],
            dataNotes: [],
        }
        this.ma_thanh_phan_hop_kc = this.props.navigation.getParam("ma_thanh_phan_hop_kc")
    }

    componentDidMount = () => {
        this.getDetails()
        this.getNotes()
        this.updateViewCV()
    }


    getDetails = async () => {
        await API.LichHop.AU_LH_CTCH(this.ma_thanh_phan_hop_kc).then((dataDetails) => {
            this.setState({ dataDetails })
        })
    }

    getNotes = async () => {
        await API.LichHop.AU_LH_DSTPH(this.ma_thanh_phan_hop_kc).then((dataNotes) => {
            this.setState({ dataNotes })
        })
    }


    updateViewCV = () => {
        this.props.navigation.state.params.updateViewCV()
    }


    render() {

        this.styles = StyleSheet.create({
            scrollContainer: {
                flex: 1,
                bottom: 0,
                backgroundColor: AppConfig.grayBackground
            },
            viewTieuDe: {
                backgroundColor: AppConfig.grayBackground,
                justifyContent: "center", alignContent: "center",
                flexWrap: "wrap",
                padding: 4,
            },
            textTieuDe: {
                fontSize: this.props.fontSize.FontSizeXLarge,
                fontWeight: "400",
                textAlign: "center"
            },

            //Table_ChiTiet_VBNB
            Card: {
                marginLeft: 6,
                marginRight: 6,
                marginTop: 0,
                padding: 10,
                borderRadius: 6,
            },
            CardItem: {
                marginTop: AppConfig.defaultPadding,
                marginBottom: AppConfig.defaultPadding,
            },
            CardItemButton: {
                marginTop: 10,
                paddingTop: 10,
                borderTopColor: AppConfig.defaultLineColor,
                borderTopWidth: AppConfig.defaultLineWidth,
                flexDirection: 'row',
                flexWrap: "wrap",
                justifyContent: "center",
                alignItems: "center"
            },
            Button: {
                margin: 3,
                height: 40,
                justifyContent: "center",
                alignItems: "center"
            },
            ButtonText: {
                fontSize: this.props.fontSize.FontSizeNorman,
                textAlign: "center"
            },
            textLeft: {
                fontSize: this.props.fontSize.FontSizeNorman,
                color: AppConfig.grayText
            },
            textRight: {
                paddingLeft: 5,
                fontSize: this.props.fontSize.FontSizeNorman,
                flexWrap: "wrap",
                flexShrink: 1,
            }
        })

        const popAction = StackActions.pop({ n: 1 })
        return (
            <Container style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
                {
                    (<HeaderWithLeft title="Chi tiết lịch họp" buttonName="arrow-back" onPressLeftButton={() => { this.props.navigation.dispatch(popAction) }} />)
                }
                <ScrollView
                    bounces={false}
                    removeClippedSubviews={true}
                    scrollEnabled={true}
                    style={this.styles.scrollContainer}
                >
                    <Table_ChiTiet_LH
                        dataDetails={this.state.dataDetails}
                        isUpdateViewCV={this.props.navigation.state.params.isUpdateViewCV}
                        updateViewCV={this.updateViewCV}
                        fontSize={this.props.fontSize}
                        styles={this.styles}
                    />
                    <Table_ButPhe_LH
                        dataNotes={this.state.dataNotes}
                        fontSize={this.props.fontSize}
                        styles={this.styles}
                    />
                </ScrollView>
            </Container>
        );
    }
}

function mapStateToProps(state) {
    return {
        fontSize: state.fontSize,
    }
}

export default connect(mapStateToProps)(Meeting_Details)