import React, { Component } from 'react';
import { View, ScrollView, StyleSheet, KeyboardAvoidingView, Platform, TextInput, Alert, Modal, FlatList, PixelRatio, TouchableOpacity, Image, Keyboard } from "react-native"
import { connect } from "react-redux"
import { Container, Toast, Item, Left, Text, Button, Icon, Spinner, Card, CardItem, ActionSheet, CheckBox } from "native-base"
import { StackActions } from "react-navigation"
import { AppConfig } from "../../AppConfig"
import HeaderWithLeftRight from "../../components/Header/HeaderWithLeftRight"
import API from "../../networks"
import { HtmlEncode, RemoveHTMLTag, resetStack, listToTree } from "../../untils/TextUntils"
import UploadFile from "../../components/UploadFile"
import TreeChuyenNCB from "../../components/treeChuyenNCB"
import serverImg from "../../assets/images/server.png"
import ModalDangThucHien from "../../components/Modal/ModalDangThucHien"

class ModalNguoiNhan extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        this.styles = this.props.styles
        let { isModalVisible, listMCB_DVCB, onPressSend, toggleModal } = this.props
        return (
            <Modal
                transparent={false}
                animationType="slide"
                visible={isModalVisible}
                supportedOrientations={["landscape", "landscape-left", "landscape-right", "portrait", "portrait-upside-down"]}
            >
                <View style={{ flex: 1, padding: 15, backgroundColor: AppConfig.grayBackground }}>
                    <View style={{ flex: 1, backgroundColor: "#FFFFFF", borderRadius: 10 }}>
                        <View style={{ position: 'absolute', zIndex: 99, height: 55, left: 0, right: 0, top: 0, justifyContent: "center", padding: 4, alignItems: "center" }}>
                            <Text style={{ fontSize: this.props.fontSize.FontSizeLarge }}>Danh sách cán bộ nhận</Text>
                        </View>
                        <FlatList
                            ref={(ref) => { this.flatListRef = ref }}
                            data={listMCB_DVCB}
                            keyExtractor={(item, index) => "key" + index}
                            style={{ position: 'absolute', left: 0, right: 0, top: 55, bottom: 55 }}
                            initialNumToRender={50}
                            renderItem={({ item, index }) => {
                                return (
                                    <Item key={item.ma_ctcb_kc} style={{ padding: 12, borderBottomWidth: AppConfig.defaultLineWidth, borderColor: AppConfig.defaultLineColor }}>
                                        <Left>
                                            <Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>{item.ho_va_ten_can_bo}</Text>
                                        </Left>
                                    </Item>
                                )
                            }}
                            ListEmptyComponent={() => {
                                return (
                                    <View style={{ alignItems: "center", marginTop: 10, }}>
                                        <Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>Không có dữ liệu</Text>
                                    </View>
                                )
                            }}
                            removeClippedSubviews={true}
                        />
                        {
                            (this.props.isSending) ? (
                                <View style={{ position: 'absolute', zIndex: 99, height: 55, left: 0, right: 0, bottom: 0, flexDirection: "row", justifyContent: "center", paddingTop: 10, alignItems: "center" }}>
                                    <Spinner />
                                    <Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}> Đang thực hiện</Text>
                                </View>
                            ) : (
                                    <View style={{ position: 'absolute', zIndex: 99, height: 55, left: 0, right: 0, bottom: 0, flexDirection: "row", justifyContent: "space-evenly", paddingTop: 10, alignItems: "center" }}>
                                        <Button small bordered rounded iconLeft onPress={onPressSend}><Icon name="done" type="MaterialIcons"></Icon><Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>Gửi</Text></Button>
                                        <Button small danger bordered rounded iconLeft onPress={toggleModal}><Icon name="close" type="MaterialIcons"></Icon><Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>Đóng</Text></Button>
                                    </View>
                                )
                        }
                    </View>
                </View>
            </Modal>
        )
    }
}

class XYKTT_Gui extends Component {
    constructor(props) {
        super(props)
        this.state = {
            newValue: "",
            height: 40,
            isModalVisible: false,
            ten_nhom_nguoi_nhan: "Chọn nhóm cán bộ nhận",
            ma_nhom_nguoi_nhan: 0,
            ma_loai_ttdh_kc: -1,
            tieuDe: "",
            noiDung: "",
            dataFile: [],
            checkEmail: false,
            isSending: false,
            dataTreeNCB: [],
            listNhomCanBo: [],
            isLoading: false,
            checkSms: false,
            loading: false,
        }
        this.titleHeader = "Gửi Xin ý kiến"
        this.noiDungChuyen = ""

        //Tham số mặc định gửi ttdh
        this.ma_ttdh_kc = 0
        this.trang_thai_ttdh = 1
        this.tra_loi_cho_ttdh = 0
        this.chuyen_tiep_tu_ttdh = 0
        this.ma_ttdh_goc = 0
        this.src_file_xin_y_kien = ""
        this.listNguoiNhan = []
        this.listMCB_DVCB = []
    }

    componentDidMount = () => {
        this.getDanhSachNhomCanBo()
    }

    updateSize = (height) => {
        this.setState({ height: height })
        setTimeout(() => {
            this.ScrollView.scrollToEnd()
        }, 10);
    }

    getDSNCB = async (ma_nhom_nguoi_nhan, ten_nhom_nguoi_nhan) => {
        await API.XinYKienThuongTruc.AU_XYKTT_YKTT_DSCBTN(ma_nhom_nguoi_nhan).then((res) => {
            let dataTreeNCB = []
            let parent = {
                "id": `N${ma_nhom_nguoi_nhan}`,
                "parent": "#",
                "text": ten_nhom_nguoi_nhan,
                "icon": "/images/user-group.png",
                "li_attr": {
                    "data_id": ma_nhom_nguoi_nhan.toString(),
                    "data_ten_can_bo": ten_nhom_nguoi_nhan,
                    "data_chuc_vu": null,
                    "data_phone": null,
                    "data_email": null
                },
                "state": {
                    "loaded": true,
                    "opened": false,
                    "selected": false,
                    "disabled": false
                }
            }
            dataTreeNCB.push(parent)
            res.forEach(element => {
                let data = {
                    "id": `${ma_nhom_nguoi_nhan}CB${element.ma_ctcb_kc}`,
                    "parent": `N${ma_nhom_nguoi_nhan}`,
                    "text": element.ho_va_ten_can_bo,
                    "icon": "/images/user_male.png",
                    "li_attr": {
                        "data_id": element.ma_ctcb_kc.toString(),
                        "data_ten_can_bo": element.ho_va_ten_can_bo,
                        "data_chuc_vu": "",
                        "data_phone": element.di_dong_can_bo,
                        "data_email": null
                    },
                    "state": {
                        "loaded": true,
                        "opened": false,
                        "selected": false,
                        "disabled": false
                    }
                }
                dataTreeNCB.push(data)
            });
            let tree = listToTree(dataTreeNCB, {
                idKey: "id",
                parentKey: "parent"
            })
            this.setState({ dataTreeNCB: tree })
        })
    }

    onPressBack = () => {
        const popAction = StackActions.pop({ n: 1 })
        this.props.navigation.dispatch(popAction)
        return false
    }

    toggleModal = () => {
        if (!this.state.isModalVisible) {
            if (this.listNguoiNhan.length > 0) {
                this.setState({ isModalVisible: true })
            } else {
                Toast.show({ text: "Lấy thông tin cán bộ nhận thất bại!", type: "warning", buttonText: "Đóng", position: "top", duration: 2000 })
            }
        } else {
            this.setState({ isModalVisible: false })
        }
    }

    onPressSend = () => {
        if (this.state.tieuDe !== "" && this.state.noiDung !== "") {
            this.setState({ loading: true })
            let listData = this.treeChuyenNCB.getDataCanBo()
            let listMCB_DVCB = listData.map((element) => {
                return element.ma
            })
            const src_file_xin_y_kien = this.UploadFile.getListFileUpload()
            // console.log('src_file_xin_y_kien', src_file_xin_y_kien)
            if (this.state.tieuDe.length > 2000) {
                this.setState({ loading: false }, () => {
                    Toast.show({ text: "Tiêu đề vượt quá độ dài cho phép 2000 ký tự", type: "warning", buttonText: "Đóng", position: "top", duration: 2000 })
                })
            } else if (this.state.noiDung.length > 2000) {
                this.setState({ loading: false }, () => {
                    Toast.show({ text: "Nội dung vượt quá độ dài cho phép 2000 ký tự", type: "warning", buttonText: "Đóng", position: "top", duration: 2000 })
                })
            } else if (src_file_xin_y_kien == "") {
                this.setState({ loading: false }, () => {
                    Toast.show({ text: "Vui lòng chọn file đính kèm!", type: "warning", buttonText: "Đóng", position: "top", duration: 2000 })
                })
            } else if (listData.length === 0) {
                this.setState({ loading: false }, () => {
                    Toast.show({ text: "Vui lòng chọn nhóm cán bộ nhận!", type: "warning", buttonText: "Đóng", position: "top", duration: 2000 })
                })
            } else {
                const chuoi_ma_ctcb_nhan = listMCB_DVCB.join(";").toString()
                const ma_nhom_nguoi_nhan = this.state.ma_nhom_nguoi_nhan
                const ma_ctcb_tao = global.ma_ctcb_kc
                const tieu_de = this.state.tieuDe
                let noi_dung = this.state.noiDung
                noi_dung = HtmlEncode(noi_dung)
                const sms = this.state.checkSms ? 1 : 0
                API.XinYKienThuongTruc.AU_XYKTT_YKTT_CREATE(chuoi_ma_ctcb_nhan, ma_ctcb_tao, ma_nhom_nguoi_nhan, noi_dung, src_file_xin_y_kien, tieu_de, 0).then(response => {
                    if (response !== "") {
                        API.Login.AU_DSNVCB(global.ma_ctcb_kc, this.props.dispatch)
                        if (sms === 1) {
                            let arrSDT = []
                            for (let item of listData) {
                                if (item.di_dong_can_bo) {
                                    arrSDT.push(item.di_dong_can_bo)
                                }
                            }
                            if (arrSDT.length > 0) {
                                let chuoi_di_dong_nhan = arrSDT.join(",")
                                API.NhanTinSMS.AU_SMS_GTN(tieu_de, chuoi_di_dong_nhan)
                            }
                        }
                        API.Login.AU_PUSH_FCM_APP(listMCB_DVCB.join(","), "xinykiendanhan", tieu_de, response, 0, 0, 0)
                        setTimeout(() => {
                            this.props.navigation.dispatch(resetStack("XYKTT"))
                        }, 100);
                    } else {
                        Alert.alert("Thông báo", "Xảy ra lỗi trong quá trình gửi!")
                        this.setState({ loading: false })
                    }
                }).finally(() => this.setState({ loading: false }))
            }
        } else {
            Toast.show({ text: "Vui lòng nhập đầy đủ thông tin!", type: "warning", buttonText: "Đóng", position: "top", duration: 2000 })
            if (this.state.tieuDe === "") {
                this.txtTieuDe.focus()
                return;
            }
            if (this.state.noiDung === "") {
                this.txtNoiDung.focus()
                return;
            }
        }
    }

    getDanhSachNhomCanBo = () => {
        // console.log('getDanhSachNhomCanBo')
        API.XinYKienThuongTruc.AU_XYKTT_DSNTDV(global.ma_don_vi_quan_tri).then((listNhomCanBo) => {
            this.setState({ listNhomCanBo }, () => {
                if (this.state.isLoading) {    
                    this.setState({ isLoading: false }, () => {
                        this.showDonVi()
                    })
                } else {
                    if (listNhomCanBo.length > 0) {
                        this.setState({
                            ten_nhom_nguoi_nhan: listNhomCanBo[0].ten_nhom_nguoi_nhan,
                            ma_nhom_nguoi_nhan: listNhomCanBo[0].ma_nhom_nguoi_nhan
                        }, () => {
                            this.getDSNCB(listNhomCanBo[0].ma_nhom_nguoi_nhan, listNhomCanBo[0].ten_nhom_nguoi_nhan)
                        })
                    }
                }
            })
        })
    }

    showDonVi = () => {
        // console.log('showDonVi', this.state.listNhomCanBo.length)
        if (this.state.listNhomCanBo.length === 0) {
            this.setState({ isLoading: true }, () => {
                this.getDanhSachNhomCanBo()
            })
            return
        }
        let { listNhomCanBo } = this.state
        ActionSheet.show(
            {
                options: listNhomCanBo.map(obj => {
                    return obj.ten_nhom_nguoi_nhan
                }),
                title: "Chọn nhóm cán bộ nhận"
            },
            buttonIndex => {
                if (buttonIndex !== undefined) {
                    this.treeChuyenNCB.removeoldValueArrOne()
                    this.setState({
                        dataTreeNCB: [],
                        ten_nhom_nguoi_nhan: listNhomCanBo[buttonIndex].ten_nhom_nguoi_nhan,
                        ma_nhom_nguoi_nhan: listNhomCanBo[buttonIndex].ma_nhom_nguoi_nhan
                    }, () => {
                        this.getDSNCB(listNhomCanBo[buttonIndex].ma_nhom_nguoi_nhan, listNhomCanBo[buttonIndex].ten_nhom_nguoi_nhan)
                    })
                }
            }
        )
    }

    onCheckSms = () => {
        this.setState({ checkSms: !this.state.checkSms })
    }

    render() {
        this.styles = StyleSheet.create({
            content: {
                flex: 1,
                marginTop: 20,
                //paddingBottom: 20,
                paddingLeft: 20,
                paddingRight: 20
            },
            viewItem: {
                paddingTop: 10,
                paddingBottom: 10
            },
            viewItem1: {
                borderColor: AppConfig.defaultLineColor,
                borderBottomWidth: AppConfig.defaultLineWidth,
            },
            labelTieuDe: {
                fontFamily: AppConfig.fontFamily,
                color: AppConfig.blueBackground,
                fontSize: this.props.fontSize.FontSizeSmall
            },
            txtLoaiThongDiep: {
                fontFamily: AppConfig.fontFamily,
                color: "#000000",
                fontSize: this.props.fontSize.FontSizeNorman,
                borderColor: AppConfig.defaultLineColor,
                borderBottomWidth: AppConfig.defaultLineWidth,
                paddingTop: 10,
                paddingBottom: 10
            },
            txtTieuDe: {
                height: 40,
                fontFamily: AppConfig.fontFamily,
                color: "#000000",
                fontSize: this.props.fontSize.FontSizeNorman / PixelRatio.getFontScale(),
                borderColor: AppConfig.defaultLineColor,
                borderBottomWidth: AppConfig.defaultLineWidth,
            },
            txtNoiDung: {
                minHeight: 40,
                fontFamily: AppConfig.fontFamily,
                color: "#000000",
                fontSize: this.props.fontSize.FontSizeNorman,
                borderColor: AppConfig.defaultLineColor,
                borderBottomWidth: AppConfig.defaultLineWidth,
            }
        })

        return (
            <Container style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
                {
                    (this.state.loading) && (
                        <View
                            style={{
                            position: "absolute",
                            flex: 1,
                            width: "100%",
                            height: "100%",
                            backgroundColor: "rgba(128, 129, 130,0.5)",
                            zIndex: 9999,
                            justifyContent: "center",
                            alignItems: "center",
                            }}
                        >
                            <Image
                            source={require("../../assets/images/dualball.gif")}
                            style={{ width: 60, height: 60, borderRadius: 10 }}
                            />
                        </View>
                    )
                }
                <HeaderWithLeftRight
                    title={this.titleHeader}
                    buttonLeftName="arrow-back"
                    buttonRightName="send"
                    onPressLeftButton={_ => this.onPressBack()}
                    onPressRightButton={_ => this.onPressSend()}
                />
                <View style={this.styles.content}>
                    {
                        (this.state.isModalVisible) && (
                            <ModalNguoiNhan
                                listMCB_DVCB={this.listNguoiNhan}
                                isModalVisible={this.state.isModalVisible}
                                toggleModal={this.toggleModal}
                                onPressSend={this.onPressSendReply}
                                isSending={this.state.isSending}
                                fontSize={this.props.fontSize}
                                styles={this.styles}
                            />
                        )
                    }
                    <KeyboardAvoidingView enabled behavior={Platform.OS === "ios" ? "padding" : null} keyboardVerticalOffset={104}>
                        <ScrollView ref={ref => { this.ScrollView = ref }} bounces={false}>
                            <View style={this.styles.viewItem}>
                                <Text style={this.styles.labelTieuDe}>Tiêu đề <Text style={{ color: 'red', justifyContent: 'center'}}>(*)</Text></Text>
                                <TextInput
                                    ref={input => { this.txtTieuDe = input }}
                                    multiline={false}
                                    style={this.styles.txtTieuDe}
                                    autoCapitalize="none"
                                    returnKeyType={"done"}
                                    value={this.state.tieuDe}
                                    onChangeText={(text) => this.setState({ tieuDe: text })}
                                    onSubmitEditing={() => { this.txtNoiDung.focus() }}
                                />
                            </View>
                            <View style={this.styles.viewItem}>
                                <Text style={this.styles.labelTieuDe}>Nội dung <Text style={{ color: 'red', justifyContent: 'center'}}>(*)</Text></Text>
                                <TextInput
                                    ref={input => { this.txtNoiDung = input }}
                                    autoCapitalize="none"
                                    returnKeyType={"done"}
                                    style={this.styles.txtNoiDung}
                                    value={this.state.noiDung}
                                    onChangeText={(text) => this.setState({ noiDung: text })}
                                    multiline={true}
                                    numberOfLinesnumber={4}
                                    onSubmitEditing={() => Keyboard.dismiss()}
                                    onContentSizeChange={(e) => this.updateSize(e.nativeEvent.contentSize.height)}
                                />
                            </View>
                            <View style={this.styles.viewItem}>
                                <UploadFile
                                    ref={(upload) => { this.UploadFile = upload }}
                                    chuc_nang="xinykienthuongtruc"
                                    request={true}
                                    sourceFiles={this.src_file_xin_y_kien && !this.isReply ? this.src_file_xin_y_kien.split(":") : []}
                                    navigation={this.props.navigation}
                                    fontSize={this.props.fontSize}
                                />
                            </View>
                            <View style={[this.styles.viewItem, { flexDirection: "row", }]}>
                                <TouchableOpacity onPress={this.onCheckSms} style={[{ flexDirection: "row", alignItems: 'center', justifyContent: "flex-start" }]}>
                                    <CheckBox checked={this.state.checkSms} color={AppConfig.blueBackground}></CheckBox>
                                    <Text style={this.styles.labelTieuDe}>    Sms      </Text>
                                </TouchableOpacity>
                            </View>
                            <Card noShadow style={this.styles.Card}>
                                <CardItem style={this.styles.CardItem}>
                                    <Text style={[this.styles.textRight, { fontSize: this.props.fontSize.FontSizeLarge, fontWeight: "bold", }]}>
                                        Chọn cán bộ xin ý kiến <Text style={{ color: 'red', justifyContent: 'center'}}>(*)</Text>
                                    </Text>
                                </CardItem>
                            </Card>
                            <ModalDangThucHien
                                isVisible={this.state.isLoading}
                                title={"Đang lấy danh sách nhóm cán bộ"}
                            />
                            <Card noShadow style={{ flex: 1, borderWidth: 1}}>
                                <TouchableOpacity onPress={this.showDonVi} 
                                    style={{
                                        flex: 1,
                                        flexDirection: "row",
                                        alignItems: "center",
                                        justifyContent: "center",
                                        height: 45,
                                        padding: 10
                                    }}>
                                    <Image source={serverImg} tintColor={AppConfig.defaultLineColor} style={{ position: 'absolute', zIndex: 99, width: 23, height: 23, left: 32, top: 11, left: 10 }} />
                                    <View style={{ paddingLeft: 30 }}>
                                        <Text style={{color: 'black', backgroundColor: 'transparent', fontSize: this.props.fontSize.FontSizeNorman}}>{this.state.ten_nhom_nguoi_nhan}</Text>
                                    </View>
                                </TouchableOpacity>
                            </Card>
                            <Card noShadow style={{ flex: 1, borderWidth: 1 }}>
                                <TreeChuyenNCB
                                    ref={treeNCB => { this.treeChuyenNCB = treeNCB }}
                                    title="Họ tên"
                                    firstChkTitle="Chọn"
                                    secondChkTitle="PH"
                                    threeChkTitle="XLC"
                                    numberSelection={1}
                                    data={this.state.dataTreeNCB}
                                    primaryCheck={1}
                                    onItemPress={(ma, once) => { console.log("press item: " + ma) }}
                                    fontSize={this.props.fontSize}
                                />
                            </Card>
                        </ScrollView>
                    </KeyboardAvoidingView>
                </View>
            </Container>
        );
    }
}

function mapStateToProps(state) {
    return {
        cb_dsctcb: state.cb_dsctcb.data,
        fontSize: state.fontSize,
    }
}

export default connect(mapStateToProps)(XYKTT_Gui)