import React, { Component, PureComponent } from 'react';
import { View, StyleSheet, FlatList, Alert, TouchableOpacity, Platform, KeyboardAvoidingView } from "react-native"
import { Container, Text, CardItem, Spinner, Toast, CheckBox } from "native-base"
import { connect } from "react-redux"
import { DrawerActions } from "react-navigation"
import { Menu, MenuOptions, MenuOption, MenuTrigger } from 'react-native-popup-menu'
import Swipeable from 'react-native-gesture-handler/Swipeable'
import Icon from "react-native-vector-icons/FontAwesome5"
import HeaderWithLeftRight from "../../components/Header/HeaderWithLeftRight"
import FooterTab from "../../components/Footer/FooterTab"
import SearchHeader from "../../components/SearchHeader"
import MyTab from "../../components/Tab"
import API from "../../networks"
import ModalListFile from "../../components/ModalListFile"
import ModalWebView from "../../components/ModalWebView"
import { AppConfig } from "../../AppConfig"
import { CheckSession } from "../../untils/NetInfo"
import { getFileChiTietTTDH, RemoveNoiDungTTDH, ConvertDateTime, ConvertDateTimeDetail, ToastSuccess, getNVNew } from "../../untils/TextUntils"
import { downloadFileToView } from "../../untils/DownloadFile"
import XYKTT_Details_iPad from "./XYKTT_Details_iPad"

class XYKTTTab_Item extends PureComponent {
    constructor(props) {
        super(props)
        this.state = {
            ma_xin_y_kien_kc: 0,
            isModalVisible: false,
            isBold: this.props.item.trang_thai_y_kien_can_bo === 0 ? true : false
        }
    }

    onPressItem = () => {
        if (Platform.OS === "ios" && Platform.isPad) {
            if (this.state.isBold) {
                this.setState({ isBold: false })
            }
            this.props.onPress()
        } else {
            if (this.state.isBold) {
                this.setState({ isBold: false })
            }
            this.props.navigation.navigate("XYKTT_Details", {
                ma_xin_y_kien_kc: this.props.item.ma_xin_y_kien_kc,
            })
        }
    }

    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible })
    }

    onViewFile = (data) => {
        let item = data.split("|")
        let urlFile = item[0]
        let fileName = item[1]
        let extFile = item[2]
        let path = item[4]
        if (urlFile !== "") {
            if (Platform.OS === "ios" && (extFile === "doc" || extFile === "docx")) {
                downloadFileToView(fileName, path)
            } else {
                if (global.US_Xem_Pdf_iOS === "Offline") {
                    downloadFileToView(fileName, path)
                    return null;
                }
                this.urlFile = urlFile
                this.path = path
                this.fileName = fileName
                this.toggleModal()
            }
        } else {
            downloadFileToView(fileName, path)
        }
    }

    updateViewCV = async () => {

    }

    rightAction = () => {
        // console.log('ma_xin_y_kien_kc', this.state.ma_xin_y_kien_kc)
        return (
            <TouchableOpacity style={{width: 80}} onPress={() => this.props.onDelete(this.state.ma_xin_y_kien_kc, 1, false)}>
                <View style={{flex: 1, backgroundColor: 'red', justifyContent: 'center', alignItems: 'center'}}>
                    <Text>Xóa</Text>
                </View>
            </TouchableOpacity>
        )
    }

    render() {
        this.styles = this.props.styles
        const { item, activeTab } = this.props
        let dataFile = item.src_file_xin_y_kien !== null && item.src_file_xin_y_kien !== "null" ? getFileChiTietTTDH(item.src_file_xin_y_kien.split(":")) : []
        let styleTemp = this.styles.textNoBold
        if (this.state.isBold) {
            styleTemp = this.styles.textBold
        }
        const ngay = item.ngay_tao
        let noiDung = RemoveNoiDungTTDH(item.noi_dung)
        noiDung = noiDung ? noiDung.replace("\n", " ") : "Không có nội dung"
        const minHeight = (activeTab === 0 || dataFile.length > 0) ? 85 : 70

        let borderRightWidth = 0.8
        let borderRightColor = "lightgray"

        if (Platform.OS === "ios" && Platform.isPad) {
            return this.props.activeTab == 1 ? (
                <Swipeable
                    onSwipeableOpen={() => {this.setState({ ma_xin_y_kien_kc: item.ma_xin_y_kien_kc})}}
                    renderRightActions={this.rightAction}
                >
                    <CardItem style={[this.styles.CardItem, { borderRightWidth, borderRightColor, paddingTop: 6, paddingBottom: 6, }]} key={item.ma_xin_y_kien_kc}>
                        <TouchableOpacity style={{ justifyContent: 'center', flex: 1 }} onPress={this.onPressItem}>
                            {
                                (activeTab === 0) && (
                                    <Text numberOfLines={2} ellipsizeMode="tail" style={[styleTemp]}>{item.ten_nguoi_gui} </Text>
                                )
                            }
                            <Text numberOfLines={2} ellipsizeMode="tail" style={[styleTemp]}>Tiêu đề: {item.tieu_de.trim()} </Text>
                            <Text numberOfLines={1} ellipsizeMode="tail" style={[styleTemp, { color: "#808080" }]}>Nội dung: {noiDung} </Text>
                            <Text numberOfLines={1} ellipsizeMode="tail" style={[styleTemp]}>Ngày: {ConvertDateTimeDetail(ngay)}  </Text>
                        </TouchableOpacity>
                    </CardItem>
                </Swipeable>
            ) : (
                <CardItem style={[this.styles.CardItem, { borderRightWidth, borderRightColor, paddingTop: 6, paddingBottom: 6, }]} key={item.ma_xin_y_kien_kc}>
                        <TouchableOpacity style={{ justifyContent: 'center', flex: 1 }} onPress={this.onPressItem}>
                            {
                                (activeTab === 0) && (
                                    <Text numberOfLines={2} ellipsizeMode="tail" style={[styleTemp]}>{item.ten_nguoi_gui} </Text>
                                )
                            }
                            <Text numberOfLines={2} ellipsizeMode="tail" style={[styleTemp]}>Tiêu đề: {item.tieu_de.trim()} </Text>
                            <Text numberOfLines={1} ellipsizeMode="tail" style={[styleTemp, { color: "#808080" }]}>Nội dung: {noiDung} </Text>
                            <Text numberOfLines={1} ellipsizeMode="tail" style={[styleTemp]}>Ngày: {ConvertDateTimeDetail(ngay)}  </Text>
                        </TouchableOpacity>
                    </CardItem>
            )
        } else {
            return (
                <CardItem key={item.ma_xin_y_kien_kc} style={[this.styles.CardItem]}>
                    <TouchableOpacity style={{ justifyContent: 'center', flex: 1, left: 0, marginRight: 100, minHeight }} onPress={this.onPressItem}>
                        {
                            (activeTab === 0) && (
                                <Text numberOfLines={1} ellipsizeMode="tail" style={[styleTemp]}>{item.ten_nguoi_gui} </Text>
                            )
                        }
                        <Text numberOfLines={1} ellipsizeMode="tail" style={[styleTemp]}>{item.tieu_de.trim()} </Text>
                        <Text numberOfLines={1} ellipsizeMode="tail" style={[styleTemp, { color: "#808080" }]}>{noiDung} </Text>
                    </TouchableOpacity>
                    <View style={{ width: 100, top: 0, right: 0, bottom: 0, position: "absolute", alignItems: 'center', justifyContent: "center" }}>
                        <Text style={this.styles.txtNgayNhan}>{ConvertDateTime(ngay)} </Text>
                        {
                            (dataFile.length > 0) && (
                                <Icon name="paperclip" style={[this.styles.menuTriggerIcon, { fontSize: this.props.fontSize.FontSizeNorman, }]}></Icon>
                            )
                        }
                        <Menu>
                            <MenuTrigger style={this.styles.menuTrigger}>
                                <Icon name="ellipsis-v" style={this.styles.menuTriggerIcon}></Icon>
                            </MenuTrigger>
                            <MenuOptions optionsContainerStyle={this.styles.menuOptionsContainer}>
                                {
                                    (dataFile.length > 1 || (dataFile.length === 1 && dataFile[0] !== "")) && (
                                        <MenuOption onSelect={() => this.onViewFile(dataFile[0])} >
                                            <Text style={this.styles.menuOptionsText}>Xem tệp tin</Text>
                                        </MenuOption>
                                    )
                                }
                                {
                                    this.props.activeTab == 1 && 
                                    (
                                        <MenuOption onSelect={() => this.props.onDelete(item.ma_xin_y_kien_kc, activeTab, false) } >
                                            <Text style={this.styles.menuOptionsTextDelete}>Xóa</Text>
                                        </MenuOption>
                                    )
                                }
                            </MenuOptions>
                        </Menu>
                    </View>
                    {
                        (dataFile.length === 1) && (
                            <ModalWebView
                                isModalVisible={this.state.isModalVisible}
                                title={dataFile[0].split("|")[1]}
                                url={dataFile[0].split("|")[0]}
                                path={dataFile[0].split("|")[4]}
                                toggleModal={() => this.toggleModal()}
                                isUpdateViewCV={this.state.isBold}
                                updateViewCV={() => this.updateViewCV()}
                            />
                        )
                    }
                    {
                        (dataFile.length > 1) && (
                            <ModalListFile
                                isModalVisible={this.state.isModalVisible}
                                dataFile={dataFile}
                                toggleModal={() => this.toggleModal()}
                                isUpdateViewCV={this.state.isBold}
                                updateViewCV={() => this.updateViewCV()}
                            />
                        )
                    }
                </CardItem>
            )
        }
    }
}

class XYKTTScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            loading: true,
            refreshing: false,
            dataSource: [],
            dataSearch: [],
            keyWord: "",
            ma_xin_y_kien_kc: 0,
            cur_item: {},
            trang_thai_y_kien_can_bo: -2,
        }
        this.page = 1
        this.activeTab = 0
        this.responseLength = 20
        this.onEndReachedCalledDuringMomentum = false
    }

    componentDidMount() {
        this.didBlurSubscription = this.props.navigation.addListener('willFocus', payload => { CheckSession(this.props.navigation) })
        this.activeTab = this.props.navigation.getParam("activeTab", 0)
        this.getListByTabIndex(this.activeTab)
        API.Login.AU_DSNVCB(global.ma_ctcb_kc, this.props.dispatch)
    }

    componentWillUnmount = () => {
        this.didBlurSubscription.remove()
    };

    shouldComponentUpdate = (nextProps, nextState) => {
        return !!(this.state.loading !== nextState.loading ||
            this.state.refreshing !== nextState.refreshing ||
            this.state.dataSource !== nextState.dataSource ||
            this.state.ma_xin_y_kien_kc !== nextState.ma_xin_y_kien_kc ||
            this.state.cur_item !== nextState.cur_item ||
            this.state.trang_thai_y_kien_can_bo !== nextState.trang_thai_y_kien_can_bo ||
            this.state.keyWord !== nextState.keyWord ||
            this.props.cb_dsnvcb !== nextProps.cb_dsnvcb);
    };

    getListByTabIndex = () => {
        let ma_can_bo = global.ma_can_bo
        let tabIndex = this.activeTab
        let page = this.page
        let size = 10
        let keyWord = this.searchHeader.getKeyWord()
        
        if (tabIndex === 0) {
            API.XinYKienThuongTruc.AU_XYKTT_DA_NHAN(ma_can_bo, page, size, keyWord, this.state.trang_thai_y_kien_can_bo).then(response => {
                if (tabIndex === this.activeTab) {
                    this.setState({ dataSource: page === 1 ? response : [...this.state.dataSource, ...response] }, () => {
                        this.setState({ loading: false, refreshing: false })
                    })
                    this.responseLength = response.length
                    if (this.responseLength === 0) {
                        this.loadMore()
                    }
                }
            })
        } else {
            API.XinYKienThuongTruc.AU_XYKTT_DA_GUI(ma_can_bo, page, size, keyWord).then(response => {
                if (tabIndex === this.activeTab) {
                    this.setState({ dataSource: [...this.state.dataSource, ...response] }, () => {
                        this.setState({ loading: false, refreshing: false })
                    })
                    this.responseLength = response.length
                    if (this.responseLength === 0) {
                        this.loadMore()
                    }
                }
            })
        }
    }

    onChangeTab = (index) => {
        if (!this.state.loading) {
            this.page = 1
            this.activeTab = index
            this.searchHeader.setKeyWord("", () => {
                this.setState({ dataSource: [], loading: true })
                this.getListByTabIndex()
            })
        }
    }

    onRefresh = () => {
        if (!this.state.loading) {
            this.page = 1
            this.setState({ dataSource: [], refreshing: true, loading: true })
            this.getListByTabIndex()
        }
    }

    loadMore = () => {
        // console.log('loadmore', this.state.loading, this.onEndReachedCalledDuringMomentum, this.page + 1)
        if (!this.state.loading && !this.onEndReachedCalledDuringMomentum) {
            this.page = this.page + 1
            this.onEndReachedCalledDuringMomentum = true
            this.getListByTabIndex()
        }
    }

    onSearchData = () => {
        // console.log('onSearchData', this.state.loading)
        if (!this.state.loading) {
            this.page = 1
            this.onEndReachedCalledDuringMomentum = true
            this.setState({ dataSource: [], loading: true })
            this.getListByTabIndex()
        }
    }

    onDelete = (ma_xin_y_kien_kc, activeTab, flagNavigate) => {
        // console.log('onDelete', ma_xin_y_kien_kc)
        Alert.alert("Xác nhận", "Xóa phiếu xin ý kiến này?",
            [
                { text: "Không", style: "cancel" },
                {
                    text: "Đồng ý", onPress: () => {
                        API.XinYKienThuongTruc.AU_XYKTT_YKTT_DELETE(ma_xin_y_kien_kc, -1).then((response) => {
                            if (response !== "") {
                                let dataSource = this.state.dataSource.filter(el => el.ma_xin_y_kien_kc !== ma_xin_y_kien_kc)
                                if (Platform.OS === "ios" && Platform.isPad) {
                                    this.setState({ dataSource, ma_xin_y_kien_kc: 0, cur_item: {} }, () => ToastSuccess("Xóa thành công!"))
                                } else {
                                    this.setState({ dataSource }, () => ToastSuccess("Xóa thành công!"))
                                }
                            } else {
                                Toast.show({ text: "Xảy ra lỗi. Vui lòng thực hiện lại sau!", type: "danger", buttonText: "Đóng", position: "top", duration: 2000 })
                            }
                        })
                    }
                }
            ],
            { cancelable: false }
        )
    }

    renderXYKTT_Details_iPad = () => {
        let { ma_xin_y_kien_kc } = this.state
        if (ma_xin_y_kien_kc !== 0) {
            // console.log('ma_xin_y_kien_kc', ma_xin_y_kien_kc, this.state.cur_item, this.activeTab)
            return (
                <XYKTT_Details_iPad
                    navigation={this.props.navigation}
                    ma_xin_y_kien_kc={ma_xin_y_kien_kc}
                    item={this.state.cur_item}
                    loaiGuiNhan={this.activeTab === 0 ? "Nhận" : "gửi"}
                />
            )
        } else {
            return null
        }
    }

    onChangTrangThai = (trangThai) => {
        this.page = 1
        this.onEndReachedCalledDuringMomentum = false
        this.setState({trang_thai_y_kien_can_bo: trangThai, dataSource: [], loading: true }, () => this.getListByTabIndex())
    }

    render() {

        this.styles = StyleSheet.create({
            CardItem: {
                flex: 1,
                borderTopWidth: 0,
                borderBottomWidth: 0.55,
                borderLeftWidth: 0,
                borderRightWidth: 0,
                borderBottomColor: "lightgray",
                paddingLeft: 8,
                alignItems: 'center',
                justifyContent: "center",
                marginBottom: 1,
            },
            colTieuDe: {
                width: "70%",
                justifyContent: "center"
            },
            textBold: {
                color: "#000000",
                fontSize: this.props.fontSize.FontSizeNorman,
                fontWeight: "bold",
                paddingBottom: AppConfig.defaultPadding
            },
            textNoBold: {
                color: "#000000",
                fontSize: this.props.fontSize.FontSizeNorman,
                paddingBottom: AppConfig.defaultPadding
            },
            txtNgayNhan: {
                width: 100,
                color: "#808080",
                fontSize: this.props.fontSize.FontSizeSmall,
                textAlign: "center",
                paddingBottom: AppConfig.defaultPadding
            },
            txtNoiDung: {
                color: "#808080",
                fontSize: this.props.fontSize.FontSizeSmall,
            },
            colNgayGui: {
                width: "30%",
                justifyContent: "center",
                alignItems: "flex-end"
            },
            menuTrigger: {
                width: 40,
                height: 25,
                justifyContent: "center",
                alignItems: 'center'
            },
            menuTriggerIcon: {
                fontSize: this.props.fontSize.FontSizeXXXLarge,
                color: "#B0BEC5"
            },
            menuOptionsContainer: {
                width: 200
            },
            menuOptions: {
                borderBottomColor: AppConfig.defaultLineColor,
                borderBottomWidth: 0.5
            },
            menuOptionsText: {
                color: AppConfig.headerBackgroundColor,
                fontSize: this.props.fontSize.FontSizeNorman,
                paddingTop: AppConfig.defaultPadding,
                paddingBottom: AppConfig.defaultPadding
            },
            menuOptionsTextDelete: {
                color: "red",
                fontSize: this.props.fontSize.FontSizeNorman,
                paddingTop: AppConfig.defaultPadding,
                paddingBottom: AppConfig.defaultPadding
            }
        })

        let nv_nhan = 0
        try {
            let cb_dsnvcb = this.props.cb_dsnvcb
            nv_nhan = getNVNew("khac", "xin-y-kien/danh-sach-xin-y-kien-chua-xu-ly?page=1", cb_dsnvcb)
        } catch (error) {
            console.log("nv_xyktt_danhan error")
        }

        if (Platform.OS === "ios" && Platform.isPad) {
            return (
                <Container style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
                    <HeaderWithLeftRight
                        title="Xin ý kiến thường trực"
                        buttonLeftName="menu"
                        onPressLeftButton={() => { this.props.navigation.dispatch(DrawerActions.toggleDrawer()) }}
                        buttonRightName="note-add"
                        onPressRightButton={() => { this.props.navigation.navigate("XYKTT_Gui") }}
                    />
                    {/* <View style={{ flex: 1 }}> */}
                    <KeyboardAvoidingView style={{ flex: 1 }} enabled behavior={Platform.OS === "ios" ? "padding" : null}>
                        <MyTab
                            values={["Đã nhận", "Đã gửi"]}
                            badges={[nv_nhan, 0]}
                            activeTab={this.activeTab}
                            onChangeTab={(index) => { this.onChangeTab(index) }}
                            fontSize={this.props.fontSize}
                        />
                        <SearchHeader
                            ref={(ref) => { this.searchHeader = ref }}
                            onSearchData={this.onSearchData}
                            fontSize={this.props.fontSize}
                        />
                        {
                            this.activeTab == 0 && 
                            (
                                <View style={{ flexDirection: "row", justifyContent: "space-evenly", padding: 10, backgroundColor: "#F5F5F5" }}>
                                    <View style={{ flexDirection: "row" }}>
                                        <CheckBox onPress={() => this.onChangTrangThai(-2)} checked={this.state.trang_thai_y_kien_can_bo === -2} color={AppConfig.blueBackground}></CheckBox>
                                        <Text style={[{ marginLeft: 20 }]}>Tất cả</Text>
                                    </View>
                                    <View style={{ flexDirection: "row" }}>
                                        <CheckBox onPress={() => this.onChangTrangThai(0)} checked={this.state.trang_thai_y_kien_can_bo === 0} color={AppConfig.blueBackground}></CheckBox>
                                        <Text style={[{ marginLeft: 20 }]}>CX</Text>
                                    </View>
                                    <View style={{ flexDirection: "row" }}>
                                        <CheckBox onPress={() => this.onChangTrangThai(1)} checked={this.state.trang_thai_y_kien_can_bo === 1} color={AppConfig.blueBackground}></CheckBox>
                                        <Text style={[{ marginLeft: 20 }]}>ĐX</Text>
                                    </View>
                                    <View style={{ flexDirection: "row" }}>
                                        <CheckBox onPress={() => this.onChangTrangThai(2)} checked={this.state.trang_thai_y_kien_can_bo === 2} color={AppConfig.blueBackground}></CheckBox>
                                        <Text style={[{ marginLeft: 20 }]}>ĐTL</Text>
                                    </View>
                                </View>
                            )
                        }
                        {
                            (this.state.loading) ? (
                                <Spinner color={AppConfig.blueBackground} />
                            ) : (
                                    <CardItem style={{ flex: 1, flexDirection: "row", paddingLeft: 5, paddingRight: 5 }}>
                                        <View style={{ width: 200 }}>
                                            <FlatList
                                                ref={(ref) => { this.flatListRef = ref }}
                                                data={this.state.dataSource}
                                                keyExtractor={(item, index) => "key" + item.ma_xin_y_kien_kc + index}
                                                renderItem={({ item }) => {
                                                    return (
                                                        <XYKTTTab_Item
                                                            activeTab={this.activeTab}
                                                            item={item}
                                                            ma_ctcb_kc={global.ma_ctcb_kc}
                                                            navigation={this.props.navigation}
                                                            onDelete={this.onDelete}
                                                            onPress={() => {
                                                                // console.log('onPress', item)
                                                                this.setState({
                                                                    ma_xin_y_kien_kc: item.ma_xin_y_kien_kc,
                                                                    cur_item: item
                                                                })
                                                            }}
                                                            fontSize={this.props.fontSize}
                                                            styles={this.styles}
                                                        />
                                                    )
                                                }}
                                                onRefresh={this.onRefresh}
                                                refreshing={this.state.refreshing}
                                                onEndReached={this.loadMore}
                                                onEndReachedThreshold={0.4}
                                                onMomentumScrollBegin={() => {
                                                    // console.log('onMomentumScrollBeginIpad')
                                                    this.onEndReachedCalledDuringMomentum = false;
                                                }}
                                                ListEmptyComponent={() => {
                                                    return (
                                                        <View style={{ alignItems: "center", marginTop: 10, }}>
                                                            <Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>Không có dữ liệu</Text>
                                                        </View>
                                                    )
                                                }}
                                            />
                                        </View>
                                        <View style={{ flex: 1 }}>
                                            {/* {this.renderXYKTT_Details_iPad()} */}
                                            {
                                                this.state.ma_xin_y_kien_kc !== 0 ? 
                                                    <XYKTT_Details_iPad
                                                        navigation={this.props.navigation}
                                                        ma_xin_y_kien_kc={this.state.ma_xin_y_kien_kc}
                                                        item={this.state.cur_item}
                                                        loaiGuiNhan={this.activeTab === 0 ? "Nhận" : "gửi"}
                                                    />
                                                : null
                                            }
                                        </View>
                                    </CardItem>
                                )
                        }
                    </KeyboardAvoidingView>
                    {/* </View> */}
                    <FooterTab tabActive="xyktt" navigation={this.props.navigation} />
                </Container>
            );
        } else {
            return (
                <Container style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
                    <HeaderWithLeftRight
                        title="Xin ý kiến thường trực"
                        buttonLeftName="menu"
                        onPressLeftButton={() => { this.props.navigation.dispatch(DrawerActions.toggleDrawer()) }}
                        buttonRightName="note-add"
                        onPressRightButton={() => { this.props.navigation.navigate("XYKTT_Gui") }}
                    />
                    <View style={{ flex: 1 }}>
                        <MyTab
                            values={["Đã nhận", "Đã gửi"]}
                            badges={[nv_nhan, 0]}
                            activeTab={this.activeTab}
                            onChangeTab={(index) => { this.onChangeTab(index) }}
                            fontSize={this.props.fontSize}
                        />
                        <SearchHeader
                            ref={(ref) => { this.searchHeader = ref }}
                            onSearchData={this.onSearchData}
                            fontSize={this.props.fontSize}
                        />
                        {
                            this.activeTab == 0 && 
                            (
                                <View style={{ flexDirection: "row", justifyContent: "space-evenly", padding: 10, backgroundColor: "#F5F5F5" }}>
                                    <View style={{ flexDirection: "row" }}>
                                        <CheckBox onPress={() => this.onChangTrangThai(-2)} checked={this.state.trang_thai_y_kien_can_bo === -2} color={AppConfig.blueBackground}></CheckBox>
                                        <Text style={[{ marginLeft: 20 }]}>Tất cả</Text>
                                    </View>
                                    <View style={{ flexDirection: "row" }}>
                                        <CheckBox onPress={() => this.onChangTrangThai(0)} checked={this.state.trang_thai_y_kien_can_bo === 0} color={AppConfig.blueBackground}></CheckBox>
                                        <Text style={[{ marginLeft: 20 }]}>CX</Text>
                                    </View>
                                    <View style={{ flexDirection: "row" }}>
                                        <CheckBox onPress={() => this.onChangTrangThai(1)} checked={this.state.trang_thai_y_kien_can_bo === 1} color={AppConfig.blueBackground}></CheckBox>
                                        <Text style={[{ marginLeft: 20 }]}>ĐX</Text>
                                    </View>
                                    <View style={{ flexDirection: "row" }}>
                                        <CheckBox onPress={() => this.onChangTrangThai(2)} checked={this.state.trang_thai_y_kien_can_bo === 2} color={AppConfig.blueBackground}></CheckBox>
                                        <Text style={[{ marginLeft: 20 }]}>ĐTL</Text>
                                    </View>
                                </View>
                            )
                        }
                        {
                            (this.state.loading) ? (
                                <View><Spinner color={AppConfig.blueBackground} /></View>
                            ) : (
                                    <FlatList
                                        ref={(ref) => { this.flatListRef = ref }}
                                        data={this.state.dataSource}
                                        keyExtractor={(item, index) => "key" + item.ma_xin_y_kien_kc + index}
                                        renderItem={({ item }) => {
                                            return (
                                                <XYKTTTab_Item
                                                    activeTab={this.activeTab}
                                                    item={item}
                                                    ma_ctcb_kc={global.ma_ctcb_kc}
                                                    navigation={this.props.navigation}
                                                    onDelete={this.onDelete}
                                                    fontSize={this.props.fontSize}
                                                    styles={this.styles}
                                                />
                                            )
                                        }}
                                        onRefresh={this.onRefresh}
                                        refreshing={this.state.refreshing}
                                        onEndReached={() => {
                                            // console.log('onEndReached')
                                            this.loadMore()
                                        }}
                                        onEndReachedThreshold={0.4}
                                        onMomentumScrollBegin={() => {
                                            // console.log('onMomentumScrollBeginmobile')
                                            this.onEndReachedCalledDuringMomentum = false;
                                        }}
                                        ListEmptyComponent={() => {
                                            return (
                                                <View style={{ alignItems: "center", marginTop: 10, }}>
                                                    <Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>Không có dữ liệu</Text>
                                                </View>
                                            )
                                        }}
                                    />
                                )
                        }
                    </View>
                    <FooterTab tabActive="xyktt" navigation={this.props.navigation} />
                </Container>
            );
        }

    }
}

function mapStateToProps(state) {
    return {
        cb_dsnvcb: state.cb_dsnvcb.data,
        fontSize: state.fontSize,
    }
}

export default connect(mapStateToProps)(XYKTTScreen)