import React, { Component, PureComponent } from 'react';
import { View, ScrollView, StyleSheet, TouchableWithoutFeedback, Image, Keyboard, TouchableOpacity, PixelRatio, TextInput, KeyboardAvoidingView, Platform } from "react-native"
import { connect } from "react-redux"
import { Container, Text, Card, CardItem, Button, Icon, Toast, Spinner } from "native-base"
import { AppConfig } from "../../AppConfig"
import { RemoveNoiDungTTDH, getFileChiTietTTDH, HtmlEncode } from "../../untils/TextUntils"
import HeaderWithLeft from "../../components/Header/HeaderWithLeft"
import API from "../../networks"
import ViewFile from "../../components/ViewFile"

class XYKTT_Details_Item extends PureComponent {
    constructor(props) {
        super(props)
        this.item = this.props.item
    }

    renderNguoiNhan = () => {
        let { listNguoiNhan } = this.props
        const dataCount = listNguoiNhan.length
        const numberShow = 20
        let reSult = []
        if (dataCount > 0) {
            let numberRender = dataCount > numberShow ? numberShow : dataCount
            for (i = 0; i < numberRender; i += 1) {
                if (i === numberRender - 1) {
                    if (listNguoiNhan[i].trang_thai_y_kien_can_bo === 0) {
                        reSult.push(<Text key={listNguoiNhan[i].ma_ctcb_kc} style={this.styles.textNguoiNhanChuaXem}>{listNguoiNhan[i].ho_va_ten_can_bo}</Text>)
                    } else {
                        reSult.push(<Text key={listNguoiNhan[i].ma_ctcb_kc} style={this.styles.textNguoiNhanDaXem}>{listNguoiNhan[i].ho_va_ten_can_bo}</Text>)
                    }
                } else {
                    if (listNguoiNhan[i].trang_thai_y_kien_can_bo === 0) {
                        reSult.push(<Text key={listNguoiNhan[i].ma_ctcb_kc} style={this.styles.textNguoiNhanChuaXem}>{listNguoiNhan[i].ho_va_ten_can_bo},</Text>)
                    } else {
                        reSult.push(<Text key={listNguoiNhan[i].ma_ctcb_kc} style={this.styles.textNguoiNhanDaXem}>{listNguoiNhan[i].ho_va_ten_can_bo},</Text>)
                    }
                }
            }
        }

        if (dataCount > numberShow) {
            return (
                <TouchableOpacity style={{ flexDirection: "row", flexWrap: "wrap" }} onPress={() => { this.setState({ isModalVisible: true }) }} >
                    <Text style={{ fontSize: this.props.fontSize.FontSizeSmall, color: "#808080", margin: 3 }}>Người nhận:</Text>
                    {reSult}
                    <Text style={{ fontSize: this.props.fontSize.FontSizeSmall, color: "#808080", margin: 3 }}>+{dataCount - numberShow}</Text>
                </TouchableOpacity>
            )
        } else {
            return (
                <View style={{ flexDirection: "row", flexWrap: "wrap" }} >
                    <Text style={{ fontSize: this.props.fontSize.FontSizeSmall, color: "#808080", margin: 3 }}>Người nhận:</Text>
                    {reSult}
                </View>
            )
        }
    }

    renderListFile = () => {
        let result = []
        if (this.item.src_file_xin_y_kien !== null) {
            let data = this.item.src_file_xin_y_kien.split(":")
            data = getFileChiTietTTDH(data)
            for (let item of data) {
                let itemTemp = item.split("|")
                result.push(
                    <View key={item} style={this.styles.viewFile}>
                        <Image style={this.styles.iconFile} source={{ uri: itemTemp[3] }} />
                        <Text numberOfLines={1} ellipsizeMode="tail" style={this.styles.textTenFile}>{itemTemp[1]}</Text>
                    </View>
                )
            }
        }
        return result
    }

    convertData = (listBinhLuan) => {
        let data = []
        for (let item of listBinhLuan) {
            const findData = data.some((element) => element.ma_can_bo_tra_loi === item.ma_can_bo_tra_loi)
            if (!findData) {
                data.push({
                    "ho_va_ten_can_bo": item.ho_va_ten_can_bo,
                    "ma_can_bo_tra_loi": item.ma_can_bo_tra_loi,
                    "ten_dv_gui": item.ten_dv_gui,
                    "data": [
                        {
                            "ma_y_kien_tra_loi": item.ma_y_kien_tra_loi,
                            "noi_dung": item.noi_dung,
                            "ngay_tao": item.ngay_tao.replace('T', ' ')
                        }
                    ]
                })
            } else {
                let updData = data.find((element) => element.ma_can_bo_tra_loi === item.ma_can_bo_tra_loi)
                let index = data.findIndex((element) => element.ma_can_bo_tra_loi === item.ma_can_bo_tra_loi)
                updData.data.push({
                    "ma_y_kien_tra_loi": item.ma_y_kien_tra_loi,
                    "noi_dung": item.noi_dung,
                    "ngay_tao": item.ngay_tao.replace('T', ' ')
                })
                data[index].data = updData.data
            }
        }
        return data
    }

    renderDataNote = (listBinhLuan) => {
        let dataaa = this.convertData(listBinhLuan)
        let result = []
        for (let item of dataaa) {
            result.push(
                <View key={item.ma_can_bo_tra_loi} style={{ marginHorizontal: 5, paddingTop: 5, paddingBottom: 10, borderColor: AppConfig.defaultLineColor, borderTopWidth: AppConfig.defaultLineWidth }}>
                    <Text style={[this.styles.textRight, { color: AppConfig.blueBackground }]}>
                        {`${item.ho_va_ten_can_bo}`}
                    </Text>
                    {this.renderComment(item.data)}
                </View>
            )
        }
        return result
    }

    renderComment = (comments) => {
        let result = []
        for (let item of comments) {
            let noi_dung = item.noi_dung.split('<br />').join('\n').split('<br>').join('\n')
            result.push(
                <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', borderRadius: 8, padding: 5, backgroundColor: '#F4F6FA',marginTop: 5}}>
                    <View style={{flex: 1, marginLeft: 5}}>
                        <Text>{item.ngay_tao}</Text>
                        <Text style={{fontWeight: '500'}}>{noi_dung}</Text>
                    </View>
                </View>
            )
        }
        return result
    }

    render() {
        this.styles = this.props.styles
        const isSend = this.props.cb_dsctcb.ma_can_bo === this.item.ma_can_bo_gui ? true : false
        const { listBinhLuan } = this.props
        const isRecieve = !isSend
        return (
            <Card noShadow={true}>
                <View style={this.styles.viewNguoiGui_NguoiNhan_File}>
                    <View style={{ flex: 1, justifyContent: "center", alignItems: 'center', flexDirection: "row" }}>
                        <View style={this.styles.viewNguoiGui}>
                            <Text style={this.styles.textNguoiGui}>{this.item.ho_va_ten_can_bo}</Text>
                        </View>
                        <View style={this.styles.viewNgayGui}>
                            <Text style={this.styles.textNgayGui}>{this.item.ngay_tao.replace('T', ' ')}</Text>
                        </View>
                    </View>
                    <View style={{ justifyContent: "flex-start" }}>
                        <ViewFile src_file={this.item.src_file_xin_y_kien} fontSize={this.props.fontSize}/>
                    </View>
                </View>
                <View style={this.styles.viewNoiDung}>
                    <Text style={this.styles.textNoiDung}>{this.item.noi_dung.split('<br />').join('\n').split('<br>').join('\n')}</Text>
                </View>
                <View style={this.styles.viewNguoiNhan}>
                    {this.renderNguoiNhan()}
                </View>
                <Card noShadow style={this.styles.Card}>
                    <CardItem style={this.styles.CardItem}>
                        <Text style={[this.styles.textRight, { fontSize: this.props.fontSize.FontSizeLarge, fontWeight: "bold", }]}>
                            Danh sách ý kiến
                        </Text>
                    </CardItem>
                    {this.renderDataNote(listBinhLuan)}
                </Card>
            </Card>
        )
    }
}

class XYKTT_Details extends Component {
    constructor(props) {
        super(props)
        this.state = {
            loading: true,
            isLoading: false,
            danhSachChiTiet: [],
            listNguoiNhan: [],
            listBinhLuan: [],
            noiDung: ""
        }
        this.ma_ttdh_kc = 0
        this.tieu_de = ""
        this.chuoi_ma_ctcb_nhan = ""
    }

    componentDidMount = () => {
        let ma_xin_y_kien_kc = this.props.ma_xin_y_kien_kc
        // console.log('componentDidMount ma_xin_y_kien_kc', ma_xin_y_kien_kc)
        this.getDSChiTietTTDHNhan(ma_xin_y_kien_kc)
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.ma_xin_y_kien_kc !== this.props.ma_xin_y_kien_kc) {
            this.item = nextProps.item
            this.loaiGuiNhan = nextProps.loaiGuiNhan
            this.getDSChiTietTTDHNhan(this.item.ma_xin_y_kien_kc)
        }
    }

    getDSChiTietTTDHNhan = (ma_xin_y_kien_kc) => {
        Promise.all([API.XinYKienThuongTruc.AU_XYKTT_CTXYKTT(global.ma_can_bo, ma_xin_y_kien_kc),
            API.XinYKienThuongTruc.AU_XYKTT_DSCBN(global.ma_can_bo, ma_xin_y_kien_kc),
            API.XinYKienThuongTruc.AU_XYKTT_DSBLCB(global.ma_can_bo, ma_xin_y_kien_kc)]).then((res) => {
                this.tieu_de = res[0].length > 0 ? res[0][0].tieu_de : ""
                this.chuoi_ma_ctcb_nhan = this.loadListNguoiNhan(res[1]).join(",")
                this.setState({ loading: false, danhSachChiTiet: res[0], listNguoiNhan: res[1], listBinhLuan: res[2] })
        });
    }

    loadListNguoiNhan = (arr) => {
        let temp = arr.map(x => x.ma_ctcb_kc)
        let result = []
        temp.forEach(element => {
            if (element !== global.ma_ctcb_kc && !result.some(x => x === element)) result.push(element)
        });
        return result
    }

    onPressReply = () => {
        if (this.state.noiDung) {
            if (this.state.noiDung.length > 2000) {
                Toast.show({ text: "Nội dung phản hồi vượt quá độ dài cho phép 2000 ký tự", type: "warning", buttonText: "Đóng", position: "top", duration: 2000 });
            } else {
                this.setState({ isLoading: true})
                let noiDung = this.state.noiDung
                noiDung = HtmlEncode(noiDung)
                let ma_xin_y_kien_kc = this.props.ma_xin_y_kien_kc
                API.XinYKienThuongTruc.AU_XYKTT_YKTT_CREATE(this.chuoi_ma_ctcb_nhan, global.ma_ctcb_kc, 0, noiDung, null, null, ma_xin_y_kien_kc).then(response => {
                    if (response !== "") {
                        API.Login.AU_DSNVCB(global.ma_ctcb_kc, this.props.dispatch)
                        API.Login.AU_PUSH_FCM_APP(this.chuoi_ma_ctcb_nhan, "xinykiendanhan", this.tieu_de, ma_xin_y_kien_kc, 0, 0, 0)
                        API.XinYKienThuongTruc.AU_XYKTT_DSBLCB(global.ma_can_bo, ma_xin_y_kien_kc).then((res) => {
                            // console.log('AU_XYKTT_DSBLCB success', res)
                            this.setState({ listBinhLuan: res, noiDung: "", isLoading: false }, () => {
                                Toast.show({ text: "Gửi phản hồi thành công!", type: "success", buttonText: "Đóng", position: "top", duration: 2000 })
                            })
                        })
                    } else {
                        Alert.alert("Thông báo", "Xảy ra lỗi trong quá trình gửi!")
                        this.setState({ isSending: false })
                    }
                })
            }
        } else {
            Toast.show({ text: "Thông tin phản hồi không được để trống!", type: "warning", buttonText: "Đóng", position: "top", duration: 2000 })
        }
    }

    renderDSChiTiet = () => {
        let result = []
        for (let item of this.state.danhSachChiTiet) {
            result.push(
                <XYKTT_Details_Item
                    listNguoiNhan={this.state.listNguoiNhan}
                    listBinhLuan={this.state.listBinhLuan}
                    navigation={this.props.navigation}
                    key={item.ma_xin_y_kien_kc}
                    item={item}
                    cb_dsctcb={this.props.cb_dsctcb}
                    fontSize={this.props.fontSize}
                    styles={this.styles}
                />
            )
        }
        return result
    }

    render() {

        this.styles = StyleSheet.create({
            viewItem: {
                paddingTop: 10,
                marginHorizontal: 5,
                borderTopWidth: AppConfig.defaultLineWidth,
                borderColor: AppConfig.defaultLineColor
            },
            labelTieuDe: {
                fontFamily: AppConfig.fontFamily,
                color: AppConfig.blueBackground,
                fontSize: this.props.fontSize.FontSizeSmall,
                marginBottom: 5,
            },
            txtTieuDe: {
                height: 120,
                fontFamily: AppConfig.fontFamily,
                color: "#000000",
                fontSize: this.props.fontSize.FontSizeNorman / PixelRatio.getFontScale(),
                borderColor: AppConfig.defaultLineColor,
                borderWidth: AppConfig.defaultLineWidth,
                borderRadius: 8
            },
            scrollContainer: {
                flex: 1,
                bottom: 0,
                backgroundColor: AppConfig.grayBackground
            },
            viewTieuDe: {
                backgroundColor: AppConfig.grayBackground,
                justifyContent: "center", alignContent: "center",
                flexWrap: "wrap",
                padding: 4,
            },
            textTieuDe: {
                fontSize: this.props.fontSize.FontSizeXLarge,
                fontWeight: "400",
                textAlign: "center"
            },
            viewNguoiGui_NguoiNhan_File: {
                marginLeft: 12,
                marginRight: 12,
            },
            viewNguoiGui: {
                width: "70%",
                justifyContent: "center",
            },
            viewNgayGui: {
                width: "30%",
                justifyContent: "center",
                alignItems: "flex-end",
            },
            viewNguoiNhan: {
                justifyContent: "center",
                flexWrap: "wrap",
            },
            textNguoiGui: {
                fontSize: this.props.fontSize.FontSizeNorman,
                margin: 3,
            },
            textLoaiTTDH: {
                fontSize: this.props.fontSize.FontSizeSmall,
                color: "#808080",
                margin: 3,
            },
            textNguoiNhanChuaXem: {
                fontSize: this.props.fontSize.FontSizeSmall,
                color: "red",
                margin: 3,
            },
            textNguoiNhanDaXem: {
                fontSize: this.props.fontSize.FontSizeSmall,
                color: "#808080",
                margin: 3,
            },
            textNgayGui: {
                fontSize: this.props.fontSize.FontSizeSmall,
                color: "#808080",
                textAlign: "center",
                margin: 3,
            },
            textNoiDung: {
                fontSize: this.props.fontSize.FontSizeNorman,
            },
            viewFile: {
                maxWidth: 250,
                height: 38,
                flexDirection: "row",
                alignItems: "center",
                margin: 2,
                padding: 2,
                borderRadius: 6,
                borderWidth: 0.5,
                borderColor: AppConfig.defaultLineColor
            },
            iconFile: {
                height: 25,
                width: 25
            },
            textTenFile: {
                maxWidth: 180,
                fontSize: this.props.fontSize.FontSizeNorman
            },
            viewNoiDung: {
                justifyContent: "center",
                alignContent: "center",
                flexWrap: "wrap",
                padding: 12,
            },
            viewPhanHoi_ChuyenTiep: {
                flexDirection: "row",
                justifyContent: "space-evenly",
                flexWrap: "wrap",
                alignItems: "center",
                marginLeft: 12,
                marginRight: 12,
                paddingTop: 6,
                paddingBottom: 6,
                borderTopWidth: AppConfig.defaultLineWidth,
                borderTopColor: AppConfig.defaultLineColor
            },
            btnPhanHoi: {
                marginTop: 3,
                marginBottom: 3,
                height: 35,
            },
            textPhanHoi: {
                color: "#000000",
                fontSize: this.props.fontSize.FontSizeSmall,
                fontWeight: "100",
            }
        })
        let title = this.state.danhSachChiTiet.length > 0 ? this.state.danhSachChiTiet[0].tieu_de : ""
        return (
            (this.state.loading) ? (
                <Spinner color={AppConfig.blueBackground} />
            ) : (
                    <Container style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
                        {/* <KeyboardAvoidingView style={{ flex: 1 }} enabled behavior={Platform.OS === "ios" ? "padding" : null}> */}
                            {/* <TouchableWithoutFeedback onPress={Keyboard.dismiss}> */}
                                <>
                                    {
                                        (this.state.isLoading) && (
                                            <View
                                                style={{
                                                position: "absolute",
                                                flex: 1,
                                                width: "100%",
                                                height: "100%",
                                                backgroundColor: "rgba(128, 129, 130, 0.5)",
                                                zIndex: 9999,
                                                justifyContent: "center",
                                                alignItems: "center",
                                                }}
                                            >
                                                <Image
                                                source={require("../../assets/images/dualball.gif")}
                                                style={{ width: 60, height: 60, borderRadius: 10 }}
                                                />
                                            </View>
                                        )
                                    }
                                    <ScrollView
                                        ref={(scroll) => {this.scroll = scroll}}
                                        bounces={false}
                                        showsVerticalScrollIndicator={false}
                                        scrollEnabled={true}
                                        style={this.styles.scrollContainer}
                                    >
                                        <View style={this.styles.viewTieuDe}>
                                            <Text style={this.styles.textTieuDe}>{RemoveNoiDungTTDH(title)}</Text>
                                        </View>
                                        {this.renderDSChiTiet()}
                                        <Card noShadow style={this.styles.Card}>
                                            <CardItem style={this.styles.CardItem}>
                                                <Text style={[this.styles.textRight, { fontSize: this.props.fontSize.FontSizeLarge, fontWeight: "bold", }]}>
                                                    Gửi ý kiến phản hồi
                                                </Text>
                                            </CardItem>
                                            <View style={this.styles.viewItem}>
                                                <Text style={[this.styles.textRight, { color: AppConfig.blueBackground, marginBottom: 5 }]}>Nội dung ý kiến</Text>
                                                <TextInput
                                                    editable
                                                    multiline
                                                    numberOfLines={4}
                                                    // onFocus={_ => this.onFocusPress()}
                                                    style={this.styles.txtTieuDe}
                                                    value={this.state.noiDung}
                                                    onChangeText={(text) => this.setState({ noiDung: text })}
                                                />
                                                <View style={this.styles.viewPhanHoi_ChuyenTiep}>
                                                    <Button bordered rounded style={this.styles.btnPhanHoi} iconLeft onPress={_ => this.onPressReply()}>
                                                        <Icon name="reply" type="FontAwesome" ></Icon>
                                                        <Text style={this.styles.textPhanHoi}>Gửi</Text>
                                                    </Button>
                                                </View>
                                            </View>
                                        </Card>
                                    </ScrollView>
                                </>
                            {/* </TouchableWithoutFeedback> */}
                        {/* </KeyboardAvoidingView> */}
                    </Container >
                )
        );
    }
}

function mapStateToProps(state) {
    return {
        cb_dsctcb: state.cb_dsctcb.data,
        fontSize: state.fontSize,
    }
}

export default connect(mapStateToProps)(XYKTT_Details)