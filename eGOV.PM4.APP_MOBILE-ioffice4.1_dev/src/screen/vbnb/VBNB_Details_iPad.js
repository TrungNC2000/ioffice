import React, { Component } from 'react';
import { View, ScrollView, StyleSheet } from "react-native"
import { connect } from "react-redux"
import { Container, Button, Text, Card, CardItem } from "native-base"
import { AppConfig } from "../../AppConfig"
import { RemoveHTMLTag, ConvertDateTimeDetail } from "../../untils/TextUntils"
import API from "../../networks"
import ViewFile from "../../components/ViewFile"

class Table_ChiTiet_VBNB extends Component {
    shouldComponentUpdate(nextProps, nextState) {
        if (this.props !== nextProps) {
            return true
        }
        return false
    }

    render() {
        this.styles = this.props.styles
        if (this.props.dataDetails.length > 0) {
            const { trich_yeu, so_hieu, ngay_luu, ten_loai_van_ban, noi_nhan,
                src_van_ban, ten_linh_vuc_van_ban, ten_co_quan_ban_hanh, } = this.props.dataDetails[0]
            return (
                <Card noShadow style={this.styles.Card}>
                    {
                        (trich_yeu !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Trích yếu:</Text>
                                <Text style={[this.styles.textRight, { fontWeight: "500" }]}>
                                    {RemoveHTMLTag(trich_yeu)}
                                </Text>
                            </CardItem>
                        )
                    }
                    {
                        (so_hieu !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Số hiệu:</Text>
                                <Text style={[this.styles.textRight, { color: "red" }]}>
                                    {so_hieu}
                                </Text>
                            </CardItem>
                        )
                    }
                    {
                        (ten_co_quan_ban_hanh !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Cơ quan ban hành:</Text>
                                <Text style={[this.styles.textRight]}>
                                    {ten_co_quan_ban_hanh}
                                </Text>
                            </CardItem>
                        )
                    }
                    {
                        (ngay_luu !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Ngày văn bản:</Text>
                                <Text style={[this.styles.textRight]}>
                                    {ConvertDateTimeDetail(ngay_luu)}
                                </Text>
                            </CardItem>
                        )
                    }
                    {
                        (noi_nhan !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Nơi nhận:</Text>
                                <Text style={[this.styles.textRight]}>
                                    {noi_nhan}
                                </Text>
                            </CardItem>
                        )
                    }
                    {
                        (ten_linh_vuc_van_ban !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Lĩnh vực:</Text>
                                <Text style={[this.styles.textRight]}>
                                    {ten_linh_vuc_van_ban}
                                </Text>
                            </CardItem>
                        )
                    }
                    {
                        (ten_loai_van_ban !== null) && (
                            <CardItem style={this.styles.CardItem}>
                                <Text style={this.styles.textLeft}>Loại:</Text>
                                <Text style={[this.styles.textRight]}>
                                    {ten_loai_van_ban}
                                </Text>
                            </CardItem>
                        )
                    }
                    {
                        (src_van_ban !== null) && (
                            <View>
                                <CardItem style={this.styles.CardItem}>
                                    <Text style={this.styles.textLeft}>Tệp tin đính kèm:</Text>
                                </CardItem>
                                <CardItem style={this.styles.CardItem}>
                                    <ViewFile
                                        src_file={src_van_ban}
                                        isUpdateViewCV={this.props.isUpdateViewCV}
                                        updateViewCV={this.props.updateViewCV}
                                        fontSize={this.props.fontSize}
                                    />
                                </CardItem>
                            </View>
                        )
                    }
                    {this.props.renderButton()}
                </Card>
            )
        } else {
            return <View></View>
        }
    }
}

class Table_ButPhe_VBNB extends Component {
    shouldComponentUpdate(nextProps, nextState) {
        if (this.props !== nextProps) {
            return true
        }
        return false
    }

    rennderdataChild = (dataChild) => {
        let result = []
        for (let item of dataChild) {
            result.push(
                <View key={item} style={{ alignItems: 'flex-start' }}>
                    <Text style={[this.styles.textRight, { paddingTop: 5 }]}>
                        {item.ten_can_bo_nhan} {item.ngay_xem !== null ? "(Đã xem lúc " + ConvertDateTimeDetail(item.ngay_xem) + ")" : null}
                    </Text>
                    {
                        (item.y_kien_xu_ly !== null) &&
                        <Text style={[this.styles.textRight, { textAlign: 'justify' }]}>
                            {item.y_kien_xu_ly !== null ? "Ý kiến xử lý: " + (item.y_kien_xu_ly) : null}
                        </Text>
                    }
                </View>
            )
        }
        return result
    }

    renderDataNote = (dataNotes) => {
        let result = []
        for (let item of dataNotes) {
            let dataChild = dataNotes.filter(el => el.ma_vbnb_gui_cha === item.ma_van_ban_noi_bo_gui_kc)
            if (dataChild.length > 0) {
                result.push(
                    <View key={item.ma_van_ban_noi_bo_gui_kc} style={{ paddingTop: 5, paddingBottom: 10, borderColor: AppConfig.defaultLineColor, borderTopWidth: AppConfig.defaultLineWidth }}>
                        <CardItem style={{ flexDirection: 'row' }}>
                            <View style={{ width: 100 }}>
                                <Text style={[this.styles.textRight]}>Người gửi</Text>
                            </View>
                            <View style={{ flex: 1, justifyContent: 'flex-start' }}>
                                <Text style={[this.styles.textRight]}>{item.ten_can_bo_nhan}</Text>
                            </View>
                        </CardItem>
                        <CardItem style={[{ flexDirection: 'row', alignItems: 'flex-start', justifyContent: 'flex-start' }]}>
                            <View style={{ justifyContent: 'flex-start', width: 100 }}>
                                <Text style={[this.styles.textRight, { textAlign: 'justify', paddingTop: 5 }]}>Người nhận</Text>
                            </View>
                            <View style={{ flex: 1 }}>{this.rennderdataChild(dataChild)}</View>
                        </CardItem>
                    </View>
                )
            }
        }
        return result
    }

    render() {
        this.styles = this.props.styles
        let dataNotes = this.props.dataNotes
        if (this.props.dataNotes.length > 0) {
            return (
                <Card noShadow style={this.styles.Card}>
                    <CardItem style={this.styles.CardItem}>
                        <Text style={[this.styles.textRight, { fontSize: this.props.fontSize.FontSizeLarge, fontWeight: "bold", }]}>
                            Tổng hợp ý kiến xử lý
                        </Text>
                    </CardItem>
                    {
                        this.renderDataNote(dataNotes)
                    }
                </Card>
            )
        } else {
            return (
                <View></View>
            )
        }
    }
}

class VBNB_Details extends Component {
    constructor(props) {
        super(props)
        this.state = {
            dataDetails: [],
            dataNotes: [],
            listNguoiNhan: []
        }

        this.activeTab = 0
        this.ma_vbnb_kc = 0
        this.ma_van_ban_noi_bo_gui_kc = 0
    }

    componentDidMount = () => {
        this.ma_vbnb_kc = this.props.ma_vbnb_kc
        this.ma_van_ban_noi_bo_gui_kc = this.props.ma_van_ban_noi_bo_gui_kc
        this.activeTab = this.props.activeTab
        this.getDetails(this.activeTab)
        this.getNotes()
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.ma_vbnb_kc !== this.props.ma_vbnb_kc) {
            this.ma_vbnb_kc = nextProps.ma_vbnb_kc
            this.ma_van_ban_noi_bo_gui_kc = nextProps.ma_van_ban_noi_bo_gui_kc
            this.activeTab = nextProps.activeTab
            this.getDetails(this.activeTab)
            this.getNotes()
        }
    }

    getDetails = async (activeTab) => {
        let ma_van_ban_noi_bo_gui_kc = this.ma_van_ban_noi_bo_gui_kc
        if (activeTab === 0) {
            await API.VanBanNoiBo.AU_VBNB_CTVBNBDN(ma_van_ban_noi_bo_gui_kc)
                .then((dataDetails) => {
                    this.setState({ dataDetails })
                })
        } else {
            await API.VanBanNoiBo.AU_VBNB_CTVBNBDG(ma_van_ban_noi_bo_gui_kc)
                .then((dataDetails) => {
                    this.setState({ dataDetails })
                })
        }
    }

    getNotes = async () => {
        let ma_vbnb_kc = this.ma_vbnb_kc
        await API.VanBanNoiBo.AU_VBNB_CQTXLVBNB(ma_vbnb_kc)
            .then((dataNotes) => {
                this.setState({ dataNotes })
            })
    }

    onForward = () => {
        if (this.state.dataDetails.length === 0) {
            return null
        }

        let ma_vbnb_kc = this.ma_vbnb_kc
        let ma_vbnb_gui_cha = this.state.dataDetails[0].ma_van_ban_noi_bo_gui_kc
        let trang_thai_xu_ly = this.state.dataDetails[0].trang_thai_xu_ly
        let trich_yeu = this.state.dataDetails[0].trich_yeu

        this.props.navigation.navigate("VBNB_Chuyen", {
            'ma_vbnb_kc': ma_vbnb_kc,
            'ma_vbnb_gui_cha': ma_vbnb_gui_cha,
            'trang_thai_xu_ly': trang_thai_xu_ly,
            'trich_yeu': trich_yeu,
        })
    }

    renderButton = () => {
        return (
            <CardItem style={[this.styles.CardItem, this.styles.CardItemButton]}>
                <Button primary style={this.styles.Button} onPress={this.onForward}><Text style={this.styles.ButtonText}>Chuyển</Text></Button>
            </CardItem>
        )
    }

    render() {

        this.styles = StyleSheet.create({
            scrollContainer: {
                flex: 1,
                bottom: 0,
                backgroundColor: AppConfig.grayBackground
            },
            viewTieuDe: {
                backgroundColor: AppConfig.grayBackground,
                justifyContent: "center", alignContent: "center",
                flexWrap: "wrap",
                padding: 4,
            },
            textTieuDe: {
                fontSize: this.props.fontSize.FontSizeXLarge,
                fontWeight: "400",
                textAlign: "center"
            },

            //Table_ChiTiet_VBNB
            Card: {
                marginLeft: 6,
                marginRight: 6,
                marginTop: 0,
                padding: 10,
                borderRadius: 6,
            },
            CardItem: {
                marginTop: AppConfig.defaultPadding,
                marginBottom: AppConfig.defaultPadding,
            },
            CardItemButton: {
                marginTop: 10,
                paddingTop: 10,
                borderTopColor: AppConfig.defaultLineColor,
                borderTopWidth: AppConfig.defaultLineWidth,
                flexDirection: 'row',
                flexWrap: "wrap",
                justifyContent: "center",
                alignItems: "center"
            },
            Button: {
                margin: 3,
                height: 40,
                justifyContent: "center",
                alignItems: "center"
            },
            ButtonText: {
                fontSize: this.props.fontSize.FontSizeNorman,
                textAlign: "center"
            },
            textLeft: {
                fontSize: this.props.fontSize.FontSizeNorman,
                color: AppConfig.grayText
            },
            textRight: {
                paddingLeft: 5,
                fontSize: this.props.fontSize.FontSizeNorman,
            }
        })

        return (
            <Container style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
                <ScrollView
                    bounces={false}
                    removeClippedSubviews={true}
                    scrollEnabled={true}
                    style={this.styles.scrollContainer}
                >
                    <Table_ChiTiet_VBNB
                        dataDetails={this.state.dataDetails}
                        renderButton={() => this.renderButton()}
                        fontSize={this.props.fontSize}
                        styles={this.styles}
                    />
                    <Table_ButPhe_VBNB
                        dataNotes={this.state.dataNotes}
                        fontSize={this.props.fontSize}
                        styles={this.styles}
                    />
                </ScrollView>
            </Container>
        );
    }
}

function mapStateToProps(state) {
    return {
        fontSize: state.fontSize,
    }
}

export default connect(mapStateToProps)(VBNB_Details)