import React, { Component, PureComponent } from 'react';
import { View, StyleSheet, FlatList, Alert, TouchableOpacity, Platform } from "react-native"
import { Container, Text, CardItem, Spinner, Toast } from "native-base"
import { connect } from "react-redux"
import { DrawerActions } from "react-navigation"
import { Menu, MenuOptions, MenuOption, MenuTrigger } from 'react-native-popup-menu'
import Icon from "react-native-vector-icons/FontAwesome5"
import HeaderWithLeft from "../../components/Header/HeaderWithLeft"
import FooterTab from "../../components/Footer/FooterTab"
import SearchHeader from "../../components/SearchHeader"
import MyTab from "../../components/Tab"
import API from "../../networks"
import ModalListFile from "../../components/ModalListFile"
import ModalWebView from "../../components/ModalWebView"
import { AppConfig } from "../../AppConfig"
import { CheckSession } from "../../untils/NetInfo"
import { getFileChiTietTTDH, RemoveNoiDungTTDH, ConvertDateTime, ConvertDateTimeDetail, ToastSuccess, checkVersionWeb, getNVOld, getNVNew } from "../../untils/TextUntils"
import VBNB_Details_iPad from "./VBNB_Details_iPad"

class VBNBTab_Item extends PureComponent {
    constructor(props) {
        super(props)
        this.state = {
            isModalVisible: false,
            isBold: this.props.item.xem === 0 ? true : false
        }
    }

    onPressItem = () => {
        if (Platform.OS === "ios" && Platform.isPad) {
            this.updateViewCV()
            this.props.onPress()
        } else {
            this.props.navigation.navigate("VBNB_Details", {
                ma_vbnb_kc: this.props.item.ma_vbnb_kc,
                ma_van_ban_noi_bo_gui_kc: this.props.item.ma_van_ban_noi_bo_gui_kc,
                isUpdateViewCV: this.state.isBold,
                updateViewCV: this.updateViewCV,
                activeTab: this.props.activeTab
            })
        }
    }

    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible })
    }

    onViewFile = () => {
        this.toggleModal()
    }

    updateViewCV = async () => {
        let ma_vbnb_gui = this.props.item.ma_van_ban_noi_bo_gui_kc
        let trang_thai_xu_ly = 3
        await API.VanBanNoiBo.AU_VBNB_XVBNB(ma_vbnb_gui, trang_thai_xu_ly).then((result) => {
            if (result) {
                this.setState({ isBold: false })
            }
        })
    }

    render() {
        this.styles = this.props.styles
        const { item } = this.props
        let dataFile = item.src_van_ban ? getFileChiTietTTDH(item.src_van_ban.split(":")) : []
        let styleTemp = this.styles.textNoBold
        if (this.state.isBold) {
            styleTemp = this.styles.textBold
        }

        let borderRightWidth = (item.ma_vbnb_kc === this.props.cur_ma_vbnb_kc) ? 2.5 : 0.8
        let borderRightColor = (item.ma_vbnb_kc === this.props.cur_ma_vbnb_kc) ? "red" : "lightgray"

        if (Platform.OS === "ios" && Platform.isPad) {
            if (this.props.activeTab === 0) {
                return (
                    <CardItem style={[this.styles.CardItem, { borderRightWidth, borderRightColor }]} key={item.ma_vbnb_kc}>
                        <TouchableOpacity style={{ justifyContent: 'center', flex: 1 }} onPress={this.onPressItem}>
                            <Text numberOfLines={2} ellipsizeMode="tail" style={[styleTemp]}>{RemoveNoiDungTTDH(item.trich_yeu)} </Text>
                            <Text numberOfLines={1} ellipsizeMode="tail" style={[this.styles.txtNoiDung]}>Số: {item.so_hieu} </Text>
                            <Text numberOfLines={1} ellipsizeMode="tail" style={[this.styles.txtNoiDung]}>Người gửi: {item.ten_can_bo_gui} </Text>
                            {
                                (item.ten_don_vi_quan_tri) && (
                                    <Text numberOfLines={1} ellipsizeMode="tail" style={[this.styles.txtNoiDung]}>Đơn vị gửi: {item.ten_don_vi_quan_tri} </Text>
                                )
                            }
                            <Text numberOfLines={1} ellipsizeMode="tail" style={[this.styles.txtNoiDung]}>Ngày: {ConvertDateTimeDetail(item.ngay_gui)}</Text>
                        </TouchableOpacity>
                    </CardItem>
                )
            } else {
                return (
                    <CardItem style={[this.styles.CardItem, { borderRightWidth, borderRightColor }]} key={item.ma_vbnb_kc}>
                        <TouchableOpacity style={{ justifyContent: 'center', flex: 1 }} onPress={this.onPressItem}>
                            <Text numberOfLines={2} ellipsizeMode="tail" style={[this.styles.textNoBold]}>{item.trich_yeu} </Text>
                            <Text numberOfLines={1} ellipsizeMode="tail" style={[this.styles.txtNoiDung]}>Số: {item.so_hieu ? item.so_hieu : "Rỗng"} </Text>
                            <Text numberOfLines={1} ellipsizeMode="tail" style={[this.styles.txtNoiDung]}>Xuất xứ: {item.xuat_xu ? item.xuat_xu : "Rỗng"} </Text>
                            <Text numberOfLines={1} ellipsizeMode="tail" style={[this.styles.txtNoiDung]}>Ngày: {ConvertDateTimeDetail(item.ngay_gui)}</Text>
                        </TouchableOpacity>
                    </CardItem>
                )
            }
        } else {
            if (this.props.activeTab === 0) {
                return (
                    <CardItem noShadow style={this.styles.CardItem} key={item.ma_vbnb_kc}>
                        <TouchableOpacity style={{ justifyContent: 'center', flex: 1, left: 0, marginRight: 100 }} onPress={this.onPressItem}>
                            <Text numberOfLines={2} ellipsizeMode="tail" style={[styleTemp]}>{RemoveNoiDungTTDH(item.trich_yeu)} </Text>
                            <Text numberOfLines={1} ellipsizeMode="tail" style={[this.styles.txtNoiDung]}>Số: {item.so_hieu} </Text>
                            <Text numberOfLines={1} ellipsizeMode="tail" style={[this.styles.txtNoiDung]}>Người gửi: {item.ten_can_bo_gui} </Text>
                            {
                                (item.ten_don_vi_quan_tri) && (
                                    <Text numberOfLines={1} ellipsizeMode="tail" style={[this.styles.txtNoiDung]}>Đơn vị gửi: {item.ten_don_vi_quan_tri} </Text>
                                )
                            }
                        </TouchableOpacity>
                        <View style={{ width: 100, top: 0, right: 0, bottom: 0, position: "absolute", alignItems: 'center', justifyContent: "center" }}>
                            <Text style={this.styles.txtNgayNhan}>{ConvertDateTime(item.ngay_gui)} </Text>
                            <Menu>
                                <MenuTrigger style={this.styles.menuTrigger}>
                                    <Icon name="ellipsis-v" style={this.styles.menuTriggerIcon}></Icon>
                                </MenuTrigger>
                                <MenuOptions optionsContainerStyle={this.styles.menuOptionsContainer}>
                                    {
                                        (dataFile.length > 1 || (dataFile.length === 1 && dataFile[0] !== "")) && (
                                            <MenuOption onSelect={() => this.onViewFile()} >
                                                <Text style={this.styles.menuOptionsText}>Xem tệp tin</Text>
                                            </MenuOption>
                                        )
                                    }
                                    <MenuOption onSelect={_ => this.props.onForwardItem(item.ma_vbnb_kc, item.trang_thai_xu_ly)}>
                                        <Text style={this.styles.menuOptionsText}>Chuyển tiếp</Text>
                                    </MenuOption>
                                    <MenuOption onSelect={_ => this.props.onDeleteItemNhan(item.ma_van_ban_noi_bo_gui_kc)}>
                                        <Text style={this.styles.menuOptionsTextDelete}>Xoá</Text>
                                    </MenuOption>
                                </MenuOptions>
                            </Menu>
                            {
                                (dataFile.length === 1) && (
                                    <ModalWebView
                                        isModalVisible={this.state.isModalVisible}
                                        title={dataFile[0].split("|")[1]}
                                        url={dataFile[0].split("|")[0]}
                                        path={dataFile[0].split("|")[4]}
                                        toggleModal={() => this.toggleModal()}
                                        isUpdateViewCV={this.state.isBold}
                                        updateViewCV={() => this.updateViewCV()}
                                    />
                                )
                            }
                            {
                                (dataFile.length > 1) && (
                                    <ModalListFile
                                        isModalVisible={this.state.isModalVisible}
                                        dataFile={dataFile}
                                        toggleModal={() => this.toggleModal()}
                                        isUpdateViewCV={this.state.isBold}
                                        updateViewCV={() => this.updateViewCV()}
                                    />
                                )
                            }
                        </View>
                    </CardItem>
                )
            } else {
                return (
                    <CardItem noShadow style={this.styles.CardItem} key={item.ma_vbnb_kc}>
                        <TouchableOpacity style={{ justifyContent: 'center', flex: 1, left: 0, marginRight: 100 }} onPress={this.onPressItem}>
                            <Text numberOfLines={2} ellipsizeMode="tail" style={[this.styles.textNoBold]}>{item.trich_yeu} </Text>
                            <Text numberOfLines={1} ellipsizeMode="tail" style={[this.styles.txtNoiDung]}>Số: {item.so_hieu ? item.so_hieu : "Rỗng"} </Text>
                            <Text numberOfLines={1} ellipsizeMode="tail" style={[this.styles.txtNoiDung]}>Xuất xứ: {item.xuat_xu ? item.xuat_xu : "Rỗng"} </Text>
                        </TouchableOpacity>
                        <View style={{ width: 100, top: 0, right: 0, bottom: 0, position: "absolute", alignItems: 'center', justifyContent: "center" }}>
                            <Text style={this.styles.txtNgayNhan}>{ConvertDateTime(item.ngay_gui)} </Text>
                            <Menu>
                                <MenuTrigger style={this.styles.menuTrigger}>
                                    <Icon name="ellipsis-v" style={this.styles.menuTriggerIcon}></Icon>
                                </MenuTrigger>
                                <MenuOptions optionsContainerStyle={this.styles.menuOptionsContainer}>
                                    {
                                        (dataFile.length > 1 || (dataFile.length === 1 && dataFile[0] !== "")) && (
                                            <MenuOption onSelect={() => this.onViewFile()} >
                                                <Text style={this.styles.menuOptionsText}>Xem tệp tin</Text>
                                            </MenuOption>
                                        )
                                    }
                                    <MenuOption onSelect={_ => this.props.onForwardItem(item.ma_vbnb_kc, item.trang_thai_xu_ly)} >
                                        <Text style={this.styles.menuOptionsText}>Chuyển tiếp</Text>
                                    </MenuOption>
                                    <MenuOption onSelect={_ => this.props.onDeleteItemGui(item.ma_vbnb_kc)} >
                                        <Text style={this.styles.menuOptionsTextDelete}>Xoá</Text>
                                    </MenuOption>
                                </MenuOptions>
                            </Menu>
                            {
                                (dataFile.length === 1) && (
                                    <ModalWebView
                                        isModalVisible={this.state.isModalVisible}
                                        title={dataFile[0].split("|")[1]}
                                        url={dataFile[0].split("|")[0]}
                                        toggleModal={() => this.toggleModal()}
                                        isUpdateViewCV={this.state.isBold}
                                        updateViewCV={() => this.updateViewCV()}
                                    />
                                )
                            }
                            {
                                (dataFile.length > 1) && (
                                    <ModalListFile
                                        isModalVisible={this.state.isModalVisible}
                                        dataFile={dataFile}
                                        toggleModal={() => this.toggleModal()}
                                        isUpdateViewCV={this.state.isBold}
                                        updateViewCV={() => this.updateViewCV()}
                                    />
                                )
                            }
                        </View>
                    </CardItem>
                )
            }
        }


    }
}

class VBNBScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            loading: true,
            refreshing: false,
            dataSource: [],
            ma_vbnb_kc: 0,
            ma_van_ban_noi_bo_gui_kc: 0,
        }
        this.page = 1
        this.activeTab = 0
        this.activeTabVBNBDN = 0
    }

    componentDidMount() {
        this.didBlurSubscription = this.props.navigation.addListener('willFocus', payload => { CheckSession(this.props.navigation) })
        this.activeTab = this.props.navigation.getParam("activeTab", 0)
        this.getListByTabIndex(this.activeTab)
        API.Login.AU_DSNVCB(global.ma_ctcb_kc, this.props.dispatch)
    }

    componentWillUnmount = () => {
        this.didBlurSubscription.remove()
    };

    shouldComponentUpdate = (nextProps, nextState) => {
        if (this.state.loading !== nextState.loading ||
            this.state.refreshing !== nextState.refreshing ||
            this.state.dataSource !== nextState.dataSource ||
            this.state.ma_vbnb_kc !== nextState.ma_vbnb_kc ||
            this.state.ma_van_ban_noi_bo_gui_kc !== nextState.ma_van_ban_noi_bo_gui_kc ||
            this.props.cb_dsnvcb !== nextProps.cb_dsnvcb) {
            return true
        }
        return false
    };

    getListByTabIndex = () => {
        let ma_ctcb = global.ma_ctcb_kc;
        let tabIndex = this.activeTab
        let tabIndexVBNBDN = this.activeTabVBNBDN
        let page = this.page
        let keyWord = this.searchHeader.getKeyWord()

        if (keyWord !== "") {
            if (tabIndex === 0) {
                let size = AppConfig.pageSizeVBNBDN
                let data1 = []
                let data2 = []
                API.VanBanNoiBo.AU_VBNB_DSVBNBDN(keyWord, "", page, size, ma_ctcb, tabIndexVBNBDN).then(response1 => {
                    data1 = response1
                    API.VanBanNoiBo.AU_VBNB_DSVBNBDN("", keyWord, page, size, ma_ctcb, tabIndexVBNBDN).then(response2 => {
                        data2 = response2
                        data1 = data2.filter(bb => !data1.find(aa => bb.ma_van_ban_kc === aa.ma_van_ban_kc)).concat(data1);
                        if (tabIndex === this.activeTab) {
                            this.setState({ dataSource: page === 1 ? data1 : [...this.state.dataSource, ...data1] }, () => {
                                this.setState({ loading: false, refreshing: false })
                            })
                        }
                    })
                })
            } else {
                let size = AppConfig.pageSizeVBNBDGDPH
                let data1 = []
                let data2 = []
                API.VanBanNoiBo.AU_VBNB_DSVBNBDGDPH(keyWord, "", page, size, ma_ctcb).then(response1 => {
                    data1 = response1
                    API.VanBanNoiBo.AU_VBNB_DSVBNBDGDPH("", keyWord, page, size, ma_ctcb).then(response2 => {
                        data2 = response2
                        data1 = data2.filter(bb => !data1.find(aa => bb.ma_van_ban_kc === aa.ma_van_ban_kc)).concat(data1);
                        if (tabIndex === this.activeTab) {
                            this.setState({ dataSource: page === 1 ? data1 : [...this.state.dataSource, ...data1] }, () => {
                                this.setState({ loading: false, refreshing: false })
                            })
                        }
                    })
                })
            }
        } else {
            if (tabIndex === 0) {
                let size = AppConfig.pageSizeVBNBDN
                API.VanBanNoiBo.AU_VBNB_DSVBNBDN("", "", page, size, ma_ctcb, tabIndexVBNBDN).then(response => {
                    if (tabIndex === this.activeTab) {
                        this.setState({ dataSource: page === 1 ? response : [...this.state.dataSource, ...response] }, () => {
                            this.setState({ loading: false, refreshing: false })
                        })
                    }
                })
            } else {
                let size = AppConfig.pageSizeVBNBDGDPH
                API.VanBanNoiBo.AU_VBNB_DSVBNBDGDPH("", "", page, size, ma_ctcb).then(response => {
                    if (tabIndex === this.activeTab) {
                        this.setState({ dataSource: page === 1 ? response : [...this.state.dataSource, ...response] }, () => {
                            this.setState({ loading: false, refreshing: false })
                        })
                    }
                })
            }
        }
    }

    onChangeTab = (index) => {
        if (!this.state.loading) {
            this.page = 1
            this.activeTab = index
            //this.activeTabVBNBDN = 0
            this.searchHeader.setKeyWord("", () => {
                this.setState({ dataSource: [], loading: true, ma_vbnb_kc: 0 })
                this.getListByTabIndex()
            })
        }
    }

    onChangeTabVBNBDN = (index) => {
        if (!this.state.loading) {
            this.page = 1
            this.activeTab = 0
            this.activeTabVBNBDN = index
            this.searchHeader.setKeyWord("", () => {
                this.setState({ dataSource: [], loading: true, ma_vbnb_kc: 0 })
                this.getListByTabIndex()
            })
        }
    }

    onRefresh = () => {
        if (!this.state.loading) {
            this.page = 1
            this.setState({ dataSource: [], refreshing: true, loading: true, ma_vbnb_kc: 0 })
            this.getListByTabIndex()
        }
    }

    loadMore = () => {
        if (!this.state.loading) {
            this.page = this.page + 1
            this.getListByTabIndex()
        }
    }

    onSearchData = () => {
        if (!this.state.loading) {
            this.page = 1
            this.setState({ dataSource: [], loading: true, ma_vbnb_kc: 0 })
            this.getListByTabIndex()
        }
    }

    onForwardItem = (ma_vbnb_kc, trang_thai_xu_ly) => {
        this.props.navigation.navigate("VBNB_Chuyen", {
            'ma_vbnb_kc': ma_vbnb_kc,
            'ma_vbnb_gui_cha': ma_vbnb_kc,
            'trang_thai_xu_ly': trang_thai_xu_ly
        })
    }

    onDeleteItemNhan = (ma_van_ban_noi_bo_gui_kc) => {
        Alert.alert("Xác nhận", "Xóa văn bản nội bộ này?",
            [
                { text: "Không", style: "cancel" },
                {
                    text: "Đồng ý", onPress: () => {
                        API.VanBanNoiBo.AU_VBNB_XVBNBDN(ma_van_ban_noi_bo_gui_kc).then(response => {
                            if (response) {
                                let dataSource = this.state.dataSource.filter(item => item.ma_van_ban_noi_bo_gui_kc !== ma_van_ban_noi_bo_gui_kc)
                                ToastSuccess("Xóa thành công!", () => {
                                    this.setState({ dataSource, ma_vbnb_kc: 0 })
                                    API.Login.AU_DSNVCB(global.ma_ctcb_kc, this.props.dispatch)
                                })
                            } else {
                                Toast.show({ text: "Xảy ra lỗi. Vui lòng thực hiện lại sau!", type: "danger", buttonText: "Đóng", position: "top", duration: 2000 })
                            }
                        })
                    }
                }
            ],
            { cancelable: false }
        )
    }

    onDeleteItemGui = (ma_vbnb_kc) => {
        Alert.alert("Xác nhận", "Xóa văn bản nội bộ này?",
            [
                { text: "Không", style: "cancel" },
                {
                    text: "Đồng ý", onPress: () => {
                        API.VanBanNoiBo.AU_VBNB_XVBNBDG(ma_vbnb_kc).then(response => {
                            if (response) {
                                let dataSource = this.state.dataSource.filter(item => item.ma_vbnb_kc !== ma_vbnb_kc)
                                ToastSuccess("Xóa thành công!", () => {
                                    this.setState({ dataSource, ma_vbnb_kc: 0 })
                                    API.Login.AU_DSNVCB(global.ma_ctcb_kc, this.props.dispatch)
                                })
                            } else {
                                Toast.show({ text: "Xảy ra lỗi. Vui lòng thực hiện lại sau!", type: "danger", buttonText: "Đóng", position: "top", duration: 2000 })
                            }
                        })
                    }
                }
            ],
            { cancelable: false }
        )
    }

    renderVBNB_Details_iPad = () => {
        let { ma_vbnb_kc, ma_van_ban_noi_bo_gui_kc } = this.state
        if (ma_vbnb_kc !== 0) {
            return (
                <VBNB_Details_iPad
                    navigation={this.props.navigation}
                    ma_vbnb_kc={ma_vbnb_kc}
                    ma_van_ban_noi_bo_gui_kc={ma_van_ban_noi_bo_gui_kc}
                    onSuccess={this.onSuccess}
                    activeTab={this.activeTab}
                />
            )
        } else {
            return null
        }
    }

    render() {

        this.styles = StyleSheet.create({
            CardItem: {
                flex: 1,
                borderTopWidth: 0,
                borderBottomWidth: 0.55,
                borderLeftWidth: 0,
                borderRightWidth: 0,
                borderBottomColor: "lightgray",
                paddingLeft: 8,
                paddingTop: 4,
                paddingBottom: 4,
                alignItems: 'center',
                justifyContent: "center",
                marginBottom: 1,
            },
            colTieuDe: {
                width: "70%",
                justifyContent: "center"
            },
            textBold: {
                color: "#000000",
                fontSize: this.props.fontSize.FontSizeNorman,
                fontWeight: "bold",
                paddingBottom: AppConfig.defaultPadding
            },
            textNoBold: {
                color: "#000000",
                fontSize: this.props.fontSize.FontSizeNorman,
                paddingBottom: AppConfig.defaultPadding
            },
            txtNgayNhan: {
                width: 100,
                color: "#808080",
                fontSize: this.props.fontSize.FontSizeSmall,
                textAlign: "center",
                paddingBottom: AppConfig.defaultPadding
            },
            txtNoiDung: {
                color: "#808080",
                fontSize: this.props.fontSize.FontSizeSmall,
                paddingBottom: AppConfig.defaultPadding
            },
            colNgayGui: {
                width: "30%",
                justifyContent: "center",
                alignItems: "flex-end"
            },
            menuTrigger: {
                width: 40,
                height: 30,
                justifyContent: "center",
                alignItems: 'center'
            },
            menuTriggerIcon: {
                fontSize: this.props.fontSize.FontSizeXXXLarge,
                color: "#B0BEC5"
            },
            menuOptionsContainer: {
                width: 200
            },
            menuOptions: {
                borderBottomColor: AppConfig.defaultLineColor,
                borderBottomWidth: 0.5
            },
            menuOptionsText: {
                color: AppConfig.headerBackgroundColor,
                fontSize: this.props.fontSize.FontSizeNorman,
                paddingTop: AppConfig.defaultPadding,
                paddingBottom: AppConfig.defaultPadding
            },
            menuOptionsTextDelete: {
                color: "red",
                fontSize: this.props.fontSize.FontSizeNorman,
                paddingTop: AppConfig.defaultPadding,
                paddingBottom: AppConfig.defaultPadding
            }
        })

        let nv_danhan = 0
        try {
            let cb_dsnvcb = this.props.cb_dsnvcb
            if (checkVersionWeb(21)) {
                nv_danhan = getNVNew("vbnb", "van-ban-noi-bo/van-ban-noi-bo-da-nhan?page=1", cb_dsnvcb)
            } else {
                nv_danhan = getNVOld("van-ban-noi-bo-da-nhan?page", "", cb_dsnvcb)
            }
        } catch (error) {
            console.log("nv_vbnb_danhan error")
        }

        console.log(cb_dsnvcb)

        if (Platform.OS === "ios" && Platform.isPad) {
            return (
                <Container style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
                    <HeaderWithLeft
                        title="Văn bản nội bộ"
                        buttonName="menu"
                        onPressLeftButton={_ => { this.props.navigation.dispatch(DrawerActions.toggleDrawer()) }}
                    />
                    <View style={{ flex: 1 }}>
                        <MyTab
                            values={["   Đã nhận   ", "   Đã gửi   "]}
                            badges={[nv_danhan, 0]}
                            activeTab={this.activeTab}
                            onChangeTab={(index) => { this.onChangeTab(index) }}
                            fontSize={this.props.fontSize}
                        />
                        {
                            (this.activeTab === 0) && (
                                <MyTab
                                    values={["Chưa xem", "Đã xem"]}
                                    badges={[0, 0]}
                                    activeTab={this.activeTabVBNBDN}
                                    onChangeTab={(index) => { this.onChangeTabVBNBDN(index) }}
                                    fontSize={this.props.fontSize}
                                />
                            )
                        }
                        <SearchHeader
                            ref={(ref) => { this.searchHeader = ref }}
                            onSearchData={this.onSearchData}
                            fontSize={this.props.fontSize}
                        />
                        {
                            (this.state.loading) ? (
                                <Spinner color={AppConfig.blueBackground} />
                            ) : (
                                    <CardItem style={{ flex: 1, flexDirection: "row", paddingLeft: 5, paddingRight: 5 }}>
                                        <View style={{ width: 200 }}>
                                            <FlatList
                                                ref={(ref) => { this.flatListRef = ref }}
                                                data={this.state.dataSource}
                                                keyExtractor={(item, index) => "key" + item.ma_vbnb_kc + index}
                                                contentContainerStyle={{ paddingBottom: 0 }}
                                                renderItem={({ item }) => {
                                                    return (
                                                        <VBNBTab_Item
                                                            activeTab={this.activeTab}
                                                            item={item}
                                                            navigation={this.props.navigation}
                                                            onForwardItem={this.onForwardItem}
                                                            onDeleteItemNhan={this.onDeleteItemNhan}
                                                            onDeleteItemGui={this.onDeleteItemGui}
                                                            cur_ma_vbnb_kc={this.state.ma_vbnb_kc}
                                                            onPress={() => {
                                                                this.setState({
                                                                    ma_vbnb_kc: item.ma_vbnb_kc,
                                                                    ma_van_ban_noi_bo_gui_kc: item.ma_van_ban_noi_bo_gui_kc
                                                                })
                                                            }}
                                                            fontSize={this.props.fontSize}
                                                            styles={this.styles}
                                                        />
                                                    )
                                                }}
                                                ListEmptyComponent={() => {
                                                    return (
                                                        <View style={{ alignItems: "center", marginTop: 10, }}>
                                                            <Text style={{ fontSize: this.props.fontSize.FontSizeNorman, }}>Không có dữ liệu</Text>
                                                        </View>
                                                    )
                                                }}
                                                onRefresh={this.onRefresh}
                                                refreshing={this.state.refreshing}
                                                onEndReached={this.loadMore}
                                                onEndReachedThreshold={2}
                                                removeClippedSubviews={true}
                                            />
                                        </View>
                                        <View style={{ flex: 1 }}>
                                            {this.renderVBNB_Details_iPad()}
                                        </View>
                                    </CardItem>
                                )
                        }
                    </View>
                    <FooterTab tabActive="vbnb" navigation={this.props.navigation} />
                </Container>
            );
        } else {
            return (
                <Container style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
                    <HeaderWithLeft
                        title="Văn bản nội bộ"
                        buttonName="menu"
                        onPressLeftButton={_ => { this.props.navigation.dispatch(DrawerActions.toggleDrawer()) }}
                    />
                    <View style={{ flex: 1 }}>
                        <MyTab
                            values={["   Đã nhận   ", "   Đã gửi   "]}
                            badges={[nv_danhan, 0]}
                            activeTab={this.activeTab}
                            onChangeTab={(index) => { this.onChangeTab(index) }}
                            fontSize={this.props.fontSize}
                        />
                        {
                            (this.activeTab === 0) && (
                                <MyTab
                                    values={["Chưa xem", "Đã xem"]}
                                    badges={[0, 0]}
                                    activeTab={this.activeTabVBNBDN}
                                    onChangeTab={(index) => { this.onChangeTabVBNBDN(index) }}
                                    fontSize={this.props.fontSize}
                                />
                            )
                        }
                        <SearchHeader
                            ref={(ref) => { this.searchHeader = ref }}
                            onSearchData={this.onSearchData}
                            fontSize={this.props.fontSize}
                        />
                        {
                            (this.state.loading) ? (
                                <View><Spinner color={AppConfig.blueBackground} /></View>
                            ) : (
                                    <FlatList
                                        ref={(ref) => { this.flatListRef = ref }}
                                        data={this.state.dataSource}
                                        keyExtractor={(item, index) => "key" + item.ma_vbnb_kc + index}
                                        contentContainerStyle={{ paddingBottom: 0 }}
                                        renderItem={({ item }) => {
                                            return (
                                                <VBNBTab_Item
                                                    activeTab={this.activeTab}
                                                    item={item}
                                                    navigation={this.props.navigation}
                                                    onForwardItem={this.onForwardItem}
                                                    onDeleteItemNhan={this.onDeleteItemNhan}
                                                    onDeleteItemGui={this.onDeleteItemGui}
                                                    cur_ma_vbnb_kc={this.state.ma_vbnb_kc}
                                                    fontSize={this.props.fontSize}
                                                    styles={this.styles}
                                                />
                                            )
                                        }}
                                        ListEmptyComponent={() => {
                                            return (
                                                <View style={{ alignItems: "center", marginTop: 10, }}>
                                                    <Text style={{ fontSize: this.props.fontSize.FontSizeNorman, }}>Không có dữ liệu</Text>
                                                </View>
                                            )
                                        }}
                                        onRefresh={this.onRefresh}
                                        refreshing={this.state.refreshing}
                                        onEndReached={this.loadMore}
                                        onEndReachedThreshold={5}
                                        removeClippedSubviews={true}
                                    />
                                )
                        }
                    </View>
                    <FooterTab tabActive="vbnb" navigation={this.props.navigation} />
                </Container>
            );
        }
    }
}

function mapStateToProps(state) {
    return {
        cb_dsnvcb: state.cb_dsnvcb.data,
        fontSize: state.fontSize
    }
}

export default connect(mapStateToProps)(VBNBScreen)