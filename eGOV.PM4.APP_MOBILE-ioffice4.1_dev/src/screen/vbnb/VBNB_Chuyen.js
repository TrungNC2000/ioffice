import React, { Component } from 'react';
import { Alert, View, StyleSheet, Dimensions, Modal, FlatList, PixelRatio } from "react-native"
import { Container, Text, Card, CardItem, Tab, Tabs, Toast, Textarea, Left, CheckBox, Spinner, Item, Button, Icon, } from "native-base"
import { connect } from "react-redux"
import { StackActions } from "react-navigation"
import API from "../../networks"
import { AppConfig } from "../../AppConfig"
import HeaderWithLeftRight from "../../components/Header/HeaderWithLeftRight"
import TreeChuyenCDTT from "../../components/treeChuyenCDTT"
import TreeChuyenNCB from "../../components/treeChuyenNCB"
import { mergeArrayDSChuyen, listToTree, resetStack } from "../../untils/TextUntils"

class ModalNguoiNhan extends Component {
    render() {
        this.styles = this.props.styles
        return (
            <Modal
                transparent={false}
                animationType="slide"
                visible={this.props.isModalVisible}
                supportedOrientations={["landscape", "landscape-left", "landscape-right", "portrait", "portrait-upside-down"]}
            >
                <View style={{ flex: 1, padding: 15, backgroundColor: AppConfig.grayBackground }}>
                    <View style={{ flex: 1, backgroundColor: "#FFFFFF", borderRadius: 10 }}>
                        <View style={{ position: 'absolute', zIndex: 99, height: 55, left: 0, right: 0, top: 0, justifyContent: "center", padding: 4, alignItems: "center" }}>
                            <Text style={{ fontSize: this.props.fontSize.FontSizeLarge }}>Danh sách cán bộ nhận</Text>
                        </View>
                        <FlatList
                            ref={(ref) => { this.flatListRef = ref }}
                            data={this.props.listMCB_DVCB}
                            keyExtractor={(item, index) => "key" + item.ma + index}
                            style={{ position: 'absolute', left: 0, right: 0, top: 55, bottom: 55 }}
                            initialNumToRender={50}
                            renderItem={({ item, index }) => {
                                return (
                                    <Item key={item.ma} style={{ padding: 12, borderBottomWidth: AppConfig.defaultLineWidth, borderColor: AppConfig.defaultLineColor }}>
                                        <Left>
                                            <Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>{item.ten}</Text>
                                            <Text note style={{ fontSize: this.props.fontSize.FontSizeSmall }}>Chức vụ: {item.ten_chuc_vu}</Text>
                                        </Left>
                                    </Item>
                                )
                            }}
                            ListEmptyComponent={() => {
                                return (
                                    <View style={{ alignItems: "center", marginTop: 10, }}>
                                        <Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>Không có dữ liệu</Text>
                                    </View>
                                )
                            }}
                            removeClippedSubviews={true}
                        />
                        {
                            (this.props.isSending) ? (
                                <View style={{ position: 'absolute', zIndex: 99, height: 55, left: 0, right: 0, bottom: 0, flexDirection: "row", justifyContent: "center", paddingTop: 10, alignItems: "center" }}>
                                    <Spinner />
                                    <Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}> Đang thực hiện</Text>
                                </View>
                            ) : (
                                    <View style={{ position: 'absolute', zIndex: 99, height: 55, left: 0, right: 0, bottom: 0, flexDirection: "row", justifyContent: "space-evenly", paddingTop: 10, alignItems: "center" }}>
                                        <Button small bordered rounded iconLeft onPress={_ => this.props.onPressSend()}><Icon name="done" type="MaterialIcons"></Icon><Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>Gửi</Text></Button>
                                        <Button small danger bordered rounded iconLeft onPress={_ => this.props.toggleModal()}><Icon name="close" type="MaterialIcons"></Icon><Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>Chọn lại</Text></Button>
                                    </View>
                                )
                        }
                    </View>
                </View>
            </Modal>
        )
    }
}

class VBNB_Chuyen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            dataTreeDVCB: [],
            dataTreeNCB: [],
            selectedItem: [],
            isLoading: true,
            isSending: false,
            yKienXuLy: '',
            checkSms: false,
            checkEmail: false,
            isModalVisible: false,
        }
        this.listMCB_DVCB = []
        this.listMCB = []
        this.listMCB_NCB = []
        this.currentTab = 0
        this.hasChangeTab = false
        Dimensions.addEventListener("change", this.onResizeScreen)
        this.ma_vbnb_kc = 0
        this.trang_thai_xu_ly = 0
        this.ma_vbnb_gui_cha = 0
        this.trich_yeu = ""
    }

    componentDidMount = () => {
        this.ma_vbnb_kc = this.props.navigation.getParam('ma_vbnb_kc')
        this.trang_thai_xu_ly = this.props.navigation.getParam('trang_thai_xu_ly')
        this.ma_vbnb_gui_cha = this.props.navigation.getParam('ma_vbnb_gui_cha')
        this.trich_yeu = this.props.navigation.getParam('trich_yeu')
        this.getDSDVCB()
        this.getDSNCB()
    }

    getDSDVCB = () => {
        const ma_don_vi = global.ma_don_vi_quan_tri
        API.VanBanNoiBo.AU_VBNB_DSCBNVBNB(ma_don_vi).then((dataTreeDVCB) => {
            this.setState({ dataTreeDVCB, isLoading: false })
        })
    }

    getDSNCB = () => {
        API.VanBanDen.AU_VBDEN_DSNCBNVBDLDD(ma_can_bo).then((dataTreeNCB) => {
            let tree = listToTree(dataTreeNCB, {
                idKey: "id",
                parentKey: "parent"
            })
            this.setState({ dataTreeNCB: tree })
        })
    }

    componentWillUnmount() {
        Dimensions.removeEventListener("change", this.onResizeScreen)
    }

    onResizeScreen = () => {
        setTimeout(() => {
            this.hasChangeTab = false
        }, 500);
    }

    onCheckSms = () => {
        this.setState({ checkSms: !this.state.checkSms })
    }

    onCheckEmail = () => {
        this.setState({ checkEmail: !this.state.checkEmail })
    }

    toggleModal = () => {
        this.listMCB_DVCB = []
        if (!this.state.isModalVisible) {
            let listTab1 = []
            let listTab2 = []

            try {
                listTab1 = this.treeChuyenCDTT.getDataCanBo_XLC_PH()
            } catch (error) {
                console.log("Error treeChuyenCDTT.getDataCanBo_XLC_PH: " + error)
            }

            try {
                listTab2 = this.treeChuyenNCB.getDataCanBo_XLC_PH()
            } catch (error) {
                console.log("Error treeChuyenNCB.getDataCanBo_XLC_PH: " + error)
            }

            this.listMCB_DVCB = mergeArrayDSChuyen(listTab1, listTab2)
            if (this.listMCB_DVCB.length > 0) {
                this.setState({ isModalVisible: true })
            } else {
                Toast.show({ text: "Vui lòng chọn cán bộ nhận!", type: "warning", buttonText: "Đóng", position: "top", duration: 2000 })
            }
        } else {
            this.setState({ isModalVisible: false })
        }
    }

    onPressSend = () => {
        let chuoi_ma_ctcb_nhan1 = []
        this.listMCB_DVCB.forEach(element => {
            chuoi_ma_ctcb_nhan1.push(parseInt(element.ma.replace("CB", "")))
        });
        let regex = /,/gi;
        let chuoi_ma_ctcb_nhan = chuoi_ma_ctcb_nhan1.join().replace(regex, ';')

        let ma_vbnb_kc = this.ma_vbnb_kc;
        let yKienXuLy = this.state.yKienXuLy;
        const ma_ctcb_gui = global.ma_ctcb_kc; //fixed 30/07/2019 by Quan vo
        let trang_thai_xu_ly = this.trang_thai_xu_ly;
        let ma_vbnb_gui_cha = this.ma_vbnb_gui_cha;
        let sms = this.state.checkSms ? 1 : 0
        this.setState({ isSending: true })
        API.VanBanNoiBo.AU_VBNB_CVBNB(ma_vbnb_kc, ma_ctcb_gui, chuoi_ma_ctcb_nhan, yKienXuLy, trang_thai_xu_ly, sms, ma_vbnb_gui_cha).then((response) => {
            if (response !== "") {
                API.Login.AU_DSNVCB(global.ma_ctcb_kc, this.props.dispatch)
                if (sms === 1) {
                    let arrSDT = []
                    for (let item of this.listMCB_DVCB) {
                        if (item.di_dong_can_bo) {
                            arrSDT.push(item.di_dong_can_bo)
                        }
                    }
                    if (arrSDT.length > 0) {
                        let noi_dung = this.trich_yeu
                        let chuoi_di_dong_nhan = arrSDT.join(",")
                        API.NhanTinSMS.AU_SMS_GTN(noi_dung, chuoi_di_dong_nhan)
                    }
                }
                API.Login.AU_PUSH_FCM_APP(chuoi_ma_ctcb_nhan1.join(","), "vanbannoibodanhan", this.trich_yeu, 0, response, 0, 0)
                Alert.alert("Thông báo", "Chuyển văn bản thành công", [{ text: "Đóng", onPress: () => { this.props.navigation.dispatch(resetStack("VBNB")) }, style: "cancel" }], { cancelable: false })
            } else {
                Alert.alert("Thông báo", "Chuyển văn bản thất bại. Vui lòng thử lại sau!")
                this.setState({ isSending: false })
            }
        })
    }

    render() {

        this.styles = StyleSheet.create({
            scrollContainer: {
                flex: 1,
                marginTop: 10,
                marginLeft: 10,
                marginRight: 10
            },
            CardItem: {
                padding: 10,
                alignItems: 'center'
            },
            CardItemLeft: {
                width: 90
            },
            CardItemRight: {
                flex: 1
            },
            TextTitle: {
                fontSize: this.props.fontSize.FontSizeSmall,
            },
            TextInput: {
                flex: 1,
                fontFamily: AppConfig.fontFamily,
                fontSize: this.props.fontSize.FontSizeNorman / PixelRatio.getFontScale(),
                color: "#000000",
                borderColor: AppConfig.defaultLineColor,
                borderWidth: AppConfig.defaultLineWidth,
                borderRadius: 5,
                marginRight: 2,
                paddingLeft: 5,
                paddingTop: 7,
                minHeight: 40,
            },
            tabStyle: {
                flex: 1,
                backgroundColor: AppConfig.blueBackground
            },
            activeTabStyle: {
                backgroundColor: AppConfig.blueBackground
            },
            textStyle: {
                fontSize: this.props.fontSize.FontSizeNorman,
                color: "#FFFFFF",
                opacity: 0.6,
            },
            activeTextStyle: {
                fontSize: this.props.fontSize.FontSizeNorman,
                color: "#FFFFFF",
                opacity: 1,
            }
        })

        const popAction = StackActions.pop({
            n: 1
        })
        return (
            <Container style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
                <HeaderWithLeftRight
                    title="Chuyển văn bản nội bộ"
                    buttonLeftName="arrow-back"
                    buttonRightName="send"
                    onPressLeftButton={() => { this.props.navigation.dispatch(popAction) }}
                    onPressRightButton={this.toggleModal}
                />
                {
                    (this.state.isModalVisible) && (
                        <ModalNguoiNhan
                            listMCB_DVCB={this.listMCB_DVCB}
                            isModalVisible={this.state.isModalVisible}
                            toggleModal={this.toggleModal}
                            onPressSend={this.onPressSend}
                            isSending={this.state.isSending}
                            fontSize={this.props.fontSize}
                            styles={this.styles}
                        />
                    )
                }
                <CardItem style={this.styles.CardItem}>
                    <CardItem style={[this.styles.CardItemLeft]}>
                        <Text style={[this.styles.TextTitle]}>Ý kiến xử lý</Text>
                    </CardItem>
                    <CardItem style={[this.styles.CardItemRight]}>
                        <Textarea
                            autoCorrect={false}
                            style={this.styles.TextInput}
                            rowSpan={3}
                            value={this.state.yKienXuLy}
                            onChangeText={(text) => { this.setState({ yKienXuLy: text }) }}
                        />
                    </CardItem>
                </CardItem>
                <CardItem style={this.styles.CardItem}>
                    <CardItem style={[this.styles.CardItemLeft]}></CardItem>
                    <CardItem style={[this.styles.CardItemRight, { marginLeft: -10 }]}>
                        <CheckBox onPress={this.onCheckSms} checked={this.state.checkSms} color={AppConfig.blueBackground}></CheckBox>
                        <Text style={[this.styles.TextTitle, { marginLeft: 20 }]}>Sms        </Text>
                    </CardItem>
                </CardItem>
                <View style={{ flex: 1 }}>
                    <Tabs onChangeTab={() => { this.hasChangeTab = true }}>
                        <Tab
                            tabStyle={this.styles.tabStyle}
                            activeTabStyle={this.styles.activeTabStyle}
                            textStyle={this.styles.textStyle}
                            activeTextStyle={this.styles.activeTextStyle}
                            heading="Đơn vị/Cán bộ"
                        >
                            <Card noShadow style={{ flex: 1, borderWidth: 1 }}>
                                {
                                    (this.state.isLoading) && (
                                        <Spinner />
                                    )
                                }
                                {
                                    (!this.state.isLoading) && (
                                        <TreeChuyenCDTT
                                            ref={tree => { this.treeChuyenCDTT = tree }}
                                            title="Họ tên"
                                            firstChkTitle="Chọn"
                                            secondChkTitle="PH"
                                            threeChkTitle="XLC"
                                            numberSelection={1}
                                            data={this.state.dataTreeDVCB}
                                            primaryCheck={1}
                                            onItemPress={(ma, once) => { console.log("press item: " + ma) }}
                                            fontSize={this.props.fontSize}
                                        />
                                    )
                                }
                            </Card>
                        </Tab>
                        <Tab
                            tabStyle={this.styles.tabStyle}
                            activeTabStyle={this.styles.activeTabStyle}
                            textStyle={this.styles.textStyle}
                            activeTextStyle={this.styles.activeTextStyle}
                            heading="Nhóm cán bộ"
                        >
                            <Card noShadow style={{ flex: 1, borderWidth: 1 }}>
                                <TreeChuyenNCB
                                    ref={treeNCB => { this.treeChuyenNCB = treeNCB }}
                                    title="Họ tên"
                                    firstChkTitle="Chọn"
                                    secondChkTitle="PH"
                                    threeChkTitle="XLC"
                                    numberSelection={1}
                                    data={this.state.dataTreeNCB}
                                    primaryCheck={1}
                                    onItemPress={(ma, once) => { console.log("press item: " + ma) }}
                                    fontSize={this.props.fontSize}
                                />
                            </Card>
                        </Tab>
                    </Tabs>
                </View>
            </Container>
        );
    }
}

function mapStateToProps(state) {
    return {
        fontSize: state.fontSize,
    }
}

export default connect(mapStateToProps)(VBNB_Chuyen)