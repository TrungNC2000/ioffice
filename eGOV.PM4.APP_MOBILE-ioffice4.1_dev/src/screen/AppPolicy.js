import React, { Component } from 'react';
import { View, StyleSheet } from "react-native"
import { StackActions } from "react-navigation"
import { Container, Icon, Text, Header } from "native-base"
import { WebView } from 'react-native-webview';
import { connect } from "react-redux"
import { Grid, Col } from "react-native-easy-grid"
import { AppConfig } from '../AppConfig'

class AppPolicy extends Component {
    render() {

        this.styles = StyleSheet.create({
            container: {
                flex: 1
            },
            viewHeader: {
                position: 'absolute',
                zIndex: 99,
                height: 58,
                left: 0,
                right: 0,
                top: 0
            },
            hearder: {
            },
            grid: {
                flex: 1,
                justifyContent: "center",
                alignItems: "center"
            },
            colLeft: {
                width: "10%",
                paddingLeft: 10
            },
            colMiddle: {
                justifyContent: "center",
                alignItems: "center"
            },
            colRight: {
                width: "15%",
                paddingRight: 10,
                alignItems: "flex-end"
            },
            viewWebview: {
                flex: 1,
                marginTop: 30
            },
            webView: {
                flex: 1
            },
            headerTitleStyle: {
                color: AppConfig.defaultTextColor,
                fontSize: this.props.fontSize.FontSizeLarge
            },
            headerIconStyle: {
                color: AppConfig.defaultTextColor
            },
        })

        const popAction = StackActions.pop({ n: 1 })
        return (
            <Container style={this.styles.container}>
                <View style={this.styles.viewHeader}>
                    <Header hasTabs style={this.styles.hearder} androidStatusBarColor={AppConfig.statusBarColor} iosBarStyle={"light-content"}>
                        <Grid style={this.styles.grid}>
                            <Col style={this.styles.colLeft}>
                                <Icon name={"arrow-back"} type="MaterialIcons" onPress={() => this.props.navigation.dispatch(popAction)} style={this.styles.headerIconStyle} />
                            </Col>
                            <Col style={this.styles.colMiddle}>
                                <Text style={this.styles.headerTitleStyle}>Điều khoản sử dụng</Text>
                            </Col>
                            <Col style={this.styles.colRight}>
                            </Col>
                        </Grid>
                    </Header>
                </View>
                <View style={this.styles.viewWebview}>
                    <WebView
                        ref={WebView => { this.WebView = WebView }}
                        style={this.styles.webView}
                        source={{ uri: "https://iofficev3notify.vnptioffice.vn/AppPolicy/ioffice41/privacy_policy.html" }}
                        javaScriptEnabled={true}
                        domStorageEnabled={true}
                        decelerationRate="normal"
                        mixedContentMode="always"
                        originWhitelist={["*"]}
                        startInLoadingState={true}
                        automaticallyAdjustContentInsets={true}
                        scalesPageToFit={true}
                        bounces={false}
                        onLoad={_ => { }}
                        onError={_ => { console.log("error") }}
                    />
                </View>
            </Container>
        )
    }
}

function mapStateToProps(state) {
    return {
        fontSize: state.fontSize,
    }
}

export default connect(mapStateToProps)(AppPolicy)