import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity } from "react-native"
import { Container, Toast, Text, CardItem, ActionSheet, Separator, Content } from "native-base"
import { connect } from "react-redux"
import { DrawerActions } from "react-navigation"
import { AppConfig } from "../AppConfig"
import HeaderWithLeftRight from "../components/Header/HeaderWithLeftRight"
import { checkVersionWeb, ReplaceAll, setFontSize } from "../untils/TextUntils"
import { CheckSession } from "../untils/NetInfo"
import API from "../networks"

class UserSettings extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listTS: []
        }
    }

    componentDidMount = () => {
        this.didBlurSubscription = this.props.navigation.addListener('didFocus', payload => { CheckSession(this.props.navigation) })
        this.getSettings()
    };

    componentWillUnmount = () => {
        this.didBlurSubscription.remove()
    };

    getSettings = async () => {
        API.Login.AU_GTSM().then((listTS) => {
            this.setState({ listTS })
        })
    }

    onPressSave = () => {
        let listTS = this.state.listTS
        let arr_chuoimathamso = []
        let arr_chuoigiatri = []
        for (let item of listTS) {
            arr_chuoimathamso.push(item.ma_tham_so)
            arr_chuoigiatri.push(item.gia_tri_tham_so)
        }
        if (arr_chuoimathamso.length > 0 && arr_chuoimathamso.length === arr_chuoigiatri.length) {
            API.Login.AU_STSM(arr_chuoimathamso.join(";"), arr_chuoigiatri.join(";")).then((response) => {
                if (response) {
                    for (let i = 0; i < arr_chuoimathamso.length; i++) {
                        global[arr_chuoimathamso[i]] = arr_chuoigiatri[i]
                        if (arr_chuoimathamso[i] === "US_Type_Font_Size") {
                            setFontSize(arr_chuoigiatri[i], this.props.dispatch)
                        }
                    }
                    Toast.show({
                        text: "Lưu cấu hình thành công!", type: "success", buttonText: "Đóng", position: "top", duration: 1500, onClose: () => {
                            this.componentDidMount()
                        }
                    })
                } else {
                    Toast.show({ text: "Có lỗi trong quá trình lưu!", type: "danger", buttonText: "Đóng", position: "top", duration: 1500 })
                }
            })
        } else {
            Toast.show({ text: "Có lỗi trong quá trình lưu!", type: "danger", buttonText: "Đóng", position: "top", duration: 1500 })
        }
    }


    onPressItem = (name) => {
        let arrTemp = this.state.listTS.filter(el => el.ma_tham_so === name)
        if (arrTemp.length > 0) {
            let { mo_ta_tham_so } = arrTemp[0]
            mo_ta_tham_so = ReplaceAll(mo_ta_tham_so, String.fromCharCode(39), String.fromCharCode(34))
            mo_ta_tham_so = JSON.parse(mo_ta_tham_so)
            if (name === "US_First_Load_Page" && global.lanh_dao !== 1) {
                mo_ta_tham_so = mo_ta_tham_so.filter(el => el.Value !== "VBDenDuyet")
                mo_ta_tham_so = mo_ta_tham_so.filter(el => el.Value !== "VBDiDuyet")
            }
            ActionSheet.show(
                {
                    options: mo_ta_tham_so.map(obj => {
                        return obj.Text
                    }),
                    title: "Vui lòng chọn cấu hình"
                },
                buttonIndex => {
                    if (buttonIndex !== undefined) {
                        let listTS = this.state.listTS
                        let listTSNEW = []
                        for (let item of listTS) {
                            if (item.ma_tham_so === name) {
                                item.gia_tri_tham_so = mo_ta_tham_so[buttonIndex].Value
                            }
                            listTSNEW.push(item)
                        }

                        this.setState({ listTS: listTSNEW })
                    }
                }
            )
        }
    }

    renderItem = (name) => {
        let arrTemp = this.state.listTS.filter(el => el.ma_tham_so === name)
        if (arrTemp.length > 0) {
            let { ma_tham_so, ten_tham_so, gia_tri_tham_so, mo_ta_tham_so } = arrTemp[0]
            mo_ta_tham_so = ReplaceAll(mo_ta_tham_so, String.fromCharCode(39), String.fromCharCode(34))
            mo_ta_tham_so = JSON.parse(mo_ta_tham_so)
            return (
                <CardItem style={[this.styles.CardItem]} key={ma_tham_so}>
                    <CardItem style={this.styles.CardItemBody}>
                        <Text style={[this.styles.textTieuDe]}>{ten_tham_so}</Text>
                    </CardItem>
                    <CardItem style={this.styles.CardItemRight}>
                        <TouchableOpacity onPress={() => this.onPressItem(name)} style={{ flex: 1 }}>
                            <Text editable={false} style={[this.styles.textNoiDung, { flex: 1, alignSelf: "flex-end" }]}>{mo_ta_tham_so.filter(el => el.Value === gia_tri_tham_so)[0].Text}</Text>
                        </TouchableOpacity>
                    </CardItem>
                </CardItem>
            )
        }
        return null
    }


    render() {

        this.styles = StyleSheet.create({
            CardItem: {
                borderBottomColor: "gray",
                borderBottomWidth: 0.35,
                paddingTop: 8,
                paddingBottom: 8,
            },
            textTieuDe: {
                fontSize: this.props.fontSize.FontSizeNorman,
            },
            textNoiDung: {
                fontSize: this.props.fontSize.FontSizeSmall,

            },
            CardItemLeft: {
                width: 150
            },
            CardItemBody: {
                flex: 1,
                alignItems: "center"
            },
            CardItemRight: {
                backgroundColor: "transparent",
                width: 200,
                position: 'absolute',
                zIndex: 99,
                right: 5,
                justifyContent: "center",
                alignItems: "flex-end"
            },
            TextTitle: {
                fontSize: this.props.fontSize.FontSizeSmall,
            },
            TextInput: {
                flex: 1,
                fontFamily: AppConfig.fontFamily,
                fontSize: this.props.fontSize.FontSizeNorman,
                color: "#000000",
                borderColor: AppConfig.defaultLineColor,
                borderWidth: AppConfig.defaultLineWidth,
                borderRadius: 5,
                marginRight: 2,
                paddingRight: 30,
                minHeight: 40,
            },
            Icon: {
                flex: 1,
                color: AppConfig.blueBackground,
                fontSize: this.props.fontSize.FontSizeXLarge,
                padding: 5,
            }
        })

        let showConfigKySo = (global.lanh_dao === 1 && checkVersionWeb(16)) ? true : false
        return (
            <Container style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
                <HeaderWithLeftRight
                    title="Cài đặt"
                    buttonLeftName="menu"
                    onPressLeftButton={_ => { this.props.navigation.dispatch(DrawerActions.toggleDrawer()) }}
                    buttonRightName="save"
                    onPressRightButton={() => { this.onPressSave() }}
                />
                <Content>
                    <Separator bordered style={[this.styles.CardItem]}>
                        <Text style={this.styles.textTieuDe}>Hiển thị</Text>
                    </Separator>
                    {this.renderItem("US_First_Load_Page")}
                    {this.renderItem("US_Xem_Pdf_iOS")}
                    {this.renderItem("US_FILE_List_Ngang_Doc")}
                    {this.renderItem("US_Show_Alert_On_Success")}
                    {this.renderItem("US_Type_Font_Size")}
                    {
                        (showConfigKySo) && (
                            <Separator bordered style={[this.styles.CardItem]}>
                                <Text style={this.styles.textTieuDe}>Chữ ký số</Text>
                            </Separator>
                        )
                    }
                    {
                        (showConfigKySo) && (
                            this.renderItem("US_Mobile_Pki_Mode")
                        )
                    }
                    {
                        (showConfigKySo) && (
                            this.renderItem("US_Mobile_Pki_Side")
                        )
                    }
                    <Separator bordered style={[this.styles.CardItem]}>
                        <Text style={this.styles.textTieuDe}>Văn bản đến</Text>
                    </Separator>
                    {this.renderItem("US_VBDEN_Mot_Can_Bo_XLC")}
                    {this.renderItem("US_VBDEN_Tree_Default_Check")}
                    <Separator bordered style={[this.styles.CardItem]}>
                        <Text style={this.styles.textTieuDe}>Lịch công tác</Text>
                    </Separator>
                    {this.renderItem("US_Time_Local_Notify")}
                </Content>
            </Container>
        );
    }
}

function mapStateToProps(state) {
    return {
        fontSize: state.fontSize,
    }
}

export default connect(mapStateToProps)(UserSettings)