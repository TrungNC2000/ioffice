import React, { Component } from 'react';
import { AsyncStorage, Platform, ImageBackground, View, Alert, Linking, StyleSheet, Modal } from "react-native"
import { Container, Text, Spinner, Button } from "native-base"
import Orientation from 'react-native-orientation';
import DeviceInfo from "react-native-device-info"
import Splash from 'react-native-splash-screen'
import { WebView } from "react-native-webview"
import { connect } from "react-redux"
import API from "../networks"
import { setUserSettings } from "../untils/TextUntils"
import { AppConfig } from '../AppConfig';
import { URL_CONFIG } from "../networks/Config"

class SplashScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            modalActive: false,
            modalData: "",
            modalUrlData: "",
            modalTypeButton: 2,
            modalAction: "",
        }
        this.ma_ctcb_kc_Store = ""
    }

    async componentWillMount() {
        Orientation.lockToPortrait()
        this.callbackToLogin()
    }

    callbackToLogin = async () => {
        this.ma_ctcb_kc_Store = await AsyncStorage.getItem("ma_ctcb_kc_Store");
        const chkSaveLogin = await AsyncStorage.getItem("chkSaveLogin");
        const ssoAutoLogin = await AsyncStorage.getItem("chkSaveDeepLinkSso");
        if (chkSaveLogin === "true") {
            this.tryLoginAuto() 
        } else {
            if(ssoAutoLogin === "true"){
                this.tryLoginAuto();
            }else{
                this.props.navigation.navigate("Login")
            }
        }
    }

    getSettings = async () => {
        API.Login.AU_GTSM().then((listTS) => {
            this.setState({ listTS })
        })
    }

    componentWillUnmount = () => {
        Splash.hide();
        global.accessTokenHSM = {}
    };

    checkUpdateVersion = (callback) => {
        global.storeURL = Platform.OS == 'ios' ? AppConfig.iOSStoreURL : AppConfig.AndroidStoreURL
        var xhr = new XMLHttpRequest();
        xhr.withCredentials = true;

        xhr.open("GET", AppConfig.AppVersionCheckURL, true)
        xhr.onload = async function (e) {
            if (xhr.readyState === 4) {

                if (xhr.status === 200 && xhr.response) {
                    try {
                        let data = JSON.parse(xhr.response)
                        if (data.modalActive) {
                            this.setState({ modalData: data.modalData, modalTypeButton: data.modalTypeButton, modalUrlData: data.modalUrlData, modalAction: data.modalAction }, () => {
                                this.setState({ modalActive: data.modalActive })
                            })
                        } else {
                            callback()
                        }
                    } catch (error) {
                        callback()
                    }
                } else {
                    callback()
                }
            }
        }.bind(this)
        xhr.send(null);
    }

    tryLoginAuto = () => {
        API.Login.AU_DSDVA().then((listDonVi) => {
            API.Login.AU_DSCTCB1(this.props.dispatch, listDonVi).then(async (response) => {
                if (response) {
                    if (global.AU_ROOT.includes('nghean-api')) {
                        API.Login.AU_GETQXYKTT(global.refresh_token)
                    }
                    Orientation.unlockAllOrientations()
                    setUserSettings(this.props.navigation, this.props.dispatch)
                } else {
                    this.props.navigation.navigate("Login")
                }
            })
        })
    }

    renderButtonModal = () => {
        if (this.state.modalTypeButton === 2) {
            return (
                <View style={{ width: "100%", height: 60, flexDirection: "row", justifyContent: "center", alignItems: "center", alignSelf: "center", position: "absolute", bottom: 0 }}>
                    <Button style={{ alignSelf: "center", width: 100, marginRight: 5, justifyContent: "center" }} onPress={() => { this.setState({ modalActive: false }, () => { this.callbackToLogin() }) }}>
                        <Text>Đóng</Text>
                    </Button>
                    <Button style={{ alignSelf: "center", width: 100, marginLeft: 5, justifyContent: "center" }} onPress={() => { Linking.openURL(this.state.modalAction) }}>
                        <Text>Cập nhật</Text>
                    </Button>
                </View>
            )
        } else {
            return (
                <View style={{ width: "100%", height: 60, flexDirection: "row", justifyContent: "center", alignItems: "center", alignSelf: "center", position: "absolute", bottom: 0 }}>
                    <Button style={{ alignSelf: "center", width: 100, marginLeft: 5, justifyContent: "center" }} onPress={() => { Linking.openURL(this.state.modalAction) }}>
                        <Text>Cập nhật</Text>
                    </Button>
                </View>
            )
        }
    }

    render() {

        this.styles = StyleSheet.create({
            imageBackground: {
                flex: 1,
                width: null,
                height: null
            },
            viewFooter: {
                position: 'absolute',
                zIndex: 9999,
                height: 60,
                left: 0,
                right: 0,
                bottom: 0,
                flexDirection: "row",
                justifyContent: "center",
                paddingTop: 5,
                alignItems: "center"
            },
            txtLoading: {
                color: "white",
                fontSize: this.props.fontSize.FontSizeNorman
            }
        })

        let sourceImage = require("../assets/splash/launch_screen_xxxhdpi.png")
        return (
            <Container>
                <Modal
                    transparent={true}
                    animationType="slide"
                    visible={this.state.modalActive}
                    supportedOrientations={["landscape", "landscape-left", "landscape-right", "portrait", "portrait-upside-down"]}
                >
                    <View style={{ flex: 1, backgroundColor: "rgba(255,255,255,0.15)" }}>
                        <View style={{ flex: 1, margin: 30, padding: 5, borderRadius: 15, backgroundColor: "white", borderColor: "red" }}>
                            <View style={{ flex: 1, marginBottom: 60, borderColor: "blue" }}>
                                <WebView
                                    ref={(ref) => { this.myWebView = ref }}
                                    source={{ uri: this.state.modalUrlData }}
                                    domStorageEnabled={true}
                                    decelerationRate="normal"
                                    mixedContentMode="always"
                                    originWhitelist={["*"]}
                                    automaticallyAdjustContentInsets={true}
                                    scalesPageToFit={true}
                                    bounces={false}
                                />
                            </View>
                            {
                                this.renderButtonModal()
                            }
                        </View>
                    </View>
                </Modal>
                <ImageBackground style={this.styles.imageBackground} resizeMode="cover" source={sourceImage}>
                </ImageBackground>
                {
                    (!this.state.modalActive) && (
                        <View style={this.styles.viewFooter}>
                            <Spinner color={"white"} />
                            <Text style={this.styles.txtLoading}> Đang tải dữ liệu...</Text>
                        </View>
                    )
                }
            </Container>
        );
    }
}

function mapStateToProps(state) {
    return {
        fontSize: state.fontSize,
    }
}

export default connect(mapStateToProps)(SplashScreen)