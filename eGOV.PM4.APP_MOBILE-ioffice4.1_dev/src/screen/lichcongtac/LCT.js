import React, { Component } from 'react'
import { Container } from "native-base"
import { DrawerActions } from "react-navigation"
import { connect } from "react-redux"
import HeaderWithLeft from "../../components/Header/HeaderWithLeft"
import HeaderWithLeftRight from "../../components/Header/HeaderWithLeftRight"
import FooterTab from "../../components/Footer/FooterTab"
import LCT_CN from './LCT_CaNhan'
import LCT_DV from './LCT_DonVi'
import LCT_CQ from './LCT_CoQuan'
import LCT_LanhDao from './LCT_LanhDao'
import MyTab from "../../components/Tab"
import { CheckSession } from "../../untils/NetInfo"

class LCTScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            activeTab: 1
        }
    }

    componentDidMount = () => {
        this.didBlurSubscription = this.props.navigation.addListener('didFocus', payload => { CheckSession(this.props.navigation) })
    }

    componentWillUnmount = () => {
        this.didBlurSubscription.remove()
    }

    onChangeTab = (index) => {
        this.setState({ activeTab: index })
    }

    renderList = () => {
        let { activeTab } = this.state
        if (activeTab == 0) {
            return (global.lct_co_quan_hien_thi_ds_co_quan === -1 ? <ActivityIndicator /> : <LCT_LanhDao />)
        } else if (activeTab == 1) {
            return (<LCT_CQ navigation={this.props.navigation} />)
        } else if (activeTab == 2) {
            return (<LCT_DV navigation={this.props.navigation} />)
        } else {
            return (<LCT_CN navigation={this.props.navigation} />)
        }
    }


    render() {
        return (
            <Container style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
                {
                    (global.lanh_dao === 1) ? (
                        <HeaderWithLeftRight
                            title="Lịch công tác"
                            buttonLeftName="menu"
                            onPressLeftButton={() => { this.props.navigation.dispatch(DrawerActions.toggleDrawer()) }}
                            buttonRightName="create"
                            onPressRightButton={() => { this.props.navigation.navigate("LCT_CoQuan_Duyet") }}
                        />
                    ) : (
                            <HeaderWithLeft
                                title="Lịch công tác"
                                buttonName="menu"
                                onPressLeftButton={_ => { this.props.navigation.dispatch(DrawerActions.toggleDrawer()) }}
                            />
                        )
                }
                <MyTab
                    values={["Lãnh đạo", "Cơ quan", "Đơn vị", "Cá nhân"]}
                    badges={[0, 0, 0, 0]}
                    activeTab={this.state.activeTab}
                    onChangeTab={this.onChangeTab}
                    fontSize={this.props.fontSize}
                />
                {this.renderList()}
                <FooterTab tabActive="lct" navigation={this.props.navigation} />
            </Container>
        )
    }
}

function mapStateToProps(state) {
    return {
        fontSize: state.fontSize,
    }
}

export default connect(mapStateToProps)(LCTScreen)