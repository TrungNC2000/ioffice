import React, { Component } from "react"
import { View, FlatList, TouchableOpacity, Image } from "react-native"
import { Container, ActionSheet, CardItem, Text, Spinner, Card, Icon, CheckBox, Toast } from "native-base"
import { StackActions } from "react-navigation"
import HeaderWithLeftRight from "../../components/Header/HeaderWithLeftRight"
import { connect } from "react-redux"
import { AppConfig } from "../../AppConfig"
import API from "../../networks"
import ViewFile from "../../components/ViewFile"
import { get_date_from_week, get_arrdate_from_week, getWeekNumber, groupBy, RemoveHTMLTag, ToastSuccess, resetStack } from "../../untils/TextUntils"

class LCT_CoQuan_Duyet extends Component {
    constructor(props) {
        super(props)
        this.state = {
            date_show: "",
            current_week: 0,
            current_year: 0,
            title_date: "",
            listDSCot: [],
            dataLCT: [],
            arrCheck: [],
            listCoQuan: [],
            madv: global.ma_don_vi_quan_tri,
            tendv: global.ten_don_vi_quan_tri,
            isLoading: false,
        }
        this.arrAllCheck = []
    }

    componentDidMount() {
        this.getDanhSachDonVi()
        this.load_data()
    }

    getDanhSachDonVi = () => {
        let ma_don_vi_quan_tri = global.ma_don_vi_quan_tri
        API.LichCongTac.AU_LCT_DSDVDDC(ma_don_vi_quan_tri).then((listCoQuan) => {
            this.setState({ listCoQuan })
        })
    }

    load_data = () => {
        let current_date = new Date()
        let arrDay = getWeekNumber(current_date)
        let current_week = arrDay[0]
        let current_year = arrDay[1]
        let title_date = "Tuần " + current_week + " năm " + current_year
        let date_show = get_date_from_week(current_week, current_year)
        this.setState({ current_week, current_year, title_date, date_show }, () => {
            this.getLCTChiTiet()
        });
    }

    getLCTChiTiet = () => {
        let { madv, current_week, current_year } = this.state
        this.setState({ isLoading: true })
        API.LichCongTac.AU_LCT_DSLCTDVQT(madv, current_year).then(async (arrMaLCT) => {
            if (arrMaLCT.length > 0) {
                let arrTemp = arrMaLCT.filter(el => el.tuan === current_week)
                if (arrTemp.length > 0) {
                    let ma_lich_cong_tac = arrTemp[0].ma_lich_cong_tac_kc
                    let dataLCT = await API.LichCongTac.AU_LCT_LLCTCT(ma_lich_cong_tac)
                    let dataLCT_Temp = Object.entries(groupBy(dataLCT, "ngay_thuc_hien_vn"))
                    let listDSCot = await API.LichCongTac.AU_LCT_DSCBLCT(ma_lich_cong_tac)
                    this.setState({ dataLCT: dataLCT_Temp, listDSCot, isLoading: false }, () => {
                        this.scrollToIndex()
                    });
                } else {
                    this.setState({ isLoading: false, dataLCT: [], listDSCot: [] })
                }
            } else {
                this.setState({ isLoading: false, dataLCT: [], listDSCot: [] })
            }
        })
    }

    scrollToIndex = () => {
        setTimeout(() => {
            let current_date = new Date()
            let day = current_date.getDate().toString()
            let month = (current_date.getMonth() + 1).toString()
            let year = current_date.getFullYear().toString()
            if (day.length === 1) {
                day = "0" + day
            }
            if (month.length === 1) {
                month = "0" + month
            }
            let nowDay = day + "/" + month + "/" + year
            for (let i = 0; i < this.state.dataLCT.length; i++) {
                let item = this.state.dataLCT[i]
                if (item[0] === nowDay) {
                    try {
                        this.myFlatlist.scrollToIndex({ animated: false, index: i })
                    } catch (err) {
                        console.log(err.message);
                        return false;
                    }
                    break
                }
            }
        }, 150);
    }

    OnPressCoQuan = () => {
        let listTenDonVi = []
        listTenDonVi.push("Đóng")
        for (let item of this.state.listCoQuan) {
            listTenDonVi.push(item.ten_don_vi)
        }
        ActionSheet.show(
            {
                options: listTenDonVi,
                cancelButtonIndex: 0,
                title: "Chọn cơ quan/đơn vị "
            },
            buttonIndex => {
                if (buttonIndex != 0) {
                    this.setState({
                        dataLCT: [],
                        isLoading: true,
                        tendv: this.state.listCoQuan[buttonIndex - 1].ten_don_vi,
                        madv: this.state.listCoQuan[buttonIndex - 1].ma_don_vi_kc
                    }, () => { this.getLCTChiTiet() })
                }
            }
        )
    }

    onPressPrevious = () => {
        let { current_week, current_year } = this.state

        if (this.state.isLoading) {
            Toast.show({ text: "Vui lòng chờ trong giây lát!", type: "warning", buttonText: "Đóng", position: "top", duration: 2000 })
            return null
        }

        if (global.lct_tuan_trong_nam_kieu_cu == 1) {
            if (current_week === 1) {
                current_week = 52
                current_year = current_year - 1
            } else {
                current_week = current_week - 1
            }
        } else {
            let arrDay = get_arrdate_from_week(current_week, current_year)
            let startDay = arrDay[0]
            startDay.setDate(startDay.getDate() - 7)
            let arrWeek = getWeekNumber(startDay)
            current_week = arrWeek[0]
            current_year = arrWeek[1]
        }
        let title_date = "Tuần " + current_week + " năm " + current_year
        let date_show = get_date_from_week(current_week, current_year)
        this.setState({ current_week, current_year, title_date, date_show }, () => {
            this.getLCTChiTiet()
        });
    }

    onPressNext = () => {
        let { current_week, current_year } = this.state

        if (this.state.isLoading) {
            Toast.show({ text: "Vui lòng chờ trong giây lát!", type: "warning", buttonText: "Đóng", position: "top", duration: 2000 })
            return null
        }

        if (global.lct_tuan_trong_nam_kieu_cu == 1) {
            if (current_week === 52) {
                current_week = 1
                current_year = current_year + 1
            } else {
                current_week = current_week + 1
            }

        } else {
            let arrDay = get_arrdate_from_week(current_week, current_year)
            let startDay = arrDay[0]
            startDay.setDate(startDay.getDate() + 7)
            let arrWeek = getWeekNumber(startDay)
            current_week = arrWeek[0]
            current_year = arrWeek[1]
        }
        let title_date = "Tuần " + current_week + " năm " + current_year
        let date_show = get_date_from_week(current_week, current_year)
        this.setState({ current_week, current_year, title_date, date_show }, () => {
            this.getLCTChiTiet()
        });
    }

    onPressSave = () => {
        let arrCheck = this.state.arrCheck
        let arrUnCheck = this.arrAllCheck
        for (let item of arrCheck) {
            console.log("item:" + item)
            arrUnCheck = arrUnCheck.filter(el => el !== item)
            console.log("arrUnCheck:" + arrUnCheck)
        }
        API.LichCongTac.AU_LCT_DLCTCQ(arrCheck.join(";"), arrUnCheck.join(";")).then((response) => {
            if (response) {
                ToastSuccess("Duyệt lịch công tác thành công!")
                this.props.navigation.dispatch(resetStack("LCT"))
            } else {
                Toast.show({ text: "Xảy ra lỗi. Vui lòng thực hiện lại sau!", type: "danger", buttonText: "Đóng", position: "top", duration: 2000 })
            }
        })
    }

    renderDataInDay = (data, listDSCot) => {
        let arrResult = []
        let haveData = false
        let num = 0
        for (let item of data) {
            if (item.noi_dung) {
                haveData = true
                let borderTopWidth = (num === 0) ? 0 : 0.8
                let arrCheck = this.state.arrCheck
                let flag = arrCheck.length > 0 && arrCheck.indexOf(item.ma_cong_viec_chi_tiet_kc) !== -1 ? true : false
                if (this.arrAllCheck.indexOf(item.ma_cong_viec_chi_tiet_kc) === -1) {
                    this.arrAllCheck = [...this.arrAllCheck, item.ma_cong_viec_chi_tiet_kc]
                }
                num += 1
                arrResult.push(
                    <Card noShadow style={{ borderTopWidth, borderLeftWidth: 0, borderRightWidth: 0, borderBottomWidth: 0, borderColor: "gray" }} key={num}>
                        {(item.gio_thuc_hien) ? (
                            <CardItem style={{ flex: 1 }}>
                                <Icon type="MaterialIcons" name="alarm" style={{ fontSize: this.props.fontSize.FontSizeXLarge + 2, color: "red" }} onPress={() => { }} />
                                <Text style={{ fontSize: this.props.fontSize.FontSizeXLarge, fontWeight: "bold", color: "red", marginLeft: -8 }}>{item.gio_thuc_hien}</Text>
                                <View style={{ position: "absolute", right: 0, paddingRight: 20 }}>
                                    <CheckBox checked={flag} color={"red"} onPress={() => {
                                        let arrTemp = []
                                        if (flag) {
                                            arrTemp = arrCheck.filter(el => el !== item.ma_cong_viec_chi_tiet_kc)
                                        } else {
                                            arrTemp = [...arrCheck, item.ma_cong_viec_chi_tiet_kc]

                                        }
                                        this.setState({ arrCheck: arrTemp })
                                    }}
                                    />
                                </View>
                            </CardItem>
                        ) : (
                            <Text style={{ height: 5 }}>&nbsp;</Text>
                        )}
                        <CardItem>
                            <Text style={{ fontSize: this.props.fontSize.FontSizeNorman, color: "blue", alignSelf: "flex-start" }}>Nội dung: </Text>
                            <Text style={{ fontSize: this.props.fontSize.FontSizeNorman, flexWrap: "wrap", flexShrink: 1, }}>{RemoveHTMLTag(item.noi_dung)}</Text>
                        </CardItem>
                        {(listDSCot.length >= 1 && listDSCot[0].trang_thai_cot == 1 && item.gia_tri_cot_1) && (
                            <CardItem style={{ paddingTop: 6 }}>
                                <Text style={{ fontSize: this.props.fontSize.FontSizeNorman, color: "blue", alignSelf: "flex-start" }}>{listDSCot[0].ten_cot}: </Text>
                                <Text style={{ fontSize: this.props.fontSize.FontSizeNorman, flexWrap: "wrap", flexShrink: 1, }}>{RemoveHTMLTag(item.gia_tri_cot_1)}</Text>
                            </CardItem>
                        )}
                        {(listDSCot.length >= 2 && listDSCot[1].trang_thai_cot == 1 && item.gia_tri_cot_2) && (
                            <CardItem style={{ paddingTop: 6 }}>
                                <Text style={{ fontSize: this.props.fontSize.FontSizeNorman, color: "blue", alignSelf: "flex-start" }}>{listDSCot[1].ten_cot}: </Text>
                                <Text style={{ fontSize: this.props.fontSize.FontSizeNorman, flexWrap: "wrap", flexShrink: 1, }}>{RemoveHTMLTag(item.gia_tri_cot_2)}</Text>
                            </CardItem>
                        )}
                        {(listDSCot.length >= 3 && listDSCot[2].trang_thai_cot == 1 && item.gia_tri_cot_3) && (
                            <CardItem style={{ paddingTop: 6 }}>
                                <Text style={{ fontSize: this.props.fontSize.FontSizeNorman, color: "blue", alignSelf: "flex-start" }}>{listDSCot[2].ten_cot}: </Text>
                                <Text style={{ fontSize: this.props.fontSize.FontSizeNorman, flexWrap: "wrap", flexShrink: 1, }}>{RemoveHTMLTag(item.gia_tri_cot_3)}</Text>
                            </CardItem>
                        )}
                        {(listDSCot.length >= 4 && listDSCot[3].trang_thai_cot == 1 && item.gia_tri_cot_4) && (
                            <CardItem style={{ paddingTop: 6 }}>
                                <Text style={{ fontSize: this.props.fontSize.FontSizeNorman, color: "blue", alignSelf: "flex-start" }}>{listDSCot[3].ten_cot}: </Text>
                                <Text style={{ fontSize: this.props.fontSize.FontSizeNorman, flexWrap: "wrap", flexShrink: 1, }}>{RemoveHTMLTag(item.gia_tri_cot_4)}</Text>
                            </CardItem>
                        )}
                        {(listDSCot.length >= 5 && listDSCot[4].trang_thai_cot == 1 && item.gia_tri_cot_5) && (
                            <CardItem style={{ paddingTop: 6 }}>
                                <Text style={{ fontSize: this.props.fontSize.FontSizeNorman, color: "blue", alignSelf: "flex-start" }}>{listDSCot[4].ten_cot}: </Text>
                                <Text style={{ fontSize: this.props.fontSize.FontSizeNorman, flexWrap: "wrap", flexShrink: 1, }}>{RemoveHTMLTag(item.gia_tri_cot_5)}</Text>
                            </CardItem>
                        )}
                        {(listDSCot.length >= 6 && listDSCot[5].trang_thai_cot == 1 && item.gia_tri_cot_6) && (
                            <CardItem style={{ paddingTop: 6 }}>
                                <Text style={{ fontSize: this.props.fontSize.FontSizeNorman, color: "blue", alignSelf: "flex-start" }}>{listDSCot[5].ten_cot}: </Text>
                                <Text style={{ fontSize: this.props.fontSize.FontSizeNorman, flexWrap: "wrap", flexShrink: 1, }}>{RemoveHTMLTag(item.gia_tri_cot_6)}</Text>
                            </CardItem>
                        )}
                        {(listDSCot.length >= 7 && listDSCot[6].trang_thai_cot == 1 && item.gia_tri_cot_7) && (
                            <CardItem style={{ paddingTop: 6 }}>
                                <Text style={{ fontSize: this.props.fontSize.FontSizeNorman, color: "blue", alignSelf: "flex-start" }}>{listDSCot[6].ten_cot}: </Text>
                                <Text style={{ fontSize: this.props.fontSize.FontSizeNorman, flexWrap: "wrap", flexShrink: 1, }}>{RemoveHTMLTag(item.gia_tri_cot_7)}</Text>
                            </CardItem>
                        )}
                        {(listDSCot.length >= 8 && listDSCot[7].trang_thai_cot == 1 && item.gia_tri_cot_8) && (
                            <CardItem style={{ paddingTop: 6 }}>
                                <Text style={{ fontSize: this.props.fontSize.FontSizeNorman, color: "blue", alignSelf: "flex-start" }}>{listDSCot[7].ten_cot}: </Text>
                                <Text style={{ fontSize: this.props.fontSize.FontSizeNorman, flexWrap: "wrap", flexShrink: 1, }}>{RemoveHTMLTag(item.gia_tri_cot_8)}</Text>
                            </CardItem>
                        )}
                        {(listDSCot.length >= 9 && listDSCot[8].trang_thai_cot == 1 && item.gia_tri_cot_9) && (
                            <CardItem style={{ paddingTop: 6 }}>
                                <Text style={{ fontSize: this.props.fontSize.FontSizeNorman, color: "blue", alignSelf: "flex-start" }}>{listDSCot[8].ten_cot}: </Text>
                                <Text style={{ fontSize: this.props.fontSize.FontSizeNorman, flexWrap: "wrap", flexShrink: 1, }}>{RemoveHTMLTag(item.gia_tri_cot_9)}</Text>
                            </CardItem>
                        )}
                        {(listDSCot.length >= 10 && listDSCot[9].trang_thai_cot == 1 && item.gia_tri_cot_10) && (
                            <CardItem style={{ paddingTop: 6 }}>
                                <Text style={{ fontSize: this.props.fontSize.FontSizeNorman, color: "blue", alignSelf: "flex-start" }}>{listDSCot[9].ten_cot}: </Text>
                                <Text style={{ fontSize: this.props.fontSize.FontSizeNorman, flexWrap: "wrap", flexShrink: 1, }}>{RemoveHTMLTag(item.gia_tri_cot_10)}</Text>
                            </CardItem>
                        )}
                        {(item.file_lct) && (
                            <ViewFile
                                src_file={item.file_lct}
                                navigation={this.props.navigation}
                                fontSize={this.props.fontSize}
                            />
                        )}
                    </Card>
                )
            }
        }
        if (haveData === false) {
            arrResult.push(
                <Text style={{ margin: 8, fontSize: this.props.fontSize.FontSizeNorman }} key={1}>Không có lịch</Text>
            )
        }
        return arrResult
    }


    render() {
        const popAction = StackActions.pop({ n: 1 })
        return (
            <Container style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
                <HeaderWithLeftRight
                    title="Duyệt lịch công tác cơ quan"
                    buttonLeftName="arrow-back"
                    onPressLeftButton={_ => { this.props.navigation.dispatch(popAction) }}
                    buttonRightName="done"
                    onPressRightButton={this.onPressSave}
                />
                <View style={{ height: 30, marginLeft: 20, marginRight: 20, alignSelf: "center", width: "100%" }}  >
                    <TouchableOpacity onPress={this.OnPressCoQuan} style={{ fontSize: this.props.fontSize.FontSizeNorman }}>
                        <Text style={{ textAlign: "center", fontSize: this.props.fontSize.FontSizeNorman, width: "100%" }} > {this.state.tendv}  </Text>
                        <View style={{ alignSelf: "center", height: 1, marginTop: 2, backgroundColor: '#C2C2C2', width: "70%" }}  >
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={{ height: 45, flexDirection: "row", marginLeft: 10, marginRight: 10 }}  >
                    <TouchableOpacity onPress={this.onPressPrevious} style={{ flex: 22, alignItems: "center", fontSize: this.props.fontSize.FontSizeNorman }}>
                        <Image style={{ width: 30, height: 30 }} source={require('./images/b1.png')} />
                    </TouchableOpacity>
                    <View style={{ flex: 56 }}  >
                        <Text style={{ textAlign: "center", fontSize: this.props.fontSize.FontSizeNorman, color: "red", fontWeight: 'bold' }} > {this.state.title_date}   </Text>
                        <Text style={{ textAlign: "center", fontSize: this.props.fontSize.FontSizeNorman, color: "gray", fontWeight: 'bold' }}>{this.state.date_show}</Text>
                    </View>
                    <TouchableOpacity onPress={this.onPressNext} style={{ flex: 22, alignItems: "center", fontSize: this.props.fontSize.FontSizeNorman }}>
                        <Image style={{ width: 30, height: 30 }} source={require('./images/b2.png')} />
                    </TouchableOpacity>
                </View>
                {
                    (this.state.isLoading) ? (
                        <Spinner />
                    ) : (
                        <FlatList
                            bounces={false}
                            ref={(myFlatlist) => { this.myFlatlist = myFlatlist }}
                            data={this.state.dataLCT}
                            extraData={this.state}
                            keyExtractor={(item, index) => index.toString()}
                            renderItem={({ item, index }) => {
                                let ngay = item[0]
                                let thu = index === 6 ? "Chủ nhật" : "Thứ " + (index + 2)
                                let data = item[1]
                                return (
                                    <Card noShadow key={index} style={{ marginLeft: 10, marginRight: 10, marginTop: 10, marginBottom: 1, borderRadius: 10, borderWidth: 1, borderColor: "gray" }}>
                                        <View style={{ backgroundColor: AppConfig.blueBackground, alignItems: 'center', justifyContent: "center", borderTopLeftRadius: 6, borderTopRightRadius: 8 }}>
                                            <Text style={{ color: "white", margin: 8, fontSize: this.props.fontSize.FontSizeNorman }}>{thu + " - " + ngay}</Text>
                                        </View>
                                        {this.renderDataInDay(data, this.state.listDSCot)}
                                    </Card>
                                )
                            }}
                            onScrollToIndexFailed={(info) => {
                                if (info.index) {
                                    this.myFlatlist.scrollToIndex({ animated: false, index: info.index })
                                } else {
                                    this.scrollToIndex()
                                }
                            }}
                        />
                    )
                }
            </Container>
        )
    }
}

function mapStateToProps(state) {
    return {
        fontSize: state.fontSize,
    }
}

export default connect(mapStateToProps)(LCT_CoQuan_Duyet)