import React, { Component } from 'react';
import { View, StyleSheet, Dimensions, TouchableOpacity, Image } from 'react-native';
import { Container, Content, CardItem, Text, Body, Spinner, List, ListItem, Left, Icon, ActionSheet } from 'native-base';
import { connect } from "react-redux"
import { AppConfig } from "../../AppConfig";
import API from "../../networks/index";
import { get_date_from_week, get_arrdate_from_week, getWeekNumber, RemoveHTMLTag } from "../../untils/TextUntils"

class LCT_LanhDao extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: [],
            goodData: [],
            reload: true,
            DSLanhDao: [],
            DSDonViQuanTri: [],
            arrCB: [],
            MaDVQT: global.ma_don_vi_quan_tri,
            TenDVQT: global.ten_don_vi_quan_tri,
            arrLastIndex: [],
            checkLCTLD: 0,
        }

        this.current_week = 0
        this.current_year = 0
        this.current_date = ""
        this.dateOfWeek = []
        this.dateConvert = ""
        this.year = parseInt(new Date().getFullYear());
        this.arrCB = [];
    }

    componentDidMount = async () => {
        this.load_data();
        this.current_week = this.state.current_week;
        this.current_year = this.state.current_year;
        this.getDanhSachDonViQuanTri();
        // await this.getLCTLanhDaoCot(MaDVQT);
        Dimensions.addEventListener("change", (e) => {
            this.setState(e.window);
        });
    }

    getDanhSachDonViQuanTri = async () => {
        let MaDVQT = this.state.MaDVQT;
        if (global.lct_co_quan_hien_thi_ds_co_quan === "0") {
            API.LichCongTac.AU_LCT_DSCQLCT()
                .then((res) => {
                    this.setState({
                        DSDonViQuanTri: res
                    }, () => this.getLCTLanhDaoCot(MaDVQT));
                })
        } else {
            API.LichCongTac.AU_LCT_DSCQLCT1(MaDVQT)
                .then((res) => {
                    this.setState({
                        DSDonViQuanTri: res
                    }, () => this.getLCTLanhDaoCot(MaDVQT));
                })
        }

    }

    getLCTLanhDaoCot = (MaDVQT) => {
        let nam = this.state.current_year;
        let tuan = this.state.current_week;

        API.LichCongTac.AU_LCT_LCTLDBC(MaDVQT)
            .then((res) => {
                res.map((e, index) => {
                    this.arrCB.push("cb" + (index + 1))
                })
                this.setState({
                    DSLanhDao: res,
                    arrCB: this.arrCB
                }, () => this.getLCTLanhDao(MaDVQT, nam, tuan))
            })
    }

    getLCTLanhDao = (MaDVQT, nam, tuan) => {
        API.LichCongTac.AU_LCT_XLCTLD(MaDVQT, nam, tuan)
            .then((res) => {
                this.setState({
                    data: res,
                    reload: false
                }, () => this.convertData())
            })
    }
    convertData = () => {
        let rawData = this.state.data;
        let arrDate = [...(rawData.map(a => a.ngay_thuc_hien_vn))];
        arrDate = [...new Set(arrDate)];
        let obj = new Object();
        let data = arrDate.map((ad) => {
            let tempData = [...rawData.filter((a) => a.ngay_thuc_hien_vn === ad)]
            obj = {
                "title": ad,
                "data": this.state.DSLanhDao.map(e => ({ ...e, tempData }))
            }
            return obj;
        })
        this.setState({
            goodData: data,
        }, () => this.checkLastIndex(data))
    }
    checkLastIndex = (data) => {
        let arrCB = this.arrCB;
        let arrLastIndex = [];
        let checkLCTLD = 0;
        data.map((e, index) => {
            let lastIndex = 0;
            e.data.map((item, i) => {
                item.tempData.map((e2, index2) => {
                    if (item.tempData[index2][arrCB[i]] != null) {
                        lastIndex = i;
                        checkLCTLD += 1;
                    }
                })
            })
            arrLastIndex.push(lastIndex)
        })
        this.setState({
            arrLastIndex,
            checkLCTLD
        })
    }

    renderData = (data) => {
        let result = []
        for (let item of data) {
            result.push(

            )
        }

        return result
    }

    renderItem = (dataItem, index) => {
        let arrCB = this.arrCB;
        let arrLastIndex = this.state.arrLastIndex;
        let result = []
        let i = -1
        for (let item of dataItem) {
            i += 1
            result.push(
                <ListItem padder avatar key={item.ho_va_ten_can_bo} style={{ paddingRight: 0, justifyContent: 'center', alignItems: 'center' }}>
                    {(item.tempData.length < 3) ?
                        (item.tempData[0][arrCB[i]] != null || item.tempData[1][arrCB[i]] != null) &&
                        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                            <Left style={this.styles.listItemContentLeft}>
                                <View>
                                    <View
                                        style={{
                                            minWidth: 100,
                                            maxWidth: 160,
                                            justifyContent: 'center',
                                            alignItems: 'center'
                                        }}
                                    >
                                        <Text
                                            style={{ textAlign: "center", alignSelf: 'center', marginBottom: 15, fontSize: this.props.fontSize.FontSizeNorman }}
                                        >
                                            {item.ho_va_ten_can_bo}
                                        </Text>
                                    </View>
                                </View>
                            </Left>
                            <Body
                                padder
                                style={{ borderBottomWidth: (i == (arrLastIndex[index])) ? 0 : 1, borderBottomColor: '#636e72' }}
                            >
                                <CardItem>
                                    <Icon
                                        type="Ionicons"
                                        name="md-partly-sunny"
                                        style={{ color: "#ffbdb6" }}
                                    />
                                    <View style={{ flex: 1 }}>
                                        <Text
                                            numberOfLines={2}
                                            ellipsizeMode={"tail"}
                                            style={{ fontSize: this.props.fontSize.FontSizeNorman }}
                                        >
                                            {RemoveHTMLTag(item.tempData[0][arrCB[i]])}
                                        </Text>
                                    </View>
                                </CardItem>
                                <CardItem>
                                    <Icon
                                        type="Ionicons"
                                        name="md-sunny"
                                        style={{ color: '#ff3434' }}
                                    />
                                    <View style={{ flex: 1 }}>
                                        <Text
                                            numberOfLines={2}
                                            ellipsizeMode='tail'
                                            style={{ fontSize: this.props.fontSize.FontSizeNorman }}
                                        >
                                            {RemoveHTMLTag(item.tempData[1][arrCB[i]])}
                                        </Text>
                                    </View>
                                </CardItem>
                                {
                                    (item.tempData.length == 3) &&
                                    <CardItem >
                                        <Icon
                                            type="FontAwesome5"
                                            name="cloud-moon"
                                            style={{ color: "#0e0e7b" }}
                                        />
                                        <View style={{ flex: 1 }}>
                                            <Text
                                                numberOfLines={2}
                                                ellipsizeMode="tail"
                                                style={{ fontSize: this.props.fontSize.FontSizeNorman }}
                                            >
                                                {RemoveHTMLTag(item.tempData[2][arrCB[i]])}
                                            </Text>
                                        </View>
                                    </CardItem>
                                }
                            </Body>
                        </View>
                        :
                        (item.tempData[0][arrCB[i]] != null || item.tempData[1][arrCB[i]] != null || item.tempData[2][arrCB[i]] != null) &&
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Left style={this.styles.listItemContentLeft}>
                                <View>
                                    <View
                                        style={{
                                            // borderWidth: 2,
                                            // borderColor: 'green',
                                            minWidth: 100,
                                            maxWidth: 160,
                                            justifyContent: 'center',
                                            alignItems: 'center'
                                        }}
                                    >
                                        <Text
                                            style={{ textAlign: "center", alignSelf: 'center', marginBottom: 15, fontSize: this.props.fontSize.FontSizeNorman }}
                                        >
                                            {item.ho_va_ten_can_bo}
                                        </Text>
                                    </View>
                                </View>
                            </Left>
                            <Body
                                padder
                                style={{ borderBottomWidth: (i == (arrLastIndex[index])) ? 0 : 1, borderBottomColor: '#636e72' }}
                            >
                                <CardItem>
                                    <Icon
                                        type="Ionicons"
                                        name="md-partly-sunny"
                                        style={{ color: "#ffbdb6" }}
                                    />
                                    <View style={{ flex: 1 }}>
                                        <Text
                                            numberOfLines={2}
                                            ellipsizeMode={"tail"}
                                            style={{ fontSize: this.props.fontSize.FontSizeNorman }}
                                        >
                                            {RemoveHTMLTag(item.tempData[0][arrCB[i]])}
                                        </Text>
                                    </View>
                                </CardItem>
                                <CardItem>
                                    <Icon
                                        type="Ionicons"
                                        name="md-sunny"
                                        style={{ color: '#ff3434' }}
                                    />
                                    <View style={{ flex: 1 }}>
                                        <Text
                                            numberOfLines={2}
                                            ellipsizeMode='tail'
                                            style={{ fontSize: this.props.fontSize.FontSizeNorman }}
                                        >
                                            {RemoveHTMLTag(item.tempData[1][arrCB[i]])}
                                        </Text>
                                    </View>
                                </CardItem>
                                <CardItem >
                                    <Icon
                                        type="FontAwesome5"
                                        name="cloud-moon"
                                        style={{ color: "#0e0e7b" }}
                                    />
                                    <View style={{ flex: 1 }}>
                                        <Text
                                            numberOfLines={2}
                                            ellipsizeMode="tail"
                                            style={{ fontSize: this.props.fontSize.FontSizeNorman }}
                                        >
                                            {RemoveHTMLTag(item.tempData[2][arrCB[i]])}
                                        </Text>
                                    </View>
                                </CardItem>

                            </Body>
                        </View>
                    }
                </ListItem>
            )
        }
        return result
    }

    renderData = (data) => {
        let result = []
        let i = -1
        for (let item of data) {
            i += 1
            result.push(
                <List key={item} style={this.styles.list}>
                    <ListItem itemDivider style={this.styles.listItemHeader}>
                        <Text style={this.styles.txtListItemHeader}>
                            {item.title}
                        </Text>
                    </ListItem>
                    {this.renderItem(item.data, i)}
                </List>
            )
        }
        return result
    }

    _renderListItem = () => {
        let data = this.state.goodData;
        let checkLCTLD = this.state.checkLCTLD;

        return (
            (checkLCTLD == 0) ?
                <Content contentContainerStyle={{ justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>Không có lịch công tác</Text>
                </Content>
                :
                <Content padder>
                    {this.renderData(data)}
                </Content>
        );
    }

    load_data = () => {
        this.current_date = new Date();
        let arrDay = getWeekNumber(this.current_date)
        this.current_week = arrDay[0]
        this.current_year = arrDay[1]
        this.title_date = "Tuần " + this.current_week + " năm " + this.current_year;
        this.setState({
            current_week: this.current_week,
            current_year: this.current_year,
            title_date: "Tuần " + this.current_week + " năm " + this.current_year,
            date_show: get_date_from_week(this.current_week, this.current_year)
        }, () => { this.current_date = get_date_from_week(this.current_week, this.current_year) });
    }

    On_Click_Previous = () => {
        this.arrCB = [];
        this.dateOfWeek = [];
        let { current_week, current_year } = this.state
        if (global.lct_tuan_trong_nam_kieu_cu == 1) {
            if (current_week === 1) {
                current_week = 52
                current_year = current_year - 1
            } else {
                current_week = current_week - 1
            }
        } else {
            let arrDay = get_arrdate_from_week(current_week, current_year)
            let startDay = arrDay[0]
            startDay.setDate(startDay.getDate() - 7)
            let arrWeek = getWeekNumber(startDay)
            current_week = arrWeek[0]
            current_year = arrWeek[1]
        }
        let title_date = "Tuần " + current_week + " năm " + current_year
        let date_show = get_date_from_week(current_week, current_year)
        this.setState({ current_week, current_year, title_date, date_show, reload: true, goodData: [], }, () => {
            this.getLCTLanhDaoCot(global.ma_don_vi_quan_tri)
        });
    }

    On_Click_Next = () => {
        this.arrCB = [];
        this.dateOfWeek = [];
        let { current_week, current_year } = this.state
        if (global.lct_tuan_trong_nam_kieu_cu == 1) {
            if (current_week === 52) {
                current_week = 1
                current_year = current_year + 1
            } else {
                current_week = current_week + 1
            }

        } else {
            let arrDay = get_arrdate_from_week(current_week, current_year)
            let startDay = arrDay[0]
            startDay.setDate(startDay.getDate() + 7)
            let arrWeek = getWeekNumber(startDay)
            current_week = arrWeek[0]
            current_year = arrWeek[1]
        }
        let title_date = "Tuần " + current_week + " năm " + current_year
        let date_show = get_date_from_week(current_week, current_year)
        this.setState({ current_week, current_year, title_date, date_show, reload: true, goodData: [], }, () => {
            this.getLCTLanhDaoCot(global.ma_don_vi_quan_tri)
        });
    }

    //Chọn đơn vị
    chonDonVi = async () => {
        let tenDonVi = [];
        let donViList = this.state.DSDonViQuanTri;
        tenDonVi = donViList.map(donvi => donvi.ten_don_vi);
        tenDonVi.unshift("Đóng");
        ActionSheet.show(
            {
                options: tenDonVi,
                cancelButtonIndex: 0,
                title: "Chọn đơn vị"
            },
            buttonIndex => {
                if (buttonIndex != 0) {
                    this.setState({
                        MaDVQT: donViList[buttonIndex - 1].ma_don_vi_kc,
                        TenDVQT: tenDonVi[buttonIndex],
                        reload: true
                    }, () => { this.getLCTLanhDaoCot(this.state.MaDVQT) });
                }
            }
        )
    }

    componentWillUnmount() {
        Dimensions.removeEventListener("change")
    }

    render() {

        this.styles = StyleSheet.create({
            container: {
                flex: 1,
            },
            list: {
                borderWidth: 1,
                borderBottomWidth: 1,
                borderRadius: 8,
                borderColor: "grey",
                marginBottom: 10,
            },
            listItemHeader: {
                backgroundColor: AppConfig.blueBackground,
                alignItems: 'center',
                justifyContent: 'center',
                borderTopLeftRadius: 5,
                borderTopRightRadius: 5,
            },
            txtListItemHeader: {
                color: "#FFF",
                alignSelf: 'center',
                fontSize: this.props.fontSize.FontSizeNorman
            },
            listItemContentLeft: {
                alignItems: 'center',
                justifyContent: 'center',
                alignSelf: 'center',
            },
            headerDateBox: {
                justifyContent: 'center',
                alignItems: 'center',
                marginTop: 5
            },
            chonDVBox: {
                flexDirection: 'row',
                justifyContent: "center"
            },
            chonDV: {
                height: 35,
                width: 250,
                borderRadius: 6,
                backgroundColor: AppConfig.blueBackground,
                flexDirection: 'row',
                justifyContent: 'flex-start',
                marginRight: 5,
            },
            iconArrowDownBox: {
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
                paddingTop: 3,
            },
            txtTenDVBox: {
                flex: 8,
                justifyContent: "center",
                alignItems: 'center'
            },
            txtTenDV: {
                color: "#FFF",
                alignSelf: 'center',
            },
            balanceBox: {
                color: "#FFF",
                alignSelf: 'center',
                flex: 1
            },
            dateBox: {
                height: 60,
                width: 260,
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
            },
            clickChangeWeek: {
                flex: 22,
                alignItems: "center",
                fontSize: this.props.fontSize.FontSizeNorman
            },
            imageChangeDate: {
                width: 30,
                height: 30
            },
            textShowTitleDate: {
                textAlign: "center",
                fontSize: this.props.fontSize.FontSizeNorman,
                color: "red",
                fontWeight: 'bold'
            },
            textShowDate: {
                textAlign: "center",
                fontSize: this.props.fontSize.FontSizeNorman,
                color: "grey",
                fontWeight: 'bold'
            },
        });

        let rawData = this.state.data;

        return (
            <View style={this.styles.container}>
                <View style={this.styles.headerDateBox}>
                    <View style={this.styles.chonDVBox}>
                        <View style={{ alignSelf: "center", width: "100%" }}  >
                            <TouchableOpacity onPress={() => this.chonDonVi()} style={{ fontSize: this.props.fontSize.FontSizeNorman }}>
                                <Text style={{ textAlign: "center", fontSize: this.props.fontSize.FontSizeNorman, width: "100%", color: 'gray' }}> {this.state.TenDVQT}  </Text>
                                <View style={{ alignSelf: "center", height: 1, marginTop: 2, backgroundColor: '#C2C2C2', width: "70%" }}  >
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={this.styles.dateBox}>
                        <TouchableOpacity
                            onPress={() => { this.On_Click_Previous() }} style={this.styles.clickChangeWeek}
                        >
                            <Image
                                style={this.styles.imageChangeDate}
                                source={require('./images/b1.png')}
                            />
                        </TouchableOpacity>

                        <View>
                            <Text style={this.styles.textShowTitleDate}>{this.state.title_date}</Text>
                            <Text style={this.styles.textShowDate}>{this.state.date_show}</Text>
                        </View>

                        <TouchableOpacity
                            onPress={() => { this.On_Click_Next() }}
                            style={this.styles.clickChangeWeek}
                        >
                            <Image
                                style={this.styles.imageChangeDate}
                                source={require('./images/b2.png')}
                            />
                        </TouchableOpacity>
                    </View>
                </View>
                <Container>
                    {
                        (this.state.reload) ?
                            <Spinner large color={AppConfig.blueBackground} />
                            :
                            (rawData[0] == "khong") ?
                                <Content
                                    contentContainerStyle={{
                                        justifyContent: 'center',
                                        alignItems: 'center'
                                    }}
                                >
                                    <Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>Không có lịch công tác</Text>

                                </Content>
                                :
                                this._renderListItem()
                    }
                </Container>
            </View>
        )
    }
}

function mapStateToProps(state) {
    return {
        fontSize: state.fontSize,
    }
}

export default connect(mapStateToProps)(LCT_LanhDao)