import React, { Component } from 'react'
import { StyleSheet,Alert, View, TouchableOpacity, Image, AsyncStorage, PixelRatio, KeyboardAvoidingView, Platform, Modal, SafeAreaView,Linking } from 'react-native'
import { Container, Content, Form, Item, Text, CheckBox, Toast, ActionSheet, Button } from "native-base"
import { AppConfig } from "../AppConfig"
import API from "../networks"
import { WebView } from 'react-native-webview';
import { connect } from "react-redux"
import Orientation from 'react-native-orientation'
import MyStatusBar from "../components/StatusBar"
import Wallpaper from '../components/Wallpaper';
import CustomTextInput from "../components/CustomTextInput"
import serverImg from "../assets/images/server.png"
import usernameImg from '../assets/images/username.png'
import passwordImg from '../assets/images/password.png'
import ModalOTP from '../components/Modal/ModalOTP'
import ModalDangThucHien from "../components/Modal/ModalDangThucHien"
import { setUserSettings } from "../untils/TextUntils"
const checkOTP = false

class LoginSSO extends Component {
    constructor() {
        super()
        this.state = {
            isVisble: false
        }
        this.WebView1 = null
    }

    getParameterByName = (name, url) => {
        if (!url) return null;
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regexS = "[\\?&]" + name + "=([^&#]*)";
        var regex = new RegExp(regexS);
        var results = regex.exec(url);
        if (results == null)
            return "";
        else
            return decodeURIComponent(results[1].replace(/\+/g, " "));
    }

    render() {
        // let { props } = this.props
        // let client_id = props.sso_client_id || ""
        // let redirect_uri = props.sso_redirect_uri || ""
        // let url_sso = props.sso_login_uri || ""
        // let urlLogin = url_sso + "/oauth2/authorize"
        // urlLogin += "?response_type=code"
        // urlLogin += "&client_id=" + client_id
        // urlLogin += "&redirect_uri=" + redirect_uri
        // urlLogin += "&scope=openid"

        return (
            <Modal
                transparent={false}
                animationType="slide"
                visible={this.state.isVisble}
                supportedOrientations={["landscape", "landscape-left", "landscape-right", "portrait", "portrait-upside-down"]}
            >
                <View style={{ flex: 1, backgroundColor: AppConfig.grayBackground }}>
                    <MyStatusBar backgroundColor={Platform.OS === 'ios' ? "#000000" : AppConfig.statusBarColor} skipAndroid={true} barStyle="light-content" />
                    <SafeAreaView style={{ zIndex: 999, backgroundColor: Platform.OS === 'ios' ? "#000000" : AppConfig.statusBarColor, flex: 1 }}>
                        <Button danger
                            style={{ position: "absolute", top: 0, left: 0, zIndex: 999, borderRadius: 0 }}
                            onPress={() => { this.setState({ isVisble: false }) }}
                        >
                            <Text>&nbsp;&nbsp;&nbsp;X&nbsp;&nbsp;&nbsp;</Text>
                        </Button>
                        <WebView
                            ref={ref => { this.WebView1 = ref }}
                            style={{ flex: 1 }}
                            source={{ uri: this.props.getSSOURL() }}
                            javaScriptEnabled={true}
                            domStorageEnabled={true}
                            decelerationRate="normal"
                            mixedContentMode="always"
                            originWhitelist={["*"]}
                            startInLoadingState={true}
                            scalesPageToFit={true}
                            bounces={false}
                            onNavigationStateChange={(event) => {
                                if (event.url) {
                                    if (event.url.includes(this.props.getRedirectURI())) {
                                        let code = this.getParameterByName("code", event.url)
                                        if (code) {
                                            this.WebView1.stopLoading()
                                            this.props.callbackLoginSSO(code)
                                            this.setState({ isVisble: false })
                                        }
                                    }
                                }
                                console.log("onNavigationStateChange: " + JSON.stringify(event))
                            }}
                        />
                    </SafeAreaView>
                </View>
            </Modal>
        )
    }
}

class LoginForm extends Component {
    constructor() {
        super();
        this.state = {
            isLoading: false,
            TaiKhoan: "",
            MatKhau: "",
            Checked: false,
            url_api: "",
            ten_don_vi: "Chọn đơn vị",
            web_version: "1",
            listDonVi: [],
            visibleForm: true,
            modalVisible: false,
            returnLogin: false,
            haveSSO: false,
            sso_client_id: "",
            sso_client_secret: "",
            sso_login_uri: "",
            sso_redirect_uri: "",
            url_logout_sso: "https://www.google.com.vn",
           
        };
        this.ma_ctcb_kc_Store = ""
        this.passWordCrypt="iOfficeCM@20@@";
        this.stringCode = ''
    }


    componentDidMount = async () => {
        Orientation.lockToPortrait()
        global.accessTokenHSM = {}
        this.getDanhSachDonVi()
        this.ma_ctcb_kc_Store = await AsyncStorage.getItem("ma_ctcb_kc_Store")
        let loaiDangNhap = this.props.navigation.getParam("loaiDangNhap", "");
        console.log('loai_dang_nhap'+JSON.stringify(loaiDangNhap));
        if (loaiDangNhap === "doiKiemNhiem") {
            this.setState({ visibleForm: false })
            this.onDoiKiemNhiem()
        } else if (loaiDangNhap === "dangXuat") {

        } else if (loaiDangNhap === "outSession") {
            this.onOutSession()
        }
        this.handleDeepLink();
       
    }

    componentWillUnmount = () => {
        Orientation.unlockAllOrientations()
        this.removeHandleDeepLink();
    };
    

    getDonViSSO = async ()=>{
        let { url_api, web_version } = this.state;
        await this.setState({
            ten_don_vi: "Cà Mau",
            url_api: url_api,
            web_version: web_version
        });
    }
    handleOpenURL= async(url)=> {
        const deeplink = url.split('://')[0]+'';
        if(deeplink ==='ioffice'){
            const path = url.split('://')[1]+'';
            var arrayParam = path.split('/');
            var authen = path.split('/')[0];
            var username = path.split('/')[1];
            var key = '';
            arrayParam.forEach(item =>{
                if(item == authen || item == username)return;
                key += item +'/';
            });
            var key = key.substring(0,(key.length-1));
            let chkDeepLinkSso = await AsyncStorage.getItem("chkDeepLinkSso");
            let loaiDangNhap = this.props.navigation.getParam("loaiDangNhap", "");
            if(!chkDeepLinkSso || loaiDangNhap ==''){
                this.tryLoginSSOMobile(username,key); 
            }
        }
	}
   
    tryLoginSSOMobile = async(username,key)=>{
        global.AU_ROOT = 'https://camau-api.vnptioffice.vn';
        let ma_ctcb_kiem_nhiem = this.ma_ctcb_kc_Store;
        await API.Login.AU_DSCTCB_SSO_MOBILE(username, key, this.passWordCrypt,ma_ctcb_kiem_nhiem,this.props.dispatch).then((response) => {     
        if (response) {
                this.checkedOTPSSOmobile()
            } else {
                Toast.show({ text: "Đăng nhập thất bại!", type: "warning", buttonText: "Đóng", position: "top", duration: 2000 })
            }
        });
    }

    checkedOTPSSOmobile = async () => {
        await AsyncStorage.setItem("chkDeepLinkSso","true");
        await AsyncStorage.setItem("chkSaveDeepLinkSso","true");
        await AsyncStorage.setItem("Url_Api_SaveLogin", "https://camau-api.vnptioffice.vn")
        setUserSettings(this.props.navigation, this.props.dispatch)
    }
	handleDeepLink =  () =>{
		Linking.addEventListener('url', event => this.handleOpenURL(event.url));
		Linking.getInitialURL().then(url => url && this.handleOpenURL(url));
	}
	removeHandleDeepLink = () =>{
		Linking.removeEventListener('url', this.handleOpenURL);
	}

    onDoiKiemNhiem = async () => {
        let TaiKhoan = global.TaiKhoan
        let MatKhau = global.MatKhau
        let url_api = global.AU_ROOT
        let web_version = global.web_version
        let chkSaveLogin = await AsyncStorage.getItem("chkSaveLogin") === "true" ? true : false
        this.setState({ TaiKhoan, MatKhau, url_api, Checked: chkSaveLogin, web_version }, () => {
            this.tryLogin()
        })

    }

    onOutSession = async () => {
        const chkSaveLogin = await AsyncStorage.getItem("chkSaveLogin");
        chkSaveLogin ? this.tryLoginAuto() : null
    }

    getDanhSachDonVi = async () => {
        API.Login.AU_DSDVA().then((listDonVi) => {
            this.setState({ listDonVi }, () => {
                if (this.state.isLoading) {    
                    this.setState({ isLoading: false }, () => {
                        this.showDonVi()
                    })
                }
            })
        })
    }

    showDonVi = () => {
        if (this.state.listDonVi.length === 0) {
            this.setState({ isLoading: true }, () => {
                this.getDanhSachDonVi()
            })
            return
        }
        let { listDonVi } = this.state
        ActionSheet.show(
            {
                options: listDonVi.map(obj => {
                    return obj.ten_don_vi
                }),
                title: "Chọn đơn vị của bạn"
            },
            buttonIndex => {
                if (buttonIndex !== undefined) {
                  
                    this.setState({
                        ten_don_vi: listDonVi[buttonIndex].ten_don_vi,
                        url_api: listDonVi[buttonIndex].url_api,
                        web_version: listDonVi[buttonIndex].version.split(".")[2]
                    }, () => {
                        API.Login.AU_TSCSY(listDonVi[buttonIndex].url_api).then((response) => {
                            if (response.length > 0) {
                                this.setState({
                                    haveSSO: true,
                                    sso_client_id: response[0].gia_tri_mac_dinh,
                                    sso_client_secret: response[1].gia_tri_mac_dinh,
                                    sso_login_uri: response[2].gia_tri_mac_dinh.split("|")[0],
                                    sso_redirect_uri: response[2].gia_tri_mac_dinh.split("|")[1]
                                })
                            } else {
                                this.setState({
                                    haveSSO: false,
                                    sso_client_id: "",
                                    sso_client_secret: "",
                                    sso_login_uri: "",
                                    sso_redirect_uri: ""
                                })
                            }
                        })
                    })
                }
            }
        )
    }

    _onPress = () => {
        if (this.state.TaiKhoan.toLocaleLowerCase() === "admin" && this.state.MatKhau.toLocaleLowerCase() === "trungnh.tgg") {
            this.props.navigation.navigate("AdminSettings")
            return
        }
        if (this.state.url_api === "") {
            Toast.show({ text: "Vui lòng chọn tỉnh thành!", type: "warning", buttonText: "Đóng", position: "top", duration: 2000 })
            return
        }
        if (this.state.TaiKhoan === "") {
            Toast.show({ text: "Vui lòng nhập tài khoản!", type: "warning", buttonText: "Đóng", position: "top", duration: 2000 })
            this.txtTaiKhoan.focus()
            return
        }
        if (this.state.MatKhau === "") {
            Toast.show({ text: "Vui lòng nhập mật khẩu!", type: "warning", buttonText: "Đóng", position: "top", duration: 2000 })
            this.txtMatKhau.focus()
            return
        }
        this.tryLogin()
    }

    tryLogin = () => {
        let { TaiKhoan, MatKhau, url_api, web_version } = this.state
        global.AU_ROOT = url_api
        global.TaiKhoan = TaiKhoan
        global.MatKhau = MatKhau
        global.web_version = web_version
        API.Login.AU_DSCTCB(TaiKhoan, MatKhau, this.ma_ctcb_kc_Store, this.props.dispatch).then((response) => {
            if (response) {
                if (global.AU_ROOT.includes("nghean-api")) {
                  this.getQuyenXinYKien();
                }
                if (ngay_dang_ky_otp !== null && checkOTP) {
                    this.getOTPString2()
                    return;
                } else {
                    this.checkedOTPSuccess()
                }
            } else {
                Toast.show({ text: "Đăng nhập thất bại!", type: "warning", buttonText: "Đóng", position: "top", duration: 2000 })
            }
        })
    }

    getQuyenXinYKien = async () => {
      await API.Login.AU_GETQXYKTT(global.refresh_token);
    }

    tryLoginAuto = () => {
        console.log('this is try login');
        API.Login.AU_DSDVA().then((listDonVi) => {
            API.Login.AU_DSCTCB1(this.props.dispatch, listDonVi).then((response) => {
                if (response) {
                    setUserSettings(this.props.navigation, this.props.dispatch)
                } else {
                    Toast.show({ text: "Đăng nhập thất bại!", type: "warning", buttonText: "Đóng", position: "top", duration: 2000 })
                }
            })
        })
    }
    autoLoginSSOdeepLink = () => {
        API.Login.AU_DSDVA().then((listDonVi) => {
            console.log('autoLoginSSO'+JSON.stringify(listDonVi));
        })
    }

    _onPressLoginSSO = async () => {
        await AsyncStorage.getItem("sso_id_token", (error, result) => {
            if (result && result !== "") {
                let id_token_hint = result
                let url_logout_sso = this.state.sso_login_uri + "/oidc/logout?id_token_hint=" + id_token_hint
                this.setState({ url_logout_sso }, () => {
                    setTimeout(() => {
                        this.wvLoginSSO.setState({ isVisble: true })
                    }, 500);
                })
            } else {
                this.wvLoginSSO.setState({ isVisble: true })
            }
        })
    }

    getSSOURL = () => {
        try {
            let url = this.state.sso_login_uri + "/oauth2/authorize"
            url += "?response_type=code"
            url += "&client_id=" + this.state.sso_client_id
            url += "&redirect_uri=" + this.state.sso_redirect_uri
            url += "&scope=openid"
            return url
        } catch (error) {
            return ""
        }
    }

    getRedirectURI = () => {
        return this.state.sso_redirect_uri
    }

    callbackLoginSSO = (code) => {
        let state = this.state
        global.AU_ROOT = state.url_api
        global.web_version = state.web_version
        API.Login.AU_OAUTH2_TOKEN(state.sso_login_uri, code, state.sso_redirect_uri, state.sso_client_id, state.sso_client_secret).then((response) => {
            if (response.access_token) {
                let { access_token, id_token } = response
                //Xử lý lưu id_token để logout cho lần sau
                AsyncStorage.setItem("sso_id_token", id_token)
                console.log("AU_OAUTH2_TOKEN: " + access_token)
                let access_token_sso_ybi = access_token
                let domain_sso_ybi = state.sso_login_uri
                let ma_ctcb_kiem_nhiem = this.ma_ctcb_kc_Store
                API.Login.AU_DSCTCB_SSO(access_token_sso_ybi, domain_sso_ybi, ma_ctcb_kiem_nhiem, this.props.dispatch).then((response) => {
                    console.log("AU_DSCTCB_SSO: " + response)
                    if (response) {
                        this.checkedOTPSuccess()
                    } else {
                        Toast.show({ text: "Đăng nhập thất bại!", type: "warning", buttonText: "Đóng", position: "top", duration: 2000 })
                    }
                })
            }
        })
    }

    onChangeTextTK = (text) => {
        this.setState({ TaiKhoan: text })
    }

    onValueChangeDonVi = (value) => {
        this.setState({ Login_MaDonVi: value })
    }

    onChangeTextMK = (text) => {
        this.setState({ MatKhau: text })
    }

    onPressCHK = () => {
        this.setState({ Checked: !this.state.Checked })
    }

    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }

    checkedOTPSuccess = async () => {
        if (this.state.Checked) {
            await AsyncStorage.setItem("chkSaveLogin", "true")
            await AsyncStorage.setItem("Url_Api_SaveLogin", this.state.url_api)
        } else {
            await AsyncStorage.setItem("chkSaveLogin", "false")
            await AsyncStorage.setItem("Url_Api_SaveLogin", "")
        }
        await AsyncStorage.setItem("web_version", this.state.web_version)
        await AsyncStorage.setItem("TaiKhoan", this.state.TaiKhoan)
        await AsyncStorage.setItem("MatKhau", this.state.MatKhau)
        setUserSettings(this.props.navigation, this.props.dispatch)
    }

    getOTPString2 = () => {
        let ma_don_vi_quan_tri = global.ma_don_vi_quan_tri
        let di_dong_can_bo = global.di_dong_can_bo;
        let username = global.username;
        API.Login.AU_GETOTPCODE(ma_don_vi_quan_tri, di_dong_can_bo, username).then((response) => {
            this.stringCode = response
            this.setModalVisible(true)
        })
    }

    render() {

        this.styles = StyleSheet.create({
            container: {
                flex: 1,
                alignItems: 'center',
                justifyContent: "flex-start"
            },
            text: {
                color: 'white',
                backgroundColor: 'transparent',
                fontSize: this.props.fontSize.FontSizeNorman
            },
            image: {
                width: 24,
                height: 24,
            },
            item: {
                margin: 10,
                borderBottomWidth: 0,
            },
            input: {
                flex: 1,
                marginHorizontal: 20,
                paddingLeft: 45,
                borderWidth: 0.5,
                borderColor: '#ffffff',
                borderRadius: 22,
                color: '#ffffff',
                fontSize: this.props.fontSize.FontSizeNorman / PixelRatio.getFontScale()
            },
            inputWrapper: {
                flex: 1,
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
                height: 45
            },
            inlineImg: {
                position: 'absolute',
                zIndex: 99,
                width: 23,
                height: 23,
                left: 32,
                top: 11
            }
        });

        let behavior = Platform.OS === "ios" ? "padding" : "null"
        return (
            <Container style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
                {
                    (this.state.haveSSO) && (
                        <View style={{ position: "absolute", width: 0, height: 0, top: 0, left: 0, zIndex: 999 }}>
                            <WebView
                                //ref={WebView => { this.wvLogoutSSO = WebView }}
                                source={{ uri: this.state.url_logout_sso }}
                                style={{ flex: 1 }}
                            />
                        </View>
                    )
                }
                {
                    (this.state.haveSSO) && (
                        <LoginSSO
                            ref={(ref) => { this.wvLoginSSO = ref }}
                            getSSOURL={this.getSSOURL}
                            getRedirectURI={this.getRedirectURI}
                            callbackLoginSSO={(code) => { this.callbackLoginSSO(code) }}
                        />
                    )
                }
                <ModalDangThucHien
                    isVisible={this.state.isLoading}
                    title={"Đang lấy danh sách đơn vị"}
                />
                <Wallpaper>
                    <KeyboardAvoidingView style={{ flex: 1, opacity: 1 }} accessible={false} behavior={behavior}>
                        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                            <Image source={AppConfig.LogoLogin} style={{ width: 150, height: 120, resizeMode: "stretch" }} />
                        </View>
                        {
                            (this.state.visibleForm) && (
                                <Form style={{ flex: 1.5 }}>
                                    <Item style={this.styles.item}>
                                        <TouchableOpacity onPress={this.showDonVi} style={[this.styles.inputWrapper, { justifyContent: "flex-start", marginLeft: 22, marginRight: 22, borderWidth: 0.5, borderColor: "#ffffff", borderRadius: 22, padding: 10, height: 44 }]}>
                                            <Image source={serverImg} tintColor="#FFFFFF" style={[this.styles.inlineImg, { left: 10 }]} />
                                            <View style={{ paddingLeft: 30 }}>
                                                <Text style={[this.styles.text]}>{this.state.ten_don_vi}</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </Item>
                                    <Item style={this.styles.item}>
                                        <View style={this.styles.inputWrapper}>
                                            <Image source={usernameImg} style={this.styles.inlineImg} />
                                            <CustomTextInput
                                                ref={ref => this.txtTaiKhoan = ref}
                                                style={this.styles.input}
                                                value={this.state.TaiKhoan}
                                                onChangeText={(text) => { this.setState({ TaiKhoan: text }) }}
                                                placeholder="Tài khoản"
                                                underlineColorAndroid={"transparent"}
                                                onRef={ref => this.input = ref}
                                                placeholderStyle={{ top: 10, left: 65, fontSize: this.props.fontSize.FontSizeNorman, color: "#fff" }}
                                                onSubmitEditing={() => { this.txtMatKhau.focus() }}
                                            />

                                        </View>
                                    </Item>
                                    <Item style={this.styles.item}>
                                        <View style={this.styles.inputWrapper}>
                                            <Image source={passwordImg} style={this.styles.inlineImg} />

                                            <CustomTextInput
                                                ref={ref => this.txtMatKhau = ref}
                                                secureTextEntry={true}
                                                style={this.styles.input}
                                                value={this.state.MatKhau}
                                                onChangeText={(text) => { this.setState({ MatKhau: text }) }}
                                                placeholder="Mật khẩu"
                                                underlineColorAndroid={"transparent"}
                                                onRef={ref => this.input = ref}
                                                placeholderStyle={{ top: 10, left: 65, fontSize: this.props.fontSize.FontSizeNorman, color: "#fff" }}
                                                onSubmitEditing={this._onPress}
                                            />
                                        </View>
                                    </Item>
                                    <Item style={this.styles.item}>
                                        <View style={[this.styles.inputWrapper, { justifyContent: "flex-start", marginLeft: 24, height: 25 }]}>
                                            <CheckBox checked={this.state.Checked} color={'rgba(255, 255, 255, 0.4)'} onPress={this.onPressCHK}></CheckBox>
                                            <Text style={[this.styles.text, { marginLeft: 21 }]} onPress={this.onPressCHK}>Ghi nhớ đăng nhập</Text>
                                        </View>
                                    </Item>
                                    <Item style={this.styles.item}>
                                        <TouchableOpacity onPress={this._onPress} style={[this.styles.inputWrapper, { marginLeft: 22, marginRight: 22, borderRadius: 22, height: 42, backgroundColor: "#1976D2" }]}>
                                            <Text style={this.styles.text}>ĐĂNG NHẬP</Text>
                                        </TouchableOpacity>
                                    </Item>
                                    {
                                        (this.state.haveSSO) && (
                                            <Item style={this.styles.item}>
                                                <TouchableOpacity onPress={this._onPressLoginSSO} style={[this.styles.inputWrapper, { marginLeft: 22, marginRight: 22, borderRadius: 22, height: 42, backgroundColor: "#1976D2" }]}>
                                                    <Text style={this.styles.text}>ĐĂNG NHẬP SSO</Text>
                                                </TouchableOpacity>
                                            </Item>
                                        )
                                    }
                                </Form>
                            )
                        }
                        <ModalOTP
                            modalVisible={this.state.modalVisible}
                            parentModal={this}
                            stringCode={this.stringCode}
                        >
                        </ModalOTP>
                    </KeyboardAvoidingView>
                </Wallpaper>
            </Container>
        );
    }
}

function mapStateToProps(state) {
    return {
        cb_ds_don_vi_app: state.cb_ds_don_vi_app.data,
        fontSize: state.fontSize,
    }
}

export default connect(mapStateToProps)(LoginForm)