import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, FlatList, BackHandler, Modal, View, Dimensions, Linking, ScrollView, Platform, AsyncStorage, Alert, Image } from "react-native"
import { Container, Content, Text, Card, CardItem, Thumbnail, Icon, Button } from "native-base"
import { connect } from "react-redux"
import { DrawerActions } from "react-navigation"
import HeaderWithLeft from "../components/Header/HeaderWithLeft"
import DeviceInfo from "react-native-device-info"
import FooterTab from "../components/Footer/FooterTab"
import { WebView } from "react-native-webview"
import { AppConfig } from "../AppConfig"
import { CheckSession } from "../untils/NetInfo"
import { resetStack, resetStackHome, checkVersionWeb, getNVOld, getNVNew } from "../untils/TextUntils"
import * as LogChange from "../../logChange.json"
import API_Login from "../networks/API_Login"
import BatchedBridge from 'react-native/Libraries/BatchedBridge/BatchedBridge';
import moment from "moment";

class HomeScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            Badge: [0, 0, 0, 0, 0, 0],
            showUpdateNote: false,
            onChangeSize: false,
            modalActive: false,
            dsTongCMU: [0, 0]
        }
        this.nvVBDen_XuLy = 0
        this.nvVBDen_Duyet = 0
        this.nvVBDi_XuLy = 0
        this.nvVBDi_Duyet = 0
        this.nvTTDH = 0
        this.nvVBNoiBo = 0
        this.arrNhacViec = [
            {
                "Key": "VBDenVanThu",
                "TenNhacViec": "Sổ văn bản đến",
                "isShow": global.van_thu === 1 ? true : false,
                "indexNhacViec": 0,
                "HinhAnh": require("../assets/images/xulyvb.png")
            },
            {
                "Key": "VBDenDuyet",
                "TenNhacViec": "Duyệt văn bản đến",
                "isShow": global.lanh_dao === 1 ? true : false,
                "indexNhacViec": 1,
                "HinhAnh": require("../assets/images/duyetvb.png")
            },
            {
                "Key": "VBDenXuLy",
                "TenNhacViec": "Xử lý văn bản đến",
                "isShow": true,
                "indexNhacViec": 2,
                "HinhAnh": require("../assets/images/xulyvb.png")
            },
            {
                "Key": "VBDiDuyet",
                "TenNhacViec": "Duyệt văn bản đi",
                "isShow": global.lanh_dao === 1 ? true : false,
                "indexNhacViec": 3,
                "HinhAnh": require("../assets/images/duyetvb.png")
            },
            {
                "Key": "VBDeTheoDoi",
                "TenNhacViec": "Văn bản đến theo dõi",
                "isShow": global.vbde_cv_theodoi_vb_mobile == 1 ? true : false,
                "indexNhacViec": 9,
                "HinhAnh": require("../assets/images/search.png")
            },
            {
                "Key": "VBDiXuLy",
                "TenNhacViec": "Xử lý văn bản đi",
                "isShow": true,
                "indexNhacViec": 4,
                "HinhAnh": require("../assets/images/xulyvb.png")
            },
            {
                "Key": "TTDH",
                "TenNhacViec": "Thông tin điều hành",
                "isShow": true,
                "indexNhacViec": 5,
                "HinhAnh": require("../assets/images/ttdh.png")
            },
            {
                "Key": "VBNB",
                "TenNhacViec": "Văn bản nội bộ",
                "isShow": true,
                "indexNhacViec": 6,
                "HinhAnh": require("../assets/images/vbnb.png")
            },
            {
                "Key": "VB7N",
                "TenNhacViec": "VB đi đã phát hành 7 ngày",
                "isShow": global.nv_nhac_viec_cmu == 1 ? true : false,
                "indexNhacViec": 7,
                "HinhAnh": require("../assets/images/reportcmu.png")
            },
            {
                "Key": "VBDP1Y",
                "TenNhacViec": "VB đi đã phát hành năm " + moment().format('YYYY'),
                "isShow": global.nv_nhac_viec_cmu == 1 ? true : false,
                "indexNhacViec": 8,
                "HinhAnh": require("../assets/images/LH.png")
            },
            {
                "Key": "XYKTT",
                "TenNhacViec": "Xin ý kiến",
                "isShow": global.xinykien,
                "indexNhacViec": 10,
                "HinhAnh": require("../assets/images/xinykien.png"),
            }
        ]
        this.style = StyleSheet.create({

        })
    }

    componentDidMount = () => {
        this.didBlurSubscription = this.props.navigation.addListener('didFocus', payload => {
            CheckSession(this.props.navigation)
            this.onLoadDSNVCMU()
            this.loadNhacViec()

        })
        this.backHandler = BackHandler.addEventListener("hardwareBackPress", () => {
            return false
        })
        Dimensions.addEventListener("change", this.onChangeSize);
        this.checkUpdateNode()
        //this.checkUpdateVersion()
    };

    componentWillUnmount = () => {
        this.didBlurSubscription.remove()
        //this.backHandler.remove()
        Dimensions.removeEventListener("change", this.onChangeSize);
    };

    checkUpdateVersion = (callback) => {
        global.storeURL = Platform.OS == 'ios' ? AppConfig.iOSStoreURL : AppConfig.AndroidStoreURL
        var xhr = new XMLHttpRequest();
        xhr.withCredentials = true;
        xhr.open("GET", AppConfig.AppVersionCheckURL, true)
        xhr.onload = async function (e) {
            if (xhr.readyState === 4) {

                if (xhr.status === 200 && xhr.response) {
                    try {
                        let data = JSON.parse(xhr.response)
                        if (data.modalActive) {
                            this.setState({ modalData: data.modalData, modalTypeButton: data.modalTypeButton, modalUrlData: data.modalUrlData, modalAction: data.modalAction }, () => {
                                this.setState({ modalActive: data.modalActive })
                            })
                        } else {
                            callback()
                        }
                    } catch (error) {
                        callback()
                    }
                } else {
                    callback()
                }
            }
        }.bind(this)
        xhr.send(null);
    }

    renderButtonModal = () => {
        if (this.state.modalTypeButton === 2) {
            return (
                <View style={{ width: "100%", height: 60, flexDirection: "row", justifyContent: "center", alignItems: "center", alignSelf: "center", position: "absolute", bottom: 0 }}>
                    <Button style={{ alignSelf: "center", width: 100, marginRight: 5, justifyContent: "center" }} onPress={() => { this.setState({ modalActive: false }, () => { this.setState({ modalActive: false }) }) }}>
                        <Text>Đóng</Text>
                    </Button>
                    <Button style={{ alignSelf: "center", width: 100, marginLeft: 5, justifyContent: "center" }} onPress={() => { Linking.openURL(this.state.modalAction) }}>
                        <Text>Cập nhật</Text>
                    </Button>
                </View>
            )
        } else {
            return (
                <View style={{ width: "100%", height: 60, flexDirection: "row", justifyContent: "center", alignItems: "center", alignSelf: "center", position: "absolute", bottom: 0 }}>
                    <Button style={{ alignSelf: "center", width: 100, marginLeft: 5, justifyContent: "center" }} onPress={() => { Linking.openURL(this.state.modalAction) }}>
                        <Text>Cập nhật</Text>
                    </Button>
                </View>
            )
        }
    }

    checkUpdateNode = async () => {
        await AsyncStorage.getItem("AppVersion", (error, result) => {
            if (result !==  DeviceInfo.getVersionSync()) {
                this.setState({ showUpdateNote: true })
            }
        })
    }

    onChangeSize = () => {
        this.setState({ onChangeSize: !this.state.onChangeSize })
    }

    onLoadDSNVCMU = () => {
        API_Login.AU_DSNVCMU_BYCB(global.ma_ctcb_kc).then(response => {
            this.setState({ dsTongCMU: [response[0], response[1]] });
        })
    }

    loadNhacViec = () => {
        let cb_dsnvcb = this.props.cb_dsnvcb
        if (checkVersionWeb(21)) {
            let nvSoVBDen = getNVNew("vbde", "van-ban-den/so-van-ban-den?page=1&t=so_vb_den_dien_tu_cua_vt", cb_dsnvcb)
            let nvVBDen_Duyet = getNVNew("vbde", "van-ban-den/duyet-van-ban-den?page=1&t=vb_den_cho_duyet_cua_ld", cb_dsnvcb) + getNVNew("vbde", "van-ban-den/duyet-van-ban-den?page=1&t=vb_den_uy_quyen_duyet_cua_ld", cb_dsnvcb)
            let nvVBDen_XuLy = getNVNew("vbde", "van-ban-den/van-ban-den-chua-xu-ly?page=1", cb_dsnvcb) + getNVNew("vbde", "van-ban-den/van-ban-den-dang-xu-ly?page=1", cb_dsnvcb)
            let nvVBDi_Duyet = getNVNew("vbdi", "van-ban-di/duyet-van-ban-di?page=1&t=vb_di_cho_duyet_cua_ld", cb_dsnvcb) + getNVNew("vbdi", "van-ban-di/duyet-van-ban-di?page=1&t=vb_di_duoc_uy_quyen_cua_ld", cb_dsnvcb)
            let nvVBDi_XuLy = getNVNew("vbdi", "van-ban-di/van-ban-di-cho-xu-ly?page=1", cb_dsnvcb)
            let nvVBDen_theodoi = getNVNew("vbde", "van-ban-den/van-ban-den-theo-doi-xu-ly?page=1", cb_dsnvcb)
            if (global.AU_ROOT && global.AU_ROOT.includes("phutho")) {
                nvVBDi_XuLy = nvVBDi_XuLy + getNVNew("vbdi", "van-ban-di/van-ban-di-dph-chua-xem-pto?page=1", cb_dsnvcb)
            } else {
                nvVBDi_XuLy = nvVBDi_XuLy + getNVNew("vbdi", "van-ban-di/van-ban-di-da-phat-hanh?page=1", cb_dsnvcb)
            }
            let nvTTDH = getNVNew("khac", "thong-diep/thong-tin-dieu-hanh-chua-xem?page=1", cb_dsnvcb)
            let nvVBNoiBo = getNVNew("vbnb", "van-ban-noi-bo/van-ban-noi-bo-da-nhan?page=1", cb_dsnvcb)
	          let nvXYKTT = getNVNew("khac", "xin-y-kien/danh-sach-xin-y-kien-chua-xu-ly?page=1", cb_dsnvcb);
            this.setState({ Badge: [nvSoVBDen, nvVBDen_Duyet, nvVBDen_XuLy, nvVBDi_Duyet, nvVBDi_XuLy, nvTTDH, nvVBNoiBo, 0, 0, nvVBDen_theodoi, nvXYKTT] })
        } else {
            let nvVBDen_Duyet = getNVOld("duyet-van-ban-den?page?t", "vb_den_cho_duyet_cua_ld", cb_dsnvcb) + getNVOld("duyet-van-ban-den?page?t", "vb_den_uy_quyen_duyet_cua_ld", cb_dsnvcb)
            let nvVBDen_XuLy = getNVOld("van-ban-den-chua-xu-ly?page", "", cb_dsnvcb) + getNVOld("van-ban-den-dang-xu-ly?page", "", cb_dsnvcb)
            let nvVBDi_Duyet = getNVOld("duyet-van-ban-di?page?t", "vb_di_cho_duyet_cua_ld", cb_dsnvcb) + getNVOld("duyet-van-ban-di?page?t", "vb_di_duoc_uy_quyen_cua_ld", cb_dsnvcb)
            let nvVBDi_XuLy = getNVOld("van-ban-di-cho-xu-ly?page", "", cb_dsnvcb) + getNVOld("van-ban-di-da-phat-hanh?page", "", cb_dsnvcb)
            let nvTTDH = getNVOld("thong-tin-dieu-hanh-da-nhan?page", "", cb_dsnvcb)
            let nvVBNoiBo = getNVOld("van-ban-noi-bo-da-nhan?page", "", cb_dsnvcb)
            this.setState({ Badge: [0, nvVBDen_Duyet, nvVBDen_XuLy, nvVBDi_Duyet, nvVBDi_XuLy, nvTTDH, nvVBNoiBo] })
        }
    }

    closeDrawer = () => {
        this.drawer._root.close()
    }

    openDrawer = () => {
        this.drawer._root.open()
    }

    renderFeedbackItems = ({ item, index }) => {
        if (!item.Key) return <Card noShadow style={{ flex: 1, borderTopWidth: 0, borderBottomWidth: 0, borderLeftWidth: 0, borderRightWidth: 0 }} key={item.Key}></Card>
        return (
            <Card noShadow style={this.styles.card} key={item.Key}>
                {/* <TouchableOpacity style={this.styles.btnItem} onPress={() => { this.props.navigation.dispatch(resetStackHome(item.Key)) }}> */}
                <TouchableOpacity style={this.styles.btnItem} onPress={() => { this.props.navigation.push(item.Key) }}>
                    <CardItem style={this.styles.cardItem1}>
                        <Thumbnail style={this.styles.thundmail} source={item.HinhAnh} />
                    </CardItem>
                    <CardItem style={this.styles.cardItemTNV}>
                        <Text style={this.styles.txtTNV}>{item.TenNhacViec}</Text>
                    </CardItem>
                </TouchableOpacity>
                {
                    (this.state.Badge[item.indexNhacViec] != 0) && (
                        <View style={this.styles.viewBadge}>
                            <Text style={this.styles.txtBadge}>{this.state.Badge[item.indexNhacViec]}</Text>
                        </View>
                    )
                }
            </Card>
        )
    }

  renderFeedbackItems_NAN = ({ item, index }) => {
    if (!item.Key) return null;
    return (
      <TouchableOpacity
        style={this.styles.btnItemNAN}
        onPress={() => {
          this.props.navigation.push(item.Key);
        }}
      >
        <View style={this.styles.imageNAN}>
          <Image
            source={item.HinhAnh}
            style={{ width: "100%", height: "100%" }}
          />
        </View>
        <View style={this.styles.textNAN}>
          <Text style={[this.styles.txtTNV, { fontWeight: "bold" }]}>
            {item.TenNhacViec}
          </Text>
        </View>
        <View style={this.styles.viewContentNAN}>
          {this.state.Badge[item.indexNhacViec] != 0 ? (
            <View style={this.styles.viewBadgeNAN}>
              <Text style={this.styles.txtBadge}>
                {this.state.Badge[item.indexNhacViec] != 0
                  ? this.state.Badge[item.indexNhacViec]
                  : ""}
              </Text>
            </View>
          ) : null}
        </View>
        <View style={this.styles.viewContentNAN}>
          <Icon
            name={"chevron-right"}
            type="MaterialIcons"
            style={{ color: "gray" }}
          />
        </View>
      </TouchableOpacity>
    );
  };

    toggleModalUpdateNote = () => {
        if (this.state.showUpdateNote) {
            AsyncStorage.setItem("AppVersion", DeviceInfo.getVersionSync(), () => {
                this.setState({ showUpdateNote: false })
            })
        }
    }

    renderInfoUpdate = () => {
        if (this.state.showUpdateNote) {
            let OS = Platform.OS.toString()
            let Version =  DeviceInfo.getVersionSync()
            let updateNote = LogChange[OS][Version] ? LogChange[OS][Version] : "Không có thông tin"
            return (
                <Modal
                    transparent={true}
                    animationType="slide"
                    onRequestClose={this.toggleModalUpdateNote}
                    visible={this.state.showUpdateNote}
                    supportedOrientations={["landscape", "landscape-left", "landscape-right", "portrait", "portrait-upside-down"]}
                >
                    <View style={this.styles.viewContainer}>
                        <View style={this.styles.viewContainer1}>
                            <View style={this.styles.viewTitleUpdate}>
                                <View style={this.styles.viewClose}>
                                    <Icon name={"close"} type="MaterialIcons" onPress={this.toggleModalUpdateNote} style={this.styles.iconClose} />
                                </View>
                                <View style={this.styles.viewTitle}>
                                    <Text style={this.styles.txtTitle}>Nội dung cập nhật</Text>
                                </View>
                            </View>
                            <ScrollView contentContainerStyle={this.styles.scrollview}>
                                <Text style={this.styles.txtUpdateNote}>{updateNote}</Text>
                            </ScrollView>
                        </View>
                    </View>
                </Modal>
            )
        } else {
            <View></View>
        }
    }

    render() {
        let screenWidth = Dimensions.get("screen").width
        let screenHeight = Dimensions.get("screen").height
        this.styles = StyleSheet.create({
            //Render
            container: {
                flex: 1,
                backgroundColor: "#FFFFFF"
            },
            cardItem: {
                flex: 1
            },
            flatList: {
                flex: 1,
                marginLeft: 10,
                marginRight: 10
            },
            //End Render

            //UpdateInfo
            viewContainer: {
                flex: 1,
                backgroundColor: "rgba(33,33,33,0.8)"
            },
            viewContainer1: {
                alignSelf: 'center',
                marginTop: screenHeight * 0.2,
                width: screenWidth * 0.8,
                maxHeight: screenHeight * 0.6,
                borderRadius: 10,
                borderWidth: 1.5,
                borderColor: AppConfig.blueBackground,
                backgroundColor: "white"
            },
            viewTitleUpdate: {
                flexDirection: 'row',
                width: "100%", height: 40,
                backgroundColor: AppConfig.blueBackground,
                borderTopLeftRadius: 8,
                borderTopRightRadius: 8
            },
            scrollview: {
                padding: 10
            },
            viewClose: {
                width: 40,
                height: 40,
                top: 0,
                left: 0,
                alignItems: 'center',
                justifyContent: "center"
            },
            iconClose: {
                color: "red"
            },
            viewTitle: {
                flex: 1,
                alignItems: 'center',
                justifyContent: "center"
            },
            txtTitle: {
                marginLeft: -40,
                fontSize: this.props.fontSize.FontSizeNorman,
                color: "white"
            },
            txtUpdateNote: {
                flex: 1,
                fontSize: this.props.fontSize.FontSizeNorman
            },
            //End UpdateInfo

            //RenderItem
            card: {
                flex: 1,
                alignItems: 'center',
                justifyContent: "center",
                flexWrap: "wrap",
                borderRadius: 5,
                borderColor: "blue"
            },
            btnItemNAN: {
                flex: 1,
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "space-between",
                borderRadius: 15,
                borderBottomWidth: 0.5,
                borderColor: "gray",
                padding: 10,
                marginTop: 10,
            },
            textNAN: {
                flex: 1,
                alignItems: "flex-start",
                marginLeft: 15,
            },
            viewContentNAN: {
                width: 40,
                height: 60,
                justifyContent: "center",
                alignItems: "center",
            },
            imageNAN: {
                width: 60,
                height: 60,
                borderRadius: 30,
            },
            viewBadgeNAN: {
                height: 30,
                maxWidth: 200,
                minWidth: 40,
                backgroundColor: "red",
                borderRadius: 12,
                alignItems: "center",
                justifyContent: "center",
                paddingLeft: 6,
                paddingRight: 6,
            },
            btnItem: {
                flex: 1,
                alignItems: 'center',
                justifyContent: "center",
                flexWrap: "wrap"
            },
            cardItem1: {
                flex: 1,
                marginTop: 10,
                alignItems: "flex-start",
            },
            thundmail: {
                height: 100,
                width: 100,
                borderRadius: 50
            },
            cardItemTNV: {
                flex: 1,
                marginTop: 10,
                marginBottom: 15,
                marginLeft: 2,
                marginRight: 2,
                flexWrap: "wrap",
                alignItems: 'center',
                flexDirection: 'row',
            },
            txtTNV: {
                fontFamily: AppConfig.fontFamily,
                fontSize: this.props.fontSize.FontSizeNorman,
                textAlign: "center",
            },
            viewBadge: {
                position: "absolute",
                top: 5,
                right: (Platform.OS === "ios" && Platform.isPad) ? 100 : 5,
                height: 30,
                maxWidth: 200,
                minWidth: 40,
                backgroundColor: "red",
                borderRadius: 12,
                alignItems: "center",
                justifyContent: "center",
                paddingLeft: 6,
                paddingRight: 6,
            },
            txtBadge: {
                fontSize: this.props.fontSize.FontSizeSmall,
                color: "white"
            },
            //End RenderItem
        })

        let data = this.arrNhacViec.filter(el => el.isShow === true)
        if (data.length % 2 === 1) {
            data.push({ "Key": null })
        }
        // append nhắc việc riêng cmu
        if (global.nv_nhac_viec_cmu == 1) {
            let { Badge, dsTongCMU } = this.state;
            Badge[7] = dsTongCMU[0];
            Badge[8] = dsTongCMU[1];
        }
        if (this.state.modalActive) {
            return (
                <Container>
                    <View style={{ flex: 1, backgroundColor: "rgba(255,255,255,0.15)" }}>
                        <View style={{ flex: 1, margin: 30, padding: 5, borderRadius: 15, backgroundColor: "white", borderColor: "red" }}>
                            <View style={{ flex: 1, marginBottom: 60, borderColor: "blue" }}>
                                <WebView
                                    ref={(ref) => { this.myWebView = ref }}
                                    source={{ uri: this.state.modalUrlData }}
                                    domStorageEnabled={true}
                                    decelerationRate="normal"
                                    mixedContentMode="always"
                                    originWhitelist={["*"]}
                                    automaticallyAdjustContentInsets={true}
                                    scalesPageToFit={true}
                                    bounces={false}
                                />
                            </View>
                            {
                                this.renderButtonModal()
                            }
                        </View>
                    </View>
                </Container>
            )
        }

        return (
            <Container style={this.styles.container}>
                <HeaderWithLeft
                    title="VNPT iOffice 4.1"
                    buttonName="menu"
                    onPressLeftButton={_ => { this.props.navigation.dispatch(DrawerActions.toggleDrawer()) }}
                />
                {this.renderInfoUpdate()}
                {/* <TouchableOpacity onPress={() => { this.props.dispatch({ type: "BIG_FONT" }) }}>
                    <Text>Tang Size</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => { this.props.dispatch({ type: "SMALL_FONT" }) }}>
                    <Text>Giam Size</Text>
                </TouchableOpacity>
                <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between" }}>
                    <Text>{this.props.fontSize.FontSizeSmall}</Text>
                    <Text>{this.props.fontSize.FontSizeNorman}</Text>
                    <Text>{this.props.fontSize.FontSizeLarge}</Text>
                    <Text>{this.props.fontSize.FontSizeXLarge}</Text>
                    <Text>{this.props.fontSize.FontSizeXXLarge}</Text>
                    <Text>{this.props.fontSize.FontSizeXXXLarge}</Text>
                </View> */}
        {global.AU_ROOT.includes("nghean-api") ? (
          <Content>
            <CardItem style={this.styles.cardItem}>
              <FlatList
                data={data}
                extraData={this.state}
                keyExtractor={(item, index) => item.Key}
                numColumns={1}
                renderItem={this.renderFeedbackItems_NAN}
                style={this.styles.flatList}
                removeClippedSubviews={true}
              />
            </CardItem>
          </Content>
        ) : (
          <Content>
            <CardItem style={this.styles.cardItem}>
              <FlatList
                data={data}
                extraData={this.state}
                keyExtractor={(item, index) => item.Key}
                numColumns={2}
                renderItem={this.renderFeedbackItems}
                style={this.styles.flatList}
                removeClippedSubviews={true}
              />
            </CardItem>
          </Content>
        )}

        <FooterTab tabActive="trangchu" navigation={this.props.navigation} />
      </Container>
    );
  }
}

function mapStateToProps(state) {
    return {
        cb_dsnvcb: state.cb_dsnvcb.data,
        fontSize: state.fontSize,
    }
}

export default connect(mapStateToProps)(HomeScreen)
