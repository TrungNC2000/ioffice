import React, { Component } from 'react';
import { StyleSheet, AsyncStorage, TouchableOpacity } from "react-native"
import { Container, Toast, Text, CardItem, ActionSheet, Separator, Content } from "native-base"
import { connect } from "react-redux"
import { DrawerActions } from "react-navigation"
import { AppConfig } from "../AppConfig"
import HeaderWithLeftRight from "../components/Header/HeaderWithLeftRight"
import { checkVersionWeb, ToastSuccess, setFontSize } from "../untils/TextUntils"
import { CheckSession } from "../untils/NetInfo"

const Default_US_Xem_Pdf_iOS = AppConfig.Default_US_Xem_Pdf_iOS
const Arr_US_Xem_Pdf_iOS = AppConfig.Arr_US_Xem_Pdf_iOS
const Default_US_First_Load_Page = AppConfig.Default_US_First_Load_Page
const Arr_US_First_Load_Page = AppConfig.Arr_US_First_Load_Page
const Default_US_Mobile_Pki_Mode = AppConfig.Default_US_Mobile_Pki_Mode
const Arr_US_Mobile_Pki_Mode = AppConfig.Arr_US_Mobile_Pki_Mode
const Default_US_Mobile_Pki_Side = AppConfig.Default_US_Mobile_Pki_Side
const Arr_US_Mobile_Pki_Side = AppConfig.Arr_US_Mobile_Pki_Side
const Default_US_VBDEN_Mot_Can_Bo_XLC = AppConfig.Default_US_VBDEN_Mot_Can_Bo_XLC
const Arr_US_VBDEN_Mot_Can_Bo_XLC = AppConfig.Arr_US_VBDEN_Mot_Can_Bo_XLC
const Default_US_VBDEN_Tree_Default_Check = AppConfig.Default_US_VBDEN_Tree_Default_Check
const Arr_US_VBDEN_Tree_Default_Check = AppConfig.Arr_US_VBDEN_Tree_Default_Check
const Default_US_FILE_List_Ngang_Doc = AppConfig.Default_US_FILE_List_Ngang_Doc
const Arr_US_FILE_List_Ngang_Doc = AppConfig.Arr_US_FILE_List_Ngang_Doc
const Default_US_Show_Alert_On_Success = AppConfig.Default_US_Show_Alert_On_Success
const Arr_US_Show_Alert_On_Success = AppConfig.Arr_US_Show_Alert_On_Success
const Default_US_Type_Font_Size = AppConfig.Default_US_Type_Font_Size
const Arr_US_Type_Font_Size = AppConfig.Arr_US_Type_Font_Size
const Default_US_Time_Local_Notify = AppConfig.Default_US_Time_Local_Notify
const Arr_US_Time_Local_Notify = AppConfig.Arr_US_Time_Local_Notify

class UserSettings extends Component {
    constructor(props) {
        super(props);
        this.state = {
            US_Xem_Pdf_iOS: Default_US_Xem_Pdf_iOS,
            US_First_Load_Page: Default_US_First_Load_Page,
            US_Mobile_Pki_Mode: Default_US_Mobile_Pki_Mode,
            US_Mobile_Pki_Side: Default_US_Mobile_Pki_Side,
            US_VBDEN_Mot_Can_Bo_XLC: Default_US_VBDEN_Mot_Can_Bo_XLC,
            US_VBDEN_Tree_Default_Check: Default_US_VBDEN_Tree_Default_Check,
            US_FILE_List_Ngang_Doc: Default_US_FILE_List_Ngang_Doc,
            US_Show_Alert_On_Success: Default_US_Show_Alert_On_Success,
            US_Type_Font_Size: Default_US_Type_Font_Size,
            US_Time_Local_Notify: Default_US_Time_Local_Notify,
        }
        this.US_Xem_Pdf_iOS = Default_US_Xem_Pdf_iOS
        this.US_First_Load_Page = Default_US_First_Load_Page
        this.US_Mobile_Pki_Mode = Default_US_Mobile_Pki_Mode
        this.US_Mobile_Pki_Side = Default_US_Mobile_Pki_Side
        this.US_VBDEN_Mot_Can_Bo_XLC = Default_US_VBDEN_Mot_Can_Bo_XLC
        this.US_VBDEN_Tree_Default_Check = Default_US_VBDEN_Tree_Default_Check
        this.US_FILE_List_Ngang_Doc = Default_US_FILE_List_Ngang_Doc
        this.US_Show_Alert_On_Success = Default_US_Show_Alert_On_Success
        this.US_Type_Font_Size = Default_US_Type_Font_Size
        this.US_Time_Local_Notify = Default_US_Time_Local_Notify
    }

    componentDidMount = () => {
        this.didBlurSubscription = this.props.navigation.addListener('didFocus', payload => { CheckSession(this.props.navigation) })
        this.getSettings()
    };

    componentWillUnmount = () => {
        this.didBlurSubscription.remove()
    };

    getSettings = async () => {
        await AsyncStorage.getItem("US_Xem_Pdf_iOS", (error, result) => {
            if (result === null) {
                result = Default_US_Xem_Pdf_iOS
            }
            this.US_Xem_Pdf_iOS = result
            global.US_Xem_Pdf_iOS = result
            this.setState({ US_Xem_Pdf_iOS: result })
        })
        await AsyncStorage.getItem("US_First_Load_Page", (error, result) => {
            if (result === null) {
                result = Default_US_First_Load_Page
            }
            this.US_First_Load_Page = result
            global.US_First_Load_Page = result
            this.setState({ US_First_Load_Page: result })
        })
        await AsyncStorage.getItem("US_Mobile_Pki_Mode", (error, result) => {
            if (result === null) {
                result = Default_US_Mobile_Pki_Mode
            }
            this.US_Mobile_Pki_Mode = result
            global.US_Mobile_Pki_Mode = result
            this.setState({ US_Mobile_Pki_Mode: result })
        })
        await AsyncStorage.getItem("US_Mobile_Pki_Side", (error, result) => {
            if (result === null) {
                result = Default_US_Mobile_Pki_Side
            }
            this.US_Mobile_Pki_Side = result
            global.US_Mobile_Pki_Side = result
            this.setState({ US_Mobile_Pki_Side: result })
        })
        await AsyncStorage.getItem("US_VBDEN_Mot_Can_Bo_XLC", (error, result) => {
            if (result === null) {
                result = Default_US_VBDEN_Mot_Can_Bo_XLC
            }
            this.US_VBDEN_Mot_Can_Bo_XLC = result
            global.US_VBDEN_Mot_Can_Bo_XLC = result
            this.setState({ US_VBDEN_Mot_Can_Bo_XLC: result })
        })
        await AsyncStorage.getItem("US_VBDEN_Tree_Default_Check", (error, result) => {
            if (result === null) {
                result = Default_US_VBDEN_Tree_Default_Check
            }
            this.US_VBDEN_Tree_Default_Check = result
            global.US_VBDEN_Tree_Default_Check = result
            this.setState({ US_VBDEN_Tree_Default_Check: result })
        })
        await AsyncStorage.getItem("US_FILE_List_Ngang_Doc", (error, result) => {
            if (result === null) {
                result = Default_US_FILE_List_Ngang_Doc
            }
            this.US_FILE_List_Ngang_Doc = result
            global.US_FILE_List_Ngang_Doc = result
            this.setState({ US_FILE_List_Ngang_Doc: result })
        })
        await AsyncStorage.getItem("US_Type_Font_Size", (error, result) => {
            if (result === null) {
                result = Default_US_Type_Font_Size
            }
            this.US_Type_Font_Size = result
            global.US_Type_Font_Size = result
            this.setState({ US_Type_Font_Size: result })
        })
        await AsyncStorage.getItem("US_Show_Alert_On_Success", (error, result) => {
            if (result === null) {
                result = Default_US_Show_Alert_On_Success
            }
            this.US_Show_Alert_On_Success = result
            global.US_Show_Alert_On_Success = result
            this.setState({ US_Show_Alert_On_Success: result })
        })
        await AsyncStorage.getItem("US_Time_Local_Notify", (error, result) => {
            if (result === null) {
                result = Default_US_Time_Local_Notify
            }
            this.US_Time_Local_Notify = result
            global.US_Time_Local_Notify = result
            this.setState({ US_Time_Local_Notify: result })
        })
    }

    showXemPdfIOS = () => {
        ActionSheet.show(
            {
                options: Arr_US_Xem_Pdf_iOS.map(obj => {
                    return obj.Text
                }),
                title: "Chọn phương thức xem tệp tin"
            },
            buttonIndex => {
                if (buttonIndex !== undefined) {
                    this.setState({
                        US_Xem_Pdf_iOS: Arr_US_Xem_Pdf_iOS[buttonIndex].Value
                    })
                }
            }
        )
    }

    showFirstLoadPage = () => {
        let Arr_US_First_Load_Page_Temp = Arr_US_First_Load_Page
        if (global.lanh_dao !== 1) {
            Arr_US_First_Load_Page_Temp = Arr_US_First_Load_Page_Temp.filter(el => el.Value !== "VBDenDuyet")
            Arr_US_First_Load_Page_Temp = Arr_US_First_Load_Page_Temp.filter(el => el.Value !== "VBDiDuyet")
        }
        ActionSheet.show(
            {
                options: Arr_US_First_Load_Page_Temp.map(obj => {
                    return obj.Text
                }),
                title: "Chọn trang đầu tiên hiển thị"
            },
            buttonIndex => {
                if (buttonIndex !== undefined) {
                    this.setState({
                        US_First_Load_Page: Arr_US_First_Load_Page_Temp[buttonIndex].Value
                    })
                }
            }
        )
    }

    showMobilePkiMode = () => {
        ActionSheet.show(
            {
                options: Arr_US_Mobile_Pki_Mode.map(obj => {
                    return obj.Text
                }),
                title: "Chọn loại thông tin ký số"
            },
            buttonIndex => {
                if (buttonIndex !== undefined) {
                    this.setState({
                        US_Mobile_Pki_Mode: Arr_US_Mobile_Pki_Mode[buttonIndex].Value
                    })
                }
            }
        )
    }

    showMobilePkiSide = () => {
        ActionSheet.show(
            {
                options: Arr_US_Mobile_Pki_Side.map(obj => {
                    return obj.Text
                }),
                title: "Chọn loại định vị chữ ký"
            },
            buttonIndex => {
                if (buttonIndex !== undefined) {
                    this.setState({
                        US_Mobile_Pki_Side: Arr_US_Mobile_Pki_Side[buttonIndex].Value
                    })
                }
            }
        )
    }

    showVBDEN_Mot_Can_Bo_XLC = () => {
        ActionSheet.show(
            {
                options: Arr_US_VBDEN_Mot_Can_Bo_XLC.map(obj => {
                    return obj.Text
                }),
                title: "Chọn số cán bộ XLC trong VB Đến"
            },
            buttonIndex => {
                if (buttonIndex !== undefined) {
                    this.setState({
                        US_VBDEN_Mot_Can_Bo_XLC: Arr_US_VBDEN_Mot_Can_Bo_XLC[buttonIndex].Value
                    })
                }
            }
        )
    }

    showVBDEN_Tree_Default_Check = () => {
        ActionSheet.show(
            {
                options: Arr_US_VBDEN_Tree_Default_Check.map(obj => {
                    return obj.Text
                }),
                title: "Chọn loại vai trò mặc định"
            },
            buttonIndex => {
                if (buttonIndex !== undefined) {
                    this.setState({
                        US_VBDEN_Tree_Default_Check: Arr_US_VBDEN_Tree_Default_Check[buttonIndex].Value
                    })
                }
            }
        )
    }

    showFILE_List_Ngang_Doc = () => {
        ActionSheet.show(
            {
                options: Arr_US_FILE_List_Ngang_Doc.map(obj => {
                    return obj.Text
                }),
                title: "Chọn loại hướng hiển thị danh sách"
            },
            buttonIndex => {
                if (buttonIndex !== undefined) {
                    this.setState({
                        US_FILE_List_Ngang_Doc: Arr_US_FILE_List_Ngang_Doc[buttonIndex].Value
                    })
                }
            }
        )
    }

    showShow_Alert_On_Success = () => {
        ActionSheet.show(
            {
                options: Arr_US_Show_Alert_On_Success.map(obj => {
                    return obj.Text
                }),
                title: "Chọn loại hiển thị thông báo"
            },
            buttonIndex => {
                if (buttonIndex !== undefined) {
                    this.setState({
                        US_Show_Alert_On_Success: Arr_US_Show_Alert_On_Success[buttonIndex].Value
                    })
                }
            }
        )
    }

    showType_Font_Size = () => {
        ActionSheet.show(
            {
                options: Arr_US_Type_Font_Size.map(obj => {
                    return obj.Text
                }),
                title: "Chọn loại hiển thị thông báo"
            },
            buttonIndex => {
                if (buttonIndex !== undefined) {
                    this.setState({
                        US_Type_Font_Size: Arr_US_Type_Font_Size[buttonIndex].Value
                    })
                }
            }
        )
    }

    showTime_Local_Notify = () => {
        ActionSheet.show(
            {
                options: Arr_US_Time_Local_Notify.map(obj => {
                    return obj.Text
                }),
                title: "Chọn loại hiển thị thông báo"
            },
            buttonIndex => {
                if (buttonIndex !== undefined) {
                    this.setState({
                        US_Time_Local_Notify: Arr_US_Time_Local_Notify[buttonIndex].Value
                    })
                }
            }
        )
    }

    onPressSave = () => {
        AsyncStorage.setItem("US_Xem_Pdf_iOS", this.state.US_Xem_Pdf_iOS, () => {
            global.US_Xem_Pdf_iOS = this.state.US_Xem_Pdf_iOS
        })
        AsyncStorage.setItem("US_First_Load_Page", this.state.US_First_Load_Page, () => {
            global.US_First_Load_Page = this.state.US_First_Load_Page
        })
        AsyncStorage.setItem("US_Mobile_Pki_Mode", this.state.US_Mobile_Pki_Mode, () => {
            global.US_Mobile_Pki_Mode = this.state.US_Mobile_Pki_Mode
        })
        AsyncStorage.setItem("US_VBDEN_Mot_Can_Bo_XLC", this.state.US_VBDEN_Mot_Can_Bo_XLC, () => {
            global.US_VBDEN_Mot_Can_Bo_XLC = this.state.US_VBDEN_Mot_Can_Bo_XLC
        })
        AsyncStorage.setItem("US_VBDEN_Tree_Default_Check", this.state.US_VBDEN_Tree_Default_Check, () => {
            global.US_VBDEN_Tree_Default_Check = this.state.US_VBDEN_Tree_Default_Check
        })
        AsyncStorage.setItem("US_FILE_List_Ngang_Doc", this.state.US_FILE_List_Ngang_Doc, () => {
            global.US_FILE_List_Ngang_Doc = this.state.US_FILE_List_Ngang_Doc
        })
        AsyncStorage.setItem("US_Show_Alert_On_Success", this.state.US_Show_Alert_On_Success, () => {
            global.US_Show_Alert_On_Success = this.state.US_Show_Alert_On_Success
        })
        AsyncStorage.setItem("US_Type_Font_Size", this.state.US_Type_Font_Size, () => {
            setFontSize(this.state.US_Type_Font_Size, this.props.dispatch)
            global.US_Type_Font_Size = this.state.US_Type_Font_Size
        })
        AsyncStorage.setItem("US_Time_Local_Notify", this.state.US_Time_Local_Notify, () => {
            setFontSize(this.state.US_Time_Local_Notify, this.props.dispatch)
            global.US_Time_Local_Notify = this.state.US_Time_Local_Notify
        })
        AsyncStorage.setItem("US_Mobile_Pki_Side", this.state.US_Mobile_Pki_Side, () => {
            global.US_Mobile_Pki_Side = this.state.US_Mobile_Pki_Side
            ToastSuccess("Lưu cài đặt thành công!", () => { this.componentDidMount() })
        })
    }

    render() {

        this.styles = StyleSheet.create({
            CardItem: {
                borderBottomColor: "gray",
                borderBottomWidth: 0.35,
                paddingTop: 8,
                paddingBottom: 8,
            },
            textTieuDe: {
                fontSize: this.props.fontSize.FontSizeNorman,
            },
            textNoiDung: {
                fontSize: this.props.fontSize.FontSizeSmall,
            },
            CardItemLeft: {
                width: 150
            },
            CardItemBody: {
                flex: 1,
                alignItems: "center"
            },
            CardItemRight: {
                backgroundColor: "transparent",
                width: 200,
                position: 'absolute',
                zIndex: 99,
                right: 5,
                justifyContent: "center",
                alignItems: "flex-end"
            },
            TextTitle: {
                fontSize: this.props.fontSize.FontSizeSmall,
            },
            TextInput: {
                flex: 1,
                fontFamily: AppConfig.fontFamily,
                fontSize: this.props.fontSize.FontSizeNorman,
                color: "#000000",
                borderColor: AppConfig.defaultLineColor,
                borderWidth: AppConfig.defaultLineWidth,
                borderRadius: 5,
                marginRight: 2,
                paddingRight: 30,
                minHeight: 40,
            },
            Icon: {
                flex: 1,
                color: AppConfig.blueBackground,
                fontSize: this.props.fontSize.FontSizeXLarge,
                padding: 5,
            }
        })

        let color_US_Xem_Pdf_iOS = (this.US_Xem_Pdf_iOS === this.state.US_Xem_Pdf_iOS) ? "black" : "red"
        let color_US_First_Load_Page = (this.US_First_Load_Page === this.state.US_First_Load_Page) ? "black" : "red"
        let color_US_Mobile_Pki_Mode = (this.US_Mobile_Pki_Mode === this.state.US_Mobile_Pki_Mode) ? "black" : "red"
        let color_US_Mobile_Pki_Side = (this.US_Mobile_Pki_Side === this.state.US_Mobile_Pki_Side) ? "black" : "red"
        let color_US_VBDEN_Mot_Can_Bo_XLC = (this.US_VBDEN_Mot_Can_Bo_XLC === this.state.US_VBDEN_Mot_Can_Bo_XLC) ? "black" : "red"
        let color_US_VBDEN_Tree_Default_Check = (this.US_VBDEN_Tree_Default_Check === this.state.US_VBDEN_Tree_Default_Check) ? "black" : "red"
        let color_US_FILE_List_Ngang_Doc = (this.US_FILE_List_Ngang_Doc === this.state.US_FILE_List_Ngang_Doc) ? "black" : "red"
        let color_US_Show_Alert_On_Success = (this.US_Show_Alert_On_Success === this.state.US_Show_Alert_On_Success) ? "black" : "red"
        let color_US_Type_Font_Size = (this.US_Type_Font_Size === this.state.US_Type_Font_Size) ? "black" : "red"
        let color_US_Time_Local_Notify = (this.US_Time_Local_Notify === this.state.US_Time_Local_Notify) ? "black" : "red"

        let showConfigKySo = (global.lanh_dao === 1 && checkVersionWeb(16)) ? true : false
        return (
            <Container style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
                <HeaderWithLeftRight
                    title="Cài đặt"
                    buttonLeftName="menu"
                    onPressLeftButton={_ => { this.props.navigation.dispatch(DrawerActions.toggleDrawer()) }}
                    buttonRightName="save"
                    onPressRightButton={() => { this.onPressSave() }}
                />
                <Content>
                    <Separator bordered style={[this.styles.CardItem]}>
                        <Text style={this.styles.textTieuDe}>Hiển thị</Text>
                    </Separator>
                    <CardItem style={[this.styles.CardItem]}>
                        <CardItem style={this.styles.CardItemBody}>
                            <Text style={[this.styles.textTieuDe, { color: color_US_First_Load_Page }]}>Trang mặc định</Text>
                        </CardItem>
                        <CardItem style={this.styles.CardItemRight}>
                            <TouchableOpacity onPress={this.showFirstLoadPage} style={{ flex: 1 }}>
                                <Text editable={false} style={[this.styles.textNoiDung, { flex: 1, alignSelf: "flex-end", color: color_US_First_Load_Page }]}>{Arr_US_First_Load_Page.filter(el => el.Value === this.state.US_First_Load_Page)[0].Text}</Text>
                            </TouchableOpacity>
                        </CardItem>
                    </CardItem>
                    <CardItem style={[this.styles.CardItem]}>
                        <CardItem style={this.styles.CardItemBody}>
                            <Text style={[this.styles.textTieuDe, { color: color_US_Xem_Pdf_iOS }]}>Loại hiển thị tệp Pdf</Text>
                        </CardItem>
                        <CardItem style={this.styles.CardItemRight}>
                            <TouchableOpacity onPress={this.showXemPdfIOS} style={{ flex: 1 }}>
                                <Text editable={false} style={[this.styles.textNoiDung, { flex: 1, alignSelf: "flex-end", color: color_US_Xem_Pdf_iOS }]}>{this.state.US_Xem_Pdf_iOS === "Offline" ? "Ngoại tuyến" : "Trực tuyến"}</Text>
                            </TouchableOpacity>
                        </CardItem>
                    </CardItem>
                    <CardItem style={[this.styles.CardItem]}>
                        <CardItem style={this.styles.CardItemBody}>
                            <Text style={[this.styles.textTieuDe, { color: color_US_FILE_List_Ngang_Doc }]}>Hướng hiển thị danh sách tệp tin</Text>
                        </CardItem>
                        <CardItem style={this.styles.CardItemRight}>
                            <TouchableOpacity onPress={this.showFILE_List_Ngang_Doc} style={{ flex: 1 }}>
                                <Text editable={false} style={[this.styles.textNoiDung, { flex: 1, alignSelf: "flex-end", color: color_US_FILE_List_Ngang_Doc }]}>{this.state.US_FILE_List_Ngang_Doc === "2" ? "Chiều dọc" : "Chiều ngang"}</Text>
                            </TouchableOpacity>
                        </CardItem>
                    </CardItem>
                    <CardItem style={[this.styles.CardItem]}>
                        <CardItem style={this.styles.CardItemBody}>
                            <Text style={[this.styles.textTieuDe, { color: color_US_Show_Alert_On_Success }]}>Loại hiển thị thông báo thành công</Text>
                        </CardItem>
                        <CardItem style={this.styles.CardItemRight}>
                            <TouchableOpacity onPress={this.showShow_Alert_On_Success} style={{ flex: 1 }}>
                                <Text editable={false} style={[this.styles.textNoiDung, { flex: 1, alignSelf: "flex-end", color: color_US_Show_Alert_On_Success }]}>{this.state.US_Show_Alert_On_Success === "1" ? "Hiện" : "Ẩn"}</Text>
                            </TouchableOpacity>
                        </CardItem>
                    </CardItem>
                    <CardItem style={[this.styles.CardItem]}>
                        <CardItem style={this.styles.CardItemBody}>
                            <Text style={[this.styles.textTieuDe, { color: color_US_Type_Font_Size }]}>Kích thước font chữ</Text>
                        </CardItem>
                        <CardItem style={this.styles.CardItemRight}>
                            <TouchableOpacity onPress={this.showType_Font_Size} style={{ flex: 1 }}>
                                <Text editable={false} style={[this.styles.textNoiDung, { flex: 1, alignSelf: "flex-end", color: color_US_Type_Font_Size }]}>{this.state.US_Type_Font_Size === "1" ? "Nhỏ" : this.state.US_Type_Font_Size === "2" ? "Bình thường" : "Lớn"}</Text>
                            </TouchableOpacity>
                        </CardItem>
                    </CardItem>
                    {
                        (showConfigKySo) && (
                            <Separator bordered style={[this.styles.CardItem]}>
                                <Text style={this.styles.textTieuDe}>Chữ ký số</Text>
                            </Separator>
                        )
                    }
                    {
                        (showConfigKySo) && (
                            <CardItem style={[this.styles.CardItem]}>
                                <CardItem style={this.styles.CardItemBody}>
                                    <Text style={[this.styles.textTieuDe, { color: color_US_Mobile_Pki_Mode }]}>Loại thông tin ký số</Text>
                                </CardItem>
                                <CardItem style={this.styles.CardItemRight}>
                                    <TouchableOpacity onPress={this.showMobilePkiMode} style={{ flex: 1 }}>
                                        <Text editable={false} style={[this.styles.textNoiDung, { flex: 1, alignSelf: "flex-end", color: color_US_Mobile_Pki_Mode }]}>{Arr_US_Mobile_Pki_Mode.filter(el => el.Value === this.state.US_Mobile_Pki_Mode)[0].Text}</Text>
                                    </TouchableOpacity>
                                </CardItem>
                            </CardItem>
                        )
                    }
                    {
                        (showConfigKySo) && (
                            <CardItem style={[this.styles.CardItem]}>
                                <CardItem style={this.styles.CardItemBody}>
                                    <Text style={[this.styles.textTieuDe, { color: color_US_Mobile_Pki_Side }]}>Loại định vị chữ ký</Text>
                                </CardItem>
                                <CardItem style={this.styles.CardItemRight}>
                                    <TouchableOpacity onPress={this.showMobilePkiSide} style={{ flex: 1 }}>
                                        <Text editable={false} style={[this.styles.textNoiDung, { flex: 1, alignSelf: "flex-end", color: color_US_Mobile_Pki_Side }]}>{Arr_US_Mobile_Pki_Side.filter(el => el.Value === this.state.US_Mobile_Pki_Side)[0].Text}</Text>
                                    </TouchableOpacity>
                                </CardItem>
                            </CardItem>
                        )
                    }
                    <Separator bordered style={[this.styles.CardItem]}>
                        <Text style={this.styles.textTieuDe}>Văn bản đến</Text>
                    </Separator>
                    <CardItem style={[this.styles.CardItem]}>
                        <CardItem style={this.styles.CardItemBody}>
                            <Text style={[this.styles.textTieuDe, { color: color_US_VBDEN_Mot_Can_Bo_XLC }]}>Số cán bộ XLC trong VB Đến</Text>
                        </CardItem>
                        <CardItem style={this.styles.CardItemRight}>
                            <TouchableOpacity onPress={this.showVBDEN_Mot_Can_Bo_XLC} style={{ flex: 1 }}>
                                <Text editable={false} style={[this.styles.textNoiDung, { flex: 1, alignSelf: "flex-end", color: color_US_VBDEN_Mot_Can_Bo_XLC }]}>{Arr_US_VBDEN_Mot_Can_Bo_XLC.filter(el => el.Value === this.state.US_VBDEN_Mot_Can_Bo_XLC)[0].Text}</Text>
                            </TouchableOpacity>
                        </CardItem>
                    </CardItem>
                    <CardItem style={[this.styles.CardItem]}>
                        <CardItem style={this.styles.CardItemBody}>
                            <Text style={[this.styles.textTieuDe, { color: color_US_VBDEN_Tree_Default_Check }]}>Loại vai trò mặc định</Text>
                        </CardItem>
                        <CardItem style={this.styles.CardItemRight}>
                            <TouchableOpacity onPress={this.showVBDEN_Tree_Default_Check} style={{ flex: 1 }}>
                                <Text editable={false} style={[this.styles.textNoiDung, { flex: 1, alignSelf: "flex-end", color: color_US_VBDEN_Tree_Default_Check }]}>{Arr_US_VBDEN_Tree_Default_Check.filter(el => el.Value === this.state.US_VBDEN_Tree_Default_Check)[0].Text}</Text>
                            </TouchableOpacity>
                        </CardItem>
                    </CardItem>
                    <Separator bordered style={[this.styles.CardItem]}>
                        <Text style={this.styles.textTieuDe}>Lịch công tác</Text>
                    </Separator>
                    <CardItem style={[this.styles.CardItem]}>
                        <CardItem style={this.styles.CardItemBody}>
                            <Text style={[this.styles.textTieuDe, { color: color_US_Time_Local_Notify }]}>Loại vai trò mặc định</Text>
                        </CardItem>
                        <CardItem style={this.styles.CardItemRight}>
                            <TouchableOpacity onPress={this.showVBDEN_Tree_Default_Check} style={{ flex: 1 }}>
                                <Text editable={false} style={[this.styles.textNoiDung, { flex: 1, alignSelf: "flex-end", color: color_US_Time_Local_Notify }]}>{Arr_US_Time_Local_Notify.filter(el => el.Value === this.state.US_Time_Local_Notify)[0].Text}</Text>
                            </TouchableOpacity>
                        </CardItem>
                    </CardItem>
                </Content>
            </Container>
        );
    }
}

function mapStateToProps(state) {
    return {
        fontSize: state.fontSize,
    }
}

export default connect(mapStateToProps)(UserSettings)