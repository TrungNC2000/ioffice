import React, { Component, PureComponent } from 'react';
import { View, StyleSheet, FlatList, Alert, Modal } from "react-native"
import { Container, Text, CardItem, Spinner, Toast, Button, Item, Left, Icon as NBIcon } from "native-base"
import { connect } from "react-redux"
import { DrawerActions } from "react-navigation"
import { Menu, MenuOptions, MenuOption, MenuTrigger } from 'react-native-popup-menu'
import HeaderWithLeftRight from "../../components/Header/HeaderWithLeftRight"
import Icon from "react-native-vector-icons/FontAwesome5"
import MyTab from "../../components/Tab"
import API from "../../networks"
import { AppConfig } from "../../AppConfig"
import { CheckSession } from "../../untils/NetInfo"
import { RemoveNoiDungTTDH, ConvertDateTime, ToastSuccess } from "../../untils/TextUntils"

class ModalNguoiNhan extends Component {
    render() {
        this.styles = this.props.styles
        return (
            <Modal
                transparent={false}
                animationType="slide"
                visible={this.props.isModalVisible}
                supportedOrientations={["landscape", "landscape-left", "landscape-right", "portrait", "portrait-upside-down"]}
            >
                <View style={{ flex: 1, padding: 15, backgroundColor: AppConfig.grayBackground }}>
                    <View style={{ flex: 1, backgroundColor: "#FFFFFF", borderRadius: 10 }}>
                        <View style={{ position: 'absolute', zIndex: 99, height: 55, left: 0, right: 0, top: 0, justifyContent: "center", padding: 4, alignItems: "center" }}>
                            <Text style={{ fontSize: this.props.fontSize.FontSizeLarge }}>Danh sách cán bộ nhận</Text>
                        </View>
                        <FlatList
                            ref={(ref) => { this.flatListRef = ref }}
                            data={this.props.listNguoiNhan}
                            keyExtractor={(item, index) => "key" + index}
                            style={{ position: 'absolute', left: 0, right: 0, top: 55, bottom: 55 }}
                            initialNumToRender={50}
                            renderItem={({ item, index }) => {
                                return (
                                    <Item key={index} style={{ padding: 12, borderBottomWidth: AppConfig.defaultLineWidth, borderColor: AppConfig.defaultLineColor }}>
                                        <Left>
                                            <Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>{item.ho_va_ten_can_bo}</Text>
                                            <Text note style={{ fontSize: this.props.fontSize.FontSizeSmall }}>Số điện thoại: {item.di_dong_nhan}</Text>
                                        </Left>
                                    </Item>
                                )
                            }}
                            ListEmptyComponent={() => {
                                return (
                                    <View style={{ alignItems: "center", marginTop: 10, }}>
                                        <Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>Không có dữ liệu</Text>
                                    </View>
                                )
                            }}
                            removeClippedSubviews={true}
                        />
                        <View style={{ position: 'absolute', zIndex: 99, height: 55, left: 0, right: 0, bottom: 0, flexDirection: "row", justifyContent: "space-evenly", paddingTop: 10, alignItems: "center" }}>
                            <Button small danger bordered rounded iconLeft onPress={_ => this.props.onShowRecirver(0)}><NBIcon name="close" type="MaterialIcons"></NBIcon><Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>Đóng</Text></Button>
                        </View>
                    </View>
                </View>
            </Modal>
        )
    }
}

class SMSTab_Item extends PureComponent {
    render() {
        this.styles = this.props.styles
        const { item } = this.props
        let noiDung = RemoveNoiDungTTDH(item.noi_dung)
        const minHeight = 80
        return (
            <CardItem key={item.ma_sms_kc} style={[this.styles.CardItem]}>
                <View style={{ justifyContent: 'center', flex: 1, left: 0, marginRight: 100, minHeight }}>
                    <Text ellipsizeMode="tail" style={[this.styles.textNoBold]}>{noiDung} </Text>
                    {
                        (this.props.activeTab === 1) && (
                            <Text numberOfLines={1} ellipsizeMode="tail" style={[this.styles.txtNoiDung]}>Cán bộ gửi: {item.ten_nguoi_gui}</Text>
                        )
                    }
                    {
                        (this.props.activeTab === 1) && (
                            <Text numberOfLines={1} ellipsizeMode="tail" style={[this.styles.txtNoiDung]}>Cán bộ nhận: {item.ho_va_ten_can_bo}</Text>
                        )
                    }
                </View>
                <View style={{ width: 100, top: 0, right: 0, bottom: 0, position: "absolute", alignItems: 'center', justifyContent: "center" }}>
                    <Text style={this.styles.txtNgayNhan}>{ConvertDateTime(item.ngay_gui)} </Text>
                    {
                        (this.props.activeTab === 0) && (
                            <Menu>
                                <MenuTrigger style={this.styles.menuTrigger}>
                                    <Icon name="ellipsis-v" style={this.styles.menuTriggerIcon}></Icon>
                                </MenuTrigger>
                                <MenuOptions optionsContainerStyle={this.styles.menuOptionsContainer}>
                                    <MenuOption onSelect={() => { this.props.onShowRecirver(item.ma_sms_kc) }} >
                                        <Text style={this.styles.menuOptionsText}>Danh sách người nhận</Text>
                                    </MenuOption>
                                    <MenuOption onSelect={() => { this.props.onDelete(item.ma_sms_kc) }} >
                                        <Text style={this.styles.menuOptionsTextDelete}>Xoá</Text>
                                    </MenuOption>
                                </MenuOptions>
                            </Menu>
                        )
                    }
                </View>
            </CardItem>
        )
    }
}

class SMSScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            loading: true,
            refreshing: false,
            dataSource: [],
            isModalVisible: false,
            listNguoiNhan: [],
        }
        this.page = 1
        this.activeTab = 0
    }

    componentDidMount() {
        this.didBlurSubscription = this.props.navigation.addListener('willFocus', payload => { CheckSession(this.props.navigation) })
        this.activeTab = this.props.navigation.getParam("activeTab", 0)
        this.getListByTabIndex(this.activeTab)
    }

    componentWillUnmount = () => {
        this.didBlurSubscription.remove()
    };

    shouldComponentUpdate = (nextProps, nextState) => {
        if (this.state.loading !== nextState.loading ||
            this.state.refreshing !== nextState.refreshing ||
            this.state.dataSource !== nextState.dataSource ||
            this.state.isModalVisible !== nextState.isModalVisible ||
            this.state.listNguoiNhan !== nextState.listNguoiNhan) {
            return true
        }
        return false
    };

    getListByTabIndex = (tabIndex) => {
        let page = this.page
        if (tabIndex === 0) {
            let size = AppConfig.pageSizeSMSCaNhan
            API.NhanTinSMS.AU_SMS_DSCBGTN(page, size)
                .then(response => {
                    if (tabIndex === this.activeTab) {
                        this.setState({ dataSource: page === 1 ? response : [...this.state.dataSource, ...response] }, () => {
                            this.setState({ loading: false, refreshing: false })
                        })
                    }
                })
        } else {
            let size = AppConfig.pageSizeSMSDonVi
            API.NhanTinSMS.AU_SMS_DSDVGTN(page, size)
                .then(response => {
                    if (tabIndex === this.activeTab) {
                        this.setState({ dataSource: page === 1 ? response : [...this.state.dataSource, ...response] }, () => {
                            this.setState({ loading: false, refreshing: false })
                        })
                    }
                })
        }
    }

    onChangeTab = (index) => {
        if (!this.state.loading) {
            this.page = 1
            this.activeTab = index
            this.setState({ loading: true })
            this.getListByTabIndex(this.activeTab)
        }
    }

    onRefresh = () => {
        if (!this.state.loading) {
            this.page = 1
            this.setState({ refreshing: true, loading: true })
            this.getListByTabIndex(this.activeTab)
        }
    }

    loadMore = () => {
        if (!this.state.loading) {
            this.page = this.page + 1
            this.getListByTabIndex(this.activeTab)
        }
    }

    onShowRecirver = (ma_sms_kc) => {
        if (!this.state.isModalVisible) {
            API.NhanTinSMS.AU_SMS_DSCBNSMSBM(ma_sms_kc).then((listNguoiNhan) => {
                this.setState({ listNguoiNhan, isModalVisible: true })
            })
        } else {
            this.setState({ listNguoiNhan: [], isModalVisible: false })
        }
    }

    onDelete = (ma_sms_kc) => {
        Alert.alert("Xác nhận", "Xóa tin nhắn khỏi danh sách?",
            [
                { text: "Không", style: "cancel" },
                {
                    text: "Đồng ý", onPress: () => {
                        API.NhanTinSMS.AU_SMS_XTNDGCCB(ma_sms_kc).then((response) => {
                            if (response) {
                                let dataSource = this.state.dataSource.filter(el => el.ma_sms_kc !== ma_sms_kc)
                                ToastSuccess("Xóa thành công!", () => { this.setState({ dataSource }) })
                            } else {
                                Toast.show({ text: "Xảy ra lỗi. Vui lòng thực hiện lại sau!", type: "danger", buttonText: "Đóng", position: "top", duration: 2000 })
                            }
                        })
                    }
                }
            ],
            { cancelable: false }
        )
    }

    render() {

        this.styles = StyleSheet.create({
            CardItem: {
                flex: 1,
                borderTopWidth: 0,
                borderBottomWidth: 0.55,
                borderLeftWidth: 0,
                borderRightWidth: 0,
                borderBottomColor: "lightgray",
                paddingLeft: 8,
                alignItems: 'center',
                justifyContent: "center",
            },
            colTieuDe: {
                width: "70%",
                justifyContent: "center"
            },
            textNoBold: {
                color: "#000000",
                fontSize: this.props.fontSize.FontSizeNorman,
                paddingBottom: AppConfig.defaultPadding
            },
            txtNgayNhan: {
                width: 100,
                color: "#808080",
                fontSize: this.props.fontSize.FontSizeSmall,
                textAlign: "center",
                paddingBottom: AppConfig.defaultPadding
            },
            txtNoiDung: {
                color: "#808080",
                fontSize: this.props.fontSize.FontSizeSmall,
                paddingBottom: AppConfig.defaultPadding
            },
            colNgayGui: {
                width: "30%",
                justifyContent: "center",
                alignItems: "flex-end"
            },
            menuTrigger: {
                width: 40,
                height: 25,
                justifyContent: "center",
                alignItems: 'center'
            },
            menuTriggerIcon: {
                fontSize: this.props.fontSize.FontSizeXXXLarge,
                color: "#B0BEC5"
            },
            menuOptionsContainer: {
                width: 200
            },
            menuOptions: {
                borderBottomColor: AppConfig.defaultLineColor,
                borderBottomWidth: 0.5
            },
            menuOptionsText: {
                color: AppConfig.headerBackgroundColor,
                fontSize: this.props.fontSize.FontSizeNorman,
                paddingTop: AppConfig.defaultPadding,
                paddingBottom: AppConfig.defaultPadding
            },
            menuOptionsTextDelete: {
                color: "red",
                fontSize: this.props.fontSize.FontSizeNorman,
                paddingTop: AppConfig.defaultPadding,
                paddingBottom: AppConfig.defaultPadding
            }
        })

        return (
            <Container style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
                <HeaderWithLeftRight
                    title="Nhắn tin sms"
                    buttonLeftName="menu"
                    onPressLeftButton={() => { this.props.navigation.dispatch(DrawerActions.toggleDrawer()) }}
                    buttonRightName="note-add"
                    onPressRightButton={() => { this.props.navigation.navigate("SMS_Gui") }}
                />
                <View style={{ flex: 1 }}>
                    <MyTab
                        values={["Cá nhân", "Đơn vị"]}
                        badges={[0, 0]}
                        activeTab={this.activeTab}
                        onChangeTab={(index) => { this.onChangeTab(index) }}
                        fontSize={this.props.fontSize}
                    />
                    {
                        (this.state.isModalVisible) && (
                            <ModalNguoiNhan
                                listNguoiNhan={this.state.listNguoiNhan}
                                isModalVisible={this.state.isModalVisible}
                                onShowRecirver={this.onShowRecirver}
                                fontSize={this.props.fontSize}
                                styles={this.styles}
                            />
                        )
                    }
                    {
                        (this.state.loading) ? (
                            <View><Spinner color={AppConfig.blueBackground} /></View>
                        ) : (
                                <FlatList
                                    ref={(ref) => { this.flatListRef = ref }}
                                    data={this.state.dataSource}
                                    keyExtractor={(item, index) => "key" + item.ma_sms_kc + index}
                                    renderItem={({ item }) => {
                                        return (
                                            <SMSTab_Item
                                                activeTab={this.activeTab}
                                                item={item}
                                                navigation={this.props.navigation}
                                                onDelete={this.onDelete}
                                                onShowRecirver={this.onShowRecirver}
                                                fontSize={this.props.fontSize}
                                                styles={this.styles}
                                            />
                                        )
                                    }}
                                    onRefresh={this.onRefresh}
                                    refreshing={this.state.refreshing}
                                    onEndReached={this.loadMore}
                                    onEndReachedThreshold={2}
                                    removeClippedSubviews={true}
                                />
                            )
                    }
                </View>
            </Container>
        );
    }
}

function mapStateToProps(state) {
    return {
        fontSize: state.fontSize,
    }
}

export default connect(mapStateToProps)(SMSScreen)