import React, { Component } from 'react';
import { StyleSheet, Alert, View, Dimensions, PixelRatio, Image, TouchableOpacity, AsyncStorage } from "react-native"
import { Container, Content, Text, Card, CardItem, Button, Toast, Icon } from "native-base"
import { DrawerActions } from "react-navigation"
import { connect } from "react-redux"
import API from "../networks"
import HeaderWithLeft from "../components/Header/HeaderWithLeft"
import CustomTextInput from "../components/CustomTextInput"
import { getURLFile, ToastSuccess } from "../untils/TextUntils"
import UploadFile from "../components/UploadFile"
import { FlatList } from 'react-native-gesture-handler';
import { AppConfig } from '../AppConfig';
let { width, height } = Dimensions.get("window")
const realWidth = width < height ? width : height

class MauChuKy extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listMauChuKy: [],
            onChangeSize: false,
            tenChuKy: "",
            widthRec: global.US_Mobile_Pki_Width_Rec || AppConfig.Default_US_Mobile_Pki_Width_Rec,
            heightRec: global.US_Mobile_Pki_Height_Rec || AppConfig.US_Mobile_Pki_Height_Rec
        }
    }

    componentDidMount() {
        this.getListMauChuKy()
        Dimensions.addEventListener("change", this.onChangeSize);

    }

    componentWillUnmount = () => {
        Dimensions.removeEventListener("change", this.onChangeSize);
    };

    onChangeSize = () => {
        this.setState({ onChangeSize: !this.state.onChangeSize })
    }

    getListMauChuKy = () => {
        API.KYSO.AU_KYSO_DSCKS(global.ma_ctcb_kc).then((listMauChuKy) => {
            if (listMauChuKy.length % 2 === 1) {
                listMauChuKy.push({ "id_ky_so": null })
            }
            this.setState({ listMauChuKy })

        })
    }

    deleteMauChuKy = (id_ky_so) => {
        Alert.alert("Xác nhận", "Xóa mẫu chữ ký này?",
            [
                { text: "Không", style: "cancel" },
                {
                    text: "Đồng ý", onPress: () => {
                        API.KYSO.AU_KYSO_XCKS(id_ky_so).then((response) => {
                            if (response) {
                                ToastSuccess("Xóa chữ ký thành công!", () => { this.componentDidMount() })
                            } else {
                                Toast.show({ text: "Xảy ra lỗi. Vui lòng thực hiện lại sau!", type: "danger", buttonText: "Đóng", position: "top", duration: 2000 })
                            }
                        })
                    }
                }
            ],
            { cancelable: false }
        )
    }

    checkDefault = (id_ky_so) => {
        API.KYSO.AU_KYSO_CNCKS(id_ky_so).then((response) => {
            if (response) {
                this.componentDidMount()
                ToastSuccess("Cài chữ ký mặc định thành công!")
            } else {
                Toast.show({ text: "Xảy ra lỗi. Vui lòng thực hiện lại sau!", type: "danger", buttonText: "Đóng", position: "top", duration: 2000 })
            }
        })
    }

    onPressCapNhatRec = async () => {
        let { widthRec, heightRec } = this.state
        if (widthRec && heightRec) {
            AsyncStorage.setItem("US_Mobile_Pki_Width_Rec", widthRec, () => { global.US_Mobile_Pki_Width_Rec = widthRec })
            AsyncStorage.setItem("US_Mobile_Pki_Height_Rec", heightRec, () => { global.US_Mobile_Pki_Height_Rec = heightRec })
            ToastSuccess("Cập nhật thành công!")
        } else {
            return Toast.show({ text: "Vui lòng nhập chiều rộng, cao khung ký!", type: "warning", buttonText: "Đóng", position: "top", duration: 2000 })
        }
    }

    onPressThemMoi = () => {
        let { tenChuKy } = this.state
        if (!tenChuKy) {
            return Toast.show({ text: "Vui lòng nhập tên chữ ký", type: "warning", buttonText: "Đóng", position: "top", duration: 2000 })
        }
        let imageName = this.UploadFile.getListFileUpload()
        if (!imageName) {
            return Toast.show({ text: "Vui lòng chọn hình ảnh", type: "warning", buttonText: "Đóng", position: "top", duration: 2000 })
        }
        API.KYSO.AU_KYSO_TMCKS(imageName, tenChuKy).then((response) => {
            if (response) {
                ToastSuccess("Thêm mới chữ ký thành công!", () => {
                    this.setState({ tenChuKy: "" })
                    this.UploadFile.clearFileUpload()
                    this.componentDidMount()
                })
            } else {
                Toast.show({ text: "Xảy ra lỗi. Vui lòng thực hiện lại sau!", type: "danger", buttonText: "Đóng", position: "top", duration: 2000 })
            }
        })
    }

    renderItem = ({ item, index }) => {
        if (item.id_ky_so) {
            let colorCheck = item.trang_thai === 1 ? "#1976D2" : "gray"
            return (
                <CardItem noShadow key={item.id_ky_so} style={this.styles.cardItem}>
                    <View style={this.styles.view}>
                        <View style={this.styles.viewImage}>
                            <Image style={this.styles.image} source={{ uri: getURLFile(item.link_cks) }} />
                        </View>
                        <View style={this.styles.viewTenChuKy}>
                            <Text style={this.styles.txtTenChuKy}>{item.ten_ky_so}</Text>
                        </View>
                    </View>
                    <TouchableOpacity style={this.styles.btnDelete} onPress={() => this.deleteMauChuKy(item.id_ky_so)}>
                        {/* <Text style={this.styles.txtDelete}>x</Text> */}
                        <Icon name="close" type="MaterialIcons" fontSize={AppConfig.FontSizeSmall} style={{ color: "red" }} />
                    </TouchableOpacity>

                    <TouchableOpacity style={this.styles.btnCheckDefault} onPress={() => this.checkDefault(item.id_ky_so)}>
                        <Icon name="check" type="MaterialIcons" fontSize={AppConfig.FontSizeSmall} style={{ color: colorCheck }} />
                    </TouchableOpacity>
                </CardItem>
            )
        } else {
            return (
                <CardItem noShadow key={index} style={{ flex: 1, margin: 5 }}></CardItem>
            )
        }
    }

    render() {

        this.styles = StyleSheet.create({
            container: {
                flex: 1,
                backgroundColor: "#FFFFFF"
            },
            cardThemMoi: {
                flex: 1,
                padding: 5
            },
            cardItemBtnThemMoi: {
                justifyContent: "center",
                alignItems: "center",
                padding: 5
            },
            cardItem: {
                flex: 1,
                margin: 5,
                borderWidth: 0.8,
                borderColor: "gray",
                height: realWidth * 0.3,
                paddingLeft: 0,
                paddingRight: 0
            },
            viewTenChuKyUpload: {
                flex: 1,
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center",
                padding: 5
            },
            txtTenChuKyUpload: {
                fontSize: this.props.fontSize.FontSizeNorman / PixelRatio.getFontScale(),
                borderBottomWidth: 0.6,
                flex: 1
            },
            viewUpload: {
                flex: 1,
                alignSelf: "flex-start",
                padding: 5
            },
            view: {
                flex: 1,
                height: "100%",
                marginRight: 35,
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center"
            },
            btnDelete: {
                width: 40,
                height: 40,
                top: 0,
                right: 0,
                paddingLeft: 5,
                position: "absolute",
                justifyContent: "center",
                alignItems: "center",
                borderWidth: 0.5,
                borderColor: "gray"
            },
            txtDelete: {
                color: "red",
                fontSize: this.props.fontSize.FontSizeXXLarge
            },
            btnCheckDefault: {
                width: 40,
                height: 40,
                top: 40,
                right: 0,
                paddingLeft: 5,
                position: "absolute",
                justifyContent: "center",
                alignItems: "center",
                borderWidth: 0.5,
                borderColor: "gray"
            },
            viewImage: {
                margin: 5,
                width: realWidth * 0.25,
                height: realWidth * 0.25,
                left: 0,
                position: "absolute"
            },
            image: {
                width: realWidth * 0.25,
                height: realWidth * 0.25,
                resizeMode: "stretch"
            },
            viewTenChuKy: {
                padding: 5,
                marginLeft: realWidth * 0.26,
                flex: 1,
                justifyContent: "center",
                alignItems: "center"
            },
            txtTenChuKy: {
                fontSize: this.props.fontSize.FontSizeNorman
            }
        })

        let { navigation } = this.props
        let { listMauChuKy, onChangeSize } = this.state
        let { width, height } = Dimensions.get("screen")
        let numColumns = width < height ? 1 : 2
        return (
            <Container style={this.styles.container}>
                <HeaderWithLeft
                    title="Cập nhật mẫu chữ ký"
                    buttonName={"menu"}
                    onPressLeftButton={_ => { navigation.dispatch(DrawerActions.toggleDrawer()) }}
                />
                <Content>
                    <Card style={this.styles.cardThemMoi}>
                        <CardItem style={{ flexDirection: "column" }}>
                            <View style={this.styles.viewTenChuKyUpload}>
                                <Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>{"Chiều rộng khung ký(px): "} </Text>
                                <CustomTextInput
                                    style={this.styles.txtTenChuKyUpload}
                                    value={this.state.widthRec}
                                    onChangeText={(text) => { this.setState({ widthRec: text }) }}
                                    placeholder=""
                                    underlineColorAndroid={"transparent"}
                                    onRef={ref => this.input = ref}
                                    placeholderStyle={{}}
                                    keyboardType={"number-pad"}
                                />
                            </View>
                            <View style={this.styles.viewTenChuKyUpload}>
                                <Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>{"Chiều cao khung ký(px): "} </Text>
                                <CustomTextInput
                                    style={this.styles.txtTenChuKyUpload}
                                    value={this.state.heightRec}
                                    onChangeText={(text) => { this.setState({ heightRec: text }) }}
                                    placeholder=""
                                    underlineColorAndroid={"transparent"}
                                    onRef={ref => this.input = ref}
                                    placeholderStyle={{}}
                                    keyboardType={"number-pad"}
                                />
                            </View>
                        </CardItem>
                        <CardItem style={this.styles.cardItemBtnThemMoi}>
                            <Button primary small onPress={this.onPressCapNhatRec}><Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>Cập nhật</Text></Button>
                        </CardItem>
                    </Card>
                    <Card style={this.styles.cardThemMoi}>
                        <CardItem style={{ flexDirection: "column" }}>
                            <View style={this.styles.viewTenChuKyUpload}>
                                <Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>Tên chữ ký: </Text>
                                <CustomTextInput
                                    style={this.styles.txtTenChuKyUpload}
                                    value={this.state.tenChuKy}
                                    onChangeText={(text) => { this.setState({ tenChuKy: text }) }}
                                    placeholder=""
                                    underlineColorAndroid={"transparent"}
                                    onRef={ref => this.input = ref}
                                    placeholderStyle={{}}
                                />
                            </View>
                            <View style={this.styles.viewUpload}>
                                <UploadFile
                                    ref={(upload) => { this.UploadFile = upload }}
                                    chuc_nang="avatar"
                                    mutilFile={false}
                                    sourceFiles={[]}
                                    navigation={navigation}
                                    fontSize={this.props.fontSize}
                                />
                            </View>
                        </CardItem>
                        <CardItem style={this.styles.cardItemBtnThemMoi}>
                            <Button primary small onPress={this.onPressThemMoi}><Text style={{ fontSize: this.props.fontSize.FontSizeNorman }}>Thêm mới</Text></Button>
                        </CardItem>
                    </Card>
                    <CardItem style={{ flex: 1 }}>
                        <FlatList
                            data={listMauChuKy}
                            extraData={this.state}
                            key={onChangeSize}
                            keyExtractor={(item, index) => index.toString()}
                            numColumns={numColumns}
                            renderItem={this.renderItem}
                            removeClippedSubviews={true}
                        />
                    </CardItem>
                </Content>
            </Container>
        )
    }
}


function mapStateToProps(state) {
    return {
        fontSize: state.fontSize,
    }
}

export default connect(mapStateToProps)(MauChuKy)