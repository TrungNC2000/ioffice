import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity } from "react-native"
import { Container, Text, CardItem, Separator, Content } from "native-base"
import { connect } from "react-redux"
import { AppConfig } from "../AppConfig"
import HeaderWithLeft from "../components/Header/HeaderWithLeft"

class AdminSettings extends Component {
    constructor(props) {
        super(props);
        this.FILE = [
            "FILE",
            "AU_FILE_CDTTWP",
            "AU_UPLOADFILE_CNTTTLTB64",
            "AU_FILE_CDLF",
            "AU_FILE_TL",
        ]
        this.KYSO = [
            "KYSO",
            "AU_KYSO_DSCKS",
            "AU_KYSO_KSS",
            "AU_KYSO_KSSGFS",
            "AU_KYSO_VTCNFKPH",
            "AU_KYSO_LFKS",
            "AU_KYSO_TMCKS",
            "AU_KYSO_XCKS",
            "AU_KYSO_TFP",
        ]
        this.LICHCONGTAC = [
            "LICHCONGTAC",
            "AU_LCT_DSCQLCT",
            "AU_LCT_LLCTCT",
            "AU_LCT_DSLCTDVQT",
            "AU_LCT_DSLCTDV",
            "AU_LCT_XLCTCN",
            "AU_LCT_CT",
            "AU_LCT_COT",
            "AU_LCT_DSDVLCT",
            "AU_LCT_DSCB",
            "AU_LCT_TSHT",
            "AU_LCT_DSCQLCT1",
            "AU_LCT_LCTLDBC",
            "AU_LCT_XLCTLD",
            "AU_LCT_DSCBLCT",
            "AU_LCT_DSDVDDC",
            "AU_LCT_DLCTCQ",
        ]
        this.LICHHOP = [
            "LICHHOP",
            "AU_LH_TKLHCN",
            "AU_LH_getPageLHCN",
            "AU_LH_CNNX",
            "AU_LH_CTCH",
            "AU_LH_DSTPH",
        ]
        this.LOGIN = [
            "LOGIN",
            "AU_DSDVA",
            "AU_DSCTCB",
            "AU_DSCTCB1",
            "AU_ACCESS_TOKEN",
            "AU_DSDVCQ",
            "AU_KTQCVLDVTCCB",
            "AU_DSNVCB",
            "AU_FCM",
            "AU_PUSH_FCM",
            "AU_DEL_FCM",
            "AU_DSCBBID",
            "AU_KTTMKC",
            "AU_CNMK",
            "AU_CCBTCDB",
            "AU_GETOTPCODE",
            "AU_CHECKOTPCODE",
            "AU_DKOTPNVSMS",
            "AU_GTSM",
            "AU_STSM",
            "AU_GMLDN"
        ]
        this.NHANTINSMS = [
            "NHANTINSMS",
            "AU_SMS_DSCBGTN",
            "AU_SMS_DSCBNSMSBM",
            "AU_SMS_XTNDGCCB",
            "AU_SMS_DSDVGTN",
            "AU_SMS_DSCBNTTDH",
            "AU_SMS_DSNCBNTN",
            "AU_SMS_CNCBTLN",
            "AU_SMS_STNSMS",
            "AU_SMS_GTN"
        ]
        this.THONGTINDIEUHANH = [
            "THONGTINDIEUHANH",
            "AU_GETPAGE_TTDH_DA_GUI",
            "AU_TTDH_DA_GUI",
            "AU_TTDH_DA_NHAN",
            "AU_TTDH_DSCBN",
            "AU_TTDH_CTTTDHN",
            "AU_TTDH_CTTTDHG",
            "AU_TTDH_LDSLTD",
            "AU_TTDH_DSCBNTTDH",
            "AU_TTDH_DSNCBNTTDH",
            "AU_TTDH_GTD",
            "AU_TTDH_LTD",
            "AU_TTDH_CNTTDHDG",
            "AU_TTDH_ATTDHN"
        ]
        this.VANBANDEN = [
            "VANBANDEN",
            "AU_VBDEN_DSVBDTTTCCV",
            "AU_VBDEN_CVXVBD",
            "AU_VBDEN_VBDCT",
            "AU_VBDEN_CVXLNVBD",
            "AU_VBDEN_CVXLVBD",
            "AU_VBDEN_CVCNYKXL",
            "AU_VBDEN_BPLDVBD",
            "AU_VBDEN_DSCBTDVTTS",
            "AU_VBDEN_DSCBTVT",
            "AU_VBDEN_DSLDDCD",
            "AU_VBDEN_CLDKD",
            "AU_VBDEN_CNCBTLN",
            "AU_VBDEN_DSNCBNVBDLDD",
            "AU_VBDEN_CVCTH",
            "AU_VBDEN_DSVBDCDCLD",
            "AU_VBDEN_DSVBDUQDCLD",
            "AU_VBDEN_DSVBDDCTHCLD",
            "AU_VBDEN_LDDVBD",
            "AU_VANBAN_TCVB",
            "AU_VANBAN_DSVBLQDK",
            "AU_VBDEN_DSDMSVBD",
            "AU_VBDEN_LSDTSVBD",
            "AU_VBDEN_DSCDK",
            "AU_VBDEN_DSCDM",
            "AU_VBDEN_DSLVB",
            "AU_VBDEN_DSLDDVBDE",
            "AU_VBDEN_DSVBDTCVT",
            "AU_VBDEN_DSVBDML",
            "AU_VBDEN_DSVBLDTLCVT",
            "AU_VBDEN_DSVBDCDCVT",
            "AU_VBDEN_DSVBDCVTC",
            "AU_VBDEN_DSVBDVTDC",
            "AU_VBDEN_DSVBDHCVT",
        ]
        this.VANBANDI = [
            "VANBANDI",
            "AU_VBDI_DSVBDCXLCCV",
            "AU_VBDI_DSVBDDXLCCV",
            "AU_VBDI_DSVBDCPHCCV",
            "AU_VBDI_DSVBDDPHCCV",
            "AU_VBDI_CNDX",
            "AU_VBDI_CTVBD",
            "AU_VBDI_QTXLVBD",
            "AU_VBDI_HTVBDI",
            "AU_VBDI_VBDCDCLD",
            "AU_VBDI_VBDDUQCLD",
            "AU_VBDI_VBDCPHCLD",
            "AU_VBDI_VBDDPHCLD",
            "AU_VBDI_DSVTTDVQT",
            "AU_VBDI_LDDVBD",
            "AU_VBDI_LDCVT",
            "AU_VBDI_DSLDCDK",
            "AU_VBDI_CVCVBDCLD",
            "AU_VBDI_DSCBTDVTS",
            "AU_VBDI_LDCVBDCCV",
            "AU_VBDI_CVCVBDCCV",
            "AU_VBDI_LDCVBDCLD"
        ]
        this.VANBANNOIBO = [
            "VANBANNOIBO",
            "AU_VBNB_DSVBNBDN",
            "AU_VBNB_DSVBNBDN_GetPage",
            "AU_VBNB_CTVBNBDN",
            "AU_VBNB_DSVBNBDGDPH",
            "AU_VBNB_DSVBNBDGDPH_GetPage",
            "AU_VBNB_XVBNB",
            "AU_VBNB_CTVBNBDG",
            "AU_VBNB_CQTXLVBNB",
            "AU_VBNB_DSCBNVBNB",
            "AU_VBNB_DSNCBNVBNB",
            "AU_VBNB_CNCBTLN",
            "AU_VBNB_CVBNB",
            "AU_VBNB_XVBNBDN",
            "AU_VBNB_XVBNBDG",
        ]
    }

    renderListItem = (arrName) => {
        let result = []
        result.push(
            <Separator bordered style={[this.styles.CardItem]} key={arrName[0]}>
                <Text style={this.styles.textTieuDe}>{arrName[0]}</Text>
            </Separator>
        )
        for (let i = 1; i < arrName.length; i++) {
            result.push(
                <CardItem style={[this.styles.CardItem]} key={arrName[i]} >
                    <CardItem style={this.styles.CardItemBody}>
                        <Text style={[this.styles.textTieuDe]}>{arrName[i]}</Text>
                    </CardItem>
                    <CardItem style={this.styles.CardItemRight}>
                        <TouchableOpacity onPress={() => { global["CONSOLE_" + arrName[i]] ? global["CONSOLE_" + arrName[i]] = null : global["CONSOLE_" + arrName[i]] = "1"; this.setState({ reload: true }) }} style={{ flex: 1 }}>
                            <Text editable={false} style={[this.styles.textNoiDung, { flex: 1, alignSelf: "flex-end" }]}>{global["CONSOLE_" + arrName[i]] ? "On" : "Off"}</Text>
                        </TouchableOpacity>
                    </CardItem>
                </CardItem>
            )
        }
        return result
    }

    render() {

        this.styles = StyleSheet.create({
            CardItem: {
                borderBottomColor: "gray",
                borderBottomWidth: 0.35,
                paddingTop: 8,
                paddingBottom: 8,
            },
            textTieuDe: {
                fontSize: this.props.fontSize.FontSizeNorman,
            },
            textNoiDung: {
                fontSize: this.props.fontSize.FontSizeSmall,
            },
            CardItemLeft: {
                width: 150
            },
            CardItemBody: {
                flex: 1,
                alignItems: "center"
            },
            CardItemRight: {
                backgroundColor: "transparent",
                width: 200,
                position: 'absolute',
                zIndex: 99,
                right: 5,
                justifyContent: "center",
                alignItems: "flex-end"
            },
            TextTitle: {
                fontSize: this.props.fontSize.FontSizeSmall,
            },
            TextInput: {
                flex: 1,
                fontFamily: AppConfig.fontFamily,
                fontSize: this.props.fontSize.FontSizeNorman,
                color: "#000000",
                borderColor: AppConfig.defaultLineColor,
                borderWidth: AppConfig.defaultLineWidth,
                borderRadius: 5,
                marginRight: 2,
                paddingRight: 30,
                minHeight: 40,
            },
            Icon: {
                flex: 1,
                color: AppConfig.blueBackground,
                fontSize: this.props.fontSize.FontSizeXLarge,
                padding: 5,
            }
        })

        return (
            <Container style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
                <HeaderWithLeft
                    title="Admin Control"
                    buttonName="arrow-back"
                    onPressLeftButton={_ => { this.props.navigation.navigate("Login") }}
                />
                <Content>
                    <CardItem style={[this.styles.CardItem]} key={"TestPKINew"} >
                        <CardItem style={this.styles.CardItemBody}>
                            <Text style={[this.styles.textTieuDe]}>Test PKI New</Text>
                        </CardItem>
                        <CardItem style={this.styles.CardItemRight}>
                            <TouchableOpacity onPress={() => { global["kyso_no_signhub"] ? global["kyso_no_signhub"] = null : global["kyso_no_signhub"] = "1"; this.setState({ reload: true }) }} style={{ flex: 1 }}>
                                <Text editable={false} style={[this.styles.textNoiDung, { flex: 1, alignSelf: "flex-end" }]}>{global["kyso_no_signhub"] ? "On" : "Off"}</Text>
                            </TouchableOpacity>
                        </CardItem>
                    </CardItem>
                    {this.renderListItem(this.FILE)}
                    {this.renderListItem(this.KYSO)}
                    {this.renderListItem(this.LICHCONGTAC)}
                    {this.renderListItem(this.LICHHOP)}
                    {this.renderListItem(this.LOGIN)}
                    {this.renderListItem(this.NHANTINSMS)}
                    {this.renderListItem(this.THONGTINDIEUHANH)}
                    {this.renderListItem(this.VANBANDEN)}
                    {this.renderListItem(this.VANBANDI)}
                    {this.renderListItem(this.VANBANNOIBO)}
                </Content>
            </Container>
        );
    }
}

function mapStateToProps(state) {
    return {
        fontSize: state.fontSize,
    }
}

export default connect(mapStateToProps)(AdminSettings)