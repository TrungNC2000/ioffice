import React, { Component, PureComponent } from 'react';
import { StyleSheet, View, PixelRatio, TouchableOpacity, FlatList, Linking, Platform } from "react-native"
import { Container, Content, Spinner, Text, CardItem, Toast, Icon } from "native-base"
import { connect } from "react-redux"
import { DrawerActions } from "react-navigation"
import HeaderWithLeft from "../components/Header/HeaderWithLeft"
import CustomTextInput from "../components/CustomTextInput"
import { AppConfig } from "../AppConfig"
import { CheckSession } from "../untils/NetInfo"
import API from "../networks"

class Search_Item extends PureComponent {
    onPressItem = () => {
        if (this.props.item.li_attr.data_phone) {
            if (Platform.OS === "ios") {
                Linking.openURL('tel://' + this.props.item.li_attr.data_phone)
            } else {
                Linking.openURL('tel:' + this.props.item.li_attr.data_phone)
            }
        } else {
            return
        }
    }

    render() {
        this.styles = this.props.styles
        let { item } = this.props
        return (
            <CardItem style={this.styles.CardItem} key={item.id}>
                <TouchableOpacity style={{ justifyContent: 'center', flex: 1, left: 0 }} onPress={this.onPressItem}>
                    <Text numberOfLines={1} ellipsizeMode="tail" style={this.styles.textNoBold}>{item.text}</Text>
                    {
                        (item.li_attr.data_email) && (
                            <Text numberOfLines={1} ellipsizeMode="tail" style={this.styles.txtNoiDung}>Email: {item.li_attr.data_email}</Text>
                        )
                    }
                    {
                        (item.li_attr.data_chuc_vu) && (
                            <Text numberOfLines={2} ellipsizeMode="tail" style={this.styles.txtNoiDung}>Chức vụ: {item.li_attr.data_chuc_vu}</Text>
                        )
                    }
                </TouchableOpacity>
            </CardItem>
        )
    }

}

class LookupContacts extends Component {
    constructor(props) {
        super(props)
        this.state = {
            dataContacts: [],
            keyWord: "",
            loading: true,
        }
        this.dataContactsOld = []
    }

    componentDidMount = () => {
        this.didBlurSubscription = this.props.navigation.addListener('didFocus', payload => { CheckSession(this.props.navigation) })
        this.getData()
    };

    componentWillUnmount = () => {
        this.didBlurSubscription.remove()
    };

    getData = () => {
        API.Login.AU_CCBTCDB().then(dataContacts => {
            dataContacts = dataContacts.filter(data => data.id.indexOf("CB") !== -1)
            this.dataContactsOld = dataContacts
            this.setState({ dataContacts, loading: false })
        })
    }

    searchData = () => {
        let keyWord = this.state.keyWord.toString().toLowerCase()
        if (keyWord === '') {
            this.setState({ loading: true })
            setTimeout(() => {
                this.setState({ dataContacts: this.dataContactsOld, loading: false })
            }, 500)
            return
        }

        if (!this.state.loading) {
            this.setState({ loading: true })
            let arrSDT = this.dataContactsOld.filter(data => data.li_attr.data_phone !== null)
            arrSDT = arrSDT.filter(data => data.li_attr.data_phone.indexOf(keyWord) !== -1)
            let arrHoTen = this.dataContactsOld.filter(data => data.li_attr.data_ten_can_bo.toLocaleLowerCase().indexOf(keyWord) !== -1)
            let dataContacts = arrSDT.filter(bb => !arrHoTen.find(aa => bb.id === aa.id)).concat(arrHoTen);
            setTimeout(() => {
                this.setState({ dataContacts, loading: false })
            }, 300)
        } else {
            Toast.show({ text: "Đang tải dữ liệu!", type: "warning", buttonText: "Đóng", position: "top", duration: 1500 })
            return
        }
    }

    render() {

        this.styles = StyleSheet.create({
            container: {
                flex: 1,
                backgroundColor: "#FFFFFF"
            },
            CardItemSearch: {
                borderColor: AppConfig.defaultLineColor,
                borderWidth: AppConfig.defaultLineWidth,
                marginTop: 10,
                marginBottom: 10,
                marginLeft: 20,
                marginRight: 20,
                borderRadius: 5,
                paddingLeft: 5,
                paddingRight: 5
            },
            CardItemBody: {
                flex: 1,
                height: 45,
            },
            CardItemRight: {
                backgroundColor: "transparent",
                width: 40,
                height: 40,
                position: 'absolute',
                zIndex: 99,
                right: 0,
                alignItems: 'center',
                justifyContent: "center",
                alignContent: "center",
            },
            TextInputSearch: {
                flex: 1,
                fontFamily: AppConfig.fontFamily,
                fontSize: this.props.fontSize.FontSizeNorman / PixelRatio.getFontScale(),
                color: "#000000",
                paddingRight: 40,
            },
            IconSearch: {
                marginLeft: 3,
                color: AppConfig.blueBackground,
            },
            btnSearch: {
                flex: 1
            },
            placeholder: {
                top: 9,
                left: 6,
                fontSize: this.props.fontSize.FontSizeNorman
            },
            CardItem: {
                flex: 1,
                borderTopWidth: 0,
                borderBottomWidth: 0.55,
                borderLeftWidth: 0,
                borderRightWidth: 0,
                borderBottomColor: "lightgray",
                paddingLeft: 8,
                paddingRight: 8,
                paddingTop: 4,
                paddingBottom: 4,
                alignItems: 'center',
                justifyContent: "center",
            },
            textNoBold: {
                color: "#000000",
                fontSize: this.props.fontSize.FontSizeNorman,
            },
            txtNoiDung: {
                color: "#808080",
                fontSize: this.props.fontSize.FontSizeSmall,
            }
        })

        return (
            <Container style={this.styles.container}>
                <HeaderWithLeft
                    title="Tra cứu danh bạ đơn vị"
                    buttonName="menu"
                    onPressLeftButton={_ => { this.props.navigation.dispatch(DrawerActions.toggleDrawer()) }}
                />
                <CardItem style={this.styles.CardItemSearch}>
                    <CardItem style={this.styles.CardItemBody}>
                        <CustomTextInput
                            style={this.styles.TextInputSearch}
                            value={this.state.keyWord}
                            onChangeText={(text) => { this.setState({ keyWord: text }) }}
                            onFocus={() => { this.setState({ keyWord: "" }) }}
                            placeholder="Nhập từ khóa tìm kiếm ..."
                            underlineColorAndroid={"transparent"}
                            onRef={ref => this.input = ref}
                            placeholderStyle={this.styles.placeholder}
                            onSubmitEditing={this.searchData}
                        />
                    </CardItem>
                    <CardItem style={this.styles.CardItemRight}>
                        <TouchableOpacity onPress={this.searchData} style={this.styles.btnSearch}>
                            <Icon name="search" type="Ionicons" style={this.styles.IconSearch}></Icon>
                        </TouchableOpacity>
                    </CardItem>
                </CardItem>
                <Content>
                    {
                        (this.state.loading) ? (
                            <View><Spinner color={AppConfig.blueBackground} /></View>
                        ) : (
                                <FlatList
                                    data={this.state.dataContacts}
                                    keyExtractor={(item, index) => "key" + item.id + index}
                                    removeClippedSubviews={true}
                                    initialNumToRender={30}
                                    renderItem={({ item }) => {
                                        return (<Search_Item
                                            item={item}
                                            navigation={this.props.navigation}
                                            fontSize={this.props.fontSize}
                                            styles={this.styles}
                                        />)
                                    }}
                                />
                            )
                    }

                </Content>
            </Container>
        );
    }
}

function mapStateToProps(state) {
    return {
        fontSize: state.fontSize,
    }
}

export default connect(mapStateToProps)(LookupContacts)