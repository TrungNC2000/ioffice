import { createDrawerNavigator } from "react-navigation"
import MainContent from "./MainContent"
import MainStack from "../Stack"

export default MainDrawer = createDrawerNavigator(
    {
        MainStack: {
            screen: MainStack,
            navigationOptions: {
                header: null
            }
        },
    },
    {
        initialRouteName: "MainStack",
        drawerWidth: 250,
        drawerPosition: "left",
        useNativeAnimations: true,
        contentComponent: MainContent
    }
)