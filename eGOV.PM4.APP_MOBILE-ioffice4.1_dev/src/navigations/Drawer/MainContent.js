import React, { Component } from 'react';
import { View, StyleSheet, ScrollView, Alert, AsyncStorage, TouchableOpacity, Platform, Linking } from 'react-native';
import { Text, Thumbnail, Badge } from "native-base"
import { connect } from "react-redux"
import { Menu, MenuOptions, MenuOption, MenuTrigger } from 'react-native-popup-menu'
import { AppStyle } from "../../AppStyle"
import { AppConfig } from "../../AppConfig"
import { Row, Col } from "react-native-easy-grid"
import Icon from "react-native-vector-icons/FontAwesome5"
import API from "../../networks"
import { getUrlAvatar, resetStack, checkVersionWeb, getNVOld, getNVNew } from "../../untils/TextUntils"
import axios from "axios"
import AntDesign from "react-native-vector-icons/AntDesign"

export class ItemChild extends Component {
    constructor(props) {
        super(props)
        this.state = {
            countBadge: this.props.countBadge
        }
    }

    shouldComponentUpdate = (nextProps, nextState) => {
        if (this.props !== nextProps ||
            this.state.countBadge !== nextState.countBadge) {
            return true
        }
        return false
    }

    componentWillReceiveProps(nextProps, nextState) {
        this.setState({ countBadge: nextProps.countBadge })
    }

    render() {
        this.styles = this.props.styles
        let { onPress, iconName, title } = this.props
        return (
            <View style={this.styles.iTemChild}>
                <Row onPress={onPress}>
                    <Col style={this.styles.colLeft}>
                        <Icon name={iconName} style={AppStyle.drawerIcon} />
                    </Col>
                    <Col style={this.styles.colBody}>
                        <Text style={this.styles.textItem}>{title}</Text>
                    </Col>
                    {
                        (this.state.countBadge > 0) && (
                            <Col style={this.styles.colRight}>
                                <Badge style={this.styles.badge}>
                                    <Text style={this.styles.badgeText}>{this.state.countBadge.toString()}</Text>
                                </Badge>
                            </Col>
                        )
                    }
                </Row>
            </View>
        )
    }
}
class MainContent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isShowVBDen: true,
            isShowVBDi: true,
            isShowTTDH: false,
            isShowUser: false,
            isShowHelp: false,
            avatar: require("../../assets/images/iAvatarUser.png"),
        }
        this.nvVBDen_Duyet = 0
        this.nvVBDen_XuLy = 0
        this.nvVBDi_Duyet = 0
        this.nvVBDi_XuLy = 0
        this.nvVBDi_DaPhatHanh = 0
        this.nvVBNoiBo = 0
        this.nvTTDH = 0
        this.nvCDDH = 0
        this.isLanhDao = false
        this.isChuyenVien = false
        this.storeSpecificId = Platform.OS === "ios" ? "1455959048" : "com.ioffice41";
    }

    componentDidMount = () => {
        this.setAvatar()
    }

    componentWillReceiveProps(nextProps) {
        let cb_dsnvcb = this.props.cb_dsnvcb
        if (checkVersionWeb(21)) {
            this.nvSoVBDen = getNVNew("vbde", "van-ban-den/so-van-ban-den?page=1&t=so_vb_den_dien_tu_cua_vt", cb_dsnvcb)
            this.nvVBDen_XuLy = getNVNew("vbde", "van-ban-den/van-ban-den-chua-xu-ly?page=1", cb_dsnvcb) + getNVNew("vbde", "van-ban-den/van-ban-den-dang-xu-ly?page=1", cb_dsnvcb)
            this.nvVBDen_Duyet = getNVNew("vbde", "van-ban-den/duyet-van-ban-den?page=1&t=vb_den_cho_duyet_cua_ld", cb_dsnvcb) + + getNVNew("vbde", "van-ban-den/duyet-van-ban-den?page=1&t=vb_den_uy_quyen_duyet_cua_ld", cb_dsnvcb)
            this.nvVBDi_XuLy = getNVNew("vbdi", "van-ban-di/van-ban-di-cho-xu-ly?page=1", cb_dsnvcb)
            if (global.AU_ROOT && (global.AU_ROOT.includes("phutho") || global.AU_ROOT.includes("nghean"))) {
                this.nvVBDi_DaPhatHanh = getNVNew("vbdi", "van-ban-di/van-ban-di-dph-chua-xem-pto?page=1", cb_dsnvcb)
            } else {
                this.nvVBDi_DaPhatHanh = getNVNew("vbdi", "van-ban-di/van-ban-di-da-phat-hanh?page=1", cb_dsnvcb)
            }
            this.nvVBDi_Duyet = getNVNew("vbdi", "van-ban-di/duyet-van-ban-di?page=1&t=vb_di_cho_duyet_cua_ld", cb_dsnvcb) + getNVNew("vbdi", "van-ban-di/duyet-van-ban-di?page=1&t=vb_di_duoc_uy_quyen_cua_ld", cb_dsnvcb)
            this.nvTTDH = getNVNew("khac", "thong-diep/thong-tin-dieu-hanh-chua-xem?page=1", cb_dsnvcb)
            this.nvVBNoiBo = getNVNew("vbnb", "van-ban-noi-bo/van-ban-noi-bo-da-nhan?page=1", cb_dsnvcb)
        } else {
            this.nvVBDen_XuLy = getNVOld("van-ban-den-chua-xu-ly?page", "", cb_dsnvcb) + getNVOld("van-ban-den-dang-xu-ly?page", "", cb_dsnvcb)
            this.nvVBDen_Duyet = getNVOld("duyet-van-ban-den?page?t", "vb_den_cho_duyet_cua_ld", cb_dsnvcb) + getNVOld("duyet-van-ban-den?page?t", "vb_den_uy_quyen_duyet_cua_ld", cb_dsnvcb)
            this.nvVBDi_XuLy = getNVOld("van-ban-di-cho-xu-ly?page", "", cb_dsnvcb)
            this.nvVBDi_DaPhatHanh = getNVOld("van-ban-di-da-phat-hanh?page", "", cb_dsnvcb)
            this.nvVBDi_Duyet = getNVOld("duyet-van-ban-di?page?t", "vb_di_cho_duyet_cua_ld", cb_dsnvcb) + getNVOld("duyet-van-ban-di?page?t", "vb_di_duoc_uy_quyen_cua_ld", cb_dsnvcb)
            this.nvTTDH = getNVOld("thong-tin-dieu-hanh-da-nhan?page", "", cb_dsnvcb)
            this.nvVBNoiBo = getNVOld("van-ban-noi-bo-da-nhan?page", "", cb_dsnvcb)
        }
    }

    resetStack = (screen) => {
        this.props.navigation.dispatch(resetStack(screen))
    }

    setAvatar = () => {
        let avatar = this.props.cb_dsctcb.avatar
        if (avatar) {
            let url = getUrlAvatar(this.props.cb_dsctcb.avatar)
            axios({
                method: "GET",
                url: url,
                timeout: AppConfig.TIME_OUT_SHORT
            }).then(response => {
                if (response.status === 200) {
                    this.setState({
                        avatar: { uri: url }
                    })
                }
            }).catch(error => {
            })
        }
    }

    onSetting = () => {

        if (checkVersionWeb(21)) {
            this.props.navigation.navigate("UserSettings")
        } else {
            this.props.navigation.navigate("UserSettings_Old")
        }
    }

    onLogout = () => {
        Alert.alert(
            "Xác nhận",
            "Bạn muốn đăng xuất tài khoản?",
            [
                { text: "Không", onPress: () => console.log("Logout"), style: "cancel" },
                {
                    text: "Có", onPress: () => {
                        API.Login.AU_DEL_FCM()
                        AsyncStorage.setItem("chkSaveLogin", "false")
                        AsyncStorage.setItem("Url_Api_SaveLogin", "")
                        AsyncStorage.setItem("US_First_Load_Page", "Home")
                        AsyncStorage.setItem("chkSaveDeepLinkSso","false")
                        this.props.dispatch({ type: "NORMAN_FONT" })
                        global.isLogin = false
                        this.props.navigation.navigate("Login", { loaiDangNhap: "dangXuat" })
                    }
                }
            ],
            { cancelable: false }
        )
    }

    onPressVbDen = () => {
        this.setState({
            isShowVBDen: !this.state.isShowVBDen
        })
    }

    renderChildVbDen = () => {
        if (!this.state.isShowVBDen) {
            return null
        } else {
            if (global.lanh_dao === 1) { //Là lãnh đạo
                return (
                    <View>
                        <ItemChild
                            onPress={() => { this.resetStack("VBDenDuyet") }}
                            iconName={'file'}
                            title="Duyệt"
                            countBadge={this.nvVBDen_Duyet}
                            fontSize={this.props.fontSize}
                            styles={this.styles}
                        />
                        <ItemChild
                            onPress={() => { this.resetStack("VBDenXuLy") }}
                            iconName={'file'}
                            title="Xử lý"
                            countBadge={this.nvVBDen_XuLy}
                            fontSize={this.props.fontSize}
                            styles={this.styles}
                        />
                    </View>
                )
            } else { //Là chuyên viên
                return (
                    <View>
                        {
                            (global.van_thu === 1) && (
                                <ItemChild
                                    onPress={() => { this.resetStack("VBDenVanThu") }}
                                    iconName={'file'}
                                    title="Sổ văn bản đến"
                                    countBadge={this.nvSoVBDen}
                                    fontSize={this.props.fontSize}
                                    styles={this.styles}
                                />
                            )
                        }
                        <ItemChild
                            onPress={() => { this.resetStack("VBDenXuLy") }}
                            iconName={'file'}
                            title="Xử lý"
                            countBadge={this.nvVBDen_XuLy}
                            fontSize={this.props.fontSize}
                            styles={this.styles}
                        />
                    </View>
                )
            }
        }
    }

    onPressVbDi = () => {
        this.setState({
            isShowVBDi: !this.state.isShowVBDi
        })
    }

    renderChildVbDi = () => {
        if (!this.state.isShowVBDi) {
            return null
        } else {
            if (global.lanh_dao === 1) { //Là lãnh đạo
                return (
                    <View>
                        <ItemChild
                            onPress={() => { this.resetStack("VBDiDuyet") }}
                            iconName={'file'}
                            title="Duyệt"
                            countBadge={this.nvVBDi_Duyet}
                            fontSize={this.props.fontSize}
                            styles={this.styles}
                        />
                        <ItemChild
                            onPress={() => { this.resetStack("VBDiXuLy") }}
                            iconName={'file'}
                            title="Xử lý"
                            countBadge={this.nvVBDi_XuLy}
                            fontSize={this.props.fontSize}
                            styles={this.styles}
                        />
                        <ItemChild
                            onPress={() => { this.props.navigation.navigate("VBDiXuLy", { activeTab: 3 }) }}
                            iconName={'file'}
                            title="Đã phát hành"
                            countBadge={this.nvVBDi_DaPhatHanh}
                            fontSize={this.props.fontSize}
                            styles={this.styles}
                        />
                    </View>
                )
            } else { //Là chuyên viên
                return (
                    <View>
                        <ItemChild
                            onPress={() => { this.resetStack("VBDiXuLy") }}
                            iconName={'file'}
                            title="Xử lý"
                            countBadge={this.nvVBDi_XuLy}
                            fontSize={this.props.fontSize}
                            styles={this.styles}
                        />
                        <ItemChild
                            onPress={() => { this.props.navigation.navigate("VBDiXuLy", { activeTab: 3 }) }}
                            iconName={'file'}
                            title="Đã phát hành"
                            countBadge={this.nvVBDi_DaPhatHanh}
                            fontSize={this.props.fontSize}
                            styles={this.styles}
                        />
                    </View>
                )
            }
        }
    }

    showArrow = (flag) => {
        if (flag) {
            return (
                <Icon name="chevron-left" style={AppStyle.drawerIcon} />
            )
        } else {
            return (
                <Icon name="chevron-down" style={AppStyle.drawerIcon} />
            )
        }
    }

    rennderChucVu = () => {
        let result = []
        this.props.cb_dsctcb_all.forEach(element => {
            if (element.ten_chuc_vu !== this.props.cb_dsctcb.ten_chuc_vu) {
                result.push(
                    <MenuOption key={element.ma_ctcb_kc} onSelect={() => { this.onPressChangeCV(element) }} >
                        <Text style={this.styles.menuOptionsText}>{element.ten_chuc_vu}</Text>
                    </MenuOption>
                )
            } else {
                result.push(
                    <MenuOption key={element.ma_ctcb_kc}>
                        <Text style={this.styles.menuOptionsText}>{element.ten_chuc_vu}</Text>
                    </MenuOption>
                )
            }

        });
        return result
    }

    onPressUser = () => {
        this.setState({
            isShowUser: !this.state.isShowUser
        })
    }

    renderChildUser = () => {
        if (!this.state.isShowUser) {
            return null
        } else {
            return (
                <View>
                    <ItemChild
                        onPress={() => { this.props.navigation.navigate("UserInfo") }}
                        iconName={'user-edit'}
                        title="Thông tin cá nhân"
                        fontSize={this.props.fontSize}
                        styles={this.styles}
                    />
                    {/* <ItemChild
                        onPress={() => { return Toast.show({ text: "Tính năng đang phát triển", type: "success", buttonText: "Đóng", position: "top", duration: 2000 }) }}
                        iconName={'unlock'}
                        title="Đổi mật khẩu"
                    />
                    <ItemChild
                        onPress={() => { return Toast.show({ text: "Tính năng đang phát triển", type: "success", buttonText: "Đóng", position: "top", duration: 2000 }) }}
                        iconName={'user-check'}
                        title="Đăng ký OTP"
                    /> */}
                    <ItemChild
                        onPress={() => { this.resetStack("LookupContacts") }}
                        iconName={'address-book'}
                        title="Danh bạ"
                        fontSize={this.props.fontSize}
                        styles={this.styles}
                    />
                    <ItemChild
                        onPress={() => { this.resetStack("MauChuKy") }}
                        iconName={'file-signature'}
                        title="Mẫu chữ ký"
                        fontSize={this.props.fontSize}
                        styles={this.styles}
                    />
                </View>
            )
        }
    }

    onPressHelp = () => {
        this.setState({
            isShowHelp: !this.state.isShowHelp
        })
    }

    renderChildHelp = () => {
        let iconVersion = (Platform.OS == "ios") ? "app-store-ios" : "google-play"
        if (!this.state.isShowHelp) {
            return null
        } else {
            return (
                <View>
                    <ItemChild
                        onPress={() => { this.props.navigation.navigate("IntroduceApp") }}
                        iconName={'info-circle'}
                        title="Giới thiệu"
                        fontSize={this.props.fontSize}
                        styles={this.styles}
                    />
                    <ItemChild
                        onPress={() => { this.props.navigation.navigate("AppPolicy") }}
                        iconName={'bullhorn'}
                        title="Điều khoản sử dụng"
                        fontSize={this.props.fontSize}
                        styles={this.styles}
                    />
                    <ItemChild
                        onPress={() => { Linking.openURL(global.storeURL); }}
                        iconName={'star-half-alt'}
                        title="Đánh giá"
                        fontSize={this.props.fontSize}
                        styles={this.styles}
                    />
                </View>
            )
        }
    }

    onPressChangeCV = async (item) => {
        Alert.alert(
            "Xác nhận",
            "Bạn muốn đổi kiêm nhiệm sang " + item.ten_chuc_vu + "?",
            [
                { text: "Không", onPress: () => console.log("Logout"), style: "cancel" },
                {
                    text: "Có", onPress: () => {
                        API.Login.AU_DEL_FCM()
                        AsyncStorage.setItem("ma_ctcb_kc_Store", item.ma_ctcb_kc.toString(), () => {
                            this.props.dispatch({ type: "NORMAN_FONT" })
                            global.isLogin = false
                            this.props.navigation.navigate("Login", { loaiDangNhap: "doiKiemNhiem" })
                        })



                        API.Login.AU_DEL_FCM()

                    }
                }
            ],
            { cancelable: false }
        )
    }

    render() {

        this.styles = StyleSheet.create({
            controlPanel: {
                flex: 1,
                backgroundColor: "#FFFFFF",
            },
            itemInfoUser: {
                flexDirection: "row",
                backgroundColor: AppConfig.blueBackground,
                width: "100%",
                height: 100,
                padding: 8,
            },
            textInfoUser: {
                color: "#FFFFFF",
                fontSize: this.props.fontSize.FontSizeNorman,
                paddingTop: 1,
                paddingBottom: 1,
                textAlign: "center",
            },
            textNoteInfoUser: {
                color: "#FFFFFF",
                fontSize: this.props.fontSize.FontSizeSmall,
                paddingTop: 1,
                paddingBottom: 1,
                textAlign: "center"
            },
            viewInfoUser: {
                flex: 1,
                //flexDirection: 'row',
                marginLeft: 10,
                alignItems: 'center',
                justifyContent: 'center',
                //flexWrap: "wrap",
                //flexShrink: 1,
            },
            iTem: {
                padding: 10,
                borderColor: AppConfig.defaultLineColor,
                borderBottomWidth: AppConfig.defaultLineWidth,
                borderBottomLeftRadius: 10,
                borderBottomRightRadius: 10
            },
            iTemChild: {
                paddingTop: 10,
                paddingBottom: 10,
                paddingRight: 10,
                paddingLeft: 30,
                borderColor: AppConfig.defaultLineColor,
                borderBottomWidth: AppConfig.defaultLineWidth,
                borderBottomLeftRadius: 10,
                borderBottomRightRadius: 10
            },
            colLeft: {
                width: 20,
                justifyContent: "center"
            },
            colBody: {
                //width: "70%",
                justifyContent: "center",
                marginLeft: 5
            },
            colRight: {
                width: 50,
                alignItems: "center",
                justifyContent: "center",
            },
            textItem: {
                color: AppConfig.blueBackground,
                fontSize: this.props.fontSize.FontSizeNorman
            },
            badge: {
                alignSelf: 'center',
                justifyContent: "center",
                alignItems: "center"
            },
            badgeText: {
                fontSize: this.props.fontSize.FontSizeSmall - 2
            },
            menuTrigger: {
                width: 100,
                height: 60,
                justifyContent: "center",
                alignItems: 'center'
            },
            menuTriggerIcon: {
                fontSize: this.props.fontSize.FontSizeXXXLarge,
                color: "#B0BEC5"
            },
            menuOptionsContainer: {
                width: 200
            },
            menuOptions: {
                borderBottomColor: AppConfig.defaultLineColor,
                borderBottomWidth: 0.5
            },
            menuOptionsText: {
                color: AppConfig.headerBackgroundColor,
                fontSize: this.props.fontSize.FontSizeNorman,
                paddingTop: AppConfig.defaultPadding,
                paddingBottom: AppConfig.defaultPadding
            },
            menuOptionsTextDelete: {
                color: "red",
                fontSize: this.props.fontSize.FontSizeNorman,
                paddingTop: AppConfig.defaultPadding,
                paddingBottom: AppConfig.defaultPadding
            }
        });

        return (
            <View style={this.styles.controlPanel}>
                <ScrollView bounces={false}>
                    <View style={this.styles.itemInfoUser}>
                        <View>
                            <Thumbnail large source={this.state.avatar} />
                        </View>
                        <View style={this.styles.viewInfoUser}>
                            <TouchableOpacity onPress={() => { this.props.navigation.navigate("UserInfo") }}>
                                <Text style={this.styles.textInfoUser}>{this.props.cb_dsctcb.ho_va_ten_can_bo}</Text>
                            </TouchableOpacity>
                            {
                                (this.props.cb_dsctcb_all.length === 1)
                                    ? (
                                        <Text style={this.styles.textNoteInfoUser}>{this.props.cb_dsctcb.ten_chuc_vu}</Text>
                                    ) : (
                                        <Menu>
                                            <MenuTrigger style={this.styles.menuTrigger}>
                                                <Text style={this.styles.textNoteInfoUser}>{this.props.cb_dsctcb.ten_chuc_vu}</Text>
                                                <Icon name="chevron-down" style={this.styles.menuTriggerIcon}></Icon>
                                            </MenuTrigger>
                                            <MenuOptions optionsContainerStyle={this.styles.menuOptionsContainer}>
                                                {this.rennderChucVu()}
                                            </MenuOptions>
                                        </Menu>
                                    )
                            }
                        </View>
                    </View>
                    <View style={this.styles.iTem}>
                        <Row onPress={() => { this.resetStack("Home") }}>
                            <Col style={this.styles.colLeft}>
                                <Icon name="home" style={AppStyle.drawerIcon} />
                            </Col>
                            <Col style={this.styles.colBody}>
                                <Text style={this.styles.textItem}>Trang chủ</Text>
                            </Col>
                        </Row>
                    </View>
                    <View style={this.styles.iTem}>
                        <Row onPress={this.onPressVbDen}>
                            <Col style={this.styles.colLeft}>
                                <Icon name="file-alt" style={AppStyle.drawerIcon} />
                            </Col>
                            <Col style={this.styles.colBody}>
                                <Text style={this.styles.textItem}>Văn bản đến</Text>
                            </Col>
                            <Col style={this.styles.colRight}>
                                {this.showArrow(this.state.isShowVBDen)}
                            </Col>
                        </Row>
                    </View>
                    {this.renderChildVbDen()}
                    <View style={this.styles.iTem}>
                        <Row onPress={this.onPressVbDi}>
                            <Col style={this.styles.colLeft}>
                                <Icon name="file-alt" style={AppStyle.drawerIcon} />
                            </Col>
                            <Col style={this.styles.colBody}>
                                <Text style={this.styles.textItem}>Văn bản đi</Text>
                            </Col>
                            <Col style={this.styles.colRight}>
                                {this.showArrow(this.state.isShowVBDi)}
                            </Col>
                        </Row>
                    </View>
                    {this.renderChildVbDi()}
                    <View style={this.styles.iTem}>
                        <Row onPress={() => { this.resetStack("VBDen_Search") }}>
                            <Col style={this.styles.colLeft}>
                                <Icon name="search" style={AppStyle.drawerIcon} />
                            </Col>
                            <Col style={this.styles.colBody}>
                                <Text style={this.styles.textItem}>Tìm kiếm chung</Text>
                            </Col>
                        </Row>
                    </View>
                    <View style={this.styles.iTem}>
                        <Row onPress={() => { this.resetStack("TTDH") }}>
                            <Col style={this.styles.colLeft}>
                                <Icon name="tasks" style={AppStyle.drawerIcon} />
                            </Col>
                            <Col style={this.styles.colBody}>
                                <Text style={this.styles.textItem}>Thông tin điều hành</Text>
                            </Col>
                            {
                                (this.nvTTDH > 0) && (
                                    <Col style={this.styles.colRight}>
                                        <Badge style={this.styles.badge}>
                                            <Text style={this.styles.badgeText}>{this.nvTTDH.toString()}</Text>
                                        </Badge>
                                    </Col>
                                )
                            }
                        </Row>
                    </View>
                    <View style={this.styles.iTem}>
                        <Row onPress={() => { this.resetStack("VBNB") }}>
                            <Col style={this.styles.colLeft}>
                                <Icon name="exchange-alt" style={AppStyle.drawerIcon} />
                            </Col>
                            <Col style={this.styles.colBody}>
                                <Text style={this.styles.textItem}>Văn bản nội bộ</Text>
                            </Col>
                            {
                                (this.nvVBNoiBo > 0) && (
                                    <Col style={this.styles.colRight}>
                                        <Badge style={this.styles.badge}>
                                            <Text style={this.styles.badgeText}>{this.nvVBNoiBo.toString()}</Text>
                                        </Badge>
                                    </Col>
                                )
                            }
                        </Row>
                    </View>
                    <View style={this.styles.iTem}>
                        <Row onPress={() => { this.resetStack("LCT") }}>
                            <Col style={this.styles.colLeft}>
                                <Icon name="calendar-alt" style={AppStyle.drawerIcon} />
                            </Col>
                            <Col style={this.styles.colBody}>
                                <Text style={this.styles.textItem}>Lịch công tác</Text>
                            </Col>
                        </Row>
                    </View>
                    <View style={this.styles.iTem}>
                        <Row onPress={() => { this.resetStack("LH") }}>
                            <Col style={this.styles.colLeft}>
                                <Icon name="calendar-check" style={AppStyle.drawerIcon} />
                            </Col>
                            <Col style={this.styles.colBody}>
                                <Text style={this.styles.textItem}>Lịch họp</Text>
                            </Col>
                        </Row>
                    </View>
                    <View style={this.styles.iTem}>
                        <Row onPress={() => { this.resetStack("SMS") }}>
                            <Col style={this.styles.colLeft}>
                                <Icon name="comment" style={AppStyle.drawerIcon} />
                            </Col>
                            <Col style={this.styles.colBody}>
                                <Text style={this.styles.textItem}>Nhắn tin sms</Text>
                            </Col>
                        </Row>
                    </View>
                    <View style={this.styles.iTem}>
                        <Row onPress={this.onPressUser}>
                            <Col style={this.styles.colLeft}>
                                <Icon name="user" style={AppStyle.drawerIcon} />
                            </Col>
                            <Col style={this.styles.colBody}>
                                <Text style={this.styles.textItem}>Cá nhân</Text>
                            </Col>
                            <Col style={this.styles.colRight}>
                                {this.showArrow(this.state.isShowUser)}
                            </Col>
                        </Row>
                    </View>
                    {this.renderChildUser()}
                    <View style={this.styles.iTem}>
                        <Row onPress={_ => this.onSetting()}>
                            <Col style={this.styles.colLeft}>
                                <Icon name="cog" style={AppStyle.drawerIcon} />
                            </Col>
                            <Col style={this.styles.colBody}>
                                <Text style={this.styles.textItem}>Cài đặt</Text>
                            </Col>
                        </Row>
                    </View>
                    <View style={this.styles.iTem}>
                        <Row onPress={this.onPressHelp}>
                            <Col style={this.styles.colLeft}>
                                <AntDesign name="questioncircleo" style={AppStyle.drawerIcon} />
                            </Col>
                            <Col style={this.styles.colBody}>
                                <Text style={this.styles.textItem}>Trợ giúp</Text>
                            </Col>
                            <Col style={this.styles.colRight}>
                                {this.showArrow(this.state.isShowHelp)}
                            </Col>
                        </Row>
                    </View>
                    {this.renderChildHelp()}
                    <View style={this.styles.iTem}>
                        <Row onPress={_ => this.onLogout()}>
                            <Col style={this.styles.colLeft}>
                                <Icon name="sign-out-alt" style={AppStyle.drawerIcon} />
                            </Col>
                            <Col style={this.styles.colBody}>
                                <Text style={this.styles.textItem}>Đăng xuất</Text>
                            </Col>
                        </Row>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

function mapStateToProps(state) {
    return {
        cb_dsctcb: state.cb_dsctcb.data,
        cb_dsctcb_all: state.cb_dsctcb.dataAll,
        cb_dsnvcb: state.cb_dsnvcb.data,
        fontSize: state.fontSize,
    }
}


export default connect(mapStateToProps)(MainContent)