import { createAppContainer, createSwitchNavigator, createStackNavigator, createDrawerNavigator } from "react-navigation"
import { Easing, Animated } from "react-native"
import Splash from "../../screen/Splash"
import IntroSlider from "../../screen/IntroSlider"
import Login from "../../screen/Login"
import { MainDrawer } from "../Drawer"
const MainSwitch = createSwitchNavigator(
    {
        Splash: {
            screen: Splash,
        },
        IntroSlider: {
            screen: IntroSlider,
        },
        Login: {
            screen: Login,
        },
        MainDrawer: {
            screen: MainDrawer,
            navigationOptions: {
                header: null
            }
        }
    },
    {
        initialRouteName: "Splash",
        // transitionConfig: () => ({
        //     transitionSpec: {
        //         duration: 300,
        //         easing: Easing.out(Easing.poly(4)),
        //         timing: Animated.timing,
        //     },
        //     screenInterpolator: sceneProps => {
        //         const { layout, position, scene } = sceneProps;
        //         const { index } = scene;

        //         const height = layout.initHeight;
        //         const translateY = position.interpolate({
        //             inputRange: [index - 1, index, index + 1],
        //             outputRange: [height, 0, 0],
        //         });

        //         const opacity = position.interpolate({
        //             inputRange: [index - 1, index - 0.99, index],
        //             outputRange: [0, 1, 1],
        //         });

        //         return { opacity, transform: [{ translateY }] };
        //     },
        // }),
    }
)

const AppContainer = createAppContainer(MainSwitch)
export default AppContainer
