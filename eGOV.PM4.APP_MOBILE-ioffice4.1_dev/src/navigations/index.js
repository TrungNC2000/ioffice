import { createAppContainer } from "react-navigation"
import {MainSwitch} from "./Switch"
const AppContainer = createAppContainer(MainSwitch)
export default AppContainer
