import { createStackNavigator } from "react-navigation"

import Home from "../../screen/Home"
import UserInfo from "../../screen/UserInfo"
import UserSettings from "../../screen/UserSettings"
import UserSettings_Old from "../../screen/UserSettings_Old"
import AdminSettings from "../../screen/AdminSettings"
import LookupContacts from "../../screen/LookupContacts"
import MauChuKy from "../../screen/MauChuKy"
import AppPolicy from "../../screen/AppPolicy"

import VBDenXuLy from "../../screen/vbden/VBDenXuLy"
import VBDenDuyet from "../../screen/vbden/VBDenDuyet"
import VBDenVanThu from "../../screen/vbden/VBDenVanThu"
import VBDen_Details from "../../screen/vbden/VBDen_Details"
import VBDen_ThemMoi from "../../screen/vbden/VBDen_ThemMoi"
import VBDen_LuuNhap from "../../screen/vbden/VBDen_LuuNhap"
import VBDen_Details_iPad from "../../screen/vbden/VBDen_Details_iPad"
import VBDen_Details_LDChuyenLDK from "../../screen/vbden/VBDen_Details_LDChuyenLDK"
import VBDen_NhapYKien from "../../screen/vbden/VBDen_NhapYKien"
import VBDen_LDNhapYKien from "../../screen/vbden/VBDen_LDNhapYKien"
import VBDen_Chuyen_XuLy from "../../screen/vbden/VBDen_Chuyen_XuLy"
import VBDen_Chuyen_Duyet from "../../screen/vbden/VBDen_Chuyen_Duyet"
import VBDen_ChuyenTiep_LD from "../../screen/vbden/VBDen_ChuyenTiep_LD"
import VBDen_Search from "../../screen/vbden/VBDen_Search"

import VBDiXuLy from "../../screen/vbdi/VBDiXuLy"
import VBDiDuyet from "../../screen/vbdi/VBDiDuyet"
import VBDi_Details from "../../screen/vbdi/VBDi_Details"
import VBDi_Details_LDChuyenVT from "../../screen/vbdi/VBDi_Details_LDChuyenVT"
import VBDi_Details_LDChuyenCV from "../../screen/vbdi/VBDi_Details_LDChuyenCV"
import VBDi_Details_LDChuyenLDK from "../../screen/vbdi/VBDi_Details_LDChuyenLDK"
import VBDi_Details_CVChuyenLD from "../../screen/vbdi/VBDi_Details_CVChuyenLD"
import VBDi_Details_CVChuyenCV from "../../screen/vbdi/VBDi_Details_CVChuyenCV"
import VBDi_Details_CVChuyenVT from "../../screen/vbdi/VBDi_Details_CVChuyenVT"
import VBDi_Details_LDChuyenKTTT from "../../screen/vbdi/VBDi_Details_LDChuyenKTTT"
import VBDi_CVChuyenCV from "../../screen/vbdi/VBDi_CVChuyenCV"
import VBDi_CVGopY from "../../screen/vbdi/VBDi_CVGopY"

import TTDH from "../../screen/ttdh/TTDH"
import TTDH_Details from "../../screen/ttdh/TTDH_Details"
import TTDH_Search_Detail from "../../screen/GeneralSearchDetail/TTDH_Search_Detail"
import TTDH_Gui from "../../screen/ttdh/TTDH_Gui"
import TTDH_Gui1 from "../../screen/ttdh/TTDH_Gui1"

import VBNB from "../../screen/vbnb/VBNB"
import VBNB_Details from "../../screen/vbnb/VBNB_Details"
import VBNB_Chuyen from "../../screen/vbnb/VBNB_Chuyen"
import VBNB_Search_Detail from '../../screen/GeneralSearchDetail/VBNB_Search_Detail'

import LCT from "../../screen/lichcongtac/LCT"
import LCT_CoQuan_Duyet from "../../screen/lichcongtac/LCT_CoQuan_Duyet"

import MeetingList from "../../screen/lichhop/MeetingList"
import Meeting_Details from "../../screen/lichhop/Meeting_Details"
import Meeting_Search_Detail from "../../screen/GeneralSearchDetail/Meeting_Search_Detail"

import SMS from "../../screen/sms/SMS"
import SMS_Gui from "../../screen/sms/SMS_Gui"

import VBDi_DSDP7N from "../../screen/vbdi/screenCMU/VBDi_DSDP7N"
import VBDi_Details_Cmu from "../../screen/vbdi/screenCMU/VBDi_Details_Cmu"
import VBDe_TheoDoi  from "../../screen/vbdi/screenCMU/VBDe_TheoDoi"
import VBDen_Details_Cmu from "../../screen/vbdi/screenCMU/VBDen_Details_Cmu"

import ModalWebView from "../../components/ModalWebView"

// Trợ giúp
import IntroduceApp from "../../screen/IntroduceApp"

// Xin ý kiến thường trực - NAN
import XYKTT from "../../screen/xyktt/XYKTT"
import XYKTT_Details from "../../screen/xyktt/XYKTT_Details"
import XYKTT_Gui from "../../screen/xyktt/XYKTT_Gui"

export default MainStack = createStackNavigator(
    {

        Home: {
            screen: Home,
            navigationOptions: {
                header: null
            }
        },
        UserInfo: {
            screen: UserInfo,
            navigationOptions: {
                header: null
            }
        },
        UserSettings: {
            screen: UserSettings,
            navigationOptions: {
                header: null
            }
        },
        UserSettings_Old: {
            screen: UserSettings_Old,
            navigationOptions: {
                header: null
            }
        },
        AdminSettings: {
            screen: AdminSettings,
            navigationOptions: {
                header: null
            }
        },
        LookupContacts: {
            screen: LookupContacts,
            navigationOptions: {
                header: null
            }
        },
        MauChuKy: {
            screen: MauChuKy,
            navigationOptions: {
                header: null
            }
        },
        AppPolicy: {
            screen: AppPolicy,
            navigationOptions: {
                header: null
            }
        },

        //START VBDEN
        VBDenXuLy: {
            screen: VBDenXuLy,
            navigationOptions: {
                header: null
            }
        },
        VBDenDuyet: {
            screen: VBDenDuyet,
            navigationOptions: {
                header: null
            }
        },
        VBDenVanThu: {
            screen: VBDenVanThu,
            navigationOptions: {
                header: null
            }
        },
        VBDen_Details: {
            screen: VBDen_Details,
            navigationOptions: {
                header: null
            }
        },
        VBDen_ThemMoi: {
            screen: VBDen_ThemMoi,
            navigationOptions: {
                header: null
            }
        },
        VBDen_LuuNhap: {
            screen: VBDen_LuuNhap,
            navigationOptions: {
                header: null
            }
        },
        VBDen_Details_iPad: {
            screen: VBDen_Details_iPad,
            navigationOptions: {
                header: null
            }
        },
        VBDen_Details_LDChuyenLDK: {
            screen: VBDen_Details_LDChuyenLDK,
            navigationOptions: {
                header: null
            }
        },
        VBDen_NhapYKien: {
            screen: VBDen_NhapYKien,
            navigationOptions: {
                header: null
            }
        },
        VBDen_LDNhapYKien: {
            screen: VBDen_LDNhapYKien,
            navigationOptions: {
                header: null
            }
        },
        VBDen_Chuyen_XuLy: {
            screen: VBDen_Chuyen_XuLy,
            navigationOptions: {
                header: null
            }
        },
        VBDen_Chuyen_Duyet: {
            screen: VBDen_Chuyen_Duyet,
            navigationOptions: {
                header: null
            }
        },
        VBDen_ChuyenTiep_LD: {
            screen: VBDen_ChuyenTiep_LD,
            navigationOptions: {
                header: null
            }
        },
        VBDen_Search: {
            screen: VBDen_Search,
            navigationOptions: {
                header: null
            }
        },
        //END VBDEN

        //START VBDI
        VBDiXuLy: {
            screen: VBDiXuLy,
            navigationOptions: {
                header: null
            }
        },
        VBDiDuyet: {
            screen: VBDiDuyet,
            navigationOptions: {
                header: null
            }
        },
        VBDi_Details: {
            screen: VBDi_Details,
            navigationOptions: {
                header: null
            }
        },
        VBDi_Details_LDChuyenVT: {
            screen: VBDi_Details_LDChuyenVT,
            navigationOptions: {
                header: null
            }
        },
        VBDi_Details_LDChuyenCV: {
            screen: VBDi_Details_LDChuyenCV,
            navigationOptions: {
                header: null
            }
        },
        VBDi_Details_LDChuyenLDK: {
            screen: VBDi_Details_LDChuyenLDK,
            navigationOptions: {
                header: null
            }
        },
        VBDi_Details_CVChuyenLD: {
            screen: VBDi_Details_CVChuyenLD,
            navigationOptions: {
                header: null
            }
        },
        VBDi_Details_LDChuyenKTTT: {
            screen: VBDi_Details_LDChuyenKTTT,
            navigationOptions: {
                header: null
            }
        },
        VBDi_Details_CVChuyenCV: {
            screen: VBDi_Details_CVChuyenCV,
            navigationOptions: {
                header: null
            }
        },
        VBDi_Details_CVChuyenVT: {
            screen: VBDi_Details_CVChuyenVT,
            navigationOptions: {
                header: null
            }
        },
        VBDi_CVChuyenCV: {
            screen: VBDi_CVChuyenCV,
            navigationOptions: {
                header: null
            }
        },
        VBDi_CVGopY: {
            screen: VBDi_CVGopY,
            navigationOptions: {
                header: null
            }
        },
        //END VBDI

        //START TTDH
        TTDH: {
            screen: TTDH,
            navigationOptions: {
                header: null
            }
        },
        TTDH_Details: {
            screen: TTDH_Details,
            navigationOptions: {
                header: null
            }
        },
        TTDH_Search_Detail: {
            screen: TTDH_Search_Detail,
            navigationOptions: {
                header: null
            }
        },
        TTDH_Gui: {
            screen: TTDH_Gui,
            navigationOptions: {
                header: null
            }
        },
        TTDH_Gui1: {
            screen: TTDH_Gui1,
            navigationOptions: {
                header: null
            }
        },
        //END TTDH

        //START VBNB
        VBNB: {
            screen: VBNB,
            navigationOptions: {
                header: null
            }
        },
        VBNB_Details: {
            screen: VBNB_Details,
            navigationOptions: {
                header: null
            }
        },
        VBNB_Search_Detail: {
            screen: VBNB_Search_Detail,
            navigationOptions: {
                header: null
            }
        },
        VBNB_Chuyen: {
            screen: VBNB_Chuyen,
            navigationOptions: {
                header: null
            }
        },
        //END VBNB

        //START LCT
        LCT: {
            screen: LCT,
            navigationOptions: {
                header: null
            }
        },
        LCT_CoQuan_Duyet: {
            screen: LCT_CoQuan_Duyet,
            navigationOptions: {
                header: null
            }
        },
        //END LCT

        //START LICH HOP
        LH: {
            screen: MeetingList,
            navigationOptions: {
                header: null
            }
        },
        Meeting_Details: {
            screen: Meeting_Details,
            navigationOptions: {
                header: null
            }
        },
        Meeting_Search_Detail: {
            screen: Meeting_Search_Detail,
            navigationOptions: {
                header: null
            }
        },
        //END LICH HOP

        //START SMS
        SMS: {
            screen: SMS,
            navigationOptions: {
                header: null
            }
        },
        SMS_Gui: {
            screen: SMS_Gui,
            navigationOptions: {
                header: null
            }
        },
        //END SMS

        ModalWebView: {
            screen: ModalWebView,
            navigationOptions: {
                header: null
            }
        },

        //START Help
        IntroduceApp: {
            screen: IntroduceApp,
            navigationOptions: {
                header: null
            }
        },
        //END Help
        
        // Lực CMU
        VB7N: {
            screen: VBDi_DSDP7N,
            navigationOptions: {
                header: null
            },
        },
        VBDP1Y:{
            screen: VBDi_DSDP7N,
            navigationOptions: {
                header: null
            },
            
           
        },
        VBDi_Details_Cmu:{
            screen: VBDi_Details_Cmu,
            navigationOptions: {
                header: null
            }
        },
        VBDeTheoDoi:{
            screen: VBDe_TheoDoi,
            navigationOptions: {
                header: null
            }
        },
        VBDen_Details_Cmu:{
            screen:VBDen_Details_Cmu,
            navigationOptions: {
                header: null
            }
        },
    // Xin ý kiến thường trực - NAN - START
    XYKTT: {
      screen: XYKTT,
      navigationOptions: {
        header: null,
      },
    },
    XYKTT_Details: {
      screen: XYKTT_Details,
      navigationOptions: {
        header: null,
      },
    },
    XYKTT_Gui: {
      screen: XYKTT_Gui,
      navigationOptions: {
        header: null,
      },
    },
    // Xin ý kiến thường trực - NAN - END
    },
    {
        initialRouteName: "Home",
        headerMode: "none",
        navigationOptions: {
            gesturesEnabled: false,
        },
    }
)

MainStack.navigationOptions = ({ navigation }) => {
    let drawerLockMode = 'unlocked'
    if (navigation.state.index > 0) { // control by depth
        drawerLockMode = 'locked-closed'
    }
    return {
        drawerLockMode,
    }
}
