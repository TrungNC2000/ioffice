import { PermissionsAndroid } from "react-native"

export async function checkReadStorage(callback) {
    let result = await PermissionsAndroid.check("android.permission.READ_EXTERNAL_STORAGE")
    if (result) {
        callback()
    } else {
        try {
            const granted = await PermissionsAndroid.request("android.permission.READ_EXTERNAL_STORAGE")
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                console.log("Storage permission granted")
                callback()
            } else {
                console.log('Storage permission denied');
            }
        } catch (err) {
            console.warn(err);
        }
    }
}

export async function checkWriteStorage(callback) {
    let result = await PermissionsAndroid.check("android.permission.WRITE_EXTERNAL_STORAGE")
    if (result) {
        callback()
    } else {
        try {
            const granted = await PermissionsAndroid.request("android.permission.WRITE_EXTERNAL_STORAGE")
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                console.log("Storage permission granted")
                callback()
            } else {
                console.log('Storage permission denied');
            }
        } catch (err) {
            console.warn(err);
        }
    }
}
