import { Platform } from "react-native"
import RNFetchBlob from 'rn-fetch-blob'
import Share from "react-native-share"
import { getURLFile, getCurrentDayForFolder } from "../untils/TextUntils"
import { checkWriteStorage } from "../untils/PermissionAndroid"
import RNFS from "react-native-fs";

export const getMimeType = (type) => {
    switch (type) {
        case "doc":
            return "application/msword";
        case "docx":
            return "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
        case "ppt":
            return "application/vnd.ms-powerpoint";
        case "pptx":
            return "application/vnd.openxmlformats-officedocument.presentationml.presentation"
        case "xls":
            return "application/vnd.ms-excel";
        case "xlsx":
            return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
        case "pdf":
            return "application/pdf";
        case "png":
            return "image/png";
        case "bmp":
            return "application/x-MS-bmp";
        case "gif":
            return "image/gif";
        case "jpg":
            return "image/jpeg";
        case "jpeg":
            return "image/jpeg";
        case "avi":
            return "video/x-msvideo";
        case "aac":
            return "audio/x-aac";
        case "mp3":
            return "audio/mpeg";
        case "mp4":
            return "video/mp4";
        case "txt":
            return "text/plain";
        case "html":
            return "text/plain";
        default:
            return "text/plain";
    }
}

export const downloadFileToView = (filename, path) => {
    try {
        if (Platform.OS === "ios") {
            downloadFileToViewIOS(filename, path)
        } else {
            checkWriteStorage(() => downloadFileToViewAndroid(filename, path))
        }
    } catch (error) {
        console.log("downloadFileToView:" + error)
    }
}

export const downloadFileToViewAndroid = (filename, path) => {
    const android = RNFetchBlob.android
    let extFile = filename.split(".").pop()
    let mimeType = getMimeType(extFile)
    let pathDownload = RNFetchBlob.fs.dirs.DocumentDir + "/" + filename
    console.log("downloadFileToViewAndroid : " + pathDownload)
    RNFetchBlob.config({
        path: pathDownload,
        fileCache: true
    }).fetch('GET', getURLFile(path)).then((res) => {
        console.log("downloadFileToViewAndroid : " + res.path())
        console.log("downloadFileToViewAndroid : " + mimeType)
        android.actionViewIntent(res.path(), mimeType).then(() => {
            console.log('File opened')
        }).catch(e => {
            console.warn('Unable to open file', e);
        })
    })
}


export const downloadFileToViewIOS = (filename, path) => {
    const ios = RNFetchBlob.ios
    let pathDownload = RNFetchBlob.fs.dirs.DocumentDir + "/" + filename

    RNFetchBlob.config({
        path: pathDownload,
        indicator: true,
    }).fetch('GET', getURLFile(path)).then((res) => {
        ios.openDocument(res.path())
    })
}

export const downloadFile = (filename, path, isShare = false) => {
    try {
        if (Platform.OS === "ios") {
            downloadFileIOS(filename, path, isShare)
        } else {
            checkWriteStorage(() => downloadFileAndroid(filename, path, isShare))
        }
    } catch (error) {
        console.log("downloadFile:" + error)
    }
}

export const downloadFileAndroid = (filename, path, isShare) => {
    const android = RNFetchBlob.android
    let extFile = filename.split(".").pop()
    let mimeType = getMimeType(extFile)
    let pathDownload = RNFS.ExternalStorageDirectoryPath
    pathDownload += "/Download/iOffice41/" + getCurrentDayForFolder() + "/" + filename
    console.log("downloadFileToViewAndroid : " + pathDownload)
    if (!isShare) {
        RNFetchBlob.config({
            addAndroidDownloads: {
                useDownloadManager: true,
                title: filename,
                description: 'Tải tệp tin',
                mime: mimeType,
                mediaScannable: false,
                notification: true,
                path: pathDownload,
            },
        }).fetch('GET', getURLFile(path)).then((res) => {

        })
    } else {
        pathDownload = RNFetchBlob.fs.dirs.CacheDir + "/" + filename
        RNFetchBlob.config({
            path: pathDownload,
            mime: mimeType
        }).fetch('GET', getURLFile(path)).then((res) => {
            Share.open({
                url: "file://" + res.path(),
                title: "Chia sẻ tệp tin",
                message: filename
            })
        })
    }

}

export const downloadFileIOS = (filename, path, isShare) => {
    const ios = RNFetchBlob.ios
    let pathDownload = RNFetchBlob.fs.dirs.DocumentDir + "/" + filename
    RNFetchBlob.config({
        path: pathDownload,
        indicator: true,
    }).fetch('GET', getURLFile(path)).then((res) => {
        if (isShare) {
            Share.open({
                url: "file://" + res.path(),
                title: "Chia sẻ tệp tin",
                message: filename
            })
        } else {
            ios.openDocument(res.path())
        }
    })
}