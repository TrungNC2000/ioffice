import { AppConfig } from "../AppConfig"
import { Alert, AsyncStorage, Dimensions, Platform, BackHandler } from "react-native"
import { Toast } from "native-base"
import * as CryptoJS from "crypto-js"
import moment from "moment"
import API from "../networks"
import { StackActions, NavigationActions } from "react-navigation"
import firebase from 'react-native-firebase';

var entities = {
    'amp': '&',
    'apos': '\'',
    'lt': '<',
    'gt': '>',
    'quot': '"',
    'nbsp': '\xa0',
};
var entityPattern = /&([a-z]+);/ig;

export const HtmlDecode = (text) => {
    if (text != "" && text != null) {
        while (text.indexOf("<br>") != -1) {
            text = text.replace("<br>", "\n")
        }
        return text.replace(entityPattern, function (match, entity) {
            entity = entity.toLowerCase()
            if (entities.hasOwnProperty(entity)) {
                return entities[entity]
            }
            return match
        });
    }
    return text
};

export const HtmlEncode = (text) => {
    if (text != "" && text != null) {
        return text.replace(/\n/g, "<br>")
    }
    return text
};

export const RemoveHTMLTag = (text) => {
    try {
        const regex = /(<([^>]+)>)/ig
        text = HtmlDecode(text)
        let result = text.replace(regex, '')
        return result.trim()
    } catch {
        return text
    }
}

export const RemoveNoiDungTTDH = (text) => {
    if (text != "" && text != null) {
        text = RemoveHTMLTag(text)
        while (text.indexOf("--") != -1) {
            text = text.replace("--", "-")
        }
    }
    return text
}

export const ReplaceAll = (text, text1, text2) => {
    if (text != "" && text != null) {
        while (text.indexOf(text1) != -1) {
            text = text.replace(text1, text2)
        }
    }
    return text
}

export const GetNowTime = () => {
    return new Date().toISOString()
}

export const ConvertDateTime = (text) => {
    if (text != "" && text != null) {
        let result = text.replace("T", " ")
        let Date = result.split(" ")[0].split("-")
        let Time = result.split(" ")[1].split(":")
        result = Date[2] + "/" + Date[1] + "/" + Date[0] + " " + Time[0] + ":" + Time[1] + ":" + Time[2]
        return result.trim().replace(" ", "\r\n")
    }
    return text
}

export const ConvertDateTimeDetail = (text) => {
    if (text != "" && text != null) {
        let result = text.replace("T", " ")
        let Date = result.split(" ")[0].split("-")
        let Time = result.split(" ")[1].split(":")
        result = Date[2] + "/" + Date[1] + "/" + Date[0] + " " + Time[0] + ":" + Time[1]
        return result.trim()
    }
    return text
}

export const ConvertDate = (text) => {
    if (text != "" && text != null) {
        let result = text.replace("T", " ")
        let Date = result.split(" ")[0].split("-")
        result = Date[2] + "/" + Date[1] + "/" + Date[0]
        return result.trim()
    }
    return text
}

export const enCryptPathFile = (toEncrypt, key, useHashing) => {
    if (toEncrypt != "" && toEncrypt != null) {
        let keyArray = CryptoJS.enc.Utf8.parse(key)
        let toEncryptArray = CryptoJS.enc.Utf8.parse(toEncrypt)

        if (useHashing) {
            keyArray = CryptoJS.MD5(keyArray)
            keyArray.words.push(keyArray.words[0], keyArray.words[1])
        }

        var encrypted = CryptoJS.TripleDES.encrypt(toEncryptArray, keyArray, {
            mode: CryptoJS.mode.ECB,
            padding: CryptoJS.pad.Pkcs7
        });

        let result = encrypted.toString()
        return result
    }
    return toEncrypt
}

export const deCryptPathFile = (toDecrypt, key, useHashing) => {
    if (toDecrypt != "" && toDecrypt != null) {
        let keyArray = CryptoJS.enc.Utf8.parse(key)
        let toDecryptArray = CryptoJS.enc.Utf8.parse(toDecrypt)

        if (useHashing) {
            keyArray = CryptoJS.MD5(keyArray)
            keyArray.words.push(keyArray.words[0], keyArray.words[1])
        }

        var decrypted = CryptoJS.TripleDES.decrypt(toDecryptArray, keyArray, {
            mode: CryptoJS.mode.ECB,
            padding: CryptoJS.pad.Pkcs7
        });

        let result = decrypted.toString()
        return result
    }
    return toDecrypt
}

export const getFileName = (path) => {
    if (path != "" && path != null) {
        try {
            let filename = path.substr(path.lastIndexOf("__"))
            let filenameTemp = filename.toLowerCase()
            let indexam = filenameTemp.indexOf("am")
            indexam = indexam !== -1 ? indexam : 99999
            let indexpm = filenameTemp.indexOf("pm")
            indexpm = indexpm !== -1 ? indexpm : 99999
            let index = Math.min(indexam, indexpm)
            if (indexam === 99999 && indexpm === 99999) {
                index = -1
            }
            filename = filename.substr(index + 3)
            return filename
        } catch (error) {
            return ""
        }
    }
    return ""
}

export const getURLFile = (path) => {
    if (path != "" && path != null) {
        let filename = getFileName(path)
        let url;

        if (global.lay_domain_tu_file_url_vpc == 1) {
            url =  path.split('___')[1]?.split('__')[0]?.replace('https-', 'https://') + "/api/file-manage/read-base/" + filename + "?type=view&path="
            path = enCryptPathFile(path, AppConfig.keyViewFile, true)
            url = url + path
            url = encodeURI(url)
        } else {    
            path = enCryptPathFile(path, AppConfig.keyViewFile, true)
            url = global.AU_ROOT.replace("api", "file") + "/api/file-manage/read-base/" + filename + "?type=view&path=" + path
            url = encodeURI(url)
        }
        
        return url
    }
    return ""
}


export const getFileChiTietTTDH = (text) => {
    let result = []
    try {
        for (let item of text) {
            if (item !== "") {
                let fileName = getFileName(item)
                let extFile = fileName.split('.').pop().toLowerCase()
                let iconFile
                let urlViewFile = ""
                switch (extFile) {
                    case "pdf":
                        urlViewFile = getViewFileDomainByVerision() + getURLFile(item)
                        iconFile = AppConfig.iconFilePdf
                        break
                    case "doc":
                        urlViewFile = "https://docs.google.com/gview?embedded=true&url=" + getURLFile(item)
                        iconFile = AppConfig.iconFileWord
                        break
                    case "docx":
                        urlViewFile = "https://docs.google.com/gview?embedded=true&url=" + getURLFile(item)
                        iconFile = AppConfig.iconFileWord
                        break
                    case "xls":
                        iconFile = AppConfig.iconFileExcel
                        break
                    case "xlsx":
                        iconFile = AppConfig.iconFileExcel
                        break
                    case "png":
                        // if (global.AU_ROOT.indexOf("https") !== -1) {
                        //     urlViewFile = getURLFile(item)
                        // }
                        urlViewFile = getURLFile(item)
                        iconFile = AppConfig.iconFile
                        break
                    case "jpg":
                        // if (global.AU_ROOT.indexOf("https") !== -1) {
                        //     urlViewFile = getURLFile(item)
                        // }
                        urlViewFile = getURLFile(item)
                        iconFile = AppConfig.iconFile
                        break
                    case "jpeg":
                        // if (global.AU_ROOT.indexOf("https") !== -1) {
                        //     urlViewFile = getURLFile(item)
                        // }
                        urlViewFile = getURLFile(item)
                        iconFile = AppConfig.iconFile
                        break
                    case "rar":
                        iconFile = AppConfig.iconFileRar
                        break
                    case "zip":
                        iconFile = AppConfig.iconFileZip
                        break
                    default:
                        iconFile = AppConfig.iconFile
                }
                result.push(urlViewFile + "|" + fileName + "|" + extFile + "|" + iconFile + "|" + item)
            }
        }
    } catch (error) {
        console.log("getFile Error: " + error)
    }
    return result
}

export function getExtFromPath(url) {
    return url.split('__').pop().split('.').pop();
}

export function allowExt(url) {
    let ext = getExtFromUrl(url);
    if (!ext) {
        return false;
    }
    ext = ext.toLowerCase();
    if (['doc', 'docx', 'pdf', 'xls', 'xlsx', 'jpg', 'jpeg', 'gif', 'bmp', 'png', 'zip', 'rar'].indexOf(ext) >= 0) {
        return true;
    } else {
        Alert.alert('Cảnh báo', 'Chỉ có thể tải tệp với đuôi sau: doc, docx, pdf, xls, xlsx, jpg, jpeg, gif, bmp, png, zip, rar');
        return false;
    }
}

export function mergeArray(a, b, p) {
    return a.filter(aa => !b.find(bb => aa[p] === bb[p])).concat(b);
}

export function mergeArrayDSChuyen(a, b) {
    return b.filter(bb => !a.find(aa => bb.ma === aa.ma)).concat(a);
}

export const getUrlAvatar = (path) => {
    if (path != "" && path != null) {
        let filename = getFileName(path)
        path = enCryptPathFile(path, AppConfig.keyViewFile, true)
        let url = global.AU_ROOT.replace("api", "file") + "/api/file-manage/read-base/" + filename + "?type=view&path=" + path
        return url
    }
    return ""
}

export const converDataNotifyAfterLogin = (notificationData) => {
    let obj = new Object()
    let arrData = notificationData.item ? JSON.parse(notificationData.item) : []
    let itemData = null
    if (arrData.length > 0) {
        itemData = arrData.filter(el => el.ma_ctcb_kc == global.ma_ctcb_kc)[0]
    }

    if (itemData) {
        switch (notificationData.action_type) {
            case "VBDiChoDuyet":
                obj.screen = "VBDi_Details"
                obj.item = null
                if (itemData) {
                    let itemTemp = new Object()
                    let dataSource = []
                    itemTemp.isDuyet = true
                    dataSource.push({ "ma_xu_ly_di": itemData.ma_xu_ly_di, "ma_van_ban_di_kc": itemData.ma_van_ban_di_kc })
                    itemTemp.dataSource = dataSource
                    itemTemp.activeTab = 0
                    itemTemp.isFromNotify = true
                    obj.item = itemTemp
                }
                break
            case "VBDiUyQuyenDuyet":
                obj.screen = "VBDi_Details"
                obj.item = null
                if (itemData) {
                    let itemTemp = new Object()
                    let dataSource = []
                    itemTemp.isDuyet = true
                    dataSource.push({ "ma_xu_ly_di": itemData.ma_xu_ly_di, "ma_van_ban_di_kc": itemData.ma_van_ban_di_kc })
                    itemTemp.dataSource = dataSource
                    itemTemp.activeTab = 1
                    itemTemp.isFromNotify = true
                    obj.item = itemTemp
                }
                break
            case "VBDiChoXuLy":
                obj.screen = "VBDi_Details"
                obj.item = null
                if (itemData) {
                    let itemTemp = new Object()
                    let dataSource = []
                    itemTemp.isDuyet = false
                    dataSource.push({ "ma_xu_ly_di": itemData.ma_xu_ly_di, "ma_van_ban_di_kc": itemData.ma_van_ban_di_kc })
                    itemTemp.dataSource = dataSource
                    itemTemp.activeTab = 0
                    itemTemp.isFromNotify = true
                    obj.item = itemTemp
                }
                break
            case "VBDiDaPhatHanh":
                obj.screen = "VBDi_Details"
                obj.item = null
                if (itemData) {
                    let itemTemp = new Object()
                    let dataSource = []
                    itemTemp.isDuyet = false
                    dataSource.push({ "ma_xu_ly_di": itemData.ma_xu_ly_di, "ma_van_ban_di_kc": itemData.ma_van_ban_di_kc })
                    itemTemp.dataSource = dataSource
                    itemTemp.activeTab = 3
                    itemTemp.isFromNotify = true
                    obj.item = itemTemp
                }
                break
            case "VBDenChoDuyet":
                obj.screen = "VBDen_Details"
                obj.item = null
                if (itemData) {
                    let itemTemp = new Object()
                    let dataSource = []
                    itemTemp.isDuyet = true
                    dataSource.push({ "ma_xu_ly_den": itemData.ma_xu_ly_den, "ma_van_ban_den_kc": itemData.ma_van_ban_den_kc })
                    itemTemp.dataSource = dataSource
                    itemTemp.activeTab = 0
                    itemTemp.isFromNotify = true
                    obj.item = itemTemp
                }
                break
            case "VBDenUyQuyenDuyet":
                obj.screen = "VBDen_Details"
                obj.item = null
                if (itemData) {
                    let itemTemp = new Object()
                    let dataSource = []
                    itemTemp.isDuyet = true
                    dataSource.push({ "ma_xu_ly_den": itemData.ma_xu_ly_den, "ma_van_ban_den_kc": itemData.ma_van_ban_den_kc })
                    itemTemp.dataSource = dataSource
                    itemTemp.activeTab = 1
                    itemTemp.isFromNotify = true
                    obj.item = itemTemp
                }
                break
            case "VBDenChoXuLy":
                obj.screen = "VBDen_Details"
                obj.item = null
                if (itemData) {
                    let itemTemp = new Object()
                    let dataSource = []
                    itemTemp.isDuyet = false
                    dataSource.push({ "ma_xu_ly_den": itemData.ma_xu_ly_den, "ma_van_ban_den_kc": itemData.ma_van_ban_den_kc })
                    itemTemp.dataSource = dataSource
                    itemTemp.activeTab = 0
                    itemTemp.isFromNotify = true
                    obj.item = itemTemp
                }
                break
            case "VBDenDangXuLy":
                obj.screen = "VBDen_Details"
                obj.item = null
                if (itemData) {
                    let itemTemp = new Object()
                    let dataSource = []
                    itemTemp.isDuyet = false
                    dataSource.push({ "ma_xu_ly_den": itemData.ma_xu_ly_den, "ma_van_ban_den_kc": itemData.ma_van_ban_den_kc })
                    itemTemp.dataSource = dataSource
                    itemTemp.activeTab = 1
                    itemTemp.isFromNotify = true
                    obj.item = itemTemp
                }
                break
            case "TTDHDaNhan":
                obj.screen = "TTDH_Details"
                obj.item = null
                if (itemData) {
                    let itemTemp = new Object()
                    itemTemp.loaiGuiNhan = "Nhận"
                    itemTemp.ma_ttdh_gui_kc = itemData.ma_ttdh_gui_kc
                    itemTemp.isFromNotify = true
                    obj.item = itemTemp
                }
                break
            case "VBNBDaNhan":
                obj.screen = "VBNB_Details"
                obj.item = null
                if (itemData) {
                    let itemTemp = new Object()
                    itemTemp.activeTab = 0
                    itemTemp.ma_van_ban_noi_bo_gui_kc = itemData.ma_van_ban_noi_bo_gui_kc
                    itemTemp.isFromNotify = true
                    obj.item = itemTemp
                }
                break
            default:
                obj.screen = "Home"
                obj.item = null
        }
    } else {
        switch (notificationData.action_type) {
            case "choduyetden":
                obj.screen = "VBDenDuyet"
                break
            case "choduyetdi":
                obj.screen = "VBDiDuyet"
                break
            case "vanbanden":
                obj.screen = "VBDenXuLy"
                break
            case "vanbandi":
                obj.screen = "VBDiXuLy"
                break
            case "xulyvanbandi":
                obj.screen = "VBDiXuLy"
                break
            case "thongdiep":
                obj.screen = "TTDH"
                break
            case "thongdiep":
                obj.screen = "TTDH"
                break
            case "NotifyLCT":
                AsyncStorage.removeItem(notificationData.id)
                obj.screen = "LCT"
                break
            case "NotifyHXLVBDEN":
                AsyncStorage.removeItem(notificationData.id)
                obj.screen = "VBDenXuLy"
                break
            default:
                obj.screen = "Home"
        }
        obj.item = null
    }
    return obj
}

export const getBytes = function (stringValue) {
    var bytes = [];
    for (var i = 0; i < stringValue.length; ++i) {
        bytes.push(stringValue.charCodeAt(i));
    }
    return bytes;
};

export const getString = function (utftext) {
    var result = "";
    for (var i = 0; i < utftext.length; i++) {
        result += String.fromCharCode(parseInt(utftext[i], 10));
    }
    return result;
};

export const resetStack = function (screenName) {
    const resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: screenName })]
    })
    return resetAction
};

export const listToTree = function (data, options) {
    options = options || {};
    var ID_KEY = options.idKey || 'ma';
    var PARENT_KEY = options.parentKey || 'cha';
    var CHILDREN_KEY = options.childrenKey || 'childs';

    var tree = [],
        childrenOf = {};
    var item, id, parentId;

    for (var i = 0, length = data.length; i < length; i++) {
        item = data[i];
        id = item[ID_KEY];
        parentId = item[PARENT_KEY] || 0;
        // every item may have children
        childrenOf[id] = childrenOf[id] || [];
        // init its children
        item[CHILDREN_KEY] = childrenOf[id];
        let isRoot = data.filter(el => el[ID_KEY] === parentId).length
        if (isRoot > 0) {
            // init its parent's children object
            childrenOf[parentId] = childrenOf[parentId] || [];
            // push it into its parent's children object
            childrenOf[parentId].push(item);
        } else {
            tree.push(item);
        }
    };

    return tree;
}

export const checkVersionWeb = function (version) {
    try {
        if (parseInt(global.web_version) >= version) {
            return true
        }
        return false
    } catch (error) {
        return false
    }
};


export const formatDate = function (date) {
    var day = date.getDate();
    var monthIndex = date.getMonth() + 1;
    var year = date.getFullYear();
    if (day < 10) {
        day = '0' + day
    }
    if (monthIndex < 10) {
        monthIndex = '0' + monthIndex
    }
    return day + '/' + monthIndex + '/' + year;
}

export const groupBy = function (arr, property) {
    return arr.reduce(function (memo, x) {
        if (!memo[x[property]]) { memo[x[property]] = []; }
        memo[x[property]].push(x);
        return memo;
    }, {});
}

export const get_date_from_week = function (week, year) {
    if (global.lct_tuan_trong_nam_kieu_cu == 1) {
        //Kiểu cũ
        var d = new Date("Jan 01, " + year + " 01:00:00");
        var dayMs = (24 * 60 * 60 * 1000);
        var offSetTimeStart = dayMs * (d.getDay() - 1);
        var w = d.getTime() + 604800000 * (week - 1) - offSetTimeStart; //reducing the offset here
        var n1 = new Date(w);
        var n2 = new Date(w + 518400000);
        return formatDate(n1) + " - " + formatDate(n2)
    } else {
        //Kiểu mới
        let startDate = new Date()
        if (year % 4 === 0) {
            startDate = new Date(year, 0, (1 + (week - 1) * 7)); // Old method
        } else {
            startDate = new Date(year, 0, (1 + (week) * 7)); // New method
        }
        startDate.setDate(startDate.getDate() + (1 - startDate.getDay())); // 0 - Sunday, 1 - Monday etc
        var rangeIsFrom = formatDate(startDate)
        startDate.setDate(startDate.getDate() + 6);
        var rangeIsTo = formatDate(startDate)
        return rangeIsFrom + " - " + rangeIsTo;
    }
};

export const get_arrdate_from_week = function (week, year) {
    if (global.lct_tuan_trong_nam_kieu_cu == 1) {
        //Kiểu cũ
        let arrResult = []
        var d = new Date("Jan 01, " + year + " 01:00:00");
        var dayMs = (24 * 60 * 60 * 1000);
        var offSetTimeStart = dayMs * (d.getDay() - 1);
        var w = d.getTime() + 604800000 * (week - 1) - offSetTimeStart; //reducing the offset here
        var n1 = new Date(w);
        var n2 = new Date(w + 518400000);
        arrResult.push(n1)
        arrResult.push(n1)
        return formatDate(n1) + " - " + formatDate(n2)
    } else {
        //Kiểu mới
        let arrResult = []
        let startDate = new Date()
        if (year % 4 === 0) {
            startDate = new Date(year, 0, (1 + (week - 1) * 7)); // Old method
        } else {
            startDate = new Date(year, 0, (1 + (week) * 7)); // New method
        }
        startDate.setDate(startDate.getDate() + (1 - startDate.getDay())); // 0 - Sunday, 1 - Monday etc
        arrResult.push(startDate)
        startDate.setDate(startDate.getDate() + 6);
        arrResult.push(startDate)
        return arrResult
    }
}

export const getWeekNumber = function (d) {
    let arrResult = []
    d = new Date(+d) // Copy date so don't modify original.
    d.setHours(0, 0, 0, 0) // Reset hours.
    d.setDate(d.getDate() + 4 - (d.getDay() || 7)) // Set to nearest Thursday: current date + 4 - current day number and make Sunday's day number 7
    // var yearStart = new Date(d.getFullYear(), 0, 1) // Get first day of year
    var yearStart = new Date("Jan " + global.lct_ngay_bat_dau_lam_viec + " , " + d.getFullYear() + " 01:00:00");
    var weekNo = Math.ceil((((d - yearStart) / 86400000) + 1) / 7) // Calculate full weeks to nearest Thursday
    var yearNo = d.getUTCFullYear()
    // if (global.lct_tuan_trong_nam_kieu_cu == 1) {
    //     weekNo += 1
    //     if (weekNo == 53) {
    //         weekNo = 1
    //         yearNo += 1
    //     }
    // }
    arrResult.push(weekNo)
    arrResult.push(yearNo)
    // Return array of year and week number
    return arrResult
}

export const setFontSize = (type, dispatch) => {
    if (type === "1") {
        dispatch({ type: "SMALL_FONT" })
    } else if (type === "3") {
        dispatch({ type: "BIG_FONT" })
    } else {
        dispatch({ type: "NORMAN_FONT" })
    }
}

export const setUserSettings = async function (navigation, dispatch) {
    await AsyncStorage.getItem("US_Mobile_Pki_Width_Rec", (error, result) => {
        if (result === null) {
            result = AppConfig.Default_US_Mobile_Pki_Width_Rec
        }
        global.US_Mobile_Pki_Width_Rec = result
    })
    await AsyncStorage.getItem("US_Mobile_Pki_Height_Rec", (error, result) => {
        if (result === null) {
            result = AppConfig.Default_US_Mobile_Pki_Height_Rec
        }
        global.US_Mobile_Pki_Height_Rec = result
    })

    if (checkVersionWeb(21)) {
        API.Login.AU_GTSM().then(async (listTS) => {
            if (listTS.length > 0) {
                for (let item of listTS) {
                    global[item.ma_tham_so] = item.gia_tri_tham_so
                    if (item.ma_tham_so === "US_Type_Font_Size") {
                        setFontSize(item.gia_tri_tham_so, dispatch)
                    }
                }
                API.Login.AU_GMLDN()
                if (global.gotoNotify !== "" && global.gotoNotify !== undefined) {
                    global.isLogin = true
                    const notification = JSON.parse(global.gotoNotify)
                    let obj = converDataNotifyAfterLogin(notification)
                    navigation.navigate(obj.screen, obj.item)
                } else {
                    global.isLogin = true
                    navigation.dispatch(resetStack(global.US_First_Load_Page))
                }
            } else {
                await AsyncStorage.getItem("US_Xem_Pdf_iOS", (error, result) => {
                    if (result === null) {
                        result = AppConfig.Default_US_Xem_Pdf_iOS
                    }
                    global.US_Xem_Pdf_iOS = result
                })
                await AsyncStorage.getItem("US_Type_Font_Size", (error, result) => {
                    if (result === null) {
                        result = AppConfig.Default_US_Type_Font_Size
                    }
                    global.US_Type_Font_Size = result
                    setFontSize(result, dispatch)
                })
                await AsyncStorage.getItem("US_Mobile_Pki_Mode", (error, result) => {
                    if (result === null) {
                        result = AppConfig.Default_US_Mobile_Pki_Mode
                    }
                    global.US_Mobile_Pki_Mode = result
                })
                await AsyncStorage.getItem("US_Mobile_Pki_Side", (error, result) => {
                    if (result === null) {
                        result = AppConfig.Default_US_Mobile_Pki_Side
                    }
                    global.US_Mobile_Pki_Side = result
                })
                await AsyncStorage.getItem("US_VBDEN_Mot_Can_Bo_XLC", (error, result) => {
                    if (result === null) {
                        result = AppConfig.Default_US_VBDEN_Mot_Can_Bo_XLC
                    }
                    global.US_VBDEN_Mot_Can_Bo_XLC = result
                })
                await AsyncStorage.getItem("US_VBDEN_Tree_Default_Check", (error, result) => {
                    if (result === null) {
                        result = AppConfig.Default_US_VBDEN_Tree_Default_Check
                    }
                    global.US_VBDEN_Tree_Default_Check = result
                })
                await AsyncStorage.getItem("US_FILE_List_Ngang_Doc", (error, result) => {
                    if (result === null) {
                        result = AppConfig.Default_US_FILE_List_Ngang_Doc
                    }
                    global.US_FILE_List_Ngang_Doc = result
                })
                await AsyncStorage.getItem("US_Show_Alert_On_Success", (error, result) => {
                    if (result === null) {
                        result = AppConfig.Default_US_Show_Alert_On_Success
                    }
                    global.US_Show_Alert_On_Success = result
                })
                await AsyncStorage.getItem("US_Time_Local_Notify", (error, result) => {
                    if (result === null) {
                        result = AppConfig.Default_US_Time_Local_Notify
                    }
                    global.US_Time_Local_Notify = result
                })
                await AsyncStorage.getItem("US_First_Load_Page", (error, result) => {
                    if (result === null) {
                        result = AppConfig.Default_US_First_Load_Page
                    }
                    global.US_First_Load_Page = result
                    API.Login.AU_GMLDN()
                    if (global.gotoNotify !== "" && global.gotoNotify !== undefined) {
                        global.isLogin = true
                        const notification = JSON.parse(global.gotoNotify)
                        let obj = converDataNotifyAfterLogin(notification)
                        navigation.navigate(obj.screen, obj.item)
                    } else {
                        global.isLogin = true
                        navigation.dispatch(resetStack(global.US_First_Load_Page))
                    }
                })
            }
        })
    } else {
        await AsyncStorage.getItem("US_Xem_Pdf_iOS", (error, result) => {
            if (result === null) {
                result = AppConfig.Default_US_Xem_Pdf_iOS
            }
            global.US_Xem_Pdf_iOS = result
        })
        await AsyncStorage.getItem("US_Type_Font_Size", (error, result) => {
            if (result === null) {
                result = AppConfig.Default_US_Type_Font_Size
            }
            global.US_Type_Font_Size = result
            setFontSize(result, dispatch)
        })
        await AsyncStorage.getItem("US_Mobile_Pki_Mode", (error, result) => {
            if (result === null) {
                result = AppConfig.Default_US_Mobile_Pki_Mode
            }
            global.US_Mobile_Pki_Mode = result
        })
        await AsyncStorage.getItem("US_Mobile_Pki_Side", (error, result) => {
            if (result === null) {
                result = AppConfig.Default_US_Mobile_Pki_Side
            }
            global.US_Mobile_Pki_Side = result
        })
        await AsyncStorage.getItem("US_VBDEN_Mot_Can_Bo_XLC", (error, result) => {
            if (result === null) {
                result = AppConfig.Default_US_VBDEN_Mot_Can_Bo_XLC
            }
            global.US_VBDEN_Mot_Can_Bo_XLC = result
        })
        await AsyncStorage.getItem("US_VBDEN_Tree_Default_Check", (error, result) => {
            if (result === null) {
                result = AppConfig.Default_US_VBDEN_Tree_Default_Check
            }
            global.US_VBDEN_Tree_Default_Check = result
        })
        await AsyncStorage.getItem("US_FILE_List_Ngang_Doc", (error, result) => {
            if (result === null) {
                result = AppConfig.Default_US_FILE_List_Ngang_Doc
            }
            global.US_FILE_List_Ngang_Doc = result
        })
        await AsyncStorage.getItem("US_Show_Alert_On_Success", (error, result) => {
            if (result === null) {
                result = AppConfig.Default_US_Show_Alert_On_Success
            }
            global.US_Show_Alert_On_Success = result
        })
        await AsyncStorage.getItem("US_Time_Local_Notify", (error, result) => {
            if (result === null) {
                result = AppConfig.Default_US_Time_Local_Notify
            }
            global.US_Time_Local_Notify = result
        })
        await AsyncStorage.getItem("US_First_Load_Page", (error, result) => {
            if (result === null) {
                result = AppConfig.Default_US_First_Load_Page
            }
            global.US_First_Load_Page = result
            API.Login.AU_GMLDN()
            if (global.gotoNotify !== "" && global.gotoNotify !== undefined) {
                global.isLogin = true
                const notification = JSON.parse(global.gotoNotify)
                let obj = converDataNotifyAfterLogin(notification)
                navigation.navigate(obj.screen, obj.item)
            } else {
                global.isLogin = true
                navigation.dispatch(resetStack(global.US_First_Load_Page))
            }
        })
    }
}

export const ToastSuccess = function (body = "Thực hiện thành công", callback = () => { }, duration = 2000) {
    if (global.US_Show_Alert_On_Success && global.US_Show_Alert_On_Success === "2") {
        callback()
    } else {
        return Toast.show({
            text: body,
            type: "success",
            buttonText: "Đóng",
            position: "top",
            duration: duration,
            onClose: callback
        })
    }
}

export const setScheduleNotification = async function (id = "1", title = "Thông báo", body = "", date = "", action_type = "NotifyLCT") {
    let today = new Date()
    let fireDate = moment(date, "DD/MM/YYYY hh:mm").subtract(parseInt(global.US_Time_Local_Notify), 'hours')
    fireDate = new Date(fireDate)
    if (fireDate.getTime() > today.getTime()) {
        let flag = await AsyncStorage.getItem(id)
        if (flag === null) {
            if (body.length > 500) {
                body = body.substring(0, 499)
            }

            const localNotification = new firebase.notifications.Notification()
                .setNotificationId(action_type)
                .setSound("default")
                .setTitle(title)
                .setBody(body)
                .setData({ "id": id, "title": title, "body": body, "action_type": action_type })
                .android.setChannelId("VNPT-iOffice41")
                .android.setPriority(firebase.notifications.Android.Priority.High);

            firebase.notifications().scheduleNotification(localNotification, {
                fireDate: fireDate.getTime()
            })
            AsyncStorage.setItem(id, id, () => {
                Toast.show({ text: "Cài đặt thông báo thành công!", type: "success", buttonText: "Đóng", position: "top", duration: 2000 })
            })
        } else {
            Toast.show({ text: "Đã cài đặt thông báo trước đó!", type: "success", buttonText: "Đóng", position: "top", duration: 2000 })
        }
    } else {
        Toast.show({ text: "Đã quá thời gian nhận thông báo. Vui lòng cài đặt lại!", type: "warning", buttonText: "Đóng", position: "top", duration: 2000 })
        AsyncStorage.removeItem(id)
    }
}

export const ReturnNumber = function (text) {
    if (text != "" && text != null) {
        return text.replace(/\D/g, "")
    }
    return text
}

export const getNVOld = function (state_name, tab, cb_dsnvcb) {
    let temp = 0
    if (tab !== "") {
        for (let item of cb_dsnvcb) {
            if (item.state_name === state_name && item.tab === tab) {
                temp += item.tong
            }
        }
    } else {
        for (let item of cb_dsnvcb) {
            if (item.state_name === state_name) {
                temp += item.tong
            }
        }
    }
    return temp
}

export const getNVNew = function (parent_key, url, cb_dsnvcb) {

    for (let itemParent of cb_dsnvcb) {
        if (itemParent.parent_key && itemParent.parent_key.includes(parent_key)) {
            let arrChilds = itemParent.childs
            for (let itemChilds of arrChilds) {
                if (itemChilds.url === url) {
                    if (itemChilds.tong < 0) {
                        return 0
                    }
                    return itemChilds.tong
                }
            }
        }
    }
    return 0
}

export const isIphoneX = function () {
    const dimen = Dimensions.get('window');
    return (
        Platform.OS === 'ios' &&
        !Platform.isPad &&
        !Platform.isTVOS &&
        ((dimen.height === 812 || dimen.width === 812) || (dimen.height === 896 || dimen.width === 896))
    );
}

export const getDefaultBackHandler = function (navigation) {
    return BackHandler.addEventListener("hardwareBackPress", () => {
        navigation.pop()
        return false
    })
}

export const getCurrentDay = function () {
    let current_date = new Date()
    let day = current_date.getDate().toString()
    let month = (current_date.getMonth() + 1).toString()
    let year = current_date.getFullYear().toString()
    if (day.length === 1) {
        day = "0" + day
    }
    if (month.length === 1) {
        month = "0" + month
    }
    return day + "/" + month + "/" + year
}

export const getCurrentDayForFolder = function () {
    let current_date = new Date()
    let day = current_date.getDate().toString()
    let month = (current_date.getMonth() + 1).toString()
    let year = current_date.getFullYear().toString()
    if (day.length === 1) {
        day = "0" + day
    }
    if (month.length === 1) {
        month = "0" + month
    }
    return day + "-" + month + "-" + year
}

//#region Prototype
Array.prototype.uniqueVBDI = function () {
    var a = this.concat();
    for (var i = 0; i < a.length; ++i) {
        for (var j = i + 1; j < a.length; ++j) {
            if (a[i].ma_van_ban_kc === a[j].ma_van_ban_kc)
                a.splice(j--, 1);
        }
    }

    return a;
};
//#endregion Prototype

export const getSignPDFDomainByVerision = function () {
    try {
        if(checkVersionWeb(64)){
        return global.AU_ROOT + "/mobile-view/pdf-sign-viewer/index.html?file=";
        }
        else{
        return "https://tiengiangdev-api.vnptioffice.vn/mobile-view/pdf-sign-viewer/index.html?file=";
        } 
    } catch (error) {
        return "https://tiengiangdev-api.vnptioffice.vn/mobile-view/pdf-sign-viewer/index.html?file=";
    }
};

export const getViewFileDomainByVerision = function () {
    try {
        if(checkVersionWeb(64)){
        return global.AU_ROOT + "/mobile-view/pdfviewer/Web/Viewer.html?file=";
        }
        else{
        return "https://tiengiangdev-api.vnptioffice.vn/mobile-view/pdfviewer/Web/Viewer.html?file=";
        } 
    } catch (error) {
        return "https://tiengiangdev-api.vnptioffice.vn/mobile-view/pdfviewer/Web/Viewer.html?file=";
    }
};