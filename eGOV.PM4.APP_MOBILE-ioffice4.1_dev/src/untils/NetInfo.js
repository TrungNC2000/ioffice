import { AsyncStorage } from "react-native"
import { Toast } from "native-base"
import { GetNowTime } from "./TextUntils"
export const CheckSession = async (navigation) => {
    CheckSession1().then((result) => {
        if (result) {
            Toast.show({
                text: "Đã hết phiên làm việc, bạn sẽ tự động đăng nhập lại để tiếp tục!", type: "warning", position: "top", duration: 4000, onClose: (reason) => {
                    navigation.navigate("Login", { loaiDangNhap: "outSession" })
                }
            })
        }
    })
};

export const CheckSession1 = async () => {
    let SessionTime = await AsyncStorage.getItem("SessionTime")
    SessionTime = new Date(SessionTime)
    let NowTime = new Date(GetNowTime())
    //28800000 = 8h x 60p x 60s + "000"
    if ((NowTime - SessionTime) > 28800000) {
        return true
    } else {
        return false
    }
};