import React, { Component } from "react"
import { View, StyleSheet, Platform, Alert, Text as RNText } from "react-native"
import { Root, Text, StyleProvider, Toast } from "native-base"
import { Provider } from "react-redux"
import { NavigationActions, SafeAreaView,NavigationContainer} from "react-navigation"
import myStore from "./stores/Store"
import { AppConfig } from "./AppConfig"
import TabNavigator from "./navigations/index"
import { MenuProvider } from 'react-native-popup-menu';
import getTheme from "./theme/components"
import material from "./theme/variables/material"
import MyStatusBar from "./components/StatusBar"
import { converDataNotifyAfterLogin } from "./untils/TextUntils"
import API from "./networks"
// Optional: Flow type
import firebase from 'react-native-firebase';

Text.defaultProps = Text.defaultProps || {};
Text.defaultProps.allowFontScaling = false;
Text.defaultProps.uppercase = false
RNText.defaultProps = RNText.defaultProps || {};
RNText.defaultProps.allowFontScaling = false;
RNText.defaultProps.uppercase = false

class Setup extends Component {
	componentDidMount = () => {
		
		global.isLogin = false
		firebase.notifications().getInitialNotification().then((notificationOpen) => {
			console.log("getInitialNotification")
			if (notificationOpen) {
				let notification = notificationOpen.notification;
				global.gotoNotify = JSON.stringify(notification.data)
				firebase.notifications().removeDeliveredNotification(notification.notificationId)
			}
		});

		//Add permissions
		firebase.messaging().hasPermission().then(enabled => {
			if (enabled) {
				console.log("hasPermission")
				this.fcmGetToken()
			} else {
				console.log("no hasPermission")
				this.fcmRequestPermission()
			}
		});

		// DEFAULT CHANNEL
		const channel = new firebase.notifications.Android.Channel('VNPT iOffice41', 'VNPT iOffice 4.1', firebase.notifications.Android.Importance.Max);
		channel.setDescription('My default channel');
		// Create the channel
		firebase.notifications().android.createChannel(channel);


		// data-only messages from FCM
		this.messageListener = firebase.messaging().onMessage((message) => {
			console.log('onMessage', message);
		});


		// LOCAL NOTIFICATION: FOREGROUND
		this.notificationDisplayedListener = firebase.notifications().onNotificationDisplayed((notification) => {
			console.log('onNotificationDisplayed', notification);
		});

		this.notificationListener = firebase.notifications().onNotification((notification) => {
			console.log('onNotification', notification);
			const localNotification = new firebase.notifications.Notification()
				.setNotificationId(notification.notificationId)
				.setTitle(notification.title)
				.setSubtitle(notification.subtitle)
				.setBody(notification.data.body)
				.setData(notification.data)
				.ios.setBadge(notification.ios.badge)
				.android.setChannelId(channel.channelId)
				.android.setPriority(firebase.notifications.Android.Priority.High);
			firebase.notifications().displayNotification(localNotification);
		});

		// APP FOREGROUND / BACKGROUND
		this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
			if (notificationOpen) {
				let notification = notificationOpen.notification;
				if (global.isLogin) {
					let obj = new Object()
					obj = converDataNotifyAfterLogin(notification.data)
					Alert.alert(
						"Xác nhận",
						"Chuyển màn hình?",
						[
							{
								text: "Từ chối", style: "cancel", onPress: () => {
									firebase.notifications().removeDeliveredNotification(notification.notificationId)
								}
							},
							{
								text: "Đồng ý", onPress: () => {
									firebase.notifications().removeDeliveredNotification(notification.notificationId)
									const navigationAction = NavigationActions.navigate({
										routeName: obj.screen,
										params: obj.item
									})
									this.navigator && this.navigator.dispatch(navigationAction)
								}
							}
						],
						{ cancelable: false }
					)
				} else {
					firebase.notifications().removeDeliveredNotification(notification.notificationId)
					Toast.show({ text: "Đăng nhập để xem thông báo!", type: "warning", buttonText: "Đóng", position: "top", duration: 2000 })
					global.gotoNotify = JSON.stringify(notification.data)
				}
			}
		});

		this.onTokenRefreshListener = firebase.messaging().onTokenRefresh(fcmToken => {
			global.fcmToken = fcmToken
		});

	}

	fcmRequestPermission = () => {
		firebase.messaging().requestPermission()
			.then(() => {
				this.fcmGetToken()
			})
			.catch(error => {
				console.log("User has rejected permissions  ")
			});
	}

	fcmGetToken = () => {
		firebase.messaging().getToken()
			.then(fcmToken => {
				console.log(fcmToken)
				if (fcmToken) {
					global.fcmToken = fcmToken
				} else {
					this.onTokenRefreshListener();
				}
			});
	}

	componentWillUnmount() {
		this.messageListener();
		this.notificationDisplayedListener();
		this.notificationListener();
		this.notificationOpenedListener();
		this.onTokenRefreshListener();
	}
	render() {
		return (
			<Provider store={myStore}>
				<StyleProvider style={getTheme(material)}>
					<MenuProvider skipInstanceCheck={true}>
						<SafeAreaView style={styles.container}>
							<MyStatusBar backgroundColor={Platform.OS === 'ios' ? "#000000" : AppConfig.statusBarColor} barStyle="light-content" />
							<Root>
								<TabNavigator ref={nav => { this.navigator = nav}}/>
							</Root>
						</SafeAreaView>
					</MenuProvider>
				</StyleProvider>
			</Provider >
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#000000",
		marginTop: 0,
	}
});

export default Setup