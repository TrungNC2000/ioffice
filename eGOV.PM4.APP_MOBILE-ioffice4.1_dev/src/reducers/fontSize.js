import DeviceInfo from 'react-native-device-info'
import { Platform } from "react-native"

const defaultState = {
    FontSizeSmall: DeviceInfo.isTabletSync() ? Platform.OS === "ios" ? 16 : 15 : 14,
    FontSizeNorman: DeviceInfo.isTabletSync() ? Platform.OS === "ios" ? 17 : 16 : 16,
    FontSizeLarge: DeviceInfo.isTabletSync() ? Platform.OS === "ios" ? 20 : 18 : 18,
    FontSizeXLarge: DeviceInfo.isTabletSync() ? Platform.OS === "ios" ? 21 : 20 : 20,
    FontSizeXXLarge: DeviceInfo.isTabletSync() ? Platform.OS === "ios" ? 23 : 22 : 22,
    FontSizeXXXLarge: DeviceInfo.isTabletSync() ? Platform.OS === "ios" ? 25 : 24 : 24

    // FontSizeSmall: 14,
    // FontSizeNorman: 16,
    // FontSizeLarge: 18,
    // FontSizeXLarge: 20,
    // FontSizeXXLarge: 22,
    // FontSizeXXXLarge: 24
}

export default reducer = (state = defaultState, action) => {
    switch (action.type) {
        case "BIG_FONT":
            return {
                FontSizeSmall: defaultState.FontSizeSmall + 1.5,
                FontSizeNorman: defaultState.FontSizeNorman + 1.5,
                FontSizeLarge: defaultState.FontSizeLarge + 1.5,
                FontSizeXLarge: defaultState.FontSizeXLarge + 1.5,
                FontSizeXXLarge: defaultState.FontSizeXXLarge + 1.5,
                FontSizeXXXLarge: defaultState.FontSizeXXXLarge + 1.5,
            }
        case 'SMALL_FONT':
            return {
                FontSizeSmall: defaultState.FontSizeSmall - 1.5,
                FontSizeNorman: defaultState.FontSizeNorman - 1.5,
                FontSizeLarge: defaultState.FontSizeLarge - 1.5,
                FontSizeXLarge: defaultState.FontSizeXLarge - 1.5,
                FontSizeXXLarge: defaultState.FontSizeXXLarge - 1.5,
                FontSizeXXXLarge: defaultState.FontSizeXXXLarge - 1.5,
            }
        case 'NORMAN_FONT':
            return {
                FontSizeSmall: defaultState.FontSizeSmall,
                FontSizeNorman: defaultState.FontSizeNorman,
                FontSizeLarge: defaultState.FontSizeLarge,
                FontSizeXLarge: defaultState.FontSizeXLarge,
                FontSizeXXLarge: defaultState.FontSizeXXLarge,
                FontSizeXXXLarge: defaultState.FontSizeXXXLarge,
            }
        default:
            return state;
    }
};