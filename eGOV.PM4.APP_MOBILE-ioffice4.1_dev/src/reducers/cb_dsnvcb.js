const defaultState = {
    data: [],
    status: false
}

export default cb_dsnvcb = (state = defaultState, action) => {
    if (action.type === "updateNhacViec") {
        return { ...state, data: action.data, status: true }
    };
    return state;
};