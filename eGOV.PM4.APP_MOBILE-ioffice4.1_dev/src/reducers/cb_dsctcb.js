const defaultState = {
    data: {},
    dataAll: [],
    status: false
}

export default cb_dsctcb = (state = defaultState, action) => {
    if (action.type === "updateDSCTCB") {
        return { ...state, data: action.data, status: true }
    };
    if (action.type === "updateDSCTCB_ALL") {
        return { ...state, dataAll: action.dataAll, status: true }
    };
    return state;
}