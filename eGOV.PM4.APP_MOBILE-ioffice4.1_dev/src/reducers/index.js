import { combineReducers } from "redux"
import cb_dsctcb from "./cb_dsctcb"
import fontSize from "./fontSize"
import cb_dsnvcb from "./cb_dsnvcb"
import cb_ds_don_vi_app from "./cb_ds_don_vi_app"

const reducer = combineReducers({
    cb_dsctcb: cb_dsctcb,
    fontSize: fontSize,
    cb_dsnvcb: cb_dsnvcb,
    cb_ds_don_vi_app: cb_ds_don_vi_app
})

export default reducer