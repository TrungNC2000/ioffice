const defaultState = {
    data: [],
    status: false
}

export default cb_ds_don_vi_app = (state = defaultState, action) => {
    if (action.type === "updateDSDVA") {
        return { ...state, data: action.data, status: true }
    };
    return state;
};