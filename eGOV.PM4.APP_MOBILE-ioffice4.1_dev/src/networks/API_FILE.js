import { URL_CONFIG } from "./Config"
import { Platform } from "react-native"
import DeviceInfo from 'react-native-device-info'
import { Toast } from "native-base"
import axios from "axios"
import { ReplaceAll } from "../untils/TextUntils"
import { AppConfig } from "../AppConfig"
import API_Login from "./API_Login"

function showWarning(error) {
    Toast.show({ text: "Kết nối bị gián đoạn!", type: "warning", buttonText: "Đóng", position: "top", duration: 2000 })
    if (error) {
        SendLog(error)
    }
};

async function SendLog(message) {
    try {
        let obj = new Object
        let date = new Date()
        obj.Model = DeviceInfo.getModelSync()
        obj.OS = Platform.OS.toString()
        obj.SystemVersion = DeviceInfo.getSystemVersionSync()
        obj.AppVersion = DeviceInfo.getVersionSync()
        obj.CurrentTime = date.toLocaleDateString() + " " + date.toLocaleTimeString()
        obj.Module = "API_File"
        obj.Domain = global.AU_ROOT ? global.AU_ROOT : ""
        delete message.headers
        obj.Message = message
        let text = ReplaceAll(JSON.stringify(obj), String.fromCharCode(34), "'")
        let url = AppConfig.AppDefaultLogURLSendText + text
        await axios({
            method: "POST",
            url: url,
            timeout: AppConfig.TIME_OUT_SHORT
        })
    } catch (error) {
        console.log("Send log error: " + error)
    }
}

function setHeaderToken() {
    return { "Authorization": "Bearer " + global.access_token }
    
};
function setHeaderTokenUploadFile() {
    return {"Content-Type": "multipart/form-data"}   
};

async function AU_FILE_CDTTWP(path) {
    let result = false
    var form = new Object()
    form.path = path
    let url = global.AU_ROOT.replace("api", "file") + URL_CONFIG.AU_FILE_CDTTWP

    await axios({
        method: "GET",
        url: url,
        headers: setHeaderToken(),
        params: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_FILE_CDTTWP) {
            console.log("CONSOLE_AU_FILE_CDTTWP: " + JSON.stringify(response))
        }
        if (response.data.split("|")[0] === "1") {
            result = true
        } else {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_FILE_CDTTWP) {
            console.log("CONSOLE_AU_FILE_CDTTWP ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_FILE_CDTTWP(path).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

// UPLOAD FILE BASE64
async function AU_UPLOADFILE_CNTTTLTB64(ma_ctcb, chuc_nang, ten_file, base64, ma_van_ban_di) {
    let result = []
    var form = new FormData();
    form.append("ma_ctcb", ma_ctcb);
    form.append("chuc_nang", chuc_nang);
    form.append("ten_file", ten_file);
    form.append("base64", base64);
    form.append("ma_van_ban_di", ma_van_ban_di);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_UPLOADFILE_CNTTTLTB64,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_UPLOADFILE_CNTTTLTB64) {
            console.log("CONSOLE_AU_UPLOADFILE_CNTTTLTB64: " + JSON.stringify(response))
        }
        result = response.data
    }).catch(async error => {
        if (global.CONSOLE_AU_UPLOADFILE_CNTTTLTB64) {
            console.log("CONSOLE_AU_UPLOADFILE_CNTTTLTB64 ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_UPLOADFILE_CNTTTLTB64(ma_ctcb, chuc_nang, ten_file, base64, ma_van_ban_di).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_FILE_CDLF(res, chuc_nang) {
    let result = false
    var form = new FormData();
    var d = new Date()
    form.append("dung_luong", res.size);
    form.append("ma_don_vi_quan_tri", global.ma_don_vi_quan_tri);
    form.append("nam", d.getFullYear());
    form.append("ten_chuc_nang", chuc_nang);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_FILE_CDLF,
        headers: setHeaderToken(),
        data: form,
    }).then(response => {
        if (global.CONSOLE_AU_FILE_CDLF) {
            console.log("CONSOLE_AU_FILE_CDLF: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data.duoc_luu === 1 ? true : false
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_FILE_CDLF) {
            console.log("CONSOLE_AU_FILE_CDLF ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_FILE_CDLF(res, chuc_nang).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_FILE_TL(res, chuc_nang) {
    global.CONSOLE_AU_FILE_TL = true
    let result = null
    var form = new FormData();
    form.append('files', {
        name: res.name,
        uri: res.uri,
        type: res.type
    });
    form.append("ma_ctcb", global.ma_ctcb_kc.toString());
    form.append("chuc_nang", chuc_nang);
    if (global.CONSOLE_AU_FILE_TL) {
        console.log("CONSOLE_AU_FILE_TL form: " + JSON.stringify(form))
    }
    // const instance = axios.create();
    // instance.interceptors.request.use(function (config) {
    //     // Do something before request is sent
    //     console.log("CONSOLE_AU_FILE_TL request: " + JSON.stringify(config))
    //     return config;
    //   }, function (error) {
    //     // Do something with request error
    //     return Promise.reject(error);
    //   });
    //   await instance.post(
    //     global.AU_ROOT + URL_CONFIG.AU_FILE_TL, 
    //     form,
    //     {
    //         headers: {
    //                     "Content-Type": "multipart/form-data",
    //                     "Authorization": "Bearer " + global.access_token
    //                   }
    //     }
    // ).then(response => {
    //         if (global.CONSOLE_AU_FILE_TL) {
    //             console.log("CONSOLE_AU_FILE_TL: " + JSON.stringify(response))
    //         }
    //         result = response.data
    //     }).catch(async error => {
    //         if (global.CONSOLE_AU_FILE_TL) {
    //             console.log("CONSOLE_AU_FILE_TL ERROR: " + JSON.stringify(error))
    //         }
    //         if (error.response.status == 401) {
    //             await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
    //                 await AU_FILE_TL(res, chuc_nang).then(async (resCallback) => {
    //                     result = resCallback
    //                 })
    //             })
    //         }
    //     })
    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_FILE_TL,
        headers: setHeaderToken(),
        data: form,
    }).then(response => {
        if (global.CONSOLE_AU_FILE_TL) {
            console.log("CONSOLE_AU_FILE_TL: " + JSON.stringify(response))
        }
        result = response.data
    }).catch(async error => {
        if (global.CONSOLE_AU_FILE_TL) {
            console.log("CONSOLE_AU_FILE_TL ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_FILE_TL(res, chuc_nang).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_FILE_CNLF(chuoi_xoa, chuoi_con_lai, ma_van_ban_di, ma_ctcb, trich_yeu, ten_can_bo_xoa) {
    let result = false
    var form = new FormData();
    form.append("chuoi_xoa", chuoi_xoa);
    form.append("chuoi_con_lai", chuoi_con_lai);
    form.append("ma_van_ban_di", ma_van_ban_di);
    form.append("ma_ctcb", ma_ctcb);
    form.append("trich_yeu", trich_yeu);
    form.append("ten_can_bo_xoa", ten_can_bo_xoa);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_FILE_CNLF,
        data: form,
        headers: setHeaderToken(),
    }).then(response => {
        if (global.CONSOLE_AU_FILE_CNLF) {
            console.log("CONSOLE_AU_FILE_CNLF: " + JSON.stringify(response))
        }
        if (response.data.message === "Thực thi thành công") {
            result = true
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_FILE_CNLF) {
            console.log("CONSOLE_AU_FILE_CNLF ERROR: " + JSON.stringify(error))
        }
        console.log("CONSOLE_AU_FILE_CNLF ERROR: " + JSON.stringify(error))
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_FILE_CNLF(chuoi_xoa, chuoi_con_lai, ma_van_ban_di, ma_ctcb, trich_yeu, ten_can_bo_xoa).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

export default {
    AU_FILE_CDTTWP,
    AU_UPLOADFILE_CNTTTLTB64,
    AU_FILE_CDLF,
    AU_FILE_TL,
    AU_FILE_CNLF,
}