import { URL_CONFIG } from "./Config"
import { Platform } from "react-native"
import DeviceInfo from 'react-native-device-info'
import { Toast } from "native-base"
import axios from "axios"
import { ReplaceAll } from "../untils/TextUntils"
import { AppConfig } from "../AppConfig"
import API_Login from "./API_Login"

function showWarning(error) {
    Toast.show({ text: "Kết nối bị gián đoạn!", type: "warning", buttonText: "Đóng", position: "top", duration: 2000 })
    if (error) {
        SendLog(error)
    }
};

async function SendLog(message) {
    try {
        let obj = new Object
        let date = new Date()
        obj.Model = DeviceInfo.getModelSync()
        obj.OS = Platform.OS.toString()
        obj.SystemVersion = DeviceInfo.getSystemVersionSync()
        obj.AppVersion = DeviceInfo.getVersionSync()
        obj.CurrentTime = date.toLocaleDateString() + " " + date.toLocaleTimeString()
        obj.Module = "API_LichCongTac"
        obj.Domain = global.AU_ROOT ? global.AU_ROOT : ""
        delete message.headers
        obj.Message = message
        let text = ReplaceAll(JSON.stringify(obj), String.fromCharCode(34), "'")
        let url = AppConfig.AppDefaultLogURLSendText + text
        await axios({
            method: "POST",
            url: url,
            timeout: AppConfig.TIME_OUT_SHORT
        })
    } catch (error) {
        console.log("Send log error: " + error)
    }
}

function setHeaderToken() {
    return { "Authorization": "Bearer " + global.access_token }
};

// TIM KIEM LICH HOP CA NHAN
async function AU_LH_TKLHCN(ten_cuoc_hop, page, size) {
    let result = []
    var form = new FormData();
    form.append("tinh_trang", 0);
    form.append("ma_don_vi_chu_tri", 0);
    form.append("page", page);
    form.append("size", size);
    form.append("ma_ctcb", global.ma_ctcb_kc);
    form.append("ten_cuoc_hop", ten_cuoc_hop);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_LH_TKLHCN,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_LH_TKLHCN) {
            console.log("CONSOLE_AU_LH_TKLHCN: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_LH_TKLHCN) {
            console.log("CONSOLE_AU_LH_TKLHCN ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_LH_TKLHCN(ten_cuoc_hop, page, size).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

// LẤY SỐ TRANG LICH HOP CA NHAN
async function AU_LH_getPageLHCN(ten_cuoc_hop, size) {
    let result = []
    var form = new FormData();
    form.append("tinh_trang", 0);
    form.append("ma_don_vi_chu_tri", 0);
    form.append("page", "");
    form.append("size", size);
    form.append("ma_ctcb", global.ma_ctcb_kc);
    form.append("ten_cuoc_hop", ten_cuoc_hop);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_LH_TKLHCN,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_LH_getPageLHCN) {
            console.log("CONSOLE_AU_LH_getPageLHCN: " + JSON.stringify(response))
        }
        if (response.data.success) {
            result = response.data.total_page
        } else {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_LH_getPageLHCN) {
            console.log("CONSOLE_AU_LH_getPageLHCN ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_LH_getPageLHCN(ten_cuoc_hop, size).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

// cập nhật đã xem
async function AU_LH_CNNX(ma_thanh_phan_hop_kc) {
    let result = false
    var form = new FormData();
    form.append("ma_thanh_phan_hop_kc", ma_thanh_phan_hop_kc);
    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_LH_CNNX,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_LH_CNNX) {
            console.log("CONSOLE_AU_LH_CNNX: " + JSON.stringify(response))
        }
        if (response.data.message === "Thực thi thành công") {
            result = true
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_LH_CNNX) {
            console.log("CONSOLE_AU_LH_CNNX ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_LH_CNNX(ma_thanh_phan_hop_kc).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_LH_CTCH(ma_thanh_phan_hop) {
    let result = []
    var form = new Object()
    form.ma_ctcb = global.ma_ctcb_kc
    form.ma_thanh_phan_hop = ma_thanh_phan_hop

    await axios({
        method: "GET",
        url: global.AU_ROOT + URL_CONFIG.AU_LH_CTCH,
        headers: setHeaderToken(),
        params: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_LH_CTCH) {
            console.log("CONSOLE_AU_LH_CTCH: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_LH_CTCH) {
            console.log("CONSOLE_AU_LH_CTCH ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_LH_CTCH(ma_thanh_phan_hop).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_LH_DSTPH(ma_thanh_phan_hop) {
    let result = []
    var form = new Object()
    form.ma_thanh_phan_hop = ma_thanh_phan_hop

    await axios({
        method: "GET",
        url: global.AU_ROOT + URL_CONFIG.AU_LH_DSTPH,
        headers: setHeaderToken(),
        params: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_LH_DSTPH) {
            console.log("CONSOLE_AU_LH_DSTPH: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_LH_DSTPH) {
            console.log("CONSOLE_AU_LH_DSTPH ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_LH_DSTPH(ma_thanh_phan_hop).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

export default {
    AU_LH_TKLHCN,
    AU_LH_getPageLHCN,
    AU_LH_CNNX,
    AU_LH_CTCH,
    AU_LH_DSTPH,
}
