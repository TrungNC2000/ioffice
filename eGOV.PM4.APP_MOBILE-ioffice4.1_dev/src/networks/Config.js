export const URL_CONFIG = {
    "AU_DANH_SACH_APP": "https://tiengiangmobilentl-api.vnptioffice.vn",
    //NODULE LOGIN
    "AU_DSDVA": "/api/he-thong/danh-sach-don-vi-app",
    "AU_DSCTCB": "/api/can-bo/danh-sach-cong-tac-can-bo",
    "AU_DSCTCB_MOBILE":"/api/sso/danh-sach-cong-tac-can-bo",
    "AU_ACCESS_TOKEN": "/api/can-bo/access-token",
    "AU_DSDVCQ": "/api/can-bo/danh-sach-don-vi-chu-quan",
    "AU_KTQCVLDVTCCB": "/api/can-bo/kiem-tra-quyen-chuyen-vien-lanh-dao-van-thu-cua-can-bo",
    "AU_DSNVCB": "/api/nhac-viec/danh-sach-nhac-viec-can-bo",
    "AU_NVDB": "/api/nhac-viec/nhac-viec-dashboard",
    "AU_FCM": "/api/FCM-Server/cud-FCM",
    "AU_PUSH_FCM": "/api/FCM-Server/pusd-FCM",
    "AU_PUSH_FCM_APP": "/api/FCM-Server/pusd-FCM-app",
    "AU_DEL_FCM": "/api/FCM-Server/del-FCM",
    "AU_DSCBBID": "/api/danhmuc/danhsachcanbobyid",
    "AU_KTTMKC": "/api/canbo/kiem-tra-trung-mat-khau-cu",
    "AU_CNMK": "/api/can-bo/cap-nhat-mat-khau",
    "AU_CCBTCDB": "/api/he-thong/cay-can-bo-tra-cuu-danh-ba",
    "AU_DSTSHTTCTCB": "/api/he-thong/danh-sach-tham-so-he-thong-theo-cong-tac-can-bo",
    "AU_XTOTP": "/api/test/sms/xac-thuc-otp",
    "AU_KTOTP": "/api/test/sms/kiem-tra-otp",
    "AU_DKOTPNVSMS": "/api/can-bo/dang-ky-otp-nhac-viec-sms",
    "AU_GTSM": "/api/he-thong/get-tham-so-mobile",
    "AU_STSM": "/api/he-thong/set-tham-so-mobile",
    "AU_GMLDN": "/api/he-thong/ghi-moi-log-dang-nhap",
    "AU_TSCSY": "/api/can-bo/tham-so-clientid-secretclient-ybi",
    "AU_OAUTH2_TOKEN": "/oauth2/token",
    "AU_DSNVCMU": "/api/nhac-viec/nhac-viec-cmu",
    //MODULE TTDH
    "AU_TTDH_DA_GUI": "/api/thong-diep/thong-tin-dieu-hanh-da-gui",
    "AU_TTDH_DA_NHAN": "/api/thong-diep/thong-tin-dieu-hanh-da-nhan",
    "AU_TTDH_DSCBN": "/api/thong-diep/danh-sach-can-bo-nhan",
    "AU_TTDH_CTTTDHN": "/api/thong-diep/chi-tiet-thong-tin-dieu-hanh-nhan",
    "AU_TTDH_CTTTDHG": "/api/thong-diep/chi-tiet-thong-tin-dieu-hanh-gui",
    "AU_TTDH_LDSLTD": "/api/thong-diep/lay-danh-sach-loai-thong-diep",
    "AU_TTDH_DSCBNTTDH": "/api/can-bo/danh-sach-can-bo-nhan-thong-tin-dieu-hanh",
    "AU_TTDH_DSNCBNTTDH": "/api/can-bo/danh-sach-nhom-can-bo-nhan-thong-tin-dieu-hanh",
    "AU_TTDH_DSNCBNTTDHLT": "/api/can-bo/danh-sach-nhom-can-bo-nhan-thong-tin-dieu-hanh-lien-thong",
    "AU_TTDH_GTD": "/api/thong-diep/gui-thong-diep",
    "AU_TTDH_LTD": "/api/thong-diep/luu-thong-diep",
    "AU_TTDH_CNTTDHDG": "/api/thong-diep/cap-nhat-thong-tin-dieu-hanh-da-gui",
    "AU_TTDH_ATTDHN": "/api/thong-diep/an-thong-tin-dieu-hanh-nhan",

    //MODULE XIN Ý KIẾN THƯỜNG TRỰC - NAN
    "AU_XYKTT_DA_GUI": "/api/xin-y-kien/lay-danh-sach-xin-y-kien-da-gui", // đã dùng
    "AU_XYKTT_DA_NHAN": "/api/xin-y-kien/lay-danh-sach-xin-y-kien-da-nhan", // đã dùng
    "AU_XYKTT_DSCBN": "/api/xin-y-kien/lay-danh-sach-trang-thai-y-kien-can-bo", // đã dùng
    "AU_XYKTT_CTXYKTT": "/api/xin-y-kien/chi-tiet-xin-y-kien", // đã dùng
    "AU_XYKTT_DSBLCB": "/api/xin-y-kien/lay-danh-sach-y-kien-can-bo-tra-loi",// đã dùng
    "AU_XYKTT_YKTT_CREATE": "/api/xin-y-kien/them-moi-xin-y-kien",
    "AU_XYKTT_YKTT_DSNCB": "/api/can-bo/danh-sach-nhom-theo-ma-can-bo",
    "AU_XYKTT_YKTT_DSCBTN": "/api/xin-y-kien/danh-sach-ctcb-by-ma-nhom",
    "AU_GETQXYKTT": "/api/phan-quyen/danh-sach-menu-theo-refresh-token",
    "AU_XYKTT_DSNTDV": "/api/xin-y-kien/lay-danh-sach-nhom-theo-don-vi",
    "AU_XYKTT_DSCBTN": "/api/xin-y-kien/danh-sach-ctcb-by-ma-nhom",
    "AU_XYKTT_YKTT_DELETE": "/api/xin-y-kien/xoa-xin-y-kien",

    //MODULE VBDEN
    "AU_VBDEN_DSVBDTTTCCV": "/api/van-ban-den/danh-sach-van-ban-den-theo-trang-thai-cua-chuyen-vien",
    "AU_VBDEN_CVXVBD": "/api/van-ban-den/chuyen-vien-xem-van-ban-den",
    "AU_VBDEN_VBDCT": "/api/van-ban-den/van-ban-den-chi-tiet",
    "AU_VBDEN_CVXLNVBD": "/api/van-ban-den/chuyen-vien-xu-ly-nhanh-van-ban-den",
    "AU_VBDEN_CVXLVBD": "/api/van-ban-den/chuyen-vien-xu-ly-van-ban-den",
    "AU_VBDEN_CVCNYKXL": "/api/van-ban-den/chuyen-vien-cap-nhat-y-kien-xu-ly",
    "AU_VBDEN_BPLDVBD": "/api/van-ban-den/but-phe-lanh-dao-van-ban-den",
    "AU_VBDEN_BPLDC": "/api/van-ban-den/but-phe-lanh-dao-chuoi",
    "AU_VBDEN_DSCBTDVTTS": "/api/can-bo/danh-sach-can-bo-theo-don-vi-theo-tham-so",
    "AU_VBDEN_DSCBMD": "/api/can-bo/danh-sach-can-bo-nhan-van-ban-den-lanh-dao-duyet-mac-dinh",
    "AU_VBDEN_DSCBTVT": "/api/can-bo/danh-sach-can-bo-theo-vai-tro",
    "AU_VBDEN_CNCBTLN": "/api/can-bo/cay-nhom-can-bo-theo-loai-nhom",
    "AU_VBDEN_DSNCBNVBDLDD": "/api/can-bo/danh-sach-nhom-can-bo-nhan-van-ban-den-lanh-dao-duyet",
    "AU_VBDEN_CVCTH": "/api/van-ban-den/chuyen-vien-chuyen-thuc-hien",
    "AU_VBDEN_CCVCAP": "/api/van-ban-den/check-chuyen-vien-chuyen-app-pto",
    "AU_VBDEN_DSVBDCDCLD": "/api/van-ban-den/danh-sach-van-ban-den-cho-duyet-cua-lanh-dao",
    "AU_VBDEN_DSVBDUQDCLD": "/api/van-ban-den/danh-sach-van-ban-den-uy-quyen-duyet-cua-lanh-dao",
    "AU_VBDEN_DSVBDDCTHCLD": "/api/van-ban-den/danh-sach-van-ban-den-da-chuyen-thuc-hien-cua-lanh-dao",
    "AU_VBDEN_LDDVBD": "/api/van-ban-den/lanh-dao-duyet-van-ban-den",
    "AU_VBDEN_LDUQDVBD": "/api/van-ban-den/lanh-dao-uy-quyen-duyet-van-ban-den",
    "AU_VBDEN_LDDHTVBD": "/api/van-ban-den/lanh-dao-duyet-hoan-tat-vbde",
    "AU_VBDEN_DSLDDCD": "/api/van-ban-den/danh-sach-lanh-dao-duoc-chuyen-duyet",
    "AU_VBDEN_CLDKD": "/api/van-ban-den/chuyen-lanh-dao-khac-duyet",
    "AU_VANBAN_TCVB": "/api/van-ban/tra-cuu-van-ban",
    "AU_VANBAN_DSVBLQDK": "/api/van-ban/danh-sach-van-ban-lien-quan-dinh-kem",
    "AU_VBDEN_LDDPD": "/api/van-ban-den/lanh-dao-doi-phuc-dap",
    "AU_VBDEN_PCTHEODOI": "/api/van-ban-den/danh-sach-van-ban-den-phan-cong-theo-doi",
    "AU_VBDEN_CVTHEODOI": "/api/van-ban-den/chuyen-vien-theo-doi-vbde",
    //Văn thư
    "AU_VBDEN_DSDMSVBD": "/api/van-ban-den/danh-sach-danh-muc-so-van-ban-den",
    "AU_VBDEN_LSDTSVBD": "/api/van-ban-den/lay-so-den-theo-so-vb-den",
    "AU_VBDEN_DSCDK": "/api/van-ban/danh-sach-cap-do-khan",
    "AU_VBDEN_DSCDM": "/api/van-ban/danh-sach-cap-do-mat",
    "AU_VBDEN_DSLVVB": "/api/linh-vuc-van-ban/danh-sanh-linh-vuc-van-ban",
    "AU_VBDEN_DSLVB": "/api/DanhMuc/danhsachloaivanban",
    "AU_VBDEN_DSLDDVBDE": "/api/van-ban/danh-sach-lanh-dao-duyet-vbde",

    "AU_VBDEN_DSVBDTCVT": "/api/van-ban-den/danh-sach-van-ban-dien-tu-cua-van-thu",
    "AU_VBDEN_DSVBDML": "/api/van-ban-den/danh-sach-van-ban-den-moi-luu",
    "AU_VBDEN_DSVBLDTLCVT": "/api/van-ban-den/danh-sach-van-ban-lanh-dao-tra-lai-cua-van-thu",
    "AU_VBDEN_DSVBDCDCVT": "/api/van-ban-den/danh-sach-van-ban-den-cho-duyet-cua-van-thu",
    "AU_VBDEN_DSVBDCVTC": "/api/van-ban-den/danh-sach-van-ban-den-cho-van-thu-chuyen",
    "AU_VBDEN_DSVBDVTDC": "/api/van-ban-den/danh-sach-van-ban-den-van-thu-da-chuyen",
    "AU_VBDEN_DSVBDHCVT": "/api/van-ban-den/danh-sach-van-ban-da-huy-cua-van-thu",

    "AU_VBDEN_UTLT": "/api/file-manage/upload-tu-lien-thong",
    "AU_VBDEN_CNCVBDDTLT": "/api/van-ban-den/cap-nhat-chuoi-van-ban-den-dien-tu-lien-thong",

    //Văn thư xử lý văn bản đến
    "AU_VBDEN_KTTSKH": "/api/van-ban-den/kiem-tra-trung-so-ky-hieu",
    "AU_VBDEN_NTNHT": "api/he-thong/ngay-thang-nam-he-thong",
    "AU_VBDEN_TMVBD": "/api/van-ban-den/them-moi-van-ban-den",
    "AU_VBDEN_TMVBDLN": "/api/van-ban-den/them-moi-van-ban-den",
    "AU_VBDEN_CNKTMVBD": "/api/van-ban-den/cap-nhat-khi-them-moi-vb-den",
    "AU_VBDEN_VTHVBDDT": "/api/van-ban-den/van-thu-huy-van-ban-den-dien-tu",
    "AU_VBDEN_HUYTHEODOI": "/api/van-ban-den/van-ban-den-huy-theo-doi",
    "AU_VBDEN_DSDTCLUU": "/api/van-ban-den/danh-sach-van-ban-dien-tu-chua-luu-cua-van-thu",
    "AU_VBDEN_GET_INFO_EDXML": "/api/edoc/get-info-edoc/",
    "AU_VBDEN_UPDATE_STATUS_TEMP_EDOC": "/api/van-ban-den/cap-nhat-trang-thai-van-ban-edoc-tmp",
    "AU_VBDE_UPDATE_STATUS_EDXML": "/api/edoc/capnhattrangthai",
    "AU_VBDE_UPDATE_STATUS_TRUC_EDOC": "/api/edoc/Send-Status",
    "AU_VBDE_THEM_DE_HUY_EDOC": "/api/van-ban-den/huy-van-ban-edoc",
    //MODULE VBDI
    ////Danh sách văn bản của Chuyên viên
    "AU_VBDI_DSVBDCXLCCV": "/api/van-ban-di/danh-sach-van-ban-di-cho-xu-ly-cua-chuyen-vien",
    "AU_VBDI_DSVBDDXLCCV": "/api/van-ban-di/danh-sach-van-ban-di-da-xu-ly-cua-chuyen-vien",
    "AU_VBDI_DSVBDCPHCCV": "/api/van-ban-di/danh-sach-van-ban-di-cho-phat-hanh-cua-chuyen-vien",
    "AU_VBDI_DSVBDDPHCCV": "/api/van-ban-di/danh-sach-van-ban-di-da-phat-hanh-cua-chuyen-vien",
    "AU_VBDI_CNDX": "/api/van-ban-di/cap-nhap-da-xem",
    "AU_VBDI_CTVBD": "/api/van-ban-di/chi-tiet-van-ban-di",
    "AU_VBDI_QTXLVBD": "/api/van-ban-di/qua-trinh-xu-ly-van-ban-di",
    "AU_VBDI_HTVBDI": "/api/van-ban-di/hoan-tat-vbdi",
    "AU_VBDI_DPH7N": "/api/van-ban/tra-cuu-vbdi-cmu",
    "AU_VBDI_CHO_THUONG_TRUC": "/api/van-ban-di/danh-sach-cho-thuong-truc-ky-cv",
    ////Danh sách văn bản của Lãnh đạo
    "AU_VBDI_VBDCDCLD": "/api/van-ban-di/van-ban-di-cho-duyet-cua-lanh-dao",
    "AU_VBDI_VBDDUQCLD": "/api/van-ban-di/van-ban-di-duoc-uy-quyen-cua-lanh-dao",
    "AU_VBDI_VBDCPHCLD": "/api/van-ban-di/van-ban-di-cho-phat-hanh-cua-lanh-dao",
    "AU_VBDI_VBDDPHCLD": "/api/van-ban-di/van-ban-di-da-phat-hanh-cua-lanh-dao",

    ////Lãnh đạo duyệt chuyển văn thư
    "AU_VBDI_DSVTTDVQT": "/api/can-bo/danh-sach-van-thu-theo-don-vi-quan-tri",
    "AU_VBDI_LDDVBD": "/api/van-ban-di/lanh-dao-duyet-van-ban-di",
    "AU_VBDI_LDCVT": "/api/van-ban-di/lanh-dao-chuyen-van-thu",

    ////Lãnh đạo duyệt chuyển chuyên viên
    "AU_VBDI_DSCBTDVTS": "/api/can-bo/danh-sach-can-bo-theo-don-vi-tham-so",
    "AU_VBDI_LDCVBDCCV": "/api/van-ban-di/lanh-dao-chuyen-van-ban-di-cho-chuyen-vien",

    ////Lãnh đạo chuyển lãnh đạo khác duyệt
    "AU_VBDI_LDCVBDCLD": "/api/van-ban-di/lanh-dao-chuyen-van-ban-di-cho-lanh-dao",

    ////Chuyên viên chuyển lãnh đạo
    "AU_VBDI_DSLDCDK": "/api/van-ban-di/danh-sach-lanh-dao-chuyen-duyet-khac",
    "AU_VBDI_CVCVBDCLD": "/api/van-ban-di/chuyen-vien-chuyen-van-ban-di-cho-lanh-dao",

    ////Chuyên viên chuyển chuyên viên
    "AU_VBDI_CVCVBDCCV": "/api/van-ban-di/chuyen-vien-chuyen-van-ban-di-cho-chuyen-vien",

    ////Chuyên viên góp ý văn bản
    "AU_VBDI_CVGY": "/api/van-ban-di/chuyen-vien-gop-y",

    ////Chuyên viên chuyển văn thư
    "AU_VBDI_CVCVT": "/api/van-ban-di/chuyen-vien-chuyen-van-thu",

    ////Lấy danh sách phiếu trình
    "AU_VBDI_DSBPVBDICHGG": "/api/van-ban-den/danh-sach-but-phe-vbdi-chinh-hgg",

    ////Xóa file văn bản
    "AU_VBDI_CNLF": "/api/van-ban/cap-nhat-lai-file",

    ////danh sách cán bộ pháp chế uu tiên
    "AU_VBDI_DSCBPCUT": "/api/can-bo/danh-sach-can-bo-phap-che-uu-tien",
    /// chuyển pháp chế lãnh đạo chuyển pháp chế
    "AU_VBDI_LDCHUYENPHAPCHE": "/api/van-ban-di/lanh-dao-chuyen-van-ban-di-cho-phap-che",
    "AU_VBDI_CVCHUYENPC": "/api/van-ban-di/chuyen-vien-chuyen-van-ban-di-cho-phap-che",
    //MODULE VBNB
    "AU_VBNB_DSVBNBDN": "/api/van-ban-noi-bo/danh-sach-van-ban-noi-bo-da-nhan",
    "AU_VBNB_CNVBNBDN": "/api/van-ban-noi-bo/cap-nhat-van-ban-noi-bo-da-nhan",
    "AU_VBNB_CTVBNB": "/api/van-ban-noi-bo/chi_tiet_van_ban_noi_bo",
    "AU_VBNB_CNVBNBCCS": "/api/van-ban-noi-bo/cap-nhat-van-ban-noi-bo-cho-cap-so",
    "AU_VBNB_CQTXLVBNB": "/api/van-ban-noi-bo/cay-qua-trinh-xu-ly-vbnb",
    "AU_VBNB_DSVBNBDGDPH": "/api/van-ban-noi-bo/danh-sach-van-ban-noi-bo-da-gui-da-phat-hanh",
    "AU_VBNB_CTVBNBG": "/api/van-ban-noi-bo/chi_tiet_van_ban_noi_bo_gui",
    "AU_VBNB_DSCBNVBNB": "/api/can-bo/danh-sach-can-bo-nhan-vbnb",
    "AU_VBNB_CNCBTLN": "/api/can-bo/cay-nhom-can-bo-theo-loai-nhom",
    "AU_VBNB_CVBNB": "/api/van-ban-noi-bo/chuyen-van-ban-noi-bo",
    "AU_VBNB_HVBNB": "/api/van-ban-noi-bo/huy-van-ban-noi-bo",
    "AU_VBNB_DSNCBNVBNB": "/api/can-bo/danh-sach-nhom-can-bo-nhan-vbnb",

    //MODULE LCT
    "AU_LCT_DSCQ": "/api/lich-cong-tac-chi-tiet/danh-sach-don-vi-quan-tri",
    "AU_LCT_DSCQ1": "/api/lich-cong-tac-chi-tiet/danh-sach-don-vi-tu-don-vi-dinh-danh-tro-xuong",
    "AU_LCT_LLCTCT": "/api/LichCongTac/LichCongTacChiTiet/LayLichCongTacChiTiet",
    //LCT Co Quan
    "AU_LCT_DSLCTDVQT": "/api/lich-cong-tac/danh-sach-lich-cong-tac-don-vi-quan-tri",
    //LCT Don Vi
    "AU_LCT_DSLCTDV": "/api/lich-cong-tac/danh-sach-lich-cong-tac-don-vi",
    //LCT Ca Nhan
    "AU_LCT_XLCTCN": "/api/LichCongTac/LichCongTacCaNhan/XemLichCongTacCaNhan",
    //DS Cot
    "AU_LCT_DSCBLCT": "/api/lich-cong-tac-chi-tiet/danh-sach-cot-by-lich-cong-tac",
    //Duyet LCT
    "AU_LCT_DSDVDDC": "/api/lich-cong-tac-chi-tiet/danh-sach-don-vi-dinh-danh-con",
    "AU_LCT_DLCTCQ": "/api/lich-cong-tac/duyet-lich-cong-tac-co-quan",

    "AU_LCT_DSDV": "/api/lich-cong-tac-chi-tiet/danh-sach-can-bo-cung-don-vi-quan-tri",
    "AU_LCT_DSCB": "/api/lich-cong-tac-chi-tiet/danh-sach-can-bo-cung-don-vi",
    "AU_LCT_CTLCT": "/api/lich-cong-tac-chi-tiet/chi-tiet-lich-cong-tac-mobile",
    "AU_LCT_DSCOT": "/api/lich-cong-tac-chi-tiet/danh-sach-cot-lich-cong-tac-mobile",
    "AU_LCT_CHHT": "/api/he-thong/danh-sach-tham-so-he-thong-theo-cong-tac-can-bo",
    "AU_LCT_LCTLDBC": "/api/lich-cong-tac/lich-cong-tac-lanh-dao-by-cot",
    "AU_LCT_XLCTLD": "/api/lich-cong-tac/xem-lich-cong-tac-lanh-dao",

    // MODULE LICH HOP
    "AU_LH_TKLHCN": "/api/danh-sach-cuoc-hop/tim-kiem-lich-hop-ca-nhan",
    "AU_LH_CTCH": "/api/cuoc-hop/chi-tiet-cuoc-hop",
    "AU_LH_DSTPH": "/api/thanh-phan-hop/danh-sach-thanh-phan-hop",
    "AU_LH_CNNX": "/api/danh-sach-cuoc-hop/cap-nhat-ngay-xem",

    //MODULE SMS
    "AU_SMS_DSCBGTN": "/api/tin-nhan/danh-sach-can-bo-gui-tin-nhan",
    "AU_SMS_DSCBNSMSBM": "/api/tin-nhan/danh-sach-can-bo-nhan_sms_by_ma",
    "AU_SMS_XTNDGCCB": "/api/tin-nhan/xoa-tin-nhan-da-gui-cua-can-bo",
    "AU_SMS_DSDVGTN": "/api/tin-nhan/danh-sach-don-vi-gui-tin-nhan",
    "AU_SMS_DSCBNTTDH": "/api/can-bo/danh-sach-can-bo-nhan-thong-tin-dieu-hanh",
    "AU_SMS_DSNCBNTN": "/api/can-bo/danh-sach-nhom-can-bo-nhan-tin-nhan",
    "AU_SMS_CNCBTLN": "/api/can-bo/cay-nhom-can-bo-theo-loai-nhom",
    "AU_SMS_STNSMS": "/api/tin-nhan/soan-tin-nhan-sms",
    "AU_SMS_GTN": "/api/test/sms/gui-tin-nhan",
    "AU_SMS_GTNSMS": "/api/tin-nhan/gui-tin-nhan-sms",

    //MODULE CDDH
    "AU_CDDH_TMCDDHTVBD": "/api/dieu-hanh-chi-dao-di/them-moi-chi-dao-theo-van-ban-den",
    "AU_CDDH_PCNVTUVBD": "/api/dieu-hanh-chi-dao-den/phan-cong-nhiem-vu-tu-vbde",

    //KY SO
    "AU_KYSO_DSCKS": "/api/DanhMuc/CKS/danhsachcks",
    "AU_KYSO_KSS": "/api/tien-ich/ky-so-sim",
    "AU_KYSO_KSSGFS": "/api/tien-ich/ky-so-sim-GetFileSigned",
    "AU_KYSO_VTCNFKPH": "/api/van-ban-di/van-thu-cap-nhat-file-khi-phat-hanh",
    "AU_KYSO_LFKS": "/api/van-ban-di/luu-file-ky-so",
    "AU_KYSO_TMCKS": "/api/DanhMuc/CKS/TaoMoiCKS",
    "AU_KYSO_XCKS": "/api/DanhMuc/CKS/XoaCKS",
    "AU_KYSO_CNCKS": "/api/DanhMuc/CKS/CapNhatCKS",
    "AU_KYSO_TFP": "/api/vgca/translate-from-path",
    "AU_KYSO_KSPKI": "/api/tien-ich/ky-so-pki",

    "AU_KYSO_GBFP": "/api/tien-ich/get-base64-from-path",
    "AU_KYSO_GATH": "/api/tien-ich/get-access-token-hsm",
    "AU_KYSO_KSH": "/api/tien-ich/ky-so-hsm",
    "AU_KYSO_SCGAT": "/api/tien-ich/smart-ca-get-access-token",
    "AU_KYSO_SCSH": "/api/tien-ich/smart-ca-sign-hash",
    "AU_KYSO_SCSH_IOS": "/api/tien-ich/smart-ca-sign-hash-ios",
    "AU_KYSO_SCGSF": "/api/tien-ich/smartca-get-signed-file-ios",

    "AU_KYSO_UFSKK": "/api/van-ban-di/update-file-sau-khi-ky",
    "AU_KYSO_GFDK": "/api/van-ban-di/get-file-da-ky",

    "AU_KYSO_CNVBDKPTO": "/api/van-ban-di/cap-nhat-van-ban-dinh-kem-pto",
    "AU_KYSO_LFKS_VBNB": "/api/van-ban-noi-bo/cap-nhat-file-app",
    "AU_KYSO_SMARTCA_GATD": "/api/tien-ich/smartca-get-access-token-db",

    //FILE
    "AU_FILE_CDTTWP": "/api/tap-tin/chuyen-doi-tep-tin-word-pdf",
    "AU_UPLOADFILE_CNTTTLTB64": "/api/quan-ly-tap-tin/cap-nhat-them-tap-tin-len-tu-base64",
    "AU_FILE_CDLF": "/api/he-thong/check-dung-luong-file",
    "AU_FILE_TL": "/api/quan-ly-tap-tin/tai-len",
    "AU_FILE_CNLF": "/api/van-ban/cap-nhat-lai-file",
    "COPY_DOCUMENT_EDOC": "/api/file/copy-document/"
}

//Chú thích cái biến global
//global.AU_ROOT
//Chứa api_url của đơn vị để gọi cái api theo domain
