import { URL_CONFIG } from "./Config"
import { Platform } from "react-native"
import DeviceInfo from 'react-native-device-info'
import { Toast } from "native-base"
import axios from "axios"
import { ReplaceAll } from "../untils/TextUntils"
import { AppConfig } from "../AppConfig"
import API_Login from "./API_Login"

function showWarning(error) {
    Toast.show({ text: "Kết nối bị gián đoạn!", type: "warning", buttonText: "Đóng", position: "top", duration: 2000 })
    if (error) {
        SendLog(error)
    }
};

async function SendLog(message) {
    try {
        let obj = new Object
        let date = new Date()
        obj.Model = DeviceInfo.getModelSync()
        obj.OS = Platform.OS.toString()
        obj.SystemVersion = DeviceInfo.getSystemVersionSync()
        obj.AppVersion = DeviceInfo.getVersionSync()
        obj.CurrentTime = date.toLocaleDateString() + " " + date.toLocaleTimeString()
        obj.Module = "API_ThongTinDieuHanh"
        obj.Domain = global.AU_ROOT ? global.AU_ROOT : ""
        delete message.headers
        obj.Message = message
        let text = ReplaceAll(JSON.stringify(obj), String.fromCharCode(34), "'")
        let url = AppConfig.AppDefaultLogURLSendText + text
        await axios({
            method: "POST",
            url: url,
            timeout: AppConfig.TIME_OUT_SHORT
        })
    } catch (error) {
        console.log("Send log error: " + error)
    }
}

function setHeaderToken() {
    return { "Authorization": "Bearer " + global.access_token }
};

async function AU_GETPAGE_TTDH_DA_GUI(ma_can_bo, page, size, tieu_de, noi_dung) {
    let result = []
    let form = new FormData()
    form.append("ma_can_bo", ma_can_bo)
    form.append("ma_loai_ttdh", 0)
    form.append("tieu_de", tieu_de)
    form.append("noi_dung", noi_dung)
    form.append("trang_thai_ttdh", 1)
    form.append("gui_tu_ngay", "")
    form.append("gui_den_ngay", "")
    form.append("nam", 0)
    form.append("co_tep_tin", -1)
    form.append("total_row", 0)
    form.append("page", page)
    form.append("size", size)

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_TTDH_DA_GUI,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_GETPAGE_TTDH_DA_GUI) {
            console.log("CONSOLE_AU_GETPAGE_TTDH_DA_GUI: " + JSON.stringify(response))
        }
        if (response.data.success) {
            result = response.data.total_page
        } else {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_GETPAGE_TTDH_DA_GUI) {
            console.log("CONSOLE_AU_GETPAGE_TTDH_DA_GUI ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_GETPAGE_TTDH_DA_GUI(ma_can_bo, page, size, tieu_de, noi_dung).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}
async function AU_TTDH_DA_GUI(ma_can_bo, page, size, tieu_de, noi_dung) {
    let result = []
    let form = new FormData()
    form.append("ma_can_bo", ma_can_bo)
    form.append("ma_loai_ttdh", 0)
    form.append("tieu_de", tieu_de)
    form.append("noi_dung", noi_dung)
    form.append("trang_thai_ttdh", 1)
    form.append("gui_tu_ngay", "")
    form.append("gui_den_ngay", "")
    form.append("nam", 0)
    form.append("co_tep_tin", -1)
    form.append("total_row", 0)
    form.append("page", page)
    form.append("size", size)

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_TTDH_DA_GUI,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_TTDH_DA_GUI) {
            console.log("CONSOLE_AU_TTDH_DA_GUI: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_TTDH_DA_GUI) {
            console.log("CONSOLE_AU_TTDH_DA_GUI ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_TTDH_DA_GUI(ma_can_bo, page, size, tieu_de, noi_dung).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_TTDH_DA_GUIV23(ma_can_bo, page, size, nam, tieu_de, noi_dung) {
    let result = []
    let form = new FormData()
    form.append("ma_can_bo", ma_can_bo)
    form.append("ma_loai_ttdh", 0)
    form.append("tieu_de", tieu_de)
    form.append("noi_dung", noi_dung)
    form.append("trang_thai_ttdh", 1)
    form.append("nam", 0)
    if (nam !== 0) {
        form.append("gui_tu_ngay", "01/01/" + nam);
        form.append("gui_den_ngay", "31/12/" + nam);
    }
    form.append("co_tep_tin", -1)
    form.append("total_row", 0)
    form.append("page", page)
    form.append("size", size)

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_TTDH_DA_GUI,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_TTDH_DA_GUI) {
            console.log("CONSOLE_AU_TTDH_DA_GUI: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_TTDH_DA_GUI) {
            console.log("CONSOLE_AU_TTDH_DA_GUI ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_TTDH_DA_GUIV23(ma_can_bo, page, size, nam, tieu_de, noi_dung).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_TTDH_DA_NHAN(ma_can_bo, page, size, tieu_de) {
    let result = []
    var form = new FormData();
    form.append("co_tep_tin", -1);
    form.append("ma_can_bo", ma_can_bo);
    //form.append("ma_don_vi_nhan", ma_don_vi_cha);
    form.append("ma_loai_ttdh", 0);
    form.append("nam", 0);
    form.append("page", page);
    form.append("size", size);
    form.append("tieu_de", tieu_de);
    form.append("trang_thai_ttdh_gui", "-1");

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_TTDH_DA_NHAN,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_TTDH_DA_NHAN) {
            console.log("CONSOLE_AU_TTDH_DA_NHAN: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_TTDH_DA_NHAN) {
            console.log("CONSOLE_AU_TTDH_DA_NHAN ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_TTDH_DA_NHAN(ma_can_bo, page, size, tieu_de).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_TTDH_DA_NHANV23(ma_can_bo, page, size, nam, tieu_de) {
    let result = []
    var form = new FormData();
    form.append("co_tep_tin", -1);
    form.append("ma_can_bo", ma_can_bo);
    form.append("ma_loai_ttdh", 0);
    form.append("nam", 0);
    if (nam !== 0) {
        form.append("nhan_tu_ngay", "01/01/" + nam);
        form.append("nhan_den_ngay", "31/12/" + nam);
    }
    form.append("page", page);
    form.append("size", size);
    form.append("tieu_de", tieu_de);
    form.append("trang_thai_ttdh_gui", "-1");

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_TTDH_DA_NHAN,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_TTDH_DA_NHAN) {
            console.log("CONSOLE_AU_TTDH_DA_NHAN: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_TTDH_DA_NHAN) {
            console.log("CONSOLE_AU_TTDH_DA_NHAN ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_TTDH_DA_NHANV23(ma_can_bo, page, size, nam, tieu_de).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_TTDH_DSCBN(ma_ttdh_kc) {
    let result = []
    var form = new Object()
    form.id = ma_ttdh_kc

    await axios({
        method: "GET",
        url: global.AU_ROOT + URL_CONFIG.AU_TTDH_DSCBN,
        headers: setHeaderToken(),
        params: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_TTDH_DSCBN) {
            console.log("CONSOLE_AU_TTDH_DSCBN: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_TTDH_DSCBN) {
            console.log("CONSOLE_AU_TTDH_DSCBN ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_TTDH_DSCBN(ma_ttdh_kc).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_TTDH_CTTTDHN(ma_ttdh_gui_kc, ma_can_bo) {
    let result = []
    var form = new Object()
    form.ma_ttdh_gui_kc = ma_ttdh_gui_kc
    form.ma_can_bo = ma_can_bo

    await axios({
        method: "GET",
        url: global.AU_ROOT + URL_CONFIG.AU_TTDH_CTTTDHN,
        headers: setHeaderToken(),
        params: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_TTDH_CTTTDHN) {
            console.log("CONSOLE_AU_TTDH_CTTTDHN: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_TTDH_CTTTDHN) {
            console.log("CONSOLE_AU_TTDH_CTTTDHN ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_TTDH_CTTTDHN(ma_ttdh_gui_kc, ma_can_bo).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_TTDH_CTTTDHG(ma_ttdh, ma_ctcb_nhan, ma_ctcb_gui) {
    let result = []
    var form = new Object()
    form.ma_ttdh = ma_ttdh
    form.ma_ctcb_nhan = ma_ctcb_nhan
    form.ma_ctcb_gui = ma_ctcb_gui

    await axios({
        method: "GET",
        url: global.AU_ROOT + URL_CONFIG.AU_TTDH_CTTTDHG,
        headers: setHeaderToken(),
        params: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_TTDH_CTTTDHG) {
            console.log("CONSOLE_AU_TTDH_CTTTDHG: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_TTDH_CTTTDHG) {
            console.log("CONSOLE_AU_TTDH_CTTTDHG ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_TTDH_CTTTDHG(ma_ttdh, ma_ctcb_nhan, ma_ctcb_gui).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_TTDH_LDSLTD() {
    let result = []
    await axios({
        method: "GET",
        url: global.AU_ROOT + URL_CONFIG.AU_TTDH_LDSLTD,
        headers: setHeaderToken(),
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_TTDH_LDSLTD) {
            console.log("CONSOLE_AU_TTDH_LDSLTD: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_TTDH_LDSLTD) {
            console.log("CONSOLE_AU_TTDH_LDSLTD ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_TTDH_LDSLTD().then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_TTDH_DSCBNTTDH() {
    let result = []
    var form = new Object()
    form.ma_don_vi = global.ma_don_vi
    form.ma_can_bo = global.ma_can_bo
    form.type = "tree"

    await axios({
        method: "GET",
        url: global.AU_ROOT + URL_CONFIG.AU_TTDH_DSCBNTTDH,
        headers: setHeaderToken(),
        params: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_TTDH_DSCBNTTDH) {
            console.log("CONSOLE_AU_TTDH_DSCBNTTDH: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_TTDH_DSCBNTTDH) {
            console.log("CONSOLE_AU_TTDH_DSCBNTTDH ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_TTDH_DSCBNTTDH().then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_TTDH_DSNCBNTTDH(ma_can_bo) {
    let result = []
    var form = new Object()
    form.ma_can_bo = ma_can_bo
    form.type = "tree"

    await axios({
        method: "GET",
        url: global.AU_ROOT + URL_CONFIG.AU_TTDH_DSNCBNTTDH,
        headers: setHeaderToken(),
        params: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_TTDH_DSNCBNTTDH) {
            console.log("CONSOLE_AU_TTDH_DSNCBNTTDH: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_TTDH_DSNCBNTTDH) {
            console.log("CONSOLE_AU_TTDH_DSNCBNTTDH ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_TTDH_DSNCBNTTDH(ma_can_bo).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_TTDH_DSNCBNTTDHLT(ma_can_bo) {
    let result = []
    var form = new Object()
    form.ma_can_bo = ma_can_bo
    form.type = "tree"

    await axios({
        method: "GET",
        url: global.AU_ROOT + URL_CONFIG.AU_TTDH_DSNCBNTTDHLT,
        headers: setHeaderToken(),
        params: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_TTDH_DSNCBNTTDHLT) {
            console.log("CONSOLE_AU_TTDH_DSNCBNTTDHLT: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_TTDH_DSNCBNTTDHLT) {
            console.log("CONSOLE_AU_TTDH_DSNCBNTTDHLT ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_TTDH_DSNCBNTTDHLT(ma_can_bo).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_TTDH_GTD(ma_ttdh_kc, ma_loai_ttdh, ma_ctcb_tao, tieu_de, noi_dung, ngay_gui, src_file_ttdh, trang_thai_ttdh, tra_loi_cho_ttdh, chuyen_tiep_tu_ttdh, chuoi_ma_ctcb_nhan, sms, ma_ttdh_goc) {
    let result = ""
    let form = new FormData();
    form.append("ma_ttdh_kc", parseInt(ma_ttdh_kc));
    form.append("ma_loai_ttdh", parseInt(ma_loai_ttdh));
    form.append("ma_ctcb_tao", parseInt(ma_ctcb_tao));
    form.append("tieu_de", tieu_de);
    form.append("noi_dung", noi_dung);
    form.append("ngay_gui", ngay_gui);
    form.append("src_file_ttdh", src_file_ttdh);
    form.append("trang_thai_ttdh", parseInt(trang_thai_ttdh));
    form.append("tra_loi_cho_ttdh", parseInt(tra_loi_cho_ttdh));
    form.append("chuyen_tiep_tu_ttdh", parseInt(chuyen_tiep_tu_ttdh));
    form.append("chuoi_ma_ctcb_nhan", chuoi_ma_ctcb_nhan);
    form.append("sms", parseInt(sms));
    form.append("ma_ttdh_goc", parseInt(ma_ttdh_goc));

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_TTDH_GTD,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_TTDH_GTD) {
            console.log("CONSOLE_AU_TTDH_GTD: " + JSON.stringify(response))
        }
        if (response.data.message === "Thực thi thành công") {
            result = response.data.id
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_TTDH_GTD) {
            console.log("CONSOLE_AU_TTDH_GTD ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_TTDH_GTD(ma_ttdh_kc, ma_loai_ttdh, ma_ctcb_tao, tieu_de, noi_dung, ngay_gui, src_file_ttdh, trang_thai_ttdh, tra_loi_cho_ttdh, chuyen_tiep_tu_ttdh, chuoi_ma_ctcb_nhan, sms, ma_ttdh_goc).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_TTDH_LTD(ma_ttdh_kc, ma_loai_ttdh, ma_ctcb_tao, tieu_de, noi_dung, ngay_gui, trang_thai_ttdh, tra_loi_cho_ttdh, chuyen_tiep_tu_ttdh, chuoi_ma_ctcb_nhan, sms, ma_ttdh_goc) {
    let result = ""
    let form = new FormData();
    form.append("ma_ttdh_kc", parseInt(ma_ttdh_kc));
    form.append("ma_loai_ttdh", parseInt(ma_loai_ttdh));
    form.append("ma_ctcb_tao", parseInt(ma_ctcb_tao));
    form.append("tieu_de", tieu_de);
    form.append("noi_dung", noi_dung);
    form.append("ngay_gui", ngay_gui);
    form.append("trang_thai_ttdh", parseInt(trang_thai_ttdh));
    form.append("tra_loi_cho_ttdh", parseInt(tra_loi_cho_ttdh));
    form.append("chuyen_tiep_tu_ttdh", parseInt(chuyen_tiep_tu_ttdh));
    form.append("chuoi_ma_ctcb_nhan", chuoi_ma_ctcb_nhan);
    form.append("sms", parseInt(sms));
    form.append("ma_ttdh_goc", parseInt(ma_ttdh_goc));

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_TTDH_LTD,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_TTDH_LTD) {
            console.log("CONSOLE_AU_TTDH_LTD: " + JSON.stringify(response))
        }
        if (response.data.message === "Thực thi thành công") {
            result = response.data.id
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_TTDH_LTD) {
            console.log("CONSOLE_AU_TTDH_LTD ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_TTDH_LTD(ma_ttdh_kc, ma_loai_ttdh, ma_ctcb_tao, tieu_de, noi_dung, ngay_gui, trang_thai_ttdh, tra_loi_cho_ttdh, chuyen_tiep_tu_ttdh, chuoi_ma_ctcb_nhan, sms, ma_ttdh_goc).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_TTDH_CNTTDHDG(chuoi_ma_ttdh, trang_thai_ttdh) {
    let result = ""
    let form = new FormData();
    form.append("chuoi_ma_ttdh", chuoi_ma_ttdh);
    form.append("trang_thai_ttdh", parseInt(trang_thai_ttdh));

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_TTDH_CNTTDHDG,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_TTDH_CNTTDHDG) {
            console.log("CONSOLE_AU_TTDH_CNTTDHDG: " + JSON.stringify(response))
        }
        if (response.data.message === "Thực thi thành công") {
            result = response.data.id
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_TTDH_CNTTDHDG) {
            console.log("CONSOLE_AU_TTDH_CNTTDHDG ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_TTDH_CNTTDHDG(chuoi_ma_ttdh, trang_thai_ttdh).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_TTDH_ATTDHN(chuoi_ma_ttdh, ma_ctcb_nhan) {
    let result = ""
    let form = new FormData();
    form.append("chuoi_ma_ttdh", chuoi_ma_ttdh);
    form.append("ma_ctcb_nhan", parseInt(ma_ctcb_nhan));

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_TTDH_ATTDHN,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_TTDH_ATTDHN) {
            console.log("CONSOLE_AU_TTDH_ATTDHN: " + JSON.stringify(response))
        }
        if (response.data.message === "Thực thi thành công") {
            result = response.data.id
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_TTDH_ATTDHN) {
            console.log("CONSOLE_AU_TTDH_ATTDHN ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_TTDH_ATTDHN(chuoi_ma_ttdh, ma_ctcb_nhan).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

export default {
    AU_GETPAGE_TTDH_DA_GUI,
    AU_TTDH_DA_GUI,
    AU_TTDH_DA_GUIV23,
    AU_TTDH_DA_NHAN,
    AU_TTDH_DA_NHANV23,
    AU_TTDH_DSCBN,
    AU_TTDH_CTTTDHN,
    AU_TTDH_CTTTDHG,
    AU_TTDH_LDSLTD,
    AU_TTDH_DSCBNTTDH,
    AU_TTDH_DSNCBNTTDH,
    AU_TTDH_DSNCBNTTDHLT,
    AU_TTDH_GTD,
    AU_TTDH_LTD,
    AU_TTDH_CNTTDHDG,
    AU_TTDH_ATTDHN
}