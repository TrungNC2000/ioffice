import { URL_CONFIG } from "./Config"
import { Platform } from "react-native"
import DeviceInfo from 'react-native-device-info'
import { Toast } from "native-base"
import axios from "axios"
import { ReplaceAll, checkVersionWeb } from "../untils/TextUntils"
import { AppConfig } from "../AppConfig"
import API_Login from "./API_Login"

function showWarning(error) {
    Toast.show({ text: "Kết nối bị gián đoạn!", type: "warning", buttonText: "Đóng", position: "top", duration: 2000 })
    if (error) {
        SendLog(error)
    }
};

async function SendLog(message) {
    try {
        let obj = new Object
        let date = new Date()
        obj.Model = DeviceInfo.getModelSync()
        obj.OS = Platform.OS.toString()
        obj.SystemVersion = DeviceInfo.getSystemVersionSync()
        obj.AppVersion = DeviceInfo.getVersionSync()
        obj.CurrentTime = date.toLocaleDateString() + " " + date.toLocaleTimeString()
        obj.Module = "API_ChiDaoDieuHanh"
        obj.Domain = global.AU_ROOT ? global.AU_ROOT : ""
        delete message.headers
        obj.Message = message
        let text = ReplaceAll(JSON.stringify(obj), String.fromCharCode(34), "'")
        let url = AppConfig.AppDefaultLogURLSendText + text
        await axios({
            method: "POST",
            url: url,
            timeout: AppConfig.TIME_OUT_SHORT
        })
    } catch (error) {
        console.log("Send log error: " + error)
    }
}

function setHeaderToken() {
    return { "Authorization": "Bearer " + global.access_token }
};

// Đăng AGG API tạo chỉ đạo điều hành khi lãnh đạo duyệt VB đến
async function AU_CD_TAO_CDDH(ma_ctcb_gui, ma_ctcb_chi_dao, ma_don_vi_quan_tri, ma_van_ban_den, noi_dung, chuoi_can_bo_nhan) {
    let result = false
    var form = new FormData();
    form.append("ma_chi_dao_kc", 0)
    form.append("ma_ctcb_gui", parseInt(ma_ctcb_gui))
    form.append("ma_ctcb_chi_dao", parseInt(ma_ctcb_chi_dao))
    form.append("ma_don_vi_quan_tri", parseInt(ma_don_vi_quan_tri))
    form.append("ma_van_ban_den", parseInt(ma_van_ban_den))
    form.append("noi_dung", noi_dung)
    form.append("chuoi_can_bo_nhan", JSON.stringify(chuoi_can_bo_nhan));
    form.append("chuoi_ma_ctcb", '');
    form.append("chuoi_hxl", '');
    form.append("chuoi_vai_tro", '');
    
    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_CDDH_TMCDDHTVBD,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_CDDH_TMCDDHTVBD) {
            console.log("CONSOLE_AU_CDDH_TMCDDHTVBD: " + JSON.stringify(response))
        }
        if (response.status === 200) {
            result = true
        } else {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_CDDH_TMCDDHTVBD) {
            console.log("CONSOLE_AU_CDDH_TMCDDHTVBD ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_CD_TAO_CDDH(ma_ctcb_gui, ma_ctcb_chi_dao, ma_don_vi_quan_tri, ma_van_ban_den, noi_dung, chuoi_can_bo_nhan).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

// Đăng AGG API phân công chỉ đạo điều hành
async function AU_CDDH_PCNVTUVBD(ma_van_ban_den, ma_ctcb_chi_dao, y_kien, chuoi_ma_ctcb_nhan) {
    let result = false
    var form = new FormData();
    form.append("ma_chi_dao_kc", 0)
    form.append("ma_van_ban_den", parseInt(ma_van_ban_den))
    form.append("ma_ctcb_chi_dao", parseInt(ma_ctcb_chi_dao))
    form.append("y_kien", y_kien)
    form.append("chuoi_ma_ctcb_nhan", JSON.stringify(chuoi_ma_ctcb_nhan))
    
    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_CDDH_PCNVTUVBD,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_CDDH_PCNVTUVBD) {
            console.log("CONSOLE_AU_CDDH_PCNVTUVBD: " + JSON.stringify(response))
        }
        if (response.status === 200) {
            result = true
        } else {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_CDDH_TMCDDHTVBD) {
            console.log("CONSOLE_AU_CDDH_PCNVTUVBD ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_CDDH_PCNVTUVBD(ma_van_ban_den, ma_ctcb_chi_dao, y_kien, chuoi_ma_ctcb_nhan).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

export default {
    AU_CD_TAO_CDDH,
    AU_CDDH_PCNVTUVBD,
}