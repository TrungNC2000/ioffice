import { URL_CONFIG } from "./Config"
import { Platform } from "react-native"
import DeviceInfo from 'react-native-device-info'
import { Toast } from "native-base"
import axios from "axios"
import { ReplaceAll } from "../untils/TextUntils"
import { AppConfig } from "../AppConfig"
import API_Login from "./API_Login"

function showWarning(error) {
    Toast.show({ text: "Kết nối bị gián đoạn!", type: "warning", buttonText: "Đóng", position: "top", duration: 2000 })
    if (error) {
        SendLog(error)
    }
};

async function SendLog(message) {
    try {
        let obj = new Object
        let date = new Date()
        obj.Model = DeviceInfo.getModelSync()
        obj.OS = Platform.OS.toString()
        obj.SystemVersion = DeviceInfo.getSystemVersionSync()
        obj.AppVersion = DeviceInfo.getVersionSync()
        obj.CurrentTime = date.toLocaleDateString() + " " + date.toLocaleTimeString()
        obj.Module = "API_NhanTinSMS"
        obj.Domain = global.AU_ROOT ? global.AU_ROOT : ""
        delete message.headers
        obj.Message = message
        let text = ReplaceAll(JSON.stringify(obj), String.fromCharCode(34), "'")
        let url = AppConfig.AppDefaultLogURLSendText + text
        await axios({
            method: "POST",
            url: url,
            timeout: AppConfig.TIME_OUT_SHORT
        })
    } catch (error) {
        console.log("Send log error: " + error)
    }
}

function setHeaderToken() {
    return { "Authorization": "Bearer " + global.access_token }
};

function xoa_dau(str) {
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
    str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
    str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
    str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
    str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
    str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
    str = str.replace(/Đ/g, "D");
    return str;
}

async function AU_SMS_DSCBGTN(page, size) {
    let result = []
    var form = new FormData();
    form.append("ma_ctcb_gui", global.ma_ctcb_kc);
    form.append("ma_ctcb_nhan", 0);
    form.append("page", parseInt(page));
    form.append("size", parseInt(size));

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_SMS_DSCBGTN,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_SMS_DSCBGTN) {
            console.log("CONSOLE_AU_SMS_DSCBGTN: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_SMS_DSCBGTN) {
            console.log("CONSOLE_AU_SMS_DSCBGTN ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_SMS_DSCBGTN(page, size).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_SMS_DSCBNSMSBM(ma_sms_kc) {
    let result = []
    let url = global.AU_ROOT + URL_CONFIG.AU_SMS_DSCBNSMSBM
    url += "?ma_sms_kc=" + ma_sms_kc

    await axios({
        method: "GET",
        url: url,
        headers: setHeaderToken(),
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_SMS_DSCBNSMSBM(ma_sms_kc).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_SMS_XTNDGCCB(ma_sms_kc) {
    let result = false
    var form = new FormData();
    form.append("ma_sms_kc", parseInt(ma_sms_kc));

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_SMS_XTNDGCCB,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_SMS_XTNDGCCB) {
            console.log("CONSOLE_AU_SMS_XTNDGCCB: " + JSON.stringify(response))
        }
        if (response.data.message === "Thực thi thành công") {
            result = true
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_SMS_XTNDGCCB) {
            console.log("CONSOLE_AU_SMS_XTNDGCCB ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_SMS_XTNDGCCB(ma_sms_kc).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_SMS_DSDVGTN(page, size) {
    let result = []
    var form = new FormData();
    form.append("ma_ctcb_gui", global.ma_ctcb_kc);
    form.append("ma_ctcb_nhan", "0");
    form.append("ma_don_vi_quan_tri_gui", global.ma_don_vi_quan_tri);
    form.append("page", parseInt(page));
    form.append("size", parseInt(size));

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_SMS_DSDVGTN,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_SMS_DSDVGTN) {
            console.log("CONSOLE_AU_SMS_DSDVGTN: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_SMS_DSDVGTN) {
            console.log("CONSOLE_AU_SMS_DSDVGTN ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_SMS_DSDVGTN(page, size).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_SMS_DSCBNTTDH() {
    let result = []
    let url = global.AU_ROOT + URL_CONFIG.AU_SMS_DSCBNTTDH
    url += "?ma_don_vi=" + global.ma_don_vi
    url += "&ma_can_bo=" + global.ma_can_bo
    url += "&type=tree"

    await axios({
        method: "GET",
        url: url,
        headers: setHeaderToken(),
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_SMS_DSCBNTTDH) {
            console.log("CONSOLE_AU_SMS_DSCBNTTDH: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_SMS_DSCBNTTDH) {
            console.log("CONSOLE_AU_SMS_DSCBNTTDH ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_SMS_DSCBNTTDH().then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_SMS_CNCBTLN() {
    let result = []
    let url = global.AU_ROOT + URL_CONFIG.AU_SMS_CNCBTLN
    url += "?ma_can_bo=" + global.ma_can_bo
    url += "&ma_loai_nhom_nguoi_nhan=7"

    await axios({
        method: "GET",
        url: url,
        headers: setHeaderToken(),
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_SMS_CNCBTLN) {
            console.log("CONSOLE_AU_SMS_CNCBTLN: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_SMS_CNCBTLN) {
            console.log("CONSOLE_AU_SMS_CNCBTLN ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_SMS_CNCBTLN().then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_SMS_DSNCBNTN() {
    let result = []
    let url = global.AU_ROOT + URL_CONFIG.AU_SMS_DSNCBNTN
    url += "?ma_can_bo=" + global.ma_can_bo

    await axios({
        method: "GET",
        url: url,
        headers: setHeaderToken(),
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_SMS_DSNCBNTN) {
            console.log("CONSOLE_AU_SMS_DSNCBNTN: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_SMS_DSNCBNTN) {
            console.log("CONSOLE_AU_SMS_DSNCBNTN ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_SMS_DSNCBNTN().then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_SMS_STNSMS(noi_dung, chuoi_di_dong_nhan, chuoi_ma_ctcb_nhan, chuoi_trang_thai_thanh_cong) {
    let result = false
    var form = new FormData();
    form.append("ma_ctcb_gui", global.ma_ctcb_kc);
    form.append("ma_don_vi_quan_tri_gui", global.ma_don_vi_quan_tri);
    form.append("noi_dung", noi_dung);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_SMS_STNSMS,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(async (response) => {
        if (global.CONSOLE_AU_SMS_STNSMS) {
            console.log("CONSOLE_AU_SMS_STNSMS: " + JSON.stringify(response))
        }
        if (response.data.message === "Thực thi thành công") {
            var form1 = new FormData();
            form1.append("madonviquantri", global.ma_don_vi_quan_tri);
            let noidungGui = ""

            if (global.tn_mau_gui_sms == "0") {
                noidungGui = "<" + global.tien_to_tin_nhan + "> Người gửi: <" + global.ho_va_ten_can_bo + "(" + global.di_dong_can_bo + ")> " + noi_dung
            } else {
                if (global.chu_vu_cb_tin_nhan != "0") {
                    noidungGui = "[" + global.ho_va_ten_can_bo + " - " + global.ten_chuc_vu + " - " + global.ten_don_vi_quan_tri + "] " + noi_dung
                } else {
                    noidungGui = "<" + global.tien_to_tin_nhan + "> " + noi_dung
                }
            }
            if (global.loc_dau_tin_nhan != 0) {
                noidungGui = xoa_dau(noidungGui);
            }

            form1.append("noidung", noidungGui);
            form1.append("somay", chuoi_di_dong_nhan);

            await axios({
                method: "POST",
                url: global.AU_ROOT + URL_CONFIG.AU_SMS_GTN,
                headers: setHeaderToken(),
                data: form1,
                timeout: AppConfig.TIME_OUT_SHORT
            }).then(async (response1) => {
                if (response1.status === 200) {
                    var form2 = new FormData();
                    form2.append("chuoi_di_dong_nhan", chuoi_di_dong_nhan);
                    form2.append("chuoi_ma_ctcb_nhan", chuoi_ma_ctcb_nhan);
                    form2.append("chuoi_trang_thai_thanh_cong", chuoi_trang_thai_thanh_cong);
                    form2.append("ma_sms", response.data.id);

                    await axios({
                        method: "POST",
                        url: global.AU_ROOT + URL_CONFIG.AU_SMS_GTNSMS,
                        headers: setHeaderToken(),
                        data: form2,
                        timeout: AppConfig.TIME_OUT_SHORT
                    }).then(response2 => {
                        if (response2.data.message === "Thực thi thành công") {
                            result = true
                        }
                    }).catch(error => {
                        if (global.CONSOLE_AU_SMS_STNSMS) {
                            console.log("CONSOLE_AU_SMS_STNSMS ERROR: " + JSON.stringify(error))
                        }
                        showWarning(error)
                    })
                }
            }).catch(error => {
                if (global.CONSOLE_AU_SMS_STNSMS) {
                    console.log("CONSOLE_AU_SMS_STNSMS ERROR: " + JSON.stringify(error))
                }
                showWarning(error)
            })
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(error => {
        if (global.CONSOLE_AU_SMS_STNSMS) {
            console.log("CONSOLE_AU_SMS_STNSMS ERROR: " + JSON.stringify(error))
        }
        showWarning(error)
    })
    return result
}

async function AU_SMS_GTN(noi_dung, chuoi_di_dong_nhan) {
    let result = false
    var form = new FormData();
    form.append("madonviquantri", global.ma_don_vi_quan_tri);
    let noidungGui = ""
    if (global.tn_mau_gui_sms == "0") {
        noidungGui = "<" + global.tien_to_tin_nhan + "> Người gửi: <" + global.ho_va_ten_can_bo + "(" + global.di_dong_can_bo + ")> " + noi_dung
    } else {
        if (global.chu_vu_cb_tin_nhan != "0") {
            noidungGui = "[" + global.ho_va_ten_can_bo + " - " + global.ten_chuc_vu + " - " + global.ten_don_vi_quan_tri + "] " + noi_dung
        } else {
            noidungGui = "<" + global.tien_to_tin_nhan + "> " + noi_dung
        }
    }
    if (global.loc_dau_tin_nhan != 0) {
        noidungGui = xoa_dau(noidungGui);
    }

    form.append("noidung", noidungGui);
    form.append("somay", chuoi_di_dong_nhan);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_SMS_GTN,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(async (response) => {
        if (global.CONSOLE_AU_SMS_GTN) {
            console.log("CONSOLE_AU_SMS_GTN: " + JSON.stringify(response))
        }
        if (response.status === 200) {
            result = true
        } else {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_SMS_GTN) {
            console.log("CONSOLE_AU_SMS_GTN ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_SMS_GTN(noi_dung, chuoi_di_dong_nhan).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

export default {
    AU_SMS_DSCBGTN,
    AU_SMS_DSCBNSMSBM,
    AU_SMS_XTNDGCCB,
    AU_SMS_DSDVGTN,
    AU_SMS_DSCBNTTDH,
    AU_SMS_DSNCBNTN,
    AU_SMS_CNCBTLN,
    AU_SMS_STNSMS,
    AU_SMS_GTN

}