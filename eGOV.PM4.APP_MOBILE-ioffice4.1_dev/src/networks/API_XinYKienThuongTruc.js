import { URL_CONFIG } from "./Config"
import { Platform } from "react-native"
import DeviceInfo from 'react-native-device-info'
import { Toast } from "native-base"
import axios from "axios"
import { ReplaceAll } from "../untils/TextUntils"
import { AppConfig } from "../AppConfig"
import API_Login from "./API_Login"

function showWarning(error) {
    Toast.show({ text: "Kết nối bị gián đoạn!", type: "warning", buttonText: "Đóng", position: "top", duration: 2000 })
    if (error) {
        SendLog(error)
    }
};

async function SendLog(message) {
    try {
        let obj = new Object
        let date = new Date()
        obj.Model = DeviceInfo.getModelSync()
        obj.OS = Platform.OS.toString()
        obj.SystemVersion = DeviceInfo.getSystemVersionSync()
        obj.AppVersion = DeviceInfo.getVersionSync()
        obj.CurrentTime = date.toLocaleDateString() + " " + date.toLocaleTimeString()
        obj.Module = "API_XinYKienThuongTruc"
        obj.Domain = global.AU_ROOT ? global.AU_ROOT : ""
        delete message.headers
        obj.Message = message
        let text = ReplaceAll(JSON.stringify(obj), String.fromCharCode(34), "'")
        let url = AppConfig.AppDefaultLogURLSendText + text
        await axios({
            method: "POST",
            url: url,
            timeout: AppConfig.TIME_OUT_SHORT
        })
    } catch (error) {
        console.log("Send log error: " + error)
    }
}

function setHeaderToken() {
    return { "Authorization": "Bearer " + global.access_token }
};

async function AU_XYKTT_DA_GUI(ma_can_bo, page, size, tieu_de) {
    let result = []
    let form = new FormData()
    form.append("ma_can_bo", ma_can_bo)
    // form.append("nam", 0)
    // form.append("noi_dung", "")
    form.append("page", page)
    form.append("size", size)
    // form.append("gui_den_ngay", null)
    // form.append("gui_tu_ngay", null)
    form.append("tieu_de", tieu_de)

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_XYKTT_DA_GUI,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_XYKTT_DA_GUI) {
            console.log("CONSOLE_AU_XYKTT_DA_GUI: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_XYKTT_DA_GUI) {
            console.log("CONSOLE_AU_XYKTT_DA_GUI ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_XYKTT_DA_GUI(ma_can_bo, page, size, tieu_de).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_XYKTT_DA_NHAN(ma_can_bo, page, size, tieu_de, trang_thai_y_kien_can_bo) {
    let result = []
    let form = new FormData()
    form.append("ma_can_bo", ma_can_bo)
    form.append("page", page)
    form.append("size", size)
    form.append("tieu_de", tieu_de)
    form.append("trang_thai_y_kien_can_bo", trang_thai_y_kien_can_bo)

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_XYKTT_DA_NHAN,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_XYKTT_DA_NHAN) {
            console.log("CONSOLE_AU_XYKTT_DA_NHAN: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_XYKTT_DA_NHAN) {
            console.log("CONSOLE_AU_XYKTT_DA_NHAN ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_XYKTT_DA_NHAN(ma_can_bo, page, size, tieu_de).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_XYKTT_DSCBN(ma_can_bo, ma_xin_y_kien) {
    let result = []
    let form = new FormData()
    form.append("ma_can_bo", ma_can_bo);
    form.append("ma_xin_y_kien", ma_xin_y_kien);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_XYKTT_DSCBN,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_XYKTT_DSCBN) {
            console.log("CONSOLE_AU_XYKTT_DSCBN: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_XYKTT_DSCBN) {
            console.log("CONSOLE_AU_XYKTT_DSCBN ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_XYKTT_DSCBN(ma_can_bo, ma_xin_y_kien).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_XYKTT_CTXYKTT(ma_can_bo, ma_xin_y_kien) {
    let result = []
    let form = new FormData()
    form.append("ma_xin_y_kien", ma_xin_y_kien)
    form.append("ma_can_bo", ma_can_bo)

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_XYKTT_CTXYKTT,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_XYKTT_CTXYKTT) {
            console.log("CONSOLE_AU_XYKTT_CTXYKTT: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_XYKTT_CTXYKTT) {
            console.log("CONSOLE_AU_XYKTT_CTXYKTT ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_XYKTT_CTXYKTT(ma_can_bo, ma_xin_y_kien).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_XYKTT_DSBLCB(ma_can_bo, ma_xin_y_kien) {
    let result = []
    let form = new FormData();
    form.append("ma_can_bo", ma_can_bo);
    form.append("ma_xin_y_kien", ma_xin_y_kien);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_XYKTT_DSBLCB,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_XYKTT_DSBLCB) {
            console.log("CONSOLE_AU_XYKTT_DSBLCB: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_XYKTT_DSBLCB) {
            console.log("CONSOLE_AU_XYKTT_DSBLCB ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_XYKTT_DSBLCB(ma_can_bo, ma_xin_y_kien).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_XYKTT_YKTT_CREATE(chuoi_ma_ctcb_nhan, ma_ctcb_tao, ma_nhom_nguoi_nhan, noi_dung, src_file_xin_y_kien, tieu_de, phan_hoi_y_kien) {
    let result = []
    let form = new FormData();
    if (phan_hoi_y_kien > 0 ) {
        form.append("ma_ctcb_tao", ma_ctcb_tao)
        form.append("noi_dung", noi_dung)
        form.append("phan_hoi_y_kien", phan_hoi_y_kien)
    } else {
        form.append("chuoi_ma_ctcb_nhan", chuoi_ma_ctcb_nhan)
        form.append("ma_ctcb_tao", ma_ctcb_tao)
        form.append("ma_nhom_nguoi_nhan", ma_nhom_nguoi_nhan)
        form.append("noi_dung", noi_dung)
        form.append("src_file_xin_y_kien", src_file_xin_y_kien)
        form.append("tieu_de", tieu_de)
    }

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_XYKTT_YKTT_CREATE,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_XYKTT_YKTT_CREATE) {
            console.log("CONSOLE_AU_XYKTT_YKTT_CREATE: " + JSON.stringify(response))
        }
        if (response.data.message === "Thực thi thành công") {
            result = response.data.id
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_XYKTT_YKTT_CREATE) {
            console.log("CONSOLE_AU_XYKTT_YKTT_CREATE ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_XYKTT_YKTT_CREATE(ma_can_bo, ma_xin_y_kien).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_XYKTT_GPH(chuoi_ma_ctcb_nhan, ma_ctcb_tao, noi_dung, phan_hoi_y_kien) {
    let result = []
    let form = new FormData();
    form.append("chuoi_ma_ctcb_nhan", chuoi_ma_ctcb_nhan);
    form.append("ma_ctcb_tao", ma_ctcb_tao);
    form.append("noi_dung", noi_dung);
    form.append("phan_hoi_y_kien", phan_hoi_y_kien);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_XYKTT_GPH,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_XYKTT_GPH) {
            console.log("CONSOLE_AU_XYKTT_GPH: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_XYKTT_GPH) {
            console.log("CONSOLE_AU_XYKTT_GPH ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_XYKTT_GPH(ma_can_bo, ma_xin_y_kien).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_XYKTT_YKTT_DSNCB(ma_don_vi, ma_can_bo) {
    let result = []
    let form = new FormData();
    form.append("ma_don_vi", ma_don_vi);
    form.append("ma_can_bo", ma_can_bo);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_XYKTT_YKTT_DSNCB,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_XYKTT_YKTT_DSNCB) {
            console.log("CONSOLE_AU_XYKTT_YKTT_DSNCB: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_XYKTT_YKTT_DSNCB) {
            console.log("CONSOLE_AU_XYKTT_YKTT_DSNCB ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_XYKTT_YKTT_DSNCB(ma_don_vi, ma_can_bo).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_XYKTT_YKTT_DSCBTN(ma_nhom_nguoi_nhan) {
    let result = []
    await axios({
        method: "GET",
        url: global.AU_ROOT + URL_CONFIG.AU_XYKTT_YKTT_DSCBTN + "?ma_nhom_nguoi_nhan=" + ma_nhom_nguoi_nhan,
        headers: setHeaderToken(),
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_XYKTT_YKTT_DSCBTN) {
            console.log("CONSOLE_AU_XYKTT_YKTT_DSCBTN: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_XYKTT_YKTT_DSCBTN) {
            console.log("CONSOLE_AU_XYKTT_YKTT_DSCBTN ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_XYKTT_YKTT_DSCBTN(ma_nhom_nguoi_nhan).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_XYKTT_DSNTDV(ma_don_vi) {
    let result = []
    await axios({
        method: "GET",
        url: global.AU_ROOT + URL_CONFIG.AU_XYKTT_DSNTDV + "?ma_don_vi=" + ma_don_vi,
        headers: setHeaderToken(),
        // data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_XYKTT_DSNTDV) {
            console.log("CONSOLE_AU_XYKTT_DSNTDV: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_XYKTT_DSNTDV) {
            console.log("CONSOLE_AU_XYKTT_DSNTDV ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_XYKTT_DSNTDV(ma_don_vi).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_XYKTT_DSCBTN(ma_nhom_nguoi_nhan) {
    let result = []

    await axios({
        method: "GET",
        url: global.AU_ROOT + URL_CONFIG.AU_XYKTT_DSCBTN + "?ma_nhom_nguoi_nhan=" + ma_nhom_nguoi_nhan,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_XYKTT_DSCBTN) {
            console.log("CONSOLE_AU_XYKTT_DSCBTN: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_XYKTT_DSCBTN) {
            console.log("CONSOLE_AU_XYKTT_DSCBTN ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_XYKTT_DSCBTN(ma_nhom_nguoi_nhan).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_XYKTT_YKTT_DELETE(ma_xin_y_kien_kc, trang_thai_xin_y_kien) {
    let result = []
    let form = new FormData();
    form.append("ma_xin_y_kien_kc", ma_xin_y_kien_kc);
    form.append("trang_thai_xin_y_kien", trang_thai_xin_y_kien);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_XYKTT_YKTT_DELETE,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_XYKTT_YKTT_DELETE) {
            console.log("CONSOLE_AU_XYKTT_YKTT_DELETE: " + JSON.stringify(response))
        }
        if (response.data.message === "Thực thi thành công") {
            result = response.data
        } else  {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_XYKTT_YKTT_DELETE) {
            console.log("CONSOLE_AU_XYKTT_YKTT_DELETE ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_XYKTT_YKTT_DELETE(ma_don_vi, ma_can_bo).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

export default {
    AU_XYKTT_DA_GUI,
    AU_XYKTT_DA_NHAN,
    AU_XYKTT_DSCBN,
    AU_XYKTT_CTXYKTT,
    AU_XYKTT_DSBLCB,
    AU_XYKTT_YKTT_CREATE,
    AU_XYKTT_GPH,
    AU_XYKTT_YKTT_DSNCB,
    AU_XYKTT_YKTT_DSCBTN,
    AU_XYKTT_DSNTDV,
    AU_XYKTT_DSCBTN,
    AU_XYKTT_YKTT_DELETE
}