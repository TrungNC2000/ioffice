import Login from "./API_Login"
import VanBanDen from "./API_VanBanDen"
import VanBanDi from "./API_VanBanDi"
import VanBanNoiBo from "./API_VanBanNoiBo"
import ThongTinDieuHanh from "./API_ThongTinDieuHanh"
import XinYKienThuongTruc from './API_XinYKienThuongTruc'
import ChiDaoDieuHanh from "./API_ChiDaoDieuHanh"
import LichCongTac from "./API_LichCongTac"
import LichHop from "./API_LichHop"
import NhanTinSMS from "./API_NhanTinSMS"
import FILE from "./API_FILE"
import KYSO from "./API_KYSO"

export default {
    Login,
    VanBanDen,
    VanBanDi,
    VanBanNoiBo,
    ThongTinDieuHanh,
    XinYKienThuongTruc,
    ChiDaoDieuHanh,
    LichCongTac,
    LichHop,
    NhanTinSMS,
    FILE,
    KYSO
}