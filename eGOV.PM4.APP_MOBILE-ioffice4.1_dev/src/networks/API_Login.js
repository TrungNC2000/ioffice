import { URL_CONFIG } from "./Config"
import { Alert, AsyncStorage, Platform } from "react-native"
import DeviceInfo from 'react-native-device-info'
import { Toast } from "native-base"
import axios from "axios"
import { AppConfig } from "../AppConfig"
import { GetNowTime, ReplaceAll, checkVersionWeb, formatDate, getNVNew } from "../untils/TextUntils"
import moment from "moment"

function showWarning(error) {
    Toast.show({ text: "Kết nối bị gián đoạn!", type: "warning", buttonText: "Đóng", position: "top", duration: 2000 })
    if (error) {
        SendLog(error)
    }
};

async function SendLog(message) {
    try {
        let obj = new Object
        let date = new Date()
        obj.Model = DeviceInfo.getModelSync()
        obj.OS = Platform.OS.toString()
        obj.SystemVersion = DeviceInfo.getSystemVersionSync()
        obj.AppVersion = DeviceInfo.getVersionSync()
        obj.CurrentTime = date.toLocaleDateString() + " " + date.toLocaleTimeString()
        obj.Module = "API_Login"
        obj.Domain = global.AU_ROOT ? global.AU_ROOT : ""
        delete message.headers
        obj.Message = message
        let text = ReplaceAll(JSON.stringify(obj), String.fromCharCode(34), "'")
        let url = AppConfig.AppDefaultLogURLSendText + text
        await axios({
            method: "POST",
            url: url,
            timeout: AppConfig.TIME_OUT_SHORT
        })
    } catch (error) {
        console.log("Send log error: " + error)
    }
}

function setHeaderToken() {
    return { "Authorization": "Bearer " + global.access_token }
};

async function AU_DSDVA() {
    let result = []
    await axios({
        method: "GET",
        url: URL_CONFIG.AU_DANH_SACH_APP + URL_CONFIG.AU_DSDVA,
        timeout: 5000
    }).then(response => {
        if (global.CONSOLE_AU_DSDVA) {
            console.log("CONSOLE_AU_DSDVA: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(error => {
        console.log("CONSOLE_AU_DSDVA ERROR: " + JSON.stringify(error))
    })
    return result
}

async function AU_DSCTCB(username, password, ma_ctcb_kiem_nhiem, dispatch) {
    let result = false
    let form = new FormData()
    form.append("username", username)
    form.append("password", password)

    try {
        await axios({
            method: "POST",
            url: global.AU_ROOT + URL_CONFIG.AU_DSCTCB,
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            data: form,
            timeout: AppConfig.TIME_OUT_SHORT
        }).then(async (thongtincb) => {
            if (global.CONSOLE_AU_DSDVA) {
                console.log("CONSOLE_AU_DSDVA: " + JSON.stringify(response))
            }
            if (thongtincb.data.message === "Lấy dữ liệu thành công") {
                dispatch({ type: "updateDSCTCB_ALL", dataAll: thongtincb.data.data })
                AsyncStorage.setItem("AU_DSCTCB", JSON.stringify(thongtincb.data.data))
                let curIndex = 0
                if (result !== undefined) {
                    thongtincb.data.data.forEach((element, index) => {
                        if (element.ma_ctcb_kc.toString() === ma_ctcb_kiem_nhiem) {
                            curIndex = index
                        }
                    });
                }
                dispatch({ type: "updateDSCTCB", data: thongtincb.data.data[curIndex] })
                let ma_ctcb_kc = thongtincb.data.data[curIndex].ma_ctcb_kc
                global.ma_ctcb_kc = ma_ctcb_kc
                global.ma_can_bo = thongtincb.data.data[curIndex].ma_can_bo
                global.username = thongtincb.data.data[curIndex].username
                global.ma_don_vi = thongtincb.data.data[curIndex].ma_don_vi
                global.ten_don_vi = thongtincb.data.data[curIndex].ten_don_vi
                global.ho_va_ten_can_bo = thongtincb.data.data[curIndex].ho_va_ten_can_bo
                global.ten_chuc_vu = thongtincb.data.data[curIndex].ten_chuc_vu
                global.di_dong_can_bo = thongtincb.data.data[curIndex].di_dong_can_bo
                global.ngay_dang_ky_otp = thongtincb.data.data[curIndex].ngay_dang_ky_otp
                global.loai_ky_so_sim = thongtincb.data.data[curIndex].loai_ky_so_sim

                await AU_ACCESS_TOKEN(thongtincb.data.data[curIndex].refresh_token).then(async () => {
                    setTimeout(() => {
                        AU_FCM(ma_ctcb_kc, global.fcmToken)
                    }, 0);

                    setTimeout(() => {
                        AU_DSDVCQ(ma_ctcb_kc)
                    }, 0);

                    await AU_DSTSHTTCTCB(ma_ctcb_kc).then(async () => {
                        await AU_DSNVCB(ma_ctcb_kc, dispatch).then(async () => {
                            await AU_KTQCVLDVTCCB(ma_ctcb_kc).then(() => {
                                result = true
                            })
                        })
                    })
                })
            }
        })
    } catch (error) {
        if (global.CONSOLE_AU_DSDVA) {
            console.log("CONSOLE_AU_DSDVA ERROR: " + JSON.stringify(error))
        }
        console.log("CONSOLE_AU_DSDVA: " + JSON.stringify(error))
        showWarning(error)
    }
    return result
}

async function AU_DSCTCB_SSO(access_token_sso_ybi, domain_sso_ybi, ma_ctcb_kiem_nhiem, dispatch) {
    let result = false
    let form = new FormData()
    form.append("access_token_sso_ybi", access_token_sso_ybi)
    form.append("domain_sso_ybi", domain_sso_ybi)
    try {   
        await axios({
            method: "POST",
            url: global.AU_ROOT + URL_CONFIG.AU_DSCTCB,
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            data: form,
            timeout: AppConfig.TIME_OUT_SHORT
        }).then(async (thongtincb) => {
            if (global.CONSOLE_AU_DSDVA) {
                console.log("CONSOLE_AU_DSDVA: " + JSON.stringify(response))
            }
            if (thongtincb.data.message === "Lấy dữ liệu thành công") {
                dispatch({ type: "updateDSCTCB_ALL", dataAll: thongtincb.data.data })
                AsyncStorage.setItem("AU_DSCTCB", JSON.stringify(thongtincb.data.data))
                let curIndex = 0
                if (result !== undefined) {
                    thongtincb.data.data.forEach((element, index) => {
                        if (element.ma_ctcb_kc.toString() === ma_ctcb_kiem_nhiem) {
                            curIndex = index
                        }
                    });
                }
                dispatch({ type: "updateDSCTCB", data: thongtincb.data.data[curIndex] })
                let ma_ctcb_kc = thongtincb.data.data[curIndex].ma_ctcb_kc
                global.ma_ctcb_kc = ma_ctcb_kc
                global.ma_can_bo = thongtincb.data.data[curIndex].ma_can_bo
                global.username = thongtincb.data.data[curIndex].username
                global.ma_don_vi = thongtincb.data.data[curIndex].ma_don_vi
                global.ten_don_vi = thongtincb.data.data[curIndex].ten_don_vi
                global.ho_va_ten_can_bo = thongtincb.data.data[curIndex].ho_va_ten_can_bo
                global.ten_chuc_vu = thongtincb.data.data[curIndex].ten_chuc_vu
                global.di_dong_can_bo = thongtincb.data.data[curIndex].di_dong_can_bo
                global.ngay_dang_ky_otp = thongtincb.data.data[curIndex].ngay_dang_ky_otp
                global.loai_ky_so_sim = thongtincb.data.data[curIndex].loai_ky_so_sim

                await AU_ACCESS_TOKEN(thongtincb.data.data[curIndex].refresh_token).then(async () => {
                    setTimeout(() => {
                        AU_FCM(ma_ctcb_kc, global.fcmToken)
                    }, 0);

                    setTimeout(() => {
                        AU_DSDVCQ(ma_ctcb_kc)
                    }, 0);

                    await AU_DSTSHTTCTCB(ma_ctcb_kc).then(async () => {
                        await AU_DSNVCB(ma_ctcb_kc, dispatch).then(async () => {
                            await AU_KTQCVLDVTCCB(ma_ctcb_kc).then(() => {
                                result = true
                            })
                        })
                    })
                })
            }
        })
    } catch (error) {
        if (global.CONSOLE_AU_DSDVA) {
            console.log("CONSOLE_AU_DSDVA ERROR: " + JSON.stringify(error))
        }
        showWarning(error)
    }
    return result
}

async function AU_DSCTCB1(dispatch, listDonVi) {
    let result = false
    try {
        await AsyncStorage.getItem("Url_Api_SaveLogin", (error, result) => {
            global.AU_ROOT = result
        }).then(async () => {
            await AsyncStorage.getItem("web_version", (error, result) => {
                let arrTemp = listDonVi.filter(el => el.url_api === global.AU_ROOT)
                let web_version = result
                if (arrTemp.length > 0) {
                    web_version = arrTemp[0].version.split(".")[2]
                }
                global.web_version = web_version
            })
            await AsyncStorage.getItem("TaiKhoan", (error, result) => {
                global.TaiKhoan = result
            })

            await AsyncStorage.getItem("MatKhau", (error, result) => {
                global.MatKhau = result
            })

            let ma_ctcb_kiem_nhiem = await AsyncStorage.getItem("ma_ctcb_kc_Store")
            let thongtincb = await AsyncStorage.getItem("AU_DSCTCB")
            thongtincb = JSON.parse(thongtincb)

            dispatch({ type: "updateDSCTCB_ALL", dataAll: thongtincb })
            let curIndex = 0
            if (result !== undefined) {
                thongtincb.forEach((element, index) => {
                    if (element.ma_ctcb_kc.toString() === ma_ctcb_kiem_nhiem) {
                        curIndex = index
                    }
                });
            }

            dispatch({ type: "updateDSCTCB", data: thongtincb[curIndex] })
            let ma_ctcb_kc = thongtincb[curIndex].ma_ctcb_kc
            global.ma_ctcb_kc = ma_ctcb_kc
            global.ma_can_bo = thongtincb[curIndex].ma_can_bo
            global.username = thongtincb[curIndex].username
            global.ma_don_vi = thongtincb[curIndex].ma_don_vi
            global.ten_don_vi = thongtincb[curIndex].ten_don_vi
            global.ho_va_ten_can_bo = thongtincb[curIndex].ho_va_ten_can_bo
            global.ten_chuc_vu = thongtincb[curIndex].ten_chuc_vu
            global.di_dong_can_bo = thongtincb[curIndex].di_dong_can_bo
            global.ngay_dang_ky_otp = thongtincb[curIndex].ngay_dang_ky_otp
            global.loai_ky_so_sim = thongtincb[curIndex].loai_ky_so_sim
            await AU_ACCESS_TOKEN(thongtincb[curIndex].refresh_token).then(async () => {
                await AU_DSDVCQ(ma_ctcb_kc).then(async () => {
                    await AU_DSTSHTTCTCB(ma_ctcb_kc).then(async () => {
                        await AU_DSNVCB(ma_ctcb_kc, dispatch).then(async () => {
                            await AU_KTQCVLDVTCCB(ma_ctcb_kc).then(() => {
                                result = true
                            })
                        })
                    })
                })


            })
        })
    } catch (error) {
        showWarning(error)
    }
    return result
}

async function AU_ACCESS_TOKEN(refresh_token) {
    let result = ""
    let url = global.AU_ROOT + URL_CONFIG.AU_ACCESS_TOKEN
    url += "?refresh_token=" + refresh_token
    await axios({
        method: "GET",
        url: url,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_ACCESS_TOKEN) {
            console.log("CONSOLE_AU_ACCESS_TOKEN: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy chuỗi quyền thành công") {
            global.access_token = response.data.data.access_token
            global.refresh_token = response.data.data.refresh_token
            AsyncStorage.setItem("SessionTime", GetNowTime())
            result = response.data.data.access_token
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(error => {
        if (global.CONSOLE_AU_ACCESS_TOKEN) {
            console.log("CONSOLE_AU_ACCESS_TOKEN ERROR: " + JSON.stringify(error))
        }
        showWarning(error)
    })
    return result
}

async function AU_DSDVCQ(ma_ctcb) {
    let result = false
    var form = new Object();
    form.ma_ctcb = parseInt(ma_ctcb)
    await axios({
        method: "GET",
        url: global.AU_ROOT + URL_CONFIG.AU_DSDVCQ,
        headers: setHeaderToken(),
        params: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_DSDVCQ) {
            console.log("CONSOLE_AU_DSDVCQ: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            response = response.data.data.filter(item => item.la_dv_dinh_danh === 1)
            if (response.length > 0) {
                global.ma_don_vi_quan_tri = response[0].ma_don_vi_kc
                global.ten_don_vi_quan_tri = response[0].ten_don_vi
                global.ma_dinh_danh_quan_tri = response[0].ma_dinh_danh
                let domain_file = response[0].domain_file ? response[0].domain_file : "|"
                global.AU_ROOT_FILE = domain_file.split("|")[0]
                global.FILE_PARTITION = domain_file.split("|")[1]
                result = true
            } else {
                result = false
            }
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(error => {
        if (global.CONSOLE_AU_DSDVCQ) {
            console.log("CONSOLE_AU_DSDVCQ: " + JSON.stringify(response))
        }
        showWarning(error)
    })
    return result
}

async function AU_KTQCVLDVTCCB(ma_ctcb) {
    let result = false
    var form = new Object();
    form.ma_ctcb = parseInt(ma_ctcb)

    await axios({
        method: "GET",
        url: global.AU_ROOT + URL_CONFIG.AU_KTQCVLDVTCCB,
        headers: setHeaderToken(),
        params: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (response.data.message === "Thực thi thành công") {
            if (global.CONSOLE_AU_KTQCVLDVTCCB) {
                console.log("CONSOLE_AU_KTQCVLDVTCCB: " + JSON.stringify(response))
            }
            global.lanh_dao = response.data.lanh_dao
            global.chuyen_vien = response.data.lanh_dao === 1 ? 0 : 1
            global.van_thu = response.data.van_thu
            global.phap_che = response.data.phap_che
            result = true
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(error => {
        if (global.CONSOLE_AU_KTQCVLDVTCCB) {
            console.log("CONSOLE_AU_KTQCVLDVTCCB ERROR: " + JSON.stringify(error))
        }
        showWarning(error)
    })
    return result
}

async function AU_DSNVCB(ma_ctcb, dispatch) {
    if (checkVersionWeb(21)) {
        let result = false
        let today = new Date()
        den_ngay = formatDate(today)
        let tu_ngay = ""
        if (global.ht_chon_tgian_nhac_viec === "all" || global.ht_chon_tgian_nhac_viec === "hide") {
            tu_ngay = "01/01/2000"
        } else {
            let lengthMonth = global.ht_chon_tgian_nhac_viec.substring(0, 1)
            tu_ngay = moment(den_ngay, "DD/MM/YYYY").subtract(parseInt(lengthMonth), "months").format("DD/MM/YYYY")
        }

        let url = global.AU_ROOT + URL_CONFIG.AU_NVDB
        url += "?ma_ctcb=" + ma_ctcb
        url += "&tu_ngay=" + tu_ngay
        url += "&den_ngay=" + den_ngay
        console.log('this is dsnvcb'+url);
        console.log('token'+setHeaderToken());
        await axios({
            method: "GET",
            url: url,
            headers: setHeaderToken(),
            timeout: AppConfig.TIME_OUT_SHORT
        }).then(response => {
            if (global.CONSOLE_AU_NVDB) {
                console.log("CONSOLE_AU_NVDB: " + JSON.stringify(response))
            }
            console.log("CONSOLE_AU_NVDB: " + JSON.stringify(response))
            if (response.status === 200) {
                dispatch({ type: "updateNhacViec", data: response.data })
                result = true
            } else {
                SendLog(response)
            }
        }).catch(async error => {
            if (global.CONSOLE_AU_NVDB) {
                console.log("CONSOLE_AU_NVDB ERROR: " + JSON.stringify(error))
            }
            if (error.response.status == 401) {
                await AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                    await AU_DSNVCB(ma_ctcb, dispatch).then(async (resCallback) => {
                        result = resCallback
                    })
                })
            }
        })
        return result
    } else {
        let result = false
        var form = new Object();
        form.ma_ctcb = parseInt(ma_ctcb)
        await axios({
            method: "GET",
            url: global.AU_ROOT + URL_CONFIG.AU_DSNVCB,
            headers: setHeaderToken(),
            params: form,
            timeout: AppConfig.TIME_OUT_SHORT
        }).then(response => {
            if (global.CONSOLE_AU_DSNVCB) {
                console.log("CONSOLE_AU_DSNVCB: " + JSON.stringify(response))
            }
            console.log("CONSOLE_AU_DSNVCB: " + JSON.stringify(response))
            if (response.data.message === "Lấy dữ liệu thành công") {
                dispatch({ type: "updateNhacViec", data: response.data.data })
                result = true
            } else if (!response.data.success) {
                SendLog(response)
            }
        }).catch(async error => {
            if (global.CONSOLE_AU_DSNVCB) {
                console.log("CONSOLE_AU_DSNVCB ERROR: " + JSON.stringify(error))
            }
            if (error.response.status == 401) {
                await AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                    await AU_DSNVCB(ma_ctcb, dispatch).then(async (resCallback) => {
                        result = resCallback
                    })
                })
            }
        })
        return result
    }
}

async function AU_FCM(ma_ctcb, Token) {
    var form = new FormData();
    form.append("ma_ctcb", ma_ctcb.toString());
    form.append("Token", Token);
    form.append("url_api", global.AU_ROOT);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_FCM,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_FCM) {
            console.log("CONSOLE_AU_FCM: " + JSON.stringify(response))
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_FCM) {
            console.log("CONSOLE_AU_FCM ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_FCM(ma_ctcb, Token).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
        SendLog(error)
    })
}

async function AU_PUSH_FCM(chuoi_ma_ctcb_nhan, title, body) {
    let result = []
    var form = new FormData();
    form.append("chuoi_ma_ctcb_nhan", chuoi_ma_ctcb_nhan);
    form.append("title", title);
    form.append("body", body);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_PUSH_FCM,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_PUSH_FCM) {
            console.log("CONSOLE_AU_PUSH_FCM: " + JSON.stringify(response))
        }
        if (response.status === 200) {
            result = response.data
        } else {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_PUSH_FCM) {
            console.log("CONSOLE_AU_PUSH_FCM ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_PUSH_FCM(chuoi_ma_ctcb_nhan, title, body).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
        SendLog(error)
    })
    return result
}

async function AU_PUSH_FCM_APP(chuoi_ma_ctcb_nhan, title, body, ma_ttdh, ma_vbnb_kc, ma_van_ban_di_kc, ma_van_ban_den_kc) {
    let result = false
    let url = global.AU_ROOT + URL_CONFIG.AU_PUSH_FCM_APP
    url += "?chuoi_ma_ctcb_nhan=" + chuoi_ma_ctcb_nhan
    url += "&title=" + title
    url += "&body=" + body
    url += "&ma_ttdh=" + ma_ttdh
    url += "&ma_vbnb_kc=" + ma_vbnb_kc
    url += "&ma_van_ban_di_kc=" + ma_van_ban_di_kc
    url += "&ma_van_ban_den_kc=" + ma_van_ban_den_kc
    console.log(url)

    if (global.vbdi_gui_thong_bao_app_pto == 0 && title !== "vanbandichoduyet") { return false }

    if (global.vbde_gui_thong_bao_app_pto == 0 && (title === "vanbandenchoxuly" || title === "vanbandenchoduyet")) { return false }

    await axios({
        method: "POST",
        url: url,
        headers: setHeaderToken(),
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_PUSH_FCM_APP) {
            console.log("CONSOLE_AU_PUSH_FCM_APP: " + JSON.stringify(response))
        }
        if (response.status === 200) {
            result = true
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_PUSH_FCM_APP) {
            console.log("CONSOLE_AU_PUSH_FCM_APP ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_PUSH_FCM_APP(chuoi_ma_ctcb_nhan, title, body, ma_ttdh, ma_vbnb_kc, ma_van_ban_di_kc, ma_van_ban_den_kc).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_DEL_FCM() {
    let result = false
    var form = new FormData();
    form.append("ma_ctcb", global.ma_ctcb_kc.toString());
    form.append("Token", global.fcmToken);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_DEL_FCM,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_DEL_FCM) {
            console.log("CONSOLE_AU_DEL_FCM: " + JSON.stringify(response))
        }
        if (response.data.message === "Thực thi thành công") {
            result = true
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_DEL_FCM) {
            console.log("CONSOLE_AU_DEL_FCM ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_DEL_FCM().then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_DSCBBID(refresh_token) {
    let result = {}
    var form = new Object();
    form.refresh_token = refresh_token
    await axios({
        method: "GET",
        url: global.AU_ROOT + URL_CONFIG.AU_DSCBBID,
        headers: setHeaderToken(),
        params: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_DSCBBID) {
            console.log("CONSOLE_AU_DSCBBID: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(error => {
        if (global.CONSOLE_AU_DSCBBID) {
            console.log("CONSOLE_AU_DSCBBID ERROR: " + JSON.stringify(error))
        }
        showWarning(error)
    })
    return result
}

async function AU_KTTMKC(ma_can_bo, password) {
    let result = {}
    var form = new FormData();
    form.append("ma_can_bo", parseInt(ma_can_bo));
    form.append("password", password);
    form.append("RESULT_OUT", 0);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_KTTMKC,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_KTTMKC) {
            console.log("CONSOLE_AU_KTTMKC: " + JSON.stringify(response))
        }
        if (response.status === 200) {
            result = response.data
        } else {
            showWarning(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_KTTMKC) {
            console.log("CONSOLE_AU_KTTMKC ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_KTTMKC(ma_can_bo, password).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
        showWarning(error)
    })
    return result
}

async function AU_CNMK(macanbo, matkhaucu, matkhaumoi) {
    let result = {}
    var form = new FormData();
    form.append("macanbo", parseInt(macanbo));
    form.append("matkhaucu", matkhaucu);
    form.append("nhaplaimatkhaumoi", matkhaumoi);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_CNMK,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_CNMK) {
            console.log("CONSOLE_AU_CNMK: " + JSON.stringify(response))
        }
        if (response.status === 200) {
            result = response.data
        } else {
            showWarning(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_CNMK) {
            console.log("CONSOLE_AU_CNMK ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_CNMK(macanbo, matkhaucu, matkhaumoi).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_CCBTCDB() {
    let result = []
    let url = global.AU_ROOT + URL_CONFIG.AU_CCBTCDB
    url += "?ma_don_vi=" + global.ma_don_vi_quan_tri
    url += "&ma_can_bo=" + global.ma_can_bo

    await axios({
        method: "GET",
        url: url,
        headers: setHeaderToken(),
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_CCBTCDB) {
            console.log("CONSOLE_AU_CCBTCDB: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_CCBTCDB) {
            console.log("CONSOLE_AU_CCBTCDB ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_CCBTCDB().then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_DSTSHTTCTCB(ma_ctcb_kc) {
    let result = false
    let url = global.AU_ROOT + URL_CONFIG.AU_DSTSHTTCTCB
    url += "?ma_ctcb=" + ma_ctcb_kc

    await axios({
        method: "GET",
        url: url,
        headers: setHeaderToken(),
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_DSTSHTTCTCB) {
            console.log("CONSOLE_AU_DSTSHTTCTCB: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            let thamsos = response.data.data
            let arrht_chon_tgian_nhac_viec = thamsos.filter(data => data.ma_tham_so === "ht_chon_tgian_nhac_viec")
            let arrcks_vnpt_mobile_pki = thamsos.filter(data => data.ma_tham_so === "cks_vnpt_mobile_pki")
            let arrcks_mobile_pki = thamsos.filter(data => data.ma_tham_so === "cks_mobile_pki")
            let arrtn_mau_gui_sms = thamsos.filter(data => data.ma_tham_so === "tn_mau_gui_sms")
            let arrloc_dau_tin_nhan = thamsos.filter(data => data.ma_tham_so === "loc_dau_tin_nhan")
            let arrrchu_vu_cb_tin_nhan = thamsos.filter(data => data.ma_tham_so === "chu_vu_cb_tin_nhan")
            let arrtien_to_tin_nhan = thamsos.filter(data => data.ma_tham_so === "tien_to_tin_nhan")
            let arrlct_co_quan_hien_thi_ds_co_quan = thamsos.filter(data => data.ma_tham_so === "lct_co_quan_hien_thi_ds_co_quan")
            let arrvbde_duyet_so_ngay_hxl = thamsos.filter(data => data.ma_tham_so === "vbde_duyet_so_ngay_hxl")
            let arrvbde_cv_xu_ly_hxl_de_mac_dinh_ld_pto = thamsos.filter(data => data.ma_tham_so === "vbde_cv_xu_ly_hxl_de_mac_dinh_ld_pto")
            let arrvai_tro_xdb_duoc_chuyen_tiep = thamsos.filter(data => data.ma_tham_so === "vai_tro_xdb_duoc_chuyen_tiep")
            let arrvbde_xdb_chuyen_tiep_pto = thamsos.filter(data => data.ma_tham_so === "vbde_xdb_chuyen_tiep_pto")
            let arrlct_co_duyet_lich_cong_tac = thamsos.filter(data => data.ma_tham_so === "lct_co_duyet_lich_cong_tac")
            let arrkyso_no_signhub = thamsos.filter(data => data.ma_tham_so === "kyso_no_signhub")
            let arrvbde_xlc_phai_chuyen_ld_pto = thamsos.filter(data => data.ma_tham_so === "vbde_xlc_phai_chuyen_ld_pto")
            let arrvbdi_gui_thong_bao_app_pto = thamsos.filter(data => data.ma_tham_so === "vbdi_gui_thong_bao_app_pto")
            let arrvbde_gui_thong_bao_app_pto = thamsos.filter(data => data.ma_tham_so === "vbde_gui_thong_bao_app_pto")
            let arrcks_hien_thi_nut_ky_so_hsm = thamsos.filter(data => data.ma_tham_so === "cks_hien_thi_nut_ky_so_hsm")
            let arrlct_tuan_trong_nam_kieu_cu = thamsos.filter(data => data.ma_tham_so === "lct_tuan_trong_nam_kieu_cu")
            let arrlct_ngay_bat_dau_lam_viec = thamsos.filter(data => data.ma_tham_so === "lct_ngay_bat_dau_lam_viec")
            let arrcks_xoa_file_sau_khi_ky_so = thamsos.filter(data => data.ma_tham_so === "cks_xoa_file_sau_khi_ky_so")
            let arrgd_thu_tu_tab_cbdv = thamsos.filter(data => data.ma_tham_so === "gd_thu_tu_tab_cbdv")
            let arrcks_smartca_hien_thi_nut_ky = thamsos.filter(data => data.ma_tham_so === "cks_smartca_hien_thi_nut_ky")
            let arrnv_nhac_viec_cmu = thamsos.filter(data => data.ma_tham_so === "ht_nhac_viec_cmu")
            let arrvbde_cv_theodoi_vb_mobile = thamsos.filter(data => data.ma_tham_so === "vbde_cv_theodoi_vb_mobile")
            let arrcd_tao_chi_dao_khi_duyet_vbde = thamsos.filter(data => data.ma_tham_so === "cd_tao_chi_dao_khi_duyet_vbde")
            let arrvbdi_chuyen_thuong_truc_ky = thamsos.filter(data => data.ma_tham_so === "vbdi_chuyen_thuong_truc_ky")
            let arrvbnb_hien_thi_vb_lien_quan_mobile = thamsos.filter(data => data.ma_tham_so === "vbnb_hien_thi_vb_lien_quan_mobile")
            let arrcd_chuyen_tiep_cddh_song_song_vbde = thamsos.filter(data => data.ma_tham_so === "cd_chuyen_tiep_cddh_song_song_vbde")
            let arrcd_cks_hien_thi_thong_tin_app_mobile = thamsos.filter(data => data.ma_tham_so === "cks_hien_thi_thong_tin_app_mobile")
            let arrcks_smartca_url = thamsos.filter(data => data.ma_tham_so === "cks_smartca_url")
            let arrcks_smartca_client_id = thamsos.filter(data => data.ma_tham_so === "cks_smartca_client_id")
            let arrcks_smartca_client_secret = thamsos.filter(data => data.ma_tham_so === "cks_smartca_client_secret")
            let arrcks_smartca_callback = thamsos.filter(data => data.ma_tham_so === "cks_smartca_callback")
            let arrvbde_gop_chung_vb_dien_tu = thamsos.filter(data => data.ma_tham_so === "vbde_gop_chung_vb_dien_tu")
            let arrkhoang_thoi_gian_danh_sach_vb = thamsos.filter(data => data.ma_tham_so === "khoang_thoi_gian_danh_sach_vb")
            let arruser_edoc = thamsos.filter(data => data.ma_tham_so === "user_edoc")
            let arrpass_edoc = thamsos.filter(data => data.ma_tham_so === "pass_edoc")
            let arrpub_key_edoc = thamsos.filter(data => data.ma_tham_so === "pub_key_edoc")
            let arrpri_key_edoc = thamsos.filter(data => data.ma_tham_so === "pri_key_edoc")
            let arrvbdi_duyet_vb_chuyen_phap_che = thamsos.filter(data => data.ma_tham_so === "vbdi_duyet_vb_chuyen_phap_che")
            let arrvbdi_cho_xl_chuyen_phap_che = thamsos.filter(data => data.ma_tham_so === "vbdi_cho_xl_chuyen_phap_che")
            let arrvbdi_an_hien_vblq_ngoai_he_thong = thamsos.filter(data => data.ma_tham_so === "vbdi_an_hien_vblq_ngoai_he_thong")
            let arrvbde_cv_theodoi_vb = thamsos.filter(data => data.ma_tham_so === "vbde_cv_theodoi_vb")
            let arrnan_nhom_nguoi_nhan_van_ban_den_mac_dinh = thamsos.filter(data => data.ma_tham_so === "nan_nhom_nguoi_nhan_van_ban_den_mac_dinh")
            let arrLay_domain_tu_file_url_vpc = thamsos.filter(data => data.ma_tham_so === "lay_domain_tu_file_url_vpc")

            let ht_chon_tgian_nhac_viec = arrht_chon_tgian_nhac_viec.length === 0 ? "3m" : arrht_chon_tgian_nhac_viec[0].gia_tri_tham_so
            let cks_vnpt_mobile_pki = arrcks_vnpt_mobile_pki.length === 0 ? null : arrcks_vnpt_mobile_pki[0].gia_tri_tham_so
            let cks_mobile_pki = arrcks_mobile_pki.length === 0 ? null : arrcks_mobile_pki[0].gia_tri_tham_so
            let tn_mau_gui_sms = arrtn_mau_gui_sms.length === 0 ? null : arrtn_mau_gui_sms[0].gia_tri_tham_so
            let loc_dau_tin_nhan = arrloc_dau_tin_nhan.length === 0 ? null : arrloc_dau_tin_nhan[0].gia_tri_tham_so
            let chu_vu_cb_tin_nhan = arrrchu_vu_cb_tin_nhan.length === 0 ? null : arrrchu_vu_cb_tin_nhan[0].gia_tri_tham_so
            let tien_to_tin_nhan = arrtien_to_tin_nhan.length === 0 ? null : arrtien_to_tin_nhan[0].gia_tri_tham_so
            let lct_co_quan_hien_thi_ds_co_quan = arrlct_co_quan_hien_thi_ds_co_quan.length === 0 ? -1 : arrlct_co_quan_hien_thi_ds_co_quan[0].gia_tri_tham_so
            let vbde_duyet_so_ngay_hxl = arrvbde_duyet_so_ngay_hxl.length === 0 ? null : arrvbde_duyet_so_ngay_hxl[0].gia_tri_tham_so
            let vbde_cv_xu_ly_hxl_de_mac_dinh_ld_pto = arrvbde_cv_xu_ly_hxl_de_mac_dinh_ld_pto.length === 0 ? null : arrvbde_cv_xu_ly_hxl_de_mac_dinh_ld_pto[0].gia_tri_tham_so
            let vai_tro_xdb_duoc_chuyen_tiep = arrvai_tro_xdb_duoc_chuyen_tiep.length === 0 ? 0 : arrvai_tro_xdb_duoc_chuyen_tiep[0].gia_tri_tham_so
            let vbde_xdb_chuyen_tiep_pto = arrvbde_xdb_chuyen_tiep_pto.length === 0 ? 1 : arrvbde_xdb_chuyen_tiep_pto[0].gia_tri_tham_so
            let lct_co_duyet_lich_cong_tac = arrlct_co_duyet_lich_cong_tac.length === 0 ? 0 : arrlct_co_duyet_lich_cong_tac[0].gia_tri_tham_so
            let kyso_no_signhub = arrkyso_no_signhub.length === 0 ? 0 : arrkyso_no_signhub[0].gia_tri_tham_so
            let vbde_xlc_phai_chuyen_ld_pto = arrvbde_xlc_phai_chuyen_ld_pto.length === 0 ? 0 : arrvbde_xlc_phai_chuyen_ld_pto[0].gia_tri_tham_so
            let vbdi_gui_thong_bao_app_pto = arrvbdi_gui_thong_bao_app_pto.length === 0 ? 1 : arrvbdi_gui_thong_bao_app_pto[0].gia_tri_tham_so
            let vbde_gui_thong_bao_app_pto = arrvbde_gui_thong_bao_app_pto.length === 0 ? 1 : arrvbde_gui_thong_bao_app_pto[0].gia_tri_tham_so
            let cks_hien_thi_nut_ky_so_hsm = arrcks_hien_thi_nut_ky_so_hsm.length === 0 ? 0 : arrcks_hien_thi_nut_ky_so_hsm[0].gia_tri_tham_so
            let lct_tuan_trong_nam_kieu_cu = arrlct_tuan_trong_nam_kieu_cu.length === 0 ? 0 : arrlct_tuan_trong_nam_kieu_cu[0].gia_tri_tham_so
            let lct_ngay_bat_dau_lam_viec = arrlct_ngay_bat_dau_lam_viec.length === 0 ? 1 : arrlct_ngay_bat_dau_lam_viec[0].gia_tri_tham_so
            let cks_xoa_file_sau_khi_ky_so = arrcks_xoa_file_sau_khi_ky_so.length === 0 ? 0 : arrcks_xoa_file_sau_khi_ky_so[0].gia_tri_tham_so
            let gd_thu_tu_tab_cbdv = arrgd_thu_tu_tab_cbdv.length === 0 ? 0 : arrgd_thu_tu_tab_cbdv[0].gia_tri_tham_so
            let cks_smartca_hien_thi_nut_ky = arrcks_smartca_hien_thi_nut_ky.length === 0 ? 0 : arrcks_smartca_hien_thi_nut_ky[0].gia_tri_tham_so
            let nv_nhac_viec_cmu = arrnv_nhac_viec_cmu.length === 0 ? 0 : arrnv_nhac_viec_cmu[0].gia_tri_tham_so
            let vbde_cv_theodoi_vb_mobile = arrvbde_cv_theodoi_vb_mobile.length === 0 ? 0 : arrvbde_cv_theodoi_vb_mobile[0].gia_tri_tham_so
            let cd_tao_chi_dao_khi_duyet_vbde = arrcd_tao_chi_dao_khi_duyet_vbde.length === 0 ? 0 : arrcd_tao_chi_dao_khi_duyet_vbde[0].gia_tri_tham_so
            let vbdi_chuyen_thuong_truc_ky = arrvbdi_chuyen_thuong_truc_ky.length === 0 ? 0 : arrvbdi_chuyen_thuong_truc_ky[0].gia_tri_tham_so
            let vbnb_hien_thi_vb_lien_quan_mobile = arrvbnb_hien_thi_vb_lien_quan_mobile.length === 0 ? 0 : arrvbnb_hien_thi_vb_lien_quan_mobile[0].gia_tri_tham_so
            let cd_chuyen_tiep_cddh_song_song_vbde = arrcd_chuyen_tiep_cddh_song_song_vbde.length === 0 ? 0 : arrcd_chuyen_tiep_cddh_song_song_vbde[0].gia_tri_tham_so
            let cks_hien_thi_thong_tin_app_mobile = arrcd_cks_hien_thi_thong_tin_app_mobile.length === 0 ? 1 : arrcd_cks_hien_thi_thong_tin_app_mobile[0].gia_tri_tham_so
            let cks_smartca_url = arrcks_smartca_url.length === 0 ? "https://gwsca.vnpt.vn" : arrcks_smartca_url[0].gia_tri_tham_so
            let cks_smartca_client_id = arrcks_smartca_client_id.length === 0 ? "42ee-637744055357737854.apps.smartcaapi.com" : arrcks_smartca_client_id[0].gia_tri_tham_so
            let cks_smartca_client_secret = arrcks_smartca_client_secret.length === 0 ? "ZDY2OThmZWE-YzRkNC00MmVl" : arrcks_smartca_client_secret[0].gia_tri_tham_so
            let cks_smartca_callback = arrcks_smartca_callback.length === 0 ? global.AU_ROOT + "/api/tien-ich/smartca-handle-access-token" : arrcks_smartca_callback[0].gia_tri_tham_so
            let vbde_gop_chung_vb_dien_tu = arrvbde_gop_chung_vb_dien_tu.length === 0 ? 0 : arrvbde_gop_chung_vb_dien_tu[0].gia_tri_tham_so
            let user_edoc = arruser_edoc.length === 0 ? 0 : arruser_edoc[0].gia_tri_tham_so
            let pass_edoc = arrpass_edoc.length === 0 ? 0 : arrpass_edoc[0].gia_tri_tham_so
            let pub_key_edoc = arrpub_key_edoc.length === 0 ? 0 : arrpub_key_edoc[0].gia_tri_tham_so
            let pri_key_edoc = arrpri_key_edoc.length === 0 ? 0 : arrpri_key_edoc[0].gia_tri_tham_so
            let vbdi_duyet_vb_chuyen_phap_che = arrvbdi_duyet_vb_chuyen_phap_che.length === 0 ? 0 : arrvbdi_duyet_vb_chuyen_phap_che[0].gia_tri_tham_so
            let vbdi_cho_xl_chuyen_phap_che = arrvbdi_cho_xl_chuyen_phap_che.length === 0 ? 0 : arrvbdi_cho_xl_chuyen_phap_che[0].gia_tri_tham_so
            let khoang_thoi_gian_danh_sach_vb = arrkhoang_thoi_gian_danh_sach_vb.length === 0 ? "0" : arrkhoang_thoi_gian_danh_sach_vb[0].gia_tri_tham_so
            let vbdi_an_hien_vblq_ngoai_he_thong = arrvbdi_an_hien_vblq_ngoai_he_thong.length === 0 ? 0 : arrvbdi_an_hien_vblq_ngoai_he_thong[0].gia_tri_tham_so
            let vbde_cv_theodoi_vb = arrvbde_cv_theodoi_vb.length === 0 ? 0 : arrvbde_cv_theodoi_vb[0].gia_tri_tham_so
            let nan_nhom_nguoi_nhan_van_ban_den_mac_dinh = arrnan_nhom_nguoi_nhan_van_ban_den_mac_dinh.length === 0 ? 0 : arrnan_nhom_nguoi_nhan_van_ban_den_mac_dinh[0].gia_tri_tham_so
            let lay_domain_tu_file_url_vpc = arrLay_domain_tu_file_url_vpc.length === 0 ? 0 : arrLay_domain_tu_file_url_vpc[0].gia_tri_tham_so

            global.ht_chon_tgian_nhac_viec = ht_chon_tgian_nhac_viec
            global.cks_vnpt_mobile_pki = cks_vnpt_mobile_pki
            global.cks_mobile_pki = cks_mobile_pki
            global.tn_mau_gui_sms = tn_mau_gui_sms
            global.loc_dau_tin_nhan = loc_dau_tin_nhan
            global.chu_vu_cb_tin_nhan = chu_vu_cb_tin_nhan
            global.tien_to_tin_nhan = tien_to_tin_nhan
            global.lct_co_quan_hien_thi_ds_co_quan = lct_co_quan_hien_thi_ds_co_quan
            global.vbde_duyet_so_ngay_hxl = vbde_duyet_so_ngay_hxl
            global.vbde_cv_xu_ly_hxl_de_mac_dinh_ld_pto = vbde_cv_xu_ly_hxl_de_mac_dinh_ld_pto
            global.vai_tro_xdb_duoc_chuyen_tiep = vai_tro_xdb_duoc_chuyen_tiep
            global.vbde_xdb_chuyen_tiep_pto = vbde_xdb_chuyen_tiep_pto
            global.lct_co_duyet_lich_cong_tac = lct_co_duyet_lich_cong_tac
            global.kyso_no_signhub = kyso_no_signhub
            global.vbde_xlc_phai_chuyen_ld_pto = vbde_xlc_phai_chuyen_ld_pto
            global.vbdi_gui_thong_bao_app_pto = vbdi_gui_thong_bao_app_pto
            global.vbde_gui_thong_bao_app_pto = vbde_gui_thong_bao_app_pto
            global.cks_hien_thi_nut_ky_so_hsm = cks_hien_thi_nut_ky_so_hsm
            global.lct_tuan_trong_nam_kieu_cu = lct_tuan_trong_nam_kieu_cu
            global.lct_ngay_bat_dau_lam_viec = lct_ngay_bat_dau_lam_viec
            global.cks_xoa_file_sau_khi_ky_so = cks_xoa_file_sau_khi_ky_so
            global.gd_thu_tu_tab_cbdv = gd_thu_tu_tab_cbdv
            global.cks_smartca_hien_thi_nut_ky = cks_smartca_hien_thi_nut_ky
            global.nv_nhac_viec_cmu = nv_nhac_viec_cmu
            global.vbde_cv_theodoi_vb_mobile = vbde_cv_theodoi_vb_mobile
            global.cd_tao_chi_dao_khi_duyet_vbde = cd_tao_chi_dao_khi_duyet_vbde
            global.vbdi_chuyen_thuong_truc_ky = vbdi_chuyen_thuong_truc_ky
            global.vbnb_hien_thi_vb_lien_quan_mobile = vbnb_hien_thi_vb_lien_quan_mobile;
            global.cd_chuyen_tiep_cddh_song_song_vbde = cd_chuyen_tiep_cddh_song_song_vbde
            global.cks_hien_thi_thong_tin_app_mobile = cks_hien_thi_thong_tin_app_mobile
            global.cks_smartca_url = cks_smartca_url
            global.cks_smartca_client_id = cks_smartca_client_id
            global.cks_smartca_client_secret = cks_smartca_client_secret
            global.cks_smartca_callback = cks_smartca_callback
            global.vbde_gop_chung_vb_dien_tu = vbde_gop_chung_vb_dien_tu
            global.khoang_thoi_gian_danh_sach_vb = khoang_thoi_gian_danh_sach_vb
            global.user_edoc = user_edoc
            global.pass_edoc = pass_edoc
            global.pub_key_edoc = pub_key_edoc
            global.pri_key_edoc = pri_key_edoc
            global.vbdi_duyet_vb_chuyen_phap_che = vbdi_duyet_vb_chuyen_phap_che
            global.vbdi_cho_xl_chuyen_phap_che = vbdi_cho_xl_chuyen_phap_che
            global.vbdi_an_hien_vblq_ngoai_he_thong = vbdi_an_hien_vblq_ngoai_he_thong
            global.vbde_cv_theodoi_vb = vbde_cv_theodoi_vb
            global.nan_nhom_nguoi_nhan_van_ban_den_mac_dinh = nan_nhom_nguoi_nhan_van_ban_den_mac_dinh
            global.lay_domain_tu_file_url_vpc = lay_domain_tu_file_url_vpc
            result = true
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(error => {
        if (global.CONSOLE_AU_DSTSHTTCTCB) {
            console.log("CONSOLE_AU_DSTSHTTCTCB ERROR: " + JSON.stringify(error))
        }
        showWarning(error)
    })
    return result
}

async function AU_GETOTPCODE(madonviquantri, somay, user) {
    let result = []
    var form = new FormData();
    form.append("madonviquantri", parseInt(madonviquantri));
    form.append("somay", somay.toString());
    form.append("user", user.toString());

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_XTOTP,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_GETOTPCODE) {
            console.log("CONSOLE_AU_GETOTPCODE: " + JSON.stringify(response))
        }
        if (response.status === 200) {
            result = response.data
        } else {
            showWarning(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_GETOTPCODE) {
            console.log("CONSOLE_AU_GETOTPCODE ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_GETOTPCODE(madonviquantri, somay, user).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_CHECKOTPCODE(chuoi1, chuoi2) {
    let result = false
    var form = new FormData();
    form.append("chuoi1", chuoi1.toString());
    form.append("chuoi2", chuoi2.toString());

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_KTOTP,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_CHECKOTPCODE) {
            console.log("CONSOLE_AU_CHECKOTPCODE: " + JSON.stringify(response))
        }
        if (response.data === 1) {
            result = true
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_CHECKOTPCODE) {
            console.log("CONSOLE_AU_CHECKOTPCODE ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_CHECKOTPCODE(chuoi1, chuoi2).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_DKOTPNVSMS(dang_ky_otp, di_dong_can_bo, ma_can_bo_kc) {
    let result = false
    var form = new FormData();
    form.append("dang_ky_otp", dang_ky_otp);
    form.append("di_dong_can_bo", di_dong_can_bo);
    form.append("ma_can_bo_kc", ma_can_bo_kc);
    form.append("nhac_viec_sms", -1);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_DKOTPNVSMS,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_DKOTPNVSMS) {
            console.log("CONSOLE_AU_DKOTPNVSMS: " + JSON.stringify(response))
        }
        if (response.data.message === "Thực thi thành công") {
            result = true
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_DKOTPNVSMS) {
            console.log("CONSOLE_AU_AU_DKOTPNVSMS ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_DKOTPNVSMS(dang_ky_otp, di_dong_can_bo, ma_can_bo_kc).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_GTSM() {
    let result = []
    let url = global.AU_ROOT + URL_CONFIG.AU_GTSM
    url += "?ma_ctcb=" + global.ma_ctcb_kc.toString()

    await axios({
        method: "GET",
        url: url,
        headers: setHeaderToken(),
        timeout: 1500
    }).then(response => {
        if (global.CONSOLE_AU_GTSM) {
            console.log("CONSOLE_AU_GTSM: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_GTSM) {
            console.log("CONSOLE_AU_GTSM ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_GTSM().then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_STSM(chuoimathamso, chuoigiatri) {
    let result = false
    let url = global.AU_ROOT + URL_CONFIG.AU_STSM
    url += "?mactcb=" + global.ma_ctcb_kc.toString()
    url += "&chuoimathamso=" + chuoimathamso
    url += "&chuoigiatri=" + chuoigiatri

    await axios({
        method: "POST",
        url: url,
        headers: setHeaderToken(),
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_STSM) {
            console.log("CONSOLE_AU_STSM: " + JSON.stringify(response))
        }
        if (response.data.message === "Thực thi thành công") {
            result = true
        } else {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_STSM) {
            console.log("CONSOLE_AU_STSM ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_STSM(chuoimathamso, chuoigiatri).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_GMLDN() {
    let result = false
    var form = new FormData();
    form.append("ma_log_dang_nhap_kc", "0");
    form.append("ma_ctcb", global.ma_ctcb_kc);
    form.append("dia_chi_dang_nhap", "");
    form.append("trinh_duyet_web", "App Mobile");
    form.append("he_dieu_hanh", Platform.OS.toString().toLocaleUpperCase());
    form.append("thiet_bi", Platform.isPad ? "Tablet" : "Phone");
    form.append("user_name", global.username);
    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_GMLDN,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_GMLDN) {
            console.log("CONSOLE_AU_GMLDN: " + JSON.stringify(response))
        }
        if (response.data.message === "Thực thi thành công") {
            result = true
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_GMLDN) {
            console.log("CONSOLE_AU_AU_GMLDN ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_GMLDN().then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

//SSO
async function AU_TSCSY(AU_ROOT) {
    let result = []
    let url = AU_ROOT + URL_CONFIG.AU_TSCSY

    await axios({
        method: "GET",
        url: url,
        timeout: 1500
    }).then(response => {
        if (global.CONSOLE_AU_TSCSY) {
            console.log("CONSOLE_AU_TSCSY: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            if (response.data.data && response.data.data.length > 2 && response.data.data[2] != "*") {
                result = response.data.data
                console.log("ts sso: " + JSON.stringify(result))

            }
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_TSCSY) {
            console.log("CONSOLE_AU_TSCSY ERROR: " + JSON.stringify(error))
        }
    })
    return result
}

async function AU_OAUTH2_TOKEN(sso_login_uri, code, redirect_uri, client_id, client_secret) {
    let result = ""
    let url = sso_login_uri + URL_CONFIG.AU_OAUTH2_TOKEN
    var qs = require('qs');
    var data = qs.stringify({
        "grant_type": "authorization_code",
        "code": code,
        "redirect_uri": redirect_uri,
        "client_id": client_id,
        "client_secret": client_secret
    });

    await axios({
        method: "POST",
        url: url,
        data,
        headers: { 'content-type': 'application/x-www-form-urlencoded' },
    }).then(response => {
        if (global.CONSOLE_AU_OAUTH2_TOKEN) {
            console.log("CONSOLE_AU_OAUTH2_TOKEN: " + JSON.stringify(response))
        }
        if (response.status === 200) {
            result = response.data
        } else {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_OAUTH2_TOKEN) {
            console.log("CONSOLE_AU_AU_OAUTH2_TOKEN ERROR: " + JSON.stringify(error))
        }
    })
    return result
}
//END SSO

//Begin Luchq: function get data NV CMU
async function AU_DSNVCMU_BYCB(ma_ctcb) {
    let today = new Date()
    den_ngay = formatDate(today)
    let tu_ngay = ""
    if (global.ht_chon_tgian_nhac_viec === "all" || global.ht_chon_tgian_nhac_viec === "hide") {
        tu_ngay = "01/01/2000"
    } else {
        let lengthMonth = global.ht_chon_tgian_nhac_viec.substring(0, 1)
        tu_ngay = moment(den_ngay, "DD/MM/YYYY").subtract(parseInt(lengthMonth), "months").format("DD/MM/YYYY")
    }
    var tu_ngay_7d = moment().subtract(7, 'days').format("DD/MM/YYYY");
    var toYear = moment().format('YYYY');
    var url = AU_ROOT + URL_CONFIG.AU_DSNVCMU;
    url += "?ma_ctcb=" + ma_ctcb
    url += "&tu_ngay=" + tu_ngay
    url += "&den_ngay=" + den_ngay
    let str_url7d = "/tra-cuu-van-ban/tra-cuu-vbdi-cmu?tu_menu=nhac_viec&denNgay=" + den_ngay + "&tuNgay=" + tu_ngay_7d + "&page=1";
    let str_url1y = "/tra-cuu-van-ban/tra-cuu-vbdi-cmu?tu_menu=nhac_viec&denNgay=" + den_ngay + "&tuNgay=1/1/" + toYear + "&page=1";
    let tong_7d = 0;
    let tong_1nam = 0;
    let res = [];
    await axios({
        method: "GET",
        url: url,
        headers: setHeaderToken(),
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        tong_7d = getNVNew("vbdi", str_url7d, response.data);
        tong_1nam = getNVNew("vbdi", str_url1y, response.data);
        res.push(tong_7d);
        res.push(tong_1nam);
    });
    return res;
}

// End Luchq: function get data NV CMU

async function AU_DSCTCB_SSO_MOBILE(username, key,password, ma_ctcb_kiem_nhiem,dispatch) {
    let result = false
    let form = new FormData()
    form.append("username", username)
    form.append("key", key)
    form.append("password",password)
    try {   
        await axios({
            method: "POST",
            url: AU_ROOT + URL_CONFIG.AU_DSCTCB_MOBILE,
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            data: form,
            timeout: AppConfig.TIME_OUT_SHORT
        }).then(async (thongtincb) => {     
            console.log(dispatch);
            if (thongtincb.data.message === "Lấy dữ liệu thành công") {
                dispatch({ type: "updateDSCTCB_ALL", dataAll: thongtincb.data.data })
                AsyncStorage.setItem("AU_DSCTCB", JSON.stringify(thongtincb.data.data))
                let curIndex = 0
                if (result !== undefined) {
                    thongtincb.data.data.forEach((element, index) => {
                        if (element.ma_ctcb_kc.toString() === ma_ctcb_kiem_nhiem) {
                            curIndex = index
                        }
                    });
                   
                }
                dispatch({ type: "updateDSCTCB", data: thongtincb.data.data[curIndex] })
                let ma_ctcb_kc = thongtincb.data.data[curIndex].ma_ctcb_kc
                global.ma_ctcb_kc = ma_ctcb_kc
                global.ma_can_bo = thongtincb.data.data[curIndex].ma_can_bo
                global.username = thongtincb.data.data[curIndex].username
                global.ma_don_vi = thongtincb.data.data[curIndex].ma_don_vi
                global.ten_don_vi = thongtincb.data.data[curIndex].ten_don_vi
                global.ho_va_ten_can_bo = thongtincb.data.data[curIndex].ho_va_ten_can_bo
                global.ten_chuc_vu = thongtincb.data.data[curIndex].ten_chuc_vu
                global.di_dong_can_bo = thongtincb.data.data[curIndex].di_dong_can_bo
                global.ngay_dang_ky_otp = thongtincb.data.data[curIndex].ngay_dang_ky_otp
                global.loai_ky_so_sim = thongtincb.data.data[curIndex].loai_ky_so_sim
                console.log(global.ho_va_ten_can_bo);
            
                await AU_ACCESS_TOKEN(thongtincb.data.data[curIndex].refresh_token).then(async () => {
                    setTimeout(() => {
                        AU_FCM(ma_ctcb_kc, global.fcmToken)
                    }, 0);

                    setTimeout(() => {
                        AU_DSDVCQ(ma_ctcb_kc)
                    }, 0);
                    await AU_DSTSHTTCTCB(ma_ctcb_kc).then(async () => {
                        await AU_DSNVCB(ma_ctcb_kc, dispatch).then(async () => {
                            await AU_KTQCVLDVTCCB(ma_ctcb_kc).then(() => {
                                result = true
                            })
                        })
                    })
                })
            }
        })
    } catch (error) {
        if (global.CONSOLE_AU_DSDVA) {
            console.log("CONSOLE_AU_DSDVA ERROR: " + JSON.stringify(error))
        }
        showWarning(error)
    }
    return result;
}

// NAN - Get quyền module xin ý kiến
async function AU_GETQXYKTT(refresh_token) {
    let url = global.AU_ROOT + URL_CONFIG.AU_GETQXYKTT
    url += "?refresh_token=" + refresh_token
    await axios({
        method: "GET",
        url: url,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_GETQXYKTT) {
            console.log("CONSOLE_AU_GETQXYKTT: " + JSON.stringify(response))
        }
        if (response.data.success) {
            global.xinykien = response.data.data.some(quyen => quyen.state_name === "xin-y-kien")
            console.log('global.xinykien', response.data.data.some(quyen => quyen.state_name === "xin-y-kien"))
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(error => {
        if (global.CONSOLE_AU_GETQXYKTT) {
            console.log("CONSOLE_AU_GETQXYKTT ERROR: " + JSON.stringify(error))
        }
        showWarning(error)
    })
}

export default {
    AU_DSDVA,
    AU_DSCTCB,
    AU_DSCTCB1,
    AU_DSCTCB_SSO,
    AU_ACCESS_TOKEN,
    AU_DSDVCQ,
    AU_KTQCVLDVTCCB,
    AU_DSNVCB,
    AU_FCM,
    AU_PUSH_FCM,
    AU_PUSH_FCM_APP,
    AU_DEL_FCM,
    AU_DSCBBID,
    AU_KTTMKC,
    AU_CNMK,
    AU_CCBTCDB,
    AU_GETOTPCODE,
    AU_CHECKOTPCODE,
    AU_DKOTPNVSMS,
    AU_GTSM,
    AU_STSM,
    AU_GMLDN,
    AU_TSCSY,
    AU_OAUTH2_TOKEN,
    AU_DSNVCMU_BYCB,
    AU_DSCTCB_SSO_MOBILE,
    AU_GETQXYKTT
}
