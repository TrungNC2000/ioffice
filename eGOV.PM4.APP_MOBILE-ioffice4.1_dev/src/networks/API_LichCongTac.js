import { URL_CONFIG } from "./Config"
import { Platform } from "react-native"
import DeviceInfo from 'react-native-device-info'
import { Toast } from "native-base"
import axios from "axios"
import { ReplaceAll } from "../untils/TextUntils"
import { AppConfig } from "../AppConfig"
import API_Login from "./API_Login"

function showWarning(error) {
    Toast.show({ text: "Kết nối bị gián đoạn!", type: "warning", buttonText: "Đóng", position: "top", duration: 2000 })
    if (error) {
        SendLog(error)
    }
};

async function SendLog(message) {
    try {
        let obj = new Object
        let date = new Date()
        obj.Model = DeviceInfo.getModelSync()
        obj.OS = Platform.OS.toString()
        obj.SystemVersion = DeviceInfo.getSystemVersionSync()
        obj.AppVersion = DeviceInfo.getVersionSync()
        obj.CurrentTime = date.toLocaleDateString() + " " + date.toLocaleTimeString()
        obj.Module = "API_LichCongTac"
        obj.Domain = global.AU_ROOT ? global.AU_ROOT : ""
        delete message.headers
        obj.Message = message
        let text = ReplaceAll(JSON.stringify(obj), String.fromCharCode(34), "'")
        let url = AppConfig.AppDefaultLogURLSendText + text
        await axios({
            method: "POST",
            url: url,
            timeout: AppConfig.TIME_OUT_SHORT
        })
    } catch (error) {
        console.log("Send log error: " + error)
    }
}

function setHeaderToken() {
    return { "Authorization": "Bearer " + global.access_token }
};
async function AU_LCT_DSCQLCT() {
    let result = []
    var form = new Object()

    await axios({
        method: "GET",
        url: global.AU_ROOT + URL_CONFIG.AU_LCT_DSCQ,
        headers: setHeaderToken(),
        params: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_LCT_DSCQLCT) {
            console.log("CONSOLE_AU_LCT_DSCQLCT: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_LCT_DSCQLCT) {
            console.log("CONSOLE_AU_LCT_DSCQLCT ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_LCT_DSCQLCT().then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}
async function AU_LCT_DSCQLCT1(madv) {
    let result = []
    var form = new Object()
    form.ma_don_vi = madv

    await axios({
        method: "GET",
        url: global.AU_ROOT + URL_CONFIG.AU_LCT_DSCQ1,
        headers: setHeaderToken(),
        params: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_LCT_DSCQLCT1) {
            console.log("CONSOLE_AU_LCT_DSCQLCT1: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_LCT_DSCQLCT1) {
            console.log("CONSOLE_AU_LCT_DSCQLCT1 ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_LCT_DSCQLCT1(madv).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_LCT_LLCTCT(ma_lich_cong_tac) {
    let result = []
    let url = global.AU_ROOT + URL_CONFIG.AU_LCT_LLCTCT
    url += "?ma_lich_cong_tac=" + ma_lich_cong_tac

    await axios({
        method: "GET",
        url: url,
        headers: setHeaderToken(),
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_LCT_LLCTCT) {
            console.log("CONSOLE_AU_LCT_LLCTCT: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_LCT_LLCTCT) {
            console.log("CONSOLE_AU_LCT_LLCTCT ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_LCT_LLCTCT(ma_lich_cong_tac).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_LCT_DSLCTDVQT(ma_don_vi_quan_tri, nam) {
    let result = []
    let url = global.AU_ROOT + URL_CONFIG.AU_LCT_DSLCTDVQT
    url += "?ma_don_vi_quan_tri=" + ma_don_vi_quan_tri
    url += "&nam=" + nam

    await axios({
        method: "GET",
        url: url,
        headers: setHeaderToken(),
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_LCT_DSLCTDVQT) {
            console.log("CONSOLE_AU_LCT_DSLCTDVQT: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_LCT_DSLCTDVQT) {
            console.log("CONSOLE_AU_LCT_DSLCTDVQT ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_LCT_DSLCTDVQT(ma_don_vi_quan_tri, nam).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_LCT_DSLCTDV(ma_don_vi, nam) {
    let result = []
    let url = global.AU_ROOT + URL_CONFIG.AU_LCT_DSLCTDV
    url += "?ma_don_vi=" + ma_don_vi
    url += "&nam=" + nam

    await axios({
        method: "GET",
        url: url,
        headers: setHeaderToken(),
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_LCT_DSLCTDV) {
            console.log("CONSOLE_AU_LCT_DSLCTDV: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_LCT_DSLCTDV) {
            console.log("CONSOLE_AU_LCT_DSLCTDV ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_LCT_DSLCTDV(ma_don_vi, nam).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_LCT_XLCTCN(ma_ctcb, nam) {
    let result = []
    let url = global.AU_ROOT + URL_CONFIG.AU_LCT_XLCTCN
    url += "?ma_ctcb=" + ma_ctcb
    url += "&nam=" + nam

    await axios({
        method: "GET",
        url: url,
        headers: setHeaderToken(),
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_LCT_XLCTCN) {
            console.log("CONSOLE_AU_LCT_XLCTCN: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_LCT_XLCTCN) {
            console.log("CONSOLE_AU_LCT_XLCTCN ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_LCT_XLCTCN(ma_ctcb, nam).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_LCT_DSCB() {
    let result = []
    var form = new Object()
    form.ma_ctcb = global.ma_ctcb_kc

    await axios({
        method: "GET",
        url: global.AU_ROOT + URL_CONFIG.AU_LCT_DSCB,
        headers: setHeaderToken(),
        params: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_LCT_DSCB) {
            console.log("CONSOLE_AU_LCT_DSCB: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_LCT_DSCB) {
            console.log("CONSOLE_AU_LCT_DSCB ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_LCT_DSCB().then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}
async function AU_LCT_DSDVLCT() {
    let result = []
    var form = new Object()
    form.ma_ctcb = global.ma_ctcb_kc

    await axios({
        method: "GET",
        url: global.AU_ROOT + URL_CONFIG.AU_LCT_DSDV,
        headers: setHeaderToken(),
        params: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_LCT_DSDVLCT) {
            console.log("CONSOLE_AU_LCT_DSDVLCT: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_LCT_DSDVLCT) {
            console.log("CONSOLE_AU_LCT_DSDVLCT ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_LCT_DSDVLCT().then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_LCT_CT(ma_cb_dv, tuan, nam, loai_lct) {
    let result = ""
    let form = new FormData();
    form.append("ma_cb_dv", parseInt(ma_cb_dv));
    form.append("tuan", parseInt(tuan));
    form.append("nam", parseInt(nam));
    form.append("loai_lct", parseInt(loai_lct));

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_LCT_CTLCT,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_LCT_CT) {
            console.log("CONSOLE_AU_LCT_CT: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_LCT_CT) {
            console.log("CONSOLE_AU_LCT_CT ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_LCT_CT(ma_cb_dv, tuan, nam, loai_lct).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}
async function AU_LCT_TSHT(ma_ctcb) {
    let result = []
    var form = new Object()
    form.ma_ctcb = ma_ctcb

    await axios({
        method: "GET",
        url: global.AU_ROOT + URL_CONFIG.AU_LCT_CHHT,
        headers: setHeaderToken(),
        params: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_LCT_TSHT) {
            console.log("CONSOLE_AU_LCT_TSHT: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_LCT_TSHT) {
            console.log("CONSOLE_AU_LCT_TSHT ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_LCT_TSHT(ma_ctcb).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_LCT_COT(ma_cb_dv, tuan, nam, loai_lct) {
    let result = ""
    let form = new FormData();
    form.append("ma_cb_dv", parseInt(ma_cb_dv));
    form.append("tuan", parseInt(tuan));
    form.append("nam", parseInt(nam));
    form.append("loai_lct", parseInt(loai_lct));

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_LCT_DSCOT,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_LCT_COT) {
            console.log("CONSOLE_AU_LCT_COT: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_LCT_COT) {
            console.log("CONSOLE_AU_LCT_COT ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_LCT_COT(ma_cb_dv, tuan, nam, loai_lct).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_LCT_DSCBLCT(ma_lich_cong_tac) {
    let result = []
    let url = global.AU_ROOT + URL_CONFIG.AU_LCT_DSCBLCT
    url += "?ma_lich_cong_tac=" + parseInt(ma_lich_cong_tac)

    await axios({
        method: "GET",
        url: url,
        headers: setHeaderToken(),
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_LCT_DSCBLCT) {
            console.log("CONSOLE_AU_LCT_DSCBLCT: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_LCT_DSCBLCT) {
            console.log("CONSOLE_AU_LCT_DSCBLCT ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_LCT_DSCBLCT(ma_lich_cong_tac).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_LCT_LCTLDBC(MaDVQT) {
    let result = []
    let obj = new Object();
    obj.ma_dvqt = MaDVQT;
    await axios({
        method: "GET",
        url: global.AU_ROOT + URL_CONFIG.AU_LCT_LCTLDBC,
        headers: setHeaderToken(),
        params: obj,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_LCT_LCTLDBC) {
            console.log("CONSOLE_AU_LCT_LCTLDBC: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_LCT_LCTLDBC) {
            console.log("CONSOLE_AU_LCT_LCTLDBC ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_LCT_LCTLDBC(MaDVQT).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_LCT_XLCTLD(MaDVQT, nam, tuan) {
    let result = []
    let obj = new Object();
    obj.ma_dvqt = MaDVQT;
    obj.nam = nam;
    obj.tuan = tuan;

    await axios({
        method: "GET",
        url: global.AU_ROOT + URL_CONFIG.AU_LCT_XLCTLD,
        headers: setHeaderToken(),
        params: obj,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_LCT_XLCTLD) {
            console.log("CONSOLE_AU_LCT_XLCTLD: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data;
        } else {
            result = ["khong"]
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_LCT_XLCTLD) {
            console.log("CONSOLE_AU_LCT_XLCTLD ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_LCT_XLCTLD(MaDVQT, nam, tuan).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_LCT_DSDVDDC(ma_don_vi) {
    let result = []
    let url = global.AU_ROOT + URL_CONFIG.AU_LCT_DSDVDDC
    url += "?ma_don_vi=" + ma_don_vi

    await axios({
        method: "GET",
        url: url,
        headers: setHeaderToken(),
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_LCT_DSDVDDC) {
            console.log("CONSOLE_AU_LCT_DSDVDDC: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data;
        } else {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_LCT_DSDVDDC) {
            console.log("CONSOLE_AU_LCT_DSDVDDC ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_LCT_DSDVDDC(ma_don_vi).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_LCT_DLCTCQ(chuoi_ma_cong_viec, chuoi_ma_khong_duyet) {
    let result = false
    let form = new FormData();
    form.append("chuoi_ma_cong_viec", chuoi_ma_cong_viec);
    form.append("chuoi_ma_khong_duyet", chuoi_ma_khong_duyet);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_LCT_DLCTCQ,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_LCT_DLCTCQ) {
            console.log("CONSOLE_AU_LCT_DLCTCQ: " + JSON.stringify(response))
        }
        if (response.data.message === "Thực thi thành công") {
            result = true
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_LCT_DLCTCQ) {
            console.log("CONSOLE_AU_LCT_DLCTCQ ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_LCT_DLCTCQ(chuoi_ma_cong_viec, chuoi_ma_khong_duyet).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

export default {
    AU_LCT_DSCQLCT,
    AU_LCT_LLCTCT,
    AU_LCT_DSLCTDVQT,
    AU_LCT_DSLCTDV,
    AU_LCT_XLCTCN,
    AU_LCT_CT,
    AU_LCT_COT,
    AU_LCT_DSDVLCT,
    AU_LCT_DSCB,
    AU_LCT_TSHT,
    AU_LCT_DSCQLCT1,
    AU_LCT_LCTLDBC,
    AU_LCT_XLCTLD,
    AU_LCT_DSCBLCT,
    AU_LCT_DSDVDDC,
    AU_LCT_DLCTCQ,
}