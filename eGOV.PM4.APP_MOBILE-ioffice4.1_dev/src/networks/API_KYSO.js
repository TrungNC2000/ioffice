import { URL_CONFIG } from "./Config"
import { Platform } from "react-native"
import DeviceInfo from 'react-native-device-info'
import { Toast } from "native-base"
import axios from "axios"
import { AppConfig } from "../AppConfig"
import * as Models from "../Models"
import base64 from "../untils/base64"
import API_Login from "./API_Login"
import { ReplaceAll, checkVersionWeb } from "../untils/TextUntils"

function showWarning(error) {
    Toast.show({ text: "Kết nối bị gián đoạn!", type: "warning", buttonText: "Đóng", position: "top", duration: 2000 })
    if (error) {
        SendLog(error)
    }
};

async function SendLog(message) {
    try {
        let obj = new Object
        let date = new Date()
        obj.OS = Platform.OS.toString()
        obj.CurrentTime = date.toLocaleDateString() + " " + date.toLocaleTimeString()
        obj.Module = "API_KySo"
        obj.UserName = global.username
        obj.SoDienThoai = global.di_dong_can_bo
        obj.Domain = global.AU_ROOT ? global.AU_ROOT : ""
        delete message.headers
        obj.Message = message
        let text = ReplaceAll(JSON.stringify(obj), String.fromCharCode(34), "'").substring(0, 2000)
        let url = AppConfig.AppDefaultLogURLSendText + text
        await axios({
            method: "POST",
            url: url,
            timeout: AppConfig.TIME_OUT_SHORT
        }).then(response => {
            console.log("Send log ok")
        })
    } catch (error) {
        console.log("Send log error: " + error)
    }
}

function setHeaderToken() {
    return { "Authorization": "Bearer " + global.access_token }
};

async function AU_KYSO_DSCKS(ma_ctcb_kc) {
    let result = []
    var form = new Object()
    form.ma_ctcb = ma_ctcb_kc

    await axios({
        method: "GET",
        url: global.AU_ROOT + URL_CONFIG.AU_KYSO_DSCKS,
        headers: setHeaderToken(),
        params: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_KYSO_DSCKS) {
            console.log("CONSOLE_AU_KYSO_DSCKS: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_KYSO_DSCKS) {
            console.log("CONSOLE_AU_KYSO_DSCKS ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_KYSO_DSCKS(ma_ctcb_kc).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_KYSO_KSS(mKySoSim: Models.mKySoSim) {
    let result = "-1"
    console.log("global.web_version: " + global.web_version)
    console.log("global.kyso_no_signhub: " + global.kyso_no_signhub)

    if (checkVersionWeb(26) && global.kyso_no_signhub == 1) {
        //Ky so moi
        var form = new FormData();
        form.append("filePath", mKySoSim.file);
        form.append("imagePath", mKySoSim.filehinhanh);
        form.append("textSearch", mKySoSim.tenchucdanh);
        form.append("mssp", mKySoSim.msspProvider);
        form.append("sdt", mKySoSim.sodienthoai);
        form.append("pageSign", mKySoSim.trangky !== "" ? parseInt(mKySoSim.trangky) : 0);
        form.append("side", checkVersionWeb(16) ? global.US_Mobile_Pki_Side : "BOTTOM");
        if (mKySoSim.tenchucdanh !== "") {
            //Ký tự động
            form.append("width", parseInt(global.US_Mobile_Pki_Width_Rec));
            form.append("height", parseInt(global.US_Mobile_Pki_Height_Rec));
            form.append("llx", 0);
            form.append("lly", 0);
            form.append("urx", 0);
            form.append("ury", 0);
        } else {
            //Ký vị trí
            form.append("width", 0);
            form.append("height", 0);
            form.append("llx", Math.round(mKySoSim.llx));
            form.append("lly", Math.round(mKySoSim.lly));
            form.append("urx", Math.round(mKySoSim.urx));
            form.append("ury", Math.round(mKySoSim.ury));
        }
        form.append("ma_ctcb", global.ma_ctcb_kc);

        console.log("AU_KYSO_KSPKI Request: " + JSON.stringify(form))

        await axios({
            method: "POST",
            url: global.AU_ROOT + URL_CONFIG.AU_KYSO_KSPKI,
            headers: setHeaderToken(),
            data: form,
        }).then(response => {
            console.log("AU_KYSO_KSPKI Response: " + JSON.stringify(response))
            SendLog(response)
            if (response.data.code == 0) {
                result = response.data.data
            }
        }).catch(async error => {
            if (global.CONSOLE_AU_KYSO_KSS) {
                console.log("AU_KYSO_KSPKI ERROR: " + JSON.stringify(error))
            }
            SendLog(error)
            if (error.response.status == 401) {
                await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                    await AU_KYSO_KSS(mKySoSim).then(async (resCallback) => {
                        result = resCallback
                    })
                })
            }
        })
        return result
    } else { //Ky so cu
        let domainfile_vpc =  mKySoSim.file.split('___')[1]?.split('__')[0]?.replace('https-', 'https://')
        let url = global.AU_ROOT + URL_CONFIG.AU_KYSO_KSS + "?"
        url += "uri=" + mKySoSim.uri
        url += "&file=" + mKySoSim.file
        url += "&filename=" + mKySoSim.filename
        url += "&macongvan=" + mKySoSim.macongvan.toString()
        url += "&tenchucdanh=" + mKySoSim.tenchucdanh
        url += "&filehinhanh=" + mKySoSim.filehinhanh
        url += "&trangky=" + mKySoSim.trangky.toString()
        url += "&domainfile=" + (global.lay_domain_tu_file_url_vpc == 1 ? domainfile_vpc : mKySoSim.domainfile)
        url += "&domainapi=" + mKySoSim.domainapi
        url += "&ma_ctcb=" + mKySoSim.ma_ctcb.toString()
        url += "&madv=" + mKySoSim.madv.toString()
        url += "&chucnang=" + mKySoSim.chucnang
        url += "&sodienthoai=" + mKySoSim.sodienthoai
        url += "&msspProvider=" + mKySoSim.msspProvider
        url += "&llx=" + Math.round(mKySoSim.llx).toString()
        url += "&lly=" + Math.round(mKySoSim.lly).toString()
        url += "&urx=" + Math.round(mKySoSim.urx).toString()
        url += "&ury=" + Math.round(mKySoSim.ury).toString()

        if (mKySoSim.tenchucdanh !== "" && global.US_Mobile_Pki_Width_Rec && global.US_Mobile_Pki_Height_Rec) {
            //Ký tự động
            url += "&width=" + global.US_Mobile_Pki_Width_Rec
            url += "&height=" + global.US_Mobile_Pki_Height_Rec
        }

        if (checkVersionWeb(16)) {
            url += "&mode=" + global.US_Mobile_Pki_Mode
            url += "&side=" + global.US_Mobile_Pki_Side
        }

        console.log("AU_KYSO_KSS Request: " + url)

        await axios({
            method: "POST",
            url: url,
            headers: setHeaderToken()
        }).then(response => {
            console.log("AU_KYSO_KSS Response: " + JSON.stringify(response))
            SendLog(response)
            if (response.data.indexOf("|") !== -1) {
                let data = response.data.split("|")
                let data1 = data[0]
                let data2 = JSON.parse(data[1])
                if (data2.message === 'Thực thi thành công') {
                    result = data1
                }
            }
        }).catch(async error => {
            if (global.CONSOLE_AU_KYSO_KSS) {
                console.log("CONSOLE_AU_KYSO_KSS ERROR: " + JSON.stringify(error))
            }
            SendLog(error)
            if (error.response.status == 401) {
                await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                    await AU_KYSO_KSS(mKySoSim).then(async (resCallback) => {
                        result = resCallback
                    })
                })
            }
        })
        return result
    }
}

async function AU_KYSO_KSSGFS(FileID) {
    let result = ""
    var form = new Object()
    form.FileID = FileID
    form.ma_ctcb = global.ma_ctcb_kc
    form.madv = global.ma_don_vi
    console.log("AU_KYSO_KSSGFS Request: " + JSON.stringify(form))
    await axios({
        method: "GET",
        url: global.AU_ROOT + URL_CONFIG.AU_KYSO_KSSGFS,
        headers: setHeaderToken(),
        params: form,
    }).then(response => {
        console.log("AU_KYSO_KSSGFS Response: " + JSON.stringify(response))
        if (response.data.status === 1) {
            result = response.data.filepath
        } else {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_KYSO_KSSGFS) {
            console.log("CONSOLE_AU_KYSO_KSSGFS ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_KYSO_KSSGFS(FileID).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_KYSO_VTCNFKPH(ma_van_ban_di, chuoi_file_moi) {
    let result = false
    var form = new Object()
    form.ma_van_ban_di = ma_van_ban_di
    form.chuoi_file_moi = chuoi_file_moi

    await axios({
        method: "GET",
        url: global.AU_ROOT + URL_CONFIG.AU_KYSO_VTCNFKPH,
        headers: setHeaderToken(),
        params: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_KYSO_VTCNFKPH) {
            console.log("CONSOLE_AU_KYSO_VTCNFKPH: " + JSON.stringify(response))
        }
        if (response.data.message = "Thực thi thành công") {
            result = true
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_KYSO_VTCNFKPH) {
            console.log("CONSOLE_AU_KYSO_VTCNFKPH ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_KYSO_VTCNFKPH(ma_van_ban_di, chuoi_file_moi).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_KYSO_CNVBDKPTO(ma_van_ban_di, chuoi_file_moi) {
    let result = false
    var form = new FormData();
    form.append("ma_van_ban_di", ma_van_ban_di);
    form.append("file", chuoi_file_moi);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_KYSO_CNVBDKPTO,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_KYSO_CNVBDKPTO) {
            console.log("CONSOLE_AU_KYSO_CNVBDKPTO: " + JSON.stringify(response))
        }
        console.log("CONSOLE_AU_KYSO_CNVBDKPTO: " + JSON.stringify(response))
        if (response.data.message = "Thực thi thành công") {
            result = true
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        console.log("CONSOLE_AU_KYSO_CNVBDKPTO ERROR: " + JSON.stringify(error))
        if (global.CONSOLE_AU_KYSO_CNVBDKPTO) {
            console.log("CONSOLE_AU_KYSO_CNVBDKPTO ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_KYSO_CNVBDKPTO(ma_van_ban_di, chuoi_file_moi).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_KYSO_LFKS(ma_van_ban_di, ma_xu_ly_di, file_ky_so) {
    let result = false
    var form = new Object()
    form.ma_ctcb = global.ma_ctcb_kc
    form.ma_van_ban_di = ma_van_ban_di
    form.ma_xu_ly_di = ma_xu_ly_di
    form.file_ky_so = file_ky_so
    form.loai = "THEM"

    console.log("AU_KYSO_LFKS:" + form)

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_KYSO_LFKS,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_KYSO_LFKS) {
            console.log("CONSOLE_AU_KYSO_LFKS: " + JSON.stringify(response))
        }
        if (response.data.message === "Thực thi thành công") {
            result = true
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_KYSO_LFKS) {
            console.log("CONSOLE_AU_KYSO_LFKS ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_KYSO_LFKS(ma_van_ban_di, ma_xu_ly_di, file_ky_so).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_KYSO_TMCKS(link_cks, ten_ky_so) {
    let result = false
    var form = new FormData();
    form.append("link_cks", link_cks);
    form.append("ma_ctcb_tao", global.ma_ctcb_kc);
    form.append("ten_ky_so", ten_ky_so);
    form.append("trang_thai", 0);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_KYSO_TMCKS,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_KYSO_TMCKS) {
            console.log("CONSOLE_AU_KYSO_TMCKS: " + JSON.stringify(response))
        }
        if (response.data.message === "Thực thi thành công") {
            result = true
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_KYSO_TMCKS) {
            console.log("CONSOLE_AU_KYSO_TMCKS ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_KYSO_TMCKS(link_cks, ten_ky_so).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_KYSO_XCKS(id_ky_so) {
    let result = false
    var form = new FormData();
    form.append("id_ky_so", id_ky_so);
    form.append("ma_ctcb_tao", global.ma_ctcb_kc);
    form.append("trang_thai", -1);

    await axios({
        method: "DELETE",
        url: global.AU_ROOT + URL_CONFIG.AU_KYSO_XCKS,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_KYSO_XCKS) {
            console.log("CONSOLE_AU_KYSO_XCKS: " + JSON.stringify(response))
        }
        console.log("CONSOLE_AU_KYSO_XCKS: " + JSON.stringify(response))
        if (response.data.message === "Thực thi thành công") {
            result = true
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_KYSO_XCKS) {
            console.log("CONSOLE_AU_KYSO_XCKS ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_KYSO_XCKS(id_ky_so).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_KYSO_CNCKS(id_ky_so) {
    let result = false
    var form = new FormData();
    form.append("id_ky_so", id_ky_so);
    form.append("ma_ctcb_tao", global.ma_ctcb_kc);
    form.append("trang_thai", 1);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_KYSO_CNCKS,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_KYSO_CNCKS) {
            console.log("CONSOLE_AU_KYSO_CNCKS: " + JSON.stringify(response))
        }
        console.log("CONSOLE_AU_KYSO_CNCKS: " + JSON.stringify(response))
        if (response.data.message === "Thực thi thành công") {
            result = true
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_KYSO_CNCKS) {
            console.log("CONSOLE_AU_KYSO_CNCKS ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_KYSO_CNCKS(id_ky_so).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_KYSO_TFP(ma_van_ban, type) {
    let result = []
    let url = global.AU_ROOT + URL_CONFIG.AU_KYSO_TFP
    if (type === "di") {
        url += "?ma_van_ban_di=" + ma_van_ban
    } else {
        url += "?ma_van_ban_den=" + ma_van_ban
    }

    await axios({
        method: "GET",
        url: url,
        headers: setHeaderToken(),
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_KYSO_TFP) {
            console.log("CONSOLE_AU_KYSO_TFP: " + JSON.stringify(response))
        }
        if (response.status === 200) {
            result = response.data
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_KYSO_TFP) {
            console.log("CONSOLE_AU_KYSO_TFP ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_KYSO_TFP(ma_van_ban, type).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_KYSO_GBFP(path) {
    let result = ""
    var form = new FormData();
    form.append("path", path);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_KYSO_GBFP,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_KYSO_GBFP) {
            console.log("CONSOLE_AU_KYSO_GBFP: " + JSON.stringify(response))
        }

        if (response.status === 200 && response.data.base64) {
            result = response.data.base64
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_KYSO_GBFP) {
            console.log("CONSOLE_AU_KYSO_GBFP ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_KYSO_GBFP(path).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_KYSO_GATH(account, password) {
    let result = ""
    var form = new FormData();
    form.append("account", base64.encode(account));
    form.append("password", base64.encode(password));
    form.append("ma_ctcb", global.ma_ctcb_kc);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_KYSO_GATH,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_KYSO_GATH) {
            console.log("CONSOLE_AU_KYSO_GATH: " + JSON.stringify(response))
        }
        if (response.data.isSuccess === 0) {
            result = JSON.stringify(response.data)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_KYSO_GATH) {
            console.log("CONSOLE_AU_KYSO_GATH ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_KYSO_GATH(account, password).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_KYSO_KSH(mKySoHSM: Models.mKySoHSM) {
    let result = ""
    var data = new FormData();
    data.append('ma_ctcb', mKySoHSM.ma_ctcb);
    data.append('filePath', mKySoHSM.filePath);
    data.append('access_token', mKySoHSM.access_token);
    data.append('Signatures', mKySoHSM.Signatures);
    data.append('FontStyle', mKySoHSM.FontStyle);
    data.append('FontSize', mKySoHSM.FontSize);
    data.append('FontName', mKySoHSM.FontName);
    data.append('FontColor', mKySoHSM.FontColor);
    data.append('VisibleType', mKySoHSM.VisibleType);
    data.append('Comment', mKySoHSM.Comment);
    data.append('Image', mKySoHSM.Image);
    data.append('DataBase64', mKySoHSM.DataBase64);
    console.log(JSON.stringify(data))

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_KYSO_KSH,
        headers: setHeaderToken(),
        data: data,
        timeout: 120000
    }).then(response => {
        if (global.CONSOLE_AU_KYSO_KSH) {
            console.log("CONSOLE_AU_KYSO_KSH: " + JSON.stringify(response))
        }
        console.log("CONSOLE_AU_KYSO_KSH: " + JSON.stringify(response))
        if (response.data.code === 0) {
            result = response.data.data
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_KYSO_KSH) {
            console.log("CONSOLE_AU_KYSO_KSH ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_KYSO_KSH(mKySoHSM).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_KYSO_SCGAT(username, password) {
    let result = ""
    var form = new FormData();
    form.append("username", username);
    form.append("password", password);
    form.append("ma_ctcb", global.ma_ctcb_kc);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_KYSO_SCGAT,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_KYSO_SCGAT) {
            console.log("CONSOLE_AU_KYSO_SCGAT: " + JSON.stringify(response))
        }
        if (response.data.isSuccess === 0) {
            result = JSON.stringify(response.data)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_KYSO_SCGAT) {
            console.log("CONSOLE_AU_KYSO_SCGAT ERROR: " + JSON.stringify(error))
        }
        if (error.response && error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_KYSO_SCGAT(username, password).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_KYSO_SCSH(mKySoSmartCA: Models.mKySoSmartCA) {
    let result = "-1"
    var data = new FormData();

    data.append('comments', "W10=");
    data.append('fontColor', mKySoSmartCA.FontColor);
    data.append('fontName', mKySoSmartCA.FontName);
    data.append('fontSize', mKySoSmartCA.FontSize);
    data.append('fontStyle', mKySoSmartCA.FontStyle);
    data.append('signatures', mKySoSmartCA.Signatures);
    data.append('visibleType', mKySoSmartCA.VisibleType);
    data.append('ma_ctcb', mKySoSmartCA.ma_ctcb);
    data.append('path', mKySoSmartCA.filePath);
    data.append('accessToken', mKySoSmartCA.access_token);
    data.append('fileBase64', mKySoSmartCA.DataBase64);
    data.append('imgBase64', mKySoSmartCA.Image);
    // console.log(JSON.stringify(data))
    // console.log("fileBase64: " + mKySoSmartCA.DataBase64)
    // console.log("imgBase64: " + mKySoSmartCA.Image)
    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_KYSO_SCSH,
        headers: setHeaderToken(),
        data: data,
        timeout: AppConfig.TIME_OUT_LONG
    }).then(response => {
        if (global.CONSOLE_AU_KYSO_SCSH) {
            console.log("CONSOLE_AU_KYSO_SCSH: " + JSON.stringify(response))
        }
        console.log("CONSOLE_AU_KYSO_SCSH: " + JSON.stringify(response))
        if (response.data.code === 0) {
            result = response.data.data
        } else {
            result = "-1"
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_KYSO_SCSH) {
            console.log("CONSOLE_AU_KYSO_SCSH ERROR: " + JSON.stringify(error))
        }
        if (error.response && error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_KYSO_SCSH(mKySoSmartCA).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}


async function AU_KYSO_SCSH_IOS(mKySoSmartCA: Models.mKySoSmartCA, key) {
    let result = "-1"
    var data = new FormData();
    // global.CONSOLE_AU_KYSO_SCSH_IOS = true
    data.append('key', key);
    data.append('comments', "W10=");
    data.append('fontColor', mKySoSmartCA.FontColor);
    data.append('fontName', mKySoSmartCA.FontName);
    data.append('fontSize', mKySoSmartCA.FontSize);
    data.append('fontStyle', mKySoSmartCA.FontStyle);
    data.append('signatures', mKySoSmartCA.Signatures);
    data.append('visibleType', mKySoSmartCA.VisibleType);
    data.append('ma_ctcb', mKySoSmartCA.ma_ctcb);
    data.append('path', mKySoSmartCA.filePath);
    data.append('accessToken', mKySoSmartCA.access_token);
    data.append('fileBase64', mKySoSmartCA.DataBase64);
    data.append('imgBase64', mKySoSmartCA.Image);
    console.log("data = " + JSON.stringify(data))

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_KYSO_SCSH_IOS,
        headers: setHeaderToken(),
        data: data,
        timeout: 2000
    }).then(response => {
        if (global.CONSOLE_AU_KYSO_SCSH_IOS) {
            console.log("CONSOLE_AU_KYSO_SCSH_IOS: " + JSON.stringify(response))
        }
        console.log("CONSOLE_AU_KYSO_SCSH_IOS: " + JSON.stringify(response))
        if (response.data.code === 0) {
            result = response.data.data
        } else {
            result = "-1"
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_KYSO_SCSH_IOS) {
            console.log("CONSOLE_AU_KYSO_SCSH_IOS ERROR: " + JSON.stringify(error))
        }
        if (error.response && error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_KYSO_SCSH_IOS(mKySoSmartCA, key).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_KYSO_SCGSF(key) {
    let result = new Object()
    result.code = 0
    result.data = ""

    var data = new FormData();
    data.append('key', key);
    data.append('ma_ctcb', global.ma_ctcb_kc);
    console.log("CONSOLE_AU_KYSO_SCGSF: " + JSON.stringify(data))

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_KYSO_SCGSF,
        headers: setHeaderToken(),
        data: data,
        timeout: 500
    }).then(response => {
        if (global.CONSOLE_AU_KYSO_SCGSF) {
            console.log("CONSOLE_AU_KYSO_SCGSF: " + JSON.stringify(response))
        }
        console.log("CONSOLE_AU_KYSO_SCGSF: " + JSON.stringify(response))
        if (response.data.data && response.data.data.length > 0 && response.data.data[0].path_file) {
            result.code = 1
            result.data = response.data.data[0].path_file
        } else if (response.data.code == -1) {
            result.code = -1
            result.data = response.data.data
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_KYSO_SCGSF) {
            console.log("CONSOLE_AU_KYSO_SCGSF ERROR: " + JSON.stringify(error))
        }
        if (error.response && error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_KYSO_SCGSF(key).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_KYSO_UFSKK(ma_van_ban_di, ma_ctcb_ky_so, file_ky_sinh_ra) {
    let result = false
    var form = new FormData();
    form.append("ma_van_ban_di", ma_van_ban_di);
    form.append("ma_ctcb_ky_so", ma_ctcb_ky_so);
    form.append("file_ky_sinh_ra", file_ky_sinh_ra);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_KYSO_UFSKK,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_KYSO_UFSKK) {
            console.log("CONSOLE_AU_KYSO_UFSKK: " + JSON.stringify(response))
        }
        console.log("CONSOLE_AU_KYSO_UFSKK: " + JSON.stringify(response))
        if (response.data.message === "Thực thi thành công") {
            result = true
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_KYSO_UFSKK) {
            console.log("CONSOLE_AU_KYSO_UFSKK ERROR: " + JSON.stringify(error))
        }
        console.log("CONSOLE_AU_KYSO_UFSKK ERROR: " + JSON.stringify(error))
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_KYSO_UFSKK(ma_van_ban_di, ma_ctcb_ky_so, file_ky_sinh_ra).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_KYSO_GFDK(ma_van_ban_di, ma_ctcb_ky_so) {
    let result = ""
    var form = new FormData();
    form.append("ma_van_ban_di", ma_van_ban_di);
    form.append("ma_ctcb_ky_so", ma_ctcb_ky_so);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_KYSO_GFDK,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_KYSO_GFDK) {
            console.log("CONSOLE_AU_KYSO_GFDK: " + JSON.stringify(response))
        }
        console.log("CONSOLE_AU_KYSO_GFDK: " + JSON.stringify(response))
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data[0].file_ky_sinh_ra
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_KYSO_GFDK) {
            console.log("CONSOLE_AU_KYSO_GFDK ERROR: " + JSON.stringify(error))
        }
        console.log("CONSOLE_AU_KYSO_GFDK ERROR: " + JSON.stringify(error))
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_KYSO_GFDK(ma_van_ban_di, ma_ctcb_ky_so).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_KYSO_LFKS_VBNB(ma_vbnb_kc, ma_vbnb_gui_kc, file_dinh_kem) {
    let result = false
    let url = global.AU_ROOT + URL_CONFIG.AU_KYSO_LFKS_VBNB
    url += "?ma_vbnb_kc=" + ma_vbnb_kc
    url += "&ma_vbnb_gui_kc=" + ma_vbnb_gui_kc
    url += "&file_dinh_kem=" + file_dinh_kem

    await axios({
        method: "POST",
        url: url,
        headers: setHeaderToken(),
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_KYSO_LFKS_VBNB) {
            console.log("CONSOLE_AU_KYSO_LFKS_VBNB: " + JSON.stringify(response))
        }
        console.log("CONSOLE_AU_KYSO_LFKS_VBNB: " + JSON.stringify(response))
        if (response.data.message === "Thực thi thành công") {
            result = true
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_KYSO_LFKS_VBNB) {
            console.log("CONSOLE_AU_KYSO_LFKS_VBNB ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_KYSO_LFKS_VBNB(ma_vbnb_kc, ma_vbnb_gui_kc, file_dinh_kem).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_KYSO_SMARTCA_GATD() {
    let result = ""
    let url = global.AU_ROOT + URL_CONFIG.AU_KYSO_SMARTCA_GATD
    url += "?ma_ctcb=" + global.ma_ctcb_kc
    // global.CONSOLE_AU_KYSO_SMARTCA_GATD = true
    console.log("CONSOLE_AU_KYSO_SMARTCA_GATD url: " + JSON.stringify(url))
    await axios({
        method: "POST",
        url: url,
        headers: setHeaderToken(),
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_KYSO_SMARTCA_GATD) {
            console.log("CONSOLE_AU_KYSO_SMARTCA_GATD: " + JSON.stringify(response))
        }
        console.log("CONSOLE_AU_KYSO_SMARTCA_GATD: " + JSON.stringify(response))
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data[0].smartca_access_token
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_KYSO_SMARTCA_GATD) {
            console.log("CONSOLE_AU_KYSO_SMARTCA_GATD ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_KYSO_SMARTCA_GATD().then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

export default {
    AU_KYSO_DSCKS,
    AU_KYSO_KSS,
    AU_KYSO_KSSGFS,
    AU_KYSO_VTCNFKPH,
    AU_KYSO_CNVBDKPTO,
    AU_KYSO_LFKS,
    AU_KYSO_TMCKS,
    AU_KYSO_XCKS,
    AU_KYSO_CNCKS,
    AU_KYSO_TFP,
    AU_KYSO_GATH,
    AU_KYSO_GBFP,
    AU_KYSO_KSH,
    AU_KYSO_UFSKK,
    AU_KYSO_GFDK,
    AU_KYSO_LFKS_VBNB,
    AU_KYSO_SCGAT,
    AU_KYSO_SCSH,
    AU_KYSO_SCSH_IOS,
    AU_KYSO_SMARTCA_GATD,
    AU_KYSO_SCGSF,
    SendLog
}
