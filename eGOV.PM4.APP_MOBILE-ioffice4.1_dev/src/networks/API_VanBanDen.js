import { URL_CONFIG } from "./Config"
import { Platform } from "react-native"
import DeviceInfo from 'react-native-device-info'
import { Toast } from "native-base"
import axios from "axios"
import { ReplaceAll, checkVersionWeb, ReturnNumber, getCurrentDay } from "../untils/TextUntils"
import base64 from "../untils/base64"
import { AppConfig } from "../AppConfig"
import API_Login from "./API_Login"
import moment from "moment"

function showWarning(error) {
    Toast.show({ text: "Kết nối bị gián đoạn!", type: "warning", buttonText: "Đóng", position: "top", duration: 2000 })
    if (error) {
        SendLog(error)
    }
};

async function SendLog(message) {
    try {
        let obj = new Object
        let date = new Date()
        obj.Model = DeviceInfo.getModelSync()
        obj.OS = Platform.OS.toString()
        obj.SystemVersion = DeviceInfo.getSystemVersionSync()
        obj.AppVersion = DeviceInfo.getVersionSync()
        obj.CurrentTime = date.toLocaleDateString() + " " + date.toLocaleTimeString()
        obj.Module = "API_VanBanDen"
        obj.Domain = global.AU_ROOT ? global.AU_ROOT : ""
        delete message.headers
        obj.Message = message
        let text = ReplaceAll(JSON.stringify(obj), String.fromCharCode(34), "'")
        let url = AppConfig.AppDefaultLogURLSendText + text
        await axios({
            method: "POST",
            url: url,
            timeout: AppConfig.TIME_OUT_SHORT
        })
    } catch (error) {
        console.log("Send log error: " + error)
    }
}

function setHeaderToken() {
    return { "Authorization": "Bearer " + global.access_token }
};

async function AU_VBDEN_DSVBDTTTCCV(so_ky_hieu, trich_yeu, page, size, ma_don_vi_quan_tri, trang_thai_xem, trang_thai_xu_ly, ma_ctcb_nhan, nam, ma_yeu_cau = 0) {
    let result = []
    var form = new FormData();
    form.append("page", parseInt(page));
    form.append("size", parseInt(size));
    form.append("ma_loai_van_ban", 0);
    form.append("ma_linh_vuc_van_ban", 0);
    form.append("ma_yeu_cau", ma_yeu_cau);
    if (checkVersionWeb(23)) {
        form.append("nam", 0);
        if (nam !== 0) {
            form.append("nhan_tu_ngay", "01/01/" + nam);
            form.append("nhan_den_ngay", "31/12/" + nam);
        } else {
            form.append("trich_yeu", trich_yeu);
        }
    } else {
        form.append("so_ky_hieu", so_ky_hieu);
        form.append("trich_yeu", trich_yeu);
        form.append("nam", nam);
    }
    form.append("ma_don_vi_quan_tri", parseInt(ma_don_vi_quan_tri));
    form.append("trang_thai_xem", parseInt(trang_thai_xem));
    form.append("trang_thai_xu_ly", parseInt(trang_thai_xu_ly));
    form.append("ma_ctcb_nhan", parseInt(ma_ctcb_nhan))

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDEN_DSVBDTTTCCV,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_LONG
    }).then(response => {
        if (global.CONSOLE_AU_VBDEN_DSVBDTTTCCV) {
            console.log("CONSOLE_AU_VBDEN_DSVBDTTTCCV: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDEN_DSVBDTTTCCV) {
            console.log("CONSOLE_AU_VBDEN_DSVBDTTTCCV ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDEN_DSVBDTTTCCV(so_ky_hieu, trich_yeu, page, size, ma_don_vi_quan_tri, trang_thai_xem, trang_thai_xu_ly, ma_ctcb_nhan, nam, ma_yeu_cau = 0).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDEN_CVXVBD(chuoi_ma_xu_ly_den, ma_ctcb) {
    let result = false
    var form = new FormData();
    form.append("chuoi_ma_xu_ly_den", chuoi_ma_xu_ly_den);
    form.append("ma_ctcb", parseInt(ma_ctcb));

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDEN_CVXVBD,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDEN_CVXVBD) {
            console.log("CONSOLE_AU_VBDEN_CVXVBD: " + JSON.stringify(response))
        }
        if (response.data.message === "Thực thi thành công") {
            result = true
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDEN_CVXVBD) {
            console.log("CONSOLE_AU_VBDEN_CVXVBD ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDEN_CVXVBD(chuoi_ma_xu_ly_den, ma_ctcb).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDEN_VBDCT(ma_xu_ly_den, ma_van_ban_den, ma_ctcb) {
    let result = []
    var form = new FormData();
    form.append("ma_xu_ly_den", parseInt(ma_xu_ly_den));
    form.append("ma_van_ban_den", parseInt(ma_van_ban_den));
    form.append("ma_ctcb", parseInt(ma_ctcb));

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDEN_VBDCT,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDEN_VBDCT) {
            console.log("CONSOLE_AU_VBDEN_VBDCT: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result.push(response.data.data)
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDEN_VBDCT) {
            console.log("CONSOLE_AU_VBDEN_VBDCT ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDEN_VBDCT(ma_xu_ly_den, ma_van_ban_den, ma_ctcb).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDEN_CVXLNVBD(chuoi_ma_xu_ly_den, ma_ctcb) {
    let result = false
    var form = new FormData();
    form.append("ma_ctcb", parseInt(ma_ctcb));
    form.append("chuoi_ma_xu_ly_den", parseInt(chuoi_ma_xu_ly_den));

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDEN_CVXLNVBD,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDEN_CVXLNVBD) {
            console.log("CONSOLE_AU_VBDEN_CVXLNVBD: " + JSON.stringify(response))
        }
        if (response.data.message === "Thực thi thành công") {
            result = true
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDEN_CVXLNVBD) {
            console.log("CONSOLE_AU_VBDEN_CVXLNVBD ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDEN_CVXLNVBD(chuoi_ma_xu_ly_den, ma_ctcb).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDEN_CVXLVBD(ma_ctcb, ma_xu_ly_den, noi_dung_xu_ly, trang_thai_xu_ly) {
    let result = false
    var form = new FormData();
    form.append("ma_ctcb", parseInt(ma_ctcb));
    form.append("ma_xu_ly_den", ma_xu_ly_den);
    form.append("noi_dung_xu_ly", noi_dung_xu_ly);
    form.append("trang_thai_xu_ly", parseInt(trang_thai_xu_ly));

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDEN_CVXLVBD,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDEN_CVXLVBD) {
            console.log("CONSOLE_AU_VBDEN_CVXLVBD: " + JSON.stringify(response))
        }
        if (response.data.message === "Thực thi thành công") {
            result = true
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDEN_CVXLVBD) {
            console.log("CONSOLE_AU_VBDEN_CVXLVBD ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDEN_CVXLVBD(ma_ctcb, ma_xu_ly_den, noi_dung_xu_ly, trang_thai_xu_ly).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDEN_CVCNYKXL(ma_xu_ly_den, y_kien_xu_ly) {
    let result = false
    var form = new FormData();
    form.append("ma_xu_ly_den", parseInt(ma_xu_ly_den));
    form.append("y_kien_xu_ly", y_kien_xu_ly);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDEN_CVCNYKXL,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDEN_CVCNYKXL) {
            console.log("CONSOLE_AU_VBDEN_CVCNYKXL: " + JSON.stringify(response))
        }
        if (response.data.message === "Thực thi thành công") {
            result = true
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDEN_CVCNYKXL) {
            console.log("CONSOLE_AU_VBDEN_CVCNYKXL ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDEN_CVCNYKXL(ma_xu_ly_den, y_kien_xu_ly).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDEN_BPLDVBD(ma_xu_ly_den, ma_van_ban_den) {
    let result = []
    let url = global.AU_ROOT + URL_CONFIG.AU_VBDEN_BPLDVBD
    if (checkVersionWeb(40)) { url = global.AU_ROOT + URL_CONFIG.AU_VBDEN_BPLDC }
    url += "?ma_xu_ly_den=" + ma_xu_ly_den
    url += "&ma_van_ban_den=" + ma_van_ban_den

    await axios({
        method: "GET",
        url: url,
        headers: setHeaderToken(),
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDEN_BPLDVBD) {
            console.log("CONSOLE_AU_VBDEN_BPLDVBD: " + JSON.stringify(response))
        }
        if (checkVersionWeb(40)) {
            if (response.data.length > 0) {
                result = response.data
            }
        } else {
            if (response.data.message === "Lấy dữ liệu thành công") {
                result = response.data.data
            }
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDEN_BPLDVBD) {
            console.log("CONSOLE_AU_VBDEN_BPLDVBD ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDEN_BPLDVBD(ma_xu_ly_den, ma_van_ban_den).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDEN_DSCBTDVTTS(ma_don_vi) {
    let result = []
    var form = new Object()
    form.ma_don_vi = ma_don_vi
    form.type = "tree"

    await axios({
        method: "GET",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDEN_DSCBTDVTTS,
        headers: setHeaderToken(),
        params: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDEN_DSCBTDVTTS) {
            console.log("CONSOLE_AU_VBDEN_DSCBTDVTTS: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDEN_DSCBTDVTTS) {
            console.log("CONSOLE_AU_VBDEN_DSCBTDVTTS ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDEN_DSCBTDVTTS(ma_don_vi).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDEN_DSCBMD() {
    let result = []
    // var form = new Object()
    // form.ma_can_bo = parseInt(global.ma_can_bo)

    await axios({
        method: "GET",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDEN_DSCBMD + "?ma_can_bo=" + global.ma_can_bo,
        headers: setHeaderToken(),
        // params: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {

        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDEN_DSCBMD().then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDEN_DSCBTVT() {
    let result = []
    var form = new Object()
    form.ma_don_vi_quan_tri = parseInt(global.ma_don_vi_quan_tri)
    form.ma_ctcb = parseInt(global.ma_ctcb_kc)

    await axios({
        method: "GET",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDEN_DSCBTVT,
        headers: setHeaderToken(),
        params: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDEN_DSCBTVT) {
            console.log("CONSOLE_AU_VBDEN_DSCBTVT: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDEN_DSCBTVT) {
            console.log("CONSOLE_AU_VBDEN_DSCBTVT ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDEN_DSCBTVT().then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDEN_DSLDDCD(ma_don_vi, ma_ctcb) {
    let result = []
    let url = global.AU_ROOT + URL_CONFIG.AU_VBDEN_DSLDDCD
    url += "?ma_don_vi=" + ma_don_vi.toString()
    url += "&ma_ctcb=" + ma_ctcb.toString()

    await axios({
        method: "GET",
        url: url,
        headers: setHeaderToken(),
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDEN_DSLDDCD) {
            console.log("CONSOLE_AU_VBDEN_DSLDDCD: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDEN_DSLDDCD) {
            console.log("CONSOLE_AU_VBDEN_DSLDDCD ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDEN_DSLDDCD(ma_don_vi, ma_ctcb).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDEN_CLDKD(ma_xu_ly_den_cha, ma_van_ban_den, ma_ctcb_gui, ma_ctcb_nhan, noi_dung_chuyen, sms, email) {
    let result = ""
    var form = new FormData();
    form.append("ma_xu_ly_den_cha", parseInt(ma_xu_ly_den_cha))
    form.append("ma_van_ban_den", parseInt(ma_van_ban_den))
    form.append("ma_ctcb_gui", parseInt(ma_ctcb_gui))
    form.append("ma_ctcb_nhan", parseInt(ma_ctcb_nhan))
    form.append("noi_dung_chuyen", noi_dung_chuyen);
    form.append("sms", parseInt(sms))
    form.append("email", parseInt(email))

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDEN_CLDKD,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDEN_CLDKD) {
            console.log("CONSOLE_AU_VBDEN_CLDKD: " + JSON.stringify(response))
        }
        if (response.data.message === "Thực thi thành công") {
            result = response.data.id
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDEN_CLDKD) {
            console.log("CONSOLE_AU_VBDEN_CLDKD ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDEN_CLDKD(ma_xu_ly_den_cha, ma_van_ban_den, ma_ctcb_gui, ma_ctcb_nhan, noi_dung_chuyen, sms, email).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDEN_CNCBTLN(ma_can_bo, ma_loai_nhom_nguoi_nhan) {
    let result = []
    var form = new Object()
    form.ma_can_bo = ma_can_bo
    form.ma_loai_nhom_nguoi_nhan = ma_loai_nhom_nguoi_nhan

    await axios({
        method: "GET",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDEN_CNCBTLN,
        headers: setHeaderToken(),
        params: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDEN_CNCBTLN) {
            console.log("CONSOLE_AU_VBDEN_CNCBTLN: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDEN_CNCBTLN) {
            console.log("CONSOLE_AU_VBDEN_CNCBTLN ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDEN_CNCBTLN(ma_can_bo, ma_loai_nhom_nguoi_nhan).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDEN_DSNCBNVBDLDD(ma_can_bo) {
    let result = []
    let url = global.AU_ROOT + URL_CONFIG.AU_VBDEN_DSNCBNVBDLDD
    url += "?ma_can_bo=" + ma_can_bo

    await axios({
        method: "GET",
        url: url,
        headers: setHeaderToken(),
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDEN_DSNCBNVBDLDD) {
            console.log("CONSOLE_AU_VBDEN_DSNCBNVBDLDD: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDEN_DSNCBNVBDLDD) {
            console.log("CONSOLE_AU_VBDEN_DSNCBNVBDLDD ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDEN_DSNCBNVBDLDD(ma_can_bo).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDEN_CVCTH(ma_xu_ly_den_out, ma_van_ban_den, ma_xu_ly_den_cha, ma_ctcb_gui, chuoi_ma_ctcb_nhan, han_xu_ly, ma_yeu_cau, sms, noi_dung_yeu_cau) {
    let result = ""
    var form = new FormData();
    form.append("ma_xu_ly_den_out", parseInt(ma_xu_ly_den_out));
    form.append("ma_van_ban_den", parseInt(ma_van_ban_den));
    form.append("ma_xu_ly_den_cha", parseInt(ma_xu_ly_den_cha));
    form.append("ma_ctcb_gui", parseInt(ma_ctcb_gui));
    form.append("chuoi_ma_ctcb_nhan", JSON.stringify(chuoi_ma_ctcb_nhan));
    form.append("han_xu_ly", han_xu_ly);
    form.append("ma_yeu_cau", parseInt(ma_yeu_cau));
    form.append("sms", parseInt(sms));
    form.append("noi_dung_yeu_cau", noi_dung_yeu_cau);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDEN_CVCTH,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDEN_CVCTH) {
            console.log("CONSOLE_AU_VBDEN_CVCTH: " + JSON.stringify(response))
        }
        if (response.status === 200) {
            result = response.data.id
        } else {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDEN_CVCTH) {
            console.log("CONSOLE_AU_VBDEN_CVCTH ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDEN_CVCTH(ma_xu_ly_den_out, ma_van_ban_den, ma_xu_ly_den_cha, ma_ctcb_gui, chuoi_ma_ctcb_nhan, han_xu_ly, ma_yeu_cau, sms, noi_dung_yeu_cau).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDEN_CCVCAP(ma_xu_ly_den_out, ma_van_ban_den, ma_xu_ly_den_cha, ma_ctcb_gui, chuoi_ma_ctcb_nhan, han_xu_ly, ma_yeu_cau, sms, noi_dung_yeu_cau) {
    let result = 0
    var form = new FormData();
    form.append("ma_xu_ly_den_out", parseInt(ma_xu_ly_den_out));
    form.append("ma_van_ban_den", parseInt(ma_van_ban_den));
    form.append("ma_xu_ly_den_cha", parseInt(ma_xu_ly_den_cha));
    form.append("ma_ctcb_gui", parseInt(ma_ctcb_gui));
    form.append("chuoi_ma_ctcb_nhan", JSON.stringify(chuoi_ma_ctcb_nhan));
    form.append("han_xu_ly", han_xu_ly);
    form.append("ma_yeu_cau", parseInt(ma_yeu_cau));
    form.append("sms", parseInt(sms));
    form.append("noi_dung_yeu_cau", noi_dung_yeu_cau);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDEN_CCVCAP,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDEN_CCVCAP) {
            console.log("CONSOLE_AU_VBDEN_CCVCAP : " + JSON.stringify(response))
        }
        if (response.status === 200) {
            console.log("CONSOLE_AU_VBDEN_CCVCAP: " + JSON.stringify(response))
            result = response.data.trang_thai
        } else {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDEN_CCVCAP) {
            console.log("CONSOLE_AU_VBDEN_CCVCAP ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDEN_CCVCAP(ma_xu_ly_den_out, ma_van_ban_den, ma_xu_ly_den_cha, ma_ctcb_gui, chuoi_ma_ctcb_nhan, han_xu_ly, ma_yeu_cau, sms, noi_dung_yeu_cau).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDEN_DSVBDCDCLD(so_ky_hieu, trich_yeu, ma_ctcb_duyet, ma_don_vi_quan_tri, page, size, nam) {
    let result = []
    var form = new FormData();
    if (checkVersionWeb(23)) {
        form.append("nam", 0);
        if (nam !== 0) {
            form.append("tu_ngay", "01/01/" + nam);
            form.append("den_ngay", "31/12/" + nam);
        } else {
            form.append("trich_yeu", trich_yeu);
        }
    } else {
        form.append("so_ky_hieu", so_ky_hieu);
        form.append("trich_yeu", trich_yeu);
        form.append("nam", nam);
    }
    form.append("ten_co_quan_ban_hanh", "");
    form.append("ma_ctcb_duyet", parseInt(ma_ctcb_duyet));
    form.append("ma_don_vi_quan_tri", parseInt(ma_don_vi_quan_tri));
    form.append("page", parseInt(page));
    form.append("size", parseInt(size));

    console.log("CONSOLE_AU_VBDEN_DSVBDCDCLD: " + JSON.stringify(form))
    global.CONSOLE_AU_VBDEN_DSVBDCDCLD = true;
    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDEN_DSVBDCDCLD,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDEN_DSVBDCDCLD) {
            console.log("CONSOLE_AU_VBDEN_DSVBDCDCLD: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDEN_DSVBDCDCLD) {
            console.log("CONSOLE_AU_VBDEN_DSVBDCDCLD ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDEN_DSVBDCDCLD(so_ky_hieu, trich_yeu, ma_ctcb_duyet, ma_don_vi_quan_tri, page, size, nam).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDEN_DSVBDUQDCLD(so_ky_hieu, trich_yeu, ma_ctcb_duyet, ma_don_vi_quan_tri, page, size, nam) {
    let result = []
    var form = new FormData();
    if (checkVersionWeb(23)) {
        form.append("nam", 0);
        if (nam !== 0) {
            form.append("tu_ngay", "01/01/" + nam);
            form.append("den_ngay", "31/12/" + nam);
        } else {
            form.append("trich_yeu", trich_yeu);
        }
    } else {
        form.append("so_ky_hieu", so_ky_hieu);
        form.append("trich_yeu", trich_yeu);
        form.append("nam", nam);
    }
    form.append("ten_co_quan_ban_hanh", "");
    form.append("ma_ctcb_duyet", parseInt(ma_ctcb_duyet));
    form.append("ma_don_vi_quan_tri", parseInt(ma_don_vi_quan_tri));
    form.append("page", parseInt(page));
    form.append("size", parseInt(size));

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDEN_DSVBDUQDCLD,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_LONG
    }).then(response => {
        if (global.CONSOLE_AU_VBDEN_DSVBDUQDCLD) {
            console.log("CONSOLE_AU_VBDEN_DSVBDUQDCLD: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDEN_DSVBDUQDCLD) {
            console.log("CONSOLE_AU_VBDEN_DSVBDUQDCLD ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDEN_DSVBDUQDCLD(so_ky_hieu, trich_yeu, ma_ctcb_duyet, ma_don_vi_quan_tri, page, size, nam).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDEN_DSVBDDCTHCLD(so_ky_hieu, trich_yeu, ma_ctcb_duyet, ma_don_vi_quan_tri, page, size, nam) {
    global.CONSOLE_AU_VBDEN_DSVBDDCTHCLD = true;
    let result = []
    var form = new FormData();
    if (global.khoang_thoi_gian_danh_sach_vb !== "0") {
        form.append("nam", 0);
        if (nam !== 0) {
            form.append("tu_ngay", moment(getCurrentDay(), "DD/MM/YYYY").subtract(parseInt(global.khoang_thoi_gian_danh_sach_vb), "months").format("DD/MM/YYYY"));
            form.append("den_ngay", getCurrentDay());
            console.log("AU_VBDEN_DSVBDDCTHCLD tu_ngay den_ngay")
            console.log(form)
        } else {
            form.append("trich_yeu", trich_yeu);
        }
    } else {
        if (checkVersionWeb(23)) {
            form.append("nam", 0);
            if (nam !== 0) {
                form.append("tu_ngay", "01/01/" + nam);
                form.append("den_ngay", "31/12/" + nam);
            } else {
                form.append("trich_yeu", trich_yeu);
            }
        } else {
            form.append("so_ky_hieu", so_ky_hieu);
            form.append("trich_yeu", trich_yeu);
            form.append("nam", nam);
        }
    }

    form.append("ten_co_quan_ban_hanh", "");
    form.append("ma_ctcb_duyet", parseInt(ma_ctcb_duyet));
    form.append("ma_don_vi_quan_tri", parseInt(ma_don_vi_quan_tri));
    form.append("page", parseInt(page));
    form.append("size", parseInt(size));
    console.log("CONSOLE_AU_VBDEN_DSVBDDCTHCLD REQUEST: " + JSON.stringify(form));
    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDEN_DSVBDDCTHCLD,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDEN_DSVBDDCTHCLD) {
            console.log("CONSOLE_AU_VBDEN_DSVBDDCTHCLD: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        console.log("CONSOLE_AU_VBDEN_DSVBDDCTHCLD ERROR: ")
        if (global.CONSOLE_AU_VBDEN_DSVBDDCTHCLD) {
            console.log("CONSOLE_AU_VBDEN_DSVBDDCTHCLD ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDEN_DSVBDDCTHCLD(so_ky_hieu, trich_yeu, ma_ctcb_duyet, ma_don_vi_quan_tri, page, size, nam).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDEN_LDDVBD(ma_xu_ly_den_kc, ma_van_ban_den, lanh_dao_chuyen_tiep, ma_ctcb_duyet, ma_ctcb_gui, noi_dung_chuyen, han_xu_ly_chung, chuoi_thong_tin_gui_sms) {
    let result = false
    var form = new FormData();
    form.append("ma_xu_ly_den_kc", parseInt(ma_xu_ly_den_kc))
    form.append("ma_van_ban_den", parseInt(ma_van_ban_den))
    form.append("lanh_dao_chuyen_tiep", parseInt(lanh_dao_chuyen_tiep))
    form.append("yeu_cau", 1)
    form.append("ma_ctcb_duyet", parseInt(ma_ctcb_duyet))
    form.append("ma_ctcb_gui", parseInt(ma_ctcb_gui))
    form.append("noi_dung_chuyen", noi_dung_chuyen);
    form.append("han_xu_ly_chung", han_xu_ly_chung);
    form.append("chuoi_thong_tin_gui_sms", JSON.stringify(chuoi_thong_tin_gui_sms));

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDEN_LDDVBD,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDEN_LDDVBD) {
            console.log("CONSOLE_AU_VBDEN_LDDVBD: " + JSON.stringify(response))
        }
        if (response.status === 200) {
            result = true
        } else {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDEN_LDDVBD) {
            console.log("CONSOLE_AU_VBDEN_LDDVBD ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDEN_LDDVBD(ma_xu_ly_den_kc, ma_van_ban_den, lanh_dao_chuyen_tiep, ma_ctcb_duyet, ma_ctcb_gui, noi_dung_chuyen, han_xu_ly_chung, chuoi_thong_tin_gui_sms).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDEN_LDUQDVBD(ma_xu_ly_den_kc, ma_van_ban_den, lanh_dao_chuyen_tiep, ma_ctcb_duyet, ma_ctcb_gui, noi_dung_chuyen, han_xu_ly_chung, chuoi_thong_tin_gui_sms) {
    let result = false
    var form = new FormData();
    form.append("ma_xu_ly_den_kc", parseInt(ma_xu_ly_den_kc))
    form.append("ma_van_ban_den", parseInt(ma_van_ban_den))
    form.append("lanh_dao_chuyen_tiep", parseInt(lanh_dao_chuyen_tiep))
    form.append("yeu_cau", 3)
    form.append("ma_ctcb_duyet", parseInt(ma_ctcb_duyet))
    form.append("ma_ctcb_gui", parseInt(ma_ctcb_gui))
    form.append("noi_dung_chuyen", noi_dung_chuyen);
    form.append("han_xu_ly_chung", han_xu_ly_chung);
    form.append("chuoi_thong_tin_gui_sms", JSON.stringify(chuoi_thong_tin_gui_sms));

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDEN_LDUQDVBD,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDEN_LDUQDVBD) {
            console.log("CONSOLE_AU_VBDEN_LDUQDVBD: " + JSON.stringify(response))
        }
        if (response.status === 200) {
            result = true
        } else {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDEN_LDUQDVBD) {
            console.log("CONSOLE_AU_VBDEN_LDUQDVBD ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDEN_LDUQDVBD(ma_xu_ly_den_kc, ma_van_ban_den, lanh_dao_chuyen_tiep, ma_ctcb_duyet, ma_ctcb_gui, noi_dung_chuyen, han_xu_ly_chung, chuoi_thong_tin_gui_sms).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDEN_LDDHTVBD(ma_ctcb_duyet, ma_van_ban_den, ma_xu_ly_den_kc, y_kien_xu_ly_hoan_tat) {
    let result = false
    var form = new FormData();
    form.append("ma_ctcb_duyet", parseInt(ma_ctcb_duyet))
    form.append("ma_van_ban_den", parseInt(ma_van_ban_den))
    form.append("ma_xu_ly_den_kc", parseInt(ma_xu_ly_den_kc))
    form.append("y_kien_xu_ly_hoan_tat", y_kien_xu_ly_hoan_tat);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDEN_LDDHTVBD,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDEN_LDDHTVBD) {
            console.log("CONSOLE_AU_VBDEN_LDDHTVBD: " + JSON.stringify(response))
        }
        if (response.status === 200) {
            result = true
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDEN_LDDHTVBD) {
            console.log("CONSOLE_AU_VBDEN_LDDHTVBD ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDEN_LDDHTVBD(ma_ctcb_duyet, ma_van_ban_den, ma_xu_ly_den_kc, y_kien_xu_ly_hoan_tat).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

// TRA CUU VAN BAN
async function AU_VANBAN_TCVB(so_ky_hieu, trich_yeu) {
    let result = []
    var form = new FormData();
    form.append("loc_tra_cuu", 0);
    form.append("ma_don_vi", parseInt(global.ma_don_vi));
    form.append("ma_don_vi_quan_tri", parseInt(global.ma_don_vi_quan_tri));
    form.append("ma_loai_van_ban", 'all');
    form.append("ma_so_van_ban", 0);
    form.append("nam", -1);
    form.append("page", 1);
    form.append("size", 1000);
    form.append("so_ky_hieu", so_ky_hieu);
    form.append("trich_yeu", trich_yeu);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VANBAN_TCVB,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VANBAN_TCVB) {
            console.log("CONSOLE_AU_VANBAN_TCVB: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VANBAN_TCVB) {
            console.log("CONSOLE_AU_VANBAN_TCVB ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VANBAN_TCVB(so_ky_hieu, trich_yeu).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VANBAN_DSVBLQDK(ma_van_ban) {
    let result = ""
    let url = global.AU_ROOT + URL_CONFIG.AU_VANBAN_DSVBLQDK
    url += "?ma_van_ban=" + ma_van_ban

    await axios({
        method: "GET",
        url: url,
        headers: setHeaderToken(),
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VANBAN_DSVBLQDK) {
            console.log("CONSOLE_AU_VANBAN_DSVBLQDK: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            let appTemp = []
            response.data.data.forEach(function (element) {
                appTemp.push(element.file_van_ban)
            })
            result = appTemp.join(":")
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VANBAN_DSVBLQDK) {
            console.log("CONSOLE_AU_VANBAN_DSVBLQDK ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VANBAN_DSVBLQDK(ma_van_ban).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDEN_LDDPD(ma_van_ban_den) {
    let result = false
    let url = global.AU_ROOT + URL_CONFIG.AU_VBDEN_LDDPD
    url += "?ma_van_ban_den=" + ma_van_ban_den

    await axios({
        method: "GET",
        url: url,
        headers: setHeaderToken(),
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDEN_LDDPD) {
            console.log("CONSOLE_AU_VBDEN_LDDPD: " + JSON.stringify(response))
        }
        console.log("CONSOLE_AU_VBDEN_LDDPD: " + JSON.stringify(response))
        if (response.data.message === "Thực thi thành công") {
            result = true
        } else {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDEN_LDDPD) {
            console.log("CONSOLE_AU_VBDEN_LDDPD ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDEN_LDDPD(ma_van_ban_den).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

//VĂN THƯ
async function AU_VBDEN_DSDMSVBD(ma_don_vi_quan_tri = global.ma_don_vi_quan_tri, nam = new Date().getFullYear()) {
    let result = []
    let url = global.AU_ROOT + URL_CONFIG.AU_VBDEN_DSDMSVBD
    url += "?ma_don_vi_quan_tri=" + ma_don_vi_quan_tri
    url += "&nam=" + nam

    await axios({
        method: "GET",
        url: url,
        headers: setHeaderToken(),
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDEN_DSDMSVBD) {
            console.log("CONSOLE_AU_VBDEN_DSDMSVBD: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDEN_DSDMSVBD) {
            console.log("CONSOLE_AU_VBDEN_DSDMSVBD ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDEN_DSDMSVBD(ma_don_vi_quan_tri = global.ma_don_vi_quan_tri, nam = new Date().getFullYear()).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDEN_LSDTSVBD(ma_so_vb_den, nam = new Date().getFullYear()) {
    let result = null
    let url = global.AU_ROOT + URL_CONFIG.AU_VBDEN_LSDTSVBD
    url += "?ma_so_vb_den=" + ma_so_vb_den
    url += "&nam=" + nam

    await axios({
        method: "GET",
        url: url,
        headers: setHeaderToken(),
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDEN_LSDTSVBD) {
            console.log("CONSOLE_AU_VBDEN_LSDTSVBD: " + JSON.stringify(response))
        }
        if (checkVersionWeb(52)) {
            if (response.data.message === "Lấy dữ liệu thành công") {
                result = response.data.data.so_den_hien_tai
            } else {
                SendLog(response)
            }
        } else {
            if (response.data.message === "Thực thi thành công") {
                result = response.data.id
            } else {
                SendLog(response)
            }
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDEN_LSDTSVBD) {
            console.log("CONSOLE_AU_VBDEN_LSDTSVBD ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDEN_LSDTSVBD(ma_so_vb_den, nam = new Date().getFullYear()).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDEN_DSCDK() {
    let result = []
    let url = global.AU_ROOT + URL_CONFIG.AU_VBDEN_DSCDK

    await axios({
        method: "GET",
        url: url,
        headers: setHeaderToken(),
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDEN_DSCDK) {
            console.log("CONSOLE_AU_VBDEN_DSCDK: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDEN_DSCDK) {
            console.log("CONSOLE_AU_VBDEN_DSCDK ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDEN_DSCDK().then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDEN_DSCDM() {
    let result = []
    let url = global.AU_ROOT + URL_CONFIG.AU_VBDEN_DSCDM

    await axios({
        method: "GET",
        url: url,
        headers: setHeaderToken(),
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDEN_DSCDM) {
            console.log("CONSOLE_AU_VBDEN_DSCDM: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDEN_DSCDM) {
            console.log("CONSOLE_AU_VBDEN_DSCDM ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDEN_DSCDM().then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDEN_DSLVVB() {
    let result = []
    let url = global.AU_ROOT + URL_CONFIG.AU_VBDEN_DSLVVB

    await axios({
        method: "GET",
        url: url,
        headers: setHeaderToken(),
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDEN_DSLVVB) {
            console.log("CONSOLE_AU_VBDEN_DSLVVB: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDEN_DSLVVB) {
            console.log("CONSOLE_AU_VBDEN_DSLVVB ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDEN_DSLVVB().then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDEN_DSLVB() {
    let result = []
    let url = global.AU_ROOT + URL_CONFIG.AU_VBDEN_DSLVB

    await axios({
        method: "GET",
        url: url,
        headers: setHeaderToken(),
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDEN_DSLVB) {
            console.log("CONSOLE_AU_VBDEN_DSLVB: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDEN_DSLVB) {
            console.log("CONSOLE_AU_VBDEN_DSLVB ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDEN_DSLVB().then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDEN_DSLDDVBDE(ma_ctcb = global.ma_ctcb_kc, ma_don_vi = global.ma_don_vi) {
    let result = []
    let url = global.AU_ROOT + URL_CONFIG.AU_VBDEN_DSLDDVBDE
    url += "?ma_ctcb=" + ma_ctcb
    url += "&ma_don_vi=" + ma_don_vi

    await axios({
        method: "GET",
        url: url,
        headers: setHeaderToken(),
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDEN_DSLDDVBDE) {
            console.log("CONSOLE_AU_VBDEN_DSLDDVBDE: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDEN_DSLDDVBDE) {
            console.log("CONSOLE_AU_VBDEN_DSLDDVBDE ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDEN_DSLDDVBDE(ma_ctcb = global.ma_ctcb_kc, ma_don_vi = global.ma_don_vi).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDEN_DSVBDTCVT(so_ky_hieu, trich_yeu, page, size, nam) {
    let result = []
    var form = new FormData();
    form.append("so_ky_hieu", so_ky_hieu);
    form.append("trich_yeu", trich_yeu);
    form.append("page", parseInt(page));
    form.append("size", parseInt(size));
    if (checkVersionWeb(23) && nam !== 0) {
        form.append("nam", 0);
        form.append("tu_ngay", "01/01/" + nam);
        form.append("den_ngay", "31/12/" + nam);
        form.append("ngay_duyet_tu_ngay", "01/01/" + nam);
        form.append("ngay_duyet_den_ngay", "31/12/" + nam);
    } else {
        form.append("nam", nam);
    }
    form.append("ma_ctcb", parseInt(global.ma_ctcb_kc));
    form.append("ma_ctcb_van_thu", parseInt(global.ma_ctcb_kc));
    form.append("ma_don_vi_nhan", parseInt(global.ma_don_vi_quan_tri));
    form.append("ma_don_vi_quan_tri", parseInt(global.ma_don_vi_quan_tri));
    form.append("ma_so_vb_den", 0);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDEN_DSVBDTCVT,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_LONG
    }).then(response => {
        if (global.CONSOLE_AU_VBDEN_DSVBDTCVT) {
            console.log("CONSOLE_AU_VBDEN_DSVBDTCVT: " + JSON.stringify(response))
        }
        console.log("CONSOLE_AU_VBDEN_DSVBDTCVT: " + JSON.stringify(response))
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDEN_DSVBDTCVT) {
            console.log("CONSOLE_AU_VBDEN_DSVBDTCVT ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDEN_DSVBDTCVT(so_ky_hieu, trich_yeu, page, size, nam).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDEN_DSVBDML(so_ky_hieu, trich_yeu, page, size, nam) {
    let result = []
    var form = new FormData();
    form.append("so_ky_hieu", so_ky_hieu);
    form.append("trich_yeu", trich_yeu);
    form.append("page", parseInt(page));
    form.append("size", parseInt(size));
    if (checkVersionWeb(23) && nam !== 0) {
        form.append("nam", 0);
        form.append("tu_ngay", "01/01/" + nam);
        form.append("den_ngay", "31/12/" + nam);
        form.append("ngay_duyet_tu_ngay", "01/01/" + nam);
        form.append("ngay_duyet_den_ngay", "31/12/" + nam);
    } else {
        form.append("nam", nam);
    }
    form.append("ma_ctcb", parseInt(global.ma_ctcb_kc));
    form.append("ma_ctcb_van_thu", parseInt(global.ma_ctcb_kc));
    form.append("ma_don_vi_quan_tri", parseInt(global.ma_don_vi_quan_tri));
    form.append("ma_so_vb_den", 0);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDEN_DSVBDML,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_LONG
    }).then(response => {
        if (global.CONSOLE_AU_VBDEN_DSVBDML) {
            console.log("CONSOLE_AU_VBDEN_DSVBDML: " + JSON.stringify(response))
        }
        console.log("CONSOLE_AU_VBDEN_DSVBDML: " + JSON.stringify(response))
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDEN_DSVBDML) {
            console.log("CONSOLE_AU_VBDEN_DSVBDML ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDEN_DSVBDML(so_ky_hieu, trich_yeu, page, size, nam).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDEN_DSVBLDTLCVT(so_ky_hieu, trich_yeu, page, size, nam) {
    let result = []
    var form = new FormData();
    form.append("so_ky_hieu", so_ky_hieu);
    form.append("trich_yeu", trich_yeu);
    form.append("page", parseInt(page));
    form.append("size", parseInt(size));
    if (checkVersionWeb(23) && nam !== 0) {
        form.append("nam", 0);
        form.append("tu_ngay", "01/01/" + nam);
        form.append("den_ngay", "31/12/" + nam);
        form.append("ngay_duyet_tu_ngay", "01/01/" + nam);
        form.append("ngay_duyet_den_ngay", "31/12/" + nam);
    } else {
        form.append("nam", nam);
    }
    form.append("ma_ctcb", parseInt(global.ma_ctcb_kc));
    form.append("ma_ctcb_van_thu", parseInt(global.ma_ctcb_kc));
    form.append("ma_don_vi_quan_tri", parseInt(global.ma_don_vi_quan_tri));
    form.append("ma_so_vb_den", 0);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDEN_DSVBLDTLCVT,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_LONG
    }).then(response => {
        if (global.CONSOLE_AU_VBDEN_DSVBLDTLCVT) {
            console.log("CONSOLE_AU_VBDEN_DSVBLDTLCVT: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDEN_DSVBLDTLCVT) {
            console.log("CONSOLE_AU_VBDEN_DSVBLDTLCVT ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDEN_DSVBLDTLCVT(so_ky_hieu, trich_yeu, page, size, nam).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDEN_DSVBDCDCVT(so_ky_hieu, trich_yeu, page, size, nam) {
    let result = []
    var form = new FormData();
    form.append("so_ky_hieu", so_ky_hieu);
    form.append("trich_yeu", trich_yeu);
    form.append("page", parseInt(page));
    form.append("size", parseInt(size));
    if (checkVersionWeb(23) && nam !== 0) {
        form.append("nam", 0);
        form.append("tu_ngay", "01/01/" + nam);
        form.append("den_ngay", "31/12/" + nam);
        form.append("ngay_duyet_tu_ngay", "01/01/" + nam);
        form.append("ngay_duyet_den_ngay", "31/12/" + nam);
    } else {
        form.append("nam", nam);
    }
    form.append("ma_ctcb", parseInt(global.ma_ctcb_kc));
    form.append("ma_ctcb_van_thu", parseInt(global.ma_ctcb_kc));
    form.append("ma_don_vi_quan_tri", parseInt(global.ma_don_vi_quan_tri));
    form.append("ma_so_vb_den", 0);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDEN_DSVBDCDCVT,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_LONG
    }).then(response => {
        if (global.CONSOLE_AU_VBDEN_DSVBDCDCVT) {
            console.log("CONSOLE_AU_VBDEN_DSVBDCDCVT: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDEN_DSVBDCDCVT) {
            console.log("CONSOLE_AU_VBDEN_DSVBDCDCVT ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDEN_DSVBDCDCVT(so_ky_hieu, trich_yeu, page, size, nam).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDEN_DSVBDCVTC(so_ky_hieu, trich_yeu, page, size, nam) {
    let result = []
    var form = new FormData();
    form.append("so_ky_hieu", so_ky_hieu);
    form.append("trich_yeu", trich_yeu);
    form.append("page", parseInt(page));
    form.append("size", parseInt(size));
    if (checkVersionWeb(23) && nam !== 0) {
        form.append("nam", 0);
        form.append("tu_ngay", "01/01/" + nam);
        form.append("den_ngay", "31/12/" + nam);
        form.append("ngay_duyet_tu_ngay", "01/01/" + nam);
        form.append("ngay_duyet_den_ngay", "31/12/" + nam);
    } else {
        form.append("nam", nam);
    }
    form.append("ma_ctcb", parseInt(global.ma_ctcb_kc));
    form.append("ma_ctcb_van_thu", parseInt(global.ma_ctcb_kc));
    form.append("ma_don_vi_quan_tri", parseInt(global.ma_don_vi_quan_tri));
    form.append("ma_so_vb_den", 0);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDEN_DSVBDCVTC,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_LONG
    }).then(response => {
        if (global.CONSOLE_AU_VBDEN_DSVBDCVTC) {
            console.log("CONSOLE_AU_VBDEN_DSVBDCVTC: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDEN_DSVBDCVTC) {
            console.log("CONSOLE_AU_VBDEN_DSVBDCVTC ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDEN_DSVBDCVTC(so_ky_hieu, trich_yeu, page, size, nam).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDEN_DSVBDVTDC(so_ky_hieu, trich_yeu, page, size, nam) {
    let result = []
    var form = new FormData();
    form.append("so_ky_hieu", so_ky_hieu);
    form.append("trich_yeu", trich_yeu);
    form.append("page", parseInt(page));
    form.append("size", parseInt(size));
    if (checkVersionWeb(23) && nam !== 0) {
        form.append("nam", 0);
        form.append("tu_ngay", "01/01/" + nam);
        form.append("den_ngay", "31/12/" + nam);
        form.append("ngay_duyet_tu_ngay", "01/01/" + nam);
        form.append("ngay_duyet_den_ngay", "31/12/" + nam);
        form.append("tgian_temp", "01/01/" + nam + " - " + "31/12/" + nam);
    } else {
        form.append("nam", nam);
    }
    form.append("ma_ctcb", parseInt(global.ma_ctcb_kc));
    form.append("ma_ctcb_van_thu", parseInt(global.ma_ctcb_kc));
    form.append("ma_don_vi_quan_tri", parseInt(global.ma_don_vi_quan_tri));
    form.append("ma_so_vb_den", 0);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDEN_DSVBDVTDC,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_LONG
    }).then(response => {
        if (global.CONSOLE_AU_VBDEN_DSVBDVTDC) {
            console.log("CONSOLE_AU_VBDEN_DSVBDVTDC: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDEN_DSVBDVTDC) {
            console.log("CONSOLE_AU_VBDEN_DSVBDVTDC ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDEN_DSVBDVTDC(so_ky_hieu, trich_yeu, page, size, nam).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDEN_DSVBDHCVT(so_ky_hieu, trich_yeu, page, size, nam) {
    let result = []
    var form = new FormData();
    form.append("so_ky_hieu", so_ky_hieu);
    form.append("trich_yeu", trich_yeu);
    form.append("page", parseInt(page));
    form.append("size", parseInt(size));
    if (checkVersionWeb(23) && nam !== 0) {
        form.append("nam", 0);
        form.append("tu_ngay", "01/01/" + nam);
        form.append("den_ngay", "31/12/" + nam);
        form.append("ngay_duyet_tu_ngay", "01/01/" + nam);
        form.append("ngay_duyet_den_ngay", "31/12/" + nam);
    } else {
        form.append("nam", nam);
    }
    form.append("ma_ctcb", parseInt(global.ma_ctcb_kc));
    form.append("ma_ctcb_van_thu", parseInt(global.ma_ctcb_kc));
    form.append("ma_don_vi_nhan", parseInt(global.ma_don_vi));
    form.append("ma_don_vi_quan_tri", parseInt(global.ma_don_vi_quan_tri));
    form.append("ma_so_vb_den", 0);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDEN_DSVBDHCVT,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_LONG
    }).then(response => {
        if (global.CONSOLE_AU_VBDEN_DSVBDHCVT) {
            console.log("CONSOLE_AU_VBDEN_DSVBDHCVT: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDEN_DSVBDHCVT) {
            console.log("CONSOLE_AU_VBDEN_DSVBDHCVT ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDEN_DSVBDHCVT(so_ky_hieu, trich_yeu, page, size, nam).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDEN_UTLT(chuc_nang, don_vi, ma_ctcb, partition, ten_file) {
    let result = ""
    var form = new FormData();
    form.append("chuc_nang", chuc_nang);
    form.append("don_vi", don_vi);
    form.append("ma_ctcb", ma_ctcb);
    form.append("partition", partition);
    form.append("ten_file", ten_file);

    console.log("AU_VBDEN_UTLT form: " + JSON.stringify(form))

    await axios({
        method: "POST",
        url: global.AU_ROOT_FILE + URL_CONFIG.AU_VBDEN_UTLT,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDEN_UTLT) {
            console.log("CONSOLE_AU_VBDEN_UTLT: " + JSON.stringify(response))
        }
        console.log("CONSOLE_AU_VBDEN_UTLT: " + JSON.stringify(response))
        if (response.data !== "") {
            result = response.data
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDEN_UTLT) {
            console.log("CONSOLE_AU_VBDEN_UTLT ERROR: " + JSON.stringify(error))
        }
        console.log("CONSOLE_AU_VBDEN_UTLT ERROR: " + JSON.stringify(error))
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDEN_UTLT(chuc_nang, don_vi, ma_ctcb, partition, ten_file).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDEN_CNCVBDDTLT(ma_van_ban, chuoi_file_moi) {
    let result = false
    let url = global.AU_ROOT + URL_CONFIG.AU_VBDEN_CNCVBDDTLT
    url += "?ma_van_ban=" + ma_van_ban
    url += "&chuoi_file_moi=" + chuoi_file_moi

    console.log("AU_VBDEN_CNCVBDDTLT url: " + url)

    await axios({
        method: "GET",
        url: url,
        headers: setHeaderToken(),
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDEN_CNCVBDDTLT) {
            console.log("CONSOLE_AU_VBDEN_CNCVBDDTLT: " + JSON.stringify(response))
        }
        console.log("CONSOLE_AU_VBDEN_CNCVBDDTLT: " + JSON.stringify(response))
        if (response.data.message === "Thực thi thành công") {
            result = true
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDEN_CNCVBDDTLT) {
            console.log("CONSOLE_AU_VBDEN_CNCVBDDTLT ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDEN_CNCVBDDTLT(ma_van_ban, chuoi_file_moi).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDEN_KTTSKH(so_ky_hieu, ten_cqbh, ma_van_ban_den, ma_dvqt, nam) {
    let result = false
    var form = new FormData();
    form.append("so_ky_hieu", so_ky_hieu);
    form.append("ten_cqbh", ten_cqbh);
    form.append("ma_van_ban_den", ma_van_ban_den);
    form.append("ma_dvqt", ma_dvqt);
    form.append("nam", nam);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDEN_KTTSKH,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDEN_KTTSKH) {
            console.log("CONSOLE_AU_VBDEN_KTTSKH: " + JSON.stringify(response))
        }
        if (response.data.message === "Thực thi thành công") {
            result = response.data.trung_so_ky_hieu === 0 ? true : false
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDEN_KTTSKH) {
            console.log("CONSOLE_AU_VBDEN_KTTSKH ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDEN_KTTSKH(so_ky_hieu, ten_cqbh, ma_van_ban_den, ma_dvqt, nam).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDEN_TMVBD(item) {
    let result = false
    var form = new FormData();
    form.append("ma_van_ban_den_kc", item.ma_van_ban_den_kc);
    form.append("ma_van_ban_kc", item.ma_van_ban_kc);
    form.append("ma_xu_ly_den", item.ma_xu_ly_den);
    form.append("so_den", parseInt(item.so_den));
    form.append("trich_yeu", item.trich_yeu);
    form.append("so_ky_hieu", item.so_ky_hieu);
    form.append("ngay_nhan", item.ngay_nhan);
    form.append("ngay_xem", item.ngay_xem);
    form.append("file_van_ban_bs", item.file_van_ban_bs);
    form.append("ten_co_quan_ban_hanh", item.ten_co_quan_ban_hanh);
    form.append("ten_don_vi_gui", item.ten_don_vi_gui);
    form.append("file_van_ban", item.file_van_ban);
    form.append("ma_linh_vuc_van_ban", item.ma_linh_vuc_van_ban);
    form.append("ma_loai_van_ban", item.ma_loai_van_ban);
    form.append("ma_cap_do_khan", item.ma_cap_do_khan);
    form.append("ma_cap_do_mat", item.ma_cap_do_mat);
    form.append("ngay_den", item.ngay_den);
    form.append("han_xu_ly_chung", item.han_xu_ly_chung);
    form.append("ngay_ban_hanh", item.ngay_ban_hanh);
    form.append("so_ioffice", item.so_ioffice);
    form.append("gui_kem_vb_giay", item.gui_kem_vb_giay);
    form.append("so_ban_phat_hanh", item.so_ban_phat_hanh);
    form.append("so_trang_vb", item.so_trang_vb);
    form.append("nguoi_ky", item.nguoi_ky);
    form.append("noi_luu_ban_chinh", item.noi_luu_ban_chinh);
    form.append("ma_ctcb_duyet", item.ma_ctcb_duyet);
    form.append("hien_thi_vblq", item.hien_thi_vblq);
    form.append("chuoi_vb_dien_tu", item.chuoi_vb_dien_tu);
    form.append("da_luu_file", item.da_luu_file);
    form.append("r", item.r);

    form.append("ma_don_vi", global.ma_don_vi);
    form.append("ma_don_vi_quan_tri", global.ma_don_vi_quan_tri);
    form.append("ma_ctcb_tao", global.ma_ctcb_kc);
    form.append("ma_van_ban_den_inout", item.ma_van_ban_den_kc);
    form.append("ma_xu_ly_den_inout", item.ma_xu_ly_den);
    form.append("gui_kem_van_ban_giay", item.gui_kem_van_ban_giay);
    form.append("ma_so_vb_den", item.ma_so_vb_den);
    form.append("chuoi_ma_vb_lien_quan", "");
    form.append("trang_thai_van_ban_den", item.trang_thai_van_ban_den);
    form.append("ngay_duyet", "");
    form.append("ghi_chu", item.ghi_chu);
    form.append("sms", item.sms);
    form.append("email", 0);
    console.log("AU_VBDEN_TMVBD:" + JSON.stringify(form))
    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDEN_TMVBD,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_LONG
    }).then(response => {
        if (global.CONSOLE_AU_VBDEN_TMVBD) {
            console.log("CONSOLE_AU_VBDEN_TMVBD: " + JSON.stringify(response))
        }
        if (response.data.message === "Thực thi thành công") {
            result = true
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDEN_TMVBD) {
            console.log("CONSOLE_AU_VBDEN_TMVBD ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDEN_TMVBD(item).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDEN_TMVBDLN(item) {
    let result = false
    var form = new FormData();
    form.append("but_phe_cb_duyet", item.but_phe_cb_duyet);
    form.append("di_dong_nguoi_gui", item.di_dong_nguoi_gui);
    form.append("di_dong_nguoi_luu", item.di_dong_nguoi_luu);
    form.append("file_van_ban", item.file_van_ban);
    form.append("file_van_ban_bs", item.file_van_ban_bs);
    form.append("file_vb_goc", item.file_vb_goc);
    form.append("hien_thi_vblq", item.hien_thi_vblq);
    form.append("ho_ten_nguoi_luu", item.ho_ten_nguoi_luu);
    form.append("ho_va_ten_can_bo_gui", item.ho_va_ten_can_bo_gui);
    form.append("lanh_dao_chuyen_vb", item.lanh_dao_chuyen_vb);
    form.append("loai_van_ban_khi_moi_tao", item.loai_van_ban_khi_moi_tao);
    form.append("ma_cap_do_khan", item.ma_cap_do_khan);
    form.append("ma_cap_do_mat", item.ma_cap_do_mat);
    form.append("ma_ctcb_duyet", item.ma_ctcb_duyet);
    form.append("ma_ctcb_gui", item.ma_ctcb_gui);
    form.append("ma_ctcb_luu", item.ma_ctcb_luu);
    form.append("ma_ctcb_tao", item.ma_ctcb_tao);
    form.append("ma_dinh_danh", item.ma_dinh_danh);
    form.append("ma_dinh_danh_gui", item.ma_dinh_danh_gui);
    form.append("ma_dinh_danh_vb", item.ma_dinh_danh_vb);
    form.append("ma_don_vi", item.ma_don_vi);
    form.append("ma_don_vi_quan_tri", item.ma_don_vi_quan_tri);
    form.append("ma_don_vi_quan_tri_tao", item.ma_don_vi_quan_tri_tao);
    form.append("ma_don_vi_tao", item.ma_don_vi_tao);
    form.append("ma_goc", item.ma_goc);
    form.append("ma_ho_so_igate", item.ma_ho_so_igate);
    form.append("ma_linh_vuc_van_ban", item.ma_linh_vuc_van_ban);
    form.append("ma_loai_lien_thong", item.ma_loai_lien_thong);
    form.append("ma_loai_van_ban", item.ma_loai_van_ban);
    form.append("ma_quy_trinh", item.ma_quy_trinh);
    form.append("ma_so_van_ban", item.ma_so_van_ban);
    form.append("ma_so_vb_den", item.ma_so_vb_den);
    form.append("ma_so_vb_den_old", item.ma_so_vb_den_old);
    form.append("ma_van_ban_den_inout", item.ma_van_ban_den_inout);
    form.append("ma_van_ban_den_kc", item.ma_van_ban_den_kc);
    form.append("ma_van_ban_goc", item.ma_van_ban_goc);
    form.append("ma_van_ban_kc", item.ma_van_ban_kc);
    form.append("ma_xu_ly_den", item.ma_xu_ly_den);
    form.append("ma_xu_ly_den_inout", item.ma_xu_ly_den_inout);
    form.append("ngay_ban_hanh", item.ngay_ban_hanh);
    form.append("ngay_den", item.ngay_den);
    form.append("ngay_luu", item.ngay_luu);
    form.append("ngay_nhan", item.ngay_nhan);
    form.append("ngay_tao", item.ngay_tao);
    form.append("ngay_xem", item.ngay_xem);
    form.append("nguoi_ky", item.nguoi_ky);
    form.append("noi_luu_ban_chinh", item.noi_luu_ban_chinh);
    form.append("sms", item.sms);
    form.append("so_ban_phat_hanh", item.so_ban_phat_hanh);
    form.append("so_den", item.so_den);
    form.append("so_ky_hieu", item.so_ky_hieu);
    form.append("ten_cap_do_khan", item.ten_cap_do_khan);
    form.append("ten_cap_do_mat", item.ten_cap_do_mat);
    form.append("ten_co_quan_ban_hanh", item.ten_co_quan_ban_hanh);
    form.append("ten_don_vi_gui", item.ten_don_vi_gui);
    form.append("ten_linh_vuc_van_ban", item.ten_linh_vuc_van_ban);
    form.append("ten_loai_van_ban", item.ten_loai_van_ban);
    form.append("ten_nguoi_duyet", item.ten_nguoi_duyet);
    form.append("ten_nguoi_luu", item.ten_nguoi_luu);
    form.append("ten_so_vb_den", item.ten_so_vb_den);
    form.append("trang_thai_ra_soat", item.trang_thai_ra_soat);
    form.append("trang_thai_van_ban", item.trang_thai_van_ban);
    form.append("trang_thai_van_ban_den", item.trang_thai_van_ban_den);
    form.append("trang_thai_xu_ly", item.trang_thai_xu_ly);
    form.append("trang_thai_xu_ly_den", item.trang_thai_xu_ly_den);
    form.append("trich_yeu", item.trich_yeu);
    form.append("ghi_chu", item.ghi_chu);

    console.log("AU_VBDEN_TMVBDLN:" + JSON.stringify(form))
    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDEN_TMVBDLN,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_LONG
    }).then(response => {
        if (global.CONSOLE_AU_VBDEN_TMVBDLN) {
            console.log("CONSOLE_AU_VBDEN_TMVBDLN: " + JSON.stringify(response))
        }
        console.log("CONSOLE_AU_VBDEN_TMVBDLN: " + JSON.stringify(response))
        if (response.data.message === "Thực thi thành công") {
            result = true
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDEN_TMVBDLN) {
            console.log("CONSOLE_AU_VBDEN_TMVBDLN ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDEN_TMVBDLN(item).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDEN_CNKTMVBD(ma_van_ban_den) {
    let result = false
    var form = new FormData();
    form.append("ma_van_ban_den", ma_van_ban_den);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDEN_CNKTMVBD,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDEN_CNKTMVBD) {
            console.log("CONSOLE_AU_VBDEN_CNKTMVBD: " + JSON.stringify(response))
        }
        if (response.data.message === "Thực thi thành công") {
            result = response.data.trung_so_ky_hieu === 0 ? true : false
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDEN_CNKTMVBD) {
            console.log("CONSOLE_AU_VBDEN_CNKTMVBD ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDEN_CNKTMVBD(ma_van_ban_den).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDEN_VTHVBDDT(ma_xu_ly_den) {
    let result = false
    let url = global.AU_ROOT + URL_CONFIG.AU_VBDEN_VTHVBDDT
    url += "?ma_xu_ly_den=" + ma_xu_ly_den
    url += "&ma_ctcb=" + global.ma_ctcb_kc

    await axios({
        method: "GET",
        url: url,
        headers: setHeaderToken(),
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDEN_VTHVBDDT) {
            console.log("CONSOLE_AU_VBDEN_VTHVBDDT: " + JSON.stringify(response))
        }
        if (response.data.message === "Thực thi thành công" && response.data.huy_duoc === 1) {
            result = true
        } else {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDEN_VTHVBDDT) {
            console.log("CONSOLE_AU_VBDEN_VTHVBDDT ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDEN_VTHVBDDT(ma_xu_ly_den).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}
//END VĂN THƯ
// Luchq Begin:
async function AU_VBDEN_PCTHEODOI(ma_can_bo, ma_ctcb_nhan, ma_don_vi_quan_tri, page, size, keyword) {
    let result = []
    let den_ngay = moment().format("DD/MM/YYYY");
    let tu_ngay = '';
    if (global.ht_chon_tgian_nhac_viec === "all" || global.ht_chon_tgian_nhac_viec === "hide") {
        tu_ngay = "01/01/2000"
    } else {
        let lengthMonth = global.ht_chon_tgian_nhac_viec.substring(0, 1)
        tu_ngay = moment(den_ngay, "DD/MM/YYYY").subtract(parseInt(lengthMonth), "months").format("DD/MM/YYYY")
    }
    var formData = new FormData();
    formData.append("co_tep_tin", -1);
    formData.append("ma_can_bo", ma_can_bo);
    formData.append("ma_ctcb_nhan", ma_ctcb_nhan);
    formData.append("ma_don_vi_quan_tri", ma_don_vi_quan_tri);
    formData.append("ma_loai_ttdh", 0);
    formData.append("ma_yeu_cau", 4);
    formData.append("nam", 0);
    formData.append("nhan_den_ngay", den_ngay);
    formData.append("nhan_tu_ngay", tu_ngay);
    formData.append("page", page);
    formData.append("size", size);
    formData.append("trang_thai_ttdh_gui", -1);
    formData.append("trang_thai_xu_ly", 0);
    if (keyword != "") {
        formData.append("trich_yeu", keyword);
    }
    await axios({
        method: "POSt",
        url: AU_ROOT + URL_CONFIG.AU_VBDEN_PCTHEODOI,
        data: formData,
        headers: setHeaderToken(),
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        result = response.data;
    }).catch(async error => {
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDEN_PCTHEODOI(ma_can_bo, ma_ctcb_nhan, ma_don_vi_quan_tri, page, size, keyword).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result;
}

async function AU_VBDEN_HUYTHEODOI(ma_xu_ly_den) {
    let result = "";
    var formData = new FormData();
    formData.append("ma_xu_ly_den", ma_xu_ly_den);
    await axios({
        method: "POST",
        url: AU_ROOT + URL_CONFIG.AU_VBDEN_HUYTHEODOI,
        data: formData,
        headers: setHeaderToken(),
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        result = response.data
    }).catch(async error => {
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDEN_HUYTHEODOI(ma_xu_ly_den).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result;
}

async function AU_VBDEN_DSVBCLVT(so_ky_hieu, trich_yeu, page, size, nam) {
    let result = []
    var form = new FormData();
    form.append("so_ky_hieu", so_ky_hieu);
    form.append("trich_yeu", trich_yeu);
    form.append("page", parseInt(page));
    form.append("size", parseInt(size));
    if (checkVersionWeb(23) && nam !== 0) {
        form.append("nam", 0);
        form.append("tu_ngay", "01/01/" + nam);
        form.append("den_ngay", "31/12/" + nam);
        form.append("ngay_duyet_tu_ngay", "01/01/" + nam);
        form.append("ngay_duyet_den_ngay", "31/12/" + nam);
    } else {
        form.append("nam", nam);
    }
    form.append("ma_ctcb", parseInt(global.ma_ctcb_kc));
    form.append("ma_ctcb_van_thu", parseInt(global.ma_ctcb_kc));
    form.append("ma_don_vi_nhan", parseInt(global.ma_don_vi_quan_tri));
    form.append("ma_don_vi_quan_tri", parseInt(global.ma_don_vi_quan_tri));
    form.append("ma_so_vb_den", 0);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDEN_DSDTCLUU,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_LONG
    }).then(response => {
        if (global.CONSOLE_AU_VBDEN_DSVBCLVT) {
            console.log("CONSOLE_AU_VBDEN_DSVBCLVT: " + JSON.stringify(response))
        }
        console.log("CONSOLE_AU_VBDEN_DSVBDTCVT: " + JSON.stringify(response))
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDEN_DSVBCLVT) {
            console.log("CONSOLE_AU_VBDEN_DSVBCLVTT ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDEN_DSVBCLVT(so_ky_hieu, trich_yeu, page, size, nam).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}
async function AU_VBDE_GET_INFOEDXML(filexml) {
    let result = [];
    var form = new FormData();
    var refresh_token = base64.encode(global.refresh_token)
    form.append("filexml", filexml);
    form.append("ma_ctcb", global.ma_ctcb_kc);
    form.append("ma_dinh_danh", global.ma_dinh_danh_quan_tri);
    form.append("user_edoc", global.user_edoc);
    form.append("pass_edoc", global.pass_edoc);
    form.append("pub_key_edoc", global.pub_key_edoc);
    form.append("pri_key_edoc", global.pri_key_edoc);
    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDEN_GET_INFO_EDXML + refresh_token,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_LONG
    }).then(response => {
        if (global.CONSOLE_AU_VBDE_GET_INFOEDXML) {
            console.log("CONSOLE_AU_VBDE_GET_INFOEDXML: " + JSON.stringify(response))
        }
        console.log("CONSOLE_AU_VBDE_GET_INFOEDXML: " + JSON.stringify(response))
        if (response.data.message === "Lấy thông tin file edoc thành công!") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDE_GET_INFOEDXML) {
            console.log("CONSOLE_AU_VBDE_GET_INFOEDXML ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDE_GET_INFOEDXML(filexml).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDE_COPY_DOCUMENT_EDOC(strFile) {
    let result = [];
    var form = new FormData();
    var refresh_token = base64.encode(global.refresh_token)
    form.append("chuoi_file_path", strFile);
    form.append("chuc_nang", "vbde_edoc");
    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.COPY_DOCUMENT_EDOC + refresh_token,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_LONG
    }).then(response => {
        if (response.data.message === "copy file thành công!") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDE_COPY_DOCUMENT_EDOC(strFile).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_UPDATE_STATUS_VBTEMP_EDOC(ma_vbden_edoc_temp, status) {
    let result = [];
    var form = new FormData();
    form.append("ma_vbde_edoc_temp_kc", ma_vbden_edoc_temp);
    form.append("trang_thai", status);
    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDEN_UPDATE_STATUS_TEMP_EDOC,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_LONG
    }).then(response => {
        console.log("state edoc: " + response);
    }).catch(async error => {
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_UPDATE_STATUS_VBTEMP_EDOC(ma_vbden_edoc_temp, status).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDE_UPDATE_STATUS_EDXML(filexml) {
    let result = [];
    var form = new FormData();
    form.append("filexml", filexml);
    form.append("trang_thai", 4);
    form.append("ma_dinh_danh", global.ma_dinh_danh_quan_tri);
    form.append("user_edoc", global.user_edoc);
    form.append("pass_edoc", global.pass_edoc);
    form.append("pub_key_edoc", global.pub_key_edoc);
    form.append("pri_key_edoc", global.pri_key_edoc);
    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDE_UPDATE_STATUS_EDXML,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_LONG
    }).then(response => {
        console.log("state edoc: " + JSON.stringify(response));
    }).catch(async error => {
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDE_UPDATE_STATUS_EDXML(filexml).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDE_UPDATE_STATUS_TRUC_EDOC(filexml) {
    let result = [];
    var form = new FormData();
    form.append("filexml", filexml);
    form.append("trang_thai", 0);
    form.append("DocumentStatusID", 2);
    form.append("Staff", global.ho_va_ten_can_bo);
    form.append("Position", global.ten_chuc_vu);
    form.append("Description", "Văn thư huỷ văn bản");
    form.append("ma_dinh_danh", global.ma_dinh_danh_quan_tri);
    form.append("Timestamp", moment().format("DD/MM/YYYY"));
    form.append("Department", global.ten_don_vi);
    form.append("user_edoc", global.user_edoc);
    form.append("pass_edoc", global.pass_edoc);
    form.append("pub_key_edoc", global.pub_key_edoc);
    form.append("pri_key_edoc", global.pri_key_edoc);
    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDE_UPDATE_STATUS_TRUC_EDOC,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_LONG
    }).then(response => {
        result = response;
    }).catch(async error => {
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDE_UPDATE_STATUS_TRUC_EDOC(filexml).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDEN_THEM_DE_HUY_EDOC(item) {
    let result = false;
    var form = new FormData();
    form.append("ma_van_ban_den_kc", item.ma_van_ban_den_kc);
    form.append("ma_van_ban_kc", item.ma_van_ban_kc);
    form.append("ma_xu_ly_den", item.ma_xu_ly_den);
    form.append("so_den", parseInt(item.so_den));
    form.append("trich_yeu", item.trich_yeu);
    form.append("so_ky_hieu", item.so_ky_hieu);
    form.append("ngay_nhan", item.ngay_nhan);
    form.append("ngay_xem", item.ngay_xem);
    form.append("file_van_ban_bs", item.file_van_ban_bs);
    form.append("ten_co_quan_ban_hanh", item.ten_co_quan_ban_hanh);
    form.append("ten_don_vi_gui", item.ten_don_vi_gui);
    form.append("file_van_ban", item.file_van_ban);
    form.append("ma_linh_vuc_van_ban", item.ma_linh_vuc_van_ban);
    form.append("ma_loai_van_ban", item.ma_loai_van_ban);
    form.append("ma_cap_do_khan", item.ma_cap_do_khan);
    form.append("ma_cap_do_mat", item.ma_cap_do_mat);
    form.append("ngay_den", item.ngay_den);
    form.append("han_xu_ly_chung", item.han_xu_ly_chung);
    form.append("ngay_ban_hanh", item.ngay_ban_hanh);
    form.append("so_ioffice", item.so_ioffice);
    form.append("gui_kem_vb_giay", item.gui_kem_vb_giay);
    form.append("so_ban_phat_hanh", item.so_ban_phat_hanh);
    form.append("so_trang_vb", item.so_trang_vb);
    form.append("nguoi_ky", item.nguoi_ky);
    form.append("noi_luu_ban_chinh", item.noi_luu_ban_chinh);
    form.append("ma_ctcb_duyet", item.ma_ctcb_duyet);
    form.append("hien_thi_vblq", item.hien_thi_vblq);
    form.append("chuoi_vb_dien_tu", item.chuoi_vb_dien_tu);
    form.append("da_luu_file", item.da_luu_file);
    form.append("r", item.r);

    form.append("ma_don_vi", global.ma_don_vi);
    form.append("ma_don_vi_quan_tri", global.ma_don_vi_quan_tri);
    form.append("ma_ctcb_tao", global.ma_ctcb_kc);
    form.append("ma_van_ban_den_inout", item.ma_van_ban_den_kc);
    form.append("ma_xu_ly_den_inout", item.ma_xu_ly_den);
    form.append("gui_kem_van_ban_giay", item.gui_kem_van_ban_giay);
    form.append("ma_so_vb_den", item.ma_so_vb_den);
    form.append("chuoi_ma_vb_lien_quan", "");
    form.append("trang_thai_van_ban_den", item.trang_thai_van_ban_den);
    form.append("ngay_duyet", "");
    form.append("ghi_chu", item.ghi_chu);
    form.append("sms", item.sms);
    form.append("email", 0);
    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDE_THEM_DE_HUY_EDOC,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_LONG
    }).then(response => {
        console.log("CONSOLE_AU_VBDEN_THEM_DE_HUY_EDOC: " + JSON.stringify(response))
        if (global.CONSOLE_AU_VBDEN_THEM_DE_HUY_EDOC) {
            console.log("CONSOLE_AU_VBDEN_THEM_DE_HUY_EDOC: " + JSON.stringify(response))
        }
        if (response.data.message === "Thực thi thành công") {
            result = true
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDEN_THEM_DE_HUY_EDOC) {
            console.log("CONSOLE_AU_VBDEN_THEM_DE_HUY_EDOC ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDEN_THEM_DE_HUY_EDOC(item).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}
async function AU_VBDEN_CVTHEODOI(ma_van_ban_den, ma_xu_ly_den) {
    var result = [];
    var form = new FormData();
    form.append("ma_van_ban_den", ma_van_ban_den);
    form.append("ma_xu_ly_den", ma_xu_ly_den);
    form.append("ma_ctcb", global.ma_ctcb_kc);
    console.log("data theo doi: " + JSON.stringify(form));
    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDEN_CVTHEODOI,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_LONG
    }).then(response => {
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data;
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDEN_CVTHEODOI) {
            console.log("CONSOLE_AU_VBDEN_CVTHEODOI: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDEN_CVTHEODOI(item).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result;
}
export default {
    AU_VBDEN_DSVBDTTTCCV,
    AU_VBDEN_CVXVBD,
    AU_VBDEN_VBDCT,
    AU_VBDEN_CVXLNVBD,
    AU_VBDEN_CVXLVBD,
    AU_VBDEN_CVCNYKXL,
    AU_VBDEN_BPLDVBD,
    AU_VBDEN_DSCBTDVTTS,
    AU_VBDEN_DSCBMD,
    AU_VBDEN_DSCBTVT,
    AU_VBDEN_DSLDDCD,
    AU_VBDEN_CLDKD,
    AU_VBDEN_CNCBTLN,
    AU_VBDEN_DSNCBNVBDLDD,
    AU_VBDEN_CVCTH,
    AU_VBDEN_CCVCAP,
    AU_VBDEN_DSVBDCDCLD,
    AU_VBDEN_DSVBDUQDCLD,
    AU_VBDEN_DSVBDDCTHCLD,
    AU_VBDEN_LDDVBD,
    AU_VBDEN_LDUQDVBD,
    AU_VBDEN_LDDHTVBD,
    AU_VANBAN_TCVB,
    AU_VANBAN_DSVBLQDK,
    AU_VBDEN_LDDPD,

    AU_VBDEN_DSDMSVBD,
    AU_VBDEN_LSDTSVBD,
    AU_VBDEN_DSCDK,
    AU_VBDEN_DSCDM,
    AU_VBDEN_DSLVVB,
    AU_VBDEN_DSLVB,
    AU_VBDEN_DSLDDVBDE,
    AU_VBDEN_DSVBDTCVT,
    AU_VBDEN_DSVBDML,
    AU_VBDEN_DSVBLDTLCVT,
    AU_VBDEN_DSVBDCDCVT,
    AU_VBDEN_DSVBDCVTC,
    AU_VBDEN_DSVBDVTDC,
    AU_VBDEN_DSVBDHCVT,
    AU_VBDEN_UTLT,
    AU_VBDEN_CNCVBDDTLT,
    AU_VBDEN_KTTSKH,
    AU_VBDEN_TMVBD,
    AU_VBDEN_TMVBDLN,
    AU_VBDEN_CNKTMVBD,
    AU_VBDEN_VTHVBDDT,
    AU_VBDEN_PCTHEODOI,
    AU_VBDEN_HUYTHEODOI,
    AU_VBDEN_DSVBCLVT,
    AU_VBDE_GET_INFOEDXML,
    AU_VBDE_COPY_DOCUMENT_EDOC,
    AU_UPDATE_STATUS_VBTEMP_EDOC,
    AU_VBDE_UPDATE_STATUS_EDXML,
    AU_VBDE_UPDATE_STATUS_TRUC_EDOC,
    AU_VBDEN_THEM_DE_HUY_EDOC,
    AU_VBDEN_CVTHEODOI
}
