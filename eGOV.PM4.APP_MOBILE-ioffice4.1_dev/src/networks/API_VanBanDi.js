import { URL_CONFIG } from "./Config"
import { Alert, Platform } from "react-native"
import DeviceInfo from 'react-native-device-info'
import { Toast } from "native-base"
import axios from "axios"
import { ReplaceAll, checkVersionWeb, formatDate } from "../untils/TextUntils"
import API_Login from "./API_Login"
import { AppConfig } from "../AppConfig"
import moment from "moment"

function showWarning(error) {
    Toast.show({ text: "Kết nối bị gián đoạn!", type: "warning", buttonText: "Đóng", position: "top", duration: 2000 })
    if (error) {
        SendLog(error)
    }
};

async function SendLog(message) {
    try {
        let obj = new Object
        let date = new Date()
        obj.Model = DeviceInfo.getModelSync()
        obj.OS = Platform.OS.toString()
        obj.SystemVersion = DeviceInfo.getSystemVersionSync()
        obj.AppVersion = DeviceInfo.getVersionSync()
        obj.CurrentTime = date.toLocaleDateString() + " " + date.toLocaleTimeString()
        obj.Module = "API_VanBanDi"
        obj.Domain = global.AU_ROOT ? global.AU_ROOT : ""
        delete message.headers
        obj.Message = message
        let text = ReplaceAll(JSON.stringify(obj), String.fromCharCode(34), "'")
        let url = AppConfig.AppDefaultLogURLSendText + text
        await axios({
            method: "POST",
            url: url,
            timeout: AppConfig.TIME_OUT_SHORT
        })
    } catch (error) {
        console.log("Send log error: " + error)
    }
}

function setHeaderToken() {
    return { "Authorization": "Bearer " + global.access_token }
};

async function AU_VBDI_DSVBDCXLCCV(so_ky_hieu, trich_yeu, page, size, nam) {
    let result = []
    var form = new FormData();
    form.append("page", parseInt(page));
    form.append("size", parseInt(size));
    form.append("ma_ctcb_cv", parseInt(global.ma_ctcb_kc))
    form.append("ma_can_bo", parseInt(global.ma_can_bo))
    form.append("ma_don_vi_quan_tri", parseInt(global.ma_don_vi_quan_tri));
    form.append("ma_loai_van_ban", 0);
    form.append("ma_linh_vuc", 0);
    form.append("so_ky_hieu", so_ky_hieu);
    form.append("trich_yeu", trich_yeu);
    form.append("nam", nam);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDI_DSVBDCXLCCV,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDI_DSVBDCXLCCV) {
            console.log("CONSOLE_AU_VBDI_DSVBDCXLCCV: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDI_DSVBDCXLCCV) {
            console.log("CONSOLE_AU_VBDI_DSVBDCXLCCV ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDI_DSVBDCXLCCV(so_ky_hieu, trich_yeu, page, size, nam).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDI_DSVBDCXLCCV_V23(trich_yeu, page, size, nam) {
    let result = []
    var form = new FormData();
    form.append("page", parseInt(page));
    form.append("size", parseInt(size));
    form.append("ma_ctcb_cv", parseInt(global.ma_ctcb_kc))
    form.append("ma_can_bo", parseInt(global.ma_can_bo))
    form.append("ma_don_vi_quan_tri", parseInt(global.ma_don_vi_quan_tri));
    form.append("trich_yeu", trich_yeu);
    form.append("nam", 0);
    if (trich_yeu !== "") {
        //Search
        form.append("tgian_temp", "01/01/2005 - 31/12/2030");
    } else {
        form.append("tgian_temp", "01/01/" + nam + " - 31/12/" + nam);
    }
    form.append("ma_linh_vuc", 0)
    form.append("ma_loai_van_ban", 0)

    console.log(form)

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDI_DSVBDCXLCCV,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDI_DSVBDCXLCCV) {
            console.log("CONSOLE_AU_VBDI_DSVBDCXLCCV: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDI_DSVBDCXLCCV) {
            console.log("CONSOLE_AU_VBDI_DSVBDCXLCCV ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDI_DSVBDCXLCCV_V23(trich_yeu, page, size, nam).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDI_DSVBDCXLCCV_V32(trich_yeu, page, size, nam) {
    let result = []
    var form = new FormData();

    form.append("page", parseInt(page));
    form.append("size", parseInt(size));
    form.append("ma_ctcb_cv", parseInt(global.ma_ctcb_kc))
    form.append("ma_can_bo", parseInt(global.ma_can_bo))
    form.append("ma_don_vi_quan_tri", parseInt(global.ma_don_vi_quan_tri));
    form.append("trich_yeu", trich_yeu);
    form.append("nam", 0);
    if (trich_yeu !== "") {
        //Search
        form.append("ngay_tao_tu_ngay", "01/01/2005");
        form.append("ngay_tao_den_ngay", "31/12/2030");
    } else {
        form.append("ngay_tao_tu_ngay", "01/01/" + nam);
        form.append("ngay_tao_den_ngay", "31/12/" + nam);
    }

    console.log(form)

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDI_DSVBDCXLCCV,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDI_DSVBDCXLCCV) {
            console.log("CONSOLE_AU_VBDI_DSVBDCXLCCV: " + JSON.stringify(response))
        }
        console.log("CONSOLE_AU_VBDI_DSVBDCXLCCV: " + JSON.stringify(response))
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDI_DSVBDCXLCCV) {
            console.log("CONSOLE_AU_VBDI_DSVBDCXLCCV ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDI_DSVBDCXLCCV_V32(trich_yeu, page, size, nam).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDI_DSVBDDXLCCV(so_ky_hieu, trich_yeu, page, size, nam) {
    let result = []
    var form = new FormData();
    form.append("page", parseInt(page));
    form.append("size", parseInt(size));
    form.append("ma_ctcb_cv", parseInt(global.ma_ctcb_kc))
    form.append("ma_can_bo", parseInt(global.ma_can_bo))
    form.append("ma_don_vi_quan_tri", parseInt(global.ma_don_vi_quan_tri));
    form.append("ma_loai_van_ban", 0);
    form.append("ma_linh_vuc", 0);
    form.append("so_ky_hieu", so_ky_hieu);
    form.append("trich_yeu", trich_yeu);
    form.append("nam", nam);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDI_DSVBDDXLCCV,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDI_DSVBDDXLCCV) {
            console.log("CONSOLE_AU_VBDI_DSVBDDXLCCV: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDI_DSVBDDXLCCV) {
            console.log("CONSOLE_AU_VBDI_DSVBDDXLCCV ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDI_DSVBDDXLCCV(so_ky_hieu, trich_yeu, page, size, nam).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDI_DSVBDDXLCCV_V23(trich_yeu, page, size, nam) {
    let result = []
    var form = new FormData();
    form.append("page", parseInt(page));
    form.append("size", parseInt(size));
    form.append("ma_ctcb_cv", parseInt(global.ma_ctcb_kc))
    form.append("ma_can_bo", parseInt(global.ma_can_bo))
    form.append("ma_don_vi_quan_tri", parseInt(global.ma_don_vi_quan_tri));
    form.append("trich_yeu", trich_yeu);
    form.append("nam", 0);
    if (trich_yeu !== "") {
        //Search
        form.append("tgian_temp", "01/01/2005 - 31/12/2030");
    } else {
        form.append("tgian_temp", "01/01/" + nam + " - 31/12/" + nam);
    }
    form.append("ma_linh_vuc", 0)
    form.append("ma_loai_van_ban", 0)

    console.log(form)

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDI_DSVBDDXLCCV,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDI_DSVBDDXLCCV) {
            console.log("CONSOLE_AU_VBDI_DSVBDDXLCCV: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDI_DSVBDDXLCCV) {
            console.log("CONSOLE_AU_VBDI_DSVBDDXLCCV ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDI_DSVBDDXLCCV_V23(trich_yeu, page, size, nam).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDI_DSVBDDXLCCV_V32(trich_yeu, page, size, nam) {
    let result = []
    var form = new FormData();
    form.append("page", parseInt(page));
    form.append("size", parseInt(size));
    form.append("ma_ctcb_cv", parseInt(global.ma_ctcb_kc))
    form.append("ma_can_bo", parseInt(global.ma_can_bo));
    form.append("ma_don_vi_quan_tri", parseInt(global.ma_don_vi_quan_tri));
    form.append("trich_yeu", trich_yeu);
    form.append("nam", 0);
    if (trich_yeu !== "") {
        //Search
        form.append("ngay_tao_tu_ngay", "01/01/2005");
        form.append("ngay_tao_den_ngay", "31/12/2030");
    } else {
        form.append("ngay_tao_tu_ngay", "01/01/" + nam);
        form.append("ngay_tao_den_ngay", "31/12/" + nam);
    }

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDI_DSVBDDXLCCV,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDI_DSVBDDXLCCV) {
            console.log("CONSOLE_AU_VBDI_DSVBDDXLCCV: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDI_DSVBDDXLCCV) {
            console.log("CONSOLE_AU_VBDI_DSVBDDXLCCV ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDI_DSVBDDXLCCV_V32(trich_yeu, page, size, nam).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDI_DSVBDCPHCCV(so_ky_hieu, trich_yeu, page, size, nam) {
    let result = []
    var form = new FormData();
    form.append("page", parseInt(page));
    form.append("size", parseInt(size));
    form.append("ma_ctcb_cv", parseInt(global.ma_ctcb_kc))
    form.append("ma_can_bo", parseInt(global.ma_can_bo))
    form.append("ma_don_vi_quan_tri", parseInt(global.ma_don_vi_quan_tri));
    form.append("ma_loai_van_ban", 0);
    form.append("ma_linh_vuc", 0);
    form.append("trang_thai_xu_ly", 3)
    form.append("so_ky_hieu", so_ky_hieu);
    form.append("trich_yeu", trich_yeu);
    form.append("nam", nam);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDI_DSVBDCPHCCV,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDI_DSVBDCPHCCV) {
            console.log("CONSOLE_AU_VBDI_DSVBDCPHCCV: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDI_DSVBDCPHCCV) {
            console.log("CONSOLE_AU_VBDI_DSVBDCPHCCV ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDI_DSVBDCPHCCV(so_ky_hieu, trich_yeu, page, size, nam).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDI_DSVBDCPHCCV_V23(trich_yeu, page, size, nam) {
    let result = []
    var form = new FormData();
    form.append("page", parseInt(page));
    form.append("size", parseInt(size));
    form.append("ma_ctcb_cv", parseInt(global.ma_ctcb_kc))
    form.append("ma_can_bo", parseInt(global.ma_can_bo))
    form.append("ma_don_vi_quan_tri", parseInt(global.ma_don_vi_quan_tri));
    form.append("trich_yeu", trich_yeu);
    form.append("nam", 0);
    if (trich_yeu !== "") {
        //Search
        form.append("tgian_temp", "01/01/2005 - 31/12/2030");
    } else {
        form.append("tgian_temp", "01/01/" + nam + " - 31/12/" + nam);
    }
    form.append("ma_linh_vuc", 0)
    form.append("ma_loai_van_ban", 0)
    form.append("trang_thai_xu_ly", 3)

    console.log(form)

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDI_DSVBDCPHCCV,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDI_DSVBDCPHCCV) {
            console.log("CONSOLE_AU_VBDI_DSVBDCPHCCV: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDI_DSVBDCPHCCV) {
            console.log("CONSOLE_AU_VBDI_DSVBDCPHCCV ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDI_DSVBDCPHCCV_V23(trich_yeu, page, size, nam).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDI_DSVBDCPHCCV_V32(trich_yeu, page, size, nam) {
    let result = []
    var form = new FormData();
    form.append("page", parseInt(page));
    form.append("size", parseInt(size));
    form.append("ma_ctcb_cv", parseInt(global.ma_ctcb_kc))
    form.append("ma_can_bo", parseInt(global.ma_can_bo));
    form.append("ma_don_vi_quan_tri", parseInt(global.ma_don_vi_quan_tri));
    form.append("trich_yeu", trich_yeu);
    form.append("nam", 0);
    if (trich_yeu !== "") {
        //Search
        form.append("ngay_tao_tu_ngay", "01/01/2005");
        form.append("ngay_tao_den_ngay", "31/12/2030");
    } else {
        form.append("ngay_tao_tu_ngay", "01/01/" + nam);
        form.append("ngay_tao_den_ngay", "31/12/" + nam);
    }
    form.append("trang_thai_xu_ly", 3);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDI_DSVBDCPHCCV,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDI_DSVBDCPHCCV) {
            console.log("CONSOLE_AU_VBDI_DSVBDCPHCCV: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDI_DSVBDCPHCCV) {
            console.log("CONSOLE_AU_VBDI_DSVBDCPHCCV ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDI_DSVBDCPHCCV_V32(trich_yeu, page, size, nam).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDI_DSVBDDPHCCV(so_ky_hieu, trich_yeu, page, size, nam) {
    let result = []
    var form = new FormData();
    form.append("page", parseInt(page));
    form.append("size", parseInt(size));
    form.append("ma_ctcb_cv", parseInt(global.ma_ctcb_kc))
    form.append("ma_don_vi_quan_tri", parseInt(global.ma_don_vi_quan_tri));
    form.append("ma_loai_van_ban", 0);
    form.append("ma_linh_vuc", 0);
    form.append("so_ky_hieu", so_ky_hieu);
    form.append("trich_yeu", trich_yeu);
    form.append("nam", nam);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDI_DSVBDDPHCCV,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDI_DSVBDDPHCCV) {
            console.log("CONSOLE_AU_VBDI_DSVBDDPHCCV: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDI_DSVBDDPHCCV) {
            console.log("CONSOLE_AU_VBDI_DSVBDDPHCCV ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDI_DSVBDDPHCCV(so_ky_hieu, trich_yeu, page, size, nam).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDI_DSVBDDPHCCV_V23(trich_yeu, page, size, nam) {
    let result = []
    var form = new FormData();
    form.append("page", parseInt(page));
    form.append("size", parseInt(size));
    form.append("ma_ctcb_cv", parseInt(global.ma_ctcb_kc))
    form.append("ma_don_vi_quan_tri", parseInt(global.ma_don_vi_quan_tri));
    form.append("trich_yeu", trich_yeu);
    form.append("nam", 0);
    if (trich_yeu !== "") {
        //Search
        form.append("tgian_temp", "01/01/2005 - 31/12/2030");
    } else {
        form.append("tgian_temp", "01/01/" + nam + " - 31/12/" + nam);
    }
    form.append("ma_linh_vuc", 0)
    form.append("ma_loai_van_ban", 0)
    form.append("xem", -1)

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDI_DSVBDDPHCCV,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDI_DSVBDDPHCCV) {
            console.log("CONSOLE_AU_VBDI_DSVBDDPHCCV: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDI_DSVBDDPHCCV) {
            console.log("CONSOLE_AU_VBDI_DSVBDDPHCCV ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDI_DSVBDDPHCCV_V23(trich_yeu, page, size, nam).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDI_DSVBDDPHCCV_V32(trich_yeu, page, size, nam) {
    let result = []
    var form = new FormData();
    form.append("page", parseInt(page));
    form.append("size", parseInt(size));
    form.append("ma_ctcb_cv", parseInt(global.ma_ctcb_kc))
    form.append("ma_don_vi_quan_tri", parseInt(global.ma_don_vi_quan_tri));
    form.append("trich_yeu", trich_yeu);
    form.append("nam", 0);
    if (trich_yeu !== "") {
        //Search
        form.append("ngay_di_tu_ngay", "01/01/2005");
        form.append("ngay_di_den_ngay", "31/12/2030");
    } else {
        form.append("ngay_di_tu_ngay", "01/01/" + nam);
        form.append("ngay_di_den_ngay", "31/12/" + nam);
    }

    console.log(form)

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDI_DSVBDDPHCCV,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDI_DSVBDDPHCCV) {
            console.log("CONSOLE_AU_VBDI_DSVBDDPHCCV: " + JSON.stringify(response))
        }
        console.log("CONSOLE_AU_VBDI_DSVBDDPHCCV: " + JSON.stringify(response))
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDI_DSVBDDPHCCV) {
            console.log("CONSOLE_AU_VBDI_DSVBDDPHCCV ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDI_DSVBDDPHCCV_V32(trich_yeu, page, size, nam).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDI_CNDX(ma_ctcb, ma_van_ban_di) {
    let result = false
    var form = new Object()
    form.ma_ctcb = ma_ctcb
    form.ma_van_ban_di = ma_van_ban_di

    await axios({
        method: "GET",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDI_CNDX,
        headers: setHeaderToken(),
        params: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDI_CNDX) {
            console.log("CONSOLE_AU_VBDI_CNDX: " + JSON.stringify(response))
        }
        if (response.data.message === "Thực thi thành công") {
            result = true
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDI_CNDX) {
            console.log("CONSOLE_AU_VBDI_CNDX ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDI_CNDX(ma_ctcb, ma_van_ban_di).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDI_CTVBD(ma_van_ban_di, ma_ctcb) {
    let result = []
    var form = new Object()
    form.ma_van_ban_di = ma_van_ban_di
    form.ma_ctcb = ma_ctcb

    await axios({
        method: "GET",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDI_CTVBD,
        headers: setHeaderToken(),
        params: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDI_CTVBD) {
            console.log("CONSOLE_AU_VBDI_CTVBD: " + JSON.stringify(response))
        }
        console.log("CONSOLE_AU_VBDI_CTVBD: " + JSON.stringify(response))
        if (response.data.message === "Lấy dữ liệu thành công") {
            result.push(response.data.data)
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDI_CTVBD) {
            console.log("CONSOLE_AU_VBDI_CTVBD ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDI_CTVBD(ma_van_ban_di, ma_ctcb).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDI_QTXLVBD(ma_van_ban_di, ma_don_vi_quan_tri) {
    let result = []
    var form = new Object()
    form.ma_van_ban_di = ma_van_ban_di
    form.ma_don_vi_quan_tri = ma_don_vi_quan_tri

    await axios({
        method: "GET",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDI_QTXLVBD,
        headers: setHeaderToken(),
        params: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDI_QTXLVBD) {
            console.log("CONSOLE_AU_VBDI_QTXLVBD: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            //SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDI_QTXLVBD) {
            console.log("CONSOLE_AU_VBDI_QTXLVBD ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDI_QTXLVBD(ma_van_ban_di, ma_don_vi_quan_tri).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDI_HTVBDI(ma_xu_ly_di) {
    let result = false
    var form = new Object()
    form.ma_xu_ly_di = ma_xu_ly_di

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDI_HTVBDI,
        headers: setHeaderToken(),
        params: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDI_HTVBDI) {
            console.log("CONSOLE_AU_VBDI_HTVBDI: " + JSON.stringify(response))
        }
        if (response.data.message === "Thực thi thành công") {
            result = true
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDI_HTVBDI) {
            console.log("CONSOLE_AU_VBDI_HTVBDI ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDI_HTVBDI(ma_xu_ly_di).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDI_VBDCDCLD(so_ky_hieu, trich_yeu, page, size, ma_ctcb_duyet, ma_don_vi_quan_tri, nam) {
    let result = []
    var form = new FormData();
    form.append("page", parseInt(page));
    form.append("size", parseInt(size));
    if (checkVersionWeb(23)) {
        form.append("nam", 0);
        if (nam !== 0) {
            form.append("ngay_duyet_tu_ngay", "01/01/" + nam);
            form.append("ngay_duyet_den_ngay", "31/12/" + nam);
        } else {
            form.append("trich_yeu", trich_yeu);
        }
    } else {
        form.append("so_ky_hieu", so_ky_hieu);
        form.append("trich_yeu", trich_yeu);
        form.append("nam", nam);
    }
    form.append("ma_ctcb_duyet", parseInt(ma_ctcb_duyet))
    form.append("ma_don_vi_quan_tri", parseInt(ma_don_vi_quan_tri));

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDI_VBDCDCLD,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDI_VBDCDCLD) {
            console.log("CONSOLE_AU_VBDI_VBDCDCLD: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDI_VBDCDCLD) {
            console.log("CONSOLE_AU_VBDI_VBDCDCLD ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDI_VBDCDCLD(so_ky_hieu, trich_yeu, page, size, ma_ctcb_duyet, ma_don_vi_quan_tri, nam).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDI_VBDDUQCLD(so_ky_hieu, trich_yeu, page, size, ma_ctcb_duyet, ma_don_vi_quan_tri, nam) {
    let result = []
    var form = new FormData();
    form.append("page", parseInt(page));
    form.append("size", parseInt(size));
    if (checkVersionWeb(23)) {
        form.append("nam", 0);
        if (nam !== 0) {
            form.append("ngay_duyet_tu_ngay", "01/01/" + nam);
            form.append("ngay_duyet_den_ngay", "31/12/" + nam);
        } else {
            form.append("trich_yeu", trich_yeu);
        }
    } else {
        form.append("so_ky_hieu", so_ky_hieu);
        form.append("trich_yeu", trich_yeu);
        form.append("nam", nam);
    }
    form.append("ma_ctcb_duyet", parseInt(ma_ctcb_duyet))
    form.append("ma_don_vi_quan_tri", parseInt(ma_don_vi_quan_tri));

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDI_VBDDUQCLD,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDI_VBDDUQCLD) {
            console.log("CONSOLE_AU_VBDI_VBDDUQCLD: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDI_VBDDUQCLD) {
            console.log("CONSOLE_AU_VBDI_VBDDUQCLD ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDI_VBDDUQCLD(so_ky_hieu, trich_yeu, page, size, ma_ctcb_duyet, ma_don_vi_quan_tri, nam).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDI_VBDCPHCLD(so_ky_hieu, trich_yeu, page, size, ma_ctcb_duyet, ma_don_vi_quan_tri, nam) {
    let result = []
    var form = new FormData();
    form.append("page", parseInt(page));
    form.append("size", parseInt(size));
    if (checkVersionWeb(23)) {
        form.append("nam", 0);
        if (nam !== 0) {
            form.append("ngay_duyet_tu_ngay", "01/01/" + nam);
            form.append("ngay_duyet_den_ngay", "31/12/" + nam);
        } else {
            form.append("trich_yeu", trich_yeu);
        }
    } else {
        form.append("so_ky_hieu", so_ky_hieu);
        form.append("trich_yeu", trich_yeu);
        form.append("nam", nam);
    }
    form.append("ma_ctcb_duyet", parseInt(ma_ctcb_duyet))
    form.append("ma_don_vi_quan_tri", parseInt(ma_don_vi_quan_tri));

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDI_VBDCPHCLD,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDI_VBDCPHCLD) {
            console.log("CONSOLE_AU_VBDI_VBDCPHCLD: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDI_VBDCPHCLD) {
            console.log("CONSOLE_AU_VBDI_VBDCPHCLD ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDI_VBDCPHCLD(so_ky_hieu, trich_yeu, page, size, ma_ctcb_duyet, ma_don_vi_quan_tri, nam).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDI_VBDDPHCLD(so_ky_hieu, trich_yeu, page, size, ma_ctcb_duyet, ma_don_vi_quan_tri, nam) {
    let result = []
    var form = new FormData();
    form.append("page", parseInt(page));
    form.append("size", parseInt(size));
    if (checkVersionWeb(23)) {
        form.append("nam", 0);
        if (nam !== 0) {
            form.append("ngay_duyet_tu_ngay", "01/01/" + nam);
            form.append("ngay_duyet_den_ngay", "31/12/" + nam);
        } else {
            form.append("trich_yeu", trich_yeu);
        }
    } else {
        form.append("so_ky_hieu", so_ky_hieu);
        form.append("trich_yeu", trich_yeu);
        form.append("nam", nam);
    }
    form.append("ma_ctcb_duyet", parseInt(ma_ctcb_duyet))
    form.append("ma_don_vi_quan_tri", parseInt(ma_don_vi_quan_tri));

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDI_VBDDPHCLD,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDI_VBDDPHCLD) {
            console.log("CONSOLE_AU_VBDI_VBDDPHCLD: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDI_VBDDPHCLD) {
            console.log("CONSOLE_AU_VBDI_VBDDPHCLD ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDI_VBDDPHCLD(so_ky_hieu, trich_yeu, page, size, ma_ctcb_duyet, ma_don_vi_quan_tri, nam).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDI_DSVTTDVQT(ma_don_vi_quan_tri) {
    let result = []
    var form = new Object()
    form.ma_don_vi_quan_tri = ma_don_vi_quan_tri

    await axios({
        method: "GET",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDI_DSVTTDVQT,
        headers: setHeaderToken(),
        params: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDI_DSVTTDVQT) {
            console.log("CONSOLE_AU_VBDI_DSVTTDVQT: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDI_DSVTTDVQT) {
            console.log("CONSOLE_AU_VBDI_DSVTTDVQT ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDI_DSVTTDVQT(ma_don_vi_quan_tri).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDI_LDDVBD(ma_ctcb_duyet, ma_van_ban_di) {
    let result = false
    var form = new Object()
    form.ma_ctcb_duyet = ma_ctcb_duyet
    form.ma_van_ban_di = ma_van_ban_di

    await axios({
        method: "GET",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDI_LDDVBD,
        headers: setHeaderToken(),
        params: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDI_LDDVBD) {
            console.log("CONSOLE_AU_VBDI_LDDVBD: " + JSON.stringify(response))
        }
        if (response.data.message === "Thực thi thành công") {
            result = true
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDI_LDDVBD) {
            console.log("CONSOLE_AU_VBDI_LDDVBD ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDI_LDDVBD(ma_ctcb_duyet, ma_van_ban_di).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDI_LDCVT(ma_van_ban_di, ma_ctcb_gui, chuoi_ma_ctcb_nhan, noi_dung_chuyen, file_dinh_kem, file_moi, loai_xu_ly, ma_xu_ly_di_cha, sms) {
    let result = false
    var form = new FormData();
    form.append("ma_van_ban_di", parseInt(ma_van_ban_di));
    form.append("ma_ctcb_gui", parseInt(ma_ctcb_gui));
    form.append("chuoi_ma_ctcb_nhan", chuoi_ma_ctcb_nhan);
    form.append("noi_dung_chuyen", noi_dung_chuyen);
    form.append("file_dinh_kem", file_dinh_kem);
    form.append("file_moi", file_moi);
    form.append("loai_xu_ly", parseInt(loai_xu_ly));
    form.append("ma_xu_ly_di_cha", parseInt(ma_xu_ly_di_cha));
    form.append("sms", parseInt(sms));

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDI_LDCVT,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDI_LDCVT) {
            console.log("CONSOLE_AU_VBDI_LDCVT: " + JSON.stringify(response))
        }
        if (response.data.message === "Thực thi thành công") {
            result = true
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDI_LDCVT) {
            console.log("CONSOLE_AU_VBDI_LDCVT ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDI_LDCVT(ma_van_ban_di, ma_ctcb_gui, chuoi_ma_ctcb_nhan, noi_dung_chuyen, file_dinh_kem, file_moi, loai_xu_ly, ma_xu_ly_di_cha, sms).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDI_DSLDCDK(ma_don_vi) {
    let result = []
    var form = new FormData();
    form.append("ma_don_vi", parseInt(ma_don_vi));

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDI_DSLDCDK,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDI_DSLDCDK) {
            console.log("CONSOLE_AU_VBDI_DSLDCDK: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDI_DSLDCDK) {
            console.log("CONSOLE_AU_VBDI_DSLDCDK ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDI_DSLDCDK(ma_don_vi).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDI_CVCVBDCLD(ma_van_ban_di, ma_ctcb_gui, ma_ctcb_nhan, noi_dung_chuyen, file_dinh_kem, ma_xu_ly_di_cha, sms, file_moi = "") {
    let result = ""
    var form = new FormData();
    form.append("ma_van_ban_di", parseInt(ma_van_ban_di));
    form.append("ma_ctcb_gui", parseInt(ma_ctcb_gui));
    form.append("ma_ctcb_nhan", parseInt(ma_ctcb_nhan));
    form.append("noi_dung_chuyen", noi_dung_chuyen);
    form.append("file_dinh_kem", file_dinh_kem);
    form.append("ma_xu_ly_di_cha", parseInt(ma_xu_ly_di_cha));
    form.append("sms", parseInt(sms));
    form.append("file_moi", file_moi);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDI_CVCVBDCLD,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDI_CVCVBDCLD) {
            console.log("CONSOLE_AU_VBDI_CVCVBDCLD: " + JSON.stringify(response))
        }
        if (response.data.message === "Thực thi thành công") {
            result = response.data.id
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDI_CVCVBDCLD) {
            console.log("CONSOLE_AU_VBDI_CVCVBDCLD ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDI_CVCVBDCLD(ma_van_ban_di, ma_ctcb_gui, ma_ctcb_nhan, noi_dung_chuyen, file_dinh_kem, ma_xu_ly_di_cha, sms).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDI_DSCBTDVTS(ma_don_vi) {
    let result = []
    var form = new Object()
    form.ma_don_vi = parseInt(ma_don_vi)

    await axios({
        method: "GET",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDI_DSCBTDVTS,
        headers: setHeaderToken(),
        params: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDI_DSCBTDVTS) {
            console.log("CONSOLE_AU_VBDI_DSCBTDVTS: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDI_DSCBTDVTS) {
            console.log("CONSOLE_AU_VBDI_DSCBTDVTS ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDI_DSCBTDVTS(ma_don_vi).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDI_LDCVBDCCV(ma_van_ban_di, ma_ctcb_gui, ma_ctcb_nhan, noi_dung_chuyen, file_dinh_kem, ma_xu_ly_di_cha, sms, file_moi = "") {
    let result = ""
    var form = new FormData();
    form.append("ma_van_ban_di", parseInt(ma_van_ban_di));
    form.append("ma_ctcb_gui", parseInt(ma_ctcb_gui));
    form.append("ma_ctcb_nhan", parseInt(ma_ctcb_nhan));
    form.append("noi_dung_chuyen", noi_dung_chuyen);
    form.append("file_dinh_kem", file_dinh_kem);
    form.append("file_moi", file_moi);
    form.append("ma_xu_ly_di_cha", parseInt(ma_xu_ly_di_cha));
    form.append("sms", parseInt(sms));

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDI_LDCVBDCCV,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDI_LDCVBDCCV) {
            console.log("CONSOLE_AU_VBDI_LDCVBDCCV: " + JSON.stringify(response))
        }
        if (response.data.message === "Thực thi thành công") {
            result = response.data.id
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDI_LDCVBDCCV) {
            console.log("CONSOLE_AU_VBDI_LDCVBDCCV ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDI_LDCVBDCCV(ma_van_ban_di, ma_ctcb_gui, ma_ctcb_nhan, noi_dung_chuyen, file_dinh_kem, ma_xu_ly_di_cha, sms).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDI_CVCVBDCCV(ma_van_ban_di, ma_ctcb_gui, chuoi_chi_tiet_temp, noi_dung_chuyen, file_dinh_kem, ma_xu_ly_di_cha, file_moi = "") {
    let result = ""
    var form = new FormData();
    form.append("ma_van_ban_di", parseInt(ma_van_ban_di));
    form.append("ma_ctcb_gui", parseInt(ma_ctcb_gui));
    form.append("noi_dung_chuyen", noi_dung_chuyen);
    form.append("ma_xu_ly_di_cha", parseInt(ma_xu_ly_di_cha));
    form.append("file_dinh_kem", file_dinh_kem);
    form.append("chuoi_chi_tiet_temp", JSON.stringify(chuoi_chi_tiet_temp));
    form.append("file_moi", file_moi);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDI_CVCVBDCCV,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDI_CVCVBDCCV) {
            console.log("CONSOLE_AU_VBDI_CVCVBDCCV: " + JSON.stringify(response))
        }
        console.log("CONSOLE_AU_VBDI_CVCVBDCCV: " + JSON.stringify(response))
        if (response.data.message === "Thực thi thành công") {
            result = response.data.id
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDI_CVCVBDCCV) {
            console.log("CONSOLE_AU_VBDI_CVCVBDCCV ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDI_CVCVBDCCV(ma_van_ban_di, ma_ctcb_gui, chuoi_chi_tiet_temp, noi_dung_chuyen, file_dinh_kem, ma_xu_ly_di_cha).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDI_CVCVT(ma_van_ban_di, ma_ctcb_gui, chuoi_ma_ctcb_nhan, noi_dung_chuyen,
    file_moi, file_dinh_kem, ma_xu_ly_di_cha, sms, ma_ctcb_duyet) {
    let result = false
    var form = new FormData();
    form.append("ma_van_ban_di", ma_van_ban_di);
    form.append("ma_ctcb_gui", ma_ctcb_gui);
    form.append("chuoi_ma_ctcb_nhan", chuoi_ma_ctcb_nhan);
    form.append("noi_dung_chuyen", noi_dung_chuyen);
    form.append("file_moi", file_moi);
    form.append("file_dinh_kem", file_dinh_kem);
    form.append("loai_xu_ly", 3);
    form.append("ma_xu_ly_di_cha", ma_xu_ly_di_cha);
    form.append("sms", sms);
    form.append("ma_ctcb_duyet", ma_ctcb_duyet);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDI_CVCVT,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDI_CVCVT) {
            console.log("CONSOLE_AU_VBDI_CVCVT: " + JSON.stringify(response))
        }
        console.log("CONSOLE_AU_VBDI_CVCVT: " + JSON.stringify(response))
        if (response.data.message === "Thực thi thành công") {
            result = true
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDI_CVCVT) {
            console.log("CONSOLE_AU_VBDI_CVCVT ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDI_CVCVT(ma_van_ban_di, ma_ctcb_gui, chuoi_ma_ctcb_nhan, noi_dung_chuyen,
                    file_moi, file_dinh_kem, ma_xu_ly_di_cha, sms, ma_ctcb_duyet).then(async (resCallback) => {
                        result = resCallback
                    })
            })
        }
    })
    return result
}

async function AU_VBDI_LDCVBDCLD(ma_van_ban_di, ma_ctcb_gui, ma_ctcb_nhan, noi_dung_chuyen, file_dinh_kem, ma_xu_ly_di_cha, sms, file_moi = "") {
    let result = ""
    var form = new FormData();
    form.append("ma_van_ban_di", parseInt(ma_van_ban_di));
    form.append("ma_ctcb_gui", parseInt(ma_ctcb_gui));
    form.append("ma_ctcb_nhan", parseInt(ma_ctcb_nhan));
    form.append("noi_dung_chuyen", noi_dung_chuyen);
    form.append("file_dinh_kem", file_dinh_kem);
    form.append("ma_xu_ly_di_cha", parseInt(ma_xu_ly_di_cha));
    form.append("sms", parseInt(sms));
    form.append("file_moi", file_moi);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDI_LDCVBDCLD,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDI_LDCVBDCLD) {
            console.log("CONSOLE_AU_VBDI_LDCVBDCLD: " + JSON.stringify(response))
        }
        if (response.data.message === "Thực thi thành công") {
            result = response.data.id
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDI_LDCVBDCLD) {
            console.log("CONSOLE_AU_VBDI_LDCVBDCLD ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDI_LDCVBDCLD(ma_van_ban_di, ma_ctcb_gui, ma_ctcb_nhan, noi_dung_chuyen, file_dinh_kem, ma_xu_ly_di_cha, sms).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDI_DSBPVBDICHGG(ma_van_ban_den, ma_don_vi_quan_tri) {
    let result = ""
    let url = global.AU_ROOT + URL_CONFIG.AU_VBDI_DSBPVBDICHGG
    url += "?ma_van_ban_den=" + ma_van_ban_den
    url += "&ma_don_vi_quan_tri=" + ma_don_vi_quan_tri
    url += "&type=1" //Lấy ra list

    await axios({
        method: "GET",
        url: url,
        headers: setHeaderToken(),
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDI_DSBPVBDICHGG) {
            console.log("CONSOLE_AU_VBDI_DSBPVBDICHGG: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            let arrResult = []
            for (let item of response.data.data) {
                if (item.file_phieu_trinh) {
                    arrResult.push(item.file_phieu_trinh)
                }
            }
            if (arrResult.length > 0) {
                result = arrResult.join(":")
            }
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDI_DSBPVBDICHGG) {
            console.log("CONSOLE_AU_VBDI_DSBPVBDICHGG ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDI_DSBPVBDICHGG(ma_van_ban_den, ma_don_vi_quan_tri).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDI_CVGY(file_dinh_kem, ma_ctcb_gui, ma_van_ban_di, ma_xu_ly_di_cha, noi_dung_chuyen) {
    let result = false
    var form = new FormData();
    form.append("chuoi_file_cu", file_dinh_kem);
    form.append("file_dinh_kem", file_dinh_kem);
    form.append("file_moi", "");
    form.append("ma_ctcb_gui", ma_ctcb_gui);
    form.append("ma_van_ban_di", ma_van_ban_di);
    form.append("ma_xu_ly_di_cha", ma_xu_ly_di_cha);
    form.append("noi_dung_chuyen", noi_dung_chuyen);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDI_CVGY,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDI_CVGY) {
            console.log("CONSOLE_AU_VBDI_CVGY: " + JSON.stringify(response))
        }
        if (response.data.message === "Thực thi thành công") {
            result = true
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDI_CVGY) {
            console.log("CONSOLE_AU_VBDI_CVGY ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDI_CVGY(file_dinh_kem, ma_ctcb_gui, ma_van_ban_di, ma_xu_ly_di_cha, noi_dung_chuyen).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDI_CNLF(chuoi_xoa, chuoi_con_lai, ma_van_ban_di, ma_ctcb, trich_yeu, ten_can_bo_xoa) {
    let result = false
    var form = new FormData();
    form.append("chuoi_xoa", chuoi_xoa);
    form.append("chuoi_con_lai", chuoi_con_lai);
    form.append("ma_van_ban_di", ma_van_ban_di);
    form.append("ma_ctcb", ma_ctcb);
    form.append("trich_yeu", trich_yeu);
    form.append("ten_can_bo_xoa", ten_can_bo_xoa);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDI_CNLF,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBDI_CNLF) {
            console.log("CONSOLE_AU_VBDI_CNLF: " + JSON.stringify(response))
        }
        if (response.data.message === "Thực thi thành công") {
            result = true
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBDI_CNLF) {
            console.log("CONSOLE_AU_VBDI_CNLF ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDI_CNLF(file_dinh_kem, ma_ctcb_gui, ma_van_ban_di, ma_xu_ly_di_cha, noi_dung_chuyen).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBDI_DSDPH7N(ma_ctcb, ma_don_vi_quan_tri, page, size, tu_ngay, den_ngay, keyword) {
    let rs = [];
    var formData = new FormData();
    formData.append("den_ngay", den_ngay);
    formData.append("tu_ngay", tu_ngay);
    formData.append("ma_ctcb", ma_ctcb);
    formData.append("ma_don_vi_quan_tri", ma_don_vi_quan_tri);
    formData.append("ma_linh_vuc_van_ban", 0);
    formData.append("ma_loai_van_ban", 0);
    formData.append("ma_so_van_ban", 0);
    formData.append("nguoi_du_thao", "ALL");
    formData.append("nguoi_ky", "ALL");
    formData.append("page", page);
    formData.append("size", size);
    formData.append("phong_ban", 0);
    formData.append("tim_chinh_xac", 0);
    formData.append("tim_cx", false);
    formData.append("trang_thai", 1);
    formData.append("vai_tro", "cv");
    if (keyword != "") {
        formData.append("trich_yeu", keyword);

    }
    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDI_DPH7N,
        headers: setHeaderToken(),
        data: formData,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (response.data.success) {
            rs = response.data;
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDI_DSDPH7N(ma_ctcb, ma_don_vi_quan_tri, page, size, tu_ngay, den_ngay).then(async (resCallback) => {
                    rs = resCallback
                })
            })
        }
    });
    return rs;
}

// Begin Luchq: function get data ds cho thuong truc ky
async function AU_DS_CHO_THUONG_TRUC(
    ma_can_bo,
    ma_ctcb_cv,
    ma_don_vi_quan_tri,
    size,
    page,
    nam,
    keyword
) {
    let rs = [];
    var formdata = new FormData();
    formdata.append("co_tep_tin", -1);
    formdata.append("ma_can_bo", ma_can_bo);
    formdata.append("ma_ctcb_cv", ma_ctcb_cv);
    formdata.append("ma_don_vi_quan_tri", ma_don_vi_quan_tri);
    formdata.append("ma_loai_ttdh", 0);
    formdata.append("nam", 0);
    if (keyword != "") {
        formdata.append("trich_yeu", keyword);
    }
    if (nam !== 0) {
        formdata.append("ngay_tao_tu_ngay", "01/01/" + nam);
        formdata.append("ngay_tao_den_ngay", "31/12/" + nam);
    }
    formdata.append("page", page);
    formdata.append("size", size);
    formdata.append("trang_thai_ttdh_gui", -1);
    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDI_CHO_THUONG_TRUC,
        headers: setHeaderToken(),
        data: formdata,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (response.data.message == "Lấy dữ liệu thành công") {
            rs = response.data.data;
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_DS_CHO_THUONG_TRUC(ma_can_bo,
                    ma_ctcb_cv,
                    ma_don_vi_quan_tri,
                    ma_loai_ttdh,
                    nam,
                    ngay_tao_den_ngay,
                    ngay_tao_tu_ngay,
                    page,
                    size).then(async (resCallback) => {
                        rs = resCallback
                    })
            })
        }
    });
    return rs;
}
// End Luchq: function get data ds cho thương truc ky
// Begin Luchq: function get danh sách chuyên viên pháp chế
 async function AU_VBDI_DSCVPCUT(ma_don_vi_tao){
     let rs = []
     var formdata = new FormData();
     formdata.append('ma_don_vi_quan_tri',global.ma_don_vi_quan_tri);
     formdata.append('ma_phong_ban',ma_don_vi_tao)
     
     await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBDI_DSCBPCUT,
        headers: setHeaderToken(),
        data: formdata,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        rs = response.data.data;
        console.log('AU_VBDI_DSCVPCUT'+JSON.stringify(response));
    });
     return rs;
 }
//End Luchq: function get danh sách chuyên viên pháp chế
 async function AU_VBDI_CHUYENPC(can_bo_xu_ly_chinh, chuoi_chi_tiet_temp,chuoi_file_cu,file_dinh_kem, ma_ctcb_gui,ma_van_ban_di,ma_xu_ly_di_cha,noi_dung_chuyen,sms,file_moi="",isDuyet){
     let rs = [];
     let api = '';
     var formData = new FormData();
     formData.append('can_bo_xu_ly_chinh',JSON.stringify(can_bo_xu_ly_chinh));
     formData.append('chuoi_chi_tiet_temp',JSON.stringify(chuoi_chi_tiet_temp));
     formData.append('chuoi_file_cu',chuoi_file_cu);
     formData.append('file_dinh_kem',file_dinh_kem);
     formData.append('file_moi',file_moi);
     formData.append('ma_ctcb_gui',parseInt(ma_ctcb_gui));
     formData.append('ma_van_ban_di',parseInt(ma_van_ban_di));
     formData.append('ma_xu_ly_di_cha',parseInt(ma_xu_ly_di_cha));
     formData.append('noi_dung_chuyen',noi_dung_chuyen);
     formData.append('sms',sms);
     if(isDuyet){
        api = global.AU_ROOT + URL_CONFIG.AU_VBDI_LDCHUYENPHAPCHE;
     }else{
        api = global.AU_ROOT + URL_CONFIG.AU_VBDI_CVCHUYENPC;
     }
     await axios({
         method:"POST",
         url: api,
         headers: setHeaderToken(),
         data: formData,
         timeout: AppConfig.TIME_OUT_SHORT
     }).then(response => {
        rs = response.data.data;
    }).catch(async error => {
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBDI_LDCHUYENPC(ma_can_bo,
                    can_bo_xu_ly_chinh,
                    chuoi_chi_tiet_temp,
                    chuoi_file_cu,
                    file_dinh_kem,
                    ma_ctcb_gui,
                    ma_van_ban_di,
                    ma_xu_ly_di_cha,
                    noi_dung_chuyen,
                    sms,
                    file_moi="").then(async (resCallback) => {
                        rs = resCallback
                    })
            })
        }
    });;
     return rs;
 }


export default {
    AU_VBDI_DSVBDCXLCCV,
    AU_VBDI_DSVBDCXLCCV_V23,
    AU_VBDI_DSVBDCXLCCV_V32,
    AU_VBDI_DSVBDDXLCCV,
    AU_VBDI_DSVBDDXLCCV_V23,
    AU_VBDI_DSVBDDXLCCV_V32,
    AU_VBDI_DSVBDCPHCCV,
    AU_VBDI_DSVBDCPHCCV_V23,
    AU_VBDI_DSVBDCPHCCV_V32,
    AU_VBDI_DSVBDDPHCCV,
    AU_VBDI_DSVBDDPHCCV_V23,
    AU_VBDI_DSVBDDPHCCV_V32,
    AU_VBDI_CNDX,
    AU_VBDI_CTVBD,
    AU_VBDI_QTXLVBD,
    AU_VBDI_HTVBDI,
    AU_VBDI_VBDCDCLD,
    AU_VBDI_VBDDUQCLD,
    AU_VBDI_VBDCPHCLD,
    AU_VBDI_VBDDPHCLD,
    AU_VBDI_DSVTTDVQT,
    AU_VBDI_LDDVBD,
    AU_VBDI_LDCVT,
    AU_VBDI_DSLDCDK,
    AU_VBDI_CVCVBDCLD,
    AU_VBDI_DSCBTDVTS,
    AU_VBDI_LDCVBDCCV,
    AU_VBDI_CVCVBDCCV,
    AU_VBDI_CVCVT,
    AU_VBDI_LDCVBDCLD,
    AU_VBDI_DSBPVBDICHGG,
    AU_VBDI_CVGY,
    AU_VBDI_CNLF,
    AU_VBDI_DSDPH7N,
    AU_DS_CHO_THUONG_TRUC,
    AU_VBDI_DSCVPCUT,
    AU_VBDI_CHUYENPC
}
