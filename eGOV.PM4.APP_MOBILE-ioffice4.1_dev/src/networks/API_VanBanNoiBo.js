import { URL_CONFIG } from "./Config"
import { Platform } from "react-native"
import DeviceInfo from 'react-native-device-info'
import { Toast } from "native-base"
import axios from "axios"
import { ReplaceAll, checkVersionWeb } from "../untils/TextUntils"
import { AppConfig } from "../AppConfig"
import API_Login from "./API_Login"

function showWarning(error) {
    Toast.show({ text: "Kết nối bị gián đoạn!", type: "warning", buttonText: "Đóng", position: "top", duration: 2000 })
    if (error) {
        SendLog(error)
    }
};

async function SendLog(message) {
    try {
        let obj = new Object
        let date = new Date()
        obj.Model = DeviceInfo.getModelSync()
        obj.OS = Platform.OS.toString()
        obj.SystemVersion = DeviceInfo.getSystemVersionSync()
        obj.AppVersion = DeviceInfo.getVersionSync()
        obj.CurrentTime = date.toLocaleDateString() + " " + date.toLocaleTimeString()
        obj.Module = "API_VanBanNoiBo"
        obj.Domain = global.AU_ROOT ? global.AU_ROOT : ""
        delete message.headers
        obj.Message = message
        let text = ReplaceAll(JSON.stringify(obj), String.fromCharCode(34), "'")
        let url = AppConfig.AppDefaultLogURLSendText + text
        await axios({
            method: "POST",
            url: url,
            timeout: AppConfig.TIME_OUT_SHORT
        })
    } catch (error) {
        console.log("Send log error: " + error)
    }
}

function setHeaderToken() {
    return { "Authorization": "Bearer " + global.access_token }
};

// DANH SACH VAN BAN NOI BO DA NHAN
async function AU_VBNB_DSVBNBDN(so_hieu, trich_yeu, page, size, ma_ctcb, xem = -1) {
    let result = []
    var form = new FormData();
    form.append("so_hieu", so_hieu);
    form.append("trich_yeu", trich_yeu);
    form.append("page", parseInt(page));
    form.append("size", parseInt(size));
    form.append("ma_ctcb", parseInt(ma_ctcb));
    form.append("xem", xem);
    form.append("trang_thai", 3);

    if (checkVersionWeb(59)) {
        form.append("ma_cap_do", -1);
    }

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBNB_DSVBNBDN,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBNB_DSVBNBDN) {
            console.log("CONSOLE_AU_VBNB_DSVBNBDN: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBNB_DSVBNBDN) {
            //console.log("CONSOLE_AU_VBNB_DSVBNBDN ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBNB_DSVBNBDN(so_hieu, trich_yeu, page, size, ma_ctcb).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBNB_DSVBNBDN_GetPage(so_hieu, trich_yeu, page, size, ma_ctcb, xem = -1) {
    let result = []
    var form = new FormData();
    form.append("so_hieu", so_hieu);
    form.append("trich_yeu", trich_yeu);
    form.append("page", parseInt(page));
    form.append("size", parseInt(size));
    form.append("ma_ctcb", parseInt(ma_ctcb));
    form.append("xem", xem);
    form.append("trang_thai", 3);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBNB_DSVBNBDN,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBNB_DSVBNBDN) {
            console.log("CONSOLE_AU_VBNB_DSVBNBDN_GetPage: " + JSON.stringify(response))
        }
        if (response.data.success) {
            result = response.data.total_page;
        } else {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBNB_DSVBNBDN) {
            //console.log("CONSOLE_AU_VBNB_DSVBNBDN_GetPage ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBNB_DSVBNBDN_GetPage(so_hieu, trich_yeu, page, size, ma_ctcb).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

// CHI TIET VAN BAN NOI BO DA NHAN
async function AU_VBNB_CTVBNBDN(ma_vbnb_gui) {
    let result = []
    var form = new Object()
    form.ma_vbnb_gui = ma_vbnb_gui

    await axios({
        method: "GET",
        url: global.AU_ROOT + URL_CONFIG.AU_VBNB_CTVBNB,
        headers: setHeaderToken(),
        params: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBNB_CTVBNBDN) {
            console.log("CONSOLE_AU_VBNB_CTVBNBDN: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBNB_CTVBNBDN) {
            console.log("CONSOLE_AU_VBNB_CTVBNBDN ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBNB_CTVBNBDN(ma_vbnb_gui).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

// DANH SACH VAN BAN NOI BO DA GUI DA PHAT HANH
async function AU_VBNB_DSVBNBDGDPH(so_hieu, trich_yeu, page, size, ma_ctcb) {
    let result = []
    var form = new FormData();
    form.append("so_hieu", so_hieu);
    form.append("trich_yeu", trich_yeu);
    form.append("page", parseInt(page));
    form.append("size", parseInt(size));
    form.append("ma_ctcb", parseInt(ma_ctcb));
    form.append("ma_linh_vuc", 0);
    form.append("ma_loai_van_ban", 0);

    if (checkVersionWeb(59)) {
        form.append("ma_cap_do", -1);
    } else {
        form.append("ma_cap_do", 0);
    }

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBNB_DSVBNBDGDPH,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBNB_DSVBNBDGDPH) {
            console.log("CONSOLE_AU_VBNB_DSVBNBDGDPH: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBNB_DSVBNBDGDPH) {
            console.log("CONSOLE_AU_VBNB_DSVBNBDGDPH ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBNB_DSVBNBDGDPH(so_hieu, trich_yeu, page, size, ma_ctcb).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}
async function AU_VBNB_DSVBNBDGDPH_GetPage(so_hieu, trich_yeu, page, size, ma_ctcb) {
    let result = []
    var form = new FormData();
    form.append("so_hieu", so_hieu);
    form.append("trich_yeu", trich_yeu);
    form.append("page", parseInt(page));
    form.append("size", parseInt(size));
    form.append("ma_ctcb", parseInt(ma_ctcb));
    form.append("ma_linh_vuc", 0);
    form.append("ma_cap_do", 0);
    form.append("ma_loai_van_ban", 0);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBNB_DSVBNBDGDPH,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBNB_DSVBNBDGDPH) {
            console.log("CONSOLE_AU_VBNB_DSVBNBDGDPH_GetPage: " + JSON.stringify(response))
        }
        if (response.data.success) {
            result = response.data.total_page
        } else {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBNB_DSVBNBDGDPH) {
            console.log("CONSOLE_AU_VBNB_DSVBNBDGDPH_GetPage ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBNB_DSVBNBDGDPH_GetPage(so_hieu, trich_yeu, page, size, ma_ctcb).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

// XEM VAN BAN NOI BO
async function AU_VBNB_XVBNB(ma_vbnb_gui, trang_thai_xu_ly) {
    let result = false
    var form = new FormData();
    form.append("ma_vbnb_gui", parseInt(ma_vbnb_gui));
    form.append("trang_thai_xu_ly", parseInt(trang_thai_xu_ly));

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBNB_CNVBNBCCS,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBNB_XVBNB) {
            console.log("CONSOLE_AU_VBNB_XVBNB: " + JSON.stringify(response))
        }
        if (response.data.message === "Thực thi thành công") {
            result = true
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBNB_XVBNB) {
            console.log("CONSOLE_AU_VBNB_XVBNB ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBNB_XVBNB(ma_vbnb_gui, trang_thai_xu_ly).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

// CHI TIET VAN BAN NOI BO DA GUI
async function AU_VBNB_CTVBNBDG(ma_vbnb_gui) {
    let result = []
    var form = new Object()
    form.ma_vbnb_gui = ma_vbnb_gui
    await axios({
        method: "GET",
        url: global.AU_ROOT + URL_CONFIG.AU_VBNB_CTVBNBG,
        headers: setHeaderToken(),
        params: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBNB_CTVBNBDG) {
            console.log("CONSOLE_AU_VBNB_CTVBNBDG: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBNB_CTVBNBDG) {
            console.log("CONSOLE_AU_VBNB_CTVBNBDG ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBNB_CTVBNBDG(ma_vbnb_gui).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

// CAY QUA TRINH XU LY VAN BAN NOI BO
async function AU_VBNB_CQTXLVBNB(ma_vbnb_kc) {
    let result = []
    var form = new Object()
    form.ma_vbnb = ma_vbnb_kc

    await axios({
        method: "GET",
        url: global.AU_ROOT + URL_CONFIG.AU_VBNB_CQTXLVBNB,
        headers: setHeaderToken(),
        params: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBNB_CQTXLVBNB) {
            console.log("CONSOLE_AU_VBNB_CQTXLVBNB: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBNB_CQTXLVBNB) {
            console.log("CONSOLE_AU_VBNB_CQTXLVBNB ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBNB_CQTXLVBNB(ma_vbnb_kc).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

// DANH SACH CAN BO NHAN VAN BAN NOI BO
async function AU_VBNB_DSCBNVBNB(ma_don_vi_quan_tri) {
    let result = []
    let url = global.AU_ROOT + URL_CONFIG.AU_VBNB_DSCBNVBNB
    url += "?ma_don_vi_quan_tri=" + ma_don_vi_quan_tri
    url += "&type=tree"

    await axios({
        method: "GET",
        url: url,
        headers: setHeaderToken(),
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBNB_DSCBNVBNB) {
            console.log("CONSOLE_AU_VBNB_DSCBNVBNB: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBNB_DSCBNVBNB) {
            console.log("CONSOLE_AU_VBNB_DSCBNVBNB ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBNB_DSCBNVBNB(ma_don_vi_quan_tri).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

// NHOM CAN BO THEO LOAI NHOM
async function AU_VBNB_CNCBTLN(ma_can_bo) {
    let result = []
    var form = new Object()
    form.ma_can_bo = ma_can_bo
    form.ma_loai_nhom_nguoi_nhan = 3

    await axios({
        method: "GET",
        url: global.AU_ROOT + URL_CONFIG.AU_VBNB_CNCBTLN,
        headers: setHeaderToken(),
        params: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBNB_CNCBTLN) {
            console.log("CONSOLE_AU_VBNB_CNCBTLN: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBNB_CNCBTLN) {
            console.log("CONSOLE_AU_VBNB_CNCBTLN ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBNB_CNCBTLN(ma_can_bo).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

async function AU_VBNB_DSNCBNVBNB() {
    let result = []
    let url = global.AU_ROOT + URL_CONFIG.AU_VBNB_DSNCBNVBNB
    url += "?ma_can_bo=" + global.ma_can_bo

    await axios({
        method: "GET",
        url: url,
        headers: setHeaderToken(),
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBNB_DSNCBNVBNB) {
            console.log("CONSOLE_AU_VBNB_DSNCBNVBNB: " + JSON.stringify(response))
        }
        if (response.data.message === "Lấy dữ liệu thành công") {
            result = response.data.data
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBNB_DSNCBNVBNB) {
            console.log("CONSOLE_AU_VBNB_DSNCBNVBNB ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBNB_DSNCBNVBNB().then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

// CHUYEN VAN BAN NOI BO
async function AU_VBNB_CVBNB(ma_vbnb_kc, ma_ctcb_gui, chuoi_ma_ctcb_nhan, yKienXuLy, trang_thai_xu_ly, sms, ma_vbnb_gui_cha) {
    let result = ""
    var form = new FormData();
    form.append("ma_vbnb_kc", parseInt(ma_vbnb_kc));
    form.append("ma_ctcb_gui", parseInt(ma_ctcb_gui));
    form.append("chuoi_ma_ctcb_nhan", chuoi_ma_ctcb_nhan.toString());
    form.append("y_kien_xu_ly", yKienXuLy);
    form.append("trang_thai_xu_ly", parseInt(trang_thai_xu_ly));
    form.append("sms", sms);
    form.append("ma_vbnb_gui_cha", parseInt(ma_vbnb_gui_cha));

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBNB_CVBNB,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBNB_CVBNB) {
            console.log("CONSOLE_AU_VBNB_CVBNB: " + JSON.stringify(response))
        }
        if (response.data.message === "Thực thi thành công") {
            result = response.data.id
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBNB_CVBNB) {
            console.log("CONSOLE_AU_VBNB_CVBNB ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBNB_CVBNB(ma_vbnb_kc, ma_ctcb_gui, chuoi_ma_ctcb_nhan, yKienXuLy, trang_thai_xu_ly, sms, ma_vbnb_gui_cha).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

// XOA VAN BAN NOI BO DA NHAN
async function AU_VBNB_XVBNBDN(chuoi_ma_vbnb) {
    let result = false
    var form = new FormData();
    form.append("chuoi_ma_vbnb", chuoi_ma_vbnb.toString());
    form.append("trang_thai", -1);

    await axios({
        method: "POST",
        url: global.AU_ROOT + URL_CONFIG.AU_VBNB_CNVBNBDN,
        headers: setHeaderToken(),
        data: form,
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBNB_XVBNBDN) {
            console.log("CONSOLE_AU_VBNB_XVBNBDN: " + JSON.stringify(response))
        }
        if (response.data.message === "Thực thi thành công") {
            result = true
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBNB_XVBNBDN) {
            console.log("CONSOLE_AU_VBNB_XVBNBDN ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBNB_XVBNBDN(chuoi_ma_vbnb).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

// XOA VAN BAN NOI BO DA GUI
async function AU_VBNB_XVBNBDG(chuoi_ma_vbnb) {
    let result = false
    let url = global.AU_ROOT + URL_CONFIG.AU_VBNB_HVBNB
    url += "?chuoi_ma_vbnb=" + chuoi_ma_vbnb.toString()
    url += "&trang_thai=1"

    await axios({
        method: "POST",
        url: url,
        headers: setHeaderToken(),
        timeout: AppConfig.TIME_OUT_SHORT
    }).then(response => {
        if (global.CONSOLE_AU_VBNB_XVBNBDG) {
            console.log("CONSOLE_AU_VBNB_XVBNBDG: " + JSON.stringify(response))
        }
        if (response.data.message === "Thực thi thành công") {
            result = true
        } else if (!response.data.success) {
            SendLog(response)
        }
    }).catch(async error => {
        if (global.CONSOLE_AU_VBNB_XVBNBDG) {
            console.log("CONSOLE_AU_VBNB_XVBNBDG ERROR: " + JSON.stringify(error))
        }
        if (error.response.status == 401) {
            await API_Login.AU_ACCESS_TOKEN(global.refresh_token).then(async () => {
                await AU_VBNB_XVBNBDG(chuoi_ma_vbnb).then(async (resCallback) => {
                    result = resCallback
                })
            })
        }
    })
    return result
}

export default {
    AU_VBNB_DSVBNBDN,
    AU_VBNB_DSVBNBDN_GetPage,
    AU_VBNB_CTVBNBDN,
    AU_VBNB_DSVBNBDGDPH,
    AU_VBNB_DSVBNBDGDPH_GetPage,
    AU_VBNB_XVBNB,
    AU_VBNB_CTVBNBDG,
    AU_VBNB_CQTXLVBNB,
    AU_VBNB_DSCBNVBNB,
    AU_VBNB_DSNCBNVBNB,
    AU_VBNB_CNCBTLN,
    AU_VBNB_CVBNB,
    AU_VBNB_XVBNBDN,
    AU_VBNB_XVBNBDG,
}
