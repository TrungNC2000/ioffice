import React, { Component } from "react"
import { Provider } from "react-redux"
import { StyleProvider } from 'native-base';
import Setup from "./src/Setup"
import myStore from "./src/stores/Store"
import getTheme from "./src/theme/components"
import variables from "./src/theme/variables/variables"

export default class App extends Component {
    render() {
        return (
            <Provider store={myStore}>
                <StyleProvider style={getTheme(variables)}>
                    <Setup/>
                </StyleProvider>
            </Provider>
        );
    }
}

